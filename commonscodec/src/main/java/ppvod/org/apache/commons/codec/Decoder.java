//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package ppvod.org.apache.commons.codec;

public interface Decoder {
    Object decode(Object var1) throws DecoderException;
}
