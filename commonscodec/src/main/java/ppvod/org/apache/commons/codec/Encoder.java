//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package ppvod.org.apache.commons.codec;

public interface Encoder {
    Object encode(Object var1) throws EncoderException;
}
