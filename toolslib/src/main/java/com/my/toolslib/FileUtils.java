package com.my.toolslib;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileUtils {

    //获取sd卡下载文件夹路径
    public static String getMediaDownloadDir() {
        boolean sdCardExist = Environment.getExternalStorageState().equals("mounted");
        String path = "/sdcard";
        if (sdCardExist) {
            File file = Environment.getExternalStorageDirectory();
            path = file.getPath();
        }

        try {
            path = path + File.separator+"mediaDown";
            File file = new File(path);
            if (!file.exists() && !file.mkdirs()) {
                path = "/sdcard";
            }
        } catch (Resources.NotFoundException var6) {
            var6.printStackTrace();
            path = "/sdcard";
        }

        return path;
    }

    //url转文件名
    public static String getUrlToFileName(String url){
        String s = MD5Utils.md5(url);
        int lastIndexOf = url.lastIndexOf(".");
        String s1 = url.substring(lastIndexOf,url.length());
        return s+"."+s1;
    }

    public static File getFileByUri(Uri uri, Context context) {//uri转file
        String path = null;
        if ("file".equals(uri.getScheme())) {
            path = uri.getEncodedPath();
            if (path != null) {
                path = Uri.decode(path);
                ContentResolver cr = context.getContentResolver();
                StringBuffer buff = new StringBuffer();
                buff.append("(").append(MediaStore.Images.ImageColumns.DATA).append("=").append("'" + path + "'").append(")");
                Cursor cur = cr.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{MediaStore.Images.ImageColumns._ID, MediaStore.Images.ImageColumns.DATA}, buff.toString(), null, null);
                int index = 0;
                int dataIdx = 0;
                for (cur.moveToFirst(); !cur.isAfterLast(); cur.moveToNext()) {
                    index = cur.getColumnIndex(MediaStore.Images.ImageColumns._ID);
                    index = cur.getInt(index);
                    dataIdx = cur.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                    path = cur.getString(dataIdx);
                }
                cur.close();
                if (index == 0) {
                } else {
                    Uri u = Uri.parse("content://media/external/images/media/" + index);
                    System.out.println("temp uri is :" + u);
                }
            }
            if (path != null) {
                return new File(path);
            }
        } else if ("content".equals(uri.getScheme())) {
            // 4.2.2以后
            String[] proj = {MediaStore.Images.Media.DATA};
            Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
            if (cursor.moveToFirst()) {
                int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                path = cursor.getString(columnIndex);
            }
            cursor.close();

            return new File(path);
        } else {
            //Log.i(TAG, "Uri Scheme:" + uri.getScheme());
        }
        return null;
    }

    /**
     * 根据byte数组生成文件
     *
     * @param bytes 生成文件用到的byte数组
     *              <p>
     *              file 文件全路径，如/aa/bb/cc.jpg
     */
    public static File createFileWithByte(byte[] bytes, File file) {
        // TODO Auto-generated method stub
        /**
         * 创建File对象，其中包含文件所在的目录以及文件的命名
         */
        // 创建FileOutputStream对象
        FileOutputStream outputStream = null;
        // 创建BufferedOutputStream对象
        BufferedOutputStream bufferedOutputStream = null;
        try {
            // 如果文件存在则删除
            if (file.exists()) {
                file.delete();
            }
            // 在文件系统中根据路径创建一个新的空文件
            file.createNewFile();
            // 获取FileOutputStream对象
            outputStream = new FileOutputStream(file);
            // 获取BufferedOutputStream对象
            bufferedOutputStream = new BufferedOutputStream(outputStream);
            // 往文件所在的缓冲输出流中写byte数据
            bufferedOutputStream.write(bytes);
            // 刷出缓冲输出流，该步很关键，要是不执行flush()方法，那么文件的内容是空的。
            bufferedOutputStream.flush();
        } catch (Exception e) {
            // 打印异常信息
            e.printStackTrace();
        } finally {
            // 关闭创建的流对象
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (bufferedOutputStream != null) {
                try {
                    bufferedOutputStream.close();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
        return file;
    }
    //本地视频插入到相册
    public static Uri videoInsertAlbum(Context context,File file){
        ContentResolver localContentResolver = context.getContentResolver();
        ContentValues localContentValues = getVideoContentValues(file, System.currentTimeMillis());
        Uri localUri = localContentResolver.insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, localContentValues);
        return localUri;
    }

    public static ContentValues getVideoContentValues(File paramFile, long paramLong)
    {
            ContentValues localContentValues = new ContentValues();
            localContentValues.put("title", paramFile.getName());
            localContentValues.put("_display_name", paramFile.getName());
            localContentValues.put("mime_type", "video/3gp");
            localContentValues.put("datetaken", Long.valueOf(paramLong));
            localContentValues.put("date_modified", Long.valueOf(paramLong));
            localContentValues.put("date_added", Long.valueOf(paramLong));
            localContentValues.put("_data", paramFile.getAbsolutePath());
            localContentValues.put("_size", Long.valueOf(paramFile.length()));
            return localContentValues;
    }


    //保存bitmap至文件夹
    public static String saveBitmap(Bitmap bmp) {
        FileOutputStream out;
        String bitmapName=System.currentTimeMillis() + "_fm.png";
        String  fileName=null;
        try { // 获取SDCard指定目录下
            String sdCardDir = Environment.getExternalStorageDirectory() + "/zupubao/";
            File dirFile = new File(sdCardDir);  //目录转化成文件夹
            if (!dirFile.exists()) {              //如果不存在，那就建立这个文件夹
                dirFile.mkdirs();
            }                          //文件夹有啦，就可以保存图片啦
            File file = new File(sdCardDir, bitmapName);// 在SDcard的目录下创建图片文,以当前时间为其命名
            out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);
//            System.out.println("_________保存到____sd______指定目录文件夹下____________________");
            Log.e("saveBitMap", "saveBitmap: 图片保存到" + Environment.getExternalStorageDirectory() + "/zupubao/" + bitmapName);
            fileName = Environment.getExternalStorageDirectory() + "/zupubao/" + bitmapName;
//            File file1 = new File(fileName);
//            boolean exists = file1.exists();
//            showShare(QQFilePath);
            out.flush();
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileName;
//        Toast.makeText(HahItemActivity.this,"保存已经至"+Environment.getExternalStorageDirectory()+"/CoolImage/"+"目录文件夹下", Toast.LENGTH_SHORT).show();
    }
}
