package com.my.toolslib;

import android.text.InputFilter;
import android.text.Spanned;

//保留小数点后两位过滤器
public class DecimalEditFilter implements InputFilter {
    private int num;

    public DecimalEditFilter(int num){
        this.num=num;
    }
    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        if (source.equals(".") && dest.toString().length() == 0) {
            return "0.";
        }
        if (dest.toString().contains(".")) {
            int index = dest.toString().indexOf(".");
            int length = dest.toString().substring(index).length();
            if (length == num+1) {
                return "";
            }
        }
        return null;
    }
}
