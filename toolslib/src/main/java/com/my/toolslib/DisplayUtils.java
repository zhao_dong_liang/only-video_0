package com.my.toolslib;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.View;

/**
 * Created by 86149 on 2018/12/4.
 */

public class DisplayUtils {

    public  int getDisplayWidth(Context context){
        Resources resources = context.getResources();
        DisplayMetrics outMetrics = resources.getDisplayMetrics();
        int width = outMetrics.widthPixels;
        return  width;
    }
    public int getViewHeigth(View view){
        int w = View.MeasureSpec.makeMeasureSpec(0,
                View.MeasureSpec.UNSPECIFIED);
        int h = View.MeasureSpec.makeMeasureSpec(0,
                View.MeasureSpec.UNSPECIFIED);
        view.measure(w, h);
        int height = view.getMeasuredHeight();
        return height;
    }
    public int getViewWidth(View view){
        int w = View.MeasureSpec.makeMeasureSpec(0,
                View.MeasureSpec.UNSPECIFIED);
        int h = View.MeasureSpec.makeMeasureSpec(0,
                View.MeasureSpec.UNSPECIFIED);
        view.measure(w, h);
        int width = view.getMeasuredWidth();
        return width;
    }
    public int getDisplayHeight(Context context){
        Resources resources = context.getResources();
        DisplayMetrics outMetrics = resources.getDisplayMetrics();
        int height= outMetrics.heightPixels;
        return  height;
    }
    public float getDisplayDensity(Context context){
        Resources resources = context.getResources();
        DisplayMetrics outMetrics = resources.getDisplayMetrics();
        float density = outMetrics.density;
        return  density;
    }
    /**
     * dp转换成px
     */
    public int dp2px(Context context,float dpValue){
        float scale=context.getResources().getDisplayMetrics().density;
        return (int)(dpValue*scale+0.5f);
    }
    /**
     * px转换成dp
     */
    public int px2dp(Context context,float pxValue){
        float scale=context.getResources().getDisplayMetrics().density;
        return (int)(pxValue/scale+0.5f);
    }
    /**
     * sp转换成px
     */
    public int sp2px(Context context,float spValue){
        float fontScale=context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (spValue*fontScale+0.5f);
    }
    /**
     * px转换成sp
     */
    public int px2sp(Context context,float pxValue){
        float fontScale=context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (pxValue/fontScale+0.5f);
    }
}
