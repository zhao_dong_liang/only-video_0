package com.my.toolslib;

import android.content.Context;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.util.Timer;
import java.util.TimerTask;

//弹出软键盘
public class PopKeyboardUtil {

  //等待弹出方法
    public static void waitPop(final EditText editText) {
        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.requestFocus(); //先将inputText取得焦点
        Timer timer = new Timer();//开启一个时间等待任务
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                InputMethodManager imm = (InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);//得到系统的输入方法服务
                imm.showSoftInput(editText, 0);
            }
        }, 300);
    }
    //关闭软键盘
    public static void hideSoftInputFromWindow(EditText editText) {
        InputMethodManager imm = (InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);//得到系统的输入方法服务
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }
}
