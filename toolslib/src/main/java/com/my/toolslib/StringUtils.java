package com.my.toolslib;

import android.annotation.SuppressLint;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 创建人:Jonlin
 * 创建时间:2016年7月14日 上午11:26:24
 * 类描述:字符串工具类
 */
@SuppressLint("DefaultLocale")
public class StringUtils {

    /**
     * 判断字符串为空
     */
    public static boolean isNull(String str) {
        return null == str || str.length() <= 0 || "null".equals(str) || "NULL".equals(str);
    }

    /**
     * 判断字符串不为空
     */
    public static boolean isNotNull(String str) {
        return !(null == str || str.length() <= 0 || "null".equals(str) || "NULL".equals(str));
    }
    public static boolean isNumeric(String str) {
        for (int i = str.length(); --i >= 0; ) {
            if (!Character.isDigit(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }
    /**
     * 字符串类型去空
     *
     * @param strObj
     * @return
     */
    public static String isCheckNullStringObj(Object strObj) {
        if (null == strObj || strObj.toString().length() <= 0 || strObj.toString().equals("null")
                || strObj.toString().equals("NULL")) {
            return "";
        }
        return strObj.toString();
    }

    /**
     * 判断字符串为空和替换为0
     */
    public static String replaceLine(String str) {
        String result = "0";
        try {
            if (null == str || str.length() <= 0 || str.toUpperCase() == "NULL" || str.toUpperCase().equals("NULL")) {
                return result;
            }
        } catch (Exception e) {
            return result;
        }

        return str;
    }

    /**
     * 将空字符串设默认值为"-"
     */
    public static String setNullOfDefaultMinus(String str) {
        if (null == str || str.length() <= 0 || "null".equals(str) || "NULL".equals(str)) {
            return "- -";
        } else {
            return str;
        }
    }

    /**
     * 手机号隐藏中间四位，显示为*
     */
    public static String showPhoneNum(String phoneNum) {
        return phoneNum.replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2");
    }

    /**
     * 替换字符串
     */
    public static String replaceString(String string, String oldString, String newString) {
        return string.replace(oldString, newString);
    }

    /**
     * 得到区的值
     */
    public static String getAreaData(String address) {
        String area = "";

        if (address.split("-").length == 3) {
            area = address.split("-")[2];
        }
        return area;
    }

    /**
     * 将显示内容类似空、0、NULL等装换为"--"
     */
    public static String formatShowInfo(Object content) {
        if (content == null || isNull(content.toString())) {
            return "--";
        } else if ("0".equals(content.toString()) || content.toString().equals("null") || content.toString().equals("NULL")) {
            return "--";
        } else {
            return content.toString();
        }
    }

    /**
     * 隐藏数字显示*
     *
     * @param formatString   需要显示*的字符串
     * @param starStartIndex *开始的位置
     * @param starEndIndex   *结束的位置
     * @return result 处理后的结果字符串
     */
    public static String formatShowStarString(String formatString, int starStartIndex, int starEndIndex) {
        String result = formatString;

        if (StringUtils.isNotNull(formatString) && formatString.length() > starStartIndex) {
            String starString = "";
            for (int i = 0; i < starEndIndex - starStartIndex; i++) {
                starString += "*";
            }
            result = formatString.substring(0, starStartIndex) + starString + formatString.substring(starEndIndex, formatString.length());
        }
        return result;
    }

    /**
     * 将对数字象转换成.00格式
     */
    public static String formatter2f(String numStr) {
        try {
            BigDecimal bigDecimal = new BigDecimal(numStr);
            BigDecimal bigDecimalHalfUp = bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP);
            return bigDecimalHalfUp.toString();
        } catch (NumberFormatException e) {
            return "0.00";
        }
    }

    /**
     * 将对数字象转换成.0格式
     */
    public static String formatter1f(String numStr) {
        try {
            BigDecimal bigDecimal = new BigDecimal(numStr);
            BigDecimal bigDecimalHalfUp = bigDecimal.setScale(1, BigDecimal.ROUND_HALF_UP);
            return bigDecimalHalfUp.toString();
        } catch (NumberFormatException e) {
            return "0.0";
        }
    }

    /**
     * 将对数字象转换成.格式,不要小数位
     */
    public static String formatter0f(String numStr) {
        try {
            BigDecimal bigDecimal = new BigDecimal(numStr);
            BigDecimal bigDecimalHalfUp = bigDecimal.setScale(0, BigDecimal.ROUND_HALF_UP);
            return bigDecimalHalfUp.toString();
        } catch (NumberFormatException e) {
            return "0";
        }
    }

    /**
     * 数组转list
     *
     * @param strs
     * @return
     */
    public static List<String> stringArrayToList(String[] strs) {
        List<String> list = new ArrayList<>();
        if (strs != null) {
            for (String str : strs) {
                list.add(str);
            }
        }
        return list;
    }

    /**
     * 数组转list(图片拼接专用)
     *
     * @param strs
     * @return
     */
    public static List<String> stringArrayToListForIMG(String[] strs, String ip) {
        List<String> list = new ArrayList<>();
        if (strs != null) {
            for (String str : strs) {
                list.add(ip + str);
            }
        }
        return list;
    }
    public static   boolean isPhone(String phone) {//验证手机号
        if (phone.isEmpty()) {
            return false;
        } else if (phone.length() != 11) {
            return false;
        }
        char c = phone.charAt(0);
        if (!phone.isEmpty()&&'1'!=c){
            return false;

        }
        return true;
    }


    /**
     *
     * list拼接成字符串
     * @param list
     * @separator separator 分隔符
     * @return
     */
    public  static String listToString(List<String> list,String separator){
        String str="";
        if (list!=null&&list.size()>0){
            for (int i = 0; i < list.size(); i++) {
                String s = list.get(i);
                if (i==0){
                    str=s;
                }else {
                    str=str+","+s;
                }

            }
        }
        return str;
    }

}