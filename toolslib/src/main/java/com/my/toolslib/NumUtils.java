package com.my.toolslib;


import com.my.toolslib.StringUtils;

import java.math.BigDecimal;

public class NumUtils {


    //数字格式化
    public static String formatBigNum(String num) {
        if (StringUtils.isNull(num)) {
            // 数据为空直接返回0
            return "0";
        }
        try {
            StringBuffer sb = new StringBuffer();
            if (!StringUtils.isNumeric(num)) {
                // 如果数据不是数字则直接返回0
                return "0";
            }
            BigDecimal b0 = new BigDecimal("1000");
            BigDecimal b1 = new BigDecimal("10000");
            BigDecimal b2 = new BigDecimal("100000000");
            BigDecimal b3 = new BigDecimal(num);
            String formatedNum = "";//输出结果
            String unit = "";//单位
             if (b3.compareTo(b0) == -1) {
                 sb.append(b3.toString());
             } else if ((b3.compareTo(b0) == 0 || b3.compareTo(b0) == 1)
                     || b3.compareTo(b1) == -1) {
                 formatedNum = b3.divide(b0).toString();
                 unit = "k";
             } else if ((b3.compareTo(b1) == 0 && b3.compareTo(b1) == 1)
                     || b3.compareTo(b2) == -1) {
                 formatedNum = b3.divide(b1).toString();
                 unit = "w";
             } else if (b3.compareTo(b2) == 0 || b3.compareTo(b2) == 1) {
                 formatedNum = b3.divide(b2).toString();
                 unit = "亿";
             }
             if (!"".equals(formatedNum)) {
                 int i = formatedNum.indexOf(".");
                 if (i == -1) {
                     sb.append(formatedNum).append(unit);
                 } else {
                     i = i + 1;
                     String v = formatedNum.substring(i, i + 1);
                     if (!v.equals("0")) {
                         sb.append(formatedNum.substring(0, i + 1)).append(unit);
                     } else {
                         sb.append(formatedNum.substring(0, i - 1)).append(unit);
                     }
                 }
             }
             if (sb.length() == 0)
                 return "0";
             return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return num;
        }
    }

    public static int parseInt(String num, int defaultValue) {
        try {
            return Integer.parseInt(num);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return defaultValue;
    }

    public static float parseFloat(String num, float defaultValue) {
        try {
            return Float.parseFloat(num);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return defaultValue;
    }
}


