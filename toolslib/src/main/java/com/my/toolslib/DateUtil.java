package com.my.toolslib;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

//1，日期格式：String dateString = "2017-06-20 10:30:30" 对应的格式：String pattern = "yyyy-MM-dd HH:mm:ss";
//
//        2，日期格式：String dateString = "2017-06-20" 对应的格式：String pattern = "yyyy-MM-dd";
//
//        3，日期格式：String dateString = "2017年06月20日 10时30分30秒 对应的格式：String pattern = "yyyy年MM月dd日 HH时mm分ss秒";
//
//        4，日期格式：String dateString = "2017年06月20日" 对应的格式：String pattern = "yyyy年MM月dd日";

public class DateUtil {


    /**
     * 获取系统时间戳
     *
     * @return
     */
    public static long getCurTimeLong() {
        long time = System.currentTimeMillis();
        return time;
    }

    /**
     * 获取当前时间
     *
     * @param pattern
     * @return
     */
    public static String getCurDate(String pattern) {
        SimpleDateFormat sDateFormat = new SimpleDateFormat(pattern);
        return sDateFormat.format(new Date());
    }

    /**
     * 时间戳转换成字符窜
     *
     * @param milSecond
     * @param pattern
     * @return
     */
    public static String getDateToString(long milSecond, String pattern) {
        Date date = new Date(milSecond);
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        return format.format(date);
    }

    //获取年份
    public static int getYear(){
        Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.YEAR);
        return day;
    }
    //获取小时
    public static int getHour(){
        Calendar cal = Calendar.getInstance();
        int hour = cal.get(Calendar.HOUR_OF_DAY);
//        hour=23;
        return hour;
    }
    //获取天
    public static int getDay(){
        Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DATE);
//        hour=23;
        return day;
    }
    //获取分钟
    public static int getMinute(){
        Calendar cal = Calendar.getInstance();
        int minute = cal.get(Calendar.MINUTE);
//        minute=41;
        return minute;
    }
    /**
     * 将字符串转为时间戳
     *
     * @param dateString
     * @param pattern
     * @return
     */
    public static long getStringToDate(String dateString, String pattern) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        Date date = new Date();
        try {
            date = dateFormat.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return date.getTime();
    }
    //时间戳转化为今天、明天还是后天
    public static String getDayString(long time){
        long oldDateTime = getOldDateTime(0);
        if (time>oldDateTime&&time<(oldDateTime+1000*60*60*24)){//今天
            return "今天";
        }
        if (time>(oldDateTime+1000*60*60*24)&&time<(oldDateTime+1000*60*60*24*2)){
            return "明天";
        }
        if (time>(oldDateTime+1000*60*60*24*2)&&time<(oldDateTime+1000*60*60*24*3)){
            return "后天";
        }
        return getDateToString(time,"MM月dd日");
    }
    /**
     * 获取前n天日期、后n天日期
     *
     * @param distanceDay 前几天 如获取前7天日期则传-7即可；如果后7天则传7
     * @return
     */
    public static String getOldDate(int distanceDay) {
//        SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dft = new SimpleDateFormat("MM月dd日");

        Date beginDate = new Date();
        Calendar date = Calendar.getInstance();
        date.setTime(beginDate);
        date.set(Calendar.DATE, date.get(Calendar.DATE) + distanceDay);
        Date endDate = null;
        try {
            endDate = dft.parse(dft.format(date.getTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
//        LogUtil.d("前7天==" + dft.format(endDate));
        return dft.format(endDate);
    }
    //获取几天后的  0 点时间戳
    public static long getOldDateTime(int distanceDay) {

//        Date beginDate = new Date();
        Calendar date = Calendar.getInstance();
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.MILLISECOND, 0);
//        date.setTime(beginDate);
        date.set(Calendar.DATE, date.get(Calendar.DATE) + distanceDay);

//        LogUtil.d("前7天==" + dft.format(endDate));
        return date.getTimeInMillis();
    }

    //获取几天后的 几点的时间戳
    public static long getOldDateHour(int day,int hour){
     return        getOldDateTime(day)+hour*1000*60*60;
    }

    //获取几天前或几天后星期几
    public static String getOldWeek(int distanceDay) {
//        SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dft = new SimpleDateFormat("yyyy年MM月dd日");
        Calendar cal = Calendar.getInstance();
        Date beginDate = new Date();
        Calendar date = Calendar.getInstance();
        date.setTime(beginDate);
        date.set(Calendar.DATE, date.get(Calendar.DATE) + distanceDay);
        Date endDate = null;
        try {
            endDate = dft.parse(dft.format(date.getTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date != null) {
            cal.setTime(new Date(endDate.getTime()));
        }
//        LogUtil.d("前7天==" + dft.format(endDate));
        int w = cal.get(Calendar.DAY_OF_WEEK);
        String week=null;
        switch (w){
            case 1:
                week="一";
                break;
            case 2:
                week="二";
                break;
            case 3:
                week="三";
                break;
            case 4:
                week="四";
                break;
            case 5:
                week="五";
                break;
            case 6:
                week="六";
                break;
            case 7:
                week="日";
                break;
        }
        return "周"+week;
    }
    public static int getDayofWeek(String dateTime) {
        Calendar cal = Calendar.getInstance();
        if (dateTime.equals("")) {
            cal.setTime(new Date(System.currentTimeMillis()));
        } else {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            Date date;
            try {
                date = sdf.parse(dateTime);
            } catch (ParseException e) {
                date = null;
                e.printStackTrace();
            }
            if (date != null) {
                cal.setTime(new Date(date.getTime()));
            }
        }
        return cal.get(Calendar.DAY_OF_WEEK);

    }


    /*
     * 毫秒转化时分秒毫秒
     */
    public static String formatTime(Long ms) {
        Integer ss = 1000;
        Integer mi = ss * 60;
        Integer hh = mi * 60;
        Integer dd = hh * 24;

        Long day = ms / dd;
        Long hour = (ms - day * dd) / hh;
        Long minute = (ms - day * dd - hour * hh) / mi;
        Long second = (ms - day * dd - hour * hh - minute * mi) / ss;
        Long milliSecond = ms - day * dd - hour * hh - minute * mi - second * ss;

        StringBuffer sb = new StringBuffer();
        if (day > 0) {
            sb.append(day + "天");
        }
        if (hour > 0) {
            sb.append(hour + "小时");
        }
//        if (minute > 0) {
//            sb.append(minute + "分");
//        }
//        if (second > 0) {
//            sb.append(second + "秒");
//        }
//        if (milliSecond > 0) {
//            sb.append(milliSecond + "毫秒");
//        }
        return sb.toString();

    }
//获取时段，比如明天上午，几月几日下午等
    public static   String  getTimeInterval(long startTime,long endTime){
        ArrayList<Long> startArr=new ArrayList<>();
        ArrayList<Long> endArr=new ArrayList<>();
        Map<String,String> map=new HashMap<>();
        String sTime="";
        String eTime="";
        for (int i=0;i<7;i++){
            String value="";
            if(i==0){
                value="今天";
            }else if(i==1){
                value="明天";
            }else if(i==2){
                value="后天";
            }else {
                value=getDateToString(startTime+(1000*10),"MM月dd日");
            }

           long dateTime= DateUtil.getOldDateTime(i);
           for(int j=1;j<5;j++){
               switch (j){
                   case 1:
                       value=value+"凌晨";
                       break;
                   case 2:
                       value=value+"上午";
                       dateTime=dateTime+8*60*60*1000;
                       break;

                   case 3:
                       value=value+"下午";
                       dateTime=dateTime+12*60*60*1000;
                       break;

                   case 4:
                       value=value+"夜晚";
                       dateTime=dateTime+18*60*60*1000;
                       break;
               }
               map.put(startArr.size()+"",value);
               if(i==0){
                   value="今天";
               }else if(i==1){
                   value="明天";
               }else if(i==2){
                   value="后天";
               }else {
                   value=getDateToString(startTime+(1000*10),"MM月dd日");
               }
               startArr.add(dateTime);
                dateTime= DateUtil.getOldDateTime(i);
           }
        }
        for (int i=0;i<7;i++) {
            long dateTime = DateUtil.getOldDateTime(i);
            for (int j = 1; j < 5; j++) {
                if (j == 1) {
                    dateTime = dateTime + 8 * 60 * 60 * 1000;
                } else if (j == 2) {
                    dateTime = dateTime + 12 * 60 * 60 * 1000;

                } else if (j == 3) {
                    dateTime = dateTime + 18 * 60 * 60 * 1000;
                } else {
                    dateTime = dateTime + 24 * 60 * 60 * 1000;
                }
                endArr.add(dateTime);
                 dateTime = DateUtil.getOldDateTime(i);
            }
        }

        for(int i=0;i<startArr.size();i++){
            if(startTime!=0&&endTime<=endArr.get(i)&&startTime>=startArr.get(i)){
                sTime=map.get(i+"");
                break;
            }
        }
        long s= Math.abs(startTime-endTime);
        long s1= 60*1000*60*24;
        if (s>=s1){
            String day="";
            try {
                int d=isTomorrowday(startTime+60*1000*60);
                if (d==0){//今天
                    day="今天";
                }else if (d==1){//明天
                    day="明天";
                }else if (d==2){//后天
                    day="后天";
                }else {
                   day= getDateToString(startTime+(1000*10),"MM月dd日");
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            sTime=day+"全天";
        }
        return  sTime;
    }

    /**

     * 判断是否为明天(效率比较高)
     * @param t 传入的 时间 戳
     * @return true明天 false不是
     * @throws ParseException
     */

    public static int isTomorrowday(long t) throws ParseException {
        int diffDay=0;
        Calendar pre = Calendar.getInstance();
        Date predate = new Date(System.currentTimeMillis());
        pre.setTime(predate);
        Calendar cal = Calendar.getInstance();
        Date date = new Date(t);
        cal.setTime(date);
        if (cal.get(Calendar.YEAR) == (pre.get(Calendar.YEAR))) {
             diffDay = cal.get(Calendar.DAY_OF_YEAR)
                    - pre.get(Calendar.DAY_OF_YEAR);
//            if (diffDay == 1) {
//                return true;
//            }
        }

//        return false;
        return  diffDay;
    }

    /** * 用于显示时间 */
    public static final String TODAY = "今天";
    public static final String YESTERDAY = "昨天";
    public static final String TOMORROW = "明天";
    public static final String BEFORE_YESTERDAY = "前天";
    public static final String AFTER_TOMORROW = "后天";
    // 将传入时间与当前时间进行对比，是否今天昨天
    public static String getDayTime(long time) {
        Calendar pre = Calendar.getInstance();
        Date predate = new Date(System.currentTimeMillis());
        pre.setTime(predate);

        Calendar cal = Calendar.getInstance();
        Date date = null;
        try{

            date = new Date(time);
        } catch (Exception e) {
            e.printStackTrace();
        }
        cal.setTime(date);

        if (cal.get(Calendar.YEAR) == (pre.get(Calendar.YEAR))) {
            int diffDay = cal.get(Calendar.DAY_OF_YEAR)
                    - pre.get(Calendar.DAY_OF_YEAR);

            return showDateDetail(diffDay, time);

        }
        return "";
    }
    /** * 将日期差显示为今天、明天或者星期 * @param diffDay * @param time * @return */
    private static String showDateDetail(int diffDay, long time){
        switch(diffDay){
            case 0:
                return TODAY+DateUtil.getDateToString(time,"HH:mm");
            case 1:
                return TOMORROW+DateUtil.getDateToString(time,"HH:mm");
            case 2:
                return AFTER_TOMORROW+DateUtil.getDateToString(time,"HH:mm");
//            case -1:
//                return YESTERDAY;
//            case -2:
//                return BEFORE_YESTERDAY;
            default:
                return  DateUtil.getDateToString(time,"MM-dd HH:mm");
        }
    }
    //秒转化为时间串
    public static String getTimeStrBySecond(long second){
        SimpleDateFormat dateformat=new SimpleDateFormat("mm:ss");
//        SimpleDateFormat dateformat=new SimpleDateFormat("HH:mm:ss");
        String a2=dateformat.format(new Date(second*1000));
        return a2;
    }


}