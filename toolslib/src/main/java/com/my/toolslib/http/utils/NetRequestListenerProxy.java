package com.my.toolslib.http.utils;

import android.content.Context;

import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.http.NAction;
import com.sunrun.sunrunframwork.http.NetRequestHandler;
import com.sunrun.sunrunframwork.http.NetServer;
import com.sunrun.sunrunframwork.http.cache.NetSession;
import com.sunrun.sunrunframwork.http.utils.UIUpdateHandler;
import com.sunrun.sunrunframwork.uiutils.UIUtils;

public class NetRequestListenerProxy implements NetRequestHandler, UIUpdateHandler {

    private NetRequestHandler mNServer;
    private Context context;

    public NetRequestListenerProxy(Context context) {
        this.context = context;
        initNetServer();
    }

    private void initNetServer() {
        if (this.mNServer == null) {
            this.mNServer = new NetServer(this.context, this);
        }
    }

    public void requestAsynGet(NAction action) {
        this.initNetServer();
        this.mNServer.requestAsynGet(action);
    }

    public void useCache(boolean useCache) {
        this.initNetServer();
        this.mNServer.useCache(useCache);
    }

    public void requestAsynPost(NAction action) {
        this.initNetServer();
        this.mNServer.requestAsynPost(action);
    }

    public void cancelRequest(int requestCode) {
        if (this.mNServer != null) {
            this.mNServer.cancelRequest(requestCode);
        }

    }

    public void cancelAllRequest() {
        if (this.mNServer != null) {
            this.mNServer.cancelAllRequest();
        }

    }

    @Override
    public void nofityUpdate(int i, BaseBean baseBean) {

    }

    @Override
    public void nofityUpdate(int i, float v) {

    }

    @Override
    public void dealData(int i, BaseBean baseBean) {

    }

    public void loadFaild(int requestCode, BaseBean bean) {
        UIUtils.shortM(bean);
    }

    @Override
    public void emptyData(int i, BaseBean baseBean) {

    }

    @Override
    public void requestFinish() {
        UIUtils.cancelLoadDialog();
    }

    @Override
    public void requestCancel() {
        UIUtils.cancelLoadDialog();
    }

    @Override
    public void requestStart() {

    }

    @Override
    public NetSession getSession() {
        return NetSession.instance(this.context);
    }
}
