package com.my.toolslib.http.utils;

import android.view.View;

import com.lzy.okgo.model.Progress;
import com.sunrun.sunrunframwork.uiutils.UIUtils;

import java.io.File;

public class OkHttpRequestListener implements OkHttpRequest {

    @Override
    public void tokenInvalid() {

    }

    @Override
    public void httpLoadFail(String err) {
        UIUtils.shortM(err);
    }

    @Override
    public void httpLoadFinal() {

    }

    @Override
    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {

    }

    @Override
    public void onProgress(Progress progress) {

    }

    @Override
    public void downLoadFinish(File file) {

    }
}