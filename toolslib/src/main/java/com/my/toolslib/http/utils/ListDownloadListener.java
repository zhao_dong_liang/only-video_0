package com.my.toolslib.http.utils;

import android.content.Context;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.lzy.okgo.model.Progress;
import com.lzy.okserver.download.DownloadListener;

import java.io.File;

public class ListDownloadListener extends DownloadListener {

    private RecyclerView.ViewHolder holder;
    private Object tag;

    private Context context;
    ListDownloadListener(Object tag, RecyclerView.ViewHolder holder,Context context) {
        super(tag);
        this.holder = holder;
        this.context=context;
    }
    ListDownloadListener(Object tag,Context context) {
        super(tag);
        this.context=context;
    }

    @Override
    public void onStart(Progress progress) {

    }

    @Override
    public void onProgress(Progress progress) {
//        if (tag == holder.getTag()) {
//            holder.refresh(progress);
//        }
    }

    @Override
    public void onError(Progress progress) {
        Throwable throwable = progress.exception;
        if (throwable != null) throwable.printStackTrace();
    }

    @Override
    public void onFinish(File file, Progress progress) {
        Toast.makeText(context, "下载完成:" + progress.filePath, Toast.LENGTH_SHORT).show();
//        updateData(type);
    }

    @Override
    public void onRemove(Progress progress) {
    }


}