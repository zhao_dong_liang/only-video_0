package com.my.toolslib.http.utils;

import android.view.View;

import com.lzy.okgo.model.Progress;

import java.io.File;

public interface OkHttpRequest {

    //token失效
    void tokenInvalid();
    //请求失败
    void httpLoadFail(String err);
    //请求完成
    void httpLoadFinal();
    //更新ui
    void nofityUpdateUi(int requestCode, LzyResponse response, View view);

    //下载进度监听
    void onProgress(Progress progress);

    //下载完成
    void downLoadFinish(File file);
}
