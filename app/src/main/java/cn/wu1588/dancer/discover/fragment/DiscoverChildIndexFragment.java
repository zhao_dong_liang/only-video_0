package cn.wu1588.dancer.discover.fragment;

import android.app.Activity;
import android.content.ClipboardManager;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.lzy.okgo.model.Progress;
import com.maning.mndialoglibrary.MProgressBarDialog;
import com.my.toolslib.NumUtils;
import com.my.toolslib.http.utils.NetRequestListenerProxy;
import com.my.toolslib.http.utils.OkHttpRequest;
import com.shuyu.gsyvideoplayer.GSYVideoManager;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uibase.BaseActivity;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;
import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.sunrun.sunrunframwork.utils.EmptyDeal;
import com.tencent.mmkv.MMKV;

import java.io.File;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.model.SystemParams;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.common.util.DeviceIdUtil;
import cn.wu1588.dancer.common.util.PageLimitDelegate;
import cn.wu1588.dancer.adp.DiscoverAdapter;
import cn.wu1588.dancer.home.activity.MoviePlayerV3Activity;
import cn.wu1588.dancer.home.fragment.SmallVideoCommentFragment;
import cn.wu1588.dancer.home.mode.MovieBean;
import cn.wu1588.dancer.share.ShareBottomSheetDialog;

import static android.content.Context.CLIPBOARD_SERVICE;
import static androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_DRAGGING;
import static androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_IDLE;
import static androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_SETTLING;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_COLLECT_MOVIE_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_DISCOVER_LIST_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_DISCOVER_REC_CODE;

/**
 * 推荐
 */
public class DiscoverChildIndexFragment extends LBaseFragment implements OkHttpRequest {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    public String refresh;
    private DiscoverAdapter discoverAdapter;
    private int page = 1;
    private int tag = 0;
    private KindCallback callback;

    PageLimitDelegate pageLimitDelegate = new PageLimitDelegate(new PageLimitDelegate.DataProvider() {
        @Override
        public void loadData(int page) {
            String deviceId = DeviceIdUtil.getDeviceId(getActivity());
            BaseQuestStart.getDiscoverRecData(deviceId,page,DiscoverChildIndexFragment.this);
        }
    });
    private ClipboardManager myClipboard;

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_discover_child;
    }

    public static DiscoverChildIndexFragment newInstance(int tag,KindCallback callback) {
        DiscoverChildIndexFragment discoverFragment = new DiscoverChildIndexFragment();
        Bundle bundle = new Bundle();
        discoverFragment.setArguments(bundle);
        discoverFragment.callback = callback;
        discoverFragment.tag = tag;
        return discoverFragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }

    private void initViews() {
        MMKV mmkv = MMKV.defaultMMKV();
        int autoPlay = NumUtils.parseInt(mmkv.decodeString(SystemParams.AUTO_PLAY), 0);
        myClipboard = (ClipboardManager) that.getSystemService(CLIPBOARD_SERVICE);
        refreshLayout.setColorSchemeColors(getResources().getColor(R.color.main_button_color));
        discoverAdapter = new DiscoverAdapter(new DiscoverAdapter.PlayCallback() {
            @Override
            public void onClick(int positon) {
                int autoPlay = Integer.parseInt(Config.getData("autoplay","0"));
                if (autoPlay == 0) {
                    MultiItemEntity item = discoverAdapter.getItem(positon);
                    if (item instanceof MovieBean) {
                        GSYVideoManager.releaseAllVideos();
                        MoviePlayerV3Activity.startTActivity((Activity) getContext(), String.valueOf(((MovieBean) item).id),recyclerView);
                    }

                }
            }
        },true);
        discoverAdapter.setTag(this.tag);
        recyclerView.setAdapter(discoverAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(that);
        recyclerView.setLayoutManager(linearLayoutManager);
        pageLimitDelegate.attach(refreshLayout, recyclerView, discoverAdapter);
        discoverAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                MultiItemEntity entity = discoverAdapter.getItem(position);
                if (entity instanceof MovieBean) {
                    MovieBean item = (MovieBean) entity;
                    ImageView ivShare = (ImageView) discoverAdapter.getViewByPosition(position, R.id.ivShare);
                    switch (view.getId()) {
                        case R.id.ivCollect:
                            BaseQuestStart.movieZan(new NetRequestListenerProxy(getActivity()) {
                                @Override
                                public void nofityUpdate(int i, BaseBean bean) {
                                    if (bean.status == 1) {
                                        if (TextUtils.equals("1", bean.Data())) {
                                            pageLimitDelegate.refreshPage();
                                        } else {
                                            UIUtils.shortM(bean.msg);
                                        }
                                    }else {
                                        UIUtils.shortM(bean.msg);
                                    }

                                }
                            }, 1, String.valueOf(item.id));
                            break;
                        case R.id.ivShare:
                            if (!EmptyDeal.isEmpy(item.play_url)) {
                                ShareBottomSheetDialog.newInstance(item).show(getChildFragmentManager(), ShareBottomSheetDialog.TAG);
                            }
                            break;
                        case R.id.llContainer:
                            GSYVideoManager.releaseAllVideos();
                            MoviePlayerV3Activity.startTActivity((Activity)getContext(),String.valueOf(item.id), view);
                            break;
                        case R.id.kind:
                            callback.onClick(((MovieBean) item).home_category);
                            break;
                        case R.id.down_view:
                            if (!item.isFree() && !TextUtils.isEmpty(item.download_url)) {
                                if (!Config.getLoginInfo().isMember()) {
                                    initDownloadDialog();
                                    mProgressBarDialog.showProgress(0,"已下载0%");
                                    BaseQuestStart.updateMovieDownloadNum(DiscoverChildIndexFragment.this,item.id+"");
                                    BaseQuestStart.downLoadVideo(DiscoverChildIndexFragment.this,item.download_url);
                                }
                            }
                            break;
                        case R.id.ivComment://评论列表
                            SmallVideoCommentFragment commentFragment=new SmallVideoCommentFragment();
                            commentFragment.setMovieBean(item, true);
                            commentFragment.show(getChildFragmentManager(),position+"");
                            break;
                        case R.id.header_img:
                        case R.id.name_tv:
                            CommonIntent.startOtherUserActiivty((BaseActivity) getActivity(), item.user_id);
                    }
                }

            }
        });
        if (autoPlay == 1) {
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

                int firstVisibleItem, lastVisibleItem, visibleCount;
                boolean scrollState = false;

                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    switch (newState) {
                        case SCROLL_STATE_IDLE: //滚动停止
                            scrollState = false;
                            break;
                        case SCROLL_STATE_DRAGGING: //手指拖动
                            scrollState = true;
                            break;
                        case SCROLL_STATE_SETTLING: //惯性滚动
                            scrollState = true;
                            break;
                    }
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    visibleCount = lastVisibleItem - firstVisibleItem;
                    //大于0说明有播放
                    if (GSYVideoManager.instance().getPlayPosition() >= 0) {
                        //当前播放的位置
                        int position = GSYVideoManager.instance().getPlayPosition();
                        //对应的播放列表TAG
                        if (GSYVideoManager.instance().getPlayTag().equals(DiscoverAdapter.TAG)
                                && (position < firstVisibleItem || position > lastVisibleItem)) {
                            GSYVideoManager.onPause();
                            //如果滑出去了上面和下面就是否，和今日头条一样
                            //是否全屏
                            if(!GSYVideoManager.isFullState(that)) {
                                GSYVideoManager.releaseAllVideos();
                                discoverAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                }
            });
        }


    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        Log.d("zdl","==========="+bean.toString());
        switch (requestCode) {
            case GET_DISCOVER_LIST_CODE:
                if (bean.status == 1) {
                    List<MovieBean> movieBeanList = bean.Data();
                    pageLimitDelegate.setData(movieBeanList);
                }
                break;
            case GET_COLLECT_MOVIE_CODE:
                if (bean.status == 1) {
                    if (TextUtils.equals("1", bean.Data())) {
                        pageLimitDelegate.refreshPage();
                    } else {
                        UIUtils.shortM(bean.msg);
                    }
                } else {
                    UIUtils.shortM(bean.Data().toString());
                }
                break;
            case GET_DISCOVER_REC_CODE:
                if (bean.status == 1) {
                    page ++;
                    List<MovieBean> movieBeanList = bean.Data();
                    pageLimitDelegate.setData(movieBeanList);
                }
                break;
        }
        super.nofityUpdate(requestCode, bean);
    }

    public boolean onBackPressed() {
        if (GSYVideoManager.backFromWindowFull(getActivity())) {
            return true;
        }
        return false;
    }

    @Override
    public void onPause() {
        super.onPause();
        GSYVideoManager.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        GSYVideoManager.onResume();
    }

    @Override
    public void onDestroy() {
        GSYVideoManager.releaseAllVideos();
        super.onDestroy();
    }

    public interface KindCallback{
        void onClick(String str);
    }


    private MProgressBarDialog mProgressBarDialog;
    private void initDownloadDialog(){
        //新建一个Dialog
        mProgressBarDialog = new MProgressBarDialog.Builder(getActivity())
                //全屏模式
                .isWindowFullscreen(true)
                .setStyle(MProgressBarDialog.MProgressBarDialogStyle_Circle)
                //全屏背景窗体的颜色
                .setBackgroundWindowColor(Color.TRANSPARENT)
                //View背景的颜色
                .setBackgroundViewColor(Color.BLACK)
                //字体的颜色
                .setTextColor(Color.WHITE)
                //View边框的颜色
                .setStrokeColor(Color.TRANSPARENT)
                //View边框的宽度
                .setStrokeWidth(2)
                //View圆角大小
                .setCornerRadius(10)
                //ProgressBar背景色
                .setProgressbarBackgroundColor(Color.BLACK)
                //ProgressBar 颜色
                .setProgressColor(Color.WHITE)
                //圆形内圈的宽度
                .setCircleProgressBarWidth(4)
                //圆形外圈的宽度
                .setCircleProgressBarBackgroundWidth(4)
                //水平进度条Progress圆角
                .setProgressCornerRadius(0)
                //水平进度条的高度
                .setHorizontalProgressBarHeight(10)
                //dialog动画
//                .setAnimationID(R.style.animate_dialog_custom)
                .build();
    }

    @Override
    public void downLoadFinish(File file) {
        super.downLoadFinish(file);
        ToastUtils.longToast("下载完成");
    }
    @Override
    public void httpLoadFail(String err) {
        super.httpLoadFail(err);
        ToastUtils.longToast(err);
    }
    @Override
    public void httpLoadFinal() {
        super.httpLoadFinal();
        if (mProgressBarDialog!=null){
            mProgressBarDialog.dismiss();
        }
    }
    @Override
    public void onProgress(Progress progress) {
        super.onProgress(progress);
        if (mProgressBarDialog!=null){
            int fraction = (int) (progress.fraction*100);
            mProgressBarDialog.showProgress(fraction,"已下载"+fraction+"%", true);
        }
    }
}
