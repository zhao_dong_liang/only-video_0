package cn.wu1588.dancer.discover.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.flyco.tablayout.SlidingTabLayout;
import com.shuyu.gsyvideoplayer.GSYVideoManager;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uibase.BaseActivity;
import com.sunrun.sunrunframwork.uiutils.UIUtils;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.base.ViewPagerFragmentAdapter;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.discover.FlowLayout;
import cn.wu1588.dancer.model.DiscoverMenu;
import cn.wu1588.dancer.home.activity.SearchActivity;

import static androidx.viewpager.widget.ViewPager.SCROLL_STATE_DRAGGING;
import static androidx.viewpager.widget.ViewPager.SCROLL_STATE_IDLE;
import static androidx.viewpager.widget.ViewPager.SCROLL_STATE_SETTLING;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_DISCOVER_MENU_LIST_CODE;

/**
 * 作者：ninetailedfox
 * 时间：2021/1/15 10:45 AM
 * 邮箱: 1037438704@qq.com
 * 功能：首页  和  教学页面
 **/
public class DiscoverFragment extends LBaseFragment {
    private ViewPagerFragmentAdapter adapter;
    private String[] titles;
    private TextView text_search;
    private List<DiscoverMenu> list;
    private List<LBaseFragment> fragments;
    private ViewPager vp;
    private SlidingTabLayout tabLayout;
    private ImageView img;
    private ImageView ib_upload;
    private FlowLayout flowLayout;
    private List<TextView> textViews = new ArrayList<>();
    private List<String> titleLists = new ArrayList<>();
    private View menu_layout;

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_discover;
    }

    public static DiscoverFragment newInstance() {
        DiscoverFragment discoverFragment = new DiscoverFragment();
        Bundle bundle = new Bundle();
        discoverFragment.setArguments(bundle);
        return discoverFragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        initEvent();
    }

    private void initViews(View v) {
        flowLayout = v.findViewById(R.id.flow_layout);
        menu_layout = v.findViewById(R.id.menu_layout);
        ib_upload = v.findViewById(R.id.ib_upload);
        menu_layout.setVisibility(View.VISIBLE);
        vp = v.findViewById(R.id.vp);
        img = v.findViewById(R.id.img_tabbar_menu);
        text_search = v.findViewById(R.id.text_search);
        tabLayout = v.findViewById(R.id.sliding_tab_layout);
    }

    private void initEvent() {
        text_search.setOnClickListener(v -> startActivity(new Intent(getActivity(), SearchActivity.class)));
        img.setOnClickListener(view12 -> {
            if (flowLayout.getVisibility() == View.VISIBLE) {
                flowLayout.setVisibility(View.GONE);
            } else {
                flowLayout.setVisibility(View.VISIBLE);
            }
        });
        ib_upload.setOnClickListener(target -> CommonIntent.startUpVideosActivity((BaseActivity) getActivity(), false));

        adapter = new ViewPagerFragmentAdapter(DiscoverFragment.this.getActivity().getSupportFragmentManager());
        vp.setAdapter(adapter);
        vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                switch (state) {
                    case SCROLL_STATE_IDLE:
                        break;
                    case SCROLL_STATE_DRAGGING:
                        break;
                    case SCROLL_STATE_SETTLING:
                }
            }
        });
        BaseQuestStart.getDiscoverMenuList(DiscoverFragment.this);
    }

    private void initChildViews() {
        if (getActivity() == null) {
            return;
        }
        ViewGroup.MarginLayoutParams lp = new ViewGroup.MarginLayoutParams(
                UIUtils.dip2px(getActivity(), 70), ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.leftMargin = UIUtils.dip2px(getActivity(), 0);
        lp.rightMargin = UIUtils.dip2px(getActivity(), 10);
        lp.topMargin = UIUtils.dip2px(getActivity(), 0);
        lp.bottomMargin = UIUtils.dip2px(getActivity(), 10);
        int l = UIUtils.dip2px(getActivity(), 3);
        int t = UIUtils.dip2px(getActivity(), 3);
        int r = UIUtils.dip2px(getActivity(), 3);
        int b = UIUtils.dip2px(getActivity(), 3);
        for (int i = 0; i < titles.length; i++) {
            TextView view = new TextView(DiscoverFragment.this.getActivity());
            view.setMaxLines(1);
            view.setEllipsize(TextUtils.TruncateAt.END);
            view.setText(titles[i]);
            titleLists.add(titles[i]);
            view.setTextColor(getResources().getColor(R.color.black_color));
            view.setGravity(Gravity.CENTER);
            view.setPadding(l, t, r, b);
            view.setBackgroundDrawable(getResources().getDrawable(R.drawable.shape_round_gray));
            view.setOnClickListener(view1 -> {
                int index = textViews.indexOf(view1);
                flowLayout.setVisibility(View.GONE);
                vp.setCurrentItem(index);
            });
            textViews.add(view);
            flowLayout.addView(view, lp);
        }
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode) {
            case GET_DISCOVER_MENU_LIST_CODE:
                list = bean.Data();
                Log.d("zdl","==========="+bean.Data().toString());
                if (list != null && !list.isEmpty()) {
                    titles = new String[list.size() + 1];
                    titles[0] = "推荐";
                    fragments = new ArrayList<>();
                    DiscoverChildIndexFragment.KindCallback callback = str -> {
                        int index = titleLists.indexOf(str);
                        vp.setCurrentItem(index);
                    };
                    fragments.add(DiscoverChildIndexFragment.newInstance(0, callback));
                    for (int i = 0; i < list.size(); i++) {
                        titles[i + 1] = list.get(i).title;
                        fragments.add(DiscoverChildFragment.newInstance(0, list.get(i).id, callback));
                    }
                    adapter.setFragments(fragments);
                    adapter.notifyDataSetChanged();
                    tabLayout.setViewPager(vp, titles);
                    initChildViews();
                }
                break;
        }
        super.nofityUpdate(requestCode, bean);
    }

    public boolean onBackPressed() {
        if (GSYVideoManager.backFromWindowFull(getActivity())) {
            return true;
        }
        return false;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            GSYVideoManager.onResume();
        } else {
            GSYVideoManager.onPause();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        GSYVideoManager.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        GSYVideoManager.onResume();
    }

    @Override
    public void onDestroy() {
        GSYVideoManager.releaseAllVideos();
        super.onDestroy();
    }
}
