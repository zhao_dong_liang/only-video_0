package cn.wu1588.dancer.adp;

import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import cn.wu1588.dancer.channel.mode.ThemeaticBean;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;

public class StarAdapter extends BaseQuickAdapter<ThemeaticBean.GirllistBean, BaseViewHolder> {
    public StarAdapter() {
        super(R.layout.item_star_layout);
    }

    @Override
    protected void convert(BaseViewHolder helper, ThemeaticBean.GirllistBean item) {
        helper.setText(R.id.tvTitle, item.name)
                .setText(R.id.tvContent, item.content)
                .setText(R.id.tvNum, mContext.getString(R.string.video_num, item.num));
        GlideMediaLoader.loadHead(mContext, helper.getView(R.id.rIvStar), item.icon);

        RecyclerView recyclerView = (RecyclerView) helper.getView(R.id.recyclerViewStar);
        StarHorizontalAdapter starHorizontalAdapter = new StarHorizontalAdapter();
        starHorizontalAdapter.setNewData(item.info);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(starHorizontalAdapter);
        starHorizontalAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                ThemeaticBean.GirllistBean.InfoBean item1 = starHorizontalAdapter.getItem(position);
                CommonIntent.startMoviePlayerActivity(mContext, item1.id);
            }
        });
    }
}
