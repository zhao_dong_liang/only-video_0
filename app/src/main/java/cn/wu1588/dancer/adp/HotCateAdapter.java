package cn.wu1588.dancer.adp;

import android.content.Context;
import android.view.View;

import androidx.annotation.Nullable;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import cn.wu1588.dancer.channel.mode.Recommend;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.adp.MovieAdapter;
import cn.wu1588.dancer.home.mode.MovieBean;

/**
 * Created by caidong on 2019/8/26.
 * email:mircaidong@163.com
 * describe: 描述
 */
public class HotCateAdapter  extends BaseQuickAdapter<Recommend, BaseViewHolder> {
    private Context context;
    public HotCateAdapter(@Nullable List<Recommend> data,Context context) {
        super(R.layout.layout_hot_cate, data);
        this.context = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, Recommend item) {
        helper.setText(R.id.hot_cate_name,item.title);
        helper.setText(R.id.hot_cate_desc,item.desc);
        helper.addOnClickListener(R.id.hot_cate_desc);
        RecyclerView recyclerView = helper.getView(R.id.hot_cate_rv);
        MovieAdapter adapters = new MovieAdapter(item.data);
        recyclerView.setLayoutManager(new GridLayoutManager(context,2));
        recyclerView.setAdapter(adapters);
        adapters.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                MovieBean item = adapters.getItem(position);
                CommonIntent.startMoviePlayerActivity(context,String.valueOf(item.id));
            }
        });

    }
}
