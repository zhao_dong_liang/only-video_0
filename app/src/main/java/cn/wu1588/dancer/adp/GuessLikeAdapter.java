package cn.wu1588.dancer.adp;

import android.view.View;
import android.widget.TextView;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.home.mode.MovieDetailBean;

public class GuessLikeAdapter extends BaseQuickAdapter<MovieDetailBean.YoulikeBean, BaseViewHolder> {

    private TagFlowLayout tagFlowLayout;

    public GuessLikeAdapter() {
        super(R.layout.item_guess_like_layout);
    }

    @Override
    protected void convert(BaseViewHolder helper, MovieDetailBean.YoulikeBean item) {
        helper.addOnClickListener(R.id.llContainer);
        GlideMediaLoader.load(mContext, helper.getView(R.id.ivImg), item.image, R.drawable.ic_history_place);
        helper.setText(R.id.tvTitle, item.title)
                .setText(R.id.tvPlayNum, mContext.getString(R.string.play_num, item.dealPlayNum()))
        .setText(R.id.tvGrade,item.grade);
        tagFlowLayout = (TagFlowLayout) helper.getView(R.id.tagFlwLayout);
        tagFlowLayout.setAdapter(new TagAdapter<String>(item.label) {
            @Override
            public View getView(FlowLayout parent, int position, String o) {
                TextView tv = (TextView) mLayoutInflater.inflate(R.layout.item_simple_type_text, tagFlowLayout, false);
                tv.setText(o);
                return tv;
            }
        });
        tagFlowLayout.setOnTagClickListener(new TagFlowLayout.OnTagClickListener() {
            @Override
            public boolean onTagClick(View view, int position, FlowLayout parent) {
                String lableText = item.label.get(position);
                CommonIntent.startLableScreenActivity(mContext,lableText);
                return false;
            }
        });
    }
}
