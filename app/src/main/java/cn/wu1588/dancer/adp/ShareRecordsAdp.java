package cn.wu1588.dancer.adp;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.Map;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.util.DataUtils;

/**
 * 作者：ninetailedfox
 * 时间：2021/1/17 11:34 PM
 * 邮箱: 1037438704@qq.com
 * 功能：分享记录适配器
 **/
public class ShareRecordsAdp extends BaseQuickAdapter<Map<String, String>, BaseViewHolder> {

    public ShareRecordsAdp(int item_share_records) {
        super(item_share_records);
    }

    @Override
    protected void convert(BaseViewHolder helper, Map<String, String> item) {
        GlideMediaLoader.load(mContext, helper.getView(R.id.image_share), item.get("image_url"), R.drawable.ic_history_place);
        helper.setText(R.id.text_title, item.get("title"))
                .setText(R.id.text_time, DataUtils.getStringDate(item.get("forward_time")))
                .addOnClickListener(R.id.buttom_share);
    }
}
