package cn.wu1588.dancer.adp;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import cn.wu1588.dancer.channel.mode.LableBean;

public class LableHorizontalAdapter extends BaseQuickAdapter<LableBean.SelectOneBean, BaseViewHolder> {

    public LableHorizontalAdapter() {
        super(R.layout.item_lable_horizontal);
    }

    @Override
    protected void convert(BaseViewHolder helper, LableBean.SelectOneBean item) {
        helper.addOnClickListener(R.id.tvlableText);
        helper.setText(R.id.tvlableText, item.name);
        helper.getView(R.id.tvlableText).setSelected(item.isSelect);
    }

}
