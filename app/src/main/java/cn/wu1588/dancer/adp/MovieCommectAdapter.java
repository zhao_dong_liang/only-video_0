package cn.wu1588.dancer.adp;

import android.view.View;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.chad.library.adapter.base.BaseSectionQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.home.mode.MovieCommentBean;
import cn.wu1588.dancer.home.mode.MovieCommentSectionEntity;

public class MovieCommectAdapter extends BaseSectionQuickAdapter<MovieCommentSectionEntity, BaseViewHolder> {
    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param data             A new list is created out of this one to avoid mutable list
     */
    public MovieCommectAdapter(List<MovieCommentSectionEntity> data) {
        super(R.layout.commect_item, R.layout.commect_item,data);
    }



    @Override
    protected void convertHead(BaseViewHolder helper, MovieCommentSectionEntity item) {
        MovieCommentBean bean = item.t;
       String content= bean.getContent();
      String  nickname=bean.getNickname();
      int  like_num=bean.getLike_num();
       String add_time=bean.getAdd_time();
      String  icon=bean.getIcon();
        TextView more_tv=helper.getView(R.id.more_tv);
        ConstraintLayout.LayoutParams cl= (ConstraintLayout.LayoutParams) helper.getView(R.id.header_img).getLayoutParams();
        cl.leftMargin=0;
        more_tv.setVisibility(View.GONE);
       int is_like=bean.getIs_like();

        helper.setText(R.id.content_tv,content)
                .setText(R.id.name_tv,nickname)
                .setText(R.id.like_num,like_num+"")
                .setText(R.id.time_tv,add_time)
        ;
        View like_view=helper.getView(R.id.like_view);
        if (is_like==0){
            like_view.setBackgroundResource(R.drawable.commect_fabulous_icon);
        }else {
            like_view.setBackgroundResource(R.drawable.home_fabulous_yes);
        }
        GlideMediaLoader.loadHead(mContext,helper.getView(R.id.header_img),icon);

        helper.addOnClickListener(R.id.like_linear,R.id.more_tv,R.id.header_img,R.id.layout);
    }

    @Override
    protected void convert(BaseViewHolder helper, MovieCommentSectionEntity item) {
        MovieCommentBean bean = item.t;
        TextView more_tv=helper.getView(R.id.more_tv);
        ConstraintLayout.LayoutParams cl= (ConstraintLayout.LayoutParams) helper.getView(R.id.header_img).getLayoutParams();
      String  content="回复"+bean.getReply_user()+"："+bean.getContent();
     String   nickname=bean.getNickname();
      int  like_num=bean.getLike_num();
     String   add_time=bean.getAdd_time();
    String    icon=bean.getIcon();
     int   is_like=bean.getIs_like();
        cl.leftMargin= (int) mContext.getResources().getDimension(R.dimen.dp_38);


        if ( bean.getDetails_next_page()>0){
            more_tv.setVisibility(View.VISIBLE);
        }else {
            more_tv.setVisibility(View.GONE);
        }


        helper.setText(R.id.content_tv,content)
                .setText(R.id.name_tv,nickname)
                .setText(R.id.like_num,like_num+"")
                .setText(R.id.time_tv,add_time)
        ;
        View like_view=helper.getView(R.id.like_view);
        if (is_like==0){
            like_view.setBackgroundResource(R.drawable.commect_fabulous_icon);
        }else {
            like_view.setBackgroundResource(R.drawable.home_fabulous_yes);
        }
        GlideMediaLoader.loadHead(mContext,helper.getView(R.id.header_img),icon);

        helper.addOnClickListener(R.id.like_linear,R.id.more_tv,R.id.header_img,R.id.layout);
    }
}
