package cn.wu1588.dancer.adp;

import android.view.View;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

import cn.wu1588.dancer.channel.mode.LableScreenBean;

public class SecondAryLabelAdapter extends BaseQuickAdapter<LableScreenBean.SecondBean, BaseViewHolder> {

    private List<LableScreenBean.SecondBean> selectList=new ArrayList<>();

    public List<LableScreenBean.SecondBean> getSelectList() {
        return selectList;
    }
    public void setSelectList(List<LableScreenBean.SecondBean> selectList) {
        this.selectList = selectList;
    }
    public SecondAryLabelAdapter() {
        super(R.layout.item_secondary_lable);
        setOnItemChildClickListener(new OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                LableScreenBean.SecondBean item = getItem(position);
                if (view.getId()==R.id.layBg){
                    if (selectList.contains(item)) {
                        selectList.remove(item);
                    } else {
                        selectList.add(item);
                    }
                }
            }
        });
    }

    @Override
    protected void convert(BaseViewHolder helper, LableScreenBean.SecondBean item) {
        helper.addOnClickListener(R.id.layBg);
        helper.setText(R.id.checkbox,item.title);
//        helper.setBackgroundRes(R.id.layBg,selectList.contains(item)? R.drawable.shape_proble_bg3 : R.drawable.shape_proble_bg);
//        helper.setChecked(R.id.checkbox,selectList.contains(item));
        if (selectList.contains(item)) {
            helper.setBackgroundRes(R.id.layBg, R.drawable.shape_proble_bg3);
            helper.setChecked(R.id.checkbox, true);
        } else {
            helper.setBackgroundRes(R.id.layBg, R.drawable.shape_proble_bg);
            helper.setChecked(R.id.checkbox, false);
        }
    }
}
