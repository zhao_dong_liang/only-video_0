package cn.wu1588.dancer.adp;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import cn.wu1588.dancer.channel.mode.LableScreenBean;

public class LableScreenAdapter extends BaseQuickAdapter<LableScreenBean, BaseViewHolder> {

    public LableScreenAdapter() {
        super(R.layout.item_lable_screen_horizontal);
    }

    @Override
    protected void convert(BaseViewHolder helper,LableScreenBean item) {
        helper.addOnClickListener(R.id.tvlableText);
        helper.setText(R.id.tvlableText, item.title);
        helper.getView(R.id.tvlableText).setSelected(item.isSelect);
    }
}
