package cn.wu1588.dancer.adp;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.shuyu.gsyvideoplayer.utils.GSYVideoType;
import com.shuyu.gsyvideoplayer.video.StandardGSYVideoPlayer;
import com.tencent.mmkv.MMKV;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.model.SystemParams;
import cn.wu1588.dancer.home.mode.MovieBean;

public class SmallVideoPlayerAdapter extends BaseQuickAdapter<MovieBean, BaseViewHolder> {

    private String showDownload;


    public SmallVideoPlayerAdapter() {
        super(R.layout.small_video_player_fragment);
        MMKV mmkv = MMKV.defaultMMKV();
        showDownload = mmkv.decodeString(SystemParams.IS_SHOW_DOWBTN);
    }

    @Override
    protected void convert(BaseViewHolder helper, MovieBean item) {
        //全屏裁减显示，为了显示正常 CoverImageView 建议使用FrameLayout作为父布局
        GSYVideoType.setShowType(GSYVideoType.SCREEN_TYPE_FULL);
        //全屏拉伸显示，使用这个属性时，surface_container建议使用FrameLayout
//        GSYVideoType.setShowType(GSYVideoType.SCREEN_MATCH_FULL);
        StandardGSYVideoPlayer player = helper.getView(R.id.moviePlayer);
        player.getBottomContainer().getLayoutParams().height = 1;
        player.getFullscreenButton().setVisibility(View.GONE);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) player.getBackButton().getLayoutParams();
        layoutParams.topMargin = (int) mContext.getResources().getDimension(R.dimen.dp_44);
        player.setPlayPosition(item.id);
        player.setLooping(true);
        player.setUpLazy(item.play_url, false, null, null, "");
        ImageView imageView = helper.getView(R.id.img);
        GlideMediaLoader.load(mContext, imageView, item.image);
        if (item.icon != null && item.icon != ""){
            Log.d("image","====="+item.icon);
            GlideMediaLoader.loadHead(mContext, helper.getView(R.id.header_img), item.icon);
        }

        helper.setText(R.id.download_num, item.down_num)
//                .setText(R.id.share_num,item.share_content)
                .setText(R.id.comment_num, item.comment_num)
//                .setText(R.id.fabulous_num,item)
                .setText(R.id.name_tv, "@" + item.username)
                .setText(R.id.title_tv, item.title)
                .setText(R.id.download_num, item.download_num + "")
                .setText(R.id.fabulous_num, item.top_num + "")
                .setText(R.id.share_num, item.share_num + "")
        ;
        TextView text4k = helper.getView(R.id.text_4k);
        Log.d("zdl","===4k==="+item.if4k);
        if (item.if4k != null){
            text4k.setVisibility(item.if4k.endsWith("0") ? View.GONE : View.VISIBLE);
        }
        helper.addOnClickListener(R.id.comment_layout, R.id.download_layout, R.id.share_layout, R.id.add_view, R.id.fabulous_view, R.id.header_img, R.id.text_4k);
        helper.setVisible(R.id.download_layout, showDownload.equals("1"));
    }
}
