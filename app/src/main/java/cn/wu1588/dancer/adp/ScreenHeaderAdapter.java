package cn.wu1588.dancer.adp;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import cn.wu1588.dancer.home.mode.HeaderScreenBean;

public class ScreenHeaderAdapter extends BaseQuickAdapter<HeaderScreenBean.ScreenBean, BaseViewHolder> {
    public ScreenHeaderAdapter() {
        super(R.layout.item_lable_horizontal);
    }

    @Override
    protected void convert(BaseViewHolder helper, HeaderScreenBean.ScreenBean item) {
        helper.addOnClickListener(R.id.tvlableText);
        helper.setText(R.id.tvlableText, item.title);
        helper.getView(R.id.tvlableText).setSelected(item.isSelect);
    }
}
