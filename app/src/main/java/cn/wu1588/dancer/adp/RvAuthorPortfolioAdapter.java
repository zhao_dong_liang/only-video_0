package cn.wu1588.dancer.adp;

import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.kongzue.baseokhttp.util.JsonMap;

import java.util.List;

import androidx.annotation.Nullable;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.utils.TimeUtils;

/**
 * Created by zhou on 2021/2/18 22:48.
 */
public class RvAuthorPortfolioAdapter extends BaseQuickAdapter<JsonMap, BaseViewHolder> {

    private String contrastId;
    private int pos;

    public void setContrastId(String contrastId) {
        this.contrastId = contrastId;
        notifyDataSetChanged();
    }

    public void setPos(int pos) {
        this.pos = pos;
        notifyDataSetChanged();
    }

    public RvAuthorPortfolioAdapter(@Nullable List<JsonMap> data) {
        super(R.layout.item_channel_layout, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, JsonMap item) {
        helper.setText(R.id.rb_ad_1,String.valueOf(helper.getAdapterPosition() + 1));
        TextView textTitle = helper.getView(R.id.item_channel_tvGrade);
        //专辑标题
        textTitle.setText(item.getString("title"));

        if(contrastId.equals(item.getString("id")) || helper.getAdapterPosition() == pos){
            textTitle.setTextColor(mContext.getResources().getColor(R.color.color_E00101_60));
            helper.getView(R.id.rb_ad_3).setVisibility(View.VISIBLE);
        }else{
            textTitle.setTextColor(mContext.getResources().getColor(R.color.black));
            helper.getView(R.id.rb_ad_3).setVisibility(View.GONE);
        }

        //专辑时间
        helper.setText(R.id.rb_ad_2, TimeUtils.timeConversion(item.getString("play_time")));

        //专辑封面
        GlideMediaLoader.load(mContext,helper.getView(R.id.item_channel_image),item.getString("image"),R.drawable.ic_place_ovel);

    }
}
