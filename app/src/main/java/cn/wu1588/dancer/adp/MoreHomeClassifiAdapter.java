package cn.wu1588.dancer.adp;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.home.mode.HomeThematicDetailBean;

public class MoreHomeClassifiAdapter extends BaseQuickAdapter<HomeThematicDetailBean.ListBean, BaseViewHolder> {
    public MoreHomeClassifiAdapter() {
        super(R.layout.item_more_classifi_layout);
    }

    @Override
    protected void convert(BaseViewHolder helper, HomeThematicDetailBean.ListBean item) {
        GlideMediaLoader.load(mContext, helper.getView(R.id.rIvCourse), item.image, R.drawable.ic_place_threee_hodel);
        helper.setText(R.id.tvTitle, item.title);
    }
}
