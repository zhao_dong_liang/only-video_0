//package common.adapter;
//
//import android.support.annotation.Nullable;
//
//import cn.wu1588.dancer.R;
//import com.chad.library.adapter.base.BaseQuickAdapter;
//import com.chad.library.adapter.base.BaseViewHolder;
//
//import java.util.List;
//
//import common.model.ClickBean;
//
///**
// * Created by cnsunrun on 2018-03-14.
// */
//
//public class PopupWindowAdapter extends BaseQuickAdapter<ClickBean, BaseViewHolder> {
//
//    private int mPosition = 0;
//
//    public PopupWindowAdapter(@Nullable List<ClickBean> data, int checkPosition) {
//        super(R.layout.new_popupwindow_list, data);
//        mPosition = checkPosition;
//    }
//
//    /**
//     * 设置选择中项
//     *
//     * @param position
//     */
//    public void setPosition(int position) {
//        this.mPosition = position;
//        notifyDataSetChanged();
//    }
//
//    @Override
//    protected void convert(BaseViewHolder helper, ClickBean item) {
//        int position = helper.getLayoutPosition();
//        helper.setText(R.id.tv_sort_name, item.title);
//        helper.setTextColor(R.id.tv_sort_name, item.isChoose() ? mContext.getResources().getColor(R.color.main_button_color) : mContext.getResources().getColor(R.color.text_color_666));
//        helper.setVisible(R.id.iv_sort_image,item.isChoose);
//
//    }
//}
