package cn.wu1588.dancer.adp;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import cn.wu1588.dancer.channel.mode.ThemeaticBean;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;

public class StarHorizontalAdapter extends BaseQuickAdapter<ThemeaticBean.GirllistBean.InfoBean, BaseViewHolder> {
    public StarHorizontalAdapter() {
        super(R.layout.item_star_horizontal_layout);
    }

    @Override
    protected void convert(BaseViewHolder helper, ThemeaticBean.GirllistBean.InfoBean item) {
        GlideMediaLoader.load(mContext, helper.getView(R.id.rIvCourse), item.image,R.drawable.ic_nv_place);
        helper.setText(R.id.tvTitle, item.title).setText(R.id.tvGrade,item.grade);
    }
}
