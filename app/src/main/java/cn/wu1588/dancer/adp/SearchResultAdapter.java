package cn.wu1588.dancer.adp;

import android.view.View;
import android.widget.TextView;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.sunrun.sunrunframwork.utils.EmptyDeal;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.home.mode.MovieBean;

public class SearchResultAdapter extends BaseQuickAdapter<MovieBean, BaseViewHolder> {

    private TagFlowLayout tagFlowLayout;

    public SearchResultAdapter() {
        super(R.layout.item_search_result_layout);
    }

    @Override
    protected void convert(BaseViewHolder helper, MovieBean item) {
        GlideMediaLoader.load(mContext, helper.getView(R.id.ivImg), item.image, R.drawable.ic_history_place);
        helper.setText(R.id.tvTitle, item.title)
                .setText(R.id.tvPlayNum, mContext.getString(R.string.play_num, item.dealPlayNum()))
                .setText(R.id.tvGrade, item.grade);
        tagFlowLayout = (TagFlowLayout) helper.getView(R.id.tagFlwLayout);
        tagFlowLayout.setAdapter(new TagAdapter<MovieBean.LabelStrBean>(item.label_str) {
            @Override
            public View getView(FlowLayout parent, int position, MovieBean.LabelStrBean o) {
                TextView tv = (TextView) mLayoutInflater.inflate(R.layout.item_simple_type_text, tagFlowLayout, false);
                if (!EmptyDeal.isEmpy(o)){
                    tv.setText(o.title);
                }
                return tv;
            }
        });
    }


}
