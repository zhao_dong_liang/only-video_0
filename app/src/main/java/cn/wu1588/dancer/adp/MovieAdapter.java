package cn.wu1588.dancer.adp;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.home.mode.MovieBean;

public class MovieAdapter extends BaseQuickAdapter<MovieBean, BaseViewHolder> {
    public MovieAdapter() {
        super(R.layout.app_item_home_two_column);
    }

    public MovieAdapter(List<MovieBean> list) {
        super(R.layout.app_item_home_two_column,list);
    }

    @Override
    protected void convert(BaseViewHolder helper, MovieBean item) {
        GlideMediaLoader.load(mContext, helper.getView(R.id.rIvCourse), item.image, R.drawable.ic_place_two_column);
        helper.setText(R.id.tvTitle, item.title).setText(R.id.tvGrade,item.grade);
    }
}
