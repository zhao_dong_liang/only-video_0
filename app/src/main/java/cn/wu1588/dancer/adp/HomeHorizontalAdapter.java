package cn.wu1588.dancer.adp;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.home.mode.HomeBean;

public class HomeHorizontalAdapter extends BaseQuickAdapter<HomeBean.DataListBean.GuessLikesBean, BaseViewHolder> {
    public HomeHorizontalAdapter() {
        super(R.layout.item_home_horizontal_layout);
    }

    @Override
    protected void convert(BaseViewHolder helper, HomeBean.DataListBean.GuessLikesBean item) {
        helper.setText(R.id.tvTitle, item.title).setText(R.id.tvGrade,item.grade);
        GlideMediaLoader.load(mContext,helper.getView(R.id.rIvCourse),item.image,R.drawable.ic_guess_like_place);
    }
}
