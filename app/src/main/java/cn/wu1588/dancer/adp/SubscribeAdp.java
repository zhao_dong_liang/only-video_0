package cn.wu1588.dancer.adp;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.shuyu.gsyvideoplayer.GSYVideoManager;

import java.util.List;
import java.util.Map;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.common.util.LoginUtil;

import cn.wu1588.dancer.home.activity.MoviePlayerV3Activity;
import cn.wu1588.dancer.utils.TimeUtils;
import cn.wu1588.dancer.videoplayer.SwitchUtil;
import cn.wu1588.dancer.videoplayer.SwitchVideo;

/**
 * 作者：ninetailedfox
 * 时间：2021/1/16 3:11 PM
 * 邮箱: 1037438704@qq.com
 * 功能：订阅列表
 **/
public class SubscribeAdp extends BaseQuickAdapter<Map<String, String>, BaseViewHolder> {
    public static final String TAG = "DiscoverAdapter";

    public SubscribeAdp(int layoutResId, @Nullable List<Map<String, String>> data) {
        super(layoutResId, data);
    }

    public SubscribeAdp(int item_subscribe_adp) {
        super(item_subscribe_adp);
    }

    @Override
    protected void convert(BaseViewHolder helper, Map<String, String> item) {
        int id = -1;
        String play_url = item.get("play_url");
        if (play_url == null) {
            play_url = "";
        }

        helper.addOnClickListener(R.id.text_share)
        .setText(R.id.name_tv,item.get("title"))
        .setText(R.id.text_time, TimeUtils.timeConversion(item.get("play_time")))
        .setText(R.id.text_share,item.get("username"))
        ;
        //点击事件跳转到视频播放页面
        RelativeLayout rlCover = helper.itemView.findViewById(R.id.rl_cover);
        rlCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int autoPlay = Integer.parseInt(Config.getData("autoplay", "0"));
                if (autoPlay == 0) {
                    if (LoginUtil.startLogin(mContext)) {
                        CommonIntent.startLoginActivity(mContext);
                    } else {
                        GSYVideoManager.releaseAllVideos();
//                        MoviePlayerActivity.startTActivity((Activity) mContext, String.valueOf(item.get("id")), rlCover);
                        MoviePlayerV3Activity.startTActivity((Activity) mContext, String.valueOf(item.get("id")), rlCover);
                    }
                }
            }
        });
        //头像加载
        GlideMediaLoader.loadHead(mContext, helper.getView(R.id.header_img), item.get("icon"));
        //视频展示
        SwitchVideo switchVideo = helper.itemView.findViewById(R.id.video_item_player);
        ImageView imageView = (ImageView) switchVideo.getThumbImageView();
        if (imageView == null) {
            imageView = new ImageView(mContext);
        }
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        switchVideo.setMovieID(id);
        GlideMediaLoader.load(mContext, imageView, item.get("image"));
        switchVideo.setThumbImageView(imageView);
        //防止错位设置
        switchVideo.setPlayTag(TAG);
        switchVideo.setPlayPosition(helper.getLayoutPosition());
        SwitchUtil.optionPlayer(switchVideo, play_url, true, "");
        switchVideo.setUpLazy(play_url, true, null, null, "");
        if (GSYVideoManager.instance().getPlayTag().equals(TAG)
                && (helper.getLayoutPosition() == GSYVideoManager.instance().getPlayPosition())) {
            switchVideo.getThumbImageViewLayout().setVisibility(View.GONE);
        } else {
            switchVideo.getThumbImageViewLayout().setVisibility(View.VISIBLE);
            RelativeLayout thumbImageViewLayout = switchVideo.getThumbImageViewLayout();
            ViewGroup parent = (ViewGroup) thumbImageViewLayout.getParent();
            parent.removeView(thumbImageViewLayout);
            parent.addView(thumbImageViewLayout, 1);
        }
    }
}
