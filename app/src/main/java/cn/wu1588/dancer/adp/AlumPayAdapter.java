package cn.wu1588.dancer.adp;

import android.widget.CheckBox;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.mine.mode.PayMentBean;

public class AlumPayAdapter extends BaseQuickAdapter<PayMentBean, BaseViewHolder> {

    public AlumPayAdapter() {
        super(R.layout.item_alum_pay_way);
    }

    @Override
    protected void convert(BaseViewHolder helper, PayMentBean item) {
        CheckBox checkBox = helper.getView(R.id.checkbox);
        checkBox.setCompoundDrawablesWithIntrinsicBounds(R.drawable.radio_check_box, 0, 0, 0);
        helper.setText(R.id.tvTitle, item.name);
        helper.setChecked(R.id.checkbox, item.isCheckd);

        switch (item.id) {
            case "2":
                helper.setBackgroundRes(R.id.ivIcon, R.drawable.vip_icon_zhifubao_normal);
                helper.setText(R.id.tvSubTitle, "推荐支付宝用户使用");
                break;
            case "1":
                helper.setBackgroundRes(R.id.ivIcon, R.drawable.vip_icon_weixin_normal);
                helper.setText(R.id.tvSubTitle, "推荐微信用户使用");
                break;
            case "3":
                helper.setBackgroundRes(R.id.ivIcon, R.drawable.vip_icon_weixin_normal);
                helper.setText(R.id.tvSubTitle, "推荐微信用户使用");
                break;
            case "4":
                helper.setBackgroundRes(R.id.ivIcon, R.drawable.vip_icon_weixin_normal);
                helper.setText(R.id.tvSubTitle, "推荐微信用户使用");
                break;
            case "5":
                helper.setBackgroundRes(R.id.ivIcon, R.drawable.chongzhi_icon_yue_normal);
                helper.setText(R.id.tvSubTitle, "推荐微信用户使用");
                break;
        }
    }
}
