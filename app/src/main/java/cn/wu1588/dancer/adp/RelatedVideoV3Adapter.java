package cn.wu1588.dancer.adp;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.kongzue.baseokhttp.util.JsonMap;

import java.util.List;

import androidx.annotation.Nullable;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.home.mode.MovieByCateV3Bean;

/**
 * Created by zhou on 2021/2/9 17:02.
 */
public class RelatedVideoV3Adapter extends BaseQuickAdapter<MovieByCateV3Bean, BaseViewHolder> {
    public RelatedVideoV3Adapter(@Nullable List<MovieByCateV3Bean> data) {
        super(R.layout.item_guess_like_layout, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, MovieByCateV3Bean item) {

        helper.addOnClickListener(R.id.cl_Container);
        GlideMediaLoader.load(mContext, helper.getView(R.id.item_imageView), item.getImage());
        helper.setText(R.id.item_textview,item.getTitle());
        helper.setText(R.id.item_textview2,item.getAdd_time());
    }

//    public RelatedVideoV3Adapter(@Nullable List<JsonMap> data) {
//        super(R.layout.item_related_vide, data);
//    }
//
//
//    @Override
//    protected void convert(BaseViewHolder helper, JsonMap item) {
//        GlideMediaLoader.load(mContext,helper.getView(R.id.item_imageView),item.getString("image"),R.drawable.ic_place_ovel);
//        helper.setText(R.id.item_textview,item.getString("title"));
//        helper.setText(R.id.item_textview2,item.getString("add_time"));
//    }
}
