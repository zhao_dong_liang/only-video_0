package cn.wu1588.dancer.adp;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.kongzue.baseokhttp.util.JsonList;
import com.kongzue.baseokhttp.util.JsonMap;

import java.util.List;

import androidx.annotation.Nullable;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;

/**
 * Created by zhou on 2021/2/9 17:02.
 */
public class RelatedVideoAdapter extends BaseQuickAdapter<JsonMap, BaseViewHolder> {

    public RelatedVideoAdapter(@Nullable List<JsonMap> data) {
        super(R.layout.item_related_vide, data);
    }


    @Override
    protected void convert(BaseViewHolder helper, JsonMap item) {
        GlideMediaLoader.load(mContext,helper.getView(R.id.item_imageView),item.getString("image"),R.drawable.ic_place_ovel);
        helper.setText(R.id.item_textview,item.getString("title"));
        helper.setText(R.id.item_textview2,item.getString("add_time"));
    }
}
