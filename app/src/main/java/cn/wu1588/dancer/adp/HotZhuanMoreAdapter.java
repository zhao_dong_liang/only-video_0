package cn.wu1588.dancer.adp;

import android.view.View;
import android.widget.AdapterView;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.sunrun.sunrunframwork.adapter.ViewHodler;
import com.sunrun.sunrunframwork.adapter.ViewHolderAdapter;
import com.sunrun.sunrunframwork.utils.EmptyDeal;
import com.sunrun.sunrunframwork.weight.GridViewForScroll;

import java.util.List;

import cn.wu1588.dancer.channel.mode.ThemeaticBean;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.config.Const;

public class HotZhuanMoreAdapter extends BaseQuickAdapter<List<ThemeaticBean.HotmoreBean>, BaseViewHolder> {
    public HotZhuanMoreAdapter() {
        super(R.layout.item_hot_zhuan_ti_more);
    }

    @Override
    protected void convert(BaseViewHolder helper,List<ThemeaticBean.HotmoreBean> item) {
        if (!EmptyDeal.isEmpy(item)){
            ((GridViewForScroll) helper.getView(R.id.gridViewForScroll)).setAdapter(new ViewHolderAdapter<ThemeaticBean.HotmoreBean>(mContext,item, R.layout.item_grid_channel) {
                @Override
                public void fillView(ViewHodler viewHodler, ThemeaticBean.HotmoreBean s, int i) {
                    viewHodler.setText(R.id.itemTitle, s.title);
                    GlideMediaLoader.load(mContext,viewHodler.getView(R.id.itemIcon),s.logo,R.drawable.ic_place_ovel);
                }
            });
            ((GridViewForScroll) helper.getView(R.id.gridViewForScroll)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    ThemeaticBean.HotmoreBean hotmoreBean = item.get(position);
                    CommonIntent.startHomeMoreClassifiListActivity(mContext,hotmoreBean.id, Const.FROM_CHANNEL,Const.TWO);
                }
            });
        }
    }
}
