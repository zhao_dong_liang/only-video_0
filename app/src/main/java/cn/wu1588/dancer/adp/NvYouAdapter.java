package cn.wu1588.dancer.adp;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import cn.wu1588.dancer.channel.mode.NvyouBean;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;

public class NvYouAdapter extends BaseQuickAdapter<NvyouBean, BaseViewHolder> {
    public int[] bgs={R.drawable.bg1,R.drawable.bg2,R.drawable.bg3,R.drawable.bg4,R.drawable.bg5,R.drawable.bg6};
    public NvYouAdapter() {
        super(R.layout.item_nv_you_list_layout);
    }

    @Override
    protected void convert(BaseViewHolder helper, NvyouBean item) {
        GlideMediaLoader.loadHead(mContext, helper.getView(R.id.rIvStar), item.icon);
        helper.setText(R.id.tvName,item.name);
        for (int i = 0; i < bgs.length; i++) {
            if (helper.getAdapterPosition()%6==i) {
                helper.setBackgroundRes(R.id.llContainer,bgs[i]);
            }
        }

    }
}
