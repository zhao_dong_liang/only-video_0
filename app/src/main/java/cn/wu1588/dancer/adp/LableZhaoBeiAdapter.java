package cn.wu1588.dancer.adp;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import cn.wu1588.dancer.channel.mode.LableBean;

public class LableZhaoBeiAdapter extends BaseQuickAdapter<LableBean.SelectTwoBean, BaseViewHolder> {

    public LableZhaoBeiAdapter() {
        super(R.layout.item_lable_horizontal);
    }

    @Override
    protected void convert(BaseViewHolder helper, LableBean.SelectTwoBean item) {
        helper.addOnClickListener(R.id.tvlableText);
        helper.setText(R.id.tvlableText, item.title);
        helper.getView(R.id.tvlableText).setSelected(item.isSelect);

    }

}
