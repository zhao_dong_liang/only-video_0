package cn.wu1588.dancer.adp;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

public class HomeAdapter extends BaseQuickAdapter<String, BaseViewHolder> {
    public HomeAdapter() {
        super(R.layout.item_home_layout);
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {

    }
}
