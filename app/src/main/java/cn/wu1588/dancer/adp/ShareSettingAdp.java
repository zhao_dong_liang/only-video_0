package cn.wu1588.dancer.adp;

import android.view.View;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.Map;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;

/**
 * 作者：ninetailedfox
 * 时间：2021/1/17 8:57 PM
 * 邮箱: 1037438704@qq.com
 * 功能：分享设置 适配器
 **/
public class ShareSettingAdp extends BaseQuickAdapter<Map<String, String>, BaseViewHolder> {

    public ShareSettingAdp(int item_share_setting) {
        super(item_share_setting);
    }

    @Override
    protected void convert(BaseViewHolder helper, Map<String, String> item) {
//        "type": "1",//广告类型：1：应用下载；2：信息收集；3：品牌推广；4：京东商品；5：淘宝商品；6：拼多多商品；7：小程序商品
        String type = "";
        switch (Integer.valueOf(item.get("type"))) {
            case 1:
                type = "广告类型";
                break;
            case 2:
                type = "应用下载";
                break;
            case 3:
                type = "信息收集";
                break;
            case 4:
                type = "品牌推广";
                break;
            case 5:
                type = "京东商品";
                break;
            case 6:
                type = "淘宝商品";
                break;
            case 7:
                type = "拼多多商品";
                break;
            case 8:
                type = "小程序商品";
                break;
            default:
                break;

        }


        helper.setText(R.id.text_title, item.get("title"))
                .setText(R.id.text_time, item.get("add_time"))
                .setText(R.id.text_download, type)
        ;
        GlideMediaLoader.load(mContext, helper.getView(R.id.image_share), item.get("logo"), R.drawable.ic_history_place);
        ImageView image_select = helper.getView(R.id.image_select);
//        是1，就代表选中
        if (item.get("current_ad").equals("1")) {
            image_select.setVisibility(View.VISIBLE);
        } else {
            image_select.setVisibility(View.INVISIBLE);
        }
    }
}
