package cn.wu1588.dancer.adp;

import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.MultipleItemRvAdapter;

import cn.wu1588.dancer.home.mode.HomeBean;
import cn.wu1588.dancer.home.provider.ChangeProvider;
import cn.wu1588.dancer.home.provider.FreeHeaderProvider;
import cn.wu1588.dancer.home.provider.ImgItemProvider;
import cn.wu1588.dancer.home.provider.ScrollItemProvider;
import cn.wu1588.dancer.home.provider.SectioonHeaderProvider;
import cn.wu1588.dancer.home.provider.ThreeColumnItemProvider;
import cn.wu1588.dancer.home.provider.TwoColumnItemProvider;

public class HomeNewAdapter extends MultipleItemRvAdapter<HomeBean.DataListBean, BaseViewHolder> {

    //通栏大图片广告
    public static final int TYPE_IMG = 100;
    //三列
    public static final int TYPE_THREE_COLUMN = 200;
    //通栏水平拖动
    public static final int TYPE_HORIZONTAL_SCROLL = 300;
    //两列
    public static final int TYPE_TWO_COLUMN = 400;
    //分组头部
    public static final int TYPE_SECTION_TITLE = 500;

    //换一换
    public static final int TYPE_CHANGE_AND_CHANGE=600;
    //免费头部
    public static final int TYPE_FREE_HEADER_TITLE = 700;

    public HomeNewAdapter() {
        super(null);
        finishInitialize();
    }

    @Override
    protected int getViewType(HomeBean.DataListBean homeBean) {
        int type = homeBean.stype;
        if (type == 1) {
            return TYPE_IMG;
        } else if (type == 2) {
            return TYPE_THREE_COLUMN;
        } else if (type == 3) {
            return TYPE_HORIZONTAL_SCROLL;
        } else if (type == 4) {
            return TYPE_TWO_COLUMN;
        }else if (type==5){
            return TYPE_SECTION_TITLE;
        }else if (type==6){
            return TYPE_CHANGE_AND_CHANGE;
        }else if (type==7){
            return TYPE_FREE_HEADER_TITLE;
        }
        return 0;
    }

    @Override
    public void registerItemProvider() {
        //通栏商品大图片
        mProviderDelegate.registerProvider(new ImgItemProvider());
        //三列图文
        mProviderDelegate.registerProvider(new ThreeColumnItemProvider());
        //通栏横向滑动
        mProviderDelegate.registerProvider(new ScrollItemProvider());
        //两列图文
        mProviderDelegate.registerProvider(new TwoColumnItemProvider());
        //分组头部
        mProviderDelegate.registerProvider(new SectioonHeaderProvider());
        //换一换
        mProviderDelegate.registerProvider(new ChangeProvider());
        //免费头部
        mProviderDelegate.registerProvider(new FreeHeaderProvider());
    }
}
