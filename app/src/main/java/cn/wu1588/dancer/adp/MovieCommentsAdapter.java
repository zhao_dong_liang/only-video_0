package cn.wu1588.dancer.adp;

import android.text.TextUtils;
import android.widget.TextView;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.sunrun.sunrunframwork.adapter.ViewHodler;
import com.sunrun.sunrunframwork.adapter.ViewHolderAdapter;
import com.sunrun.sunrunframwork.uiutils.TextColorUtils;
import com.sunrun.sunrunframwork.weight.ListViewForScroll;

import cn.wu1588.dancer.home.mode.MovieDetailBean;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;

public class MovieCommentsAdapter extends BaseQuickAdapter<MovieDetailBean.CommentBean, BaseViewHolder> {

    public MovieCommentsAdapter() {
        super(R.layout.item_comments_layout);
    }

    @Override
    protected void convert(BaseViewHolder helper, MovieDetailBean.CommentBean item) {
        helper.addOnClickListener(R.id.llContainer, R.id.tvZanNum);
        GlideMediaLoader.loadHead(mContext, helper.getView(R.id.rIvStar), item.icon);
        helper.setText(R.id.tvNikeName, item.nickname)
                .setText(R.id.tvTime, item.add_time)
                .setText(R.id.tvZanNum, item.like_num)
                .setText(R.id.tvContent, item.content);
        TextView tvNikeName = helper.getView(R.id.tvNikeName);
        TextView tvZanNum = helper.getView(R.id.tvZanNum);
        //性别   1=男  2=女
        if (TextUtils.equals("1", item.gender)) {
            tvNikeName.setCompoundDrawables(null, null, mContext.getResources().getDrawable(R.drawable.icon_men_normal), null);
        } else {
            tvNikeName.setCompoundDrawables(null, null, mContext.getResources().getDrawable(R.drawable.icon_women_normal), null);
        }
        //点赞状态 0=可以赞 1=不能赞
        if (item.status == 1) {
            TextColorUtils.setCompoundDrawables(tvZanNum,null, null, mContext.getResources().getDrawable(R.drawable.pinglun_btn_zan_selected), null);
        } else {
            TextColorUtils.setCompoundDrawables(tvZanNum,null, null, mContext.getResources().getDrawable(R.drawable.pinglun_btn_zan_normal), null);
        }
        ListViewForScroll listViewForScroll = helper.getView(R.id.listViewChild);
        listViewForScroll.setAdapter(new ViewHolderAdapter<MovieDetailBean.CommentBean.ChildBean>(mContext, item.child, R.layout.simple_list_item) {
            @Override
            public void fillView(ViewHodler viewHodler, MovieDetailBean.CommentBean.ChildBean childBean, int i) {
                viewHodler.setText(R.id.tvReComments, childBean.content);
            }
        });
    }
}
