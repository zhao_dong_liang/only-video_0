//package common.adapter;
//
//import android.support.annotation.Nullable;
//
//import com.chad.library.adapter.base.BaseQuickAdapter;
//import com.chad.library.adapter.base.BaseViewHolder;
//
//import java.util.List;
//
///**
// * Created by wangchao on 2018-12-05.
// */
//public class LeftAdapter extends BaseQuickAdapter<CategoryBean,BaseViewHolder> {
//
//    private int position;
//
//    public LeftAdapter(@Nullable List<CategoryBean> data) {
//        super(R.layout.item_left,data);
//    }
//
//    @Override
//    protected void convert(BaseViewHolder helper, CategoryBean item) {
//        helper.setText(R.id.tv_title,item.title);
//        if (position == helper.getLayoutPosition()){
//            helper.setBackgroundColor(R.id.rl_container,mContext.getResources().getColor(R.color.white));
//            item.isLeftSelect=true;
//        }else{
//            helper.setBackgroundColor(R.id.rl_container,mContext.getResources().getColor(R.color.gray_color_f5f5));
//            item.isLeftSelect=false;
//        }
//    }
//    public void setPosition(int currentPosition) {
//        position = currentPosition;
//        notifyDataSetChanged();
//
//    }
//}
