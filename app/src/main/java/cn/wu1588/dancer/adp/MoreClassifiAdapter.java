package cn.wu1588.dancer.adp;

import cn.wu1588.dancer.R;
import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.makeramen.roundedimageview.RoundedImageView;

import androidx.annotation.NonNull;
import cn.wu1588.dancer.channel.mode.ThemeDetailBean;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.widget.ArcImageView;


public class MoreClassifiAdapter extends BaseQuickAdapter<ThemeDetailBean, BaseViewHolder> {

    public MoreClassifiAdapter() {
        super(R.layout.item_more_classifi_layout);
    }

    @Override
    protected void convert(BaseViewHolder helper, ThemeDetailBean item) {
        GlideMediaLoader.loadLargeImg(mContext,helper.getView(R.id.rIvCourse),item.image,R.drawable.ic_place_threee_hodel);
        helper.setText(R.id.tvTitle,item.title);
    }

    @Override
    public void onViewRecycled(@NonNull BaseViewHolder holder) {
        super.onViewRecycled(holder);
        RoundedImageView view = holder.getView(R.id.rIvCourse);
        ArcImageView arcImageView = holder.getView(R.id.arcImg);
        if (view!=null){
            Glide.clear(view);
        }
    }
}
