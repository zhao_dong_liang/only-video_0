package cn.wu1588.dancer.adp;


import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.my.toolslib.NumUtils;
import com.my.toolslib.http.utils.NetRequestListenerProxy;
import com.shuyu.gsyvideoplayer.GSYVideoManager;
import com.shuyu.gsyvideoplayer.listener.GSYSampleCallBack;
import com.shuyu.gsyvideoplayer.video.StandardGSYVideoPlayer;
import com.shuyu.gsyvideoplayer.video.base.GSYVideoPlayer;
import com.tencent.mmkv.MMKV;

import java.util.ArrayList;

import cn.wu1588.dancer.model.Ad;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.model.SystemParams;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.common.util.DownloadUtils;
import cn.wu1588.dancer.home.mode.MovieBean;
import cn.wu1588.dancer.videoplayer.SwitchUtil;
import cn.wu1588.dancer.videoplayer.SwitchVideo;

/**
 * 作者：ninetailedfox
 * 时间：2021/1/17 3:31 PM
 * 邮箱: 1037438704@qq.com
 * 功能：首页列表适配器
 **/
public class DiscoverAdapter extends BaseMultiItemQuickAdapter<MultiItemEntity, BaseViewHolder> {
    public static final String TAG = "DiscoverAdapter";
    private int freeTime = 20;
    private int autoPlay = 0;
    private String showDownload;
    private PlayCallback callback;
    private boolean showTag;
    private int tag;


    public void setTag(int tag) {
        this.tag = tag;
    }

    public DiscoverAdapter(PlayCallback callback) {
        super(new ArrayList<>());
        addItemType(1, R.layout.item_discover_layout);
        addItemType(2, R.layout.item_discover_ad_layout);
        this.callback = callback;

        MMKV mmkv = MMKV.defaultMMKV();
        autoPlay = NumUtils.parseInt(mmkv.decodeString(SystemParams.AUTO_PLAY), 0);
        freeTime = NumUtils.parseInt(mmkv.decodeString(SystemParams.FREE_PLAY_TIME), 20);
        showDownload = mmkv.decodeString(SystemParams.IS_SHOW_DOWBTN);
    }

    public DiscoverAdapter(PlayCallback callback, boolean showTag) {
        super(new ArrayList<>());
        addItemType(1, R.layout.item_discover_layout);
        addItemType(2, R.layout.item_discover_ad_layout);
        this.callback = callback;
        this.showTag = showTag;

        MMKV mmkv = MMKV.defaultMMKV();
        autoPlay = NumUtils.parseInt(mmkv.decodeString(SystemParams.AUTO_PLAY), 0);
        freeTime = NumUtils.parseInt(mmkv.decodeString(SystemParams.FREE_PLAY_TIME), 20);
        showDownload = mmkv.decodeString(SystemParams.IS_SHOW_DOWBTN);
    }

    @Override
    protected void convert(BaseViewHolder helper, MultiItemEntity entity) {
        int itemType = entity.getItemType();
        if (itemType == 1) {
            MovieBean item = (MovieBean) entity;
            bindMovie(helper, item);
        } else {
            Ad item = (Ad) entity;
            bindAd(helper, item);
        }
    }

    private void bindAd(BaseViewHolder helper, Ad item) {
        View.OnClickListener lis = v -> {
            BaseQuestStart.updateStatistics(new NetRequestListenerProxy(helper.itemView.getContext()), item.id, "click");
            if (!TextUtils.isEmpty(item.outer_url)) {
                CommonIntent.startLoadUrlWebActivity((Activity) v.getContext(), item.outer_url, item);
                return;
            }
            if (!TextUtils.isEmpty(item.html)) {
                CommonIntent.startLoadHtmlWebActivity((Activity) v.getContext(), item.html, item);
            }
        };
        helper.setText(R.id.tv_ad_title, item.title);
        helper.setText(R.id.tv_ad_label, item.short_title);
        helper.setGone(R.id.btn_ad_download, !TextUtils.isEmpty(item.android_package_url));
        helper.setGone(R.id.tv_learn_more, !TextUtils.isEmpty(item.outer_url) || !TextUtils.isEmpty(item.html));
        if (!TextUtils.isEmpty(item.img_url) && TextUtils.isEmpty(item.video_url)) {
            helper.setGone(R.id.iv_ad_image, true);
            helper.setGone(R.id.video_item_player, false);
            GlideMediaLoader.load(mContext, helper.getView(R.id.iv_ad_image), item.img_url);
            helper.getView(R.id.iv_ad_image).setOnClickListener(lis);
        } else if (!TextUtils.isEmpty(item.video_url)) {
            helper.setGone(R.id.iv_ad_image, false);
            helper.setGone(R.id.video_item_player, true);
            setMoviePlayer(helper, item);
        }

        helper.getView(R.id.btn_ad_download).setOnClickListener(v -> {
            if (!TextUtils.isEmpty(item.android_package_url)) {
                DownloadUtils downloadUtils = new DownloadUtils(v.getContext());
                downloadUtils.download(item.android_package_url, item.package_name);
                BaseQuestStart.updateStatistics(new NetRequestListenerProxy(helper.itemView.getContext()), item.id, "download");
            }
        });

        helper.getView(R.id.tv_learn_more).setOnClickListener(lis);
        helper.getView(R.id.llContainer).setOnClickListener(lis);
    }

    private void bindMovie(BaseViewHolder helper, MovieBean item) {
//        movie为视频，ad广告，
        Log.d("bindMovie","=========="+item.getContent_type());
        RelativeLayout layVideo = helper.getView(R.id.rlTestOver);
        helper.addOnClickListener(R.id.ivCollect, R.id.ivShare, R.id.llContainer, R.id.kind, R.id.down_view, R.id.ivComment, R.id.header_img, R.id.name_tv);

        if (showTag) {
            helper.getView(R.id.kind).setVisibility(View.VISIBLE);
            helper.setText(R.id.kind, item.home_category);
        } else {
            helper.getView(R.id.kind).setVisibility(View.GONE);
        }
        helper.setImageResource(R.id.ivCollect, !item.getIsLike() ? R.drawable.home_fabulous_no : R.drawable.home_fabulous_yes);
        RelativeLayout cover = helper.getView(R.id.rl_cover);
        if (autoPlay == 0) {
            cover.setVisibility(View.VISIBLE);
            cover.setOnTouchListener((view, motionEvent) -> {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (callback != null) {
                        callback.onClick(helper.getLayoutPosition());
                    }
                }
                return true;
            });
        } else {
            cover.setVisibility(View.GONE);
        }
        setMoviePlayer(helper, item);
        SwitchVideo gsyVideoPlayer = helper.getView(R.id.video_item_player);
        helper.getView(R.id.btnBuyMember).setOnClickListener(v -> CommonIntent.startRechargeActivity(mContext, 0));
        gsyVideoPlayer.setGSYVideoProgressListener((progress, secProgress, currentPosition, duration) -> {
            if (!item.isFree()) {
                if (!Config.getLoginInfo().isMember()) {
                    if (currentPosition > freeTime * 1000) {
                        layVideo.setVisibility(View.VISIBLE);
                        gsyVideoPlayer.onVideoReset();
                    } else {
                        layVideo.setVisibility(View.GONE);
                    }
                }
            }
        });
        helper.getView(R.id.btnRest).setOnClickListener(v -> {
            layVideo.setVisibility(View.GONE);
            gsyVideoPlayer.startPlayLogic();
        });
        helper.setText(R.id.title_tv, item.title)
                .setText(R.id.name_tv, item.username)
                .setText(R.id.title_tv2, item.title);

        TextView titleTv2 = helper.getView(R.id.title_tv2);
        TextView titleTv = helper.getView(R.id.title_tv);

        if (tag == 0) {
            titleTv.setVisibility(View.VISIBLE);
            titleTv2.setVisibility(View.GONE);
        } else {
            titleTv.setVisibility(View.GONE);
            titleTv2.setVisibility(View.VISIBLE);
        }


        GlideMediaLoader.loadHead(mContext, helper.getView(R.id.header_img), item.icon);
        gsyVideoPlayer.setVideoAllCallBack(new GSYSampleCallBack() {
            @Override
            public void onEnterFullscreen(String url, Object... objects) {
                super.onEnterFullscreen(url, objects);
                GSYVideoPlayer fullgsyVideoPlayer = (GSYVideoPlayer) objects[1];
                fullgsyVideoPlayer.getTitleTextView().setText(item.title);
                fullgsyVideoPlayer.setGSYVideoProgressListener((progress, secProgress, currentPosition, duration) -> {
                    if (!item.isFree()) {
                        if (!Config.getLoginInfo().isMember()) {
                            if (currentPosition > freeTime * 1000) {
                                layVideo.setVisibility(View.VISIBLE);
                                GSYVideoManager.backFromWindowFull(mContext);
                                GSYVideoManager.releaseAllVideos();
                            } else {
                                layVideo.setVisibility(View.GONE);
                            }
                        }
                    }
                });
            }
        });

        helper.setVisible(R.id.down_view, showDownload.equals("1"));
    }

    private void setMoviePlayer(BaseViewHolder helper, MultiItemEntity item) {
        int id = -1;
        String image = "";
        String playUrl = "";
        String title = "";
        if (item instanceof MovieBean) {
            id = ((MovieBean) item).id;
            image = ((MovieBean) item).image;
            playUrl = ((MovieBean) item).play_url;
            // 不设置视频标题： title = ((MovieBean) item).title;
        } else if (item instanceof Ad) {
            id = NumUtils.parseInt(((Ad) item).id, 0);
            image = ((Ad) item).img_url;
            playUrl = ((Ad) item).video_url;
            // 不设置视频标题： title = "";
        }

        if (TextUtils.isEmpty(playUrl)) {
            playUrl = "";//防止空指针
        }

        SwitchVideo gsyVideoPlayer = helper.getView(R.id.video_item_player);
        gsyVideoPlayer.setIsClikListner(true);
        ImageView imageView = (ImageView) gsyVideoPlayer.getThumbImageView();
        if (imageView == null) {
            imageView = new ImageView(mContext);
        }
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        gsyVideoPlayer.setMovieID(id);
        GlideMediaLoader.load(mContext, imageView, image);
        gsyVideoPlayer.setThumbImageView(imageView);
        //防止错位设置
        gsyVideoPlayer.setPlayTag(TAG);
        gsyVideoPlayer.setPlayPosition(helper.getLayoutPosition());
        SwitchUtil.optionPlayer(gsyVideoPlayer, playUrl, true, title);
        gsyVideoPlayer.setUpLazy(playUrl, true, null, null, title);
        if (GSYVideoManager.instance().getPlayTag().equals(DiscoverAdapter.TAG)
                && (helper.getLayoutPosition() == GSYVideoManager.instance().getPlayPosition())) {
            gsyVideoPlayer.getThumbImageViewLayout().setVisibility(View.GONE);
        } else {
            gsyVideoPlayer.getThumbImageViewLayout().setVisibility(View.VISIBLE);
            RelativeLayout thumbImageViewLayout = gsyVideoPlayer.getThumbImageViewLayout();
            ViewGroup parent = (ViewGroup) thumbImageViewLayout.getParent();
            parent.removeView(thumbImageViewLayout);
            parent.addView(thumbImageViewLayout, 1);
        }
        //设置全屏按键功能
        gsyVideoPlayer.getFullscreenButton().setOnClickListener(v -> resolveFullBtn(gsyVideoPlayer));
        if (autoPlay == 0) {
            gsyVideoPlayer.getBottomContainer().setVisibility(View.GONE);
        }
    }

    /**
     * 全屏幕按键处理
     */
    private void resolveFullBtn(final StandardGSYVideoPlayer standardGSYVideoPlayer) {
        if (!standardGSYVideoPlayer.isIfCurrentIsFullscreen()) {
            standardGSYVideoPlayer.startWindowFullscreen(mContext, true, true);
        }
    }

    public interface PlayCallback {
        void onClick(int positon);
    }
}