package cn.wu1588.dancer.adp;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.Map;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;

public class SubscribeTopAdp extends BaseQuickAdapter<Map<String, String>, BaseViewHolder> {

    public SubscribeTopAdp(int rv_content) {
        super(rv_content);
    }

    @Override
    protected void convert(BaseViewHolder helper, Map<String, String> item) {
        helper.setText(R.id.tvTitle,item.get("nickname") == null?"":item.get("nickname"));
        GlideMediaLoader.loadHead(mContext, helper.getView(R.id.image_view), item.get("icon"));


    }
}