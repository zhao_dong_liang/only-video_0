package cn.wu1588.dancer.adp;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import cn.wu1588.dancer.channel.mode.ThemeaticBean;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;

public class GuanggaoAdapter extends BaseQuickAdapter<ThemeaticBean.AddslideBean, BaseViewHolder> {
    public GuanggaoAdapter() {
        super(R.layout.item_guanggao_layout);
    }

    @Override
    protected void convert(BaseViewHolder helper, ThemeaticBean.AddslideBean item) {
        GlideMediaLoader.load(mContext, helper.getView(R.id.rIvStar), item.image, R.drawable.ic_placeholder_image);
    }
}
