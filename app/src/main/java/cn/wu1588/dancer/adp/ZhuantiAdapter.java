package cn.wu1588.dancer.adp;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import cn.wu1588.dancer.channel.mode.ThemeaticBean;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;

public class ZhuantiAdapter extends BaseQuickAdapter<ThemeaticBean.MustseeBean, BaseViewHolder> {
    public ZhuantiAdapter() {
        super(R.layout.item_zhuan_ti_layout);
    }

    @Override
    protected void convert(BaseViewHolder helper, ThemeaticBean.MustseeBean item) {
        helper.addOnClickListener(R.id.llContainer,R.id.btnChooseMovie);
        GlideMediaLoader.loadHead(mContext, helper.getView(R.id.rIvStar), item.logo);
        helper.setText(R.id.tvTitle, item.title)
                .setText(R.id.tvTitme, item.add_time)
                .setText(R.id.tvContent, item.content);

    }
}
