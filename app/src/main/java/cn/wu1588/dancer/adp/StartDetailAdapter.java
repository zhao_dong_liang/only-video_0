package cn.wu1588.dancer.adp;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.home.mode.MovieBean;

public class StartDetailAdapter extends BaseQuickAdapter<MovieBean, BaseViewHolder> {
    public StartDetailAdapter() {
        super(R.layout.item_nv_you_detail_layout);
    }

    @Override
    protected void convert(BaseViewHolder helper, MovieBean item) {
        GlideMediaLoader.load(mContext, helper.getView(R.id.ivImg), item.image,R.drawable.ic_history_place);
        helper.setText(R.id.tvTitle, item.title)
                .setText(R.id.tvPlayNum, mContext.getString(R.string.play_num, item.dealPlayNum()));
    }
}
