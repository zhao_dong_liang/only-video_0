package cn.wu1588.dancer.adp;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;

public class MyPagerAdapter extends FragmentPagerAdapter {
    private List<Fragment> list ;
    private Context mContext;
    private List<String> listTitle;

    public MyPagerAdapter(FragmentManager fm, List<Fragment> list,List<String> listTitle) {
        super(fm);
        this.list = list;
        this.listTitle = listTitle;
    }
    public MyPagerAdapter(FragmentManager fm, List<Fragment> list, Context context) {
        super(fm);
        this.list = list;
        this.mContext = context;
    }


    @Override
    public Fragment getItem(int position) {
        return list.get(position);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if (listTitle == null){
            return super.getPageTitle(position);
        }
        return listTitle.get(position);

    }
}
