package cn.wu1588.dancer.adp;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.cnsunrun.commonui.widget.viewpager.OpenPagerAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cn.wu1588.dancer.home.fragment.SmallVideoPlayerFragment;
import cn.wu1588.dancer.home.mode.MovieBean;

public class SmallVideoPageAdapter extends OpenPagerAdapter<MovieBean> {
    private List<MovieBean> mDatas=new ArrayList<>();

    public SmallVideoPageAdapter(FragmentManager fm,List<MovieBean> movieBeans) {
        super(fm);
        mDatas.clear();
        if (movieBeans!=null){
            mDatas.addAll(movieBeans);
        }
    }


    @Override
    public Fragment getItem(int position) {
        return SmallVideoPlayerFragment.newInstance(mDatas.get(position),position);
    }

    @Override
    protected MovieBean getItemData(int position) {
        return mDatas.get(position);
    }

    @Override
    protected boolean dataEquals(MovieBean oldData, MovieBean newData) {
        return oldData.id==newData.id;
    }

    @Override
    protected int getDataPosition(MovieBean data) {
        return mDatas.indexOf(data);
    }

    @Override
    public int getCount() {
        return mDatas.size();
    }

    public void setNewData(List<MovieBean> datas) {
        mDatas.clear();
        mDatas.addAll(datas);
        notifyDataSetChanged();
    }

    public void addData(MovieBean movieBean) {
        mDatas.add(movieBean);
        notifyDataSetChanged();
    }

    public void addData(int position, MovieBean movieBean) {
        mDatas.add(position, movieBean);
        notifyDataSetChanged();
    }

    public void remove(int position) {
        mDatas.remove(position);
        notifyDataSetChanged();
    }

    public void moveData(int from, int to) {
        if (from == to) return;
        Collections.swap(mDatas, from, to);
        notifyDataSetChanged();
    }

    public void moveDataToFirst(int from) {
        MovieBean tempData = mDatas.remove(from);
        mDatas.add(0, tempData);
        notifyDataSetChanged();
    }

    public void updateByPosition(int position, MovieBean movieBean) {
//        if (position >= 0 && mDatas.size() > position) {
//            mDatas.set(position, movieBean);
//            SmallVideoPlayerFragment targetF = getCachedFragmentByPosition(position);
//            if (targetF != null) {
//                targetF.updateData(movieBean);
//            }
//        }
    }

    public SmallVideoPlayerFragment getCachedFragmentByPosition(int position) {
        return (SmallVideoPlayerFragment) getFragmentByPosition(position);
    }
}
