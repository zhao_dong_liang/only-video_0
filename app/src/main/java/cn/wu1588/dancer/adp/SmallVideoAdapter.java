package cn.wu1588.dancer.adp;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.home.mode.MovieBean;

public class SmallVideoAdapter extends BaseQuickAdapter<MovieBean, BaseViewHolder> {


    public SmallVideoAdapter(){
        super(R.layout.small_video_item);
    }

    @Override
    protected void convert(BaseViewHolder helper, MovieBean item) {
        GlideMediaLoader.load(mContext,helper.getView(R.id.image),item.image);
        GlideMediaLoader.loadHead(mContext,helper.getView(R.id.header_img),item.icon);
        helper.setText(R.id.name_tv,item.username).setText(R.id.fabulous_num,item.top_num+"");



    }
}
