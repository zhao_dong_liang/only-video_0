package cn.wu1588.dancer.home.activity;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.View;

import cn.wu1588.dancer.R;
import com.flyco.tablayout.SlidingTabLayout;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.wu1588.dancer.home.fragment.HistoryFragment;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.base.ViewPagerFragmentAdapter;
import cn.wu1588.dancer.common.event.MessageEvent;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;

public class HistoryRecordActivity extends LBaseActivity {


    @BindView(R.id.tabLayout)
    SlidingTabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.titleBar)
    TitleBar titleBar;
    private String[] mTitles = {"7日", "今日", "更早"};
    private List<Fragment> baseFragments;
    private ViewPagerFragmentAdapter mVPAdapter;
    private boolean isEdit = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_record);
        ButterKnife.bind(this);
        initViews();
        initListener();
    }

    private void initListener() {
        titleBar.setRightListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEdit) {
                    EventBus.getDefault().postSticky(new MessageEvent("isEdit",isEdit));
                    isEdit = false;
                    titleBar.setRightText("取消");
                } else {
                    EventBus.getDefault().postSticky(new MessageEvent("isEdit",isEdit));
                    isEdit = true;
                    titleBar.setRightText("编辑");

                }
            }
        });
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                EventBus.getDefault().postSticky(new MessageEvent("isEdit",false));
                isEdit = true;
                titleBar.setRightText("编辑");
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    private void initViews() {
        baseFragments = new ArrayList<>();
        baseFragments.add(HistoryFragment.newInstance("2"));//七日
        baseFragments.add(HistoryFragment.newInstance("1"));//今日
        baseFragments.add(HistoryFragment.newInstance("3"));//更早
        mVPAdapter = new ViewPagerFragmentAdapter(getSupportFragmentManager());
        mVPAdapter.setFragments(baseFragments);
        viewPager.setAdapter(mVPAdapter);
        viewPager.setCurrentItem(0, false);
        tabLayout.setViewPager(viewPager, mTitles);
    }
}
