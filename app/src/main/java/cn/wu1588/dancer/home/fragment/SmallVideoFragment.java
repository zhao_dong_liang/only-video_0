package cn.wu1588.dancer.home.fragment;

import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.sunrun.sunrunframwork.bean.BaseBean;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.adp.SmallVideoAdapter;
import cn.wu1588.dancer.home.mode.MovieBean;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.event.SmallVideoDataEvent;
import cn.wu1588.dancer.common.event.SmallVideoIndexEvent;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.DeviceIdUtil;
import cn.wu1588.dancer.common.util.LoginUtil;
import cn.wu1588.dancer.common.util.PageLimitDelegate;
import cn.wu1588.dancer.common.util.RecycleHelper;

//小视频
public class SmallVideoFragment extends LBaseFragment {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    private SmallVideoAdapter adapter;
    private boolean isPause;

    @Override
    public void onResume() {
        super.onResume();
        isPause=false;
    }
    @Override
    public void onPause() {
        super.onPause();
        isPause=true;
    }
    private PageLimitDelegate pageLimitDelegate = new PageLimitDelegate(page -> {
        Log.i("NetServer:","loadData=="+page);
        if (LoginUtil.startLogin(getActivity())){
            CommonIntent.startLoginActivity(getActivity());
        }else {
            String deviceId = DeviceIdUtil.getDeviceId(getActivity());
            BaseQuestStart.getSmallVideo(SmallVideoFragment.this,CommonApp.samllVideoId+"",page,deviceId);
        }

    });
    public static SmallVideoFragment newInstance(){
        SmallVideoFragment fragment=new SmallVideoFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initEventBus();
        initView();
    }

    private void initView(){
        RecycleHelper.setGridLayoutManager(recyclerView, 2);
        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                if (parent.getChildAdapterPosition(view) % 2 == 0) {
                    outRect.right = 1;
                }
                outRect.bottom = 1;
            }
        });
        adapter = new SmallVideoAdapter();
        pageLimitDelegate.attach(refreshLayout, recyclerView,adapter);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener((adapter, view, position) -> {
            List<MovieBean> data = SmallVideoFragment.this.adapter.getData();
            CommonIntent.startSmallVideoPlayerActivity(that,position, (ArrayList<MovieBean>) data);
        });
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        if (requestCode==BaseQuestStart.GET_CATE_SHOW_LIST_CODE){
            if (bean.status==1){
                pageLimitDelegate.setData(bean.Data());
                ArrayList<MovieBean> beans=bean.Data();
                if (beans!=null&&beans.size()!=0){
                    Log.i("NetServer:",beans.size()+"发送"+adapter.getData().size());
                    EventBus.getDefault().post(new SmallVideoDataEvent(adapter.getData()));//发送数据
                }else {
                    EventBus.getDefault().post(new SmallVideoDataEvent(null));//发送数据
                }

            }
        }
        super.nofityUpdate(requestCode, bean);
    }

    @Override
    public void onVisible() {
        super.onVisible();
        if(refreshLayout!=null){
            pageLimitDelegate.refreshPage();
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.small_video_fragment;
    }
// EventBus.getDefault().post(new MessageEvent("login_success","1"));
    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void eventBusMethod(SmallVideoIndexEvent event) {
        Log.i("NetServer:",event.getIndex()+"");
            if (recyclerView!=null){
                int index = event.getIndex();
                if (index==-1){
//                    刷新
                    pageLimitDelegate.refreshPage();
                }else {
                    if (index>=adapter.getData().size()-4){
                        BaseQuickAdapter.RequestLoadMoreListener loadMoreListener = pageLimitDelegate.getLoadMoreListener();
                        if (loadMoreListener!=null){//手动加载更多
                            loadMoreListener.onLoadMoreRequested();
                        }
                    }
                    recyclerView.scrollToPosition(event.getIndex());
                }
                MovieBean bean = event.getBean();
                if (bean!=null&&adapter!=null){
                    adapter.getData().set(index,bean);
                }

        }
    }
}