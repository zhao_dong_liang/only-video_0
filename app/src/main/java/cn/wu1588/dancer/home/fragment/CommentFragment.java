package cn.wu1588.dancer.home.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.lzy.okgo.model.Progress;
import com.my.toolslib.OneClickUtils;
import com.my.toolslib.http.utils.LzyResponse;
import com.my.toolslib.http.utils.NetRequestListenerProxy;
import com.my.toolslib.http.utils.OkHttpRequest;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uibase.BaseActivity;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;
import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.sunrun.sunrunframwork.utils.EmptyDeal;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.adp.MovieCommectAdapter;
import cn.wu1588.dancer.home.dialog.EditCommentDialog;
import cn.wu1588.dancer.home.mode.CommectNumBean;
import cn.wu1588.dancer.home.mode.MovieActionStateBean;
import cn.wu1588.dancer.home.mode.MovieBean;
import cn.wu1588.dancer.home.mode.MovieCommentBean;
import cn.wu1588.dancer.home.mode.MovieCommentSectionEntity;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.share.ShareBottomSheetDialog;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_MOVIE_ACTION_STATES_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.MOVIE_DIANZAN_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.MOVIE_SHOUCANG_CODE;

public class CommentFragment extends Fragment implements View.OnClickListener, OkHttpRequest, BaseQuickAdapter.OnItemChildClickListener {

    private View rootView;
    private RecyclerView recyclerView;
    private View close_view;
    private TextView commect_tv;
    private View commect_view;
    private TextView commect_num;
    private int commentNum;
    private LinearLayout commect_layout;
    private ImageButton ibCommentActionCollect;
    private ImageButton ibCommentActionLike;
    private ImageButton ibCommentActionSend;

    public MovieBean movieBean;
    public MovieCommectAdapter adapter;
    private boolean isShowActions;
    private boolean showCommentInput = true;
    private int commentHeight;
    private View.OnClickListener onCloseClickListener;
    //调试
    public void setMovieBean(MovieBean movieBean, boolean isShowActions) {
        this.movieBean = movieBean;
        if (movieBean!=null){
            try {
                commentNum=Integer.parseInt(movieBean.comment_num);
            } catch (Throwable ex) {
                ex.printStackTrace();
            }

        }
        this.isShowActions = isShowActions;
    }

    public void setMovieBeanHideCommentInput(MovieBean movieBean) {
        this.movieBean = movieBean;
        if (movieBean!=null){
            try {
                commentNum=Integer.parseInt(movieBean.comment_num);
            } catch (Throwable ex) {
                    ex.printStackTrace();
            }

        }
        showCommentInput = false;
    }

    public void setMovieBean(MovieBean movieBean) {
        setMovieBean(movieBean, false);
    }

    public void setCommentHeight(int commentHeight) {
        this.commentHeight = commentHeight;
    }

    public void setOnCloseClickListener(View.OnClickListener onCloseClickListener) {
        this.onCloseClickListener = onCloseClickListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView==null){
            rootView=inflater.inflate(R.layout.small_video_comment_fragment,container,false);
            initViews();
        }else {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            parent.removeView(rootView);
        }
        return rootView;
    }
    private int page=1;
    public void refreshPage(){
        page=1;
//        if (adapter!=null){
//            adapter.setEnableLoadMore(true);
//        }
        movieCommenBeans.clear();
        BaseQuestStart.getMovieComment(this,movieBean.id+"",page);
        commectSucces();

    }
    private void initViews(){
        commect_tv=rootView.findViewById(R.id.commect_tv);
        commect_tv.setOnClickListener(this);
        commect_view=rootView.findViewById(R.id.commect_view);
        commect_layout = rootView.findViewById(R.id.commect_layout);
        close_view=rootView.findViewById(R.id.close_view);
        ibCommentActionCollect = rootView.findViewById(R.id.ibCommentActionCollect);
        ibCommentActionCollect.setOnClickListener(v -> BaseQuestStart.movieShoucang(new NetRequestListenerProxy(getContext()) {
            @Override
            public void nofityUpdate(int i, BaseBean baseBean) {
                updateUI(i, baseBean);
            }
        }, movieBean.id + ""));
        ibCommentActionLike = rootView.findViewById(R.id.ibCommentActionLike);
        ibCommentActionLike.setOnClickListener(v -> BaseQuestStart.movieZan(new NetRequestListenerProxy(getContext()) {
            @Override
            public void nofityUpdate(int i, BaseBean baseBean) {
                updateUI(i, baseBean);
            }
        }, 1, movieBean.id + ""));
        ibCommentActionSend = rootView.findViewById(R.id.ibCommentActionSend);
        ibCommentActionSend.setOnClickListener(v -> {
            ShareBottomSheetDialog shareBottomSheetDialog= ShareBottomSheetDialog.newInstance(movieBean);
            shareBottomSheetDialog.show(getChildFragmentManager(), ShareBottomSheetDialog.TAG);
        });
        close_view.setOnClickListener(view -> {
            if (onCloseClickListener != null) {
                onCloseClickListener.onClick(view);
            }
        });
        commect_num=rootView.findViewById(R.id.commect_num);
        recyclerView=rootView.findViewById(R.id.recyclerView);
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(manager);

        if (commentHeight > 0) {
            ViewGroup.LayoutParams layoutParams = recyclerView.getLayoutParams();
            layoutParams.height = commentHeight;
            recyclerView.setLayoutParams(layoutParams);
        }

        BaseQuestStart.getMovieComment(this,movieBean.id+"",page);

        rootView.findViewById(R.id.llCommentActions).setVisibility(isShowActions ? View.VISIBLE : View.GONE);

        BaseQuestStart.getMoveieActionStates(new NetRequestListenerProxy(getContext()) {
            @Override
            public void nofityUpdate(int i, BaseBean baseBean) {
                updateUI(i, baseBean);
            }
        }, movieBean.id + "");//查看影片是否点赞或者收藏


        commect_layout.setVisibility(showCommentInput ? View.VISIBLE : View.GONE);
        close_view.setVisibility(showCommentInput ? View.VISIBLE : View.GONE);

    }

    public void sendComment() {
        EditCommentDialog dialog=new EditCommentDialog(getActivity(),commect_tv,commect_view,this);
        dialog.setMovieBean(movieBean);
        dialog.showDialog();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.commect_tv:
                EditCommentDialog dialog=new EditCommentDialog(getActivity(),commect_tv,commect_view,this);
                dialog.setMovieBean(movieBean);
                dialog.showDialog();
                break;


        }

    }


    private void commectSucces(){//评论成功
        commentNum++;
        commect_num.setText("全部评论("+commentNum+")");
        EventBus.getDefault().post(new CommectNumBean(commentNum));
    }
    @Override
    public void tokenInvalid() {

    }

    @Override
    public void httpLoadFail(String err) {
        ToastUtils.longToast(err);
    }

    @Override
    public void httpLoadFinal() {

    }
    private ArrayList<MovieCommentSectionEntity> movieCommenBeans=new ArrayList<>();
    @Override
    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
        if (requestCode==BaseQuestStart.GET_MOVIE_COMMENT_CODE){//获取评论列表
            if (response.code==1){
                page++;
                ArrayList<MovieCommentSectionEntity>   movieCommenBeans= (ArrayList<MovieCommentSectionEntity>) response.data;
                if (EmptyDeal.empty(movieCommenBeans)&&adapter!=null){
                    adapter.setEnableLoadMore(false);
                    return;
                }
                if (adapter==null){
                    this.movieCommenBeans=movieCommenBeans;
                    adapter=new MovieCommectAdapter(movieCommenBeans);
                    recyclerView.setAdapter(adapter);
                    adapter.expandAll();
                    adapter.setEnableLoadMore(true);
                    adapter.disableLoadMoreIfNotFullPage(recyclerView);
                    adapter.setOnItemChildClickListener(this);
                    adapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
                        @Override
                        public void onLoadMoreRequested() {
                            BaseQuestStart.getMovieComment(CommentFragment.this,movieBean.id+"",page);
                        }
                    }, recyclerView);
                }else {
                    this.movieCommenBeans.addAll(movieCommenBeans);
                    adapter.setNewData(this.movieCommenBeans);
                }
                adapter.notifyDataSetChanged();
            }else {
                adapter.setEnableLoadMore(false);
            }
        }else if (BaseQuestStart.COMMENTS_DIANZAN_CODE==requestCode){//点赞

            if (response.code==1){
                MovieCommentSectionEntity  entitie= (MovieCommentSectionEntity) adapter.getData().get(position);
                if (entitie==null){
                    return;
                }
                entitie.t.setIs_like(1);
                entitie.t.setLike_num( entitie.t.getLike_num()+1);
                if (view !=null&&view instanceof LinearLayout){
                    TextView like_num_tv=  view.findViewById(R.id.like_num);
                    like_num_tv.setText( entitie.t.getLike_num()+"");
                    View like_view=  view.findViewById(R.id.like_view);
                    like_view.setBackgroundResource(R.drawable.fabuloused_icon);
                }
                ToastUtils.longToast("点赞成功");
            }else {
                ToastUtils.longToast("点赞失败");
            }
        }else if (requestCode==BaseQuestStart.POST_COMMENTS_CODE){//评论成功
            if ("2".equals(response.msg)){//二级评论
                try {
                    JSONObject detailsJSONObject= (JSONObject) response.data;
                    MovieCommentBean child=new MovieCommentBean();
//                bean.setDetail_count(detail_count);
//                    child.setUid(detailsJSONObject.getString("uid"));
                    child.setNickname(Config.getLoginInfo().nickname);
                    child.setIcon(Config.getLoginInfo().icon);
                    child.setContent(detailsJSONObject.getString("content"));
                    child.setLike_num(detailsJSONObject.getInt("like_num"));
                    child.setAdd_time(detailsJSONObject.getString("add_time"));
                    child.setId(detailsJSONObject.getString("id"));
                    child.setPid(detailsJSONObject.getString("pid"));
                    child.setIs_like(0);
                    child.setReply_user(detailsJSONObject.getString("reply_user"));
                    List<MovieCommentSectionEntity> entities= adapter.getData();
                    for (int i = 0; i < entities.size(); i++) {
                        MovieCommentSectionEntity entity = entities.get(i);
                        if (entity.isHeader){
                            MovieCommentBean commentBean=entity.t;
                            if (commentBean.getId().equals(parentId)){
                                child.setParentId(parentId);
                                child.setDetail_count(commentBean.getDetail_count()+1);
                                entities.add(i+1,new MovieCommentSectionEntity(child));
                                adapter.notifyDataSetChanged();
                                break;
                            }
                        }

                    }
                }catch (Exception e){
                    Log.e("OkGo:",e.toString());
                    ToastUtils.longToast("二级评论失败");
                }

            }

            commectSucces();
        }
        else if (BaseQuestStart.GET_MOVIE_CHILD_COMMENT_CODE==requestCode){//加载更多二级评论
            List<MovieCommentSectionEntity> entities= (List<MovieCommentSectionEntity>) response.data;
            List<MovieCommentSectionEntity> data = adapter.getData();
            data.get(childPostion).t.setDetails_next_page(0);
            data.addAll(childPostion+1,entities);
            adapter.notifyDataSetChanged();
        }
        if (movieBean!=null){
            commect_num.setText("全部评论("+commentNum+")");
        }

    }

    @Override
    public void onProgress(Progress progress) {

    }

    @Override
    public void downLoadFinish(File file) {

    }

    private  int position;
    private String parentId="";
    private int childPostion;
    private MovieCommentSectionEntity entity; //点击的item
    @Override
    public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        if (OneClickUtils.isFastDoubleClick(view.getId())){return;}
        this.position=position;
        MovieCommentSectionEntity  entitie= (MovieCommentSectionEntity) adapter.getData().get(position);
        String mem_id="";
        String nickname="";
        int typeid=2;//  2=点一级评论进行评论（二级评论）  3=点一级评论下方的评论回复
        String uid="";
        entity=entitie;
        int is_like=0;
        MovieCommentBean bean=  entitie.t;
        if (entitie.isHeader){
            mem_id=bean.getId();
            nickname= bean.getNickname();
            uid=bean.getUid();
            is_like=bean.getIs_like();
            parentId=bean.getId();
        }else {
            typeid=3;
            parentId=bean.getParentId();
            mem_id=bean.getId();
            nickname= bean.getNickname();
            uid=bean.getUid();
            is_like=bean.getIs_like();
        }

        switch (view.getId()){
            case R.id.more_tv://展开更多
                childPostion=position;
                BaseQuestStart.getMovieChildComment(this,parentId,bean.getPage()+1);
                break;
            case R.id.like_linear:
                if (is_like==0){//未点赞
                    BaseQuestStart.CommentsDianzanOk(this,mem_id,view);
                }else {
                    ToastUtils.longToast("已点赞");
                }

                break;
            case R.id.layout://回复
                EditCommentDialog   dialog=new EditCommentDialog(getActivity(),commect_tv,commect_view,this);
                dialog.setHintText("回复 @"+nickname+"：",mem_id,typeid,uid);
                dialog.setMovieBean(movieBean);
                dialog.showDialog();
                break;
            case R.id.header_img://用户头像
                CommonIntent.startOtherUserActiivty((BaseActivity) getActivity(),uid);
                break;
        }
    }

    private void updateUI(int i, BaseBean baseBean) {
        switch (i) {
            case MOVIE_SHOUCANG_CODE:
            case MOVIE_DIANZAN_CODE://视频点赞
                BaseQuestStart.getMoveieActionStates(new NetRequestListenerProxy(getContext()) {
                    @Override
                    public void nofityUpdate(int i, BaseBean baseBean) {
                        updateUI(i, baseBean);
                    }
                }, movieBean.id + "");//查看影片是否点赞或者收藏
                if (baseBean.status != 1) {
                    UIUtils.shortM(baseBean.msg);
                }
                break;
            case GET_MOVIE_ACTION_STATES_CODE://查看影片是否点赞或者收藏
                if (baseBean.status == 1) {
                    MovieActionStateBean state = baseBean.Data();
                    if (!state.isAlowZan()) {
                        //ivDianZan.setEnabled(false);
                        //ivBuZan.setEnabled(false);
                    }
                    if (state.top == 1) {
                        ibCommentActionLike.setImageResource(R.drawable.icon_liked_oro);
                    } else if (state.top == 0) {
                        ibCommentActionLike.setImageResource(R.drawable.icon_comment_like);
                    }
                    if (state.isLike()) {
                        ibCommentActionCollect.setImageResource(R.drawable.icon_oro_collection);
                    } else {
                        ibCommentActionCollect.setImageResource(R.drawable.icon_comment_collect);
                    }
                }
                break;
        }

    }
}
