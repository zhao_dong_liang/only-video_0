package cn.wu1588.dancer.home.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.makeramen.roundedimageview.RoundedImageView;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.TextColorUtils;
import com.sunrun.sunrunframwork.uiutils.UIUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.adp.CommmentsDetailAdapter;
import cn.wu1588.dancer.home.mode.CommentDetailBean;
import cn.wu1588.dancer.base.LBaseDialogFragment;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.event.MessageEvent;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.common.util.RecycleHelper;

import static android.view.WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE;
import static android.view.WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.COMMENTS_DIANZAN_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_COMMENTS_DETAIL_CODE;

/**
 * 影片播放页点击评论弹起评论详情
 */
public class CommentsDetailDialogFragment extends LBaseDialogFragment {

    @BindView(R.id.ivClose)
    ImageView ivClose;
    @BindView(R.id.rIvStar)
    RoundedImageView rIvStar;
    @BindView(R.id.tvNikeName)
    TextView tvNikeName;
    @BindView(R.id.tvTime)
    TextView tvTime;
    @BindView(R.id.tvZanNum)
    TextView tvZanNum;
    @BindView(R.id.tvContent)
    TextView tvContent;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.rUserStar)
    RoundedImageView rUserStar;
    @BindView(R.id.llComments)
    LinearLayout llComments;
    public String mem_id;
    public String noticeid;
    private int height;
    private CommmentsDetailAdapter commmentsDetailAdapter;
    private String movieId;
    private String pid;
    private String nickname;
    private int status;

    public static CommentsDetailDialogFragment newInstance() {
        CommentsDetailDialogFragment commentsDetailDialogFragment = new CommentsDetailDialogFragment();
        Bundle bundle = new Bundle();
        commentsDetailDialogFragment.setArguments(bundle);
        return commentsDetailDialogFragment;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.dialog_comments_detail;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initEventBus();
        initViews();
        initData();
    }

    private void initViews() {
        GlideMediaLoader.loadHead(that, rUserStar, Config.getLoginInfo().icon);
        RecycleHelper.setLinearLayoutManager(recyclerView, LinearLayoutManager.VERTICAL);
        commmentsDetailAdapter = new CommmentsDetailAdapter();
        recyclerView.setAdapter(commmentsDetailAdapter);

        commmentsDetailAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                CommentDetailBean.ChildBean item = commmentsDetailAdapter.getItem(position);
                switch (view.getId()) {
                    case R.id.llContainer:
                        PostCommentsDialogFragment.newInstance()
                                .setHitText(item.nickname)
                                .setMovieId(movieId)
                                .setTypeid("3")
                                .setUserId(item.me_id)
                                .setPid(pid)
                                .setPlayerHeight(height)
                                .show(getFragmentManager(), "PostCommentsDialogFragment");
                        break;
                    case R.id.tvZanNum:
                        if (item.status == 0) {
                            BaseQuestStart.CommentsDianzan(CommentsDetailDialogFragment.this, item.mem_id);
                        }
                        break;
                }
            }
        });
    }

    private void initData() {
        UIUtils.showLoadDialog(that);
        BaseQuestStart.getCommentsDetail(CommentsDetailDialogFragment.this, mem_id, noticeid);
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode) {
            case GET_COMMENTS_DETAIL_CODE:
                if (bean.status == 1) {
                    CommentDetailBean commentDetailBean = bean.Data();
                    updateUI(commentDetailBean);

                }
                break;
            case COMMENTS_DIANZAN_CODE:
                if (bean.status == 1) {
                    EventBus.getDefault().post(new MessageEvent("refresh_data"));
                } else {
                    UIUtils.shortM(bean.msg);
                }
                break;
        }
        super.nofityUpdate(requestCode, bean);
    }

    private void updateUI(CommentDetailBean commentDetailBean) {
        pid = commentDetailBean.mem_id;
        nickname = commentDetailBean.nickname;
        status = commentDetailBean.status;
        GlideMediaLoader.loadHead(that, rIvStar, commentDetailBean.icon);
        tvNikeName.setText(commentDetailBean.nickname);
        tvZanNum.setText(commentDetailBean.like_num);
        tvContent.setText(commentDetailBean.content);
        commmentsDetailAdapter.setNewData(commentDetailBean.child);
        if (commentDetailBean.status == 1) {
            TextColorUtils.setCompoundDrawables(tvZanNum,null, null, that.getResources().getDrawable(R.drawable.pinglun_btn_zan_selected), null);
        } else {
            TextColorUtils.setCompoundDrawables(tvZanNum,null, null, that.getResources().getDrawable(R.drawable.pinglun_btn_zan_normal), null);
        }

    }

    public CommentsDetailDialogFragment setPlayerHeight(int height) {
        this.height = height;
        return this;
    }

    public CommentsDetailDialogFragment setMemID(String mem_id) {
        this.mem_id = mem_id;
        return this;
    }

    @OnClick({R.id.ivClose, R.id.llComments, R.id.tvZanNum})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivClose:
                dismissAllowingStateLoss();
                break;
            case R.id.llComments:
                PostCommentsDialogFragment.newInstance().setHitText(nickname).setMovieId(movieId).setTypeid("2").setUserId(pid).setPid(pid).setPlayerHeight(height).show(getFragmentManager(), "PostCommentsDialogFragment");
                break;
            case R.id.tvZanNum:
                if (status == 0) {
                    BaseQuestStart.CommentsDianzan(CommentsDetailDialogFragment.this, pid);
                }
                break;
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = getDialog();
        Window dialogWindow = dialog.getWindow();
        applyCompat(dialogWindow);
        dialogWindow.setSoftInputMode(SOFT_INPUT_STATE_ALWAYS_HIDDEN | SOFT_INPUT_ADJUST_RESIZE);
        //设置对话框从底部进入
        dialogWindow.setWindowAnimations(R.style.bottomInWindowAnim);
        WindowManager.LayoutParams p = dialogWindow.getAttributes();
        p.width = ViewGroup.LayoutParams.MATCH_PARENT;
        p.height = ViewGroup.LayoutParams.MATCH_PARENT;//高度自己设定
        dialogWindow.setAttributes(p);
        dialogWindow.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        dialogWindow.setGravity(Gravity.BOTTOM);
        dialog.setCancelable(true);

        dialog.setCanceledOnTouchOutside(true);
        //修复状态栏变黑的问题
        int screenHeight = getScreenHeight(getActivity());
        int statusBarHeight = getStatusBarHeight(getContext());
        int dialogHeight = screenHeight - height;//screenHeight - statusBarHeight;
        dialogWindow.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, dialogHeight == 0 ? ViewGroup.LayoutParams.MATCH_PARENT : dialogHeight);
        return dialog;

    }

    public Dialog getDialog() {
        return new Dialog(getActivity(), R.style.NTitleDialog);
    }

    private void applyCompat(Window dialogWindow) {
        if (Build.VERSION.SDK_INT <= 19) {
            return;
        }
        dialogWindow.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialogWindow.setStatusBarColor(Color.TRANSPARENT);
        }

    }

    public CommentsDetailDialogFragment setMovieId(String movieId) {
        this.movieId = movieId;
        return this;
    }

    @Subscribe
    public void eventBusMethod(MessageEvent event) {
        String type = event.getType();
        if (TextUtils.equals("refresh_data", type)) {
            initData();
        }
    }
}
