package cn.wu1588.dancer.home.mode;

import java.util.List;

public class CommentDetailBean {

    /**
     * mem_id : 122
     * content : 留言给我，腹肌颜值
     * like_num : 3
     * pid : 0
     * reply_id : 0
     * add_time : 1天前
     * me_id : 56
     * nickname : 广州***
     * icon : www.code.cn/duboshiping_app/Uploads/Avatar/2019-04-02/5ca2b3d15a5e2.jpg
     * gender : 2
     * child : [{"like_num":"0","content":"回复兔子**:我脑壳好疼啊","reply_id":"57","add_time":"56分钟前","me_id":"61","icon":"www.code.cn/duboshiping_app/22222222222","nickname":"131***360","gender":"1","userid":"57"},{"like_num":"0","content":"我脑壳好疼啊","reply_id":"0","add_time":"57分钟前","me_id":"57","icon":"www.code.cn/duboshiping_app/22222222222","nickname":"131***360","gender":"1","userid":"57"},{"like_num":"0","content":"回复134***155:还不能发表情，靠","reply_id":"57","add_time":"7小时前","me_id":"62","icon":"www.code.cn/duboshiping_app/22222222222","nickname":"131***360","gender":"1","userid":"57"},{"like_num":"0","content":"哈哈哈","reply_id":"0","add_time":"1天前","me_id":"57","icon":"www.code.cn/duboshiping_app/22222222222","nickname":"131***360","gender":"1","userid":"57"},{"like_num":"0","content":"回复兔子**:a136514a","reply_id":"62","add_time":"4天前","me_id":"61","icon":"www.code.cn/duboshiping_app/Uploads/Avatar/2019-04-02/5ca2c9f50fb3f.jpg","nickname":"134***155","gender":"1","userid":"62"}]
     */

    public String mem_id;
    public String content;
    public String like_num;
    public String pid;
    public String reply_id;
    public String add_time;
    public String me_id;
    public String nickname;
    public String icon;
    public String gender;
    public int status;
    public List<ChildBean> child;



    public static class ChildBean {
        /**
         * like_num : 0
         * content : 回复兔子**:我脑壳好疼啊
         * reply_id : 57
         * add_time : 56分钟前
         * me_id : 61
         * icon : www.code.cn/duboshiping_app/22222222222
         * nickname : 131***360
         * gender : 1
         * userid : 57
         */

        public String like_num;
        public String content;
        public String reply_id;
        public String add_time;
        public String me_id;
        public String icon;
        public String nickname;
        public String gender;
        public String userid;
        public String mem_id;
        public int status;
    }
}
