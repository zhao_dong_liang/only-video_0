package cn.wu1588.dancer.home.mode;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import cn.wu1588.dancer.mine.mode.BuyAlbumBean;

public class AlumBean implements Parcelable  {
    public List<BuyAlbumBean> recommend_teaching_channels;
    public List<BuyAlbumBean> hot_teaching_channels;
    public List<BuyAlbumBean> new_teaching_channels;

    protected AlumBean(Parcel in) {
        recommend_teaching_channels = in.createTypedArrayList(BuyAlbumBean.CREATOR);
        hot_teaching_channels = in.createTypedArrayList(BuyAlbumBean.CREATOR);
        new_teaching_channels = in.createTypedArrayList(BuyAlbumBean.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(recommend_teaching_channels);
        dest.writeTypedList(hot_teaching_channels);
        dest.writeTypedList(new_teaching_channels);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AlumBean> CREATOR = new Creator<AlumBean>() {
        @Override
        public AlumBean createFromParcel(Parcel in) {
            return new AlumBean(in);
        }

        @Override
        public AlumBean[] newArray(int size) {
            return new AlumBean[size];
        }
    };
}
