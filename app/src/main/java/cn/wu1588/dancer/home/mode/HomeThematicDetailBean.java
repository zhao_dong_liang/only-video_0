package cn.wu1588.dancer.home.mode;

import java.util.List;

public class HomeThematicDetailBean {

    /**
     * banner : {"background_image":"Uploads/Movie/5cab07f7794b1.png","content":"听不懂日文？加上字幕能看懂了吧","logo":"Uploads/Movie/5cab07f0a4fba.png","title":"中文字幕"}
     * list : [{"grade":"8.1","id":"12","image":"https://test.cnsunrun.com/duboshiping_app/Uploads/Movie/5caabda17ca56.png","label":"2,3","label_str":[{"add_time":"2019-03-29 17:25:10","id":"2","is_del":"0","is_hid":"0","pid":"0","sort":"0","title":"家庭剧","update_time":"2019-03-28 11:53:10"},{"add_time":"2019-03-29 17:25:13","id":"3","is_del":"0","is_hid":"0","pid":"0","sort":"0","title":"简体中文","update_time":"2019-03-28 11:53:48"}],"play_num":"0","play_url":"http://shuchujzxia.com:5999/20190409/DTTbr4pJ/index.m3u8","title":"我要测试视频"},{"grade":"8.1","id":"19","image":"https://test.cnsunrun.com/duboshiping_app/Uploads/Movie/5cab17ca15b83.jpg","label":"2,3","label_str":[{"add_time":"2019-03-29 17:25:10","id":"2","is_del":"0","is_hid":"0","pid":"0","sort":"0","title":"家庭剧","update_time":"2019-03-28 11:53:10"},{"add_time":"2019-03-29 17:25:13","id":"3","is_del":"0","is_hid":"0","pid":"0","sort":"0","title":"简体中文","update_time":"2019-03-28 11:53:48"}],"play_num":"0","play_url":"http://shuchujzxia.com:5818/20190331/YENabV5E/index.m3u8","title":"森奈奈子--和嫂子同居的日子"},{"grade":"8.1","id":"32","image":"https://test.cnsunrun.com/duboshiping_app/Uploads/Movie/5cac634634a4a.jpg","label":"2","label_str":[{"add_time":"2019-03-29 17:25:10","id":"2","is_del":"0","is_hid":"0","pid":"0","sort":"0","title":"家庭剧","update_time":"2019-03-28 11:53:10"}],"play_num":"0","play_url":"http://shuchujzxia.com:5999/20190409/DTTbr4pJ/index.m3u8","title":"11111111111111"},{"grade":"8.1","id":"33","image":"https://test.cnsunrun.com/duboshiping_app/Uploads/Movie/5cac644518218.jpg","label":"1","label_str":[{"add_time":"2019-03-29 17:25:02","id":"1","is_del":"0","is_hid":"0","pid":"0","sort":"0","title":"热门","update_time":"2019-03-28 11:52:54"}],"play_num":"0","play_url":"http://shuchujzxia.com:5999/20190409/DTTbr4pJ/index.m3u8","title":"22222222222"}]
     */

    public BannerBean banner;
    public List<ListBean> list;



    public static class BannerBean {
        /**
         * background_image : Uploads/Movie/5cab07f7794b1.png
         * content : 听不懂日文？加上字幕能看懂了吧
         * logo : Uploads/Movie/5cab07f0a4fba.png
         * title : 中文字幕
         */

        public String background_image;
        public String content;
        public String logo;
        public String title;


    }

    public static class ListBean {
        /**
         * grade : 8.1
         * id : 12
         * image : https://test.cnsunrun.com/duboshiping_app/Uploads/Movie/5caabda17ca56.png
         * label : 2,3
         * label_str : [{"add_time":"2019-03-29 17:25:10","id":"2","is_del":"0","is_hid":"0","pid":"0","sort":"0","title":"家庭剧","update_time":"2019-03-28 11:53:10"},{"add_time":"2019-03-29 17:25:13","id":"3","is_del":"0","is_hid":"0","pid":"0","sort":"0","title":"简体中文","update_time":"2019-03-28 11:53:48"}]
         * play_num : 0
         * play_url : http://shuchujzxia.com:5999/20190409/DTTbr4pJ/index.m3u8
         * title : 我要测试视频
         */

        public String grade;
        public String id;
        public String image;
        public String label;
        public String play_num;
        public String play_url;
        public String title;
        public List<LabelStrBean> label_str;



        public static class LabelStrBean {
            /**
             * add_time : 2019-03-29 17:25:10
             * id : 2
             * is_del : 0
             * is_hid : 0
             * pid : 0
             * sort : 0
             * title : 家庭剧
             * update_time : 2019-03-28 11:53:10
             */

            public String add_time;
            public String id;
            public String is_del;
            public String is_hid;
            public String pid;
            public String sort;
            public String title;
            public String update_time;

        }
    }
}
