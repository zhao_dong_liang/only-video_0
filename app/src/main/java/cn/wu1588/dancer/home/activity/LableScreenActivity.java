package cn.wu1588.dancer.home.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.cnsunrun.commonui.widget.titlebar.TitleBar;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.utils.EmptyDeal;

import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import cn.wu1588.dancer.adp.SearchResultAdapter;
import cn.wu1588.dancer.home.mode.MovieBean;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.GetEmptyViewUtils;
import cn.wu1588.dancer.common.util.PageLimitDelegate;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_lable_SEARCH_CODE;

/**
 * 标签筛选
 */
public class LableScreenActivity extends LBaseActivity {

    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.tvlableText)
    TextView tvlableText;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    private String keyWords;
    private SearchResultAdapter searchResultAdapter;
    PageLimitDelegate pageLimitDelegate=new PageLimitDelegate(new PageLimitDelegate.DataProvider() {
        @Override
        public void loadData(int page) {
            BaseQuestStart.getLableSearchList(that,keyWords,page);
        }
    });
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lable_screen);
        ButterKnife.bind(this);
        keyWords = getIntent().getStringExtra("lableText");
        initViews();
    }

    private void initViews() {
        tvlableText.setText(keyWords);
        refreshLayout.setColorSchemeColors(getResources().getColor(R.color.main_button_color));
        searchResultAdapter = new SearchResultAdapter();
        recyclerView.setAdapter(searchResultAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(that));
        pageLimitDelegate.attach(refreshLayout,recyclerView, searchResultAdapter);
        searchResultAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                MovieBean item = searchResultAdapter.getItem(position);
                CommonIntent.startMoviePlayerActivity(that,String.valueOf(item.id));
            }
        });
    }
    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode){
            case GET_lable_SEARCH_CODE:
                if (bean.status==1){
                    List<MovieBean> list=bean.Data();
                    pageLimitDelegate.setData(list);
                    if (EmptyDeal.isEmpy(list)){
                        GetEmptyViewUtils.bindEmptyView(that,searchResultAdapter,R.drawable.ic_empty_message,"暂无数据",true);
                    }
                }
                break;
        }
        super.nofityUpdate(requestCode, bean);
    }
}
