package cn.wu1588.dancer.home.mode;

import android.text.TextUtils;

import com.google.gson.reflect.TypeToken;
import com.sunrun.sunrunframwork.http.cache.NetSession;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cnsunrun on 2018-04-08.
 * 搜索的历史记录
 */

public class HistoryMode {
    private NetSession session;
    private List<HistoryBean> searchHistorys;
    private String KEY=HistoryMode.class.getName();
    public HistoryMode(NetSession session) {
        this.session = session;
        searchHistorys=session.getBean(KEY,new TypeToken<List<HistoryBean>>(){});
        if(searchHistorys==null){
            searchHistorys=new ArrayList<>();
        }
    }
    public void addHistory(String keywords, String searchType){
        if(TextUtils.isEmpty(keywords))return;
        HistoryBean searchHistoryBean=new HistoryBean();
        searchHistoryBean.keywords=keywords;
        searchHistoryBean.seachType=searchType;
        searchHistorys.remove(searchHistoryBean);//移除重复项
        searchHistorys.add(0,searchHistoryBean);
//        if(searchHistorys.size()>6){
//            searchHistorys.remove(searchHistorys.size()-1);
//        }
    }
    public void clearHistory(int position){
        searchHistorys.remove(position);
    }
    public void clearAllHistory(){
        searchHistorys.clear();
    }

    public List<HistoryBean> getSearchHistorys(){
        return searchHistorys;
    }

    public void save(){
        session.put(KEY,searchHistorys);
    }
}
