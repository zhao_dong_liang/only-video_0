package cn.wu1588.dancer.home.fragment;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.makeramen.roundedimageview.RoundedImageView;
import com.my.toolslib.DateUtil;
import com.my.toolslib.http.utils.LzyResponse;
import com.my.toolslib.http.utils.OkHttpRequestListener;
import com.stx.xhb.xbanner.XBanner;
import com.sunrun.sunrunframwork.uibase.BaseActivity;
import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.sunrun.sunrunframwork.utils.EmptyDeal;
import com.tencent.mmkv.MMKV;

import java.text.DecimalFormat;
import java.util.List;

import butterknife.BindView;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.home.mode.AlumBean;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.model.SystemParams;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;
import cn.wu1588.dancer.mine.mode.BuyAlbumBean;
import cn.wu1588.dancer.my_video.bean.MyVideoBean;

public class AlumFragment extends LBaseFragment implements RadioGroup.OnCheckedChangeListener, SwipeRefreshLayout.OnRefreshListener {
    private View mHeadView;
    private XBanner xbanner;
    private RadioGroup tab_rg;
    @BindView(R.id.title_bar)
    TitleBar title_bar;
    @BindView(R.id.rv_main)
    RecyclerView rv_main;
    @BindView(R.id.srf_refresh_layout)
    SwipeRefreshLayout srf_refresh_layout;
    private BaseQuickAdapter<BuyAlbumBean, BaseViewHolder> mAdapter;
    // 0 热门 1 最新
    private int currentChecked = 0;

    private DecimalFormat decimalFormat = new DecimalFormat("#.00");

    public static AlumFragment newInstance() {
        Bundle args = new Bundle();
        AlumFragment fragment = new AlumFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public int getLayoutRes() {
        return R.layout.alum_fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        srf_refresh_layout.setOnRefreshListener(this);
        mHeadView = LayoutInflater.from(getActivity()).inflate(R.layout.head_ablum_layout, rv_main, false);
        mAdapter = new BaseQuickAdapter<BuyAlbumBean, BaseViewHolder>(R.layout.item_ablum) {
            @Override
            protected void convert(BaseViewHolder helper, BuyAlbumBean item) {
                BaseQuestStart.checkUserAttention(new OkHttpRequestListener() {
                    @Override
                    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
                        if (response.code == 1) {
                            helper.setText(R.id.btn_movie_subscription, "已关注");
                            item.followMessage = "已关注";
                        } else {
                            helper.setText(R.id.btn_movie_subscription, "关注");
                            item.followMessage = "关注";
                        }
                    }
                }, item.user_info.id);
                helper.setText(R.id.tv_alum_title, item.title);
                helper.setText(R.id.tv_video_author_name, item.user_info.nickname);
                GlideMediaLoader.loadHead(getActivity(), helper.getView(R.id.iv_video_author_avatar), item.user_info.icon);
                helper.setText(R.id.tv_movies_count, String.format("共%s个视频", item.movies_count));
                helper.setText(R.id.tv_movies_price, String.format("¥ %s元", item.price));
                MMKV mmkv = MMKV.defaultMMKV();
                String vipDiscount = mmkv.decodeString(SystemParams.VIP_DISCOUNT);
                try {
                    float vipDiscountFloat = Float.parseFloat(vipDiscount);
                    float priceFloat = Float.parseFloat(item.price);
                    helper.setText(R.id.tv_movies_vip_discount, String.format("会员(%s折):", vipDiscount.replace("0.", "")));
                    helper.setText(R.id.tv_movies_discount_price, String.format("¥ %s元", decimalFormat.format(priceFloat * vipDiscountFloat)));
                    helper.setVisible(R.id.tv_movies_vip_discount, true);
                    helper.setVisible(R.id.tv_movies_discount_price, true);
                } catch (Exception r) {
                    helper.setVisible(R.id.tv_movies_vip_discount, false);
                    helper.setVisible(R.id.tv_movies_discount_price, false);
                }
                helper.setText(R.id.tv_movies_desc, String.format("专辑介绍:%s", item.content));
                helper.getView(R.id.btn_movies_buy).setOnClickListener(v -> CommonIntent.startAlumDetailsOrderPayActivity(getActivity(), item));
                helper.itemView.setOnClickListener(v -> CommonIntent.startAlumDetailsActivity(getActivity(), item));

                helper.getView(R.id.rl_author_layout).setOnClickListener(v -> CommonIntent.startOtherUserActiivty((BaseActivity) getActivity(), item.user_info.id));

                helper.getView(R.id.btn_movie_subscription).setOnClickListener(v -> {
                    if ("已关注".equals(item.followMessage)) {

                        BaseQuestStart.cancelAttention(new OkHttpRequestListener() {
                            @Override
                            public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
                                UIUtils.shortM("成功取消关注");
                                helper.setText(R.id.btn_movie_subscription, "关注");
                                item.followMessage = "关注";
                            }
                        }, item.user_info.id);

                    } else {
                        BaseQuestStart.followUser(new OkHttpRequestListener() {
                            @Override
                            public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
                                UIUtils.shortM("成功增加关注");
                                helper.setText(R.id.btn_movie_subscription, "已关注");
                                item.followMessage = "已关注";
                            }
                        }, item.user_info.id);
                    }

                });

                helper.setText(R.id.btn_movie_subscription, item.followMessage);

                GridLayout authorWorks = helper.getView(R.id.tv_movies_author_works);
                authorWorks.removeAllViews();

                List<MyVideoBean> movies = item.movies;
                if (movies != null && !movies.isEmpty()) {
                    for (int index = 0; index < movies.size(); index++) {
                        MyVideoBean movie = movies.get(index);
                        View works = LayoutInflater.from(getActivity()).inflate(R.layout.layout_alum_author_works, authorWorks, false);
                        ImageView movieImage = works.findViewById(R.id.iv_movie_img);
                        TextView tv_movie_duration = works.findViewById(R.id.tv_movie_duration);
                        TextView tv_movie_title = works.findViewById(R.id.tv_movie_title);

                        GlideMediaLoader.load(getActivity(), movieImage, movie.getImage(), R.drawable.ic_place_threee_hodel);
                        tv_movie_duration.setText(DateUtil.getDateToString(movie.getPlay_time() * 1000, "mm:ss"));
                        tv_movie_title.setText(movie.getTitle());
                        authorWorks.addView(works);
                        final int ep = index;
                        works.setOnClickListener(v -> CommonIntent.startAlumDetailsPlayerActivity(v.getContext(), item, movie.getId() + "", ep));
                    }
                }
            }
        };
        mAdapter.addHeaderView(mHeadView);
        rv_main.setAdapter(mAdapter);

        tab_rg = mHeadView.findViewById(R.id.tab_rg);
        xbanner = mHeadView.findViewById(R.id.xbanner);
        BaseQuestStart.getTeachingChannel(this);
        title_bar.setTitleBarBackgroundDrawable(CommonApp.getApplication().getStateBarDrawable());
        MMKV mmkv = MMKV.defaultMMKV();
        int[] colors = {Color.parseColor(mmkv.decodeString("up_b_color","#000000")),
                Color.parseColor(mmkv.decodeString("up_e_color","#000000"))
        };
        GradientDrawable drawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT,colors);
        drawable.setGradientType(GradientDrawable.LINEAR_GRADIENT);//设置线性渐变
        drawable.setOrientation(GradientDrawable.Orientation.LEFT_RIGHT);//设置渐变方向
        mHeadView.findViewById(R.id.view_bg).setBackground(drawable);

        tab_rg.setOnCheckedChangeListener(this);
    }

    private AlumBean bean;
    @Override
    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
        super.nofityUpdateUi(requestCode, response, view);
        if (srf_refresh_layout != null){
            srf_refresh_layout.setRefreshing(false);
        }

         bean= (AlumBean) response.data;
        //轮播图
        if (!EmptyDeal.isEmpy(bean.recommend_teaching_channels)) {
            //刷新数据之后，需要重新设置是否支持自动轮播
            xbanner.setAutoPlayAble(bean.recommend_teaching_channels.size() > 1);
            xbanner.setData(R.layout.home_banner_view, bean.recommend_teaching_channels, null);
            xbanner.loadImage(new XBanner.XBannerAdapter() {
                @Override
                public void loadBanner(XBanner banner, Object model, View view, int position) {
                    RoundedImageView roundedImageView = (RoundedImageView) view.findViewById(R.id.rIvBanner);
                    GlideMediaLoader.load(that, roundedImageView, bean.recommend_teaching_channels.get(position).background_image, R.mipmap.img_def);
                }
            });

            xbanner.setOnItemClickListener((banner, model, view1, position) -> {
                BuyAlbumBean bean = AlumFragment.this.bean.recommend_teaching_channels.get(position);
                CommonIntent.startAlumDetailsActivity(getActivity(), bean);
            });
        }

        switch (currentChecked) {
            default:
            case 0:
                mAdapter.setNewData(bean.hot_teaching_channels);
                break;
            case 1:
                mAdapter.setNewData(bean.new_teaching_channels);
                break;
        }


    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (i){
            default:
            case R.id.hot_rb://最热
                currentChecked = 0;
                break;
            case R.id.new_rb://最新
                currentChecked = 1;
                break;
        }
        BaseQuestStart.getTeachingChannel(this);
    }

    @Override
    public void onRefresh() {
        if (srf_refresh_layout!= null){
            srf_refresh_layout.setRefreshing(true);
        }
        BaseQuestStart.getTeachingChannel(this);
    }
}
