package cn.wu1588.dancer.home.mode;

import android.os.Parcel;
import android.os.Parcelable;

public class MovieHisBean  implements Parcelable {
    //"me_id": "84",
    //		"mo_id": "12",
    //		"title": "\u6211\u8981\u6d4b\u8bd5\u89c6\u9891",
    //		"image": "https:\/\/test.cnsunrun.com\/duboshiping_app\/http:\/\/juzishipinpic.com:456\/images\/bulunluanlun\/%E4%B8%8D%E8%AE%BA%E4%B9%B1%E8%BD%AE-14.jpg",
    //		"play_url": "http:\/\/shuchujzxia.com:5999\/20190411\/KGByaGs7\/index.m3u8",
    //		"play_time": "1200"
    public String mem_id;
    public int mo_id;
    public String title;
    public String play_url;
    public String play_time;
    public String image;
    public String add_time;
    @Override
    public int hashCode() {
        return mo_id;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof MovieHisBean){
            return  hashCode()==obj.hashCode();
        }
        return super.equals(obj);
    }
    @Override
    public String toString() {
        return mem_id ;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(mo_id);
        parcel.writeString(mem_id);
        parcel.writeString(title);
        parcel.writeString(play_url);
        parcel.writeString(play_time);
        parcel.writeString(image);
        parcel.writeString(add_time);
    }
}
