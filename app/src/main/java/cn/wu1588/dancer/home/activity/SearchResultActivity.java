package cn.wu1588.dancer.home.activity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;
import com.sunrun.sunrunframwork.utils.EmptyDeal;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.adp.SearchResultAdapter;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.EditTextHelper;
import cn.wu1588.dancer.common.util.GetEmptyViewUtils;
import cn.wu1588.dancer.common.util.PageLimitDelegate;
import cn.wu1588.dancer.common.widget.titlebar.TabTitleBar;
import cn.wu1588.dancer.home.mode.MovieBean;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_SEARCH_CODE;

/**
 * 作者：ninetailedfox
 * 时间：2021/1/14 2:27 PM
 * 邮箱: 1037438704@qq.com
 * 功能：搜索页面
 **/
public class SearchResultActivity extends LBaseActivity {

    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.ivHuiTui)
    ImageView ivHuiTui;
    @BindView(R.id.tvCancel)
    TextView tvCancel;
    @BindView(R.id.titleBar)
    TabTitleBar titleBar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    private SearchResultAdapter searchResultAdapter;
    private String keyWords;
    PageLimitDelegate pageLimitDelegate = new PageLimitDelegate(new PageLimitDelegate.DataProvider() {
        @Override
        public void loadData(int page) {
            BaseQuestStart.getSearchList(that, keyWords, page);
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        ButterKnife.bind(this);
        keyWords = getIntent().getStringExtra("keyWords");
        initViews();
        initEvent();
    }

    private void initEvent() {
        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    keyWords = etSearch.getText().toString();
                    if (!EmptyDeal.isEmpy(keyWords)) {
                        pageLimitDelegate.refreshPage();
                    }
                    return true;
                }
                return false;
            }
        });
        searchResultAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                MovieBean item = searchResultAdapter.getItem(position);
                CommonIntent.startMoviePlayerActivity(that, String.valueOf(item.id));
            }
        });
        //关键字搜索
//        http://app.myylook.com/Appapi/Home/home/hot_search
    }

    private void initViews() {
        etSearch.setText(keyWords);
        EditTextHelper.setTextAndSelectEnd(etSearch, keyWords);
        refreshLayout.setColorSchemeColors(getResources().getColor(R.color.main_button_color));
        searchResultAdapter = new SearchResultAdapter();
        recyclerView.setAdapter(searchResultAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(that));
        pageLimitDelegate.attach(refreshLayout, recyclerView, searchResultAdapter);


//        FlexboxLayoutManager flexboxLayoutManager = new FlexboxLayoutManager(this);
//        //flexDirection 属性决定主轴的方向（即项目的排列方向）。类似 LinearLayout 的 vertical 和 horizontal。
//        flexboxLayoutManager.setFlexDirection(FlexDirection.ROW);//主轴为水平方向，起点在左端。
//        //flexWrap 默认情况下 Flex 跟 LinearLayout 一样，都是不带换行排列的，但是flexWrap属性可以支持换行排列。
//        flexboxLayoutManager.setFlexWrap(FlexWrap.WRAP);//按正常方向换行
//        //justifyContent 属性定义了项目在主轴上的对齐方式。
//        flexboxLayoutManager.setJustifyContent(JustifyContent.FLEX_START);//交叉轴的起点对齐。
//        rv_history.setLayoutManager(flexboxLayoutManager);
//
//        searchHotAdp = new SearchHotAdp(R.layout.item_search_tag);
//        rv_history.setAdapter(searchHotAdp);

    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode) {
            case GET_SEARCH_CODE:
                if (bean.status == 1) {
                    List<MovieBean> list = bean.Data();
                    pageLimitDelegate.setData(list);
                    if (EmptyDeal.isEmpy(list)) {
                        GetEmptyViewUtils.bindEmptyView(that, searchResultAdapter, R.drawable.ic_empty_message, "暂无数据", true);
                    }
                }
                break;
        }
        super.nofityUpdate(requestCode, bean);
    }

    @OnClick({R.id.ivHuiTui, R.id.tvCancel, R.id.image_finish})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivHuiTui:
                finish();
                break;
            case R.id.tvCancel:
                if (etSearch.getText().toString() == null) {
                    ToastUtils.shortToast("搜索内容不能为空！");
                    return;
                }
                keyWords = etSearch.getText().toString();
                if (!EmptyDeal.isEmpy(keyWords)) {
                    pageLimitDelegate.refreshPage();
                }
                break;
            case R.id.image_finish:
                CommonApp.getInstance().closeActivitys(SearchActivity.class, SearchResultActivity.class);
                break;
        }
    }

}
