package cn.wu1588.dancer.home.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import cn.wu1588.dancer.R;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.home.mode.MovieDetailBean;
import cn.wu1588.dancer.base.LBaseDialogFragment;

import static android.view.WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE;
import static android.view.WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN;

/**
 * 简介
 */
public class JianJieDialogFragment extends LBaseDialogFragment {

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivClose)
    ImageView ivClose;
    @BindView(R.id.tvPlayNum)
    TextView tvPlayNum;
    @BindView(R.id.tagFlwLayout)
    TagFlowLayout tagFlwLayout;
    @BindView(R.id.tvJianjieContent)
    TextView tvJianjieContent;
    private int height;
    private MovieDetailBean data;

    public static JianJieDialogFragment newInstance() {
        JianJieDialogFragment jianJieDialogFragment = new JianJieDialogFragment();
        Bundle bundle = new Bundle();
        jianJieDialogFragment.setArguments(bundle);
        return jianJieDialogFragment;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.dialog_jianjie_layout;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvTitle.setText(data.title);
        tvJianjieContent.setText(data.content);
        tvPlayNum.setText(getActivity().getString(R.string.play_num,data.play_num));
        tagFlwLayout.setAdapter(new TagAdapter<String>(data.label) {
            @Override
            public View getView(FlowLayout parent, int position, String o) {
                TextView tv = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.item_simple_type_text, tagFlwLayout, false);
                tv.setText(o);
                return tv;
            }
        });
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = getDialog();
        Window dialogWindow = dialog.getWindow();
        applyCompat(dialogWindow);
        dialogWindow.setSoftInputMode(SOFT_INPUT_STATE_ALWAYS_HIDDEN | SOFT_INPUT_ADJUST_RESIZE);
        //设置对话框从底部进入
        dialogWindow.setWindowAnimations(R.style.bottomInWindowAnim);
        WindowManager.LayoutParams p = dialogWindow.getAttributes();
        p.width = ViewGroup.LayoutParams.MATCH_PARENT;
        p.height = ViewGroup.LayoutParams.MATCH_PARENT;//高度自己设定
        dialogWindow.setAttributes(p);
        dialogWindow.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        dialogWindow.setGravity(Gravity.BOTTOM);
        dialog.setCancelable(true);

        dialog.setCanceledOnTouchOutside(true);
        //修复状态栏变黑的问题
        int screenHeight = getScreenHeight(getActivity());
        int statusBarHeight = getStatusBarHeight(getContext());
        int dialogHeight = screenHeight - height;//screenHeight - statusBarHeight;
        dialogWindow.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, dialogHeight == 0 ? ViewGroup.LayoutParams.MATCH_PARENT : dialogHeight);
        return dialog;

    }
    public Dialog getDialog(){
        return new Dialog(getActivity(), R.style.NTitleDialog);
    }
    private void applyCompat(Window dialogWindow) {
        if (Build.VERSION.SDK_INT <= 19) {
            return;
        }
        dialogWindow.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialogWindow.setStatusBarColor(Color.TRANSPARENT);
        }

    }

    public JianJieDialogFragment setPlayerHeight(int height) {
        this.height = height;
        return this;
    }

    public JianJieDialogFragment setData(MovieDetailBean movieDetailBean) {
        this.data = movieDetailBean;
        return this;
    }

    @OnClick(R.id.ivClose)
    public void onViewClicked() {
        dismissAllowingStateLoss();
    }
}
