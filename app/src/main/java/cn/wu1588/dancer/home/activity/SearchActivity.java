package cn.wu1588.dancer.home.activity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cn.wu1588.dancer.R;

import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;
import com.sunrun.sunrunframwork.utils.EmptyDeal;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.wu1588.dancer.home.mode.HistoryBean;
import cn.wu1588.dancer.home.mode.HistoryMode;
import cn.wu1588.dancer.home.mode.HotSearchBean;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.ViewUtils;
import cn.wu1588.dancer.common.widget.titlebar.TabTitleBar;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_HOT_SEARCH_CODE;

/**
 * 搜索
 */
public class SearchActivity extends LBaseActivity {

    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.tvCancel)
    TextView tvCancel;
    @BindView(R.id.titleBar)
    TabTitleBar titleBar;
    @BindView(R.id.rlEmptyView)
    RelativeLayout rlEmptyView;
    @BindView(R.id.llDelete)
    LinearLayout llDelete;
    @BindView(R.id.rlHistorySearch)
    RelativeLayout rlHistorySearch;
    @BindView(R.id.tagFlwLayoutHistory)
    TagFlowLayout tagFlwLayoutHistory;
    @BindView(R.id.rlHotSearch)
    RelativeLayout rlHotSearch;
    @BindView(R.id.tagFlwLayoutHot)
    TagFlowLayout tagFlwLayoutHot;
    private HistoryMode historyMode;
    private LayoutInflater inflater;
    private List<String> stringList;
    private List<HotSearchBean> hotSearchBeans;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_new);
        ButterKnife.bind(this);
        initListener();
        initData();
    }

    private void initData() {
        BaseQuestStart.getHotSearchData(that);
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode) {
            case GET_HOT_SEARCH_CODE:
                if (bean.status == 1) {
                    hotSearchBeans = bean.Data();
                    updateUI(hotSearchBeans);
                }
                break;
        }
        super.nofityUpdate(requestCode, bean);
    }

    private void updateUI(List<HotSearchBean> hotSearchBeans) {
        if (!EmptyDeal.isEmpy(hotSearchBeans)) {
            tagFlwLayoutHot.setAdapter(new TagAdapter<HotSearchBean>(hotSearchBeans) {
                @Override
                public View getView(FlowLayout parent, int position, HotSearchBean o) {
                    TextView tv = (TextView) inflater.inflate(R.layout.item_simple_type_text, tagFlwLayoutHistory, false);
                    tv.setText(o.keywords);
                    return tv;
                }
            });
        }
    }

    @Override
    protected void onResume() {
        initViews();
        super.onResume();
    }

    private void initListener() {
        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    historyMode.addHistory(v.getText().toString().trim(), actionId + "");
                    String keyWords = etSearch.getText().toString();
                    if (!EmptyDeal.isEmpy(keyWords)) {
                        CommonIntent.startSearchResult(that, keyWords);
                    }
                    return true;
                }
                return false;
            }
        });
        tagFlwLayoutHistory.setOnTagClickListener(new TagFlowLayout.OnTagClickListener() {
            @Override
            public boolean onTagClick(View view, int position, FlowLayout parent) {
                List<HistoryBean> searchHistorys = historyMode.getSearchHistorys();
                if (!EmptyDeal.isEmpy(searchHistorys)) {
                    CommonIntent.startSearchResult(that, searchHistorys.get(position).keywords);
                }
                return false;
            }
        });
        tagFlwLayoutHot.setOnTagClickListener(new TagFlowLayout.OnTagClickListener() {
            @Override
            public boolean onTagClick(View view, int position, FlowLayout parent) {
                if (!EmptyDeal.isEmpy(hotSearchBeans)) {
                    historyMode.addHistory(hotSearchBeans.get(position).keywords, "");
                    CommonIntent.startSearchResult(that, hotSearchBeans.get(position).keywords);
                }
                return false;
            }
        });
    }

    private void initViews() {
        historyMode = new HistoryMode(getSession());
        inflater = LayoutInflater.from(this);

        if (isHistoryVisbility()) return;
        tagFlwLayoutHistory.setAdapter(new TagAdapter<HistoryBean>(historyMode.getSearchHistorys()) {
            @Override
            public View getView(FlowLayout parent, int position, HistoryBean s) {
                TextView tv = (TextView) inflater.inflate(R.layout.item_simple_type_text, tagFlwLayoutHistory, false);
                tv.setText(s.keywords);
                return tv;
            }
        });


    }

    private boolean isHistoryVisbility() {
        if (EmptyDeal.isEmpy(historyMode.getSearchHistorys())) {
            tagFlwLayoutHistory.setVisibility(View.GONE);
            rlHistorySearch.setVisibility(View.GONE);
            ViewUtils.setMargins(rlHotSearch, 0, 0, 0, 0);
            return true;
        } else {
            tagFlwLayoutHistory.setVisibility(View.VISIBLE);
            rlHistorySearch.setVisibility(View.VISIBLE);
            ViewUtils.setMargins(rlHotSearch, 0, 50, 0, 0);
        }
        return false;
    }

    @OnClick({R.id.tvCancel, R.id.llDelete, R.id.image_finish})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvCancel:
                if (etSearch.getText().toString() == null) {
                    ToastUtils.shortToast("搜索内容不能为空！");
                    return;
                }
                if (!EmptyDeal.isEmpy(etSearch.getText().toString())) {
                    CommonIntent.startSearchResult(that, etSearch.getText().toString());
                }
                break;
            case R.id.llDelete:
                if (!EmptyDeal.isEmpy(historyMode.getSearchHistorys())) {
                    historyMode.clearAllHistory();
                    tagFlwLayoutHistory.getAdapter().notifyDataChanged();
                    tagFlwLayoutHistory.setVisibility(View.GONE);
                    rlHistorySearch.setVisibility(View.GONE);
                    ViewUtils.setMargins(rlHotSearch, 0, 0, 0, 0);
                }
                break;
            case R.id.image_finish:
                finish();
                break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (historyMode != null) {
            historyMode.save();
        }
    }
}
