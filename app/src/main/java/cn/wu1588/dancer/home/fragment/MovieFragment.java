package cn.wu1588.dancer.home.fragment;

import android.os.Bundle;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.sunrun.sunrunframwork.bean.BaseBean;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.adp.MovieAdapter;
import cn.wu1588.dancer.adp.SearchResultAdapter;
import cn.wu1588.dancer.home.mode.MovieBean;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.PageLimitDelegate;
import cn.wu1588.dancer.common.util.RecycleHelper;
import cn.wu1588.dancer.home.activity.AllMovieActivity;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_ALL_MOVIE_CODE;

public class MovieFragment extends LBaseFragment {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
//    public String screen_id;
    public String home_category_id;
    public String recommend_category_id;
    PageLimitDelegate pageLimitDelegate = new PageLimitDelegate(new PageLimitDelegate.DataProvider() {
        @Override
        public void loadData(int page) {
            String screen_id = ((AllMovieActivity) getActivity()).screen_id;
            BaseQuestStart.getMovieAll(MovieFragment.this, screen_id, home_category_id, recommend_category_id, page);
        }
    });
    private String style_type;
    private SearchResultAdapter searchResultAdapter;
    private MovieAdapter movieAdapter;

    public static MovieFragment newInstance(String id) {
        MovieFragment movieFragment = new MovieFragment();
        Bundle bundle = new Bundle();
        bundle.putString("home_category_id",id);
        movieFragment.setArguments(bundle);
        return movieFragment;
    }

    public MovieFragment  updateDate() {
//        this.screen_id=screen_id;
        pageLimitDelegate.refreshPage();
        return this;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        home_category_id = getArguments().getString("home_category_id");


    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_movie;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        screen_id =getActivity().getIntent().getStringExtra("screen_id");
        style_type =getActivity().getIntent().getStringExtra("style_type");
        initViews();
    }

    private void initViews() {
        if ("newest_moive".equals(style_type)){
            RecycleHelper.setGridLayoutManager(recyclerView, 2);
            movieAdapter = new MovieAdapter();
            pageLimitDelegate.attach(refreshLayout,recyclerView, movieAdapter);
            recyclerView.setAdapter(movieAdapter);
            movieAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                    MovieBean item = movieAdapter.getItem(position);
                    CommonIntent.startMoviePlayerActivity(that,String.valueOf(item.id));
                }
            });
        }else{
            RecycleHelper.setLinearLayoutManager(recyclerView, LinearLayoutManager.VERTICAL);
            searchResultAdapter = new SearchResultAdapter();
            pageLimitDelegate.attach(refreshLayout,recyclerView,searchResultAdapter);
            recyclerView.setAdapter(searchResultAdapter);
            searchResultAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                    MovieBean item = searchResultAdapter.getItem(position);
                    CommonIntent.startMoviePlayerActivity(that,String.valueOf(item.id));
                }
            });
        }


    }

    @Override
    public void onVisible() {
        super.onVisible();
        if(refreshLayout!=null){
            pageLimitDelegate.refreshPage();
        }
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode){
            case GET_ALL_MOVIE_CODE:
                if (bean.status==1){
                    List<MovieBean> list=bean.Data();
                    pageLimitDelegate.setData(list);
                }
                break;
        }
        super.nofityUpdate(requestCode, bean);
    }
}
