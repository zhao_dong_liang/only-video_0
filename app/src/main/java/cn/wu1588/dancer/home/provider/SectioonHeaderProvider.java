package cn.wu1588.dancer.home.provider;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.provider.BaseItemProvider;

import cn.wu1588.dancer.adp.HomeNewAdapter;
import cn.wu1588.dancer.home.mode.HomeBean;

/**
 * 分组头部  更多
 */
public class SectioonHeaderProvider extends BaseItemProvider<HomeBean.DataListBean, BaseViewHolder> {
    @Override
    public int viewType() {
        return HomeNewAdapter.TYPE_SECTION_TITLE;
    }

    @Override
    public int layout() {
        return R.layout.home_more_layout;
    }

    @Override
    public void convert(BaseViewHolder helper, HomeBean.DataListBean data, int position) {
        helper.setText(R.id.tvTitle,data.title);
        helper.setGone(R.id.tvMore, data.isMore.equals("true"));
        if ( "is_free".equals(data.style)){
            helper.setTextColor(R.id.tvTitle,mContext.getResources().getColor(R.color.red_color_ff0000));
        }else {
            helper.setTextColor(R.id.tvTitle,mContext.getResources().getColor(R.color.color_eee));

        }
    }

    @Override
    public void onClick(BaseViewHolder helper, HomeBean.DataListBean data, int position) {
        super.onClick(helper, data, position);

    }
}
