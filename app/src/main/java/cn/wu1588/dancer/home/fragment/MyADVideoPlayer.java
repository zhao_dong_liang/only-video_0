package cn.wu1588.dancer.home.fragment;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;

import com.shuyu.gsyvideoplayer.video.GSYADVideoPlayer;

public class MyADVideoPlayer extends GSYADVideoPlayer {

    public MyADVideoPlayer(Context context, Boolean fullFlag) {
        super(context, fullFlag);
    }

    public MyADVideoPlayer(Context context) {
        super(context);
    }

    public MyADVideoPlayer(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void init(Context context) {
        super.init(context);
        mJumpAd.setVisibility(GONE);
    }

    @Override
    protected void changeAdUIState() {
        if (mADTime != null) {
            mADTime.setVisibility((isFirstPrepared) ? VISIBLE : GONE);
        }
        if (mBottomContainer != null) {
            int color = (isFirstPrepared) ? Color.TRANSPARENT : getContext().getResources().getColor(com.shuyu.gsyvideoplayer.R.color.bottom_container_bg);
            mBottomContainer.setBackgroundColor(color);
        }
        if (mCurrentTimeTextView != null) {
            mCurrentTimeTextView.setVisibility((isFirstPrepared) ? INVISIBLE : VISIBLE);
        }
        if (mTotalTimeTextView != null) {
            mTotalTimeTextView.setVisibility((isFirstPrepared) ? INVISIBLE : VISIBLE);
        }
        if (mProgressBar != null) {
            mProgressBar.setVisibility((isFirstPrepared) ? INVISIBLE : VISIBLE);
            mProgressBar.setEnabled(!(isFirstPrepared));
        }
    }
}
