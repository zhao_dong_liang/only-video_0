package cn.wu1588.dancer.home.mode;

public class ThirdBean {

    /**
     * id : 7
     * type : 1
     * app_key : e89d357d10d9fc1d74d80dc1c207b8ce
     * app_id : wx21fb0e3b0433eded
     * status : 1
     */

    private String id;
    private String type;
    private String app_key;
    private String app_id;
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getApp_key() {
        return app_key;
    }

    public void setApp_key(String app_key) {
        this.app_key = app_key;
    }

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
