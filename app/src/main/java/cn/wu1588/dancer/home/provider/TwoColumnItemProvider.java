package cn.wu1588.dancer.home.provider;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.provider.BaseItemProvider;

import cn.wu1588.dancer.adp.HomeNewAdapter;
import cn.wu1588.dancer.home.mode.HomeBean;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;


/**
 * 两列
 */

public class TwoColumnItemProvider extends BaseItemProvider<HomeBean.DataListBean, BaseViewHolder> {
    @Override
    public int viewType() {
        return HomeNewAdapter.TYPE_TWO_COLUMN;
    }

    @Override
    public int layout() {
        return R.layout.app_item_home_two_column;
    }

    @Override
    public void convert(BaseViewHolder helper, HomeBean.DataListBean data, int position) {
        helper.setText(R.id.tvTitle, data.title).setText(R.id.tvGrade, data.grade);
        GlideMediaLoader.load(mContext, helper.getView(R.id.rIvCourse), data.getImage(), R.drawable.ic_place_two_column);
//        helper.setGone(R.id.tvGrade, "newest_moive".equals(data.stype));
        helper.setGone(R.id.ivVip, !("1".equals(data.is_free)));
    }
}
