package cn.wu1588.dancer.home.mode;

import android.graphics.drawable.Drawable;

public class HomeChannelBean {
    public String title;
    public int icon;
    public int id;

    public HomeChannelBean(String title, int id) {
        this.title = title;
        this.id = id;
    }

    public HomeChannelBean(String title, int icon, int id) {
        this.title = title;
        this.icon = icon;
        this.id = id;
    }

    public HomeChannelBean() {
    }
}
