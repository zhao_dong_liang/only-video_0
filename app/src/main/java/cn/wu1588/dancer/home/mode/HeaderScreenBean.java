package cn.wu1588.dancer.home.mode;

import java.util.List;

public class HeaderScreenBean {

    public List<HomeCategoryBean> home_category;
    public List<ScreenBean> screen;


    public static class HomeCategoryBean {
        /**
         * id : 0
         * title : 综合
         * add_time : 2019-03-28 14:15:31
         * is_del : 0
         * is_hid : 0
         * logo : 
         * sort : 0
         * update_time : 0000-00-00 00:00:00
         */

        public String id;
        public String title;
        public String add_time;
        public String is_del;
        public String is_hid;
        public String logo;
        public String sort;
        public String update_time;

    }

    public static class ScreenBean {
        /**
         * screen_id : 0
         * title : 综合
         */

        public String screen_id;
        public String title;
        public boolean isSelect;
    }
}
