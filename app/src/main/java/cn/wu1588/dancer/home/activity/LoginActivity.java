package cn.wu1588.dancer.home.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.utils.UserAgreementDialog;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.common.model.LoginInfo;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.common.util.DeviceIdUtil;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.PHONE_LOGIN_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.SEND_SMS_CODE_CODE;


/*
* 登录页面
* */
public class LoginActivity extends LoginBaseActivity {

    @BindView(R.id.tv_password_login)
    TextView tvPasswordLogin;
    @BindView(R.id.tv_password_login_line)
    View tvPasswordLoginLine;
    @BindView(R.id.tv_code_login)
    TextView tvCodeLogin;
    @BindView(R.id.tv_code_login_line)
    View tvCodeLoginLine;
    @BindView(R.id.ll_title)
    RelativeLayout llTitle;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_code)
    EditText etCode;
    @BindView(R.id.tv_get_code)
    TextView tvGetCode;
    @BindView(R.id.rl_code)
    RelativeLayout rlCode;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.iv_eyes)
    ImageView ivEyes;
    @BindView(R.id.rl_password)
    RelativeLayout rlPassword;
    @BindView(R.id.bt_login)
    Button btLogin;
    @BindView(R.id.tv_login)
    TextView tvLogin;
    @BindView(R.id.tv_forget_password)
    TextView tvForgetPassword;
    @BindView(R.id.tv_agment)
    TextView tvAgment;
    @BindView(R.id.iv_wechat)
    ImageView ivWechat;
    @BindView(R.id.iv_qq)
    ImageView ivQq;
    private int loginType = 0;
    private boolean isEyes;
    /**
     * 计时器
     */
    private TimeCount timer;

    private boolean status = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        status =  sharedPreferences.getBoolean("status",false);
        editor.putBoolean("status",status);
        editor.commit();
        new Thread() {
            @Override
            public void run() {
                super.run();
                try {
                    Thread.sleep(1000);
                    startActivity(new Intent(LoginActivity.this, UserAgreementDialog.class));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }.start();


    }

    @OnClick({R.id.bt_login, R.id.tv_password_login, R.id.tv_code_login, R.id.tv_get_code, R.id.iv_eyes, R.id.tv_login, R.id.tv_forget_password, R.id.tv_agment, R.id.iv_wechat, R.id.iv_qq})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_password_login:
                loginType = 0;
                tvPasswordLogin.setTextColor(getResources().getColor(R.color.black));
                tvPasswordLoginLine.setVisibility(View.VISIBLE);
                tvCodeLogin.setTextColor(getResources().getColor(R.color.gray));
                tvCodeLoginLine.setVisibility(View.INVISIBLE);
                rlCode.setVisibility(View.GONE);
                rlPassword.setVisibility(View.VISIBLE);
                break;
            case R.id.tv_code_login:
                loginType = 1;
                tvPasswordLogin.setTextColor(getResources().getColor(R.color.gray));
                tvPasswordLoginLine.setVisibility(View.INVISIBLE);
                tvCodeLogin.setTextColor(getResources().getColor(R.color.black));
                tvCodeLoginLine.setVisibility(View.VISIBLE);
                rlCode.setVisibility(View.VISIBLE);
                rlPassword.setVisibility(View.GONE);
                break;
            case R.id.tv_get_code:
                String phone = etPhone.getText().toString().trim();
                if (TextUtils.isEmpty(phone)) {
                    ToastUtils.shortToast("请输入手机号");
                    return;
                } else if (!phone.matches("^((13[0-9])|(15[^4,\\D])|(18[0,1-9])|(17[0-9])|(14[0-9])|(19[0-9])|(16[0-9]))\\d{8}$")) {
                    ToastUtils.shortToast("请输入正确的手机格式");
                    return;
                }
                //获取验证码接口
                BaseQuestStart.getCode(phone, this);
                break;
            case R.id.iv_eyes:
                if (!isEyes) {
                    etPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT);
                } else {
                    etPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                }
                isEyes = !isEyes;
                break;
            case R.id.bt_login:
                String etphone = etPhone.getText().toString().trim();
                if (TextUtils.isEmpty(etphone)) {
                    ToastUtils.shortToast("请输入手机号");
                    return;
                } else if (!etphone.matches("^((13[0-9])|(15[^4,\\D])|(18[0,1-9])|(17[0-9])|(14[0-9])|(19[0-9])|(16[0-9]))\\d{8}$")) {
                    ToastUtils.shortToast("请输入正确的手机格式");
                    return;
                }
                if (loginType == 0) {
                    String password = etPassword.getText().toString().trim();
                    if (TextUtils.isEmpty(password)) {
                        ToastUtils.shortToast("请输入密码");
                        return;
                    }
                    String deviceId = DeviceIdUtil.getDeviceId(that);
                    if (!TextUtils.isEmpty(deviceId)) {
                        BaseQuestStart.userLogin(this, "1", etphone, password, "", deviceId);
                    } else {
                        BaseQuestStart.userLogin(this, loginType + "1", etphone, password, "", "未获取到");
                    }


                } else {
                    String code = etCode.getText().toString().trim();
                    if (TextUtils.isEmpty(code)) {
                        ToastUtils.shortToast("请输入验证码");
                        return;
                    }
                    String deviceId = DeviceIdUtil.getDeviceId(that);
                    if (!TextUtils.isEmpty(deviceId)) {
                        BaseQuestStart.userLogin(this, "2", etphone, "", code, deviceId);
                    } else {
                        BaseQuestStart.userLogin(this, "2", etphone, "", code, "未获取到");
                    }
                }
                break;
            case R.id.tv_login:
                CommonIntent.startRegisterActivity(that);
                break;
            case R.id.tv_forget_password:
                CommonIntent.forgetPasswordActivity(that);
                break;
            case R.id.tv_agment:
                break;
            case R.id.iv_wechat:
                WXLogin(this);
                break;
            case R.id.iv_qq:
                QQLogin(this);
                break;
        }
    }


    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode) {
            case SEND_SMS_CODE_CODE:
                if (bean.status == 1) {
                    ToastUtils.shortToast(bean.msg);
                    timer = new TimeCount(60000, 1000);
                    timer.start();
                } else {
                    ToastUtils.shortToast(bean.msg);
                }
                break;
            case PHONE_LOGIN_CODE:
                if (bean.status == 1) {
                    ToastUtils.shortToast(bean.msg);
                    LoginInfo loginInfo = bean.Data();
                    if (loginInfo != null) {
                        Config.putLoginInfo(loginInfo);
                        //存储数据时选用对应类型的方法
                        editor.putString("userid", loginInfo.uid);
                        //提交保存数据
                        editor.commit();
                    }
                    BaseQuestStart.getUserInfo(this);

                } else {
                    ToastUtils.shortToast(bean.msg);
                }
                break;
//            case GET_USER_INFO_CODE:
//                if (bean.status == 1) {
//                   LoginInfo loginInfo = bean.Data();
//                    if (loginInfo != null) {
//                        loginInfo.setUid(loginInfo.id);
//                        Config.putLoginInfo(loginInfo);
//                        SPUtils.put(that, ConstantValue.IS_LOGIN,true);
//                        finish();
//                    }
//                    EventBus.getDefault().post(new MessageEvent("login_success","1"));
//                }
//                break;
        }
        super.nofityUpdate(requestCode, bean);
    }

    /**
     * 倒计时
     */
    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            /*
             * millisInFuture 总时长,countDownInterval 计时的时间间隔
             */
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            /**
             * 计时完毕时触发
             */
            tvGetCode.setText("获取验证码");
            tvGetCode.setClickable(true);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            /**
             * 计时过程显示
             */
            tvGetCode.setClickable(false);
            tvGetCode.setText(millisUntilFinished / 1000 + "秒");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null) timer.cancel();
    }


}
