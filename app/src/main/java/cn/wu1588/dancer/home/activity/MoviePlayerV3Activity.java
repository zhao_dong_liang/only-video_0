package cn.wu1588.dancer.home.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.adp.RelatedVideoAdapter;
import cn.wu1588.dancer.adp.RelatedVideoV3Adapter;
import cn.wu1588.dancer.adp.RvAuthorPortfolioAdapter;
import cn.wu1588.dancer.base.BaseAty;
import cn.wu1588.dancer.channel.mode.NvyouBean;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.model.LoginInfo;
import cn.wu1588.dancer.common.model.SystemParams;
import cn.wu1588.dancer.common.quest.BaseQuestConfig;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.common.util.DataUtils;
import cn.wu1588.dancer.common.util.LoginUtil;
import cn.wu1588.dancer.common.util.RecycleHelper;
import cn.wu1588.dancer.common.util.SPUtils;
import cn.wu1588.dancer.common.util.ViewUtils;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;
import cn.wu1588.dancer.home.fragment.AdPlayerFragment;
import cn.wu1588.dancer.home.mode.MovieBean;
import cn.wu1588.dancer.home.mode.MovieByCateV3Bean;
import cn.wu1588.dancer.share.ShareBottomSheetDialog;
import cn.wu1588.dancer.share.ShareBottomSheetV3Dialog;
import cn.wu1588.dancer.utils.map.JSONUtils;
import cn.wu1588.dancer.utils.map.MD5Utils;
import cn.wu1588.dancer.videoplayer.SwitchUtil;
import cn.wu1588.dancer.videoplayer.SwitchVideo;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.sdk.android.vod.upload.common.utils.MD5;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.cnsunrun.commonui.widget.button.RoundButton;
import com.kongzue.baseframework.interfaces.BindView;
import com.kongzue.baseframework.interfaces.DarkStatusBarTheme;
import com.kongzue.baseframework.interfaces.Layout;
import com.kongzue.baseframework.util.JumpParameter;
import com.kongzue.baseokhttp.HttpRequest;
import com.kongzue.baseokhttp.listener.ResponseListener;
import com.kongzue.baseokhttp.util.JsonList;
import com.kongzue.baseokhttp.util.JsonMap;
import com.kongzue.baseokhttp.util.Parameter;
import com.kongzue.dialog.interfaces.OnDialogButtonClickListener;
import com.kongzue.dialog.util.BaseDialog;
import com.kongzue.dialog.v3.MessageDialog;
import com.lzy.okgo.model.Progress;
import com.makeramen.roundedimageview.RoundedImageView;
import com.maning.mndialoglibrary.MProgressBarDialog;
import com.my.toolslib.NumUtils;
import com.my.toolslib.http.utils.LzyResponse;
import com.my.toolslib.http.utils.NetRequestListenerProxy;
import com.my.toolslib.http.utils.OkHttpRequestListener;
import com.shuyu.gsyvideoplayer.GSYVideoManager;
import com.shuyu.gsyvideoplayer.builder.GSYVideoOptionBuilder;
import com.shuyu.gsyvideoplayer.listener.GSYSampleCallBack;
import com.shuyu.gsyvideoplayer.utils.OrientationUtils;
import com.shuyu.gsyvideoplayer.video.base.GSYVideoPlayer;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.tencent.bugly.proguard.P;
import com.tencent.mmkv.MMKV;

import org.apache.http.HttpResponse;

import java.io.File;
import java.util.List;
import java.util.function.ToDoubleBiFunction;

@Layout(R.layout.activity_movie_player_v3)
public class MoviePlayerV3Activity extends BaseAty implements BaseQuickAdapter.OnItemClickListener {

    @BindView(R.id.moviePlayer)
    private SwitchVideo moviePlayer;
    @BindView(R.id.moviePlayer_ad)
    private SwitchVideo moviePlayer_ad;
    @BindView(R.id.rb_ad_1)
    private RoundButton rb_ad_1;
    @BindView(R.id.rb_ad_time)
    private RoundButton rb_ad_time;
    @BindView(R.id.rb_mp_count_down)
    private RoundButton rb_mp_count_down;
    @BindView(R.id.rb_mp_skip)
    private RoundButton rb_mp_skip;
    @BindView(R.id.mp_ad_container_layout)
    private ConstraintLayout mp_ad_container_layout;
    @BindView(R.id.iv_mp_ad)
    private ImageView iv_mp_ad;
    @BindView(R.id.tv_mp_ad)
    private TextView tv_mp_ad;
    @BindView(R.id.rb_ad)
    private RoundButton rb_ad;
    @BindView(R.id.btn_ad_download)
    private RoundButton btn_ad_download;
    @BindView(R.id.tv_movie_title)
    private TextView tv_movie_title;
    @BindView(R.id.tv_movie_watch_number)
    private TextView tv_movie_watch_number;
    @BindView(R.id.tv_mp_report)
    private TextView tv_mp_report;
    @BindView(R.id.tv_mp_concise)
    private TextView tv_mp_concise;
    @BindView(R.id.ll_mp_album)
    private LinearLayout ll_mp_album;
    @BindView(R.id.tv_mp_cd_title)
    private TextView tv_mp_cd_title;
    @BindView(R.id.tv_mp_buy_album)
    private TextView tv_mp_buy_album;
    @BindView(R.id.tv_mp_time)
    private TextView tv_mp_time;
    @BindView(R.id.tv_mp_video_num)
    private TextView tv_mp_video_num;
    @BindView(R.id.tv_mp_video_money)
    private TextView tv_mp_video_money;
    @BindView(R.id.rv_author_portfolio)
    private RecyclerView rv_author_portfolio;
    @BindView(R.id.ivDianZan)
    private TextView ivDianZan;
    @BindView(R.id.ivXiaZai)
    private TextView ivXiaZai;
    @BindView(R.id.ivShare)
    private TextView ivShare;
    @BindView(R.id.tv_movie_4k_download)
    private TextView tv_movie_4k_download;
    @BindView(R.id.ivShouCang)
    private TextView ivShouCang;
    @BindView(R.id.iv_video_author_avatar)
    private RoundedImageView iv_video_author_avatar;
    @BindView(R.id.tv_video_author_name)
    private TextView tv_video_author_name;
    @BindView(R.id.tv_video_author_desc)
    private TextView tv_video_author_desc;
    @BindView(R.id.btn_movie_subscription)
    private RoundButton btn_movie_subscription;
    @BindView(R.id.rv_be_interested_video)
    private RecyclerView rv_be_interested_video;
    @BindView(R.id.llCommentActions)
    private LinearLayout llCommentActions;
    @BindView(R.id.ibVideoCommentActionCollect)
    private ImageButton ibVideoCommentActionCollect;
    @BindView(R.id.ibVideoCommentActionLike)
    private ImageButton ibVideoCommentActionLike;
    @BindView(R.id.ibVideoCommentActionSend)
    private ImageButton ibVideoCommentActionSend;
    @BindView(R.id.rl_mp)
    private RelativeLayout rl_mp;

    @BindView(R.id.fl_cover)
    FrameLayout fl_cover;

    private static final String OPTION_VIEW = "view";
    private String movieId;
    private int isZan;
    //视频作者的id
    private String attention_uid;
    //"home_category_id": "15" 视频分类
    public String home_category_id;
    //              视频的Url  视频封面url     视频标题      专辑Id            4k
    private String play_url, video_img_url, video_title, hot_category_id, if4K;
    private static final String ATTENTION_TAG = "已关注";

    private RelatedVideoV3Adapter mAdapter;
    private RvAuthorPortfolioAdapter mChannelAdapter;
    List<MovieByCateV3Bean> byCateV3Beans;
    private int isCommonVip = -1;

    private AdPlayerFragment adPlayerFragment;
    private String showDownload;
    private GSYVideoOptionBuilder gsyVideoOption;
    private OrientationUtils orientationUtils;

    //会员是否过期
    private boolean memberDateMature;

    private boolean isPlay;
    private boolean isPause;

    public static void startTActivity(Activity activity, String movieId, View transitionView) {
        if (LoginUtil.startLogin(activity)) {
            CommonIntent.startLoginActivity(activity);
        } else {
            Intent intent = new Intent(activity, MoviePlayerV3Activity.class);
            intent.putExtra("movieId", movieId);
            // 这里指定了共享的视图元素
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, transitionView, OPTION_VIEW);
            ActivityCompat.startActivity(activity, intent, options.toBundle());
        }
    }

    @Override
    public void initViews() {
        rv_author_portfolio.setNestedScrollingEnabled(false);
        rv_be_interested_video.setNestedScrollingEnabled(false);
        RecycleHelper.setLinearLayoutManager(rv_author_portfolio, LinearLayoutManager.HORIZONTAL);
        RecycleHelper.setLinearLayoutManager(rv_be_interested_video, LinearLayoutManager.VERTICAL);


    }

    @Override
    public void initDatas(JumpParameter parameter) {
        movieId = getIntent().getStringExtra("movieId");
        LogUtils.e("获取的movieId是：" + movieId);
        LoginInfo loginInfo = Config.getLoginInfo();
        memberDateMature = DataUtils.compare_date(loginInfo.member_end_time);

        initPlayer();

        //播放接口
        getMovieDeatil();

        //验证是否是Vip
        isVIP();

        //查看影片是否点赞或者收藏
        getCheckTheMovieFavoritesAndLike();


        MMKV mmkv = MMKV.defaultMMKV();
        showDownload = mmkv.decodeString(SystemParams.IS_SHOW_DOWBTN);

        // TODO: 2021/2/18 这里是旧页面用fragment播放广告 新的该怎么弄呢？
//        int code = NumUtils.parseInt(mmkv.decodeString(SystemParams.INSERT_AD_IS_OPEN), 0);
//        if (code == 1) {
//            adPlayerFragment = AdPlayerFragment.newInstance();
//            adPlayerFragment.setOnHostPlayerLoadVideo(this::getMovieDeatil);
//            adPlayerFragment.setOnHostPlayerResume(() -> moviePlayer.onVideoResume());
//            adPlayerFragment.setOnHostPlayerPause(() -> moviePlayer.onVideoPause());
//
//            getSupportFragmentManager()
//                    .beginTransaction()
//                    .add(R.id.fl_ad_player, adPlayerFragment, "AdPlayerFragment")
//                    .commit();
//        } else {
//            //播放接口
//            getMovieDeatil();
//        }



    }


    @Override
    public void setEvents() {
        //点赞 点击事件
        ivDianZan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToastUtils.showShort("in click ");
                getGiveLikeOrDis();
            }
        });
        ibVideoCommentActionLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ToastUtils.showShort("in click ");
                getGiveLikeOrDis();
            }
        });

        ivShouCang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getVideoCollect();
            }
        });

        btn_movie_subscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.equals(btn_movie_subscription.getText(), ATTENTION_TAG)) {
                    androidx.appcompat.app.AlertDialog dialog = new androidx.appcompat.app.AlertDialog.Builder(me)
                            .setMessage("确定要取消该用户订阅吗？")
                            .setPositiveButton("确定", (d, which) -> cancelAttention())
                            .setNegativeButton("取消", (d, which) -> Log.d("MoviePlayerActivity", "取消订阅窗口"))
                            .create();
                    dialog.show();
                    Button btnPos = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                    Button btnNeg = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
                    btnPos.setTextColor(ContextCompat.getColor(me, R.color.colorPrimary));
                    btnNeg.setTextColor(ContextCompat.getColor(me, R.color.colorPrimary));
                } else {
                    getFocusUser();
                }

            }
        });

        ivXiaZai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isCommonVip == 0) {
                    MessageDialog.build(MoviePlayerV3Activity.this)
                            .setTitle("提示")
                            .setMessage("您需要购买会员！")
                            .setOkButton("了解", new OnDialogButtonClickListener() {
                                @Override
                                public boolean onClick(BaseDialog baseDialog, View v) {
                                    return false;
                                }
                            }).setCancelable(false).show();

                } else if (isCommonVip == 2) {
                    MessageDialog.build(MoviePlayerV3Activity.this)
                            .setTitle("提示")
                            .setMessage("您会员事件已过期！")
                            .setOkButton("了解", new OnDialogButtonClickListener() {
                                @Override
                                public boolean onClick(BaseDialog baseDialog, View v) {
                                    return false;
                                }
                            }).setCancelable(false).show();
                } else {
                    getDownLoadVideo();
                }
            }
        });

        ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ShareBottomSheetV3Dialog shareBottomSheetDialog = ShareBottomSheetV3Dialog.newInstance(Config.getLoginInfo().uid, movieId, video_img_url, video_title, play_url);
                shareBottomSheetDialog.show(getSupportFragmentManager(), ShareBottomSheetV3Dialog.TAG);

            }
        });
        ibVideoCommentActionSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ShareBottomSheetV3Dialog shareBottomSheetDialog = ShareBottomSheetV3Dialog.newInstance(Config.getLoginInfo().uid, movieId, video_img_url, video_title, play_url);
                shareBottomSheetDialog.show(getSupportFragmentManager(), ShareBottomSheetV3Dialog.TAG);

            }
        });

        tv_movie_4k_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (if4K.equals("0")) {
                    tv_movie_4k_download.setClickable(false);
                    MessageDialog.build(MoviePlayerV3Activity.this)
                            .setTitle("提示")
                            .setMessage("此视频没有4K资源！")
                            .setOkButton("确定", new OnDialogButtonClickListener() {
                                @Override
                                public boolean onClick(BaseDialog baseDialog, View v) {
                                    return false;
                                }
                            }).setCancelable(false).show();
                }
            }
        });

    }


    /**
     * 初始化播放器
     */
    private void initPlayer() {
        //外部辅助的旋转，帮助全屏
        orientationUtils = new OrientationUtils(this, moviePlayer);
        //初始化不打开外部的旋转
        orientationUtils.setEnable(false);


        moviePlayer.getFullscreenButton().setOnClickListener(v -> {
            //直接横屏
            orientationUtils.resolveByClick();
            //第一个true是否需要隐藏actionbar，第二个true是否需要隐藏statusbar
            moviePlayer.startWindowFullscreen(me
                    , true, true);
        });
        moviePlayer.setIsClikListner(false);

    }


    /**
     * vip会员验证
     * vip 套餐 id 免费投广告会员：1；分享推广告会员：2；普通视频下载会员：3
     */
    private void isVIP() {
        HttpRequest.GET(me, BaseQuestConfig.GET_NEW_IS_VIP,
                new Parameter()
                        .add("uid", Config.getLoginInfo().uid)
                        .add("vip_id", 3),
                new ResponseListener() {
                    @Override
                    public void onResponse(String main, Exception error) {
                        if (error == null) {
                            JsonMap jsonMap = JsonMap.parse(main);
                            String status = jsonMap.getString("status");
                            if (status.equals("1")) {
                                JsonMap data = jsonMap.getJsonMap("data");
                                isCommonVip = data.getInt("result");
                            }
                        }
                    }
                });
    }

    /**
     * 下载视频
     */
    private MProgressBarDialog mProgressBarDialog;

    private void getDownLoadVideo() {
        if (TextUtils.isEmpty(play_url)) {
            LogUtils.e("卧槽 下载地址空了?");
            return;
        } else {
            initDownloadDialog();
            BaseQuestStart.downLoadVideo(new OkHttpRequestListener() {
                @Override
                public void downLoadFinish(File file) {
                    super.downLoadFinish(file);
                    ToastUtils.showShort("下载完成");
                    //下载完成之后 添加下载记录？
                    getDownloadRecords();
                }

                @Override
                public void httpLoadFail(String err) {
                    super.httpLoadFail(err);
                    ToastUtils.showShort(err);
                }

                @Override
                public void httpLoadFinal() {
                    super.httpLoadFinal();
                    if (mProgressBarDialog != null) {
                        mProgressBarDialog.dismiss();
                    }
                }

                @Override
                public void onProgress(Progress progress) {
                    super.onProgress(progress);
                    if (mProgressBarDialog != null) {
                        int fraction = (int) (progress.fraction * 100);
                        mProgressBarDialog.showProgress(fraction, "已下载" + fraction + "%", true);
                    }
                }
            }, play_url);
        }
    }

    private void initDownloadDialog() {
        //新建一个Dialog
        mProgressBarDialog = new MProgressBarDialog.Builder(this)
                //全屏模式
                .isWindowFullscreen(true)
                .setStyle(MProgressBarDialog.MProgressBarDialogStyle_Circle)
                //全屏背景窗体的颜色
                .setBackgroundWindowColor(Color.TRANSPARENT)
                //View背景的颜色
                .setBackgroundViewColor(Color.BLACK)
                //字体的颜色
                .setTextColor(Color.WHITE)
                //View边框的颜色
                .setStrokeColor(Color.TRANSPARENT)
                //View边框的宽度
                .setStrokeWidth(2)
                //View圆角大小
                .setCornerRadius(10)
                //ProgressBar背景色
                .setProgressbarBackgroundColor(Color.BLACK)
                //ProgressBar 颜色
                .setProgressColor(Color.WHITE)
                //圆形内圈的宽度
                .setCircleProgressBarWidth(4)
                //圆形外圈的宽度
                .setCircleProgressBarBackgroundWidth(4)
                //水平进度条Progress圆角
                .setProgressCornerRadius(0)
                //水平进度条的高度
                .setHorizontalProgressBarHeight(10)
                //dialog动画
//                .setAnimationID(R.style.animate_dialog_custom)
                .build();
    }


    /**
     * 取消关注
     */
    private void cancelAttention() {
        HttpRequest.POST(me, BaseQuestConfig.GET_NEW_CANCEL_ATTENTION,
                new Parameter()
                        .add("user_id", Config.getLoginInfo().uid)
                        .add("attention_uid", attention_uid),
                new ResponseListener() {
                    @Override
                    public void onResponse(String main, Exception error) {
                        if (error == null) {
                            JsonMap jsonMap = JsonMap.parse(main);
                            String status = jsonMap.getString("status");
                            if (status.equals("1")) {
                                String msg = jsonMap.getString("msg");
                                if (msg.equals("success")) {
                                    btn_movie_subscription.setText("关注");
                                }
                            }
                        }
                    }
                });
    }

    /**
     * 关注用户
     */
    private void getFocusUser() {
        HttpRequest.POST(me, BaseQuestConfig.GET_NEW_ATTENTION_USER,
                new Parameter()
                        .add("uid", Config.getLoginInfo().uid)
                        .add("attention_uid", attention_uid),
                new ResponseListener() {
                    @Override
                    public void onResponse(String main, Exception error) {
                        if (error == null) {
                            JsonMap jsonMap = JsonMap.parse(main);
                            String status = jsonMap.getString("status");
                            if (status.equals("1")) {
                                String msg = jsonMap.getString("msg");
                                if (msg.equals("success")) {
                                    btn_movie_subscription.setText(ATTENTION_TAG);
                                }
                            }
                        }
                    }
                });
    }

    /**
     * 视频收藏
     */
    private void getVideoCollect() {
        // TODO: 2021/2/1 视频收藏接口 无法取消收藏
        HttpRequest.POST(me, BaseQuestConfig.GET_NEW_MOVIE_COLLECT,
                new Parameter()
                        .add("uid", Config.getLoginInfo().uid)
                        .add("movie_id", movieId),
                new ResponseListener() {
                    @Override
                    public void onResponse(String main, Exception error) {
                        if (error == null) {
                            JsonMap jsonMap = JsonMap.parse(main);
                            String status = jsonMap.getString("status");
                            if (status.equals("1")) {
                                String data = jsonMap.getString("data");
                                if (data.equals("1")) {
                                    ivShouCang.setText("已收藏");
                                    ViewUtils.setTextViewTopDrawableColor(ivDianZan, R.color.color_FF9600);
                                    ibVideoCommentActionCollect.setImageResource(R.drawable.icon_oro_collection);
                                }
                            } else if (status.equals("0")) {
                                ToastUtils.showShort("您已经收藏过了！");
                            }
                        }
                    }
                });
    }

    /**
     * 点赞
     */
    private void getGiveLikeOrDis() {
        int type = -1;
        //如果是 点了赞的
        if (isZan == 1) {
            type = 1;
        } else {
            type = 0;
        }
        HttpRequest.POST(me,
                BaseQuestConfig.GET_NEW_MOVIE_DIANZAN,
                new Parameter().add("is_top", 1)
                        .add("movie_id", movieId)
                        .add("uid", Config.getLoginInfo().uid),
                new ResponseListener() {
                    @Override
                    public void onResponse(String main, Exception error) {
                        if (error == null) {
                            JsonMap jsonMap = JsonMap.parse(main);
                            String msg = jsonMap.getString("msg");
                            if (msg.equals("点赞成功")) {
                                // TODO: 2021/1/31
                                ViewUtils.setTextViewTopDrawableColor(ivDianZan, R.color.color_FF9600);
//                            isZan = 1;
                                ivDianZan.setText("已赞");
                                ibVideoCommentActionLike.setImageResource(R.drawable.icon_liked_oro);

                            } else if (msg.equals("取消点赞成功")) {
                                // TODO: 2021/1/31
                                ViewUtils.setTextViewTopDrawableColor(ivDianZan, R.color.color_15);
//                            isZan = 0;
                                ivDianZan.setText("点赞");
                                ibVideoCommentActionLike.setImageResource(R.drawable.icon_comment_like);
                            }
                        }
                    }
                });
    }


    /**
     * 视频详情
     */
    private void getMovieDeatil() {
        HttpRequest.POST(me,
                BaseQuestConfig.GET_MOVIE_PLAY_DETAIL_V3,
                new Parameter().add("movie_id", movieId),
                new ResponseListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(String main, Exception error) {
                        if (error == null) {
                            JsonMap jsonMap = JsonMap.parse(main);
                            if (jsonMap.get("status").equals("1")) {
                                JsonMap data = jsonMap.getJsonMap("data");
                                //视频标题
                                tv_movie_title.setText(data.getString("title"));

                                //观看次数
                                String palyNumber = data.getString("play_num");
                                tv_movie_watch_number.setText(String.format("%s 次播放", palyNumber));
                                //简介
                                if(TextUtils.isEmpty(data.getString("content"))){
                                    tv_mp_concise.setVisibility(View.GONE);
                                }else{
                                    tv_mp_concise.setText(data.getString("content"));
                                }

                                //音乐辑是否显示
                                if (data.getString("hot_category_id").equals("0")) {
                                    ll_mp_album.setVisibility(View.GONE);
                                    return;
                                } else {
                                    // TODO: 2021/1/25 如果是专辑
                                    ll_mp_album.setVisibility(View.VISIBLE);
                                    hot_category_id = data.getString("hot_category_id");
                                    getChannelMovies();
                                }

                                //头像
                                GlideMediaLoader.loadHead(me, iv_video_author_avatar, data.getString("user_icon"));
                                //名称
                                tv_video_author_name.setText(data.getString("user_nickname"));
                                //粉丝数量
                                tv_video_author_desc.setText(data.getString("usdr_attention_count") + "订阅者");
                                //视频作者的id
                                attention_uid = data.getString("user_id");
                                //按照分类按照用户依次获取视频接口 所需的 分类id
                                if (TextUtils.isEmpty(data.getString("home_category_id"))) {
                                    ToastUtils.showShort("分类Id为空");
                                    return;
                                }else{
                                    home_category_id = data.getString("home_category_id");
                                    LogUtils.e("当前的分类id是：" + home_category_id);
                                    //相关视频
                                    getMovieByCate();
                                }
                                //视频的Url
                                play_url = data.getString("play_url");
                                //封面
                                video_img_url = data.getString("image");
                                //标题
                                video_title = data.getString("title");

                                if4K = data.getString("if4k");

                                String isFree = data.getString("is_free");

                                ImageView imageView = new ImageView(me);
                                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                                ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, UIUtils.dip2px(me, 210));
                                imageView.setLayoutParams(layoutParams);
//                                GlideMediaLoader.load(this, imageView, data.getString("image"));
                                Glide.with(me).load(data.getString("image")).into(imageView);
                                moviePlayer.setThumbImageView(imageView);
                                moviePlayer.getThumbImageViewLayout().setVisibility(View.VISIBLE);
                                if (TextUtils.equals("1", data.getString("is_free"))) {
                                    fl_cover.setVisibility(View.GONE);

                                    setPlayVideo(play_url, isFree, video_title);
                                } else {
                                    fl_cover.setVisibility(View.VISIBLE);
                                    fl_cover.setOnClickListener(v -> UIUtils.shortM("不是免费视频"));
                                }
                            }
                        }
                    }
                });

    }

    /**
     * 设置播放的Url
     */
    private void setPlayVideo(String play_url, String isFree, String title) {
        gsyVideoOption = new GSYVideoOptionBuilder();
        gsyVideoOption.setIsTouchWiget(true)
                .setRotateViewAuto(false)
                .setLockLand(false)
                .setAutoFullWithSize(true)
                .setShowFullAnimation(false)
                .setNeedLockFull(true)
                .setNeedShowWifiTip(true)
                .setCacheWithPlay(false)
                .setVideoAllCallBack(new GSYSampleCallBack() {
                    @Override
                    public void onEnterFullscreen(String url, Object... objects) {
                        super.onEnterFullscreen(url, objects);
                        GSYVideoPlayer gsyVideoPlayer = (GSYVideoPlayer) objects[1];
                        gsyVideoPlayer.setGSYVideoProgressListener((progress, secProgress, currentPosition, duration) -> {
                            if (adPlayerFragment != null) {
                                adPlayerFragment.onHostPlayerProgress(currentPosition);
                            }

                            if (!TextUtils.equals("1", isFree)) {
                                if (!Config.getLoginInfo().isMember()) {
                                    //判断是否限时播放视频
                                    // TODO: 2021/2/18 这是个空方法
                                    stopOrstartVideo(currentPosition);
                                } else {
                                    if (!memberDateMature) {
                                        //判断是否限时播放视频
                                        stopOrstartVideo(currentPosition);
                                    }
                                }
                            }
                        });
                    }

                    @Override
                    public void onPrepared(String url, Object... objects) {
                        super.onPrepared(url, objects);
                        //开始播放了才能旋转和全屏
                        orientationUtils.setEnable(true);
                        isPlay = true;
                    }

                    @Override
                    public void onQuitFullscreen(String url, Object... objects) {
                        super.onQuitFullscreen(url, objects);
                        if (orientationUtils != null) {
                            orientationUtils.backToProtVideo();
                        }
                    }

                    @Override
                    public void onClickResume(String url, Object... objects) {
                        super.onClickResume(url, objects);

                    }

                    @Override
                    public void onClickStop(String url, Object... objects) {
                        super.onClickStop(url, objects);
                        if (adPlayerFragment != null) {
                            adPlayerFragment.onHostPlayerPause();
                        }

                    }

                    @Override
                    public void onAutoComplete(String url, Object... objects) {
                        super.onAutoComplete(url, objects);
                        if (adPlayerFragment != null) {
                            adPlayerFragment.onHostPlayerComplete();
                        }

                    }
                }).setLockClickListener((view, lock) -> {
            if (orientationUtils != null) {
                //配合下方的onConfigurationChanged
                orientationUtils.setEnable(!lock);
            }
        }).setGSYVideoProgressListener((progress, secProgress, currentPosition, duration) -> {
            Log.d("VideoPlayProgress", String.format("进度：%s", progress));
            if (adPlayerFragment != null) {
                adPlayerFragment.onHostPlayerProgress(currentPosition);
            }

            if (!TextUtils.equals("1", isFree)) {
                if (!Config.getLoginInfo().isMember()) {
                    stopOrstartVideo(currentPosition);
                } else {
                    //如果会员过期
                    if (!memberDateMature) {
                        stopOrstartVideo(currentPosition);
                    }

                }
            }
        }).build(moviePlayer);
        moviePlayer.setUp(play_url, false, title);
        gsyVideoOption.setVideoTitle(title);
        moviePlayer.startPlayLogic();
    }

    /**
     * 判断会员，是否过期，扣除金币的限时操作
     *
     * @param currentPosition
     */
    public void stopOrstartVideo(int currentPosition) {
        //如果不是会员，去扣除金币在播放
//        if (movieDetailBean.gold > 0) {
//            if (currentPosition > freeTime * 1000) {
//                if (!isDeductGold) {
//                    //计算购买金币是否超过24小时
//                    long currentTimeMillis = System.currentTimeMillis();
//                    long saveTimeMillis = (long) SPUtils.get(me, Config.getLoginInfo().uid + movieId, 1568111276000l);
//                    Log.i("onlyVideo", "当前的时间：" + currentTimeMillis + "--" + saveTimeMillis);
//                    int hour = DataUtils.getOffectHour(currentTimeMillis, saveTimeMillis);
//                    Log.i("onlyVideo", "当前相差的时间：" + hour);
//                    if (hour > 24) {
//                        Log.i("onlyVideo", "去支付金币");
//                        BaseQuestStart.deductGoldAccount(me, movieDetailBean.gold + "");
//                    } else {
//                        isDeductGold = true;
//                    }
//                }
//            }
//        }
    }


    /**
     * 获取专辑详情
     */
    private void getChannelMovies() {
        HttpRequest.GET(me,
                BaseQuestConfig.GET_NEW_CHANNEL_MOVIES,
                new Parameter()
                        .add("md5", MD5Utils.md5())
                        .add("channel_id", Integer.valueOf(hot_category_id)),
                new ResponseListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(String main, Exception error) {
                        if (error == null) {
                            JsonMap jsonMap = JsonMap.parse(main);
                            if (jsonMap.get("status").equals("1")) {
                                JsonMap data = jsonMap.getJsonMap("data");
                                //专辑数量
                                tv_mp_video_num.setText(data.getInt("count") + "个视频");
                                JsonList list = data.getList("channel");
                                //专辑名称
                                tv_mp_cd_title.setText(list.getJsonMap(0).getString("title"));
                                //tv_mp_time 接口字段里面差 专辑时间
                                //专辑售价
                                tv_mp_video_money.setText("¥" + list.getJsonMap(0).getString("price"));
                                //专辑介绍
                                String channel_content = list.getJsonMap(0).getString("content");
                                //专辑拥有者的id
                                String channel_user_id = list.getJsonMap(0).getString("user_id");
                                //专辑封面
                                String channel_background_image = list.getJsonMap(0).getString("background_image");

                                // TODO: 2021/2/17 未完成 专辑的adapter
                                JsonList moviesList = data.getList("movies");

                                mChannelAdapter = new RvAuthorPortfolioAdapter(moviesList);
                                rv_author_portfolio.setAdapter(mChannelAdapter);
                                mChannelAdapter.setContrastId(movieId);
                                mChannelAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {

//                                        JsonMap jm = mChannelAdapter.getData().get(position);
//                                        String channelUrl = jm.getString("play_url");
                                        mChannelAdapter.setPos(position);
                                        // TODO: 2021/2/19  点击所得的下标 Url
                                        String channelUrl = moviesList.getJsonMap(position).getString("play_url");

                                    }
                                });
                            }
                        }
                    }
                });
    }

    /**
     * 查看影片是否点赞或者收藏
     */
    private void getCheckTheMovieFavoritesAndLike() {
        HttpRequest.POST(me,
                BaseQuestConfig.GET_NEW_MOVIE_ACTION_STATES,
                new Parameter()
                        .add("movie_id", movieId)
                        .add("uid", Config.getLoginInfo().uid),
                new ResponseListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(String main, Exception error) {
                        if (error == null) {
                            JsonMap jsonMap = JsonMap.parse(main);
                            if (jsonMap.get("status").equals("1")) {
                                JsonMap data = jsonMap.getJsonMap("data");
                                int zan = data.getInt("top");
                                int like = data.getInt("like");
                                // "top": "1",     //0是不赞  1=赞  2=还未进行点赞操作
                                //        "like": 0       //0是未收藏   1=已收藏
                                switch (zan) {
                                    case 2:
                                        ViewUtils.setTextViewTopDrawableColor(ivDianZan, R.color.color_15);
                                        isZan = 2;
                                        ivDianZan.setText("点赞");
                                        ibVideoCommentActionLike.setImageResource(R.drawable.icon_comment_like);
                                        break;
                                    case 1:
                                        ViewUtils.setTextViewTopDrawableColor(ivDianZan, R.color.color_FF9600);
                                        isZan = 1;
                                        ivDianZan.setText("已赞");
                                        ibVideoCommentActionLike.setImageResource(R.drawable.icon_liked_oro);
                                        break;

                                    case 0:
                                        ViewUtils.setTextViewTopDrawableColor(ivDianZan, R.color.color_15);
                                        isZan = 0;
                                        ivDianZan.setText("点赞");
                                        ibVideoCommentActionLike.setImageResource(R.drawable.icon_comment_like);
                                        break;
                                    default:
                                        break;
                                }

                                if (like == 0) {
                                    ivShouCang.setText("收藏");
                                    ViewUtils.setTextViewTopDrawableColor(ivDianZan, R.color.color_15);
                                    ibVideoCommentActionCollect.setImageResource(R.drawable.icon_comment_collect);
                                } else {
                                    ivShouCang.setText("已收藏");
                                    ViewUtils.setTextViewTopDrawableColor(ivDianZan, R.color.color_FF9600);
                                    ibVideoCommentActionCollect.setImageResource(R.drawable.icon_oro_collection);
                                }

                            }
                        }
                    }
                });

    }


    /**
     * 这里用此项目自带的请求方式，暂时不用超哥的baseJson
     * 当期分类按照用户依次获取视频
     * num : 需要的视频数量，默认为 1 我传入2
     */
    private void getMovieByCate() {

//            HttpRequest.GET(me,
//                    BaseQuestConfig.GET_NEW_MOVIE_BY_CATE,
//                    new Parameter()
//                            .add("md5", MD5Utils.md5())
//                            .add("cate_id", Integer.valueOf(home_category_id))
//                            .add("num", 1),
//
//                    new ResponseListener() {
//                        @Override
//                        public void onResponse(String main, Exception error) {
//                            if(error == null){
//                                JsonMap jsonMap = JsonMap.parse(main);
//                                if(jsonMap.getString("status").equals("1")){
//                                    JsonList data = jsonMap.getList("data");
//                                    adapter = new RelatedVideoAdapter(data);
//                                    rv_be_interested_video.setAdapter(adapter);
//                                }
//                            }
//                        }
//                    }
//            );
            // TODO: 2021/2/14 进入页面后，随机的相关视频多少个
            BaseQuestStart.getMovieByCateV3(Integer.valueOf(home_category_id), 2, new NetRequestListenerProxy(me) {
                @Override
                public void nofityUpdate(int i, BaseBean baseBean) {
                    if (baseBean.status == 1) {
                        byCateV3Beans = baseBean.Data();
                        mAdapter = new RelatedVideoV3Adapter(byCateV3Beans);
                        mAdapter.setNewData(baseBean.Data());
                        mAdapter.setOnItemClickListener(MoviePlayerV3Activity.this);
                    }

                }
            });

    }


    /**
     * 添加下载记录
     */
    private void getDownloadRecords() {
        HttpRequest.POST(me,
                BaseQuestConfig.GET_NEW_ADD_DOWNLOAD_RECORD,
                new Parameter()
                        .add("uid", Integer.parseInt(Config.getLoginInfo().uid))
                        .add("movie_id", Integer.parseInt(movieId)),
                new ResponseListener() {
                    @Override
                    public void onResponse(String main, Exception error) {
                        if (error == null) {
                            JsonMap jsonMap = JsonMap.parse(main);
                            if (jsonMap.get("status").equals("1")) {
                                String downloadRecordsMsg = jsonMap.getString("msg");
                                if (downloadRecordsMsg.equals("success")) {
                                    ToastUtils.showShort(jsonMap.getString("data"));
                                }
                            }
                        }
                    }
                });
    }


    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {

    }

    @Override
    protected void onPause() {
        moviePlayer.getCurrentPlayer().onVideoPause();
        super.onPause();
        isPause = true;
    }

    @Override
    protected void onResume() {
        moviePlayer.getCurrentPlayer().onVideoResume(false);
        super.onResume();
        isPause = false;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        //如果旋转了就全屏
        if (isPlay && !isPause) {
            moviePlayer.onConfigurationChanged(this, newConfig, orientationUtils, true, true);
        }
    }

    @Override
    public void onBackPressed() {
        if (orientationUtils != null) {
            orientationUtils.backToProtVideo();
        }
        if (GSYVideoManager.backFromWindowFull(this)) {
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {

//        if (isPlay && moviePlayer != null) {
//            moviePlayer.getCurrentPlayer().release();
//        }
        moviePlayer.getGSYVideoManager().setListener(moviePlayer.getGSYVideoManager().lastListener());
        moviePlayer.getGSYVideoManager().setLastListener(null);
        GSYVideoManager.releaseAllVideos();
        if (orientationUtils != null) {
            orientationUtils.releaseListener();
        }
        SwitchUtil.release();
        super.onDestroy();
//        unregisterReceiver(receiver);
    }

}