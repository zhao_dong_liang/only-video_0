package cn.wu1588.dancer.home.mode;

/**
 * Created by wangchao on 2018-12-11.
 */
public class BannerBean {
    //"id": "2",
    //		"image": "https:\/\/test.cnsunrun.com\/qijushiping\/Uploads\/2019-01-24\/5c499e892be6f.jpg",
    //		"url": "www.google.com",
    //		"sort": "2",
    //		"add_time": "0000-00-00 00:00:00",
    //		"update_time": "2019-01-24 19:16:25",
    //		"is_check": "home1",
    //		"is_del": "0"
    public String id;
    public String image;
    public String url;
    public String sort;
    public String add_time;
    public String update_time;
    public String is_check;
    public String is_del;

    @Override
    public String toString() {
        return image;
    }
}
