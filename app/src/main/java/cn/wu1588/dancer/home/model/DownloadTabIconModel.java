package cn.wu1588.dancer.home.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.sunrun.sunrunframwork.utils.MD5Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.model.TabBar;
import io.rong.common.FileUtils;

//管理和下载导航的选中未选中图片
public class DownloadTabIconModel {
    private  ExecutorService executorService;
    private List<TabBar> tabBarList;
    private int total;
    private Context context;
    private int index;
    private final String TAG=DownloadTabIconModel.class.getName();

    private ArrayList<File> files;

    private DownloadTabIconCall downloadTabIconCall;

    private int wh;
    public DownloadTabIconModel(List<TabBar> tabBarList, Context context){
        if (tabBarList!=null&&tabBarList.size()>0){
            wh= (int) context.getResources().getDimension(R.dimen.dp_89);
            this.context=context;
            total=tabBarList.size()*2;
            this.tabBarList=tabBarList;
            /*创建可用线程数量的固定线程池*/
            executorService =  Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        }

    }

    public void setDownloadTabIconCall(DownloadTabIconCall downloadTabIconCall) {
        this.downloadTabIconCall = downloadTabIconCall;
    }
    Handler handler=new Handler(Looper.myLooper()){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (downloadTabIconCall!=null){
                downloadTabIconCall.downloadFinal();
            }
        }
    };
    public void startThead(){
        if (executorService!=null){
            for (int i = 0; i < tabBarList.size(); i++) {
                TabBar tabBar = tabBarList.get(i);
                downloadPic(tabBar.selected_img);
                downloadPic(tabBar.unselected_img);
            }
            executorService.shutdown();
        }
    }
    private void downloadPic(String picUrl){
        String picFileName = getPicFileName(picUrl);
        File picfile= new File(getPicFilePath(),picFileName);
        if (picfile.exists()){
            finallyMet(getPicFileName(picUrl));
            return;
        }
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    byte[] bytes = Glide.with(context)
                            .load(picUrl)
                            .asBitmap()
                            .toBytes()
                            .into(wh, wh)
                            .get();
                    if (bytes!=null&&bytes.length>0){
                        File file = FileUtils.byte2File(bytes, getPicFilePath(), getPicFileName(picUrl));
                        if (file.exists()){
                            Log.e(TAG,"下载导航图片成功："+picUrl+"\n"+file.toString());
                        }
                        if (files==null){
                            files=new ArrayList<>();
                        }
                        files.add(file);
                        finallyMet(getPicFileName(picUrl));
                    }
                } catch (ExecutionException e) {
                    Log.e(TAG,"下载导航图片异常："+picUrl+"\n"+e.toString());
                    e.printStackTrace();
                    finallyMet(getPicFileName(picUrl));
                } catch (InterruptedException e) {
                    Log.e(TAG,"下载导航图片异常："+picUrl+"\n"+e.toString());
                    e.printStackTrace();
                    finallyMet(getPicFileName(picUrl));
                }
            }
        });
    }

    private  void finallyMet(String picName){
        Bitmap bitmap = BitmapFactory.decodeFile(getPicFilePath()+File.separator+picName);
        CommonApp.getApplication().getTabBitmapMap().put(picName,bitmap);
        index++;
        if (index>=total){
         handler.sendEmptyMessage(1);
        }
    }
    public String getPicFileName(String picUrl){
        return MD5Utils.md5(picUrl) + ".png";
    }
    public String getPicFilePath(){
        return  FileUtils.getCachePath(context,"tabPic");
    }
    public interface  DownloadTabIconCall{
        void downloadFinal();
    }
}
