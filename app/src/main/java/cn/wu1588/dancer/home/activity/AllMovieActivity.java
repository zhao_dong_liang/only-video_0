package cn.wu1588.dancer.home.activity;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.View;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemChildClickListener;
import com.cnsunrun.commonui.widget.titlebar.TitleBar;
import com.flyco.tablayout.SlidingTabLayout;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.utils.EmptyDeal;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.wu1588.dancer.adp.ScreenHeaderAdapter;
import cn.wu1588.dancer.home.fragment.MovieFragment;
import cn.wu1588.dancer.home.mode.HeaderScreenBean;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.base.ViewPagerFragmentAdapter;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.RecycleHelper;


import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_HEDER_SCREEN_CODE;

/**
 * 全部影片
 */
public class AllMovieActivity extends LBaseActivity {

    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.recyclerViewSelectOne)
    RecyclerView recyclerViewSelectOne;
    @BindView(R.id.magicIndicator)
    SlidingTabLayout magicIndicator;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    private List<HeaderScreenBean.HomeCategoryBean> home_category;
    public String screen_id;
    public String home_category_id;
    public int recommend_category_id;
    private ArrayList<Fragment> baseFragments;
    private List<String> mTitles;
    private ViewPagerFragmentAdapter mVPAdapter;
    private ScreenHeaderAdapter screenHeaderAdapter;
    private String style_type;
    private int pos;
    private MovieFragment movieFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_movie);
        ButterKnife.bind(this);
        style_type = getIntent().getStringExtra("style_type");
        screen_id = getIntent().getStringExtra("screen_id");
        home_category_id = getIntent().getStringExtra("home_category_id");
        initViews();
        initData();
    }

    private void initData() {
        BaseQuestStart.getHederScreen(that);
    }

    private void initViews() {
        if ("hot_moive".equals(style_type) || "is_free".equals(style_type)) {
            recyclerViewSelectOne.setVisibility(View.GONE);
            if ("hot_moive".equals(style_type)) {
                titleBar.setTitle("重磅热播");
            } else {
                titleBar.setTitle("免费区");
            }
        } else {
            recyclerViewSelectOne.setVisibility(View.VISIBLE);

        }
        baseFragments = new ArrayList<>();
        mTitles = new ArrayList<>();
        mVPAdapter = new ViewPagerFragmentAdapter(this.getSupportFragmentManager());
        RecycleHelper.setLinearLayoutManager(recyclerViewSelectOne, LinearLayoutManager.HORIZONTAL);
        screenHeaderAdapter = new ScreenHeaderAdapter();
        recyclerViewSelectOne.setAdapter(screenHeaderAdapter);
        recyclerViewSelectOne.addOnItemTouchListener(new OnItemChildClickListener() {
            @Override
            public void onSimpleItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                List<HeaderScreenBean.ScreenBean> data = screenHeaderAdapter.getData();
                for (int i = 0; i < data.size(); i++) {
                    if (i == position) {
                        data.get(i).isSelect = true;
                        screen_id = data.get(position).screen_id;

                        MovieFragment fragment = (MovieFragment) baseFragments.get(viewPager.getCurrentItem());
                        fragment.updateDate();
                    } else {
                        data.get(i).isSelect = false;
                    }
                }
                screenHeaderAdapter.notifyDataSetChanged();
            }
        });

        titleBar.setRightAction(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonIntent.startSearchActivity(that);
            }
        });
    }


    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode) {
            case GET_HEDER_SCREEN_CODE:
                if (bean.status == 1) {
                    HeaderScreenBean headerScreenBean = bean.Data();
                    updateUI(headerScreenBean);
                }
                break;
        }
        super.nofityUpdate(requestCode, bean);
    }

    private void updateUI(HeaderScreenBean headerScreenBean) {
        home_category = headerScreenBean.home_category;
        List<HeaderScreenBean.ScreenBean> screen = headerScreenBean.screen;
        if ("newest_moive".equals(style_type)) {
            for (int i = 0; i < screen.size(); i++) {
                if (TextUtils.isEmpty(screen_id)) {
                    screen.get(0).isSelect = true;
                } else {
                    if (screen.get(i).title.equals("最近更新")) {
                        screen.get(i).isSelect = true;
                    }
                }
            }
        }
        screenHeaderAdapter.setNewData(screen);
        if (!EmptyDeal.isEmpy(home_category)) {
            for (int i = 0; i < home_category.size(); i++) {
                movieFragment = MovieFragment.newInstance(home_category.get(i).id);
                baseFragments.add(movieFragment);
                mTitles.add(home_category.get(i).title);
                if (home_category_id.equals(home_category.get(i).id)) {
                    pos = i;
                }
            }
        }
        mVPAdapter.setFragments(baseFragments);
        magicIndicator.setViewPager(viewPager, mTitles.toArray(new String[home_category.size()]), this, baseFragments);
        viewPager.setCurrentItem(pos);
    }
}
