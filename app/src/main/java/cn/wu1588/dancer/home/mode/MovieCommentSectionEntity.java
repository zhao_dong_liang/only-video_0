package cn.wu1588.dancer.home.mode;

import com.chad.library.adapter.base.entity.SectionEntity;

public class MovieCommentSectionEntity   extends SectionEntity<MovieCommentBean> {
    public MovieCommentSectionEntity(boolean isHeader, String header) {
        super(isHeader, header);
    }
    public MovieCommentSectionEntity(MovieCommentBean t) {
        super(t);
    }
}
