package cn.wu1588.dancer.home.mode;

public class GoldListBean {

    /**
     * id : 27
     * title : 金币套餐1
     * quantity : 10
     * price : 10.00
     * discount_price : 8.00
     * add_time : 2019-09-07 11:18:57
     * update_time : 2019-09-07 11:33:40
     * is_del : 0
     * sort : 1
     * is_commend : 0
     * url : null
     */

    private String id;
    private String title;
    private String quantity;
    private String price;
    private String discount_price;
    private String add_time;
    private String update_time;
    private String is_del;
    private String sort;
    private String is_commend;
    private Object url;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDiscount_price() {
        return discount_price;
    }

    public void setDiscount_price(String discount_price) {
        this.discount_price = discount_price;
    }

    public String getAdd_time() {
        return add_time;
    }

    public void setAdd_time(String add_time) {
        this.add_time = add_time;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getIs_del() {
        return is_del;
    }

    public void setIs_del(String is_del) {
        this.is_del = is_del;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getIs_commend() {
        return is_commend;
    }

    public void setIs_commend(String is_commend) {
        this.is_commend = is_commend;
    }

    public Object getUrl() {
        return url;
    }

    public void setUrl(Object url) {
        this.url = url;
    }
}
