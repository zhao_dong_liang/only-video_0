package cn.wu1588.dancer.home.mode;

import java.util.List;

import cn.wu1588.dancer.common.util.DataUtils;

public class MovieDetailBean {

    /**
     * adcenter : [{"image":"Uploads/Banner/5cab0b205e736.jpg","url":"http://www.baidu.com"}]
     * add_time : 2019-03-28 10:12:29
     * comment : [{"add_time":"1天前","child":[{"content":"131***360回复兔子**:我脑壳好疼啊"},{"content":"131***360:我脑壳好疼啊"},{"content":"131***360回复134***155:还不能发表情，靠"},{"content":"131***360:哈哈哈"},{"content":"134***155回复兔子**:a136514a"},{"content":"广州***回复兔子**:九三七一七九九五九"},{"content":"兔子**:恩"}],"content":"留言给我，腹肌颜值","gender":"2","icon":"www.code.cn/duboshiping_app/Uploads/Avatar/2019-04-02/5ca2b3d15a5e2.jpg","like_num":"3","me_id":"56","mem_id":"122","nickname":"广州***","pid":"0","reply_id":"0","status":0},{"add_time":"3小时前","child":[],"content":"视频真刺激啊","gender":"1","icon":"www.code.cn/duboshiping_app/Uploads/Avatar/2019-04-02/5ca2c9f50fb3f.jpg","like_num":"0","me_id":"61","mem_id":"1247","nickname":"兔子**","pid":"0","reply_id":"0"},{"add_time":"37分钟前","child":[],"content":"我脑壳好疼啊","gender":"1","icon":"www.code.cn/duboshiping_app/22222222222","like_num":"0","me_id":"57","mem_id":"1248","nickname":"131***360","pid":"0","reply_id":"0"}]
     * comment_num : 23
     * cpl : 88.69％
     * label : ["热门","家庭剧","简体中文"]
     * play_num : 12
     * title : 影片3
     * youlike : [{"id":"3","image":"Uploads/Movie/5c9db9c9bfaac.jpg","label":["热门","家庭剧","简体中文"],"play_num":"12","play_url":"uploads/aaa/bbb","title":"影片3"},{"id":"9","image":"http://103.85.85.135:456/images/%E4%B8%8D%E8%AE%BA%E4%B9%B1%E8%BD%AE-10.jpg ","label":["国产","热门"],"play_num":"0","play_url":"http://shuchujzxia.com:5818/20190331/EFXzGeVC/index.m3u8","title":"波多野结衣--和嫂子同居的日子"},{"id":"10","image":"http://juzishipinpic.com:456/images/bulunluanlun/%E4%B8%8D%E8%AE%BA%E4%B9%B1%E8%BD%AE-2.jpg","label":["家庭剧","简体中文"],"play_num":"0","play_url":"http://shuchujzxia.com:5818/20190331/YENabV5E/index.m3u8","title":"小泽玛利亚--和嫂子同居的日子"},{"id":"11","image":"http://juzishipinpic.com:456/images/bulunluanlun/%E4%B8%8D%E8%AE%BA%E4%B9%B1%E8%BD%AE-3.jpg","label":["安国","写真"],"play_num":"199","play_url":"http://shuchujzxia.com:5818/20190331/YENabV5E/index.m3u8","title":"西山希--和嫂子同居的日子"},{"id":"12","image":"http://juzishipinpic.com:456/images/bulunluanlun/%E4%B8%8D%E8%AE%BA%E4%B9%B1%E8%BD%AE-14.jpg","label":["家庭剧","简体中文"],"play_num":"230","play_url":"http://shuchujzxia.com:5999/20190409/DTTbr4pJ/index.m3u8","title":"我要测试视频"},{"id":"13","image":"http://juzishipinpic.com:456/images/bulunluanlun/%E4%B8%8D%E8%AE%BA%E4%B9%B1%E8%BD%AE-15.jpg","label":["家庭剧","简体中文"],"play_num":"214","play_url":"http://shuchujzxia.com:5818/20190331/YENabV5E/index.m3u8","title":"武藤绫香--和嫂子同居的日子"},{"id":"14","image":"http://juzishipinpic.com:456/images/bulunluanlun/%E4%B8%8D%E8%AE%BA%E4%B9%B1%E8%BD%AE-16.jpg","label":["家庭剧","简体中文"],"play_num":"243","play_url":"http://shuchujzxia.com:5818/20190331/YENabV5E/index.m3u8","title":"有村千佳--和嫂子同居的日子"},{"id":"15","image":"http://103.85.85.135:456/images/%E4%B8%8D%E8%AE%BA%E4%B9%B1%E8%BD%AE-7.jpg","label":["家庭剧","简体中文"],"play_num":"253","play_url":"http://shuchujzxia.com:5818/20190331/YENabV5E/index.m3u8","title":"森咲满--和嫂子同居的日子"},{"id":"16","image":"http://103.85.85.135:456/images/%E4%B8%8D%E8%AE%BA%E4%B9%B1%E8%BD%AE-8.jpg","label":["家庭剧","简体中文"],"play_num":"153","play_url":"http://shuchujzxia.com:5818/20190331/YENabV5E/index.m3u8","title":"森野明音--和嫂子同居的日子"},{"id":"17","image":"http://103.85.85.135:456/images/%E4%B8%8D%E8%AE%BA%E4%B9%B1%E8%BD%AE-9.jpg","label":["家庭剧","简体中文"],"play_num":"278","play_url":"http://shuchujzxia.com:5818/20190331/YENabV5E/index.m3u8","title":"森川真羽--和嫂子同居的日子"}]
     */

    public String add_time;
    public String comment_num;
    public float cpl;
    public int play_num;
    public String play_url;
    public String title;
    public String content;
    public String shaer_content;
    public List<AdcenterBean> adcenter;
    public List<CommentBean> comment;
    public List<String> label;
    public List<YoulikeBean> youlike;
    public int gold;
    public String username;
    public String icon;
    public String user_id;
    public String image;
    public String top_num;
    public String down_num;
    public String shared_num;
    public String download_num;
    public String collection_num;
    public String dealPlayNum(){
        if (play_num>1000){
            return DataUtils.div(play_num,10000,1) +"万";
        }else {
            return String.valueOf(play_num);
        }
    }

    public static class AdcenterBean {
        /**
         * image : Uploads/Banner/5cab0b205e736.jpg
         * url : http://www.baidu.com
         */

        public String image;
        public String url;
    }

    public static class CommentBean {
        /**
         * add_time : 1天前
         * child : [{"content":"131***360回复兔子**:我脑壳好疼啊"},{"content":"131***360:我脑壳好疼啊"},{"content":"131***360回复134***155:还不能发表情，靠"},{"content":"131***360:哈哈哈"},{"content":"134***155回复兔子**:a136514a"},{"content":"广州***回复兔子**:九三七一七九九五九"},{"content":"兔子**:恩"}]
         * content : 留言给我，腹肌颜值
         * gender : 2
         * icon : www.code.cn/duboshiping_app/Uploads/Avatar/2019-04-02/5ca2b3d15a5e2.jpg
         * like_num : 3
         * me_id : 56
         * mem_id : 122
         * nickname : 广州***
         * pid : 0
         * reply_id : 0
         * status : 0
         */

        public String add_time;
        public String content;
        public String gender;
        public String icon;
        public String like_num;
        public String me_id;
        public String mem_id;
        public String nickname;
        public String pid;
        public String reply_id;
        public int status;
        public List<ChildBean> child;


        public static class ChildBean {
            /**
             * content : 131***360回复兔子**:我脑壳好疼啊
             */

            public String content;

        }
    }

    public static class YoulikeBean {
        /**
         * id : 3
         * image : Uploads/Movie/5c9db9c9bfaac.jpg
         * label : ["热门","家庭剧","简体中文"]
         * play_num : 12
         * play_url : uploads/aaa/bbb
         * title : 影片3
         */

        public String id;
        public String image;
        public int play_num;
        public String play_url;
        public String title;
        public String grade;
        public List<String> label;
        public int gold;
        public String dealPlayNum(){
            if (play_num>1000){
                return DataUtils.div(play_num,10000,1) +"万";
            }else {
                return String.valueOf(play_num);
            }
        }
    }
}
