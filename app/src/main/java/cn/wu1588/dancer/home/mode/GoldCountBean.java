package cn.wu1588.dancer.home.mode;

public class GoldCountBean {

    /**
     * member_id : 1
     * gold_account : 0
     */

    private String member_id;
    private int gold_account;

    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    public int getGold_account() {
        return gold_account;
    }

    public void setGold_account(int gold_account) {
        this.gold_account = gold_account;
    }
}
