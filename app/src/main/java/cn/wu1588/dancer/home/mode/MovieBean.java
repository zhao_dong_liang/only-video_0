package cn.wu1588.dancer.home.mode;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.chad.library.adapter.base.entity.MultiItemEntity;

import java.util.List;

import cn.wu1588.dancer.common.util.DataUtils;

public class MovieBean implements Parcelable, MultiItemEntity  {

    public MovieBean() {
    }

    /**
     * id : 1
     * title : 影片1
     * image : uploads/aaa/bbb
     * play_url : uploads/aaa/bbb
     * label : 1,2
     * play_num : 12
     * grade : 8.1
     * label_str : [{"id":"1","pid":"0","title":"热门","sort":"0","add_time":null,"update_time":"2019-03-28 11:52:54","is_hid":"0","is_del":"0"},{"id":"2","pid":"0","title":"家庭剧","sort":"0","add_time":null,"update_time":"2019-03-28 11:53:10","is_hid":"0","is_del":"0"}]
     */

    public int id;


    protected MovieBean(Parcel in) {
        id = in.readInt();
        title = in.readString();
        image = in.readString();
        play_url = in.readString();
        play_time = in.readString();
        label = in.readString();
        play_num = in.readInt();
        grade = in.readString();
        mem_id = in.readString();
        share_content = in.readString();
        is_free = in.readString();
        mo_id = in.readInt();
        is_like = in.readInt();
        home_category = in.readString();
        gold = in.readInt();
        username = in.readString();
        icon = in.readString();
        down_num=in.readString();
        comment_num=in.readString();
        top_num=in.readInt();
        download_url=in.readString();
        user_id=in.readString();
        download_num=in.readInt();
        share_num=in.readInt();
        adcenter = in.createTypedArrayList(AdCenter.CREATOR);
        type = in.readString();
    }

    public static final Creator<MovieBean> CREATOR = new Creator<MovieBean>() {
        @Override
        public MovieBean createFromParcel(Parcel in) {
            return new MovieBean(in);
        }

        @Override
        public MovieBean[] newArray(int size) {
            return new MovieBean[size];
        }
    };

    @Override
    public int hashCode() {
        return mo_id;
    }

    @Override
    public String toString() {
        return "MovieBean{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", image='" + image + '\'' +
                ", play_url='" + play_url + '\'' +
                ", play_time='" + play_time + '\'' +
                ", label='" + label + '\'' +
                ", play_num=" + play_num +
                ", grade='" + grade + '\'' +
                ", mem_id='" + mem_id + '\'' +
                ", share_content='" + share_content + '\'' +
                ", is_free='" + is_free + '\'' +
                ", mo_id=" + mo_id +
                ", is_like=" + is_like +
                ", label_str=" + label_str +
                ", home_category='" + home_category + '\'' +
                ", gold=" + gold +
                ", username='" + username + '\'' +
                ", icon='" + icon + '\'' +
                ", down_num='" + down_num + '\'' +
                ", comment_num='" + comment_num + '\'' +
                ", top_num=" + top_num +
                ", download_url='" + download_url + '\'' +
                ", user_id='" + user_id + '\'' +
                ", download_num=" + download_num +
                ", share_num=" + share_num +
                ", adcenter=" + adcenter +
                ", type='" + type + '\'' +
                ", content_type='" + content_type + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof MovieBean){
            return  hashCode()==obj.hashCode();
        }
        return super.equals(obj);
    }
    public String title;
    public String image;
    public String play_url;
    public String play_time;
    public String label;
    public int play_num;
    public String grade;
    public String mem_id;
    public String share_content;
    public String is_free;
    public int mo_id;
    public int is_like;
    public List<LabelStrBean> label_str;
    public String home_category;
    public int gold;
    public String username;
    public String icon;
    public String down_num;
    public String comment_num;
    public int top_num;
    public String download_url;
    public String user_id;
    public int download_num;
    public int share_num;
    public List<AdCenter> adcenter;
    public String type = "movie";
    public String content_type;
//        "if4k": "0",//是否4K
    public String if4k;

    public String getContent_type() {
        return content_type;
    }

    public void setContent_type(String content_type) {
        this.content_type = content_type;
    }

    public String dealPlayNum(){
        if (play_num>1000){
            return DataUtils.div(play_num,10000,1) +"万";
        }else {
            return String.valueOf(play_num);
        }
    }
    public boolean isFree(){
        return TextUtils.equals("1",is_free) || gold <= 0;
    }

    public boolean getIsLike(){
        return top_num > 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(title);
        parcel.writeString(image);
        parcel.writeString(play_url);
        parcel.writeString(play_time);
        parcel.writeString(label);
        parcel.writeInt(play_num);
        parcel.writeString(grade);
        parcel.writeString(mem_id);
        parcel.writeString(share_content);
        parcel.writeString(is_free);
        parcel.writeInt(mo_id);
        parcel.writeInt(is_like);
        parcel.writeString(home_category);
        parcel.writeInt(gold);
        parcel.writeString(username);
        parcel.writeString(icon);
        parcel.writeString(down_num);
        parcel.writeString(comment_num);
        parcel.writeInt(top_num);
        parcel.writeString(download_url);
        parcel.writeString(user_id);
        parcel.writeInt(download_num);
        parcel.writeInt(share_num);
        parcel.writeTypedList(adcenter);
        parcel.writeString(type);
        parcel.writeString(content_type);
        parcel.writeString(if4k);
    }

    @Override
    public int getItemType() {
        return 1;
    }


    public static class LabelStrBean {
        /**
         * id : 1
         * pid : 0
         * title : 热门
         * sort : 0
         * add_time : null
         * update_time : 2019-03-28 11:52:54
         * is_hid : 0
         * is_del : 0
         */

        public String id;
        public String pid;
        public String title;
        public String sort;
        public String add_time;
        public String update_time;
        public String is_hid;
        public String is_del;


    }

    public static class AdCenter implements Parcelable {

        public String image;
        public String url;

        public AdCenter() {
        }

        protected AdCenter(Parcel in) {
            image = in.readString();
            url = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(image);
            dest.writeString(url);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<AdCenter> CREATOR = new Creator<AdCenter>() {
            @Override
            public AdCenter createFromParcel(Parcel in) {
                return new AdCenter(in);
            }

            @Override
            public AdCenter[] newArray(int size) {
                return new AdCenter[size];
            }
        };
    }
}
