package cn.wu1588.dancer.home.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import cn.wu1588.dancer.R;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.utils.EmptyDeal;

import java.util.List;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import cn.wu1588.dancer.channel.mode.ThemeDetailBean;
import cn.wu1588.dancer.adp.MoreHomeClassifiAdapter;
import cn.wu1588.dancer.home.mode.ThematicBean;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.PageLimitDelegate;
import cn.wu1588.dancer.common.util.RecycleHelper;
import cn.wu1588.dancer.common.widget.ArcImageView;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;
import cn.wu1588.dancer.adp.MoreClassifiAdapter;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_HEDER_INFO_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_THEMATIC_DETAIL_CODE;

/**
 * 专题详情
 */
public class HomeMoreClassifiListActivity extends LBaseActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    @BindView(R.id.titleBar)
    TitleBar titleBar;
    private GridLayoutManager gridLayoutManager;
    private String id;
    private String from_type;
    private ArcImageView arcImg;
    private TextView tvTitle;
    private TextView tvContent;
    private MoreClassifiAdapter moreClassifiAdapter;
    private MoreHomeClassifiAdapter moreHomeClassifiAdapter;
    private int typeid;//类别id   1=代表必看专题   2=代表热门专题

    PageLimitDelegate pageLimitDelegate = new PageLimitDelegate(new PageLimitDelegate.DataProvider() {
        @Override
        public void loadData(int page) {
//            if (TextUtils.equals(Const.FROM_CHANNEL,from_type)) {
//                BaseQuestStart.getThematicDetail(that, id, typeid, page);
//            } else {
//                BaseQuestStart.getHomeThematicDetail(that, id, page);
//            }
            BaseQuestStart.getThematicDetail(that, id, typeid, page);
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_more_classify);
        ButterKnife.bind(this);
        initData();
        initViews();
    }

    private void initData() {
        id = getIntent().getStringExtra("id");
        from_type = getIntent().getStringExtra("from_type");
        typeid = getIntent().getIntExtra("typeid", 0);
        BaseQuestStart.getHeaderInfo(that, id);
    }

    private void initViews() {
        RecycleHelper.setHasHeaderGridLayoutManager(recyclerView, 3, true);
//        if (TextUtils.equals(Const.FROM_CHANNEL, from_type)) {
        moreClassifiAdapter = new MoreClassifiAdapter();
        moreClassifiAdapter.addHeaderView(getHeaderView(recyclerView));
        recyclerView.setAdapter(moreClassifiAdapter);
        pageLimitDelegate.attach(refreshLayout, recyclerView, moreClassifiAdapter);
        moreClassifiAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                ThemeDetailBean item = moreClassifiAdapter.getItem(position);
                CommonIntent.startMoviePlayerActivity(that, item.id);
            }
        });
//        } else {
//            moreHomeClassifiAdapter = new MoreHomeClassifiAdapter();
//            moreHomeClassifiAdapter.addHeaderView(getHeaderView(recyclerView));
//            recyclerView.setAdapter(moreHomeClassifiAdapter);
//            pageLimitDelegate.attach(refreshLayout, recyclerView, moreHomeClassifiAdapter);
//            moreHomeClassifiAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
//                @Override
//                public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
//                    HomeThematicDetailBean.ListBean item = moreHomeClassifiAdapter.getItem(position);
//                    CommonIntent.startMoviePlayerActivity(that, item.id);
//                }
//            });
//        }

    }

    private View getHeaderView(RecyclerView v) {
        View convertView = LayoutInflater.from(this).inflate(R.layout.activity_home_more_classify_head, (ViewGroup) v.getParent(), false);
        arcImg = convertView.findViewById(R.id.arcImg);
        tvTitle = convertView.findViewById(R.id.tvtTitle);
        tvContent = convertView.findViewById(R.id.tvContent);
        return convertView;
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode) {
            case GET_THEMATIC_DETAIL_CODE:
                if (bean.status == 1) {
                    List<ThemeDetailBean> themeDetailBean = bean.Data();
                    pageLimitDelegate.setData(themeDetailBean);
                }
                break;
            case GET_HEDER_INFO_CODE:
                if (bean.status == 1) {
                    ThematicBean thematicBean = bean.Data();
                    setHederData(thematicBean);
                }
                break;
        }
        super.nofityUpdate(requestCode, bean);
    }

    private void setHederData(ThematicBean thematicBean) {
        if (!EmptyDeal.isEmpy(thematicBean)) {
            tvTitle.setText(thematicBean.title);
            tvContent.setText(thematicBean.content);
            GlideMediaLoader.loadLargeImg(that, arcImg, thematicBean.background_image, R.drawable.ic_place_arcimg);
        }

    }

}
