package cn.wu1588.dancer.home.mode;

public class NavbarColorBean {

  private    String up_b_color;
    private String up_e_color;
    private   String down_b_color;
    private   String down_e_color;
    private int status_bar_color;
    private int search_bar_show;
    private String logo_img;
    /**
     * up_b_color	string	上导航栏起始颜色
     * up_e_color	string	上导航栏结束颜色
     * down_b_color	string	下导航栏起始颜色
     * down_e_color	string	下导航栏结束颜色
     * status_bar_color	string	0:黑色1：白色
     * search_bar_show 首页是否显示搜索栏
     * logo_img 首页logo
     */

    public String getUp_b_color() {
        return up_b_color;
    }

    public void setUp_b_color(String up_b_color) {
        this.up_b_color = up_b_color;
    }

    public String getUp_e_color() {
        return up_e_color;
    }

    public void setUp_e_color(String up_e_color) {
        this.up_e_color = up_e_color;
    }

    public String getDown_b_color() {
        return down_b_color;
    }

    public void setDown_b_color(String down_b_color) {
        this.down_b_color = down_b_color;
    }

    public String getDown_e_color() {
        return down_e_color;
    }

    public void setDown_e_color(String down_e_color) {
        this.down_e_color = down_e_color;
    }

    public int getStatus_bar_color() {
        return status_bar_color;
    }

    public void setStatus_bar_color(int status_bar_color) {
        this.status_bar_color = status_bar_color;
    }

    public int getSearch_bar_show() {
        return search_bar_show;
    }

    public void setSearch_bar_show(int search_bar_show) {
        this.search_bar_show = search_bar_show;
    }

    public String getLogo_img() {
        return logo_img;
    }

    public void setLogo_img(String logo_img) {
        this.logo_img = logo_img;
    }
}