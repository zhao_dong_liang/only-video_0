package cn.wu1588.dancer.home.provider;



import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.provider.BaseItemProvider;


import cn.wu1588.dancer.adp.HomeNewAdapter;
import cn.wu1588.dancer.home.mode.HomeBean;

/**
 * 限时免费 颜色跳动
 */
public class FreeHeaderProvider extends BaseItemProvider<HomeBean.DataListBean, BaseViewHolder> {
    @Override
    public int viewType() {
        return HomeNewAdapter.TYPE_FREE_HEADER_TITLE;
    }

    @Override
    public int layout() {
        return R.layout.home_free_header_layout;
    }

    @Override
    public void convert(BaseViewHolder helper, HomeBean.DataListBean data, int position) {
        helper.setText(R.id.tvTitle, data.title);
        helper.setGone(R.id.tvMore, data.isMore.equals("true"));

    }

    @Override
    public void onClick(BaseViewHolder helper, HomeBean.DataListBean data, int position) {
        super.onClick(helper, data, position);

    }
}
