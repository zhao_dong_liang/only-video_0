package cn.wu1588.dancer.home.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cnsunrun.commonui.widget.button.RoundButton;
import com.cnsunrun.commonui.widget.roundwidget.QMUIRoundButton;
import com.makeramen.roundedimageview.RoundedImageView;
import com.my.toolslib.NumUtils;
import com.my.toolslib.http.utils.LzyResponse;
import com.my.toolslib.http.utils.OkHttpRequestListener;
import com.shuyu.gsyvideoplayer.GSYVideoManager;
import com.shuyu.gsyvideoplayer.builder.GSYVideoOptionBuilder;
import com.shuyu.gsyvideoplayer.listener.GSYSampleCallBack;
import com.shuyu.gsyvideoplayer.utils.OrientationUtils;
import com.shuyu.gsyvideoplayer.video.base.GSYVideoPlayer;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;
import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.sunrun.sunrunframwork.utils.EmptyDeal;
import com.tencent.mmkv.MMKV;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.adp.GuessLikeAdapter;
import cn.wu1588.dancer.home.fragment.AdPlayerFragment;
import cn.wu1588.dancer.home.fragment.CommentFragment;
import cn.wu1588.dancer.home.fragment.CommentsDetailDialogFragment;
import cn.wu1588.dancer.home.mode.MovieActionStateBean;
import cn.wu1588.dancer.home.mode.MovieBean;
import cn.wu1588.dancer.home.mode.MovieDetailBean;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.event.MessageEvent;
import cn.wu1588.dancer.common.model.LoginInfo;
import cn.wu1588.dancer.common.model.SystemParams;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.common.util.DataUtils;
import cn.wu1588.dancer.common.util.LoginUtil;
import cn.wu1588.dancer.common.util.RecycleHelper;
import cn.wu1588.dancer.common.util.SPUtils;
import cn.wu1588.dancer.common.util.ViewUtils;
import cn.wu1588.dancer.adp.MovieCommentsAdapter;
import cn.wu1588.dancer.share.ShareBottomSheetDialog;
import cn.wu1588.dancer.videoplayer.SwitchUtil;
import cn.wu1588.dancer.videoplayer.SwitchVideo;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.COMMENTS_DIANZAN_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.DEDUCT_GOLD_ACCOUNT_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_MOVIE_ACTION_STATES_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_MOVIE_PLAY_DETAIL_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_MOVIE_URL_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.MOVIE_DIANZAN_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.MOVIE_SHOUCANG_CODE;

/**
 * 影片播放页面
 */
public class MoviePlayerActivity extends LBaseActivity {

    private static final String ATTENTION_TAG = "已订阅";

    @BindView(R.id.recyclerViewMovie)
    RecyclerView recyclerViewMovie;
    @BindView(R.id.recyclerViewComments)
    RecyclerView recyclerViewComments;
    @BindView(R.id.moviePlayer)
    SwitchVideo moviePlayer;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivDianZan)
    TextView ivDianZan;
    @BindView(R.id.ivBuZan)
    TextView ivBuZan;
    @BindView(R.id.ivShouCang)
    TextView ivShouCang;
    @BindView(R.id.ivShare)
    TextView ivShare;
    @BindView(R.id.llScrollHeightLayout)
    LinearLayout llScrollHeightLayout;
    @BindView(R.id.nestedScorllView)
    NestedScrollView nestedScorllView;
    @BindView(R.id.ivFanhui)
    ImageView ivFanhui;
    @BindView(R.id.tvOver)
    TextView tvOver;
    @BindView(R.id.btnRest)
    QMUIRoundButton btnRest;
    @BindView(R.id.btnBuyMember)
    QMUIRoundButton btnBuyMember;
    @BindView(R.id.rlTestOver)
    RelativeLayout rlTestOver;
    @BindView(R.id.tvAllCommentsNum)
    TextView tvAllCommentsNum;
    @BindView(R.id.layVideo)
    FrameLayout layVideo;
    @BindView(R.id.btnRecharge)
    RelativeLayout btnRecharge;
    @BindView(R.id.download_film)
    RelativeLayout download_film;
    @BindView(R.id.layComment)
    LinearLayout layComment;
    @BindView(R.id.iv_video_author_avatar)
    RoundedImageView ivVideoAuthorAvatar;
    @BindView(R.id.tv_video_author_name)
    TextView tvVideoAuthorName;
    @BindView(R.id.tv_video_author_desc)
    TextView tvVideoAuthorDesc;
    @BindView(R.id.tv_video_change)
    TextView tvVideoChange;
    @BindView(R.id.fl_comment_layout)
    FrameLayout flCommentLayout;
    @BindView(R.id.ibVideoCommentActionCollect)
    ImageButton ibVideoCommentActionCollect;
    @BindView(R.id.ibVideoCommentActionLike)
    ImageButton ibVideoCommentActionLike;
    @BindView(R.id.ibVideoCommentActionSend)
    ImageButton ibVideoCommentActionSend;
    @BindView(R.id.tv_movie_download)
    TextView tv_movie_download;
    @BindView(R.id.btn_movie_subscription)
    RoundButton btn_movie_subscription;
    @BindView(R.id.fl_cover)
    FrameLayout fl_cover;

    private GuessLikeAdapter guessLikeAdapter;
    private boolean isPlay;
    private boolean isPause;

    private OrientationUtils orientationUtils;
    private String typeid = "2";
    private MovieCommentsAdapter commentsAdapter;
    private List<MovieDetailBean.YoulikeBean> youlike;
    private MovieDetailBean movieDetailBean;
    private String movieId;
    private MovieActionStateBean movieActionStateBean;
    private int is_top;
    private static final String OPTION_VIEW = "view";
    private GSYVideoOptionBuilder gsyVideoOption;
    private ClipboardManager myClipboard;
    private String adurl;
    private MovieBean movieBean;
    private int freeTime = 60;
    private int insertAdTime;
    private RefreshDataReceiver receiver;
    //是否扣过金币
    private boolean isDeductGold;
    //会员是否过期
    private boolean memberDateMature;

    private AdPlayerFragment adPlayerFragment;
    private String showDownload;

    public static void startTActivity(Activity activity, String movieId, View transitionView) {
        if (LoginUtil.startLogin(activity)) {
            CommonIntent.startLoginActivity(activity);
        } else {
            Intent intent = new Intent(activity, MoviePlayerActivity.class);
            intent.putExtra("movieId", movieId);
            // 这里指定了共享的视图元素
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, transitionView, OPTION_VIEW);
            ActivityCompat.startActivity(activity, intent, options.toBundle());
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LoginInfo loginInfo = Config.getLoginInfo();
        memberDateMature = DataUtils.compare_date(loginInfo.member_end_time);
        setContentView(R.layout.activity_movie_player);
        initEventBus();
        ButterKnife.bind(this);
        movieId = getIntent().getStringExtra("movieId");
        initViews();
        MMKV mmkv = MMKV.defaultMMKV();
        showDownload = mmkv.decodeString(SystemParams.IS_SHOW_DOWBTN);
        freeTime = NumUtils.parseInt(mmkv.decodeString(SystemParams.FREE_PLAY_TIME), 20);
        if (showDownload.equals("1")) {
            download_film.setVisibility(View.VISIBLE);
            tv_movie_download.setVisibility(View.VISIBLE);
        } else {
            download_film.setVisibility(View.GONE);
            tv_movie_download.setVisibility(View.GONE);
        }
        initData();
//
        initPlayer();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.to.mine");
        receiver = new RefreshDataReceiver();
        registerReceiver(receiver, intentFilter);

        String interval = mmkv.decodeString(SystemParams.AD_TIME_INTERVAL);
        try {
            insertAdTime = Integer.parseInt(interval);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        int code = NumUtils.parseInt(mmkv.decodeString(SystemParams.INSERT_AD_IS_OPEN), 0);
        if (code == 1) {
            adPlayerFragment = AdPlayerFragment.newInstance();
            adPlayerFragment.setOnHostPlayerLoadVideo(() -> BaseQuestStart.getMovieUrl(this, movieId));
            adPlayerFragment.setOnHostPlayerResume(() -> moviePlayer.onVideoResume());
            adPlayerFragment.setOnHostPlayerPause(() -> moviePlayer.onVideoPause());

            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fl_ad_player, adPlayerFragment, "AdPlayerFragment")
                    .commit();
        } else {
            BaseQuestStart.getMovieUrl(this, movieId);
        }
    }

    @OnClick(R.id.tv_video_change)
    void changeMayAlsoLike(View view) {
        initData();
    }

    @OnClick(R.id.commect_tv)
    void sendComment(View view) {

        Fragment f = getSupportFragmentManager().findFragmentByTag(CommentFragment.class.getSimpleName());
        if (f instanceof CommentFragment) {
            ((CommentFragment) f).sendComment();
        }
    }

    @OnClick(R.id.ll_user_layout)
    void toAuthor(View view) {
        CommonIntent.startOtherUserActiivty(this, movieDetailBean.user_id);
    }

    @OnClick(R.id.btn_movie_subscription)
    void followUser(View view) {
        if (TextUtils.equals(btn_movie_subscription.getText(), ATTENTION_TAG)) {
            androidx.appcompat.app.AlertDialog dialog = new androidx.appcompat.app.AlertDialog.Builder(this)
                    .setMessage("确定要取消该用户订阅吗？")
                    .setPositiveButton("确定", (d, which) -> cancelAttention())
                    .setNegativeButton("取消", (d, which) -> Log.d("MoviePlayerActivity", "取消订阅窗口"))
                    .create();
            dialog.show();
            Button btnPos = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
            Button btnNeg = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
            btnPos.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
            btnNeg.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
        } else {
            attention();
        }
    }

    private void attention() {
        BaseQuestStart.followUser(new OkHttpRequestListener() {
            @Override
            public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
                if (response.code == 1) {
                    btn_movie_subscription.setText(ATTENTION_TAG);
                }
            }
        }, movieDetailBean.user_id);
    }

    private void cancelAttention() {
        BaseQuestStart.cancelAttention(new OkHttpRequestListener() {
            @Override
            public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
                if (response.code == 1) {
                    btn_movie_subscription.setText("+订阅");
                }
            }
        }, movieDetailBean.user_id);
    }

    /**
     * 请求数据
     */
    private void initData() {

        BaseQuestStart.getMoviePlayDetail(that, movieId, typeid);
        BaseQuestStart.getMoveieActionStates(that, movieId);//查看影片是否点赞或者收藏

    }

    /**
     * 初始化播放器
     */
    private void initPlayer() {
        //外部辅助的旋转，帮助全屏
        orientationUtils = new OrientationUtils(this, moviePlayer);
        //初始化不打开外部的旋转
        orientationUtils.setEnable(false);


        moviePlayer.getFullscreenButton().setOnClickListener(v -> {
            //直接横屏
            orientationUtils.resolveByClick();
            //第一个true是否需要隐藏actionbar，第二个true是否需要隐藏statusbar
            moviePlayer.startWindowFullscreen(MoviePlayerActivity.this, true, true);
        });
        moviePlayer.setIsClikListner(false);

    }

    private void initViews() {
        myClipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        recyclerViewMovie.setNestedScrollingEnabled(false);
        recyclerViewComments.setNestedScrollingEnabled(false);
        RecycleHelper.setLinearLayoutManager(recyclerViewMovie, LinearLayoutManager.VERTICAL);
        RecycleHelper.setLinearLayoutManager(recyclerViewComments, LinearLayoutManager.VERTICAL);


        guessLikeAdapter = new GuessLikeAdapter();
        recyclerViewMovie.setAdapter(guessLikeAdapter);
        commentsAdapter = new MovieCommentsAdapter();
        recyclerViewComments.setAdapter(commentsAdapter);
        recyclerViewComments.setLayoutManager(new LinearLayoutManager(that));
        guessLikeAdapter.setOnItemChildClickListener((adapter, view, position) -> {
            isDeductGold = false;
            layVideo.setVisibility(View.GONE);
            MovieDetailBean.YoulikeBean item = guessLikeAdapter.getItem(position);
            BaseQuestStart.getMovieUrl(that, item.id);
            BaseQuestStart.getMoviePlayDetail(that, item.id, typeid);
            BaseQuestStart.getMoveieActionStates(that, item.id);//查看影片是否点赞或者收藏
            movieId = item.id;
        });
        commentsAdapter.setOnItemChildClickListener((adapter, view, position) -> {
            MovieDetailBean.CommentBean item = commentsAdapter.getItem(position);
            switch (view.getId()) {
                case R.id.llContainer:
                    CommentsDetailDialogFragment.newInstance()
                            .setPlayerHeight(moviePlayer.getHeight())
                            .setMovieId(movieId)
                            .setMemID(item.mem_id)
                            .show(getSupportFragmentManager(), "CommentsDetailDialogFragment");
                    break;
                case R.id.tvZanNum:
                    if (item.status == 0) {
                        BaseQuestStart.CommentsDianzan(that, item.mem_id);
                    }
                    break;
            }
        });
    }

    @OnClick({R.id.download_film, R.id.ivDianZan, R.id.ibVideoCommentActionCollect, R.id.ibVideoCommentActionLike, R.id.ibVideoCommentActionSend, R.id.ivBuZan, R.id.ivShouCang, R.id.ivShare, R.id.ivFanhui, R.id.btnRest, R.id.btnBuyMember, R.id.tv_movie_download, R.id.btnRecharge, R.id.buy_gold})
    public void onViewClicked(View view) {
        int height = moviePlayer.getHeight();
        switch (view.getId()) {
            case R.id.ibVideoCommentActionLike:
            case R.id.ivDianZan://点赞
                is_top = 1;
                BaseQuestStart.movieZan(that, is_top, movieId);
                break;
            case R.id.ivBuZan://不赞
                is_top = 0;
                BaseQuestStart.movieZan(that, is_top, movieId);
                break;
            case R.id.ibVideoCommentActionCollect:
            case R.id.ivShouCang://收藏
                BaseQuestStart.movieShoucang(that, movieId);
                break;
            case R.id.ibVideoCommentActionSend:
            case R.id.ivShare://分享
                MovieBean movieBean = new MovieBean();
                movieBean.id = Integer.parseInt(movieId);
                movieBean.title = movieDetailBean.title;
                movieBean.play_url = movieDetailBean.play_url;
                movieBean.image = movieDetailBean.image;
                ShareBottomSheetDialog shareBottomSheetDialog = ShareBottomSheetDialog.newInstance(movieBean);
                shareBottomSheetDialog.show(getSupportFragmentManager(), ShareBottomSheetDialog.TAG);
                break;
            case R.id.ivFanhui://试看结束返回
                finish();
                break;
            case R.id.btnRest://重新播放
                layVideo.setVisibility(View.GONE);
                moviePlayer.startPlayLogic();
                break;
            case R.id.btnBuyMember://购买会员
                CommonIntent.startRechargeActivity(that, 0);
                break;
            case R.id.buy_gold://购买金币
                CommonIntent.startRechargeActivity(that, 1);
                break;
            case R.id.btnRecharge:
                CommonIntent.startRechargeActivity(that, 0);
                break;
            case R.id.tv_movie_download:
            case R.id.download_film:
                AlertDialog dialog = new AlertDialog.Builder(this)
                        .setMessage("高清视频下载请在PC端前往http://www.ml1588.cn进行下载")
                        .setTitle("提示")
                        .setNegativeButton("确定", (dialogInterface, i) -> dialogInterface.dismiss()).create();
                dialog.show();
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.BLACK);

                break;
        }
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode) {
            case GET_MOVIE_URL_CODE://获取视频播放地址
                if (bean.status == 1) {
                    movieBean = bean.Data();
                    ImageView imageView = new ImageView(this);
                    imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                    ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, UIUtils.dip2px(this, 210));
                    imageView.setLayoutParams(layoutParams);
                    GlideMediaLoader.load(this, imageView, movieBean.image);
                    moviePlayer.setThumbImageView(imageView);
                    moviePlayer.getThumbImageViewLayout().setVisibility(View.VISIBLE);
                    if (movieBean.isFree()) {
                        fl_cover.setVisibility(View.GONE);
                        setPlayVideo(movieBean);
                    } else {
                        fl_cover.setVisibility(View.VISIBLE);
                        fl_cover.setOnClickListener(v -> UIUtils.shortM("不是免费视频"));
                    }
                }
                break;
            case GET_MOVIE_PLAY_DETAIL_CODE:
                if (bean.status == 1) {
                    movieDetailBean = bean.Data();
                    if (movieDetailBean == null) {
                        return;
                    }
                    updateUI(movieDetailBean);
                    Log.i("onlyVideo", movieDetailBean.gold + ":所需gold");
                }
                break;
            case COMMENTS_DIANZAN_CODE://评论点赞
                if (bean.status == 1) {
                    BaseQuestStart.getMoviePlayDetail(that, movieId, typeid);
                    BaseQuestStart.getMoveieActionStates(that, movieId);//查看影片是否点赞或者收藏
                } else {
                    UIUtils.shortM(bean.msg);
                }
                break;
            case GET_MOVIE_ACTION_STATES_CODE://查看影片是否点赞或者收藏
                if (bean.status == 1) {
                    movieActionStateBean = bean.Data();
                    if (!movieActionStateBean.isAlowZan()) {
                        ivDianZan.setEnabled(false);
                        ivBuZan.setEnabled(false);
                    }
                    if (movieActionStateBean.top == 1) {
                        ViewUtils.setTextViewTopDrawableColor(ivDianZan, R.color.color_A88860);
                        ibVideoCommentActionLike.setImageResource(R.drawable.icon_liked_oro);
                    } else if (movieActionStateBean.top == 0) {
                        ViewUtils.setTextViewTopDrawableColor(ivBuZan, R.color.color_A88860);
                        ibVideoCommentActionLike.setImageResource(R.drawable.icon_comment_like);
                    }
                    if (movieActionStateBean.isLike()) {
                        ibVideoCommentActionCollect.setImageResource(R.drawable.icon_comment_collect);
                    } else {
                        ibVideoCommentActionCollect.setImageResource(R.drawable.icon_oro_collection);
                    }
                }
                break;
            case MOVIE_DIANZAN_CODE://视频点赞
                BaseQuestStart.getMoveieActionStates(that, movieId);//查看影片是否点赞或者收藏
                if (bean.status == 1) {
                    UIUtils.shortM(bean.msg);
                    if (is_top == 1) {

                    } else {

                    }
                } else {
                    UIUtils.shortM(bean.msg);
                }
                break;
            case MOVIE_SHOUCANG_CODE:
                BaseQuestStart.getMoveieActionStates(that, movieId);//查看影片是否点赞或者收藏
                if (bean.status == 1) {

                } else {
                    UIUtils.shortM(bean.msg);
                }
                break;
            case DEDUCT_GOLD_ACCOUNT_CODE:
                if (bean.status == 1) {
                    isDeductGold = true;
                    moviePlayer.getCurrentPlayer().onVideoResume(false);
                    GSYVideoManager.onResume(true);
                    ToastUtils.longToast(movieDetailBean.gold + "金币扣除成功！");
                    Log.i("onlyVideo", "保存的当前时间：" + System.currentTimeMillis());
                    SPUtils.put(this, Config.getLoginInfo().uid + movieId, System.currentTimeMillis());
                } else {
                    moviePlayer.onVideoPause();
                    isDeductGold = false;
                    GSYVideoManager.releaseAllVideos();
                    orientationUtils.backToProtVideo();
                    layVideo.setVisibility(View.VISIBLE);
                    moviePlayer.onVideoReset();
                }
                break;
        }
        super.nofityUpdate(requestCode, bean);
    }

    /**
     * 设置播放的Url
     *
     * @param movieBean
     */
    private void setPlayVideo(MovieBean movieBean) {


        gsyVideoOption = new GSYVideoOptionBuilder();
        gsyVideoOption.setIsTouchWiget(true)
                .setRotateViewAuto(false)
                .setLockLand(false)
                .setAutoFullWithSize(true)
                .setShowFullAnimation(false)
                .setNeedLockFull(true)
                .setNeedShowWifiTip(true)
                .setCacheWithPlay(false)
                .setVideoAllCallBack(new GSYSampleCallBack() {
                    @Override
                    public void onEnterFullscreen(String url, Object... objects) {
                        super.onEnterFullscreen(url, objects);
                        GSYVideoPlayer gsyVideoPlayer = (GSYVideoPlayer) objects[1];
                        gsyVideoPlayer.setGSYVideoProgressListener((progress, secProgress, currentPosition, duration) -> {
                            if (adPlayerFragment != null) {
                                adPlayerFragment.onHostPlayerProgress(currentPosition);
                            }

                            if (!movieBean.isFree()) {
                                if (!Config.getLoginInfo().isMember()) {
                                    //判断是否限时播放视频
                                    stopOrstartVideo(currentPosition);
                                } else {
                                    if (!memberDateMature) {
                                        //判断是否限时播放视频
                                        stopOrstartVideo(currentPosition);
                                    }
                                }
                            }
                        });
                    }

                    @Override
                    public void onPrepared(String url, Object... objects) {
                        super.onPrepared(url, objects);
                        //开始播放了才能旋转和全屏
                        orientationUtils.setEnable(true);
                        isPlay = true;
                    }

                    @Override
                    public void onQuitFullscreen(String url, Object... objects) {
                        super.onQuitFullscreen(url, objects);
                        if (orientationUtils != null) {
                            orientationUtils.backToProtVideo();
                        }
                    }

                    @Override
                    public void onClickResume(String url, Object... objects) {
                        super.onClickResume(url, objects);

                    }

                    @Override
                    public void onClickStop(String url, Object... objects) {
                        super.onClickStop(url, objects);
                        if (adPlayerFragment != null) {
                            adPlayerFragment.onHostPlayerPause();
                        }

                    }

                    @Override
                    public void onAutoComplete(String url, Object... objects) {
                        super.onAutoComplete(url, objects);
                        if (adPlayerFragment != null) {
                            adPlayerFragment.onHostPlayerComplete();
                        }

                    }
                }).setLockClickListener((view, lock) -> {
            if (orientationUtils != null) {
                //配合下方的onConfigurationChanged
                orientationUtils.setEnable(!lock);
            }
        }).setGSYVideoProgressListener((progress, secProgress, currentPosition, duration) -> {
            Log.d("VideoPlayProgress", String.format("进度：%s", progress));
            if (adPlayerFragment != null) {
                adPlayerFragment.onHostPlayerProgress(currentPosition);
            }

            if (!movieBean.isFree()) {
                if (!Config.getLoginInfo().isMember()) {
                    stopOrstartVideo(currentPosition);
                } else {
                    //如果会员过期
                    if (!memberDateMature) {
                        stopOrstartVideo(currentPosition);
                    }

                }
            }
        }).build(moviePlayer);
        moviePlayer.setUp(movieBean.play_url, false, movieBean.title);
        gsyVideoOption.setVideoTitle(movieBean.title);
        moviePlayer.startPlayLogic();
    }

    private void updateUI(MovieDetailBean movieDetailBean) {
        //TODO 报空 NullPointerException
        if (movieDetailBean == null) {
            return;
        }

        if (!EmptyDeal.isEmpy(movieDetailBean.adcenter)) {
            adurl = movieDetailBean.adcenter.get(0).url;
        }
        tvTitle.setText(movieDetailBean.title);

        GlideMediaLoader.loadHead(that, ivVideoAuthorAvatar, movieDetailBean.icon);
        tvVideoAuthorName.setText(movieDetailBean.username);
        tvVideoAuthorDesc.setText(String.format("%s %s次播放", movieDetailBean.add_time, movieDetailBean.play_num));
        ivDianZan.setText(TextUtils.isEmpty(movieDetailBean.top_num) ? "0" : movieDetailBean.top_num);
        ivBuZan.setText(TextUtils.isEmpty(movieDetailBean.down_num) ? "0" : movieDetailBean.down_num);
        //ivShare.setText(TextUtils.isEmpty(movieDetailBean.shared_num) ? "0" : movieDetailBean.shared_num);
        ivShare.setText("分享");
        //ivShouCang.setText(TextUtils.isEmpty(movieDetailBean.collection_num) ? "0" : movieDetailBean.collection_num);
        ivShouCang.setText("收藏");
        //tv_movie_download.setText(TextUtils.isEmpty(movieDetailBean.download_num) ? "0" : movieDetailBean.download_num);
        tv_movie_download.setText("下载");

        tvAllCommentsNum.setText(String.format(getString(R.string.comments_num), movieDetailBean.comment_num));


        //猜你喜欢
        youlike = movieDetailBean.youlike;
        List<MovieDetailBean.YoulikeBean> youlikeBeans = new ArrayList<>();
        if (!EmptyDeal.isEmpy(youlike)) {
            if (youlike.size() > 3) {
                for (int i = 0; i < 3; i++) {
                    youlikeBeans.add(youlike.get(i));
                }
                guessLikeAdapter.setNewData(youlikeBeans);
            }
        }

        //评论
        commentsAdapter.setNewData(movieDetailBean.comment);

        MovieBean movieBean = new MovieBean();
        movieBean.id = Integer.parseInt(movieId);
        movieBean.comment_num = movieDetailBean.comment_num;
        //新版评论
        CommentFragment commentFragment = (CommentFragment) getSupportFragmentManager().findFragmentByTag(CommentFragment.class.getSimpleName());
        FragmentTransaction tcx = getSupportFragmentManager().beginTransaction();
        if (commentFragment == null) {
            commentFragment = new CommentFragment();
            commentFragment.setMovieBeanHideCommentInput(movieBean);
            tcx.add(R.id.fl_comment_layout, commentFragment, CommentFragment.class.getSimpleName());
        } else {
            commentFragment = new CommentFragment();
            commentFragment.setMovieBeanHideCommentInput(movieBean);
            tcx.replace(R.id.fl_comment_layout, commentFragment, CommentFragment.class.getSimpleName());
        }
        tcx.commitNowAllowingStateLoss();

        BaseQuestStart.checkUserAttention(new OkHttpRequestListener() {
            @Override
            public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
                if (response.code == 1) {
                    // 已关注
                    btn_movie_subscription.setText(ATTENTION_TAG);
                } else {
                    btn_movie_subscription.setText("+订阅");
                }
            }
        }, movieDetailBean.user_id);
    }

    @Override
    public void onBackPressed() {
        if (orientationUtils != null) {
            orientationUtils.backToProtVideo();
        }
        if (GSYVideoManager.backFromWindowFull(this)) {
            return;
        }
        super.onBackPressed();
    }


    @Override
    protected void onPause() {
        moviePlayer.getCurrentPlayer().onVideoPause();
        super.onPause();
        isPause = true;
    }

    @Override
    protected void onResume() {
        moviePlayer.getCurrentPlayer().onVideoResume(false);
        super.onResume();
        isPause = false;
    }

    @Override
    protected void onDestroy() {

//        if (isPlay && moviePlayer != null) {
//            moviePlayer.getCurrentPlayer().release();
//        }
        moviePlayer.getGSYVideoManager().setListener(moviePlayer.getGSYVideoManager().lastListener());
        moviePlayer.getGSYVideoManager().setLastListener(null);
        GSYVideoManager.releaseAllVideos();
        if (orientationUtils != null) {
            orientationUtils.releaseListener();
        }
        SwitchUtil.release();
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        //如果旋转了就全屏
        if (isPlay && !isPause) {
            moviePlayer.onConfigurationChanged(this, newConfig, orientationUtils, true, true);
        }
    }

    @Subscribe
    public void eventBusMethod(MessageEvent event) {
        String type = event.getType();
        if (TextUtils.equals("refresh_data", type)) {
            BaseQuestStart.getMoviePlayDetail(that, movieId, typeid);
            BaseQuestStart.getMoveieActionStates(that, movieId);//查看影片是否点赞或者收藏
        }
    }


    public class RefreshDataReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("android.intent.action.to.mine")) {
                finish();
            }
        }
    }

    /**
     * 判断会员，是否过期，扣除金币的限时操作
     *
     * @param currentPosition
     */
    public void stopOrstartVideo(int currentPosition) {
        //如果不是会员，去扣除金币在播放
        if (movieDetailBean.gold > 0) {
            if (currentPosition > freeTime * 1000) {
                if (!isDeductGold) {
                    //计算购买金币是否超过24小时
                    long currentTimeMillis = System.currentTimeMillis();
                    long saveTimeMillis = (long) SPUtils.get(that, Config.getLoginInfo().uid + movieId, 1568111276000l);
                    Log.i("onlyVideo", "当前的时间：" + currentTimeMillis + "--" + saveTimeMillis);
                    int hour = DataUtils.getOffectHour(currentTimeMillis, saveTimeMillis);
                    Log.i("onlyVideo", "当前相差的时间：" + hour);
                    if (hour > 24) {
                        Log.i("onlyVideo", "去支付金币");
                        BaseQuestStart.deductGoldAccount(that, movieDetailBean.gold + "");
                    } else {
                        isDeductGold = true;
                    }
                }
            }
        }
    }
}
