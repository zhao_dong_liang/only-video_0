package cn.wu1588.dancer.home.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.cnsunrun.commonui.widget.button.RoundButton;
import com.my.toolslib.http.utils.NetRequestListenerProxy;
import com.shuyu.gsyvideoplayer.listener.GSYSampleCallBack;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.tencent.mmkv.MMKV;

import cn.wu1588.dancer.model.Ad;
import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.model.SystemParams;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.DownloadUtils;
import moe.codeest.enviews.ENPlayView;

public class AdPlayerFragment extends LBaseFragment {

    public interface OnHostPlayerResume {
        void onResume();
    }

    public interface OnHostPlayerLoadVideo {
        void onLoad();
    }

    public interface OnHostPlayerPause {
        void onPause();
    }

    @BindView(R.id.ad_video_player)
    MyADVideoPlayer adPlayer;
    @BindView(R.id.iv_ad_image)
    ImageView ivAdImage;
    @BindView(R.id.btn_ad_subtitle)
    RoundButton btnAdSubtitle;
    @BindView(R.id.btn_ad_download)
    RoundButton btnAdDownload;
    @BindView(R.id.btn_ad_desc)
    RoundButton btnAdDesc;
    @BindView(R.id.enp_start)
    ENPlayView start;

    private OnHostPlayerLoadVideo onHostPlayerLoadVideo;
    private OnHostPlayerResume onHostPlayerResume;
    private OnHostPlayerPause onHostPlayerPause;

    private int adLoadType;
    private static final int LOAD_AD_TYPE_START = 0;
    private static final int LOAD_AD_TYPE_PLAYING = 1;
    private static final int LOAD_AD_TYPE_PLAY_PAUSE = 2;
    private static final int LOAD_AD_TYPE_PLAY_COMPLETE = 3;

    private int insertAdTime;

    private Ad mAd;

    public void setOnHostPlayerResume(OnHostPlayerResume onHostPlayerResume) {
        this.onHostPlayerResume = onHostPlayerResume;
    }

    public void setOnHostPlayerLoadVideo(OnHostPlayerLoadVideo onHostPlayerLoadVideo) {
        this.onHostPlayerLoadVideo = onHostPlayerLoadVideo;
    }

    public void setOnHostPlayerPause(OnHostPlayerPause onHostPlayerPause) {
        this.onHostPlayerPause = onHostPlayerPause;
    }

    public static AdPlayerFragment newInstance() {
        return new AdPlayerFragment();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_ad_player;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.setVisibility(View.GONE);
        start.setVisibility(View.GONE);
        initAdIntervalTime();
        adPlayer.setVideoAllCallBack(new GSYSampleCallBack() {
            @Override
            public void onPrepared(String url, Object... objects) {
                super.onPrepared(url, objects);
                ivAdImage.setVisibility(View.GONE);
            }

            @Override
            public void onAutoComplete(String url, Object... objects) {
                super.onAutoComplete(url, objects);
                //广告结束，释放
                adPlayer.getCurrentPlayer().release();
                adPlayer.onVideoReset();
                view.setVisibility(View.GONE);
                if (adLoadType == LOAD_AD_TYPE_START && onHostPlayerLoadVideo != null) {
                    onHostPlayerLoadVideo.onLoad();
                } else if (adLoadType == LOAD_AD_TYPE_PLAYING && onHostPlayerResume != null) {
                    onHostPlayerResume.onResume();
                    insertAdTime += insertAdTime;
                }
            }

            @Override
            public void onPlayError(String url, Object... objects) {
                super.onPlayError(url, objects);
                //广告结束，释放
                adPlayer.getCurrentPlayer().release();
                adPlayer.onVideoReset();
                view.setVisibility(View.GONE);
                if (adLoadType == LOAD_AD_TYPE_START && onHostPlayerLoadVideo != null) {
                    onHostPlayerLoadVideo.onLoad();
                } else if (adLoadType == LOAD_AD_TYPE_PLAYING && onHostPlayerResume != null) {
                    onHostPlayerResume.onResume();
                    insertAdTime += insertAdTime;
                }
            }
        });

        adLoadType = LOAD_AD_TYPE_START;
        loadAds();
    }

    @OnClick(R.id.start)
    void start(View view) {
        getView().setVisibility(View.GONE);
        start.setVisibility(View.GONE);
        if (onHostPlayerResume != null) {
            onHostPlayerResume.onResume();
        }
    }

    @OnClick(R.id.btn_ad_download)
    void download(View view) {
        if (mAd == null) {
            return;
        }
        if (!TextUtils.isEmpty(mAd.android_package_url)) {
            DownloadUtils downloadUtils = new DownloadUtils(getActivity());
            downloadUtils.download(mAd.android_package_url, mAd.package_name);
        }
    }

    @OnClick(R.id.btn_ad_desc)
    void learMore(View view) {
        if (mAd == null) {
            return;
        }
        if (!TextUtils.isEmpty(mAd.outer_url)) {
            CommonIntent.startLoadUrlWebActivity(getActivity(), mAd.outer_url, mAd);
        } else if (!TextUtils.isEmpty(mAd.html)) {
            CommonIntent.startLoadUrlWebActivity(getActivity(), mAd.html, mAd);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (adPlayer != null) {
            adPlayer.onVideoPause();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adPlayer != null) {
            adPlayer.onVideoResume();
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        if (adPlayer != null) {
            adPlayer.setVideoAllCallBack(null);
            adPlayer.getCurrentPlayer().release();
            adPlayer.onVideoReset();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    public void onHostPlayerProgress(int currentProgress) {
        Log.i("HostPlayerProgress", currentProgress + "");
        if (currentProgress >= insertAdTime) {
            if (onHostPlayerPause != null) {
                onHostPlayerPause.onPause();
            }
            adLoadType = LOAD_AD_TYPE_PLAYING;
            loadAds();
        }
    }

    public void onHostPlayerPause() {
        getView().setVisibility(View.VISIBLE);
        start.setVisibility(View.VISIBLE);
        adLoadType = LOAD_AD_TYPE_PLAY_PAUSE;
        loadAds();
    }

    public void onHostPlayerComplete() {
        getView().setVisibility(View.VISIBLE);
        adLoadType = LOAD_AD_TYPE_PLAY_COMPLETE;
        loadAds();
    }

    private void loadAds() {
        BaseQuestStart.getPlayAds(new NetRequestListenerProxy(getActivity()) {
            @Override
            public void nofityUpdate(int i, BaseBean baseBean) {
                if (getView() == null) {
                    return;
                }
                if (baseBean.status == 1) {
                    Ad ad = baseBean.Data();
                    mAd = ad;
                    if (adLoadType == LOAD_AD_TYPE_PLAY_PAUSE) {
                        setAdActions(ad);
                        return;
                    }
                    if (ad != null) {
                        getView().setVisibility(View.VISIBLE);
                        setAdActions(ad);
                        if (!TextUtils.isEmpty(ad.video_url)) {
                            adPlayer.setUp(ad.video_url, false, ad.title);
                            adPlayer.startPlayLogic();
                        } else {
                            notifyAdLoadError();
                        }
                    } else {
                        onHostPlayerLoadVideo.onLoad();
                    }
                } else {
                    //广告结束，释放
                    notifyAdLoadError();
                }
            }
        });
    }

    private void notifyAdLoadError() {
        //广告结束，释放
        adPlayer.getCurrentPlayer().release();
        adPlayer.onVideoReset();
        getView().setVisibility(View.GONE);
        if (adLoadType == LOAD_AD_TYPE_START && onHostPlayerLoadVideo != null) {
            onHostPlayerLoadVideo.onLoad();
        } else if (adLoadType == LOAD_AD_TYPE_PLAYING && onHostPlayerResume != null) {
            onHostPlayerResume.onResume();
            insertAdTime += insertAdTime;
        } else if (adLoadType == LOAD_AD_TYPE_PLAY_PAUSE && onHostPlayerResume != null) {
            onHostPlayerResume.onResume();
        }
    }

    private void initAdIntervalTime() {
        MMKV mmkv = MMKV.defaultMMKV();
        String interval = mmkv.decodeString(SystemParams.AD_TIME_INTERVAL);
        try {
            insertAdTime = Integer.parseInt(interval) * 1000;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setAdActions(Ad ad) {
        GlideMediaLoader.load(getActivity(), ivAdImage, ad.img_url, R.drawable.ic_place_arcimg);
        ivAdImage.setVisibility(View.VISIBLE);
        btnAdSubtitle.setText(ad.short_title);
        boolean showDesc = !TextUtils.isEmpty(ad.outer_url) || !TextUtils.isEmpty(ad.html);
        btnAdDesc.setVisibility(showDesc ? View.VISIBLE : View.GONE);
        btnAdDownload.setVisibility(TextUtils.isEmpty(ad.android_package_url) ? View.GONE : View.VISIBLE);
    }
}
