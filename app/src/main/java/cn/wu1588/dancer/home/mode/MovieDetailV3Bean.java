package cn.wu1588.dancer.home.mode;

/**
 * Created by zhou on 2021/1/25 17:26.
 */
public class MovieDetailV3Bean {

        /**
         * id : 3655
         * user_id : 190
         * title : 《Xiao Qing》广场舞《大叔DJ》4K画面大家觉得好看吗
         * image : http://video.myylook.com/a7bc238eca224afc9b0ab4a44d9205bf/snapshots/3b5c35fffa9c46ad994559607b0ae75b-00002.jpg
         * play_url : http://video.myylook.com/sv/361b5859-175ffb3677b/361b5859-175ffb3677b.mp4
         * play_num : 3
         * comment_num : 0
         * share_num : 0
         * label : 0
         * like_num : 0
         * download_num : 0
         * home_category_id : 15
         * recommend_category_id : 0
         * hot_category_id : 0
         * top_num : 0
         * down_num : 0
         * collect_num : 0
         * grade : 0.0
         * sort : 0
         * add_time : 2020-11-25 22:14:13
         * update_time : 2021-01-25 17:31:37
         * recommend : 0
         * play_image : http://video.myylook.com/a7bc238eca224afc9b0ab4a44d9205bf/snapshots/3b5c35fffa9c46ad994559607b0ae75b-00002.jpg
         * play_time : 162
         * content :
         * is_free : 1
         * gold : 0
         * download_url :
         * user_movie_type : 0
         * videoid : a7bc238eca224afc9b0ab4a44d9205bf
         * type : 1
         * down_type : 百度网盘
         * img_size : null
         * img_fb : null
         * if4k : 0
         * retrieve_password :
         * extract_password :
         * price : 0.00
         * screenshots : null
         * resolution : null
         * size :
         * movie_time :
         * sta : 2
         * bvid :
         * is_original : 0
         * ifjx : 0
         * ifzy : 0
         * is_share : 0
         * actor_id : 0
         * is_hid : 0
         * is_del : 0
         * usdr_attention_count : 1
         * user_nickname : 今夜不回家
         * user_icon : https://lifefang.oss-cn-chengdu.aliyuncs.com/bb1604287211426393.jpg
         */

        private String id;
        private String user_id;
        private String title;
        private String image;
        private String play_url;
        private String play_num;
        private String comment_num;
        private String share_num;
        private String label;
        private String like_num;
        private String download_num;
        private String home_category_id;
        private String recommend_category_id;
        private String hot_category_id;
        private String top_num;
        private String down_num;
        private String collect_num;
        private String grade;
        private String sort;
        private String add_time;
        private String update_time;
        private String recommend;
        private String play_image;
        private String play_time;
        private String content;
        private String is_free;
        private String gold;
        private String download_url;
        private String user_movie_type;
        private String videoid;
        private String type;
        private String down_type;
        private Object img_size;
        private Object img_fb;
        private String if4k;
        private String retrieve_password;
        private String extract_password;
        private String price;
        private Object screenshots;
        private Object resolution;
        private String size;
        private String movie_time;
        private String sta;
        private String bvid;
        private String is_original;
        private String ifjx;
        private String ifzy;
        private String is_share;
        private String actor_id;
        private String is_hid;
        private String is_del;
        private String usdr_attention_count;
        private String user_nickname;
        private String user_icon;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getPlay_url() {
            return play_url;
        }

        public void setPlay_url(String play_url) {
            this.play_url = play_url;
        }

        public String getPlay_num() {
            return play_num;
        }

        public void setPlay_num(String play_num) {
            this.play_num = play_num;
        }

        public String getComment_num() {
            return comment_num;
        }

        public void setComment_num(String comment_num) {
            this.comment_num = comment_num;
        }

        public String getShare_num() {
            return share_num;
        }

        public void setShare_num(String share_num) {
            this.share_num = share_num;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getLike_num() {
            return like_num;
        }

        public void setLike_num(String like_num) {
            this.like_num = like_num;
        }

        public String getDownload_num() {
            return download_num;
        }

        public void setDownload_num(String download_num) {
            this.download_num = download_num;
        }

        public String getHome_category_id() {
            return home_category_id;
        }

        public void setHome_category_id(String home_category_id) {
            this.home_category_id = home_category_id;
        }

        public String getRecommend_category_id() {
            return recommend_category_id;
        }

        public void setRecommend_category_id(String recommend_category_id) {
            this.recommend_category_id = recommend_category_id;
        }

        public String getHot_category_id() {
            return hot_category_id;
        }

        public void setHot_category_id(String hot_category_id) {
            this.hot_category_id = hot_category_id;
        }

        public String getTop_num() {
            return top_num;
        }

        public void setTop_num(String top_num) {
            this.top_num = top_num;
        }

        public String getDown_num() {
            return down_num;
        }

        public void setDown_num(String down_num) {
            this.down_num = down_num;
        }

        public String getCollect_num() {
            return collect_num;
        }

        public void setCollect_num(String collect_num) {
            this.collect_num = collect_num;
        }

        public String getGrade() {
            return grade;
        }

        public void setGrade(String grade) {
            this.grade = grade;
        }

        public String getSort() {
            return sort;
        }

        public void setSort(String sort) {
            this.sort = sort;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public String getUpdate_time() {
            return update_time;
        }

        public void setUpdate_time(String update_time) {
            this.update_time = update_time;
        }

        public String getRecommend() {
            return recommend;
        }

        public void setRecommend(String recommend) {
            this.recommend = recommend;
        }

        public String getPlay_image() {
            return play_image;
        }

        public void setPlay_image(String play_image) {
            this.play_image = play_image;
        }

        public String getPlay_time() {
            return play_time;
        }

        public void setPlay_time(String play_time) {
            this.play_time = play_time;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getIs_free() {
            return is_free;
        }

        public void setIs_free(String is_free) {
            this.is_free = is_free;
        }

        public String getGold() {
            return gold;
        }

        public void setGold(String gold) {
            this.gold = gold;
        }

        public String getDownload_url() {
            return download_url;
        }

        public void setDownload_url(String download_url) {
            this.download_url = download_url;
        }

        public String getUser_movie_type() {
            return user_movie_type;
        }

        public void setUser_movie_type(String user_movie_type) {
            this.user_movie_type = user_movie_type;
        }

        public String getVideoid() {
            return videoid;
        }

        public void setVideoid(String videoid) {
            this.videoid = videoid;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getDown_type() {
            return down_type;
        }

        public void setDown_type(String down_type) {
            this.down_type = down_type;
        }

        public Object getImg_size() {
            return img_size;
        }

        public void setImg_size(Object img_size) {
            this.img_size = img_size;
        }

        public Object getImg_fb() {
            return img_fb;
        }

        public void setImg_fb(Object img_fb) {
            this.img_fb = img_fb;
        }

        public String getIf4k() {
            return if4k;
        }

        public void setIf4k(String if4k) {
            this.if4k = if4k;
        }

        public String getRetrieve_password() {
            return retrieve_password;
        }

        public void setRetrieve_password(String retrieve_password) {
            this.retrieve_password = retrieve_password;
        }

        public String getExtract_password() {
            return extract_password;
        }

        public void setExtract_password(String extract_password) {
            this.extract_password = extract_password;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public Object getScreenshots() {
            return screenshots;
        }

        public void setScreenshots(Object screenshots) {
            this.screenshots = screenshots;
        }

        public Object getResolution() {
            return resolution;
        }

        public void setResolution(Object resolution) {
            this.resolution = resolution;
        }

        public String getSize() {
            return size;
        }

        public void setSize(String size) {
            this.size = size;
        }

        public String getMovie_time() {
            return movie_time;
        }

        public void setMovie_time(String movie_time) {
            this.movie_time = movie_time;
        }

        public String getSta() {
            return sta;
        }

        public void setSta(String sta) {
            this.sta = sta;
        }

        public String getBvid() {
            return bvid;
        }

        public void setBvid(String bvid) {
            this.bvid = bvid;
        }

        public String getIs_original() {
            return is_original;
        }

        public void setIs_original(String is_original) {
            this.is_original = is_original;
        }

        public String getIfjx() {
            return ifjx;
        }

        public void setIfjx(String ifjx) {
            this.ifjx = ifjx;
        }

        public String getIfzy() {
            return ifzy;
        }

        public void setIfzy(String ifzy) {
            this.ifzy = ifzy;
        }

        public String getIs_share() {
            return is_share;
        }

        public void setIs_share(String is_share) {
            this.is_share = is_share;
        }

        public String getActor_id() {
            return actor_id;
        }

        public void setActor_id(String actor_id) {
            this.actor_id = actor_id;
        }

        public String getIs_hid() {
            return is_hid;
        }

        public void setIs_hid(String is_hid) {
            this.is_hid = is_hid;
        }

        public String getIs_del() {
            return is_del;
        }

        public void setIs_del(String is_del) {
            this.is_del = is_del;
        }

        public String getUsdr_attention_count() {
            return usdr_attention_count;
        }

        public void setUsdr_attention_count(String usdr_attention_count) {
            this.usdr_attention_count = usdr_attention_count;
        }

        public String getUser_nickname() {
            return user_nickname;
        }

        public void setUser_nickname(String user_nickname) {
            this.user_nickname = user_nickname;
        }

        public String getUser_icon() {
            return user_icon;
        }

        public void setUser_icon(String user_icon) {
            this.user_icon = user_icon;
        }
}
