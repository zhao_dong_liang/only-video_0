package cn.wu1588.dancer.home.activity;


import android.content.ClipboardManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.OrientationHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.cnsunrun.commonui.util.DouYinLayoutManager;
import com.cnsunrun.commonui.util.OnViewPagerListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lzy.okgo.model.Progress;
import com.maning.mndialoglibrary.MProgressBarDialog;
import com.my.toolslib.OneClickUtils;
import com.my.toolslib.http.utils.LzyResponse;
import com.shuyu.gsyvideoplayer.GSYVideoManager;
import com.shuyu.gsyvideoplayer.listener.GSYSampleCallBack;
import com.shuyu.gsyvideoplayer.video.StandardGSYVideoPlayer;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.adp.SmallVideoPlayerAdapter;
import cn.wu1588.dancer.home.fragment.SmallVideoCommentFragment;
import cn.wu1588.dancer.home.mode.CommectNumBean;
import cn.wu1588.dancer.home.mode.MovieActionStateBean;
import cn.wu1588.dancer.home.mode.MovieBean;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.event.SmallVideoDataEvent;
import cn.wu1588.dancer.common.event.SmallVideoIndexEvent;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.LoginUtil;
import cn.wu1588.dancer.common.util.PageLimitDelegate;

/**
 * 小视频竖直播放页
 */
public class SmallVideoPlayerActivity extends LBaseActivity implements BaseQuickAdapter.OnItemChildClickListener {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    @BindView(R.id.back_bt)
    ImageButton back_bt;
    private ArrayList<MovieBean> movieBeans;
    private int index;
    private SmallVideoPlayerAdapter adapter;
    private DouYinLayoutManager douYinLayoutManager;
    private PageLimitDelegate pageLimitDelegate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setDark(CommonApp.getApplication().isBarTvColor());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_small_video_player);
        ButterKnife.bind(this);
        Bundle extras = getIntent().getExtras();
        String jsonString = extras.getString("movieBeans");
        Gson gson = new Gson();
        movieBeans = gson.fromJson(jsonString, new TypeToken<List<MovieBean>>(){}.getType()); // 参数二：需要指定类型，类型来决定解析的集合
        index = extras.getInt("index");
        initEventBus();
        initViews();
        initListener();
    }


    protected void initViews() {

        back_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        douYinLayoutManager = new DouYinLayoutManager(that, OrientationHelper.VERTICAL, false);
        recyclerView.setLayoutManager(douYinLayoutManager);
        recyclerView.setItemViewCacheSize(5);

        adapter = new SmallVideoPlayerAdapter();
        adapter.setOnItemChildClickListener(this::onItemChildClick);
        recyclerView.setAdapter(adapter);
        recyclerView.scrollToPosition(index);
        douYinLayoutManager.setInitIndex(index);

        pageLimitDelegate = new PageLimitDelegate(new PageLimitDelegate.DataProvider() {
            @Override
            public void loadData(int page) {
                //加载更多
            }
        });
        pageLimitDelegate.attach(refreshLayout, recyclerView, adapter);
        SwipeRefreshLayout refreshLayout = pageLimitDelegate.getRefreshLayout();
        if (refreshLayout != null) {
            refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {//刷新
                    EventBus.getDefault().post(new SmallVideoIndexEvent(-1));
                }
            });
        }
        if (movieBeans != null && movieBeans.size() > 0) {
            pageLimitDelegate.setData(movieBeans);
            GSYVideoManager.releaseAllVideos();
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        GSYVideoManager.releaseAllVideos();
    }

    @Override
    public void onPause() {
        super.onPause();
        GSYVideoManager.onPause();
    }

    /**
     * 释放视频
     *
     * @param index
     */
    private void releaseVideo(int index) {
        //通过子条目 初始化当前视频播放器
        StandardGSYVideoPlayer standardGSYVideoPlayer = null;
        View view = adapter.getViewByPosition(index, R.id.moviePlayer);
        BaseViewHolder holder = (BaseViewHolder) recyclerView.findViewHolderForAdapterPosition(index);
        if (view != null) {
            standardGSYVideoPlayer = (StandardGSYVideoPlayer) view;
        } else {
            if (holder != null) {
                standardGSYVideoPlayer = holder.getView(R.id.moviePlayer);
            }
        }
        if (standardGSYVideoPlayer != null) {
            //开启视频播放器  可以设置一个按钮 点击后进行播放
            standardGSYVideoPlayer.release();
        }


    }

    /**
     * 3 设置监听事件
     */
    private ImageView img = null;
    private View download_num;//下载次数
    private View add_view;//关注用户
    private View fabulous_view;//点赞
    private View fabulous_num;//赞数
    private View share_num;//分享次数
    private View comment_num;//评论次数
    private int position;
    private String user_id;
    private MovieBean movieBean;

    private void initListener() {
        //管理器 设置
        douYinLayoutManager.setOnViewPagerListener(new OnViewPagerListener() {
            @Override
            public void onInitComplete() {

            }

            @Override
            public void onPageRelease(boolean isNext, int position) {
                Log.e("管理器监听：", "释放位置:" + position + " 下一页:" + isNext);

                int index = 0;
                if (isNext) {
                    index = 0;
                } else {
                    index = 1;
                }
                //调用方法 释放视频
                releaseVideo(index);
            }

            @Override
            public void onPageSelected(int position, boolean isBottom) {
                Log.e("管理器监听：", "释放位置:" + position + " 下一页:" + isBottom);
                StandardGSYVideoPlayer standardGSYVideoPlayer = null;
                View view = adapter.getViewByPosition(index, R.id.moviePlayer);
                download_num = adapter.getViewByPosition(index, R.id.download_num);
                add_view = adapter.getViewByPosition(index, R.id.add_view);
                fabulous_view = adapter.getViewByPosition(index, R.id.fabulous_view);
                fabulous_num = adapter.getViewByPosition(index, R.id.fabulous_num);
                share_num = adapter.getViewByPosition(index, R.id.share_num);
                comment_num = adapter.getViewByPosition(index, R.id.comment_num);
                if (recyclerView == null) {
                    return;
                }
                SmallVideoPlayerActivity.this.position = position;
                BaseViewHolder holder = (BaseViewHolder) recyclerView.findViewHolderForAdapterPosition(position);

                MovieBean data = adapter.getData().get(position);
                movieBean = data;
                user_id = data.user_id;
                if (!LoginUtil.startLogin(SmallVideoPlayerActivity.this)) {
                    BaseQuestStart.checkUserAttention(SmallVideoPlayerActivity.this, data.user_id);//判断用户是否关注
                }
                BaseQuestStart.getMoveieActionStatesOk(SmallVideoPlayerActivity.this, data.id + "");//判断是否点赞
                if (view != null) {
                    standardGSYVideoPlayer = (StandardGSYVideoPlayer) view;
                } else {
                    if (holder != null) {
                        standardGSYVideoPlayer = holder.getView(R.id.moviePlayer);
                        download_num = holder.getView(R.id.download_num);
                        add_view = holder.getView(R.id.add_view);
                        fabulous_view = holder.getView(R.id.fabulous_view);
                        fabulous_num = holder.getView(R.id.fabulous_num);
                        share_num = holder.getView(R.id.share_num);
                        comment_num = holder.getView(R.id.comment_num);
                    }
                }
                img = null;
                View imgView = adapter.getViewByPosition(index, R.id.img);
                if (imgView != null) {
                    img = (ImageView) imgView;
                } else {
                    if (holder != null) {
                        img = holder.getView(R.id.img);
                    }
                }

                if (standardGSYVideoPlayer != null) {
                    EventBus.getDefault().post(new SmallVideoIndexEvent(position));
                    standardGSYVideoPlayer.startPlayLogic();
                }
                if (standardGSYVideoPlayer == null) {
                    return;
                }
                //调用方法  播放视频
                standardGSYVideoPlayer.setVideoAllCallBack(new GSYSampleCallBack() {
                    @Override
                    public void onStartPrepared(String url, Object... objects) {
                        //开始加载，objects[0]是title，object[1]是当前所处播放器（全屏或非全屏）
                        super.onStartPrepared(url, objects);
                        if (img != null) {
                            img.setVisibility(View.VISIBLE);
                        }
                        Log.i("视频播放器监听：", "onStartPrepared: " + "视频标题：" + objects[0] + "当前所处播放器：" + objects[1]);
                    }

                    @Override
                    public void onPrepared(String url, Object... objects) {
                        //加载成功，objects[0]是title，object[1]是当前所处播放器（全屏或非全屏）
                        super.onPrepared(url, objects);
                        if (img != null) {
                            img.setVisibility(View.GONE);
                        }
                        Log.i("视频播放器监听：", "onPrepared: " + "视频标题：" + objects[0] + "当前所处播放器：" + objects[1]);
                    }
                });
            }
        });
    }

    private SmallVideoCommentFragment commentFragment;

    @Override
    public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        if (OneClickUtils.isFastDoubleClick(view.getId())) {
            return;
        }
        List<MovieBean> movieBeans = adapter.getData();
        MovieBean bean = movieBeans.get(position);
        switch (view.getId()) {
            case R.id.comment_layout://评论列表
                if (commentFragment == null) {
                    commentFragment = new SmallVideoCommentFragment();
                }
                commentFragment.setMovieBean(bean);
                commentFragment.show(getSupportFragmentManager(), position + "");
                break;
            case R.id.download_layout://下载
                initDownloadDialog();
                mProgressBarDialog.showProgress(0, "已下载0%");
//                   String u="http://chuangfen.oss-cn-hangzhou.aliyuncs.com/public/attachment/201805/100651/201805181532123423.mp4";
//                    BaseQuestStart.downLoadVideo(this,u);
                BaseQuestStart.updateMovieDownloadNum(this, bean.id + "");
                BaseQuestStart.downLoadVideo(this, movieBeans.get(position).download_url);
                break;
            case R.id.share_layout://分享
                ClipboardManager myClipboard = (ClipboardManager) that.getSystemService(CLIPBOARD_SERVICE);
                myClipboard.setText(bean.play_url);
                ToastUtils.longToast("复制成功，去分享吧！");
                BaseQuestStart.updateMovieShareNum(this, bean.id + "");
                break;
            case R.id.add_view://关注用户
                BaseQuestStart.followUser(this, bean.user_id);
                break;
            case R.id.fabulous_view://点赞
                BaseQuestStart.movieZanOk(this, 1, bean.id + "");
                break;
            case R.id.header_img://用户头像点击
                CommonIntent.startOtherUserActiivty(that, bean.user_id);
                break;
            case R.id.text_4k:
                //4k下载
                break;
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (!LoginUtil.startLogin(SmallVideoPlayerActivity.this) && user_id != null) {
            BaseQuestStart.checkUserAttention(SmallVideoPlayerActivity.this, user_id);//判断用户是否关注
        }
    }

    @Override
    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
        super.nofityUpdateUi(requestCode, response, view);
        if (requestCode == BaseQuestStart.UPDATE_MOVIE_DOWNLOAD_NUM_CODE) {//新增下载次数成功
            if (download_num != null) {
                TextView download_tv = (TextView) download_num;
                MovieBean bean = adapter.getData().get(position);
                download_tv.setText((bean.download_num + 1) + "");
                bean.download_num = bean.download_num + 1;

                SmallVideoIndexEvent event = new SmallVideoIndexEvent(position);
                event.setBean(bean);
                EventBus.getDefault().post(event);
            }
        } else if (requestCode == BaseQuestStart.CHECK_USER_ATTENTION_CODE) {//判断用户是否关注了
            if (response.code == 1) {
                add_view.setVisibility(View.INVISIBLE);
            } else {
                add_view.setVisibility(View.VISIBLE);
            }
        } else if (requestCode == BaseQuestStart.ATTENTION_USER_CODE) {//关注用户
            if (response.code == 1) {
                LzyResponse res = new LzyResponse();
                res.code = 1;
                nofityUpdateUi(BaseQuestStart.CHECK_USER_ATTENTION_CODE, res, null);
            } else {
                ToastUtils.longToast("关注失败");
            }
        } else if (requestCode == BaseQuestStart.GET_MOVIE_ACTION_STATES_CODE) {
            if (response.code == 1) {
                MovieActionStateBean movieActionStateBean = (MovieActionStateBean) response.data;
                if (movieActionStateBean.isAlowZan()) {//未赞
                    fabulous_view.setBackgroundResource(R.drawable.fabulous_icon);
                    fabulous_view.setFocusable(true);
                    fabulous_view.setEnabled(true);
                } else {//已赞
                    fabulous_view.setBackgroundResource(R.drawable.fabuloused_icon);
                    fabulous_view.setFocusable(false);
                    fabulous_view.setEnabled(false);
                }
            }
        } else if (requestCode == BaseQuestStart.MOVIE_DIANZAN_CODE) {//视频点赞
            if (response.code == 1) {
                ToastUtils.longToast("点赞成功");
                LzyResponse<MovieActionStateBean> res = new LzyResponse<>();
                res.code = 1;
                MovieActionStateBean stateBean = new MovieActionStateBean();
                stateBean.top = 1;
                res.data = stateBean;
                nofityUpdateUi(BaseQuestStart.GET_MOVIE_ACTION_STATES_CODE, res, null);

                if (fabulous_num != null) {
                    TextView fabulousTv = (TextView) fabulous_num;
                    MovieBean bean = adapter.getData().get(position);
                    fabulousTv.setText((bean.top_num + 1) + "");
                    bean.top_num = bean.top_num + 1;

                    SmallVideoIndexEvent event = new SmallVideoIndexEvent(position);
                    event.setBean(bean);
                    EventBus.getDefault().post(event);
                }
            }
        } else if (requestCode == BaseQuestStart.UPDATE_MOVIE_SHARE_NUM_CODE) {//记录分享次数成功
            if (share_num != null) {
                TextView shareTv = (TextView) share_num;
                MovieBean bean = adapter.getData().get(position);
                shareTv.setText((bean.share_num + 1) + "");
                bean.share_num = bean.share_num + 1;

                SmallVideoIndexEvent event = new SmallVideoIndexEvent(position);
                event.setBean(bean);
                EventBus.getDefault().post(event);
            }

        }
    }

    // EventBus.getDefault().post(new MessageEvent("login_success","1"));
    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void eventBusMethod(SmallVideoDataEvent event) {
        ArrayList<MovieBean> movieBeans = (ArrayList<MovieBean>) event.getMovieBeans();
        if (movieBeans != null && movieBeans.size() > 0) {
            pageLimitDelegate.setData(movieBeans);
            pageLimitDelegate.loadComplete();
            Log.i("NetServer:", movieBeans.size() + "==接收" + adapter.getData().size());
            if (pageLimitDelegate.page <= 1) {
                adapter.notifyDataSetChanged();
            }

        } else {
            adapter.setEnableLoadMore(false);
        }
    }


    @Override
    public void onProgress(Progress progress) {
        super.onProgress(progress);
        if (mProgressBarDialog != null) {
            Log.i("OkGo:", "progress==" + progress.fraction);
            int fraction = (int) (progress.fraction * 100);
            mProgressBarDialog.showProgress(fraction, "已下载" + fraction + "%", true);
        }
    }

    @Override
    public void downLoadFinish(File file) {
        super.downLoadFinish(file);
        ToastUtils.longToast("下载完成");
    }

    @Override
    public void httpLoadFail(String err) {
        super.httpLoadFail(err);
        ToastUtils.longToast(err);
    }

    @Override
    public void httpLoadFinal() {
        super.httpLoadFinal();
        if (mProgressBarDialog != null) {
            mProgressBarDialog.dismiss();
        }
    }

    private MProgressBarDialog mProgressBarDialog;

    private void initDownloadDialog() {
        //新建一个Dialog
        mProgressBarDialog = new MProgressBarDialog.Builder(this)
                //全屏模式
                .isWindowFullscreen(true)
                .setStyle(MProgressBarDialog.MProgressBarDialogStyle_Circle)
                //全屏背景窗体的颜色
                .setBackgroundWindowColor(Color.TRANSPARENT)
                //View背景的颜色
                .setBackgroundViewColor(Color.BLACK)
                //字体的颜色
                .setTextColor(Color.WHITE)
                //View边框的颜色
                .setStrokeColor(Color.TRANSPARENT)
                //View边框的宽度
                .setStrokeWidth(2)
                //View圆角大小
                .setCornerRadius(10)
                //ProgressBar背景色
                .setProgressbarBackgroundColor(Color.BLACK)
                //ProgressBar 颜色
                .setProgressColor(Color.WHITE)
                //圆形内圈的宽度
                .setCircleProgressBarWidth(4)
                //圆形外圈的宽度
                .setCircleProgressBarBackgroundWidth(4)
                //水平进度条Progress圆角
                .setProgressCornerRadius(0)
                //水平进度条的高度
                .setHorizontalProgressBarHeight(10)
                //dialog动画
//                .setAnimationID(R.style.animate_dialog_custom)
                .build();
    }


    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void eventBusMethod(CommectNumBean event) {
        int num = Integer.parseInt((movieBean.comment_num == null) ? "0" : (movieBean.comment_num)) + 1;
        movieBean.comment_num = num + "";
        if (comment_num instanceof TextView) {
            TextView tv = (TextView) comment_num;
            tv.setText(movieBean.comment_num);
        }
    }
}
