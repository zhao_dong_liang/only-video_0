package cn.wu1588.dancer.home.mode;

import java.util.List;

public class HomeBean {


    public List<BannerListBean> banner_list;
    public List<CategoryListBean> category_list;
    public List<DataListBean> data_list;
    public String marquee;

    public static class BannerListBean {
        /**
         * image : www.a.com
         * url : www.a.com
         * title : 头部广告
         */

        public String image;
        public String url;
        public String title;
        public int type;

    }

    public static class CategoryListBean {
        /**
         * id : 1
         * title : 无码
         * logo : 
         * sort : 0
         * add_time : 2019-03-28 14:15:31
         * update_time : 0000-00-00 00:00:00
         * is_hid : 0
         * is_del : 0
         */

        public String id;
        public String title;
        public String logo;
        public String sort;
        public String add_time;
        public String update_time;
        public String is_hid;
        public String is_del;


    }

    public static class DataListBean {
        /**
         * ids : 1
         * title : 最新片源
         * isMore : true
         * stype : 5
         * id : 8
         * image : uploads/aaa/bbb
         * play_url : uploads/aaa/bbb
         * guessLikes : [{"id":"3","title":"影片二一","image":"uploads/aaa/bbb","play_url":"uploads/aaa/bbb","stype":3},{"id":"4","title":"影片二二","image":"uploads/aaa/bbb","play_url":"uploads/aaa/bbb","stype":3},{"id":"5","title":"影片三一","image":"uploads/aaa/bbb","play_url":"uploads/aaa/bbb","stype":3},{"id":"6","title":"影片三二","image":"uploads/aaa/bbb","play_url":"uploads/aaa/bbb","stype":3},{"id":"7","title":"影片四一","image":"uploads/aaa/bbb","play_url":"uploads/aaa/bbb","stype":3},{"id":"8","title":"影片四二","image":"uploads/aaa/bbb","play_url":"uploads/aaa/bbb","stype":3}]
         * place_id : 2
         * url : www.a.com
         * sort : 0
         * add_time : 0000-00-00 00:00:00
         * update_time : 0000-00-00 00:00:00
         * is_hid : 0
         * is_del : 0
         * type : 0
         * content : 一
         * logo : uploads/一
         * background_image : uploads/一
         * is_recommend : 1
         * recommend_category_id : 2
         */

        public String ids;
        public String title;
        public String is_free;
        public String isMore;
        public int stype;
        public String id;
        public String image;
        public String play_url;
        public String place_id;
        public String url;
        public String sort;
        public String add_time;
        public String update_time;
        public String is_hid;
        public String is_del;
        public String type;
        public String content;
        public String grade;
        public String logo;
        public String background_image;
        public String is_recommend;
        public String recommend_category_id;
        public String style;
        public List<GuessLikesBean> guessLikes;

        public String getImage() {
            return image;
        }

        public static class GuessLikesBean {
            /**
             * id : 3
             * title : 影片二一
             * image : uploads/aaa/bbb
             * play_url : uploads/aaa/bbb
             * stype : 3
             */

            public String id;
            public String title;
            public String image;
            public String play_url;
            public String grade;
            public int stype;

        }
    }
}
