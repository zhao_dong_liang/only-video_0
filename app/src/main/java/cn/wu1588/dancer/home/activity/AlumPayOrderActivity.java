package cn.wu1588.dancer.home.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.cnsunrun.commonui.widget.button.RoundButton;
import com.makeramen.roundedimageview.RoundedImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;
import cn.wu1588.dancer.mine.mode.BuyAlbumBean;

public class AlumPayOrderActivity extends LBaseActivity {

    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.iv_video_author_avatar)
    RoundedImageView ivVideoAuthorAvatar;
    @BindView(R.id.tv_video_author_name)
    TextView tvVideoAuthorName;
    @BindView(R.id.iv_alum_image)
    RoundedImageView ivAlumImage;
    @BindView(R.id.btn_alum_tag)
    RoundButton btnAlumTag;
    @BindView(R.id.btn_alum_count)
    RoundButton btnAlumCount;
    @BindView(R.id.fl_alum_image)
    FrameLayout flAlumImage;
    @BindView(R.id.tv_alum_title)
    TextView tvAlumTitle;
    @BindView(R.id.tv_alum_price)
    TextView tvAlumPrice;
    @BindView(R.id.tv_alum_real_price)
    TextView tvAlumRealPrice;
    @BindView(R.id.btn_pay)
    RoundButton btnPay;

    private BuyAlbumBean mBuyAlbumBean;

    public static void start(Context context, BuyAlbumBean bean) {
        Intent intent = new Intent(context, AlumPayOrderActivity.class);
        intent.putExtra("buyAlumBean", bean);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBuyAlbumBean = getIntent().getParcelableExtra("buyAlumBean");
        setContentView(R.layout.activity_alum_pay_order);
        ButterKnife.bind(this);

        GlideMediaLoader.loadHead(this, ivVideoAuthorAvatar, mBuyAlbumBean.user_info.icon);
        tvVideoAuthorName.setText(mBuyAlbumBean.user_info.nickname);
        GlideMediaLoader.load(this, ivAlumImage, mBuyAlbumBean.background_image, R.drawable.ic_place_ad_img);
        btnAlumTag.setText("教学视频");
        btnAlumCount.setText(String.format("共%s节", mBuyAlbumBean.movies_count));
        tvAlumTitle.setText(mBuyAlbumBean.title);
        tvAlumPrice.setText(String.format("¥%s", mBuyAlbumBean.price));
        tvAlumRealPrice.setText(String.format("¥%s", mBuyAlbumBean.price));
    }

    @OnClick(R.id.btn_pay)
    void startPay(View view) {
        CommonIntent.startAlumPayActivity(this, mBuyAlbumBean);
    }
}
