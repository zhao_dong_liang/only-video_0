package cn.wu1588.dancer.home.activity;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kongzue.baseframework.interfaces.BindView;
import com.kongzue.baseframework.interfaces.DarkStatusBarTheme;
import com.kongzue.baseframework.interfaces.Layout;
import com.kongzue.baseframework.util.JumpParameter;
import com.kongzue.baseokhttp.HttpRequest;
import com.kongzue.baseokhttp.listener.ResponseListener;
import com.kongzue.baseokhttp.util.JsonList;
import com.kongzue.baseokhttp.util.JsonMap;
import com.kongzue.baseokhttp.util.Parameter;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.adp.RelatedVideoAdapter;
import cn.wu1588.dancer.base.BaseAty;
import cn.wu1588.dancer.common.quest.BaseQuestConfig;
import cn.wu1588.dancer.common.util.RecycleHelper;
import cn.wu1588.dancer.utils.map.MD5Utils;

/**
 * 这个activity是 超哥的baseJson和baseQuickAdapter结合使用
 */
@Layout(R.layout.activity_test1)
@DarkStatusBarTheme(true)
public class Test1Activity extends BaseAty {


    @BindView(R.id.rv_test1)
    private RecyclerView recyclerView;
    private RelatedVideoAdapter adapter;

    @Override
    public void initViews() {
        recyclerView.setNestedScrollingEnabled(false);
        RecycleHelper.setLinearLayoutManager(recyclerView, LinearLayoutManager.VERTICAL);
    }

    @Override
    public void initDatas(JumpParameter parameter) {
        getMovieByCate();
    }

    @Override
    public void setEvents() {

    }


    /**
     * 当期分类按照用户依次获取视频
     */
    private void getMovieByCate(){
            HttpRequest.GET(me,
                    BaseQuestConfig.GET_NEW_MOVIE_BY_CATE,
                    new Parameter()
                            .add("md5", MD5Utils.md5())
                            .add("cate_id", 15)
                            .add("num", 10),
                    new ResponseListener() {
                        @Override
                        public void onResponse(String main, Exception error) {
                            if(error == null){
                                JsonMap jsonMap = JsonMap.parse(main);
                                if(jsonMap.getString("status").equals("1")){
                                    JsonList data = jsonMap.getList("data");
                                    adapter = new RelatedVideoAdapter(data);
                                    recyclerView.setAdapter(adapter);
                                }
                            }
                        }
                    }
            );

    }
}