package cn.wu1588.dancer.home.activity;

import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cnsunrun.commonui.widget.button.RoundButton;
import com.makeramen.roundedimageview.RoundedImageView;
import com.my.toolslib.http.utils.NetRequestListenerProxy;
import com.sunrun.sunrunframwork.bean.BaseBean;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.home.mode.MovieDetailBean;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.videoplayer.SwitchVideo;

public class NewMoviePlayerActivity extends LBaseActivity {


    @BindView(R.id.rl_title_layout)
    RelativeLayout rlTitleLayout;
    @BindView(R.id.moviePlayer)
    SwitchVideo moviePlayer;
    @BindView(R.id.tv_movie_title)
    TextView tvMovieTitle;
    @BindView(R.id.iv_video_author_avatar)
    RoundedImageView ivVideoAuthorAvatar;
    @BindView(R.id.tv_video_author_name)
    TextView tvVideoAuthorName;
    @BindView(R.id.tv_video_author_desc)
    TextView tvVideoAuthorDesc;
    @BindView(R.id.btn_movie_subscription)
    RoundButton btnMovieSubscription;
    @BindView(R.id.tv_movie_like)
    TextView tvMovieLike;
    @BindView(R.id.tv_movie_dont_like)
    TextView tvMovieDontLike;
    @BindView(R.id.tv_movie_share)
    TextView tvMovieShare;
    @BindView(R.id.tv_movie_download)
    TextView tvMovieDownload;
    @BindView(R.id.tv_movie_collect)
    TextView tvMovieCollect;
    @BindView(R.id.tv_buy_vip)
    TextView tvBuyVip;
    @BindView(R.id.tv_buy_scope)
    TextView tvBuyScope;
    @BindView(R.id.tv_may_also_like_btn)
    TextView tvMayAlsoLikeBtn;
    @BindView(R.id.ll_may_also_like)
    LinearLayout llMayAlsoLike;
    @BindView(R.id.fl_comment_layout)
    FrameLayout flCommentLayout;

    private String mMovieId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMovieId = getIntent().getStringExtra("movieId");
        setContentView(R.layout.activity_movie_player_new);
        ButterKnife.bind(this);

        loadData();
    }

    private void loadData() {
        BaseQuestStart.getMoviePlayDetail(new NetRequestListenerProxy(this) {
            @Override
            public void nofityUpdate(int i, BaseBean baseBean) {
                if (baseBean.status == 1) {
                    MovieDetailBean movieDetailBean = baseBean.Data();
                    tvMovieTitle.setText(movieDetailBean.title);
                }
            }
        }, mMovieId, "2");
    }
}
