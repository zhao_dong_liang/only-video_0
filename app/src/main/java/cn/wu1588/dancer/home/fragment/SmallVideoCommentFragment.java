package cn.wu1588.dancer.home.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.adp.MovieCommectAdapter;
import cn.wu1588.dancer.home.mode.MovieBean;

public class SmallVideoCommentFragment extends BottomSheetDialogFragment {

    public MovieBean movieBean;
    public MovieCommectAdapter adapter;
    private boolean isShowActions;

    //调试
    public void setMovieBean(MovieBean movieBean, boolean isShowActions) {
        this.movieBean = movieBean;
        this.isShowActions = isShowActions;
    }

    public void setMovieBean(MovieBean movieBean) {
        setMovieBean(movieBean, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //设置背景透明，才能显示出layout中诸如圆角的布局，否则会有白色底（框）
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.TransCustomBottomSheetDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.bottom_sheet_dialog_comment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        CommentFragment commentFragment = new CommentFragment();
        commentFragment.setMovieBean(movieBean, isShowActions);
        commentFragment.setCommentHeight((int) getResources().getDimension(R.dimen.dp_360));
        commentFragment.setOnCloseClickListener(v -> dismissAllowingStateLoss());
        getChildFragmentManager()
                .beginTransaction()
                .add(R.id.fl_comment_layout, commentFragment, CommentFragment.class.getSimpleName())
                .commit();
    }
}
