package cn.wu1588.dancer.home.mode;

import java.io.Serializable;
import java.util.List;

/**
 * Created by zhou on 2021/2/9 18:42.
 */
public class MovieByCateV3Bean implements Serializable {


        /**
         * id : 3944
         * user_id : 206
         * title : 姑娘窑洞前讲解鬼步舞难点，很实用的一个基础步，你们会了吗
         * play_url : http://video.myylook.com/sv/661e641-17604c2c166/661e641-17604c2c166.mp4
         * image : http://video.myylook.com/1cdc370c9f624b2e89dfc2f59eddeb0a/snapshots/02abd6b5ba484d0f806e001163792ca5-00007.jpg
         * play_num : 4
         * like_num : 0
         * if4k : 0
         * add_time : 2020-11-26 22:28:22
         * user_name : null
         * user_icon : null
         */

        private String id;
        private String user_id;
        private String title;
        private String play_url;
        private String image;
        private String play_num;
        private String like_num;
        private String if4k;
        private String add_time;
        private Object user_name;
        private Object user_icon;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPlay_url() {
            return play_url;
        }

        public void setPlay_url(String play_url) {
            this.play_url = play_url;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getPlay_num() {
            return play_num;
        }

        public void setPlay_num(String play_num) {
            this.play_num = play_num;
        }

        public String getLike_num() {
            return like_num;
        }

        public void setLike_num(String like_num) {
            this.like_num = like_num;
        }

        public String getIf4k() {
            return if4k;
        }

        public void setIf4k(String if4k) {
            this.if4k = if4k;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public Object getUser_name() {
            return user_name;
        }

        public void setUser_name(Object user_name) {
            this.user_name = user_name;
        }

        public Object getUser_icon() {
            return user_icon;
        }

        public void setUser_icon(Object user_icon) {
            this.user_icon = user_icon;
        }

}
