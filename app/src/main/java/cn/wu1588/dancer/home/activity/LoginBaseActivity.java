package cn.wu1588.dancer.home.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;

import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;
import com.tencent.connect.common.Constants;
import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import androidx.annotation.Nullable;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.event.MessageEvent;
import cn.wu1588.dancer.common.model.LoginInfo;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.common.util.ConstantValue;
import cn.wu1588.dancer.common.util.DeviceIdUtil;
import cn.wu1588.dancer.common.util.SPUtils;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_USER_INFO_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.QQ_LOGIN_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.WX_LOGIN_CODE;

public class LoginBaseActivity extends LBaseActivity {
    private Activity activity;
    private IWXAPI mWxApi;
    private QQLoginListener qqLoginListener;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = getSharedPreferences("login", MODE_PRIVATE);
        editor = sharedPreferences.edit();
        initEventBus();
    }
    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void eventBusMethod(MessageEvent event) {
        String type = event.getType();
        if (type.equals("wx_code")){
            String code = event.getContent();
            if (!TextUtils.isEmpty(code)){
                String deviceId = DeviceIdUtil.getDeviceId(that);
                if (TextUtils.isEmpty(deviceId)) {
                    BaseQuestStart.WXLogin(that, code, "");
                }else {
                    BaseQuestStart.WXLogin(that, code, deviceId);
                }
            }
        }
    }

    public  void QQLogin(Activity activitys){
        activity = activitys;
        String qqid = (String) SPUtils.get(that, ConstantValue.QQ_ID, "0");
        Tencent tencent = Tencent.createInstance(qqid, this.getApplicationContext());
        qqLoginListener = new QQLoginListener();
        tencent.login(this, "get_user_info", qqLoginListener);
    }
    public  void WXLogin(Activity activitys){
        activity = activitys;
        String wechatid = (String) SPUtils.get(that, ConstantValue.WECHAT_ID, "0");
        mWxApi = WXAPIFactory.createWXAPI(this, wechatid, true);
        // 将该app注册到微信
        mWxApi.registerApp(wechatid);
        final SendAuth.Req req = new SendAuth.Req();
        req.scope = "snsapi_userinfo";
        mWxApi.sendReq(req);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode ==  Constants.REQUEST_LOGIN) {
            Tencent.onActivityResultData(requestCode, resultCode, data, qqLoginListener);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public class QQLoginListener implements IUiListener {
        @Override
        public void onComplete(Object object) { //登录成功
            JSONObject jb = (JSONObject) object;
            try {
                String access_token = jb.getString("access_token");

                if (!TextUtils.isEmpty(access_token)) {
                    String deviceId = DeviceIdUtil.getDeviceId(that);
                    if (TextUtils.isEmpty(deviceId)) {
                        BaseQuestStart.QQLogin(that, access_token, "");
                    }else {
                        BaseQuestStart.QQLogin(that, access_token, deviceId);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(UiError uiError) {  //登录失败
            ToastUtils.shortToast("登录失败");
        }
        @Override
        public void onCancel() {                //取消登录
            ToastUtils.shortToast("取消登录");
        }
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode) {
            case GET_USER_INFO_CODE:
                if (bean.status == 1) {
                    if (activity!=null) activity.finish();
                    LoginInfo loginInfo = bean.Data();
                    if (loginInfo != null) {
                        loginInfo.setUid(loginInfo.id);
                        Config.putLoginInfo(loginInfo);
                        SPUtils.put(that, ConstantValue.IS_LOGIN,true);
                        editor.putString("userid", loginInfo.uid);
                        //提交保存数据
                        editor.commit();
                        finish();
                    }
                    EventBus.getDefault().post(new MessageEvent("login_success","1"));
                }
                break;
            case QQ_LOGIN_CODE:
                if (bean.status == 1) {
                    ToastUtils.shortToast(bean.msg);
                    LoginInfo loginInfo = bean.Data();
                    if (loginInfo != null) {
                        Config.putLoginInfo(loginInfo);
                        //存储数据时选用对应类型的方法
                        SPUtils.put(that, ConstantValue.IS_LOGIN,true);
                        editor.putString("userid", loginInfo.uid);
                        //提交保存数据
                        editor.commit();
                    }
                    BaseQuestStart.getUserInfo(this);
                }
                break;
            case WX_LOGIN_CODE:
                if (bean.status == 1) {
                    ToastUtils.shortToast(bean.msg);
                    LoginInfo loginInfo = bean.Data();
                    if (loginInfo != null) {
                        Config.putLoginInfo(loginInfo);
                        SPUtils.put(that, ConstantValue.IS_LOGIN,true);
                        editor.putString("userid", loginInfo.uid);
                        //提交保存数据
                        editor.commit();
                    }
                    BaseQuestStart.getUserInfo(this);
                }
                break;
        }
        super.nofityUpdate(requestCode, bean);
    }
}
