package cn.wu1588.dancer.home.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.shuyu.gsyvideoplayer.builder.GSYVideoOptionBuilder;
import com.shuyu.gsyvideoplayer.listener.GSYSampleCallBack;
import com.shuyu.gsyvideoplayer.listener.GSYVideoProgressListener;
import com.shuyu.gsyvideoplayer.listener.LockClickListener;
import com.shuyu.gsyvideoplayer.utils.GSYVideoHelper;
import com.shuyu.gsyvideoplayer.video.StandardGSYVideoPlayer;
import com.shuyu.gsyvideoplayer.video.base.GSYVideoPlayer;

import butterknife.BindView;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.home.mode.MovieBean;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.common.util.DataUtils;
import cn.wu1588.dancer.common.util.SPUtils;
import cn.wu1588.dancer.home.activity.SmallVideoPlayerActivity;

public class SmallVideoPlayerFragment extends LBaseFragment  {

    @BindView(R.id.moviePlayer)
    StandardGSYVideoPlayer moviePlayer;

    private MovieBean movieBean;
    //是否扣过金币
    private boolean isDeductGold;
    //会员是否过期
    private boolean memberDateMature;
    private boolean isDiscover;
    private int freeTime = 60;
    private int index;

    public static  SmallVideoPlayerFragment newInstance(MovieBean movieBean, int index){
        SmallVideoPlayerFragment fragment=new SmallVideoPlayerFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("index",index);
        bundle.putParcelable("bean",movieBean);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        movieBean = getArguments().getParcelable("bean");
        index = getArguments().getInt("index");
        Log.i("aa==","onViewCreated=="+index);
        freeTime = Integer.parseInt(Config.getData("free_play_time","20"));
        if (movieBean!=null){

            initViews();
            moviePlayer.getTitleTextView().setVisibility(View.GONE);
            moviePlayer.getFullscreenButton().setVisibility(View.GONE);
            moviePlayer.getBackButton().setVisibility(View.GONE);
            SmallVideoPlayerActivity activity= (SmallVideoPlayerActivity) getActivity();
//            int initIndex = activity.getInitIndex();
//            if (initIndex==index){
                startPlay();
//                activity.setInitIndex(-1);
//            }
        }
    }

    private void initViews(){
        if (moviePlayer!=null){
            gsyVideoOption = new GSYVideoHelper.GSYVideoHelperBuilder();

            ImageView imageView=new ImageView(getActivity());
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
//            imageView.setImageResource(R.mipmap.icon_qq);
            GlideMediaLoader.load(getActivity(),imageView,movieBean.image);
            moviePlayer.setThumbImageView(imageView);
            gsyVideoOption .setThumbPlay(true)
                    .setThumbImageView(imageView);
        }

    }
    private void startPlay(){
        if (moviePlayer!=null){
            //播放
            setPlayVideo(movieBean);
        }

    }
    @Override
    public int getLayoutRes() {
        return R.layout.small_video_player_fragment;
    }

    /**
     * 设置播放的Url
     *
     * @param movieBean
     */
    private GSYVideoOptionBuilder gsyVideoOption;
    GSYVideoHelper   smallVideoHelper;
    private void setPlayVideo(MovieBean movieBean) {
        gsyVideoOption
                .setLooping(true)
                .setIsTouchWiget(true)
                .setStartAfterPrepared(true)//加载完自动播放
                .setNeedLockFull(true)
                .setIsUpDownTouch(false)
                .setCacheWithPlay(true)
                .setShowFullAnimation(false)
                .setRotateViewAuto(false)
                .setLockLand(true)
                .setIsTouchWigetFull(false)
                .setLooping(true)
                .setVideoAllCallBack(new GSYSampleCallBack() {
                    @Override
                    public void onEnterFullscreen(String url, Object... objects) {
                        super.onEnterFullscreen(url, objects);
                        GSYVideoPlayer gsyVideoPlayer = (GSYVideoPlayer) objects[1];
                        gsyVideoPlayer.setGSYVideoProgressListener(new GSYVideoProgressListener() {
                            @Override
                            public void onProgress(int progress, int secProgress, int currentPosition, int duration) {
                                if (!movieBean.isFree()) {
                                    if (!Config.getLoginInfo().isMember()) {
                                        //判断是否限时播放视频
                                        stopOrstartVideo(currentPosition);
                                    }else {
                                        if (!memberDateMature){
                                            //判断是否限时播放视频
                                            stopOrstartVideo(currentPosition);
                                        }
                                    }
                                }
                            }
                        });
                    }

                    @Override
                    public void onPrepared(String url, Object... objects) {
                        super.onPrepared(url, objects);
                        //开始播放了才能旋转和全屏
//                        orientationUtils.setEnable(true);
//                        isPlay = true;
                    }

                    @Override
                    public void onQuitFullscreen(String url, Object... objects) {
                        super.onQuitFullscreen(url, objects);
//                        if (orientationUtils != null) {
//                            orientationUtils.backToProtVideo();
//                        }
                    }
                }).setLockClickListener(new LockClickListener() {
            @Override
            public void onClick(View view, boolean lock) {
//                if (orientationUtils != null) {
//                    //配合下方的onConfigurationChanged
//                    orientationUtils.setEnable(!lock);
//                }
            }
        }).setGSYVideoProgressListener(new GSYVideoProgressListener() {
            @Override
            public void onProgress(int progress, int secProgress, int currentPosition, int duration) {
                if (!movieBean.isFree()) {
                    if (!Config.getLoginInfo().isMember()) {
                        stopOrstartVideo(currentPosition);
                    }else {
                        //如果会员过期
                        if (!memberDateMature){
                            stopOrstartVideo(currentPosition);
                        }

                    }
                }
            }
        }).build(moviePlayer);

//        if (!isDiscover) {
            moviePlayer.setUp(movieBean.play_url, false, movieBean.title);
//        }
//        gsyVideoOption.setVideoTitle(movieBean.title);

    }


    /**
     * 判断会员，是否过期，扣除金币的限时操作
     * @param currentPosition
     */
    public void stopOrstartVideo(int currentPosition){
        //如果不是会员，去扣除金币在播放
        if (movieBean.gold>0 ) {
            if (currentPosition > freeTime * 1000) {
                if (!isDeductGold) {
                    //计算购买金币是否超过24小时
                    long currentTimeMillis = System.currentTimeMillis();
                    long saveTimeMillis = (long) SPUtils.get(that, Config.getLoginInfo().uid + movieBean.id, 1568111276000l);
                    Log.i("onlyVideo", "当前的时间：" + currentTimeMillis + "--" + saveTimeMillis);
                    int hour = DataUtils.getOffectHour(currentTimeMillis, saveTimeMillis);
                    Log.i("onlyVideo", "当前相差的时间：" + hour);
                    if (hour > 24) {
                        Log.i("onlyVideo", "去支付金币");
                        BaseQuestStart.deductGoldAccount(SmallVideoPlayerFragment.this, movieBean.gold + "");
                    }else {
                        isDeductGold = true;
                    }
                }
            }
        }
    }

    @Override
    public void onVisible() {
        super.onVisible();
        startPlay();
        Log.i("aa==","onVisible=="+index);
    }

    @Override
    public void onInVisible() {
        super.onInVisible();
        Log.i("aa==","onInVisible=="+index);
    }

}
