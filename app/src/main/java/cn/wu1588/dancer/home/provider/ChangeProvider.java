package cn.wu1588.dancer.home.provider;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.provider.BaseItemProvider;

import cn.wu1588.dancer.adp.HomeNewAdapter;
import cn.wu1588.dancer.home.mode.HomeBean;

/**
 * 换一换
 */
public class ChangeProvider extends BaseItemProvider<HomeBean.DataListBean,BaseViewHolder> {
    @Override
    public int viewType() {
        return HomeNewAdapter.TYPE_CHANGE_AND_CHANGE;
    }

    @Override
    public int layout() {
        return R.layout.item_home_change;
    }

    @Override
    public void convert(BaseViewHolder helper, HomeBean.DataListBean data, int position) {

    }
}
