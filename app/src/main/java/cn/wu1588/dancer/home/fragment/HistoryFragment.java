package cn.wu1588.dancer.home.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemChildClickListener;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.sunrun.sunrunframwork.utils.EmptyDeal;
import com.sunrun.sunrunframwork.utils.Utils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.adp.HistoryRecordAdapter;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.common.dialog.MessageTipDialog;
import cn.wu1588.dancer.common.event.MessageEvent;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.PageLimitDelegate;
import cn.wu1588.dancer.home.mode.MovieBean;
import cn.wu1588.dancer.home.mode.MovieHisBean;
import cn.wu1588.dancer.share.ShareBottomSheetDialog;
import cn.wu1588.dancer.share.ShareWatchHisDialog;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_MY_COLLECTION_LIST_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.POST_DEL_MOVIE_CODE;

/**
 * 历史记录
 */
public class HistoryFragment extends LBaseFragment {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    @BindView(R.id.tvAll)
    CheckBox tvAll;
    @BindView(R.id.tvDel)
    CheckBox tvDel;
    @BindView(R.id.llBottom)
    LinearLayout llBottom;
    private HistoryRecordAdapter historyRecordAdapter;
    private String type_id;
    PageLimitDelegate pageLimitDelegate = new PageLimitDelegate(new PageLimitDelegate.DataProvider() {
        @Override
        public void loadData(int page) {
            BaseQuestStart.getMyCollectList(HistoryFragment.this, page, "1", type_id);
        }
    });
    private List<MovieHisBean> selectList;

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_history;
    }

    public static HistoryFragment newInstance(String type_id) {
        HistoryFragment historyFragment = new HistoryFragment();
        Bundle bundle = new Bundle();
        bundle.putString("type_id", type_id);
        historyFragment.setArguments(bundle);
        return historyFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        type_id = getArguments().getString("type_id");
        initEventBus();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();

    }


    private void initViews() {
        refreshLayout.setColorSchemeColors(getResources().getColor(R.color.main_button_color));
        historyRecordAdapter = new HistoryRecordAdapter();
        recyclerView.setAdapter(historyRecordAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(that));
        pageLimitDelegate.attach(refreshLayout, recyclerView, historyRecordAdapter);
        recyclerView.addOnItemTouchListener(new OnItemChildClickListener() {
            @Override
            public void onSimpleItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                MovieHisBean item = historyRecordAdapter.getItem(position);
                switch (view.getId()) {
                    case R.id.checkbox:
                        if (historyRecordAdapter.getSelectList().contains(item)) {
                            historyRecordAdapter.getSelectList().remove(item);
                        } else {
                            historyRecordAdapter.getSelectList().add(item);
                        }
                        historyRecordAdapter.notifyDataSetChanged();
                        if (historyRecordAdapter.getData().size() == historyRecordAdapter.getSelectList().size()) {
                            tvAll.setText("取消全选");
                            tvAll.setChecked(true);
                        } else {
                            tvAll.setText("全选");
                            tvAll.setChecked(false);
                        }
                        break;
                    case R.id.item_watch_history_share:
                        ShareWatchHisDialog.newInstance(item).show(getChildFragmentManager(), ShareBottomSheetDialog.TAG);
                        break;
                    case R.id.rlContainer:
                        CommonIntent.startMoviePlayerActivity(that, String.valueOf(item.mo_id));
                        break;
                }
            }
        });

    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode) {
            case GET_MY_COLLECTION_LIST_CODE:
                List<MovieBean> movieBeans = bean.Data();
                if (bean.status == 1) {
                    pageLimitDelegate.setData(movieBeans);
                } else {
                    pageLimitDelegate.setData(movieBeans);
                    pageLimitDelegate.loadComplete();
                    historyRecordAdapter.setEnableLoadMore(false);
                }
                break;
            case POST_DEL_MOVIE_CODE:
                if (bean.status == 1) {
                    UIUtils.shortM(bean.msg);
                    historyRecordAdapter.getSelectList().clear();
                    tvAll.setChecked(false);
                    tvAll.setText("全选");
                    pageLimitDelegate.refreshPage();
                } else {
                    UIUtils.shortM(bean.msg);
                }
                break;
        }
        super.nofityUpdate(requestCode, bean);
    }

    @OnClick({R.id.tvAll, R.id.tvDel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvAll://全选或取消全选
                selectAll();
                break;
            case R.id.tvDel:
                //删除
                delete();
                break;
        }
    }

    private void delete() {
        selectList = historyRecordAdapter.getSelectList();
        if (EmptyDeal.isEmpy(selectList)) {
            UIUtils.shortM("您还未选择要删除的影片");
        } else {
            MessageTipDialog.newInstance().setContentTxt("是否要删除这" + selectList.size() + "条影片?")
                    .setOnSubmitAction(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            BaseQuestStart.delMovie(HistoryFragment.this, Utils.listToString(selectList, new Utils.DefaultToString(",")), "1");
                        }
                    }).show(getFragmentManager(), "MessageTipDialog");
        }

    }

    /**
     * 全选或反选
     */
    private void selectAll() {
        boolean checked = tvAll.isChecked();
        selectList = historyRecordAdapter.getSelectList();
        if (checked) {
            tvAll.setText("取消全选");
            selectList.clear();
            selectList.addAll(historyRecordAdapter.getData());
        } else {
            tvAll.setText("全选");
            selectList.clear();
        }
        historyRecordAdapter.notifyDataSetChanged();
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void eventBusMethod(MessageEvent event) {
        String type = event.getType();
        historyRecordAdapter.setEditor(event.isEdit());
        historyRecordAdapter.notifyDataSetChanged();
        llBottom.setVisibility(event.isEdit() ? View.VISIBLE : View.GONE);
    }


}
