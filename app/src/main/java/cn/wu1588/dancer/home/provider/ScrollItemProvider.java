package cn.wu1588.dancer.home.provider;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.provider.BaseItemProvider;

import cn.wu1588.dancer.adp.HomeHorizontalAdapter;
import cn.wu1588.dancer.adp.HomeNewAdapter;
import cn.wu1588.dancer.home.mode.HomeBean;
import cn.wu1588.dancer.common.CommonIntent;

/**
 * 横向滚动
 */

public class ScrollItemProvider extends BaseItemProvider<HomeBean.DataListBean, BaseViewHolder> {

    private HomeHorizontalAdapter adapter;

    @Override
    public int viewType() {
        return HomeNewAdapter.TYPE_HORIZONTAL_SCROLL;
    }

    @Override
    public int layout() {
        return R.layout.app_item_home_scroll;
    }

    @Override
    public void convert(BaseViewHolder helper, HomeBean.DataListBean data, int position) {
        RecyclerView mRecyclerView = helper.getView(R.id.item_list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        adapter = new HomeHorizontalAdapter();
        adapter.setNewData(data.guessLikes);
        mRecyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int position) {
                HomeBean.DataListBean.GuessLikesBean item = adapter.getItem(position);
                CommonIntent.startMoviePlayerActivity(mContext, item.id);
            }
        });
    }
}
