package cn.wu1588.dancer.home.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.makeramen.roundedimageview.RoundedImageView;
import com.stx.xhb.xbanner.XBanner;
import com.sunrun.sunrunframwork.adapter.ViewHodler;
import com.sunrun.sunrunframwork.adapter.ViewHolderAdapter;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.utils.EmptyDeal;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.adp.HomeNewAdapter;
import cn.wu1588.dancer.home.mode.HomeBean;
import cn.wu1588.dancer.home.mode.HomeNoticeBean;
import cn.wu1588.dancer.home.mode.VersionBean;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.config.Const;
import cn.wu1588.dancer.common.dialog.AdDiglogFragment;
import cn.wu1588.dancer.common.dialog.VersionTipDiglog;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.widget.MarqueeTextView;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_CHANGE_DATA_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_HOME_DATA_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_HOME_NOTICE_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_VERSION_UPDATE_CODE;

/**
 * 首页
 */

public class HomeNewFragment extends LBaseFragment {
    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.recyclerViewHome)
    RecyclerView mRecyclerView;
    @BindView(R.id.llContainer)
    LinearLayout llContainer;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    @BindView(R.id.llSearch)
    LinearLayout llSearch;
    @BindView(R.id.ivHistory)
    ImageView ivHistory;
    Unbinder unbinder;
    private XBanner xbanner;
    private GridView gridView;
    private List<HomeBean> data;
    private HomeNewAdapter homeNewAdapter;
    private List<HomeBean.CategoryListBean> category_list;
    private List<HomeBean.DataListBean> data_list;
    private List<HomeBean.DataListBean> changeList;
    private MarqueeTextView tvMarquee;

    public static HomeNewFragment newInstance() {
        HomeNewFragment homeFragment = new HomeNewFragment();
        Bundle bundle = new Bundle();
        homeFragment.setArguments(bundle);
        return homeFragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_new_home;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        //设置状态栏颜色
        titleBar.setTitleBarBackgroundDrawable(CommonApp.getApplication().getStateBarDrawable());
        initData();
        BaseQuestStart.getHomeNotice(HomeNewFragment.this);
        BaseQuestStart.getVersionUpdate(HomeNewFragment.this);
    }


    private void initData() {
        BaseQuestStart.getHomeData(HomeNewFragment.this);
    }

    private void initView() {
        refreshLayout.setColorSchemeColors(getResources().getColor(R.color.main_button_color));
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initData();
                refreshLayout.setRefreshing(false);
            }
        });
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 6);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        homeNewAdapter = new HomeNewAdapter();
        homeNewAdapter.setSpanSizeLookup(new BaseQuickAdapter.SpanSizeLookup() {
            @Override
            public int getSpanSize(GridLayoutManager gridLayoutManager, int position) {
                int type = homeNewAdapter.getItem(position).stype;
                if (type == 1 || type == 3 || type == 5 || type == 6 || type == 7) {
                    return 6;
                } else if (type == 2) {
                    return 2;
                } else if (type == 4) {
                    return 3;
                }
                return 0;
            }
        });
        mRecyclerView.setAdapter(homeNewAdapter);
        homeNewAdapter.setHeaderView(getHeaderView(mRecyclerView));

        homeNewAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                HomeBean.DataListBean item = homeNewAdapter.getItem(position);
//                UIUtils.shortM(item.title);
                if (item.stype == 5) {
                    if ("newest_moive".equals(item.style) || "hot_moive".equals(item.style) || "is_free".equals(item.style)) {
                        CommonIntent.startAllMovieActivity(that, item.style, item.ids, "");
                    } else {
                        if (TextUtils.equals("true", item.isMore)) {
                            CommonIntent.startHomeMoreClassifiListActivity(that, item.id, Const.FROM_HOME_PAGE, 1);
                        }

                    }
                } else if (item.stype == 6) {//换一换
                    BaseQuestStart.getChangeData(HomeNewFragment.this);
                } else if (item.stype == 1) {
                    CommonIntent.startBrowser(that, item.url);
                } else if (item.stype == 7) {//限时免费
                    CommonIntent.startAllMovieActivity(that, item.style, item.ids, "");
                } else {
                    CommonIntent.startMoviePlayerActivity(that, item.id);

                }

            }
        });
    }

    private View getHeaderView(RecyclerView v) {
        View convertView = LayoutInflater.from(getContext()).inflate(R.layout.include_home_header, (ViewGroup) v.getParent(), false);
        xbanner = convertView.findViewById(R.id.xbanner);
        gridView = convertView.findViewById(R.id.gridViewChannel);
        tvMarquee = convertView.findViewById(R.id.tvMarquee);
        tvMarquee.setTextColor(getResources().getColor(R.color.white));
        tvMarquee.setTextSpeed(5.0f);
        tvMarquee.setScroll(true);
        return convertView;
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        super.nofityUpdate(requestCode, bean);
        switch (requestCode) {
            case GET_HOME_DATA_CODE:
                if (bean.status == 1) {
                    HomeBean data = bean.Data();
                    updateUI(data);
                }
                break;
            case GET_CHANGE_DATA_CODE:
                if (bean.status == 1) {
                    changeList = bean.Data();
                    restData(changeList);
                }
                break;
            case GET_HOME_NOTICE_CODE:
                if (bean == null) {
                    return;
                }
                if (bean.status == 1) {
                    HomeNoticeBean homeNoticeBean = bean.Data();
                    if (homeNoticeBean == null) {
                        return;
                    }
                    if (homeNoticeBean.isShowWin()) {
                        AdDiglogFragment.newInstance().setData(homeNoticeBean).show(getFragmentManager(), "AdDiglogFragment");
                    }
                }
                break;
            case GET_VERSION_UPDATE_CODE:
                if (bean.status == 1) {
                    VersionBean versionBean = bean.Data();
                    if (versionBean.getIs_box() == 1) {
                        VersionTipDiglog.newInstance().setVersionData(versionBean).show(getFragmentManager(), "VersionTipDiglog");
                    }
                }
                break;
        }
    }

    /**
     * 重新排列数据
     *
     * @param changeList
     */
    private void restData(List<HomeBean.DataListBean> changeList) {
        if (!EmptyDeal.isEmpy(data_list)) {
            for (int i = 0; i < data_list.size(); i++) {
                if (TextUtils.equals(data_list.get(i).ids, "3")) {
                    for (int j = 0; j < changeList.size(); j++) {
                        data_list.set(i + j + 1, changeList.get(j));
                    }
                    homeNewAdapter.notifyItemRangeChanged(i + 1, 5);
                    break;
                }
            }


        }
    }

    private void updateUI(HomeBean data) {
        if (!EmptyDeal.isEmpy(data)) {
            tvMarquee.setText(data.marquee + data.marquee);
            data_list = data.data_list;
            homeNewAdapter.setNewData(data_list);
            final List<HomeBean.BannerListBean> bannerBeanList = data.banner_list;

            //轮播图
            if (!EmptyDeal.isEmpy(bannerBeanList)) {
                //刷新数据之后，需要重新设置是否支持自动轮播
                xbanner.setAutoPlayAble(bannerBeanList.size() > 1);
                xbanner.setData(R.layout.home_banner_view, bannerBeanList, null);
                xbanner.loadImage(new XBanner.XBannerAdapter() {
                    @Override
                    public void loadBanner(XBanner banner, Object model, View view, int position) {
                        RoundedImageView roundedImageView = (RoundedImageView) view.findViewById(R.id.rIvBanner);
                        GlideMediaLoader.load(that, roundedImageView, bannerBeanList.get(position).image, R.drawable.home_banner_placle);
                    }
                });

                xbanner.setOnItemClickListener(new XBanner.OnItemClickListener() {
                    @Override
                    public void onItemClick(XBanner banner, Object model, View view, int position) {
                        HomeBean.BannerListBean bannerListBean = bannerBeanList.get(position);
                        if (bannerListBean.type == 0) {

                            CommonIntent.startBrowser(that, bannerBeanList.get(position).url);
                        } else {
                            CommonIntent.startHomeMoreClassifiListActivity(that, bannerListBean.url, Const.FROM_HOME_PAGE, 1);
                        }
                    }
                });
            }
            //分类
            category_list = data.category_list;
            if (!EmptyDeal.isEmpy(category_list)) {
                gridView.setAdapter(new ViewHolderAdapter<HomeBean.CategoryListBean>(that, category_list, R.layout.item_grid_channel) {
                    @Override
                    public void fillView(ViewHodler viewHodler, HomeBean.CategoryListBean s, int i) {
                        viewHodler.setText(R.id.itemTitle, s.title);
                        GlideMediaLoader.load(that, viewHodler.getView(R.id.itemIcon), s.logo, R.drawable.ic_place_ovel);
                    }
                });
                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        HomeBean.CategoryListBean categoryListBean = category_list.get(position);
                        CommonIntent.startAllMovieActivity(that, "newest_moive", "", categoryListBean.id);
                    }
                });
            }
        }
    }


    @OnClick({R.id.llSearch, R.id.ivHistory})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llSearch:
                CommonIntent.startSearchActivity(that);
                break;
            case R.id.ivHistory:
                CommonIntent.startHistoryRecordActivity(that);
                break;
        }
    }
}
