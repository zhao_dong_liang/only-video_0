package cn.wu1588.dancer.home.mode;

public class MovieActionStateBean {
    // "top": "1",     //0是不赞  1=赞  2=还未进行点赞操作
    //        "like": 0       //0是未收藏   1=已收藏
    public int top;
    public int like;

    public boolean isAlowZan() {
        return top == 2;
    }

    public boolean isLike() {
        return like == 0;
    }
}
