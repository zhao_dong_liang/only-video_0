package cn.wu1588.dancer.home.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.lzy.okgo.model.Progress;
import com.my.toolslib.OneClickUtils;
import com.my.toolslib.PopKeyboardUtil;
import com.my.toolslib.StringUtils;
import com.my.toolslib.http.utils.LzyResponse;
import com.my.toolslib.http.utils.OkHttpRequest;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;

import java.io.File;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.home.fragment.CommentFragment;
import cn.wu1588.dancer.home.mode.MovieBean;
import keyboardvisibilityevent.net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import keyboardvisibilityevent.net.yslibrary.android.keyboardvisibilityevent.Unregistrar;

public class EditCommentDialog implements View.OnClickListener, OkHttpRequest {
    //调试
    private Dialog dialog;
    private EditText edit_commect;
    private View root_view;
    private Activity activity;
    private TextView commectTv;
    private View sendView, view_bt;
    private boolean isCommented = false;
    private CommentFragment fragment;

    //  typeid=1评论视频  2=点一级评论进行评论（二级评论）  3=点一级评论下方的评论回复
    private String typeid = "1", userid = "0", movie_id, pid = "0", content;
    private MovieBean movieBean;

    public EditCommentDialog(Activity activity, TextView commectTv, View sendView, CommentFragment fragment) {
        this.activity = activity;
        this.commectTv = commectTv;
        this.sendView = sendView;
        this.fragment = fragment;
        dialog = new Dialog(activity, R.style.inputDialog);
        dialog.setContentView(R.layout.activity_edit_comment_dialog);
        view_bt = dialog.findViewById(R.id.view_bt);

        edit_commect = dialog.findViewById(R.id.edit_commect);
        edit_commect.setText(commectTv.getText().toString());
        root_view = dialog.findViewById(R.id.root_view);
        setListenerFotEditText(root_view);
        dialog.setOnShowListener(dialogInterface -> PopKeyboardUtil.waitPop(edit_commect));

        dialog.setOnDismissListener(dialogInterface -> {
            String s = edit_commect.getText().toString();
            if (!StringUtils.isNull(s) && !isCommented) {
                commectTv.setText(s);
            }
        });
        this.sendView.setOnClickListener(this);
        view_bt.setOnClickListener(this);


    }

    private String hintText;

    public void setHintText(String hintText, String id, int typeid, String userid) {
        this.hintText = hintText;
        this.pid = id;
        this.userid = userid;
        this.typeid = typeid + "";
    }

    public void showDialog() {
        if (!StringUtils.isNull(hintText)) {
            edit_commect.setHint(hintText);
        }
        if (dialog != null) {
            dialog.show();
            WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
            layoutParams.gravity = Gravity.BOTTOM;
            layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
            layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().getDecorView().setPadding(0, 0, 0, 0);
            dialog.getWindow().setAttributes(layoutParams);
        }

    }

    public void dismissDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
        if (unregistrar != null) {
            unregistrar.unregister();
        }
    }

    private Unregistrar unregistrar;

    private void setListenerFotEditText(View view) {
        unregistrar = KeyboardVisibilityEvent.registerEventListener(activity, (isOpen, heightDiff) -> {
            if (isOpen) {
                if (dialog.isShowing()) {
                    Log.d("EdiCommentDialog", String.format("show dialog bottom %s", dialog.getWindow().getDecorView().getBottom()));
                    //dialog.getWindow().getDecorView().setPadding(0, 0, 0, heightDiff);
                }
            } else {
                if (dialog.isShowing()) {
                    Log.d("EdiCommentDialog", String.format("dismiss dialog bottom %s", dialog.getWindow().getDecorView().getBottom()));
                    //dialog.getWindow().getDecorView().setPadding(0, 0, 0, 0);
                }
                dismissDialog();
            }
        });
    }


    public void close() {
        if (dialog != null) {
            dialog.cancel();
            dialog = null;
        }
    }

    @Override
    public void onClick(View view) {
        if (OneClickUtils.isFastDoubleClick(view.getId())) {
            return;
        }
        if (movieBean != null) {
            movie_id = movieBean.id + "";
        }
        String s = edit_commect.getText().toString();
        if (StringUtils.isNull(s)) {
            ToastUtils.longToast("评论不允许为空");
            return;
        } else {
            content = s;
        }
        switch (view.getId()) {
            case R.id.view_bt:
                BaseQuestStart.postComments(this, typeid, userid, movie_id, pid, content);
                break;

            case R.id.commect_view:
                BaseQuestStart.postComments(this, typeid, userid, movie_id, pid, content);
                break;
        }
    }


    @Override
    public void tokenInvalid() {

    }

    @Override
    public void httpLoadFail(String err) {
        ToastUtils.longToast(err);
    }

    @Override
    public void httpLoadFinal() {

    }

    @Override
    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
        if (requestCode == BaseQuestStart.POST_COMMENTS_CODE) {//提交评论
            //评论成功
            if (response.code == 1) {
                isCommented = true;

                if ("1".equals(typeid)) {
                    response.msg = "1";
                    fragment.refreshPage();
                } else {
                    response.msg = "2";
                    fragment.nofityUpdateUi(BaseQuestStart.POST_COMMENTS_CODE, response, null);
                }
                dismissDialog();
            } else {
                ToastUtils.longToast("评论失败");
            }
        }
    }

    @Override
    public void onProgress(Progress progress) {

    }

    @Override
    public void downLoadFinish(File file) {

    }

    public void setMovieBean(MovieBean movieBean) {
        this.movieBean = movieBean;
    }

    //    类型ID  1=电影评论（全部评论下方入口即为一级评论）  2=点一级评论进行评论（二级评论）  3=点一级评论下方的评论回复
    public void setTypeid(String typeid) {
        this.typeid = typeid;
    }
}
