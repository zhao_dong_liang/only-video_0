package cn.wu1588.dancer.home.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.mine.mode.BuyAlbumBean;
import cn.wu1588.dancer.my_video.bean.MyVideoBean;

public class AlumChildFragment extends LBaseFragment {

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipe_refresh;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;


    private ArrayList<BuyAlbumBean> mBeans;
    public static AlumChildFragment newInstance(ArrayList<BuyAlbumBean> beans) {
        Bundle args = new Bundle();
        args.putParcelableArrayList("beans",beans);
        AlumChildFragment fragment = new AlumChildFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public int getLayoutRes() {
        return R.layout.alum_child_fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBeans=getArguments().getParcelableArrayList("beans");
        recyclerView.setNestedScrollingEnabled(false);


        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(new BaseQuickAdapter<BuyAlbumBean, BaseViewHolder>(R.layout.item_ablum, mBeans) {
            @Override
            protected void convert(BaseViewHolder helper, BuyAlbumBean item) {
                helper.setText(R.id.tv_alum_title, item.title);
                helper.setText(R.id.tv_video_author_name, item.user_info.nickname);
                GlideMediaLoader.loadHead(getActivity(), helper.getView(R.id.iv_video_author_avatar), item.user_info.icon);
                helper.setText(R.id.tv_movies_count, String.format("共%s个视频", item.movies_count));
                helper.setText(R.id.tv_movies_price, String.format("¥ %s元", item.price));
                helper.setText(R.id.tv_movies_vip_discount, String.format("会员(%s折)", 8));
                helper.setText(R.id.tv_movies_discount_price, String.format("¥ %s元", 32.00));
                helper.setText(R.id.tv_movies_desc, String.format("专辑介绍:%s", item.desc));
                helper.getView(R.id.btn_movies_buy).setOnClickListener(v -> {

                });

                GridLayout authorWorks = helper.getView(R.id.tv_movies_author_works);
                authorWorks.removeAllViews();

                List<MyVideoBean> movies = item.movies;
                if (movies != null && !movies.isEmpty()) {
                    for (MyVideoBean movie : movies) {
                        View works = LayoutInflater.from(getActivity()).inflate(R.layout.layout_alum_author_works, authorWorks, false);
                        ImageView movieImage = works.findViewById(R.id.iv_movie_img);
                        TextView tv_movie_duration = works.findViewById(R.id.tv_movie_duration);
                        TextView tv_movie_title = works.findViewById(R.id.tv_movie_title);

                        GlideMediaLoader.load(getActivity(), movieImage, movie.getImage(), R.drawable.ic_place_threee_hodel);
                        tv_movie_duration.setText(movie.getPlay_time() + "");
                        tv_movie_title.setText(movie.getTitle());
                        authorWorks.addView(works);
                    }
                }
            }
        });

    }
}
