package cn.wu1588.dancer.home.mode;

import java.util.Objects;

/**
 * Created by wangchao on 2019-01-28.
 */
public class HistoryBean {
    public String keywords;
    public String seachType;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HistoryBean that = (HistoryBean) o;
        return Objects.equals(keywords, that.keywords);
    }

    @Override
    public int hashCode() {
        return Objects.hash(keywords);
    }
}
