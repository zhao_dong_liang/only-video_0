package cn.wu1588.dancer.home.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import cn.wu1588.dancer.R;

import com.my.toolslib.PopKeyboardUtil;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.sunrun.sunrunframwork.utils.EmptyDeal;

import org.greenrobot.eventbus.EventBus;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.base.LBaseDialogFragment;
import cn.wu1588.dancer.common.event.MessageEvent;
import cn.wu1588.dancer.common.quest.BaseQuestStart;

import static android.view.WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN;
import static android.view.WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.POST_COMMENTS_CODE;

public class PostCommentsDialogFragment extends LBaseDialogFragment {

    @BindView(R.id.ivClose)
    ImageView ivClose;
    @BindView(R.id.etCommentsContent)
    EditText etContent;
    @BindView(R.id.tvTextNum)
    TextView tvTextNum;
    private int height;
    private Dialog dialog;
    public String typeid="1";
    private String movieId;
    private String uid;
    private String userid;
    private String pid;
    private String hitName;

    public static PostCommentsDialogFragment newInstance() {
        PostCommentsDialogFragment postCommentsDialogFragment = new PostCommentsDialogFragment();
        Bundle bundle = new Bundle();
        postCommentsDialogFragment.setArguments(bundle);
        return postCommentsDialogFragment;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.dialog_fragment_post_comments;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }

    private void initViews() {
        etContent.setHint(EmptyDeal.isEmpy(hitName)?"请输入回复内容":"回复："+hitName);
//        InputMethodUtil.ShowKeyboard(etContent);
        PopKeyboardUtil.waitPop(etContent);
        etContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                tvTextNum.setText(etContent.getText().length() + "/50");
            }

            @Override
            public void afterTextChanged(Editable s) {
                tvTextNum.setText(etContent.getText().length() + "/50");
            }
        });
        etContent.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId== EditorInfo.IME_ACTION_SEND){
                    String content = etContent.getText().toString();
                    if (!EmptyDeal.isEmpy(content)){
                        BaseQuestStart.postComments(PostCommentsDialogFragment.this,typeid,userid,movieId,pid,content);
                    }else {
                        UIUtils.shortM("请输入评论内容");
                    }
                }
                return false;
            }
        });
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode){
            case POST_COMMENTS_CODE:
                if (bean.status==1){
                    EventBus.getDefault().post(new MessageEvent("refresh_data"));
                    dismissAllowingStateLoss();
                }else {
                    UIUtils.shortM(bean.msg);
                }
                break;
        }
        super.nofityUpdate(requestCode, bean);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = getDialog();
        Window dialogWindow = dialog.getWindow();
        applyCompat(dialogWindow);
        dialogWindow.setSoftInputMode(SOFT_INPUT_STATE_ALWAYS_VISIBLE|SOFT_INPUT_ADJUST_PAN);
        //设置对话框从底部进入
        dialogWindow.setWindowAnimations(R.style.bottomInWindowAnim);
        WindowManager.LayoutParams p = dialogWindow.getAttributes();
        p.width = ViewGroup.LayoutParams.MATCH_PARENT;
        p.height = ViewGroup.LayoutParams.MATCH_PARENT;//高度自己设定
        dialogWindow.setAttributes(p);
        dialogWindow.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        dialogWindow.setGravity(Gravity.BOTTOM);
        dialog.setCancelable(true);

        dialog.setCanceledOnTouchOutside(true);
        //修复状态栏变黑的问题
        int screenHeight = getScreenHeight(getActivity());
        int statusBarHeight = getStatusBarHeight(getContext());
        int dialogHeight = screenHeight - height;//screenHeight - statusBarHeight;
        dialogWindow.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, dialogHeight == 0 ? ViewGroup.LayoutParams.MATCH_PARENT : dialogHeight);
        return dialog;

    }
    public Dialog getDialog(){
        return new Dialog(that, R.style.NTitleDialog);
    }
    private void applyCompat(Window dialogWindow) {
        if (Build.VERSION.SDK_INT <= 19) {
            return;
        }
        dialogWindow.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialogWindow.setStatusBarColor(Color.TRANSPARENT);
        }

    }

    public PostCommentsDialogFragment setPlayerHeight(int height) {
        this.height = height;
        return this;
    }
    public PostCommentsDialogFragment setMovieId(String movieId) {
        this.movieId = movieId;
        return this;
    }
    public PostCommentsDialogFragment setTypeid(String typeid) {
        this.typeid=typeid;
        return this;
    }

    public PostCommentsDialogFragment setPid(String pid) {
        this.pid=pid;
        return this;
    }
    @OnClick(R.id.ivClose)
    public void onViewClicked() {
        FragmentActivity activity = getActivity();
        if (activity!=null&&!activity.isDestroyed()){
//            InputMethodUtil.HideKeyboard(activity);
            PopKeyboardUtil.hideSoftInputFromWindow(etContent);
            dismissAllowingStateLoss();
        }

    }
    @Override
    public void onDismiss(DialogInterface mdialog) {
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        params.softInputMode = WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN;
        super.onDismiss(dialog);
    }


    public PostCommentsDialogFragment setUserId(String me_id) {
        this.userid=me_id;
        return this;
    }

    public PostCommentsDialogFragment setHitText(String nickname) {
        this.hitName=nickname;
        return this;
    }
}
