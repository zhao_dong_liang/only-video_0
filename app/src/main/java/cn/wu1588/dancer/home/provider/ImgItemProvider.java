package cn.wu1588.dancer.home.provider;



import cn.wu1588.dancer.R;

import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.provider.BaseItemProvider;

import cn.wu1588.dancer.adp.HomeNewAdapter;
import cn.wu1588.dancer.home.mode.HomeBean;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;

/**
 * 广告
 */
public class ImgItemProvider extends BaseItemProvider<HomeBean.DataListBean, BaseViewHolder> {
    @Override
    public int viewType() {
        return HomeNewAdapter.TYPE_IMG;
    }

    @Override
    public int layout() {
        return R.layout.app_item_home_img;
    }

    @Override
    public void convert(BaseViewHolder helper, HomeBean.DataListBean data, int position) {
        GlideMediaLoader.load(mContext,helper.getView(R.id.ivImg),data.getImage(),R.drawable.ic_place_ad_img);
        helper.setText(R.id.tvTitle,data.title);
    }

}
