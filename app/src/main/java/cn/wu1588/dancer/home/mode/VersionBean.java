package cn.wu1588.dancer.home.mode;

public class VersionBean {
    // "version": "version",                                            //版本号
    //        "description": "本次更新内容为123456",            //描述
    //        "url": "2454556787",                   //更新地址
    //        "is_box": "1",                             //是否需要弹框  0=否  1=是
    //        "type": "1",                                //更新类型    1=普通更新  2=强制更新
    //        "system": "1"                             //标识  1=安卓  2=IOS
    public String version;
    public String description;
    public String url;
    public int is_box;
    public int type;
    public String system;

    public boolean isShowBox(){
        return is_box==1;
    }

    public int getIs_box() {
        return is_box;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setIs_box(int is_box) {
        this.is_box = is_box;
    }

    public boolean isForcedToUpdate(){
        return type==2;
    }
}
