package cn.wu1588.dancer.utils;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;

import com.sunrun.sunrunframwork.uiutils.ToastUtils;

public class SystemFunctionUtils {
    /**
     * 复制内容到剪贴板
     *
     * @param content
     * @param context
     */
    public static void copyContentToClipboard(String content, Context context) {
        if (content == null){
            ToastUtils.shortToast("复制失败");
            return;
        }
        //获取剪贴板管理器：
        ClipboardManager cm = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        // 创建普通字符型ClipData
        ClipData mClipData = ClipData.newPlainText("Label", content);
        // 将ClipData内容放到系统剪贴板里。
        cm.setPrimaryClip(mClipData);
        ToastUtils.shortToast("已复制" + content);
    }
}
