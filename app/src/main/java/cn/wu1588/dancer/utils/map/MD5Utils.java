package cn.wu1588.dancer.utils.map;

import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;

/**
 * Created by zhou on 2021/2/9 16:08.
 */
public class MD5Utils {

    public static String md5() {




        //获取当前日期
        Calendar calendar = Calendar.getInstance();
        String year = String.valueOf(calendar.get(Calendar.YEAR));
        String month = calendar.get(Calendar.MONTH) + 1 < 10 ? "0" + (calendar.get(Calendar.MONTH) + 1) : String.valueOf(calendar.get(Calendar.MONTH) + 1);
        String day = calendar.get(Calendar.DAY_OF_MONTH) < 10 ? "0" + calendar.get(Calendar.DAY_OF_MONTH) : String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
        Long data = Long.valueOf(year + month + day);
        Double dataDouble = Double.  valueOf(year + month + day);
        String ssh = "ds76%882kJG80(76Hf4)" + (data * 18 / 5);
        byte[] hash;
        try {
            hash = MessageDigest.getInstance("MD5").digest(ssh.getBytes("UTF-8"));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("NoSuchAlgorithmException", e);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("UnsupportedEncodingException", e);
        }
        StringBuilder hex = new StringBuilder(hash.length * 2);
        for (byte b : hash) {
            if ((b & 0xFF) < 0x10) {
                hex.append("0");
            }
            hex.append(Integer.toHexString(b & 0xFF));
        }
        Log.d("zdl","=====日期4==="+hex.toString());
        return hex.toString();
    }

} 
