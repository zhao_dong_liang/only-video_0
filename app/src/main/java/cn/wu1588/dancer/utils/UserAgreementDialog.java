package cn.wu1588.dancer.utils;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import cn.wu1588.dancer.R;

//协议对话框
public class UserAgreementDialog extends Activity {
    TextView text_refuse, text_agree;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_agreement_dialog);
        text_refuse = findViewById(R.id.text_agree);
        text_agree = findViewById(R.id.text_agree);
        text_agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.exit(0);
            }
        });
        text_refuse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = getSharedPreferences("shanping", MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean("status", true);
                editor.commit();
                finish();
            }
        });
    }
}