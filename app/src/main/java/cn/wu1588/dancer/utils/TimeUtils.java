package cn.wu1588.dancer.utils;

public class TimeUtils {
    public static String getTimeFromInt(String timeStr) {
        long time = 0;
        if (timeStr == null){
            return "0:00";
        }
        time = Long.valueOf(timeStr);
        if (time <= 0) {
            return "0:00";
        }
        Long secondnd = time / 60;
        Long million = time % 60;
        String f = String.valueOf(secondnd);
        String m = million >= 10 ? String.valueOf(million) : "0"
                + String.valueOf(million);
        return f + ":" + m;
    }

    public static String timeConversion(String timeStr) {

        long time = 0;
        if (timeStr == null){
            return "0:00";
        }
        time = Long.valueOf(timeStr);
        if (time <= 0) {
            return "0:00";
        }
        long hour = 0;
        long minutes = 0;
        long sencond = 0;
        long temp = time % 3600;
        if (time > 3600) {
            hour = time / 3600;
            if (temp != 0) {
                if (temp > 60) {
                    minutes = temp / 60;
                    if (temp % 60 != 0) {
                        sencond = temp % 60;
                    }
                } else {
                    sencond = temp;
                }
            }
        } else {
            minutes = time / 60;
            if (time % 60 != 0) {
                sencond = time % 60;
            }
        }

        if ((hour<10?("0"+hour):hour).equals("00")){
            return (minutes<10?("0"+minutes):minutes) + ":" + (sencond<10?("0"+sencond):sencond);
        }else{
            return (hour<10?("0"+hour):hour) + ":" + (minutes<10?("0"+minutes):minutes) + ":" + (sencond<10?("0"+sencond):sencond);
        }
    }
}
