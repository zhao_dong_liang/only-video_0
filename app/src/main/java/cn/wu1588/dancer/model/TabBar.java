package cn.wu1588.dancer.model;

/**
 * Created by caidong on 2019/8/26.
 * email:mircaidong@163.com
 * describe: 描述
 */
public class TabBar {
    public String name;
    public int location;
    public String selected_img;
    public String unselected_img;

    public String getSelected_img() {
        return selected_img;
    }

    public void setSelected_img(String selected_img) {
        this.selected_img = selected_img;
    }

    public String getUnselected_img() {
        return unselected_img;
    }

    public void setUnselected_img(String unselected_img) {
        this.unselected_img = unselected_img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLocation() {
        return location;
    }

    public void setLocation(int location) {
        this.location = location;
    }
}
