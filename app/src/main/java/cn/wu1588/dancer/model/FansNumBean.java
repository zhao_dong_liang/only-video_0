package cn.wu1588.dancer.model;

public class FansNumBean {
    String focus;
    String fans;
    String gold_fans;

    public String getFocus() {
        return focus;
    }

    public void setFocus(String focus) {
        this.focus = focus;
    }

    public String getFans() {
        return fans;
    }

    public void setFans(String fans) {
        this.fans = fans;
    }

    public String getGold_fans() {
        return gold_fans;
    }

    public void setGold_fans(String gold_fans) {
        this.gold_fans = gold_fans;
    }

    @Override
    public String toString() {
        return "FansNumBean{" +
                "focus='" + focus + '\'' +
                ", fans='" + fans + '\'' +
                ", gold_fans='" + gold_fans + '\'' +
                '}';
    }
}