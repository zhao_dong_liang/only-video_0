
package cn.wu1588.dancer.model;

public class AdHistory {

    public String title;
    public String id;
    public String ad_id;
    public String mode_type;
    public String mode_id;
    public String show;
    public String click;
    public String download;
    public String cost;
    public String user_id;
    public String update_time;
    public String info;
}