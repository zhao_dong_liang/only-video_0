
package cn.wu1588.dancer.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.chad.library.adapter.base.entity.MultiItemEntity;

public class Ad implements Parcelable, MultiItemEntity {


        public String id;
        public String title;
        public String short_title;
        public String video_url;
        public String img_url;
        public String package_name;
        public String android_package_url;
        public String ios_package_url;
        public String qq;
        public String phone;
        public int status;
        public String sort;
        public String mode_one;
        public String mode_two;
        public String mode_three;
        public String add_time;
        public String html;
        public String uid;
        public String outer_url;
        public String introduction_url;
        public String qualification_url;

        public Ad() {
        }

        protected Ad(Parcel in) {
                id = in.readString();
                title = in.readString();
                short_title = in.readString();
                video_url = in.readString();
                img_url = in.readString();
                package_name = in.readString();
                android_package_url = in.readString();
                ios_package_url = in.readString();
                qq = in.readString();
                phone = in.readString();
                status = in.readInt();
                sort = in.readString();
                mode_one = in.readString();
                mode_two = in.readString();
                mode_three = in.readString();
                add_time = in.readString();
                html = in.readString();
                uid = in.readString();
                outer_url = in.readString();
                introduction_url = in.readString();
                qualification_url = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(id);
                dest.writeString(title);
                dest.writeString(short_title);
                dest.writeString(video_url);
                dest.writeString(img_url);
                dest.writeString(package_name);
                dest.writeString(android_package_url);
                dest.writeString(ios_package_url);
                dest.writeString(qq);
                dest.writeString(phone);
                dest.writeInt(status);
                dest.writeString(sort);
                dest.writeString(mode_one);
                dest.writeString(mode_two);
                dest.writeString(mode_three);
                dest.writeString(add_time);
                dest.writeString(html);
                dest.writeString(uid);
                dest.writeString(outer_url);
                dest.writeString(introduction_url);
                dest.writeString(qualification_url);
        }

        @Override
        public int describeContents() {
                return 0;
        }

        public static final Creator<Ad> CREATOR = new Creator<Ad>() {
                @Override
                public Ad createFromParcel(Parcel in) {
                        return new Ad(in);
                }

                @Override
                public Ad[] newArray(int size) {
                        return new Ad[size];
                }
        };

        @Override
        public int getItemType() {
                return 2;
        }
}