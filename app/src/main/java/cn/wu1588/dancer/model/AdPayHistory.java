package cn.wu1588.dancer.model;

public class AdPayHistory {
    public String id;

    public String pay_type;

    public String order_no;

    public String deal_no;

    public String from_member_id;

    public String to_member_id;

    public String vip_id;

    public String money;

    public String status;

    public String remark;

    public String is_hid;

    public String is_del;

    public String add_time;

    public String update_time;

    public String opt_info;

    public String pay_time;

    public String meal_type;
}