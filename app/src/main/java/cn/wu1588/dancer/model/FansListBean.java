package cn.wu1588.dancer.model;

public class FansListBean {
    String id;
    String icon;
    String nickname;
    String add_time;
    String is_new;
    String is_attention;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAdd_time() {
        return add_time;
    }

    public void setAdd_time(String add_time) {
        this.add_time = add_time;
    }

    public String getIs_new() {
        return is_new;
    }

    public void setIs_new(String is_new) {
        this.is_new = is_new;
    }

    public String getIs_attention() {
        return is_attention;
    }

    public void setIs_attention(String is_attention) {
        this.is_attention = is_attention;
    }

    @Override
    public String toString() {
        return "FansListBean{" +
                "id='" + id + '\'' +
                ", icon='" + icon + '\'' +
                ", nickname='" + nickname + '\'' +
                ", add_time='" + add_time + '\'' +
                ", is_new='" + is_new + '\'' +
                ", is_attention='" + is_attention + '\'' +
                '}';
    }
}
