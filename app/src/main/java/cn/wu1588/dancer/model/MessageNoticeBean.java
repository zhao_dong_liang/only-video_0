package cn.wu1588.dancer.model;

public class MessageNoticeBean {
    String id;
    String member_id;
    String type;
    String content;
    String add_time;
    String is_del;
    String is_read;

    @Override
    public String toString() {
        return "MessageNoticeBean{" +
                "id='" + id + '\'' +
                ", member_id='" + member_id + '\'' +
                ", type='" + type + '\'' +
                ", content='" + content + '\'' +
                ", add_time='" + add_time + '\'' +
                ", is_del='" + is_del + '\'' +
                ", is_read='" + is_read + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAdd_time() {
        return add_time;
    }

    public void setAdd_time(String add_time) {
        this.add_time = add_time;
    }

    public String getIs_del() {
        return is_del;
    }

    public void setIs_del(String is_del) {
        this.is_del = is_del;
    }

    public String getIs_read() {
        return is_read;
    }

    public void setIs_read(String is_read) {
        this.is_read = is_read;
    }
}