package cn.wu1588.dancer.advert.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cnsunrun.commonui.widget.button.RoundButton;
import com.google.gson.Gson;
import com.my.toolslib.NumUtils;
import com.my.toolslib.http.utils.NetRequestListenerProxy;
import cn.wu1588.dancer.logic.AliPayLogic;
import cn.wu1588.dancer.ui.WePaymentActivity;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;
import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.sunrun.sunrunframwork.utils.EmptyDeal;
import com.tencent.mmkv.MMKV;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.dialog.SelectPayDialog;
import cn.wu1588.dancer.common.model.LoginInfo;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.common.util.RecycleHelper;
import cn.wu1588.dancer.common.util.Tool;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;
import cn.wu1588.dancer.adp.AlumPayAdapter;
import cn.wu1588.dancer.mine.activity.WebRechargeActivity;
import cn.wu1588.dancer.mine.mode.PayChannelBean;
import cn.wu1588.dancer.mine.mode.PayInfo;
import cn.wu1588.dancer.mine.mode.PayMentBean;
import cn.wu1588.dancer.mine.mode.RechargeBean;
import cn.wu1588.dancer.mine.mode.WxPayBean;

import static butterknife.OnTextChanged.Callback.AFTER_TEXT_CHANGED;
import static cn.wu1588.dancer.ui.WePaymentActivity.PAY_ORDER_CONTENT;
import static cn.wu1588.dancer.ui.WePaymentActivity.PAY_SUCCESS;
import static cn.wu1588.dancer.ui.WePaymentActivity.PAY_TYPE;
import static cn.wu1588.dancer.ui.WePaymentActivity.PAY_TYPE_WXPAY;
import static cn.wu1588.dancer.ui.WePaymentActivity.RESULT_MSG;
import static cn.wu1588.dancer.ui.WePaymentActivity.RESULT_STATUS;
import static cn.wu1588.dancer.common.model.SystemParams.AD_MINIMUM_RECHARGE_AMOUNT;

/**
 * 广告充值
 */
public class AdPayActivity extends LBaseActivity implements Handler.Callback {


    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.et_recharge_money)
    EditText et_recharge_money;
    @BindView(R.id.tv_min_recharge_money)
    TextView tv_min_recharge_money;
    @BindView(R.id.recyclerViewPayment)
    RecyclerView recyclerViewPayment;
    @BindView(R.id.tv_alum_real_price)
    TextView tvAlumRealPrice;
    @BindView(R.id.btn_pay)
    RoundButton btnPay;

    private AlumPayAdapter payMentAdapter;
    private int checkPosition;
    private PayMentBean item;
    private boolean thirdPay;
    private AliPayLogic aliPayLogic;

    public static void start(Context context) {
        Intent intent = new Intent(context, AdPayActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ad_pay);
        ButterKnife.bind(this);


        Handler handler = new Handler(this);
        aliPayLogic = new AliPayLogic(that, handler);

        initViews();
        initData();
    }

    @OnTextChanged(value = R.id.et_recharge_money, callback = AFTER_TEXT_CHANGED)
    void afterTextChanged(Editable s) {
        try {
            if (s.length() > 0) {
                ((View)tvAlumRealPrice.getParent()).setVisibility(View.VISIBLE);
                tvAlumRealPrice.setText(String.format("¥%s", s.toString()));
            } else {
                ((View)tvAlumRealPrice.getParent()).setVisibility(View.GONE);
                tvAlumRealPrice.getEditableText().clear();
            }


        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @OnClick(R.id.btn_pay)
    void pay(View view) {
        item = payMentAdapter.getItem(checkPosition);
        switch (item.id) {
            case "2":
                if (thirdPay) {
                    if (Tool.checkAliPayInstalled(that)) {
                        BaseQuestStart.getPayChanel(new NetRequestListenerProxy(that) {
                            @Override
                            public void nofityUpdate(int i, BaseBean baseBean) {
                                handleGetPayChanelResult(baseBean);
                            }
                        }, "1");
                    } else {
                        UIUtils.shortM("请安装支付宝客户端");
                    }
                } else {
                    int money = NumUtils.parseInt(et_recharge_money.getText().toString().replace("¥", ""), 0);
                    MMKV mmkv = MMKV.defaultMMKV();
                    String minMoney = mmkv.decodeString(AD_MINIMUM_RECHARGE_AMOUNT, "1000");
                    int minMoneyInt = Integer.parseInt(minMoney);

                    if (money < minMoneyInt) {
                        UIUtils.shortM(String.format("最低起充金额：%s元", minMoney));
                        return;
                    }
                    RechargeBean.VipMealBean vipMealBean = new RechargeBean.VipMealBean();
                    vipMealBean.id = "0";
                    vipMealBean.price = money + "";
                    BaseQuestStart.postCreateOrder(new NetRequestListenerProxy(this) {
                        @Override
                        public void nofityUpdate(int i, BaseBean baseBean) {
                            if (baseBean.status == 1) {
                                aliPayLogic.startAliPay(baseBean.data.toString());
                            } else {
                                UIUtils.shortM(baseBean.msg);
                            }
                        }
                    }, vipMealBean,3);
                }
                break;
            case "1":
                if (Tool.isWeixinAvilible(that)) {
                    if (thirdPay) {
                        BaseQuestStart.getPayChanel(that, "2");
                    } else {
                        int money = NumUtils.parseInt(et_recharge_money.getText().toString().replace("¥", ""), 0);
                        MMKV mmkv = MMKV.defaultMMKV();
                        String minMoney = mmkv.decodeString(AD_MINIMUM_RECHARGE_AMOUNT, "1000");
                        int minMoneyInt = Integer.parseInt(minMoney);

                        if (money < minMoneyInt) {
                            UIUtils.shortM(String.format("最低起充金额：%s元", minMoney));
                            return;
                        }
                        RechargeBean.VipMealBean vipMealBean = new RechargeBean.VipMealBean();
                        vipMealBean.id = "0";
                        vipMealBean.price = money + "";
                        BaseQuestStart.createWeixinrder(new NetRequestListenerProxy(this) {
                            @Override
                            public void nofityUpdate(int i, BaseBean baseBean) {
                                if (baseBean.status == 1) {
                                    Intent intent = new Intent(AdPayActivity.this, WePaymentActivity.class);
                                    intent.putExtra(PAY_TYPE, PAY_TYPE_WXPAY);
                                    WxPayBean wxPayBean = baseBean.Data();
                                    PayInfo payInfo = wxPayBean.app_create_data;
                                    intent.putExtra(PAY_ORDER_CONTENT, new Gson().toJson(payInfo, PayInfo.class));
                                    startActivityForResult(intent, 0x09);
                                } else {
                                    UIUtils.shortM(baseBean.msg);
                                }
                            }
                        }, vipMealBean, 3);
                    }
                } else {
                    UIUtils.shortM("请安装微信客户端");
                }
                break;
        }
    }

    private void handleGetPayChanelResult(BaseBean baseBean) {
        if (baseBean.status == 1) {
            List<PayChannelBean> payChannelBeans = baseBean.Data();
            new SelectPayDialog(that, payChannelBeans, content -> BaseQuestStart.getPayUrl(new NetRequestListenerProxy(that) {
                @Override
                public void nofityUpdate(int i, BaseBean baseBean) {
                    if (baseBean.status == 1) {
                        WebRechargeActivity.startThis(that, baseBean.Data().toString());
                    } else {
                        UIUtils.shortM(baseBean.msg);
                    }
                }
            }, "0", content));
        } else {
            UIUtils.shortM(baseBean.msg);
        }
    }

    private void initViews() {
        RecycleHelper.setLinearLayoutManager(recyclerViewPayment, LinearLayoutManager.VERTICAL);
        payMentAdapter = new AlumPayAdapter();
        payMentAdapter.setOnItemClickListener((adapter, view, position) -> {
            checkPosition = position;
            List<PayMentBean> data = payMentAdapter.getData();
            for (int i = 0; i < data.size(); i++) {
                data.get(i).isCheckd = false;
            }
            data.get(position).isCheckd = true;
            payMentAdapter.notifyDataSetChanged();
        });
        recyclerViewPayment.setAdapter(payMentAdapter);
        SpannableString sp = new SpannableString("请输入金额");
        int size = UIUtils.sp2px(getResources().getDisplayMetrics(), 14);
        sp.setSpan(new AbsoluteSizeSpan(size), 0, sp.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        et_recharge_money.setHint(sp);
    }

    private void initData() {
        List<PayMentBean> payMentBeans = new ArrayList<>();
        payMentBeans.add(new PayMentBean("2", "支付宝", true));
        payMentBeans.add(new PayMentBean("1", "微信", false));
        payMentAdapter.setNewData(payMentBeans);

        MMKV mmkv = MMKV.defaultMMKV();
        String minMoney = mmkv.decodeString(AD_MINIMUM_RECHARGE_AMOUNT, "1000");
        String tips = String.format("最低起充金额：%s元", minMoney);
        SpannableString sb = new SpannableString(tips);
        sb.setSpan(new ForegroundColorSpan(Color.parseColor("#D4AA5C")), 7, tips.length() - 1, Spannable. SPAN_EXCLUSIVE_INCLUSIVE);
        tv_min_recharge_money.setText(sb);

        BaseQuestStart.getPayMethod(new NetRequestListenerProxy(this){
            @Override
            public void nofityUpdate(int i, BaseBean bean) {
                if (bean.status == 1) {
                    List<PayMentBean> payMentBeans = bean.Data();
                    if (!EmptyDeal.isEmpy(payMentBeans)) {
                        payMentBeans.get(0).isCheckd = true;
                    }
                    payMentAdapter.setNewData(payMentBeans);
                }
            }
        });
        BaseQuestStart.getPayWay(new NetRequestListenerProxy(this) {
            @Override
            public void nofityUpdate(int i, BaseBean baseBean) {
                if (baseBean.status == 1) {
                    if ("第三方支付".equals(baseBean.Data())) {
                        thirdPay = true;
                    } else {
                        thirdPay = false;
                    }
                } else {
                    UIUtils.shortM(baseBean.msg);
                }
            }
        });
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case 1901:  //支付宝
                if (msg.obj.equals("9000")) {
                    BaseQuestStart.getUserInfo(this);
                } else {
                    ToastUtils.shortToast("支付失败");
                }
                break;
        }
        return false;
    }

    //接收支付结果回调和结果信息
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == 0x09) {
            int intExtra = data.getIntExtra(RESULT_STATUS, 0);
            Toast.makeText(this, data.getStringExtra(RESULT_MSG), Toast.LENGTH_SHORT).show();
            if (intExtra == PAY_SUCCESS) {
                Intent broadcast = new Intent("android.intent.action.to.mine");
                sendBroadcast(broadcast);
                Log.d("WePayDemoActivity", "成功");
                BaseQuestStart.getUserInfo(new NetRequestListenerProxy(this) {
                    @Override
                    public void nofityUpdate(int i, BaseBean baseBean) {
                        if (baseBean.status == 1) {
                            LoginInfo loginInfo = baseBean.Data();
                            if (loginInfo != null) {
                                loginInfo.setUid(loginInfo.id);
                                Config.putLoginInfo(loginInfo);
                                CommonIntent.startAdPayHistories(that);
                            }
                        }
                    }
                });
            } else {
                Log.d("WePayDemoActivity", "失败");
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
