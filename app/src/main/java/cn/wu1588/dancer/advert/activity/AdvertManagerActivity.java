package cn.wu1588.dancer.advert.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bigkoo.pickerview.OptionsPickerView;
import com.bigkoo.pickerview.TimePickerView;
import com.my.toolslib.DateUtil;
import com.my.toolslib.http.utils.NetRequestListenerProxy;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.UIUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.wu1588.dancer.model.Ad;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;

public class AdvertManagerActivity extends LBaseActivity  {

    @BindView(R.id.title_bar)
    TitleBar titleBar;
    @BindView(R.id.tv_ad_title)
    TextView tv_ad_title;

    public static void start(Context context, Ad ad) {
        Intent intent = new Intent(context, AdvertManagerActivity.class);
        intent.putExtra("ad", ad);
        context.startActivity(intent);
    }

    private static final String DATE_AND_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    private static final String DATE_PATTERN = "yyyy-MM-dd";
    private static final String TIME_PATTERN = " HH:mm:ss";

    private static final List<RadioItem> ITEMS = Arrays.asList(
            new RadioItem(Arrays.asList(
                    new RadioTextItem("方式一(单次投放)", "", "", false, false),
                    new RadioTextItem("开始时间", DateUtil.getCurDate(DATE_AND_TIME_PATTERN), "",true, true),
                    new RadioTextItem("结束时间", DateUtil.getCurDate(DATE_AND_TIME_PATTERN), "",true, true),
                    new RadioTextItem("花费限额", "", "请输入金额",false, false)
            ), false,1),
            new RadioItem(Arrays.asList(
                    new RadioTextItem("方式二(多日同一时段反复投放)", "", "", false, false),
                    new RadioTextItem("开始日期", DateUtil.getCurDate(DATE_PATTERN), "", true, false),
                    new RadioTextItem("结束日期", DateUtil.getCurDate(DATE_PATTERN), "", true, false),
                    new RadioTextItem("每日开始时间",  DateUtil.getCurDate(TIME_PATTERN), "", false, true),
                    new RadioTextItem("每日结束时间",  DateUtil.getCurDate(TIME_PATTERN), "", false, true),
                    new RadioTextItem("每日花费限额",  "", "请输入金额", false, false)

            ), false,2),
            new RadioItem(Arrays.asList(
                    new RadioTextItem("方式三(包月24小时投放)", "", "", false, false),
                    new RadioTextItem("包月", "1", "", false, false),
                    new RadioTextItem("开始时间", DateUtil.getCurDate(DATE_AND_TIME_PATTERN), "", true, true),
                    new RadioTextItem("所需金额(元)", "1500", "请输入金额", false, false)
            ), false, 3)
    );

    @BindView(R.id.radioGroup)
    LinearLayout mRadioGroup;
    private TextView mCurrentSelected;
    private RadioItem mCurrentSelectedRadioItem;
    private EditText type3MoneyView;

    private Ad mAd;

    private NetRequestListenerProxy netRequestListenerProxy = new NetRequestListenerProxy(this) {
        @Override
        public void nofityUpdate(int i, BaseBean baseBean) {
            if (baseBean.status == 1) {
                finish();
            } else {
                UIUtils.shortM(baseBean.msg);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ad_manager);
        ButterKnife.bind(this);
        titleBar.setTitleBarBackgroundDrawable(CommonApp.getApplication().getStateBarDrawable());
        mAd = getIntent().getParcelableExtra("ad");
        tv_ad_title.setText(mAd.title);
        LayoutInflater inflater = LayoutInflater.from(this);
        for (RadioItem it : ITEMS) {
            LinearLayout radioButtonLayout = (LinearLayout) inflater.inflate(R.layout.layout_ad_manager_radio_button, mRadioGroup, false);

            List<RadioTextItem> items = it.items;
            for (int i = 0, size = items.size(); i < size; i++) {
                if (i == 0) {
                    RadioTextItem radioTextItem = items.get(0);
                    TextView radioButton = (TextView) inflater.inflate(R.layout.layout_ad_manager_date_radio, radioButtonLayout, false);
                    radioButton.setText(radioTextItem.startTitle);
                    radioButtonLayout.addView(radioButton);
                    radioButton.setOnClickListener(v -> {
                        if (mCurrentSelected != null) {
                            mCurrentSelected.setSelected(false);
                        }
                        if (mCurrentSelectedRadioItem != null) {
                            mCurrentSelectedRadioItem.checked = false;
                        }
                        v.setSelected(true);
                        it.checked = true;

                        mCurrentSelected = (TextView) v;
                        mCurrentSelectedRadioItem = it;
                    });
                } else if (i == size - 1) {
                    RadioTextItem radioTextItem = items.get(size - 1);
                    RelativeLayout timesItem = (RelativeLayout) inflater.inflate(R.layout.layout_ad_manager_price_setter, radioButtonLayout, false);
                    TextView startView = timesItem.findViewById(R.id.tv_start_title);
                    EditText endView = timesItem.findViewById(R.id.et_price);
                    startView.setText(radioTextItem.startTitle);
                    endView.setHint(radioTextItem.editHint);
                    radioButtonLayout.addView(timesItem);
                    endView.addTextChangedListener(new TextWaterAdapter() {
                        @Override
                        public void afterTextChanged(Editable s) {
                            radioTextItem.endTitle = s.toString();
                        }
                    });
                    if (radioTextItem.startTitle.equals("所需金额(元)")) {
                        endView.setEnabled(false);
                        endView.setText(radioTextItem.endTitle);
                        type3MoneyView = endView;
                    }
                } else {
                    RadioTextItem radioTextItem = items.get(i);
                    RelativeLayout timesItem = (RelativeLayout) inflater.inflate(R.layout.layout_ad_manager_date, radioButtonLayout, false);
                    TextView startView = timesItem.findViewById(R.id.tv_start_date);
                    TextView endView = timesItem.findViewById(R.id.tv_times);
                    startView.setText(radioTextItem.startTitle);
                    String title = RadioTextItem.MONTH_MAPPING.get(radioTextItem.endTitle);
                    endView.setText(TextUtils.isEmpty(title) ? radioTextItem.endTitle : title);
                    radioButtonLayout.addView(timesItem);
                    timesItem.setOnClickListener(v -> {
                        if (radioTextItem.isDate && radioTextItem.isTime) {
                            showDateAndTime(endView, radioTextItem);
                            return;
                        }
                        if (radioTextItem.isDate) {
                            showDate(endView, radioTextItem);
                            return;
                        }
                        if (radioTextItem.isTime) {
                            showTime(endView, radioTextItem);
                            return;
                        }
                        if (radioTextItem.startTitle.equals("包月")) {
                            showMonths(endView, radioTextItem);
                        }
                    });
                }
            }

            mRadioGroup.addView(radioButtonLayout);
        }
    }

    @OnClick(R.id.btn_submit)
    void submit(View view) {
        for (RadioItem item : ITEMS) {
            if (item.checked) {
                switch (item.type) {
                    case 1:
                        RadioTextItem begenDateAndTime = item.items.get(1);
                        RadioTextItem endDateAndTime = item.items.get(2);
                        RadioTextItem type1Money = item.items.get(3);
                        if (TextUtils.isEmpty(type1Money.endTitle)) {
                            UIUtils.shortM("请设置金额");
                            return;
                        }
                        BaseQuestStart.setAdPutType1(netRequestListenerProxy, begenDateAndTime.endTitle, endDateAndTime.endTitle, type1Money.endTitle, mAd
                        .id);
                        break;
                    case 2:
                        RadioTextItem begenDate = item.items.get(1);
                        RadioTextItem endDate = item.items.get(2);
                        RadioTextItem begenTime = item.items.get(3);
                        RadioTextItem endtime = item.items.get(4);
                        RadioTextItem type2Money = item.items.get(5);
                        if (TextUtils.isEmpty(type2Money.endTitle)) {
                            UIUtils.shortM("请设置金额");
                            return;
                        }
                        BaseQuestStart.setAdPutType2(netRequestListenerProxy, begenDate.endTitle, endDate.endTitle, begenTime.endTitle, endtime.endTitle, type2Money.endTitle, mAd
                                .id);
                        break;
                    case 3:
                        RadioTextItem month = item.items.get(1);
                        RadioTextItem startDateAndTime = item.items.get(2);
                        BaseQuestStart.setAdPutType3(netRequestListenerProxy, month.endTitle, startDateAndTime.endTitle, mAd
                                .id);
                        break;
                }
            }
        }
    }

    private void showDate(TextView dateView, RadioTextItem dateItem) {
        boolean[] options = new boolean[]{true, true, true, false, false, false};
        new TimePickerView.Builder(AdvertManagerActivity.this, (date, v) -> {//选中事件回调
            dateItem.endTitle = DateUtil.getDateToString(date.getTime(), DATE_PATTERN);
            dateView.setText(dateItem.endTitle);
        }).setType(options).build().show();


    }

    private void showTime(TextView dateView, RadioTextItem dateItem) {
        boolean[] options = new boolean[]{false, false, false, true, true, true};
        new TimePickerView.Builder(AdvertManagerActivity.this, (date, v) -> {//选中事件回调
            dateItem.endTitle = DateUtil.getDateToString(date.getTime(), TIME_PATTERN);
            dateView.setText(dateItem.endTitle);
        }).setType(options).build().show();
    }

    private void showDateAndTime(TextView dateView, RadioTextItem dateItem) {
        new TimePickerView.Builder(AdvertManagerActivity.this, (date, v) -> {//选中事件回调
            dateItem.endTitle = DateUtil.getDateToString(date.getTime(), DATE_AND_TIME_PATTERN);
            dateView.setText(dateItem.endTitle);
        }).build().show();
    }

    private void showMonths(TextView dateView, RadioTextItem dateItem) {
        List<String> options = Arrays.asList("1","2","3","4","5","6","7","8","9","10","11","12");
        OptionsPickerView pvCustomOptions = new OptionsPickerView.Builder(this, (options1, option2, options3, v) -> {
            String s = options.get(options1);
            dateView.setText(RadioTextItem.MONTH_MAPPING.get(s));
            dateItem.endTitle = s;
            type3MoneyView.setText((Integer.parseInt(s) * 1500) + "");
        }).isDialog(true).setOutSideCancelable(false)
                .build();

        pvCustomOptions.setPicker(options);//添加数据
        pvCustomOptions.show();
    }

    private static class TextWaterAdapter implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

    private static class RadioItem {

        List<RadioTextItem> items;
        boolean checked;
        int type;

        public RadioItem(List<RadioTextItem> items, boolean checked, int type) {
            this.items = items;
            this.checked = checked;
            this.type = type;
        }
    }

    private static class RadioTextItem {
        String startTitle;
        String endTitle;
        String editHint;
        boolean isDate;
        boolean isTime;

        static final Map<String, String> MONTH_MAPPING = new HashMap<String, String>() {
            {
                put("1", "一个月");
                put("2", "二个月");
                put("3", "三个月");
                put("4", "四个月");
                put("5", "五个月");
                put("6", "六个月");
                put("7", "七个月");
                put("8", "八个月");
                put("9", "九个月");
                put("10", "十个月");
                put("11", "十一个月");
                put("12", "十二个月");
            }
        };

        RadioTextItem(String startTitle, String endTitle, String editHint, boolean isDate, boolean isTime) {
            this.startTitle = startTitle;
            this.endTitle = endTitle;
            this.editHint = editHint;
            this.isDate = isDate;
            this.isTime = isTime;
        }
    }
}
