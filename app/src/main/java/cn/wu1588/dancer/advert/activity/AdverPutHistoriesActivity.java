package cn.wu1588.dancer.advert.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.cnsunrun.commonui.widget.recyclerview.DivideLineItemDecoration;
import com.my.toolslib.http.utils.NetRequestListenerProxy;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.UIUtils;

import cn.wu1588.dancer.model.AdHistory;
import butterknife.BindView;
import butterknife.ButterKnife;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;

public class AdverPutHistoriesActivity extends LBaseActivity {

    @BindView(R.id.title_bar)
    TitleBar titleBar;
    @BindView(R.id.rv_histories)
    RecyclerView rvHistories;
    private BaseQuickAdapter<AdHistory, BaseViewHolder> mAdpter;

    public static void start(Context context) {
        context.startActivity(new Intent(context, AdverPutHistoriesActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ad_put_history);
        ButterKnife.bind(this);
        titleBar.setTitleBarBackgroundDrawable(CommonApp.getApplication().getStateBarDrawable());

        mAdpter = new BaseQuickAdapter<AdHistory, BaseViewHolder>(R.layout.item_ad_put_manager_history) {
            @Override
            protected void convert(BaseViewHolder helper, AdHistory item) {
                String datas = "";
                if (!TextUtils.isEmpty(item.update_time)) {
                    datas = item.update_time;
                }
                //2019-12-31 00:00:00至81f32020-01-10 00:00:00
                PutDate putDate = parseDates(datas + item.info);
                if (putDate != null) {
                    helper.setText(R.id.tv_start_date, putDate.startDate)
                            .setGone(R.id.tv_start_date, !TextUtils.isEmpty(putDate.startDate));
                    helper.setText(R.id.tv_start_time, putDate.startTime)
                            .setGone(R.id.tv_start_time, !TextUtils.isEmpty(putDate.startTime));
                    helper.setText(R.id.tv_end_date, putDate.endDate)
                            .setGone(R.id.tv_end_date, !TextUtils.isEmpty(putDate.endDate));
                    helper.setText(R.id.tv_end_time, putDate.endTime)
                            .setGone(R.id.tv_end_time, !TextUtils.isEmpty(putDate.endTime));
                } else {
                    helper.setGone(R.id.tv_start_date, false);
                    helper.setGone(R.id.tv_start_time, false);
                    helper.setText(R.id.tv_end_date, item.info);
                    helper.setGone(R.id.tv_end_date, true);
                    helper.setGone(R.id.tv_end_time, false);
                }

                helper.setText(R.id.tv_ad_title, item.title)
                        .setText(R.id.tv_show_count, item.show)
                        .setText(R.id.tv_spend_count, item.cost)
                        .setText(R.id.tv_click_count, item.click)
                        .setText(R.id.tv_download_count, item.download);

                // 接下来的代码是微调各种间距，没办法，老板傻逼
                View view = helper.getView(R.id.ll_counts_layout);
                ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
                TextView endDateView = helper.getView(R.id.tv_end_date);
                LinearLayout.LayoutParams endDateViewlp = (LinearLayout.LayoutParams) endDateView.getLayoutParams();
                TextView startDateView = helper.getView(R.id.tv_start_date);
                LinearLayout.LayoutParams startDateLp = (LinearLayout.LayoutParams) startDateView.getLayoutParams();
                if (putDate != null && putDate.marginLeft > 0) {
                    lp.leftMargin = putDate.marginLeft;
                    endDateViewlp.gravity = Gravity.START;
                    startDateLp.gravity = Gravity.START;
                } else {
                    lp.leftMargin = (int) getResources().getDimension(R.dimen.dp_50);
                    endDateViewlp.gravity = Gravity.CENTER;
                    startDateLp.gravity = Gravity.CENTER;
                }
                view.setLayoutParams(lp);
                endDateView.setLayoutParams(endDateViewlp);
                startDateView.setLayoutParams(startDateLp);

            }
        };

        rvHistories.setAdapter(mAdpter);
        rvHistories.addItemDecoration(new DivideLineItemDecoration(this, Color.parseColor("#f4f4f4"), 1));

        BaseQuestStart.getPutAdHistories(new NetRequestListenerProxy(this) {
            @Override
            public void nofityUpdate(int i, BaseBean baseBean) {
                if (baseBean.status == 1) {
                    mAdpter.setNewData(baseBean.Data());
                } else {
                    UIUtils.shortM(baseBean.msg);
                }

            }
        });
    }

    private PutDate parseDates(String dates) {
        PutDate putDate = null;
        try {
            if (dates.startsWith("至")) {
                String[] dateTimes = dates.split("[\\u4e00-\\u9fa5]");
                String[] endTimes = dateTimes[1].split(" ");
                putDate = new PutDate();
                putDate.startDate = "";
                putDate.startTime = "至";
                putDate.endDate = endTimes[0];
                putDate.endTime = endTimes[1];
            } else if (dates.contains("每天")) {
                String[] dateTimes = dates.split("[\\u4e00-\\u9fa5]");
                putDate = new PutDate();
                putDate.startDate = dateTimes[0] + "至";
                putDate.endDate = dateTimes[1];
                String[] times = dateTimes[3].split("-");
                String startTime = times[0];
                String endTime = times[1];
                String resultTime = startTime.substring(0, startTime.length() - 3) + "-" + endTime.substring(0, endTime.length() - 3);
                putDate.endTime = "每天" + resultTime;
                putDate.marginLeft = (int) getResources().getDimension(R.dimen.dp_40);
            } else {
                String[] dateTimes = dates.split("[\\u4e00-\\u9fa5]");
                String[] startDateTime = dateTimes[0].split(" ");
                String[] endDateTime = dateTimes[1].split(" ");
                putDate = new PutDate();
                putDate.startDate = startDateTime[0];
                putDate.startTime = startDateTime[1] + "至\n";
                putDate.endDate = endDateTime[0];
                putDate.endTime = endDateTime[1];

            }
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
        return putDate;
    }

    private static class PutDate {

        String startDate = "";
        String endDate = "";
        String startTime = "";
        String endTime = "";
        int marginLeft;
    }
}
