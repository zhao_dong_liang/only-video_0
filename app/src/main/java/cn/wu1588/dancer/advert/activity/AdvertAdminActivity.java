package cn.wu1588.dancer.advert.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.cnsunrun.commonui.widget.button.RoundButton;
import com.my.toolslib.http.utils.LzyResponse;
import com.my.toolslib.http.utils.NetRequestListenerProxy;
import com.my.toolslib.http.utils.OkHttpRequestListener;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.tencent.mmkv.MMKV;

import cn.wu1588.dancer.model.Ad;
import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.model.SystemParams;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;
import cn.wu1588.dancer.mine.mode.AccountBean;

public class AdvertAdminActivity extends LBaseActivity {

    @BindView(R.id.title_bar)
    TitleBar title_bar;
    @BindView(R.id.rv_ads)
    RecyclerView rvAds;
    @BindView(R.id.surplus_money)
    TextView surplus_money;
    @BindView(R.id.today_moey)
    TextView today_moey;
    @BindView(R.id.price_tv)
    TextView price_tv;
    private BaseQuickAdapter<Ad, BaseViewHolder> mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setDark(CommonApp.getApplication().isBarTvColor());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advert_admin);
        title_bar.setTitleBarBackgroundDrawable(CommonApp.getApplication().getStateBarDrawable());
        mAdapter = new BaseQuickAdapter<Ad, BaseViewHolder>(R.layout.item_ad) {
            @Override
            protected void convert(BaseViewHolder helper, Ad item) {
                helper.setText(R.id.tv_ad_title, item.title);
                RoundButton button = helper.getView(R.id.btn_ad_state);
                switch (item.status) {
                    case 0:
                        helper.setText(R.id.btn_ad_state, "审核中");
                        button.setBackgroundColor(Color.parseColor("#F19149"));
                        break;
                    case 1:
                        helper.setText(R.id.btn_ad_state, "通过");
                        button.setBackgroundColor(Color.parseColor("#80C269"));
                        break;
                    case 2:
                        helper.setText(R.id.btn_ad_state, "未通过");
                        button.setBackgroundColor(Color.parseColor("#D2D2D2"));
                        break;
                }
                helper.itemView.setOnClickListener(v -> CommonIntent.startAddAdvertActivity(v.getContext(), item));
                helper.getView(R.id.tv_ad_update).setOnClickListener(v -> CommonIntent.startAddAdvertActivity(v.getContext(), item));
                helper.getView(R.id.tv_ad_manager).setOnClickListener(v -> CommonIntent.startAdManagerActivity(AdvertAdminActivity.this, item));
                helper.getView(R.id.tv_ad_detele).setOnClickListener(v -> BaseQuestStart.deleteAd(new NetRequestListenerProxy(v.getContext()){
                    @Override
                    public void nofityUpdate(int i, BaseBean baseBean) {
                        UIUtils.shortM(baseBean.msg);
                        loadDatas();
                    }
                }, item.id));

            }
        };

        rvAds.setAdapter(mAdapter);

    }

    @Override
    protected void onResume() {
        super.onResume();
        loadDatas();
    }

    @OnClick({R.id.add_tv})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.add_tv:
                CommonIntent.startAddAdvertActivity(that, null);
                break;

        }
    }

    @OnClick(R.id.put_history_bt)
    void histories(View view) {
        CommonIntent.startAdHistoriesActivity(this);
    }

    @OnClick(R.id.recharge_history_bt)
    void rechargeHistories(View view) {
        CommonIntent.startAdPayHistories(this);
    }

    @OnClick(R.id.recharge_bt)
    void adRecharge(View view) {
        CommonIntent.startAdRecharge(this);
    }

    private void loadDatas() {
        BaseQuestStart.getAdList(new NetRequestListenerProxy(this) {
            @Override
            public void nofityUpdate(int i, BaseBean baseBean) {
                mAdapter.setNewData(baseBean.Data());
            }
        });

        BaseQuestStart.getUserAccount(new OkHttpRequestListener() {
            @Override
            public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
                if (response.code == 1) {
                    AccountBean accountBean = (AccountBean) response.data;
                    surplus_money.setText(accountBean.ad_account);
                    today_moey.setText(accountBean.ad_day_cost);
                } else {
                    UIUtils.shortM(response.msg);
                }
            }
        });//获取用户总金额

        MMKV mmkv = MMKV.defaultMMKV();
        String ad_show_cost_every_thousand = mmkv.decodeString(SystemParams.AD_SHOW_COST_EVERY_THOUSAND);
        price_tv.setText(String.format("%s元/千次", ad_show_cost_every_thousand));
    }
}
