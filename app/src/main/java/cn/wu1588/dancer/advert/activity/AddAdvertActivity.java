package cn.wu1588.dancer.advert.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import androidx.constraintlayout.widget.Group;

import com.bilibili.boxing.Boxing;
import com.bilibili.boxing.model.config.BoxingConfig;
import com.bilibili.boxing.model.entity.BaseMedia;
import com.bilibili.boxing.model.entity.impl.ImageMedia;
import com.bilibili.boxing.utils.BoxingFileHelper;
import com.bilibili.boxing.utils.ImageCompressor;
import com.bilibili.boxing_impl.ui.BoxingActivity;
import com.maning.mndialoglibrary.MProgressBarDialog;
import com.my.toolslib.FileProviderUtils;
import com.my.toolslib.StringUtils;
import com.my.toolslib.http.utils.LzyResponse;
import com.shuyu.gsyvideoplayer.GSYVideoManager;
import com.shuyu.gsyvideoplayer.listener.GSYSampleCallBack;
import com.shuyu.gsyvideoplayer.video.StandardGSYVideoPlayer;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;
import com.sunrun.sunrunframwork.uiutils.UIUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import cn.wu1588.dancer.advert.MyScrollView;
import cn.wu1588.dancer.model.Ad;
import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.OssService;
import cn.wu1588.dancer.common.util.UpLoadUtils;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;

public class AddAdvertActivity extends LBaseActivity implements UpLoadUtils.UpLoadCallBack {
    @BindView(R.id.scroll)
    MyScrollView scrollView;
    @BindView(R.id.title_bar)
    TitleBar title_bar;
    @BindView(R.id.title_edit)
    EditText title_edit;
    @BindView(R.id.phrase_edit)
    EditText phrase_edit;
    @BindView(R.id.video_player)
    StandardGSYVideoPlayer video_player;
    @BindView(R.id.video_group)
    Group video_group;
    @BindView(R.id.android_edit)
    EditText android_edit;
    @BindView(R.id.ios_edit)
    EditText ios_edit;
    @BindView(R.id.qq_edit)
    EditText qq_edit;
    @BindView(R.id.phone_edit)
    EditText phone_edit;
    @BindView(R.id.img_text_url_edit)
    EditText img_text_url_edit;
    @BindView(R.id.data_edit)
    EditText data_edit;
    @BindView(R.id.rb_img_text_url)
    RadioButton rb_img_text_url;
    @BindView(R.id.rb_img_text)
    RadioButton rb_img_text;
    @BindView(R.id.cover_img)
    ImageView cover_img;
    @BindView(R.id.cover_group)
    Group cover_group;
    @BindView(R.id.qualification_labe)
    TextView qualification_labe;
    private String videoUrl;
    private int currentClickId;

    private final int REQUEST_CODE = 1;

    private Ad mAd;

    private String chooseApkPath;
    private String chooseFilePath;
    private String choosePkgPath;

    public static void start(Context context, @Nullable Ad ad) {
        Intent intent = new Intent(context, AddAdvertActivity.class);
        intent.putExtra("ad", ad);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setDark(CommonApp.getApplication().isBarTvColor());
        super.onCreate(savedInstanceState);
        mAd = getIntent().getParcelableExtra("ad");
        setContentView(R.layout.activity_add_advert);
        title_bar.setTitleBarBackgroundDrawable(CommonApp.getApplication().getStateBarDrawable());
        video_player.getBackButton().setVisibility(View.GONE);
        video_player.getFullscreenButton().setVisibility(View.GONE);
        video_player.setVideoAllCallBack(new GSYSampleCallBack());

        if (!TextUtils.isEmpty(mAd.html)) {
            rb_img_text.setChecked(true);
        } else if (!TextUtils.isEmpty(mAd.outer_url)) {
            rb_img_text_url.setChecked(true);
        } else {
            rb_img_text.setChecked(true);
        }
        rb_img_text.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                img_text_url_edit.getText().clear();
            }
        });
        rb_img_text_url.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                html = null;
            }
        });

        scrollView.setOnCustomerScrollChangeListener((scrollView, l, t, oldl, oldt) -> {
            title_edit.setFocusable(false);
            android_edit.setFocusable(false);
            data_edit.setFocusable(false);
            ios_edit.setFocusable(false);
            phone_edit.setFocusable(false);
            qq_edit.setFocusable(false);
            phrase_edit.setFocusable(false);
            img_text_url_edit.setFocusable(false);
        });
        scrollView.setOnFinishedListener(isFinish -> {
            if (isFinish) {
                title_edit.setFocusable(true);
                android_edit.setFocusable(true);
                data_edit.setFocusable(true);
                ios_edit.setFocusable(true);
                phone_edit.setFocusable(true);
                qq_edit.setFocusable(true);
                phrase_edit.setFocusable(true);
                img_text_url_edit.setFocusable(true);
                title_edit.setFocusableInTouchMode(true);
                android_edit.setFocusableInTouchMode(true);
                data_edit.setFocusableInTouchMode(true);
                ios_edit.setFocusableInTouchMode(true);
                phone_edit.setFocusableInTouchMode(true);
                qq_edit.setFocusableInTouchMode(true);
                phrase_edit.setFocusableInTouchMode(true);
                img_text_url_edit.setFocusableInTouchMode(true);
            }
        });

        if (mAd != null) {
            bindAd();
        }
    }

    private void bindAd() {
        title_edit.setText(mAd.title);
        title = mAd.title;
        phrase_edit.setText(mAd.short_title);
        short_title = mAd.short_title;
        cover_group.setVisibility(View.VISIBLE);
        GlideMediaLoader.load(that, cover_img, mAd.img_url);
        coverUrl = mAd.img_url;
        videoPlayUrl = mAd.video_url;
        qualification_url = mAd.qualification_url;
        if (!TextUtils.isEmpty(mAd.html)) {
            html = Html.fromHtml(mAd.html).toString();
        } else {
            html = mAd.html;
        }
        if (!TextUtils.isEmpty(mAd.outer_url)) {
            img_text_url_edit.setText(mAd.outer_url);
        }
        qq_edit.setText(mAd.qq);
        phone_edit.setText(mAd.phone);
        data_edit.setText(mAd.introduction_url);
        android_edit.setText(mAd.android_package_url);
        ios_edit.setText(mAd.ios_package_url);

        if (TextUtils.isEmpty(mAd.video_url)) {
            video_group.setVisibility(View.GONE);
            video_player.getStartButton().setVisibility(View.GONE);
        } else {
            video_group.setVisibility(View.VISIBLE);
            video_player.getStartButton().setVisibility(View.VISIBLE);
            video_player.setUp(mAd.video_url, false, "");
        }

    }

    @OnClick({R.id.send_bt, R.id.video_bt, R.id.cover_bt, R.id.rb_img_text, R.id.rb_img_text_url, R.id.img_text_bt, R.id.data_bt, R.id.android_bt, R.id.qualification_bt})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.video_bt://选择视频
                BoxingConfig videoConfig = new BoxingConfig(BoxingConfig.Mode.VIDEO);//.withVideoDurationRes(R.mipmap.add_video_icon)
                Boxing.of(videoConfig).withIntent(this, BoxingActivity.class).start(this, REQUEST_CODE);
                break;

            case R.id.send_bt://提交
                title = title_edit.getText().toString();
                if (StringUtils.isNull(title)) {
                    ToastUtils.longToast("请填写标题");
                    return;
                }
                short_title = phrase_edit.getText().toString();
                if (StringUtils.isNull(short_title)) {
                    ToastUtils.longToast("请填写广告短语");
                    return;
                }
                if (coverUrl == null) {
                    ToastUtils.longToast("请选择封面");
                    return;
                }
                if (!StringUtils.isNull(videoPath)) {
                    initDownloadDialog();
                    OssService ossService = new OssService();
                    ossService.startUpload(videoPath, "VOD_NO_TRANSCODE", title_edit.getText().toString(), title_edit.getText().toString(), null, handler);
                } else {
                    uploadFile();
                }

                break;
            case R.id.cover_bt://选择图片
                String cachePath = BoxingFileHelper.getCacheDir(that);
                Uri destUri = new Uri.Builder()
                        .scheme("file")
                        .appendPath(cachePath)
                        .appendPath(String.format(Locale.US, "%s.jpg", System.currentTimeMillis()))
                        .build();
                Boxing.of().withIntent(that, BoxingActivity.class).start(that, 2);
                break;
            case R.id.rb_img_text:
                rb_img_text_url.setChecked(false);
                break;
            case R.id.rb_img_text_url:
                rb_img_text.setChecked(false);
                break;
            case R.id.img_text_bt://广告图文选择
                CommonIntent.startRicheditorActivity(that, 10, html);
                break;
            case R.id.data_bt:
            case R.id.android_bt:
            case R.id.qualification_bt:
                currentClickId = view.getId();
                CommonIntent.pickFile(this);
                break;
        }
    }

    private String videoPath;
    private String videoPlayUrl;
    private String coverUrl;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null) {
            return;
        }
        if (requestCode == REQUEST_CODE && data != null) {
            final ArrayList<BaseMedia> medias = Boxing.getResult(data);
            BaseMedia baseMedia = medias.get(0);
            videoPath = baseMedia.getPath();
            video_player.setUp(videoPath, false, "");
            Log.i("OkGo:path==", baseMedia.getPath());
            video_player.getStartButton().setVisibility(View.VISIBLE);
            video_group.setVisibility(View.VISIBLE);

        }
        if (requestCode == 2) {
            //相册获取
            List<BaseMedia> medias = Boxing.getResult(data);
            //这里一定要做非空的判断
            if (medias != null && medias.size() > 0) {
                BaseMedia baseMedia = medias.get(0);
                String path;
                if (baseMedia instanceof ImageMedia) {
                    ((ImageMedia) baseMedia).compress(new ImageCompressor(that));
                    path = ((ImageMedia) baseMedia).getThumbnailPath();
                } else {
                    path = baseMedia.getPath();
                }
                File file = new File(path);
                UpLoadUtils upLoadUtils = new UpLoadUtils(that);
                upLoadUtils.setUpLoadCallBack(this::upLoadResult);
                upLoadUtils.upLoad(file.toString());
                //上传
            }
        } else if (requestCode == 10) {//广告图文混排
            html = data.getStringExtra("html");
            Log.d("Html", html);
        } else if (requestCode == CommonIntent.PICK_FILE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getData();
                String path;
                if ("file".equalsIgnoreCase(uri.getScheme())) {//使用第三方应用打开
                    path = uri.getPath();
                    return;
                }
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {//4.4以后
                    path = FileProviderUtils.getPath(this, uri);
                } else {//4.4以下下系统调用方法
                    path = FileProviderUtils.getRealPathFromURI(this, uri);
                }

                if (path != null) {
                    switch (currentClickId) {
                        case R.id.data_bt:
                            chooseFilePath = path;
                            data_edit.setText(chooseFilePath);
                            break;
                        case R.id.android_bt:
                            chooseApkPath = path;
                            android_edit.setText(chooseApkPath);
                            break;
                        case R.id.qualification_bt:
                            choosePkgPath = path;
                            SpannableStringBuilder sb = new SpannableStringBuilder("资质文件包上传\n");
                            SpannableString pathString = new SpannableString(choosePkgPath);
                            ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.parseColor("#f4f4f4"));
                            pathString.setSpan(foregroundColorSpan, 0, pathString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                            sb.append(pathString);
                            qualification_labe.setText(sb);
                            break;
                    }
                } else {
                    UIUtils.shortM("未找到文件");
                }
            }
        }
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 1) {
                int pro = (int) msg.obj;
                mProgressBarDialog.showProgress(pro, "已上传" + pro + "%", true);
            } else if (msg.what == 2) {//上传视频成功
                mProgressBarDialog.dismiss();
                videoPlayUrl = (String) ((Map) msg.obj).get("playUrl");

                ToastUtils.longToast("视频上传成功");
                uploadFile();

            } else if (msg.what == 3) {//上传视频失败
                mProgressBarDialog.dismiss();

                ToastUtils.longToast("视频上传失败，请重试");

            }
        }
    };

    private void uploadFile() {
        OssService ossService = new OssService();
        new Thread(() -> {
            String[] paths = {chooseFilePath, chooseApkPath, choosePkgPath};
            for (int i = 0; i < paths.length; i++) {
                String path = paths[i];
                if (!TextUtils.isEmpty(path)) {
                    String fileUrl = uploadFile(ossService, path);
                    if (TextUtils.isEmpty(fileUrl)) {
                        uploadFileFail(i, path, fileUrl);
                    } else {
                        uploadFileSeccess(i, fileUrl);
                    }
                }
            }
            runOnUiThread(this::saveAdvert);
        }).start();
    }

    private void uploadFileFail(int i, String path, String fileUrl) {
        runOnUiThread(() -> UIUtils.shortM(String.format("%s上传失败", path)));
        switch (i) {
            case 0:
                runOnUiThread(() -> data_edit.setText(null));
                break;
            case 1:
                runOnUiThread(() -> android_edit.setText(null));
                break;
            case 2:
                qualification_url = fileUrl;
                break;
        }
    }

    private void uploadFileSeccess(int i, String fileUrl) {
        switch (i) {
            case 0:
                runOnUiThread(() -> data_edit.setText(fileUrl));
                break;
            case 1:
                runOnUiThread(() -> android_edit.setText(fileUrl));
                break;
            case 2:
                qualification_url = fileUrl;
                break;
        }
    }

    @WorkerThread
    private String uploadFile(OssService service, String path) {
        runOnUiThread(() -> {
            initDownloadDialog();
            mProgressBarDialog.showProgress(0, "已上传" + 0 + "%", true);
        });
        String ret = service.uploadFile(path, progress -> runOnUiThread(() -> {
            int pro = (int) (progress.fraction * 100);
            mProgressBarDialog.showProgress(pro, "已上传" + pro + "%", true);
            if (pro >= 100) {
                mProgressBarDialog.showProgress(100, "上传完成，处理中...", true);
            }
        }));
        runOnUiThread(() -> mProgressBarDialog.dismiss());
        return ret;
    }

    private MProgressBarDialog mProgressBarDialog;

    private void initDownloadDialog() {
        //新建一个Dialog
        mProgressBarDialog = new MProgressBarDialog.Builder(this)
                //全屏模式
                .isWindowFullscreen(true)
                .setStyle(MProgressBarDialog.MProgressBarDialogStyle_Circle)
                //全屏背景窗体的颜色
                .setBackgroundWindowColor(Color.TRANSPARENT)
                //View背景的颜色
                .setBackgroundViewColor(Color.BLACK)
                //字体的颜色
                .setTextColor(Color.WHITE)
                //View边框的颜色
                .setStrokeColor(Color.TRANSPARENT)
                //View边框的宽度
                .setStrokeWidth(2)
                //View圆角大小
                .setCornerRadius(10)
                //ProgressBar背景色
                .setProgressbarBackgroundColor(Color.BLACK)
                //ProgressBar 颜色
                .setProgressColor(Color.WHITE)
                //圆形内圈的宽度
                .setCircleProgressBarWidth(4)
                //圆形外圈的宽度
                .setCircleProgressBarBackgroundWidth(4)
                //水平进度条Progress圆角
                .setProgressCornerRadius(0)
                //水平进度条的高度
                .setHorizontalProgressBarHeight(10)
                //dialog动画
//                .setAnimationID(R.style.animate_dialog_custom)
                .build();
    }

    @Override
    public void upLoadResult(String url, File file) {
        coverUrl = url;
        if (!StringUtils.isNull(url)) {
            cover_group.setVisibility(View.VISIBLE);
            GlideMediaLoader.load(that, cover_img, url);
        }
    }

    private String title, short_title, html, qualification_url;

    //保存广告
    private void saveAdvert() {
        if (StringUtils.isNull(title)) {
            ToastUtils.longToast("请先填写标题");
            return;
        }
        if (StringUtils.isNull(short_title)) {
            ToastUtils.longToast("请先填写广告短语");
            return;
        }
        if (StringUtils.isNull(coverUrl)) {
            ToastUtils.longToast("请先上传封面");
            return;
        }
        String updateId = mAd == null ? null : mAd.id;
        BaseQuestStart.saveAd(this, updateId, 3, title, short_title, null, android_edit.getText().toString(),
                ios_edit.getText().toString(), qq_edit.getText().toString(), phone_edit.getText().toString(), videoPlayUrl,
                coverUrl, rb_img_text.isChecked() ? html : null, rb_img_text_url.isChecked() ? img_text_url_edit.getText().toString() : null, data_edit.getText().toString(), qualification_url);
    }

    @Override
    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
        super.nofityUpdateUi(requestCode, response, view);
        if (requestCode == BaseQuestStart.SAVE_AD_CODE) {
            ToastUtils.longToast("添加成功");
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        GSYVideoManager.releaseAllVideos();
    }
}
