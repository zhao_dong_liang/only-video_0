package cn.wu1588.dancer.advert.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.my.toolslib.http.utils.NetRequestListenerProxy;
import com.sunrun.sunrunframwork.bean.BaseBean;

import cn.wu1588.dancer.model.AdPayHistory;
import butterknife.BindView;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.GetEmptyViewUtils;

public class AdPayHistoriesActivity extends LBaseActivity {

    @BindView(R.id.rv_ad_pay_histories)
    RecyclerView rv_ad_pay_histories;
    private BaseQuickAdapter<AdPayHistory, BaseViewHolder> mAdapter;

    public static void start(Context context) {
        Intent intent = new Intent(context, AdPayHistoriesActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ad_pay_histories);
        rv_ad_pay_histories.setAdapter(mAdapter = new BaseQuickAdapter<AdPayHistory, BaseViewHolder>(R.layout.item_ad_pay_history) {
            @Override
            protected void convert(BaseViewHolder helper, AdPayHistory item) {
                String[] dateAndTime = item.add_time.split(" ");
                helper.setText(R.id.tv_ad_pay_date, dateAndTime[0])
                        .setText(R.id.tv_ad_pay_time, dateAndTime[1])
                        .setText(R.id.tv_ad_pay_amount, item.money)
                        .setText(R.id.tv_ad_pay_type, item.pay_type.equals("2") ? "微信" : "支付宝");
            }
        });
        GetEmptyViewUtils.bindEmptyView(that, mAdapter, 0, "暂无充值记录", true);

        BaseQuestStart.getAdPayHistories(new NetRequestListenerProxy(this) {
            @Override
            public void nofityUpdate(int i, BaseBean baseBean) {
                if (baseBean.status == 1) {
                    mAdapter.setNewData(baseBean.Data());
                }
            }
        });
    }
}
