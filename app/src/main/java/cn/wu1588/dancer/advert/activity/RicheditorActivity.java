package cn.wu1588.dancer.advert.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;

import com.bilibili.boxing.Boxing;
import com.bilibili.boxing.model.config.BoxingConfig;
import com.bilibili.boxing.model.config.BoxingCropOption;
import com.bilibili.boxing.model.entity.BaseMedia;
import com.bilibili.boxing.model.entity.impl.ImageMedia;
import com.bilibili.boxing.utils.BoxingFileHelper;
import com.bilibili.boxing.utils.ImageCompressor;
import com.bilibili.boxing_impl.ui.BoxingActivity;
import com.my.toolslib.StringUtils;

import java.io.File;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.util.UpLoadUtils;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;
import jp.wasabeef.richeditor.RichEditor;

public class RicheditorActivity extends LBaseActivity implements UpLoadUtils.UpLoadCallBack {

    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.editor)
    RichEditor editor;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setDark(CommonApp.getApplication().isBarTvColor());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_richeditor);
        titleBar.setTitleBarBackgroundDrawable(CommonApp.getApplication().getStateBarDrawable());
        editor.setPadding(10, 10, 10, 10);
        titleBar.setRightVisible(View.VISIBLE);
        titleBar.setRightText("确认");
        titleBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("OkGo:",editor.getHtml());
                Intent intent=new Intent();
                intent.putExtra("html",editor.getHtml());
                setResult(1,intent);
                finish();
            }
        });
        String html=getIntent().getStringExtra("html");
        if (!StringUtils.isNull(html)){
            editor.setHtml(html);
        }
    }

    @OnClick({R.id.action_undo,R.id.action_redo,R.id.action_bold,R.id.action_italic,R.id.action_subscript,R.id.action_superscript,R.id.action_strikethrough
    ,R.id.action_underline,R.id.action_heading1,R.id.action_heading2,R.id.action_heading3,R.id.action_heading4,R.id.action_heading5,R.id.action_heading6,
    R.id.action_indent,R.id.action_outdent,R.id.action_align_left,R.id.action_align_center,R.id.action_align_right,R.id.action_blockquote,R.id.action_insert_bullets,
    R.id.action_insert_numbers,R.id.action_insert_image,R.id.action_insert_checkbox})
    public void onClick(View v){
        switch (v.getId()){
            case R.id.action_undo:
            editor.undo();
                break;
            case R.id.action_redo:
                editor.redo();
                break;
            case R.id.action_bold:
                editor.setBold();
                break;
            case R.id.action_italic:
                editor.setItalic();
                break;
            case R.id.action_subscript:
                editor.setSubscript();
                break;
            case R.id.action_superscript:
                editor.setSuperscript();
                break;
            case R.id.action_strikethrough:
                editor.setStrikeThrough();
                break;
            case R.id.action_underline:
                editor.setUnderline();
                break;
            case R.id.action_heading1:
                editor.setHeading(1);
                break;
            case R.id.action_heading2:
                editor.setHeading(2);
                break;
            case R.id.action_heading3:
                editor.setHeading(3);
                break;
            case R.id.action_heading4:
                editor.setHeading(4);
                break;
            case R.id.action_heading5:
                editor.setHeading(5);
                break;
            case R.id.action_heading6:
                editor.setHeading(6);
                break;
            case R.id.action_indent:
                editor.setIndent();
                break;
            case R.id.action_outdent:
                editor.setOutdent();
                break;
            case R.id.action_align_left:
                editor.setAlignLeft();
                break;
            case R.id.action_align_center:
                editor.setAlignCenter();
                break;
            case R.id.action_align_right:
                editor.setAlignRight();
                break;
            case R.id.action_blockquote:
                editor.setBlockquote();
                break;
            case R.id.action_insert_bullets:
                editor.setBullets();
                break;
            case R.id.action_insert_numbers:
                editor.setNumbers();
                break;
            case R.id.action_insert_image:
                String cachePath = BoxingFileHelper.getCacheDir(that);
                Uri destUri = new Uri.Builder()
                        .scheme("file")
                        .appendPath(cachePath)
                        .appendPath(String.format(Locale.US, "%s.jpg", System.currentTimeMillis()))
                        .build();
                BoxingConfig singleCropImgConfig = new BoxingConfig(BoxingConfig.Mode.SINGLE_IMG)
                        .needCamera()
                        .needGif()
                        .withCropOption(new BoxingCropOption(destUri).withMaxResultSize(200, 200).aspectRatio(1, 1));
                Boxing.of(singleCropImgConfig).withIntent(that, BoxingActivity.class).start(that, 2);
                break;
            case R.id.action_insert_checkbox:
                editor.insertTodo();
                break;












        }
    }
    @Override
    public void upLoadResult(String url, File file) {
     editor.insertImage(url,"twitter");

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2) {
            //相册获取
            List<BaseMedia> medias = Boxing.getResult(data);
            //这里一定要做非空的判断
            if (medias != null && medias.size() > 0) {
                BaseMedia baseMedia = medias.get(0);
                String path;
                if (baseMedia instanceof ImageMedia) {
                    ((ImageMedia) baseMedia).compress(new ImageCompressor(that));
                    path = ((ImageMedia) baseMedia).getThumbnailPath();
                } else {
                    path = baseMedia.getPath();
                }
                File  file = new File(path);
                UpLoadUtils upLoadUtils=new UpLoadUtils(that);
                upLoadUtils.setUpLoadCallBack(this::upLoadResult);
                upLoadUtils.upLoad(file.toString());
                //上传
            }
        }
    }
}
