package cn.wu1588.dancer.moviefeedback;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.cnsunrun.commonui.widget.recyclerview.DivideLineItemDecoration;
import com.my.toolslib.http.utils.NetRequestListenerProxy;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.UIUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;

public class MovieFeedbackActivity extends LBaseActivity {

    @BindView(R.id.title_bar)
    TitleBar titleBar;
    @BindView(R.id.rv_feedback_list)
    RecyclerView rvFeedBackList;

    private BaseQuickAdapter<MovieFeedBack, BaseViewHolder> mAdapter;
    private MovieFeedBack mCurrentSelected;
    private String movieId;

    public static void start(Context context, String movieId) {
        Intent intent = new Intent(context, MovieFeedbackActivity.class);
        intent.putExtra("movieId", movieId);
        context.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        movieId = getIntent().getStringExtra("movieId");
        setContentView(R.layout.activity_movie_feedback);
        ButterKnife.bind(this);
        titleBar.setTitleBarBackgroundDrawable(CommonApp.getApplication().getStateBarDrawable());
        rvFeedBackList.addItemDecoration(new DivideLineItemDecoration(this, Color.parseColor("#f4f4f4"), 1));
        rvFeedBackList.setAdapter(mAdapter = new BaseQuickAdapter<MovieFeedBack, BaseViewHolder>(R.layout.item_movie_feedback) {
            @Override
            protected void convert(BaseViewHolder helper, MovieFeedBack item) {
                ((TextView)helper.itemView).setText(item.title);
                helper.itemView.setSelected(item.isSelected);
                helper.itemView.setOnClickListener(v -> {
                    if (mCurrentSelected != null) {
                        mCurrentSelected.isSelected = !mCurrentSelected.isSelected;
                    }
                    item.isSelected = !item.isSelected;
                    mCurrentSelected = item;
                    notifyDataSetChanged();
                });
            }
        });

        BaseQuestStart.getFeedBacks(new NetRequestListenerProxy(this) {
            @Override
            public void nofityUpdate(int i, BaseBean baseBean) {
                mAdapter.setNewData(baseBean.Data());
            }
        });
    }

    @OnClick(R.id.submit)
    void submit(View view) {
        MovieFeedbackRequest movieFeedbackRequest = new MovieFeedbackRequest();
        movieFeedbackRequest.cate_id = mCurrentSelected.id;
        movieFeedbackRequest.content = mCurrentSelected.title;
        movieFeedbackRequest.movie_id = movieId;
        BaseQuestStart.submitFeedBacks(new NetRequestListenerProxy(this) {
            @Override
            public void nofityUpdate(int i, BaseBean baseBean) {
                if (baseBean.status == 1) {
                    finish();
                } else {
                    UIUtils.shortM(baseBean.msg);
                }
            }
        }, movieFeedbackRequest);
    }
}
