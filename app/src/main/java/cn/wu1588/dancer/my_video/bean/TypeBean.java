package cn.wu1588.dancer.my_video.bean;

import java.util.List;

public class TypeBean {
    public List<HomeCategory> home_category;

    public static class HomeCategory{
        public int id;
        public  String title;
    }
}
