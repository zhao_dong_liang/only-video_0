package cn.wu1588.dancer.my_video.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bilibili.boxing.Boxing;
import com.bilibili.boxing.model.config.BoxingConfig;
import com.bilibili.boxing.model.entity.BaseMedia;
import com.bilibili.boxing_impl.ui.BoxingActivity;
import com.maning.mndialoglibrary.MProgressBarDialog;
import com.my.toolslib.FileUtils;
import com.my.toolslib.StringUtils;
import com.my.toolslib.http.utils.LzyResponse;
import com.shuyu.gsyvideoplayer.video.StandardGSYVideoPlayer;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;
import com.sunrun.sunrunframwork.utils.EmptyDeal;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.OssService;
import cn.wu1588.dancer.common.util.UpLoadUtils;
import cn.wu1588.dancer.common.util.VideoThumbnailUtils;
import cn.wu1588.dancer.my_video.bean.LabeBean;

/**
 * 作者：ninetailedfox
 * 时间：2021/1/14 5:40 PM
 * 邮箱: 1037438704@qq.com
 * 功能：发布视频页面
 **/
public class UpVideosActivity extends LBaseActivity implements UpLoadUtils.UpLoadCallBack {
    @BindView(R.id.add_bt)
    Button add_bt;
    @BindView(R.id.video_player)
    StandardGSYVideoPlayer video_player;
    @BindView(R.id.thumbnail_layout)
    LinearLayout thumbnail_layout;
    @BindView(R.id.frame_01)
    FrameLayout frame_01;
    @BindView(R.id.frame_02)
    FrameLayout frame_02;
    @BindView(R.id.frame_03)
    FrameLayout frame_03;
    @BindView(R.id.frame_04)
    FrameLayout frame_04;
    @BindView(R.id.frame_05)
    FrameLayout frame_05;
    @BindView(R.id.frame_06)
    FrameLayout frame_06;
    @BindView(R.id.select_img)
    ImageView select_img;
    @BindView(R.id.select_linear)
    LinearLayout select_linear;
    @BindView(R.id.linear_view)
    View linear_view;
    @BindView(R.id.public_rb)
    RadioButton public_rb;
    @BindView(R.id.privite_rb)
    RadioButton privite_rb;
    @BindView(R.id.title_edit)
    EditText title_edit;
    @BindView(R.id.text_num)
    TextView text_num;
    @BindView(R.id.type_tv)
    TextView type_tv;
    @BindView(R.id.labe_tv)
    TextView labe_tv;
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @BindView(R.id.gold_edit)
    EditText gold_edit;
    @BindView(R.id.dec_edit)
    EditText dec_edit;
    @BindView(R.id.dec_num)
    TextView dec_num;
    @BindView(R.id.linear_view04)
    View linear_view04;
    @BindView(R.id.see_layout)
    LinearLayout see_layout;
    @BindView(R.id.title_bar)
    TextView title_bar;
    @BindView(R.id.title_right_text)
    TextView title_right_text;

    @BindView(R.id.teachingVideoRG)
    RadioGroup teachingVideoRG;

    @BindView(R.id.rb_teaching_false)
    RadioButton rb_teaching_false;

    private VideoThumbnailUtils videoThumbnailUtils;
    private View oldView;
    private boolean isVertical;//是否是竖直视频
    private boolean isDebug = true;//是否测试
    private boolean isPrivate;
    private final int REQUEST_CODE = 1;
    private boolean teachingVideoBool = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setDark(CommonApp.getApplication().isBarTvColor());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_up_videos);
        initViews();
    }

    private void initViews() {
        title_bar.setText("作品发布");
        title_right_text.setText("提交");
        Window window = getWindow();
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        isPrivate = getIntent().getBooleanExtra("isPrivate", false);
        user_movie_type = isPrivate ? 1 : 0;
        linear_view04.setVisibility(isPrivate ? View.GONE : View.VISIBLE);
        see_layout.setVisibility(isPrivate ? View.GONE : View.VISIBLE);
        video_player.getBackButton().setVisibility(View.GONE);
        video_player.getStartButton().setVisibility(View.GONE);
        video_player.getFullscreenButton().setVisibility(View.GONE);
        rb_teaching_false.setChecked(true);

        if (videoPath == null) {
            thumbnail_layout.setVisibility(View.GONE);
        } else {
            thumbnail_layout.setVisibility(View.VISIBLE);
        }

        videoThumbnailUtils = new VideoThumbnailUtils(that, new VideoThumbnailUtils.VideoThumbnailCall() {
            @Override
            public void getVideoThumbnailResult(ArrayList<Bitmap> bitmaps) {
                if (bitmaps != null && bitmaps.size() > 0) {
                    if (isStandard()) {
                        select_linear.setVisibility(View.VISIBLE);
                        thumbnail_layout.setVisibility(View.VISIBLE);
                        linear_view.setVisibility(View.VISIBLE);
                        setThumbImageView(videoThumbnailUtils.getFristThumbnail());
                        setThumbViews(bitmaps);
                        add_bt.setVisibility(View.GONE);
                        video_player.getStartButton().setVisibility(View.VISIBLE);
                    } else {
                        add_bt.setVisibility(View.VISIBLE);
                        video_player.getStartButton().setVisibility(View.GONE);
                        ToastUtils.longToast("该视频不符合规范");
                    }
                }
            }
        });
        title_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String s = charSequence.toString();
                text_num.setText("不超过30个字（" + s.length() + "/30）");
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.public_rb) {
                    user_movie_type = 0;
                } else {
                    user_movie_type = 1;
                }
            }
        });

        public_rb.setChecked(true);
        dec_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int length = charSequence.length();
                dec_num.setText("(" + length + "/100)");
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    //设置视频封面
    private void setThumbImageView(Bitmap bitmap) {
        select_img.setImageBitmap(bitmap);
        ImageView imageView = new ImageView(that);
//        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setImageBitmap(bitmap);
        video_player.setThumbImageView(imageView);
    }

    @Override
    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
        super.nofityUpdateUi(requestCode, response, view);
        if (requestCode == BaseQuestStart.ADD_VIDEO_CODE) {//视频上传成功
            if (response.code == 1) {
                ToastUtils.longToast("上传成功");
                finish();
            }
        }
    }

    @OnClick({R.id.add_bt, R.id.frame_01, R.id.frame_02, R.id.frame_03, R.id.frame_04, R.id.frame_05, R.id.frame_06,
            R.id.type_layout, R.id.labe_layout, R.id.image_finish, R.id.title_right_text})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add_bt:
                BoxingConfig videoConfig = new BoxingConfig(BoxingConfig.Mode.VIDEO);//.withVideoDurationRes(R.mipmap.add_video_icon)
                Boxing.of(videoConfig).withIntent(this, BoxingActivity.class).start(this, REQUEST_CODE);
                break;
            case R.id.frame_01:
                selectView(frame_01, 0);
                break;
            case R.id.frame_02:
                selectView(frame_02, 1);
                break;
            case R.id.frame_03:
                selectView(frame_03, 2);
                break;
            case R.id.frame_04:
                selectView(frame_04, 3);
                break;
            case R.id.frame_05:
                selectView(frame_05, 4);
                break;
            case R.id.frame_06:
                selectView(frame_06, 5);
                break;
            case R.id.type_layout://分类页
                CommonIntent.startTypeActivity(that, 2, isVertical);
                break;
            case R.id.labe_layout://标签页
                CommonIntent.startLabeActivity(that, 1);
                break;
            case R.id.title_right_text:
                if (StringUtils.isNull(videoId)) {
                    if (StringUtils.isNull(videoPath)) {
                        ToastUtils.longToast("请先选择视频");
                        return;
                    } else {
                        String title = title_edit.getText().toString();
                        if (StringUtils.isNull(title)) {
                            ToastUtils.longToast("请输入标题");
                            return;
                        }

                        if (StringUtils.isNull(typeId)) {
                            ToastUtils.longToast("请选择分类");
                            return;
                        }
                        initDownloadDialog();
                        UpLoadUtils upLoadUtils = new UpLoadUtils(that);
                        upLoadUtils.setUpLoadCallBack(this::upLoadResult);
                        Bitmap bitmap = videoThumbnailUtils.getBitmaps().get(selectPos);
                        if (bitmap != null) {
                            String fileName = FileUtils.saveBitmap(bitmap);
                            if (fileName != null) {
                                mProgressBarDialog.showProgress(0, "上传中");
                                upLoadUtils.upLoad(fileName);
                            }
                        }

                    }

                }
                break;
            case R.id.image_finish:
                finish();
                break;
        }
    }

    private void selectView(FrameLayout frameLayout, int pos) {
        if (oldView == null) {
            oldView = frame_01.getChildAt(1);
        }
        selectPos = pos;
        oldView.setBackgroundColor(Color.TRANSPARENT);
        View childAt = frameLayout.getChildAt(1);
        childAt.setBackgroundResource(R.drawable.white_rect);
        oldView = childAt;
        Bitmap bitmap = videoThumbnailUtils.getBitmaps().get(pos);
        if (bitmap != null) {
            select_img.setImageBitmap(bitmap);
        }

    }

    private String videoPath, image, videoId;
    private String labe;
    private int selectPos;
    private String typeId;//分类
    private int user_movie_type;//是否公开，0公开，1私密

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            final ArrayList<BaseMedia> medias = Boxing.getResult(data);
            BaseMedia baseMedia = medias.get(0);
            videoPath = baseMedia.getPath();
            video_player.setUp(videoPath, false, "");
            Log.i("OkGo:path==", baseMedia.getPath() + ", " + baseMedia.getId());
            String[] paths = baseMedia.getPath().split("/");
            String videoName = paths[paths.length - 1];
            title_edit.setText(videoName);
            title_edit.setSelection(0, videoName.length());

            videoThumbnailUtils.getVideoThumbnail(baseMedia.getPath());
            video_player.getStartButton().setVisibility(View.GONE);
        }
        if (data == null) {
            return;
        }
        if (requestCode == 2) {//分类
            String type = data.getStringExtra("type");
            typeId = data.getStringExtra("id");
            type_tv.setText(type);
        } else if (requestCode == 1) {//标签
            if (data != null) {
                ArrayList<LabeBean> labeBeans = data.getParcelableArrayListExtra("list");
                if (!EmptyDeal.empty(labeBeans)) {
                    String s = "";
                    String l = "";
                    for (int i = 0; i < labeBeans.size(); i++) {
                        if (i < labeBeans.size() - 1) {
                            s = s + labeBeans.get(i).getTitle() + " ";
                            l = l + labeBeans.get(i).getId() + ",";
                        } else {
                            s = " " + s + labeBeans.get(i).getTitle();
                            l = "," + l + labeBeans.get(i).getId();
                        }
                    }
                    labe_tv.setText(s);
                    labe = l;
                }

                Log.i("", "");
            }
        }
    }

    //布局封面view
    private void setThumbViews(ArrayList<Bitmap> bitmaps) {
        for (int i = 0; i < thumbnail_layout.getChildCount(); i++) {
            Bitmap bitmap = bitmaps.get(i);
            FrameLayout frameLayout = (FrameLayout) thumbnail_layout.getChildAt(i);
            ImageView imageView = (ImageView) frameLayout.getChildAt(0);
            imageView.setImageBitmap(bitmap);
        }
    }

    //判断视频是否规范
    private boolean isStandard() {
        String videoVode = videoThumbnailUtils.getVideoVode();
        int video_height = videoThumbnailUtils.getVideo_height();
        int video_width = videoThumbnailUtils.getVideo_width();
        if (!StringUtils.isNull(videoVode)) {
            if ("h265".equals(videoVode) || "H265".equals(videoVode) || "hevc".equals(videoVode)) {
                if ((video_height == 854 && video_width == 480) || (video_height == 480 && video_width == 854)) {
                    if (video_height > video_width) {
                        isVertical = true;
                    } else {
                        isVertical = false;
                    }
                    return true;
                }

            }
        }
        if (isDebug) {
            if (video_height > video_width) {
                isVertical = true;
            } else {
                isVertical = false;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (handler != null) {
            handler.removeCallbacks(null);
        }
        videoThumbnailUtils.close();
    }

    private MProgressBarDialog mProgressBarDialog;

    private void initDownloadDialog() {
        //新建一个Dialog
        mProgressBarDialog = new MProgressBarDialog.Builder(this)
                //全屏模式
                .isWindowFullscreen(true)
                .setStyle(MProgressBarDialog.MProgressBarDialogStyle_Circle)
                //全屏背景窗体的颜色
                .setBackgroundWindowColor(Color.TRANSPARENT)
                //View背景的颜色
                .setBackgroundViewColor(Color.BLACK)
                //字体的颜色
                .setTextColor(Color.WHITE)
                //View边框的颜色
                .setStrokeColor(Color.TRANSPARENT)
                //View边框的宽度
                .setStrokeWidth(2)
                //View圆角大小
                .setCornerRadius(10)
                //ProgressBar背景色
                .setProgressbarBackgroundColor(Color.BLACK)
                //ProgressBar 颜色
                .setProgressColor(Color.WHITE)
                //圆形内圈的宽度
                .setCircleProgressBarWidth(4)
                //圆形外圈的宽度
                .setCircleProgressBarBackgroundWidth(4)
                //水平进度条Progress圆角
                .setProgressCornerRadius(0)
                //水平进度条的高度
                .setHorizontalProgressBarHeight(10)
                //dialog动画
//                .setAnimationID(R.style.animate_dialog_custom)
                .build();
    }

    @Override
    public void upLoadResult(String url, File file) {
        image = url;
        OssService ossService = new OssService();
        ossService.startUpload(videoPath, "VOD_NO_TRANSCODE", url, dec_edit.getText().toString(), title_edit.getText().toString(), handler);
    }

    @SuppressLint("HandlerLeak")
    Handler handler = new Handler() {
        @SuppressLint("HandlerLeak")
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 1) {
                int pro = (int) msg.obj;
                mProgressBarDialog.showProgress(pro, "已上传" + pro + "%", true);
            } else if (msg.what == 2) {//上传视频成功
                String title = title_edit.getText().toString();
                String gold = gold_edit.getText().toString();
                String dec = dec_edit.getText().toString();
                videoId = (String) ((Map) msg.obj).get("videoId");
                BaseQuestStart.addVideo(UpVideosActivity.this, mProgressBarDialog, title, image, videoId, labe, gold, user_movie_type, dec, typeId);
            } else if (msg.what == 3) {//上传视频失败
                mProgressBarDialog.dismiss();
                ToastUtils.longToast("视频上传失败，请重试");
            }
        }
    };

}
