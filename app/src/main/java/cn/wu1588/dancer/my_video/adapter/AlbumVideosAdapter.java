package cn.wu1588.dancer.my_video.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.my_video.bean.MyVideoBean;

public class AlbumVideosAdapter extends BaseQuickAdapter<MyVideoBean, BaseViewHolder> {



    public AlbumVideosAdapter() {
        super(R.layout.my_download_item);
    }
    @Override
    protected void convert(BaseViewHolder helper, MyVideoBean item) {
//        String download_time = item.getDownload_time();
//        String[] s = download_time.split(" ");
        GlideMediaLoader.load(mContext,helper.getView(R.id.img),item.getImage(),R.drawable.ic_history_place);
        helper.setText(R.id.video_name,item.getTitle())
                .setText(R.id.time_tv,"观看次数:"+item.getPlay_num())
                .setText(R.id.download_num,"已下载:"+item.getDown_num());
    }
}
