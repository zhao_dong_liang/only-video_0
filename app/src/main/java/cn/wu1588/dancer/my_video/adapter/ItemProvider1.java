package cn.wu1588.dancer.my_video.adapter;

import android.widget.ImageView;

import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.provider.BaseItemProvider;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.my_video.bean.MyVideoBean;

//单行类型view
public class ItemProvider1 extends BaseItemProvider<MyVideoBean, BaseViewHolder> {
    @Override
    public int viewType() {
        return MyVideoAdapter.ITEM_CODE;
    }

    @Override
    public int layout() {
        return R.layout.my_video_item1;
    }

    @Override
    public void convert(BaseViewHolder helper, MyVideoBean data, int position) {
        ImageView imageView = helper.getView(R.id.img);
        GlideMediaLoader.load(mContext, imageView, data.getImage(), R.mipmap.img_def);
        helper.setText(R.id.text_num, data.getPlay_num());
//        long play_time = data.getPlay_time();
//        String strBySecond = DateUtil.getTimeStrBySecond(play_time);

//                .setText(R.id.see_num, data.getPlay_num()+"播放\t\t\t"+data.getLike_num()+"评论")
//                .setText(R.id.text_title, data.getTitle())
//                .setText(R.id.video_time, strBySecond);
    }
}
