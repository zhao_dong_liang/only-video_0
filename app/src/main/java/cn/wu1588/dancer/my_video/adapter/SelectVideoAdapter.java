package cn.wu1588.dancer.my_video.adapter;

import android.view.View;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.my.toolslib.DateUtil;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.my_video.bean.MyVideoBean;

public class SelectVideoAdapter extends BaseQuickAdapter<MyVideoBean, BaseViewHolder> {
    private int type;
    public SelectVideoAdapter() {
        super(R.layout.select_video_item);
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    protected void convert(BaseViewHolder helper, MyVideoBean item) {
        int indexNum = item.getIndexNum();
        if (type==0){
            helper.getView(R.id.index_tv).setVisibility(indexNum==-1? View.GONE:View.VISIBLE);
        }
        helper.setText(R.id.index_tv,item.getIndexNum()+"");
        helper.setText(R.id.title_tv,item.getTitle());

        ImageView imageView= helper.getView(R.id.img);
        GlideMediaLoader.load(mContext,imageView,item.getImage(),R.mipmap.img_def);
        long play_time = item.getPlay_time();
        String strBySecond = DateUtil.getTimeStrBySecond(play_time);
        helper.setText(R.id.title_tv,item.getTitle())
                .setText(R.id.see_num,item.getPlay_num())
                .setText(R.id.video_time,strBySecond);



    }


}
