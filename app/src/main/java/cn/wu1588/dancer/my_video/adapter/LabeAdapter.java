package cn.wu1588.dancer.my_video.adapter;

import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.google.android.flexbox.FlexboxLayout;
import com.my.toolslib.DisplayUtils;

import java.util.ArrayList;
import java.util.List;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.my_video.bean.LabeBean;

public class LabeAdapter extends BaseQuickAdapter<LabeBean,BaseViewHolder> {
    public DisplayUtils displayUtil;
    public LabeAdapter(@Nullable List data) {
        super(R.layout.labe_item, data);
        displayUtil=new DisplayUtils();
    }


    @Override
    protected void convert(BaseViewHolder helper, LabeBean item) {

       FlexboxLayout flexboxLayout= helper.getView(R.id.flexboxLayout);
        ArrayList<LabeBean> labeBeans = item.getLabeBeans();
        helper.setText(R.id.title_tv,item.getTitle());
        setViews(labeBeans,flexboxLayout);

    }
    private  int checkedNum;
    private int checkedCount=3;
    public void  setViews(ArrayList<LabeBean> labeBeans, FlexboxLayout flexboxLayout){
        for (int i=0;i<labeBeans.size();i++){
            boolean clicked = labeBeans.get(i).isClicked();
            CheckBox checkBox= (CheckBox) LayoutInflater.from(mContext).inflate(R.layout.labe_check_item,null);
//            ableCheck(checkBox);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b){
                        checkedNum++;
                    }else {
                        checkedNum--;
                    }
                    ableCheck(checkBox,labeBeans,b);
                }
            });
            flexboxLayout.addView(checkBox);
            checkBox.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            checkBox.setPadding(displayUtil.dp2px(mContext,8),displayUtil.dp2px(mContext,5),displayUtil.dp2px(mContext,8),
                    displayUtil.dp2px(mContext,5));
            checkBox.setId(labeBeans.get(i).getIndex());
            checkBox.setText(labeBeans.get(i).getTitle());
            checkBox.getLayoutParams().width= FlexboxLayout.LayoutParams.WRAP_CONTENT;
            checkBox.getLayoutParams().height=FlexboxLayout.LayoutParams.WRAP_CONTENT;
            FlexboxLayout.LayoutParams layoutParams = (FlexboxLayout.LayoutParams) checkBox.getLayoutParams();
            layoutParams.bottomMargin=displayUtil.dp2px(mContext,8);
            layoutParams.leftMargin=displayUtil.dp2px(mContext,5);
            layoutParams.rightMargin=displayUtil.dp2px(mContext,5);

            checkBox.setChecked(clicked);
        }
    }
    private void ableCheck(CheckBox checkBox,ArrayList<LabeBean> labeBeans,boolean b){
        if (checkedNum>=checkedCount+1){
            checkBox.setChecked(false);
            labeBeans.get(checkBox.getId()).setClicked(false);
        }else {
           if (b){
               checkArrayList.add(labeBeans.get(checkBox.getId()));
           }else {
               checkArrayList.remove(labeBeans.get(checkBox.getId()));
           }
            labeBeans.get(checkBox.getId()).setClicked(true);
           checkBox.setChecked(true);
        }
    }
    private  ArrayList<LabeBean> checkArrayList=new ArrayList<>();
    public ArrayList<LabeBean> getCheckArrayList(){
        return checkArrayList;
    }
}
