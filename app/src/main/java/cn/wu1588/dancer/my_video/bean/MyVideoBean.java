package cn.wu1588.dancer.my_video.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.my.toolslib.StringUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class MyVideoBean implements Parcelable {
    private String id;
    private String title;
    private String image;
    private int type=0;//1是单行，2是多行
    private int VFType = 0;
    private int indexNum=-1;//被选中的下标
    private String update_time;
    private String date;
    private String play_num;
    private long  play_time;
    private int down_num;
    @SerializedName(value = "like_num", alternate = {"top_num"})
    private int like_num=0;
    public int user_movie_type;

    //审核状态 1 待审；2过审；3审核失败
    private int sta;
    //4K售价
    private String price;
    //是否4K视频
    private String if4k;
    //是否分享视频
    private String is_share;
    //4K体积
    private String size;

    public MyVideoBean(){}
    protected MyVideoBean(Parcel in) {
        id = in.readString();
        title = in.readString();
        type = in.readInt();
        indexNum=in.readInt();
        update_time=in.readString();
        image=in.readString();
        date=in.readString();
        play_num=in.readString();
        play_time=in.readLong();
        down_num=in.readInt();
        like_num=in.readInt();
        user_movie_type = in.readInt();
        sta = in.readInt();
        price = in.readString();
        size = in.readString();
        if4k = in.readString();
        is_share = in.readString();
    }


    public int getVFType() {
        return VFType;
    }

    public void setVFType(int VFType) {
        this.VFType = VFType;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public static final Creator<MyVideoBean> CREATOR = new Creator<MyVideoBean>() {
        @Override
        public MyVideoBean createFromParcel(Parcel in) {
            return new MyVideoBean(in);
        }

        @Override
        public MyVideoBean[] newArray(int size) {
            return new MyVideoBean[size];
        }
    };

    public int getLike_num() {
        return like_num;
    }

    public void setLike_num(int like_num) {
        this.like_num = like_num;
    }

    public int getDown_num() {
        return down_num;
    }

    public void setDown_num(int down_num) {
        this.down_num = down_num;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPlay_num() {
        if (!StringUtils.isNull(play_num)){
            int num=Integer.parseInt(play_num);
            if (num>=10000){
                BigDecimal decimal=new BigDecimal(num);
              return   decimal.divide(new BigDecimal(10000),1, RoundingMode.UP).toString()+"w";

            }
        }
        return play_num;
    }

    public int getSta() {
        return sta;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public void setSta(int sta) {
        this.sta = sta;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setPlay_num(String play_num) {
        this.play_num = play_num;
    }

    public long getPlay_time() {
        return play_time;
    }

    public void setPlay_time(long play_time) {
        this.play_time = play_time;
    }

    public int getIndexNum() {
        return indexNum;
    }

    public void setIndexNum(int indexNum) {
        this.indexNum = indexNum;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(title);
        parcel.writeInt(type);
        parcel.writeInt(indexNum);
        parcel.writeString(update_time);
        parcel.writeString(image);
        parcel.writeString(date);
        parcel.writeString(play_num);
        parcel.writeLong(play_time);
        parcel.writeInt(down_num);
        parcel.writeInt(like_num);
        parcel.writeInt(user_movie_type);
        parcel.writeInt(sta);
        parcel.writeString(price);
        parcel.writeString(size);
        parcel.writeString(if4k);
        parcel.writeString(is_share);
    }


    @Override
    public String toString() {
        return "MyVideoBean{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", image='" + image + '\'' +
                ", type=" + type +
                ", VFType=" + VFType +
                ", indexNum=" + indexNum +
                ", update_time='" + update_time + '\'' +
                ", date='" + date + '\'' +
                ", play_num='" + play_num + '\'' +
                ", play_time=" + play_time +
                ", down_num=" + down_num +
                ", like_num=" + like_num +
                ", user_movie_type=" + user_movie_type +
                ", sta=" + sta +
                ", price='" + price + '\'' +
                ", if4k='" + if4k + '\'' +
                ", is_share='" + is_share + '\'' +
                ", size='" + size + '\'' +
                '}';
    }
}
