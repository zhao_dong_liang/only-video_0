package cn.wu1588.dancer.my_video.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bilibili.boxing.Boxing;
import com.bilibili.boxing.model.config.BoxingConfig;
import com.bilibili.boxing.model.entity.BaseMedia;
import com.bilibili.boxing_impl.ui.BoxingActivity;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.cnsunrun.commonui.widget.button.RoundButton;
import com.kongzue.titlebar.TitleBar;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.language.LanguageConfig;
import com.luck.picture.lib.listener.OnResultCallbackListener;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.interfaces.OnSelectListener;
import com.maning.mndialoglibrary.MProgressBarDialog;
import com.my.toolslib.FileUtils;
import com.my.toolslib.StringUtils;
import com.shuyu.gsyvideoplayer.video.StandardGSYVideoPlayer;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;
import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.sunrun.sunrunframwork.utils.EmptyDeal;
import com.unistrong.yang.zb_permission.ZbPermission;
import com.unistrong.yang.zb_permission.ZbPermissionFail;
import com.unistrong.yang.zb_permission.ZbPermissionSuccess;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.common.util.GlideEngine;
import cn.wu1588.dancer.common.util.OssService;
import cn.wu1588.dancer.common.util.RecycleHelper;
import cn.wu1588.dancer.common.util.UpLoadUtils;
import cn.wu1588.dancer.common.util.VideoThumbnailUtils;
import cn.wu1588.dancer.common.widget.ClearEditText;
import cn.wu1588.dancer.my_video.adapter.UpVideoV3Adapter;
import cn.wu1588.dancer.my_video.bean.LabeBean;

import static cn.wu1588.dancer.common.config.Const.PERMISSION;

/**
 * Created by admin on 2021/2/26 .
 * With cn.wu1588.dancer.my_video.activity
 */
public class UpVideosV3Activity extends LBaseActivity implements BaseQuickAdapter.OnItemChildClickListener, UpLoadUtils.UpLoadCallBack {

    @BindView(R.id.title_bars)
    TitleBar titleBar;
    @BindView(R.id.add_bt)
    Button add_bt;
    @BindView(R.id.video_player)
    StandardGSYVideoPlayer video_player;
    @BindView(R.id.thumbnail_layout)
    LinearLayout thumbnail_layout;
    @BindView(R.id.frame_01)
    FrameLayout frame_01;
    @BindView(R.id.frame_02)
    FrameLayout frame_02;
    @BindView(R.id.frame_03)
    FrameLayout frame_03;
    @BindView(R.id.frame_04)
    FrameLayout frame_04;
    @BindView(R.id.frame_05)
    FrameLayout frame_05;
    @BindView(R.id.frame_06)
    FrameLayout frame_06;
    @BindView(R.id.rb_chong_xuan)
    RoundButton rb_chong_xuan;
    @BindView(R.id.rb_shang_chuan)
    RoundButton rb_shang_chuan;
    @BindView(R.id.rb_jie_qu)
    RoundButton rb_jie_qu;
    @BindView(R.id.title_edit)
    EditText title_edit;
    @BindView(R.id.text_num)
    TextView text_num;
    @BindView(R.id.dec_edit)
    EditText dec_edit;
    @BindView(R.id.dec_num)
    TextView dec_num;
    @BindView(R.id.type_layout)
    LinearLayout type_layout;
    @BindView(R.id.labe_layout)
    LinearLayout labe_layout;
    @BindView(R.id.jiaoxue_layout)
    LinearLayout jiaoxue_layout;
    @BindView(R.id.teachingVideoRG)
    RadioGroup teachingVideoRG;
//    @BindView(R.id.teachingVideoRG)
//    RadioButton rb_teaching_false;
//    @BindView(R.id.teachingVideoRG)
//    RadioButton rb_teaching_true;

    @BindView(R.id.originalRG)
    RadioGroup originalRG;
    @BindView(R.id.rb_original_false)
    RadioButton rb_original_false;
    @BindView(R.id.rb_original_true)
    RadioButton rb_original_true;

    @BindView(R.id.shareRG)
    RadioGroup shareRG;
    @BindView(R.id.rb_share_false)
    RadioButton rb_share_false;
    @BindView(R.id.rb_share_true)
    RadioButton rb_share_true;

    @BindView(R.id.is4kRG)
    RadioGroup is4kRG;
    @BindView(R.id.rb_is4k_false)
    RadioButton rb_is4k_false;
    @BindView(R.id.rb_is4k_true)
    RadioButton rb_is4k_true;

    @BindView(R.id.yuanchuang_layout)
    LinearLayout yuanchuang_layout;
    @BindView(R.id.shares_layout)
    LinearLayout shares_layout;
    @BindView(R.id.is4k_layout)
    LinearLayout is4k_layout;
    @BindView(R.id.red_tip_layout)
    LinearLayout red_tip_layout;

    @BindView(R.id.download_layout)
    LinearLayout download_layout;
    @BindView(R.id.lianti_layout)
    LinearLayout lianti_layout;
    @BindView(R.id.jieye_layout)
    LinearLayout jieye_layout;
    @BindView(R.id.shoujia_layout)
    LinearLayout shoujia_layout;
    @BindView(R.id.tiji_layout)
    LinearLayout tiji_layout;
    @BindView(R.id.bofang_layout)
    LinearLayout bofang_layout;
    @BindView(R.id.jietu_layout_ll)
    LinearLayout jietu_layout_ll;

    @BindView(R.id.linear_view11)
    View linear_view11;
    @BindView(R.id.linear_view12)
    View linear_view12;
    @BindView(R.id.linear_view13)
    View linear_view13;
    @BindView(R.id.linear_view14)
    View linear_view14;
    @BindView(R.id.linear_view15)
    View linear_view15;
    @BindView(R.id.linear_view16)
    View linear_view16;

    @BindView(R.id.download_edit)
    ClearEditText download_edit;
    @BindView(R.id.tiqu_edit2)
    ClearEditText tiqu_edit2;
    @BindView(R.id.jieya_edit3)
    ClearEditText jieya_edit3;
    @BindView(R.id.money_edit4)
    ClearEditText money_edit4;
    @BindView(R.id.tiji_edit5)
    ClearEditText tiji_edit5;
    @BindView(R.id.shichang_edit6)
    ClearEditText shichang_edit6;
    @BindView(R.id.rb_fenbianlv)
    RoundButton rb_fenbianlv;
    @BindView(R.id.rb_tujing)
    RoundButton rb_tujing;
    @BindView(R.id.rv_jietu)
    RecyclerView rv_jietu;
    @BindView(R.id.bt_commit)
    Button bt_commit;

    @BindView(R.id.type_tv)
    TextView type_tv;
    @BindView(R.id.labe_tv)
    TextView labe_tv;

    private final int REQUEST_CODE = 1;
    private boolean isVertical;//是否是竖直视频
    private VideoThumbnailUtils videoThumbnailUtils;
    private boolean isDebug = true;//是否测试

    private int is_teaching_video;//是否教学视频，0否，1是
    private int is_original_video;//是否原创视频，0否，1是
    private int is_share_video;//是否分享视频，0否，1是
    private int is_4k_video;//是否4K视频，0否，1是

    private List<LocalMedia> selectList = new ArrayList<>();
    private UpVideoV3Adapter upVideoV3Adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setDark(CommonApp.getApplication().isBarTvColor());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_up_videos_3);
        initViews();
    }

    private void initViews() {

        video_player.getBackButton().setVisibility(View.GONE);
        video_player.getStartButton().setVisibility(View.GONE);
        video_player.getFullscreenButton().setVisibility(View.GONE);

        //支付方式rv
        RecycleHelper.setLinearLayoutManager(rv_jietu, LinearLayoutManager.HORIZONTAL);
        upVideoV3Adapter = new UpVideoV3Adapter();
        rv_jietu.setAdapter(upVideoV3Adapter);
        upVideoV3Adapter.setOnItemChildClickListener(this);

        if (videoPath == null) {
            thumbnail_layout.setVisibility(View.GONE);
        } else {
            thumbnail_layout.setVisibility(View.VISIBLE);
        }

        if (rb_is4k_false.isChecked()) {
            hideOnShow(0);
        } else {
            hideOnShow(1);
        }


        videoThumbnailUtils = new VideoThumbnailUtils(that, new VideoThumbnailUtils.VideoThumbnailCall() {
            @Override
            public void getVideoThumbnailResult(ArrayList<Bitmap> bitmaps) {
                if (bitmaps != null && bitmaps.size() > 0) {
                    if (isStandard()) {
                        thumbnail_layout.setVisibility(View.VISIBLE);
                        setThumbImageView(videoThumbnailUtils.getFristThumbnail());
                        setThumbViews(bitmaps);
                        add_bt.setVisibility(View.GONE);
                        video_player.getStartButton().setVisibility(View.VISIBLE);
                    } else {
                        add_bt.setVisibility(View.VISIBLE);
                        video_player.getStartButton().setVisibility(View.GONE);
                        ToastUtils.longToast("该视频不符合规范");
                    }
                }
            }
        });

        title_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String s = charSequence.toString();
                text_num.setText("不超过30个字（" + s.length() + "/30）");
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        dec_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int length = charSequence.length();
                dec_num.setText("(" + length + "/100)");
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        teachingVideoRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_teaching_false) {
                    is_teaching_video = 0;
                } else {
                    is_teaching_video = 1;
                }
            }
        });

        originalRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_original_false) {
                    is_original_video = 0;
                } else {
                    is_original_video = 1;
                }
            }
        });

        shareRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_share_false) {
                    is_share_video = 0;
                } else {
                    is_share_video = 1;
                }
            }
        });

        is4kRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_is4k_false) {
                    is_4k_video = 0;
                    hideOnShow(is_4k_video);
                } else {
                    is_4k_video = 1;
                    hideOnShow(is_4k_video);
                }
            }
        });
    }

    private void hideOnShow(int value) {
        if (value == 0) {
            red_tip_layout.setVisibility(View.GONE);
            download_layout.setVisibility(View.GONE);
            lianti_layout.setVisibility(View.GONE);
            jieye_layout.setVisibility(View.GONE);
            shoujia_layout.setVisibility(View.GONE);
            tiji_layout.setVisibility(View.GONE);
            bofang_layout.setVisibility(View.GONE);
            jietu_layout_ll.setVisibility(View.GONE);

            linear_view11.setVisibility(View.GONE);
            linear_view12.setVisibility(View.GONE);
            linear_view13.setVisibility(View.GONE);
            linear_view14.setVisibility(View.GONE);
            linear_view15.setVisibility(View.GONE);
            linear_view16.setVisibility(View.GONE);

            rv_jietu.setVisibility(View.GONE);

        } else {
            red_tip_layout.setVisibility(View.VISIBLE);
            download_layout.setVisibility(View.VISIBLE);
            lianti_layout.setVisibility(View.VISIBLE);
            jieye_layout.setVisibility(View.VISIBLE);
            shoujia_layout.setVisibility(View.VISIBLE);
            tiji_layout.setVisibility(View.VISIBLE);
            bofang_layout.setVisibility(View.VISIBLE);
            jietu_layout_ll.setVisibility(View.VISIBLE);

            linear_view11.setVisibility(View.VISIBLE);
            linear_view12.setVisibility(View.VISIBLE);
            linear_view13.setVisibility(View.VISIBLE);
            linear_view14.setVisibility(View.VISIBLE);
            linear_view15.setVisibility(View.VISIBLE);
            linear_view16.setVisibility(View.VISIBLE);

            rv_jietu.setVisibility(View.VISIBLE);
        }
    }

    //设置视频封面
    private void setThumbImageView(Bitmap bitmap) {
//        select_img.setImageBitmap(bitmap);
        ImageView imageView = new ImageView(that);
//        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setImageBitmap(bitmap);
        video_player.setThumbImageView(imageView);
    }

    //布局封面view
    private void setThumbViews(ArrayList<Bitmap> bitmaps) {
        for (int i = 0; i < thumbnail_layout.getChildCount(); i++) {
            Bitmap bitmap = bitmaps.get(i);
            FrameLayout frameLayout = (FrameLayout) thumbnail_layout.getChildAt(i);
            ImageView imageView = (ImageView) frameLayout.getChildAt(0);
            imageView.setImageBitmap(bitmap);
        }
    }


    @OnClick({R.id.add_bt, R.id.rb_chong_xuan, R.id.type_layout, R.id.labe_layout, R.id.rb_fenbianlv, R.id.rb_tujing, R.id.jietu_layout_ll,
            R.id.frame_01, R.id.frame_02, R.id.frame_03, R.id.frame_04, R.id.frame_05, R.id.frame_06, R.id.bt_commit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add_bt:

            case R.id.rb_chong_xuan:

                BoxingConfig videoConfig = new BoxingConfig(BoxingConfig.Mode.VIDEO);//.withVideoDurationRes(R.mipmap.add_video_icon)
                Boxing.of(videoConfig).withIntent(this, BoxingActivity.class).start(this, REQUEST_CODE);
                break;

            case R.id.rb_shang_chuan:
                //上传封面按钮
                break;

            case R.id.rb_jie_qu:
                //封面截取按钮
                break;


            case R.id.type_layout:
                CommonIntent.startTypeActivity(that, 2, isVertical);
                break;

            case R.id.labe_layout:
                // TODO: 2021/3/1 LabeActivity 有BUG
                CommonIntent.startLabeActivity(that, 1);
                break;

            case R.id.frame_01:
                selectView(frame_01, 0);
                break;
            case R.id.frame_02:
                selectView(frame_02, 1);
                break;
            case R.id.frame_03:
                selectView(frame_03, 2);
                break;
            case R.id.frame_04:
                selectView(frame_04, 3);
                break;
            case R.id.frame_05:
                selectView(frame_05, 4);
                break;
            case R.id.frame_06:
                selectView(frame_06, 5);
                break;

            case R.id.rb_fenbianlv:
                chooseResolution();
                break;

            case R.id.rb_tujing:
                chooseResolution2();
                break;


            case R.id.jietu_layout_ll:
                choosePhoto();
                break;

            case R.id.bt_commit:
                if (StringUtils.isNull(videoId)) {
                    if (StringUtils.isNull(videoPath)) {
                        ToastUtils.longToast("请先选择视频");
                        return;
                    } else {
                        String title = title_edit.getText().toString();
                        if (StringUtils.isNull(title)) {
                            ToastUtils.longToast("请输入标题");
                            return;
                        }

                        if (StringUtils.isNull(typeId)) {
                            ToastUtils.longToast("请选择分类");
                            return;
                        }

                        //如果上传视频是4K
                        if (is_4k_video == 1) {
                            String dowloadLink = download_edit.getText().toString();
                            String extractPassword = tiqu_edit2.getText().toString();
                            String videoPrice = money_edit4.getText().toString();
                            String videoSize = tiji_edit5.getText().toString();
                            String videoTime = shichang_edit6.getText().toString();

                            if (StringUtils.isNull(dowloadLink)) {
                                ToastUtils.longToast("请下载链接");
                                return;
                            }

                            if (StringUtils.isNull(videoPrice)) {
                                ToastUtils.longToast("请输入视频售价");
                                return;
                            }

                            if (StringUtils.isNull(videoSize)) {
                                ToastUtils.longToast("请输入视频大小");
                                return;
                            }

                            if (StringUtils.isNull(videoTime)) {
                                ToastUtils.longToast("请输入视频时长");
                                return;
                            }
                        }

                        initDownloadDialog();
                        UpLoadUtils upLoadUtils = new UpLoadUtils(that);
                        upLoadUtils.setUpLoadCallBack(this::upLoadResult);
                        Bitmap bitmap = videoThumbnailUtils.getBitmaps().get(selectPos);
                        if (bitmap != null) {
                            String fileName = FileUtils.saveBitmap(bitmap);
                            if (fileName != null) {
                                mProgressBarDialog.showProgress(0, "上传中");
                                upLoadUtils.upLoad(fileName);
                            }
                        }

                    }

                }

                break;

            default:
                break;
        }
    }


    /**
     * 截图
     */
    private void choosePhoto() {
        ZbPermission.with(that)
                .addRequestCode(PERMISSION)
                .permissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                .request();
    }


    @ZbPermissionSuccess(requestCode = PERMISSION)
    public void permissionSuccess() {
        //选取照片
        PictureSelector.create(that)
                .openGallery(PictureMimeType.ofImage())//相册 媒体类型 PictureMimeType.ofAll()、ofImage()、ofVideo()、ofAudio()
                .isWeChatStyle(true)
                .maxSelectNum(9)//最大选择数量,默认9张
                .isGif(false)//是否显示gif
                .rotateEnabled(false)//裁剪是否可旋转图片
                .isDragFrame(false)//是否可拖动裁剪框(固定)
                .isEnableCrop(false)//是否开启裁剪
                .cropImageWideHigh(1000, 1000)// 裁剪宽高比，设置如果大于图片本身宽高则无效
                .withAspectRatio(1000, 1000)//裁剪比例
                .freeStyleCropEnabled(true)//裁剪框是否可拖拽
                .isCompress(true)//是否压缩
                .setLanguage(LanguageConfig.CHINESE)//国际化语言 LanguageConfig.CHINESE
                .imageEngine(GlideEngine.createGlideEngine())
                .forResult(new OnResultCallbackListener<LocalMedia>() {
                    @Override
                    public void onResult(List<LocalMedia> result) {

                        initAdapter(result);

                    }

                    @Override
                    public void onCancel() {

                    }
                });
    }

    private void initAdapter(List<LocalMedia> result) {
        selectList.clear();
        selectList.addAll(result);
//        for (int i = 0; i < selectList.size(); i++) {
//            LogUtils.eTag("zjl",selectList.get(i).getRealPath());
//        }
        upVideoV3Adapter.setNewData(selectList);

    }

    /**
     * 选择分辨率
     */
    int caoNima = 0;

    private void chooseResolution() {
        new XPopup.Builder(this)
                .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                .asCenterList("请选择分辨率", new String[]{"3840*1080", "3840*2160", "4096*2160"},
                        null, caoNima,
                        new OnSelectListener() {
                            @Override
                            public void onSelect(int position, String text) {
//                                ToastUtils.shortToast("click " + text);
                                caoNima = position;
                                rb_fenbianlv.setText(text);
                            }
                        })
                .show();
    }

    int chooseWp = 0;

    private void chooseResolution2() {
        new XPopup.Builder(this)
                .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                .asCenterList("请选择网盘", new String[]{"百度网盘", "腾讯网盘", "其他"},
                        null, chooseWp,
                        new OnSelectListener() {
                            @Override
                            public void onSelect(int position, String text) {
//                                ToastUtils.shortToast("click " + text);
                                chooseWp = position;
                                rb_tujing.setText(text);
                            }
                        })
                .show();
    }

    private View oldView;
    private int selectPos;

    private void selectView(FrameLayout frameLayout, int pos) {
        if (oldView == null) {
            oldView = frame_01.getChildAt(1);
        }
        selectPos = pos;
        oldView.setBackgroundColor(Color.TRANSPARENT);
        View childAt = frameLayout.getChildAt(1);
        childAt.setBackgroundResource(R.drawable.white_rect);
        oldView = childAt;
        Bitmap bitmap = videoThumbnailUtils.getBitmaps().get(pos);
//        if (bitmap != null) {
//            select_img.setImageBitmap(bitmap);
//        }
    }

    private String videoPath, image, videoId;
    private String typeId;//分类
    private String labe;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            final ArrayList<BaseMedia> medias = Boxing.getResult(data);
            BaseMedia baseMedia = medias.get(0);
            videoPath = baseMedia.getPath();//视频path
            video_player.setUp(videoPath, false, "");
            Log.i("OkGo:path==", baseMedia.getPath() + ", " + baseMedia.getId());
            String[] paths = baseMedia.getPath().split("/");
            String videoName = paths[paths.length - 1];
            title_edit.setText(videoName);
            title_edit.setSelection(0, videoName.length());

            videoThumbnailUtils.getVideoThumbnail(baseMedia.getPath());
            video_player.getStartButton().setVisibility(View.GONE);
        }

        if (data == null) {
            return;
        }

        if (requestCode == 2) {//分类
            String type = data.getStringExtra("type");
            typeId = data.getStringExtra("id");
            type_tv.setText(type);
        } else if (requestCode == 1) {//标签
            if (data != null) {
                ArrayList<LabeBean> labeBeans = data.getParcelableArrayListExtra("list");
                if (!EmptyDeal.empty(labeBeans)) {
                    String s = "";
                    String l = "";
                    for (int i = 0; i < labeBeans.size(); i++) {
                        if (i < labeBeans.size() - 1) {
                            s = s + labeBeans.get(i).getTitle() + " ";
                            l = l + labeBeans.get(i).getId() + ",";
                        } else {
                            s = " " + s + labeBeans.get(i).getTitle();
                            l = "," + l + labeBeans.get(i).getId();
                        }
                    }
                    labe_tv.setText(s);
                    labe = l;
                }

                Log.i("", "");
            }
        }

    }


    //判断视频是否规范
    private boolean isStandard() {
        String videoVode = videoThumbnailUtils.getVideoVode();
        int video_height = videoThumbnailUtils.getVideo_height();
        int video_width = videoThumbnailUtils.getVideo_width();
        if (!StringUtils.isNull(videoVode)) {
            if ("h265".equals(videoVode) || "H265".equals(videoVode) || "hevc".equals(videoVode)) {
                if ((video_height == 854 && video_width == 480) || (video_height == 480 && video_width == 854)) {
                    if (video_height > video_width) {
                        isVertical = true;
                    } else {
                        isVertical = false;
                    }
                    return true;
                }

            }
        }
        if (isDebug) {
            if (video_height > video_width) {
                isVertical = true;
            } else {
                isVertical = false;
            }
            return true;
        }
        return false;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (handler != null) {
            handler.removeCallbacks(null);
        }
        videoThumbnailUtils.close();
    }

    private MProgressBarDialog mProgressBarDialog;

    private void initDownloadDialog() {
        //新建一个Dialog
        mProgressBarDialog = new MProgressBarDialog.Builder(this)
                //全屏模式
                .isWindowFullscreen(true)
                .setStyle(MProgressBarDialog.MProgressBarDialogStyle_Circle)
                //全屏背景窗体的颜色
                .setBackgroundWindowColor(Color.TRANSPARENT)
                //View背景的颜色
                .setBackgroundViewColor(Color.BLACK)
                //字体的颜色
                .setTextColor(Color.WHITE)
                //View边框的颜色
                .setStrokeColor(Color.TRANSPARENT)
                //View边框的宽度
                .setStrokeWidth(2)
                //View圆角大小
                .setCornerRadius(10)
                //ProgressBar背景色
                .setProgressbarBackgroundColor(Color.BLACK)
                //ProgressBar 颜色
                .setProgressColor(Color.WHITE)
                //圆形内圈的宽度
                .setCircleProgressBarWidth(4)
                //圆形外圈的宽度
                .setCircleProgressBarBackgroundWidth(4)
                //水平进度条Progress圆角
                .setProgressCornerRadius(0)
                //水平进度条的高度
                .setHorizontalProgressBarHeight(10)
                //dialog动画
//                .setAnimationID(R.style.animate_dialog_custom)
                .build();
    }

    @Override
    public void upLoadResult(String url, File file) {
        image = url;
        OssService ossService = new OssService();
        ossService.startUpload(videoPath, "VOD_NO_TRANSCODE", url, dec_edit.getText().toString(), title_edit.getText().toString(), handler);
    }

//  TODO 视频上传接口  https://www.showdoc.com.cn/1116882694352789?page_id=6021054562884087

    @SuppressLint("HandlerLeak")
    Handler handler = new Handler() {
        @SuppressLint("HandlerLeak")
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 1) {
                int pro = (int) msg.obj;
                mProgressBarDialog.showProgress(pro, "已上传" + pro + "%", true);
            } else if (msg.what == 2) {//上传视频成功
//                String title = title_edit.getText().toString();
//                String gold = gold_edit.getText().toString();
//                String dec = dec_edit.getText().toString();
//                videoId = (String) ((Map) msg.obj).get("videoId");
//                BaseQuestStart.addVideo(UpVideosV3Activity.this, mProgressBarDialog, title, image, videoId, labe, gold, user_movie_type, dec, typeId);
            } else if (msg.what == 3) {//上传视频失败
                mProgressBarDialog.dismiss();
                ToastUtils.longToast("视频上传失败，请重试");
            }
        }
    };


    @ZbPermissionFail(requestCode = PERMISSION)
    public void permissionFail() {
        UIUtils.shortM("请在设置里面打开应用使用相机的权限");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        ZbPermission.onRequestPermissionsResult(UpVideosV3Activity.this, requestCode, permissions, grantResults);
    }

    @Override
    public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        switch (view.getId()) {
            case R.id.ll_del:
                selectList.remove(position);
                upVideoV3Adapter.notifyDataSetChanged();
                break;
        }
    }
}

