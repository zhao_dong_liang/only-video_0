package cn.wu1588.dancer.my_video.activity;

import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.my.toolslib.http.utils.LzyResponse;

import java.util.List;

import butterknife.BindView;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.PageLimitDelegate;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;
import cn.wu1588.dancer.my_video.adapter.AlbumVideosAdapter;
import cn.wu1588.dancer.my_video.bean.MyVideoBean;

public class MyAlbumVidesActivity extends LBaseActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.title_bar)
    TitleBar title_bar;

    private String channel_id;
    private AlbumVideosAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setDark(CommonApp.getApplication().isBarTvColor());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_album_vides);
        initViews();
    }
    PageLimitDelegate<MyVideoBean> pageLimitDelegate=new PageLimitDelegate<>(new PageLimitDelegate.DataProvider() {
        @Override
        public void loadData(int page) {
            BaseQuestStart.getChannelMovies(MyAlbumVidesActivity.this,channel_id);
        }
    });
    private void initViews(){
        title_bar.setTitleBarBackgroundDrawable(CommonApp.getApplication().getStateBarDrawable());
        channel_id=getIntent().getStringExtra("channel_id");
        adapter=new AlbumVideosAdapter();
        recyclerView.setLayoutManager(new LinearLayoutManager(that));
        recyclerView.setAdapter(adapter);
        pageLimitDelegate.attach(null,recyclerView,adapter);
        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                MyVideoBean bean= (MyVideoBean) adapter.getItem(position);
                CommonIntent.startMoviePlayerActivity(that,bean.getId());
            }
        });
    }

    @Override
    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
        super.nofityUpdateUi(requestCode, response, view);
        if (requestCode==BaseQuestStart.GET_CHANNEL_MOVIES_CODE){
           List<MyVideoBean> videoBeans= (List<MyVideoBean>) response.data;
           pageLimitDelegate.setData(videoBeans);
        }
    }
}
