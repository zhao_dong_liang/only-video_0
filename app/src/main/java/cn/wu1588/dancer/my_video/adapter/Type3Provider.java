package cn.wu1588.dancer.my_video.adapter;

import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.provider.BaseItemProvider;
import com.my.toolslib.DateUtil;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.my_video.bean.MyVideoBean;

//单行类型view
public class Type3Provider extends BaseItemProvider<MyVideoBean, BaseViewHolder> {
    @Override
    public int viewType() {
        return MyVideoAdapter.TYPE_CODE;
    }

    @Override
    public int layout() {
        return R.layout.item_album_adp;
    }

    @Override
    public void convert(BaseViewHolder helper, MyVideoBean data, int position) {
        String strBySecond = DateUtil.getTimeStrBySecond(data.getPlay_time());
        helper
                .setText(R.id.time_tv, data.getDate())
                .setText(R.id.text_title, data.getTitle())
                .setText(R.id.time_tv, strBySecond + "\t\t" + (data.getPlay_num() == null ? 0 : data.getPlay_num()) + "个视频\t\t售价\t\t¥" + (data.getPrice() == null ? 0 : data.getPrice()))
                .addOnClickListener(R.id.image_delete);
    }


}
