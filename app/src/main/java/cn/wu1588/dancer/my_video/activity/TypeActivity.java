package cn.wu1588.dancer.my_video.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.my.toolslib.http.utils.LzyResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;
import cn.wu1588.dancer.my_video.adapter.TypeAdapter;
import cn.wu1588.dancer.my_video.bean.TypeBean;

public class TypeActivity extends LBaseActivity {
    @BindView(R.id.title_bar)
    TitleBar title_bar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private TypeAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setDark(CommonApp.getApplication().isBarTvColor());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_type);
        initViews();
    }
    private void initViews(){
        title_bar.setTitleBarBackgroundDrawable(CommonApp.getApplication().getStateBarDrawable());
        adapter=new TypeAdapter(null);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                TypeBean.HomeCategory item = (TypeBean.HomeCategory) adapter.getItem(position);
                view.findViewById(R.id.check_icon).setVisibility(View.VISIBLE);
                Intent intent=new Intent();
                intent.putExtra("id",item.id+"");
                intent.putExtra("type",item.title);
                setResult(1,intent);
                finish();
            }
        });
        BaseQuestStart.getVideoTypeList(this,that);
    }

    @Override
    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
        super.nofityUpdateUi(requestCode, response, view);
        if (response.code==1){
            TypeBean typeBean= (TypeBean) response.data;
            boolean isVertical = getIntent().getBooleanExtra("isVertical", false);
            if (isVertical){
                List<TypeBean.HomeCategory> categories=new ArrayList<>();
                for (TypeBean.HomeCategory category : typeBean.home_category
                     ) {
                    if (category.id== CommonApp.samllVideoId){
                        categories.add(category);
                        adapter.setNewData(categories);
                        return;
                    }
                }
            }else {
                adapter.setNewData(typeBean.home_category);
            }

        }
    }
}
