package cn.wu1588.dancer.my_video.adapter;

import android.widget.ImageView;

import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.provider.BaseItemProvider;
import com.my.toolslib.DateUtil;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.my_video.bean.MyVideoBean;

//单行类型view
public class ItemProvider extends BaseItemProvider<MyVideoBean, BaseViewHolder> {
    @Override
    public int viewType() {
        return MyVideoAdapter.ITEM_CODE;
    }

    @Override
    public int layout() {
        return R.layout.my_video_item;
    }

    @Override
    public void convert(BaseViewHolder helper, MyVideoBean data, int position) {
        ImageView imageView= helper.getView(R.id.img);
        GlideMediaLoader.load(mContext,imageView,data.getImage(),R.mipmap.img_def);
        long play_time = data.getPlay_time();
        String strBySecond = DateUtil.getTimeStrBySecond(play_time);
        helper.setText(R.id.title_tv,data.getTitle())
        .setText(R.id.see_num,data.getPlay_num())
        .setText(R.id.video_time,strBySecond);
    }
}
