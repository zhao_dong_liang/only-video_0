package cn.wu1588.dancer.my_video.adapter;

import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.luck.picture.lib.entity.LocalMedia;

import java.util.List;

import androidx.annotation.Nullable;
import cn.wu1588.dancer.R;

/**
 * Created by zhou on 2021/3/2 15:33.
 */
public class UpVideoV3Adapter extends BaseQuickAdapter<LocalMedia, BaseViewHolder> {

    public UpVideoV3Adapter() {
        super(R.layout.adapter_up_video_v3);
    }

    @Override
    protected void convert(BaseViewHolder helper, LocalMedia item) {

        Glide.with(mContext)
                .load(item.getRealPath()).into((ImageView) helper.getView(R.id.upvideo_fiv));
        helper.addOnClickListener(R.id.upvideo_fiv);
        helper.addOnClickListener(R.id.ll_del);
    }
}
