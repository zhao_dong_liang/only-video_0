package cn.wu1588.dancer.my_video.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.my.toolslib.http.utils.LzyResponse;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;

import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.quest.BaseQuestStart;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.SEND_SMS_CODE_CODE;

public class ResetVideosPwdActivity extends LBaseActivity {
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.tv_get_code)
    TextView tvGetCode;
    @BindView(R.id.bt_modify)
    Button btModify;
    @BindView(R.id.et_code)
    EditText etCode;
    @BindView(R.id.et_password)
    EditText etPassword;

    /**
     * 计时器
     */
    private TimeCount timer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_pwd);
        initViews();
    }

    private void initViews(){

    }
    @OnClick({R.id.tv_get_code,R.id.bt_modify})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.tv_get_code:
                String etphone = etPhone.getText().toString().trim();
                if (etphone.equals("")) {
                    ToastUtils.shortToast("请输入手机号");
                    return;
                }else if (!etphone.matches("^((13[0-9])|(15[^4,\\D])|(18[0,1-9])|(17[0-9])|(14[0-9])|(19[0-9])|(16[0-9]))\\d{8}$")) {
                    ToastUtils.shortToast("请输入正确的手机格式");
                    return;
                }
                //获取验证码接口
                BaseQuestStart.getCode(etphone, this);
                break;
            case R.id.bt_modify:
                String phone = etPhone.getText().toString().trim();
                String etcode = etCode.getText().toString().trim();
                String password = etPassword.getText().toString().trim();
                if (TextUtils.isEmpty(phone)){
                    ToastUtils.shortToast("请输入手机号");
                    return;
                }
                if (!phone.matches("^((13[0-9])|(15[^4,\\D])|(18[0,1-9])|(17[0-9])|(14[0-9])|(19[0-9])|(16[0-9]))\\d{8}$")) {
                    ToastUtils.shortToast("请输入正确的手机格式");
                    return;
                }
                if (TextUtils.isEmpty(etcode)){
                    ToastUtils.shortToast("请输入验证码");
                    return;
                }
                if (TextUtils.isEmpty(password)){
                    ToastUtils.shortToast("请重置登录密码");
                    return;
                }
                if (password.length()<6){
                    ToastUtils.shortToast("请设置至少6个数字或字母");
                    return;
                }
                BaseQuestStart.setSecretPwd(this,phone,etcode,password);
                break;
        }
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode) {
            case SEND_SMS_CODE_CODE:
                if (bean.status == 1) {
                    timer = new TimeCount(60000, 1000);
                    timer.start();
                }else {
                    ToastUtils.shortToast(bean.msg);
                }
                break;
        }
        super.nofityUpdate(requestCode, bean);
    }

    @Override
    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
        super.nofityUpdateUi(requestCode, response, view);
        if (requestCode==BaseQuestStart.SET_SECRET_CODE){//重置密码
            if (response.code==1){
                ToastUtils.longToast("设置成功");
                setResult(1,new Intent());
                finish();
            }else {
                ToastUtils.longToast("设置失败");
            }
        }
    }

    /**
     * 倒计时
     */
    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            /*
             * millisInFuture 总时长,countDownInterval 计时的时间间隔
             */
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            /**
             * 计时完毕时触发
             */
            tvGetCode.setText("获取验证码");
            tvGetCode.setClickable(true);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            /**
             * 计时过程显示
             */
            tvGetCode.setClickable(false);
            tvGetCode.setText(millisUntilFinished / 1000 + "秒");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer!=null) timer.cancel();
    }
}
