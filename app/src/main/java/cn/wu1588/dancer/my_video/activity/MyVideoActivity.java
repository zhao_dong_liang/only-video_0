package cn.wu1588.dancer.my_video.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.flyco.tablayout.SlidingTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.my.toolslib.http.utils.LzyResponse;
import com.tencent.mmkv.MMKV;

import java.util.ArrayList;

import butterknife.BindView;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.my_video.fragment.MyVideoFragment;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.enums.MyVideoTypeEnum;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;
import cn.wu1588.dancer.mine.fragment.BuyAlbumFragment;

public class MyVideoActivity extends LBaseActivity {
    @BindView(R.id.title_bar)
    TitleBar title_bar;
    @BindView(R.id.tab_layout)
    SlidingTabLayout tab_layout;
    @BindView(R.id.view_pager)
    ViewPager view_pager;


    private boolean check_is_secret;//是否设置过密码
    private boolean validate_secret;//验证密码是否正确
    String [] titles=new String[]{
            "公开视频",
            "收费视频",
            "我的专辑",
            "私密视频"
    };
    private boolean checkIsSecret=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setDark(CommonApp.getApplication().isBarTvColor());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_video);

        initViews();
        BaseQuestStart.checkIsSecret(this);
    }

    private void initViews(){
        MMKV mmkv = MMKV.defaultMMKV();
        int[] colors = {Color.parseColor(mmkv.decodeString("up_b_color","#000000")),
                Color.parseColor(mmkv.decodeString("up_e_color","#000000"))
        };
        GradientDrawable drawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT,colors);
        drawable.setGradientType(GradientDrawable.LINEAR_GRADIENT);//设置线性渐变
        drawable.setOrientation(GradientDrawable.Orientation.LEFT_RIGHT);//设置渐变方向
        tab_layout.setBackground(drawable);
        title_bar.setTitleBarBackgroundDrawable(CommonApp.getApplication().getStateBarDrawable());
        ArrayList<Fragment> fragments=new ArrayList<>();
        fragments.add(new MyVideoFragment().newInstance(MyVideoTypeEnum.PUBLIC_TYPE.getCode()));//公开视频
        fragments.add(new MyVideoFragment().newInstance(MyVideoTypeEnum.CHARGE_TYPE.getCode()));//收费视频
        fragments.add(new BuyAlbumFragment().newInstance(MyVideoTypeEnum.ALBUM_TYPE.getCode()));//我的专辑
        fragments.add(new MyVideoFragment().newInstance(MyVideoTypeEnum.PRIVATE_TYPE.getCode()));//私密视频
        tab_layout.setViewPager(view_pager,titles,this,fragments);
        tab_layout.setNoClickPostion(3);
        tab_layout.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                if (position==3&&!validate_secret){
                    CommonIntent.startPrivateVideosActivity(that,check_is_secret);
                }
            }
            @Override
            public void onTabReselect(int position) {

            }
        });
    }

    @Override
    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
        super.nofityUpdateUi(requestCode, response, view);
        if (requestCode==BaseQuestStart.CHECK_IS_SECRET_CODE){//是否设置过私密视频密码
            if (response.code==1){
                check_is_secret=true;
            }else {
                check_is_secret=false;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==1&&data!=null){
            check_is_secret= data.getBooleanExtra("check_is_secret",false);
            validate_secret=data.getBooleanExtra("validate_secret",false);
            if (validate_secret){
                tab_layout.setNoClickPostion(-1);
                tab_layout.setCurrentTab(3,false);
            }
        }
    }
}
