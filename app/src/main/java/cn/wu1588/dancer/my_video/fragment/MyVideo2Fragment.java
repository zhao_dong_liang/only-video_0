package cn.wu1588.dancer.my_video.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.my.toolslib.http.utils.LzyResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.PageLimitDelegate;
import cn.wu1588.dancer.my_video.adapter.MyVideo2Adapter;
import cn.wu1588.dancer.my_video.adapter.MyVideoAdapter;
import cn.wu1588.dancer.my_video.bean.MyVideoBean;
import cn.wu1588.dancer.my_video.bean.MyVideoData;

//我的视频fragment
public class MyVideo2Fragment extends LBaseFragment {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.num_tv)
    TextView num_tv;
    @BindView(R.id.text_original_application)
    TextView text_original_application;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    private boolean isFirstLoadData = true;
    @BindView(R.id.flayout_null)
    FrameLayout flayout_null;
    private int type;//   0"全部",
    //                        1"小视频",
//                        2"4K",
//                        3"教学",
//                        4"专辑",
//                        5"分享视频"
    private MyVideo2Adapter adapter;

    public MyVideo2Fragment newInstance(int type) {
        MyVideo2Fragment fragment = new MyVideo2Fragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.myy_video_fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }

    private void initViews() {
        type = getArguments().getInt("type");

        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new MyVideo2Adapter();
        recyclerView.setAdapter(adapter);
        adapter.setSpanSizeLookup((gridLayoutManager, i) -> {
            MyVideoBean item = adapter.getItem(i);
            int type = item.getType();
            if (type == 1) {
                return 3;
            } else {
                return 1;
            }
        });
        adapter.setOnItemClickListener((adapter, view, position) -> {
            MyVideoBean bean = (MyVideoBean) adapter.getItem(position);
            CommonIntent.startMoviePlayerActivity(that, bean.getId());
        });
        pageLimitDelegate.attach(refreshLayout, recyclerView, adapter);

    }

    private PageLimitDelegate<MyVideoBean> pageLimitDelegate = new PageLimitDelegate<>(new PageLimitDelegate.DataProvider() {
        @Override
        public void loadData(int page) {
            BaseQuestStart.getMineVideos(MyVideo2Fragment.this, type, page, 15);
        }
    });

    @Override
    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
        super.nofityUpdateUi(requestCode, response, view);
        if (response.code == 1) {
            isFirstLoadData = false;
            if (requestCode == BaseQuestStart.GET_MINE_VIDEOS_CODE) {
                MyVideoData data = (MyVideoData) response.data;
                num_tv.setText("作品\t" + data.count);
                ArrayList<MyVideoBean> myVideoBeans = (ArrayList<MyVideoBean>) data.movies;
                pageLimitDelegate.setData(groupMyVideoBeansByUpdateDate(myVideoBeans));
                flayout_null.setVisibility(myVideoBeans.size() == 0 ? View.VISIBLE : View.GONE);
            } else {
                num_tv.setText("作品\t0");
            }
        }
    }

    private List<MyVideoBean> groupMyVideoBeansByUpdateDate(ArrayList<MyVideoBean> myVideoBeans) {
        List<MyVideoBean> results = new ArrayList<>();

        if (myVideoBeans != null) {
            Map<String, List<MyVideoBean>> groupsByUpdateDate = new HashMap<>();
            for (MyVideoBean video : myVideoBeans) {
                String key = video.getUpdate_time().split(" ")[0].replace("-", ".");
                video.setType(2);
                List<MyVideoBean> videoBeanList = groupsByUpdateDate.get(key);
                if (videoBeanList == null) {
                    videoBeanList = new ArrayList<>();
                }
                videoBeanList.add(video);
                groupsByUpdateDate.put(key, videoBeanList);
            }

            for (Map.Entry<String, List<MyVideoBean>> entry : groupsByUpdateDate.entrySet()) {
                String key = entry.getKey();
                MyVideoBean videoBean = new MyVideoBean();
                videoBean.setDate(key);
                videoBean.setType(1);
                results.add(videoBean);
                results.addAll(entry.getValue());
            }

        }
        return results;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (!isFirstLoadData) {
            pageLimitDelegate.refreshPage();
        }

    }
}
