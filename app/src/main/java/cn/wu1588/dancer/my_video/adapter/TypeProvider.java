package cn.wu1588.dancer.my_video.adapter;

import android.view.View;

import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.provider.BaseItemProvider;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.my_video.bean.MyVideoBean;

//单行类型view
public class TypeProvider extends BaseItemProvider<MyVideoBean, BaseViewHolder> {
    @Override
    public int viewType() {
        return MyVideoAdapter.TYPE_CODE;
    }

    @Override
    public int layout() {
        return R.layout.my_video_type;
    }

    @Override
    public void convert(BaseViewHolder helper, MyVideoBean data, int position) {
        helper.getView(R.id.view_gone).setVisibility(View.VISIBLE);
        helper.getView(R.id.time_tv).setVisibility(View.VISIBLE);
        helper.setText(R.id.time_tv,data.getDate());
    }
}
