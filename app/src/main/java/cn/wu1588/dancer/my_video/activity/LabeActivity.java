package cn.wu1588.dancer.my_video.activity;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.my.toolslib.http.utils.LzyResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;
import cn.wu1588.dancer.my_video.adapter.LabeAdapter;
import cn.wu1588.dancer.my_video.bean.LabeBean;

public class LabeActivity extends LBaseActivity {
    @BindView(R.id.title_bar)
    TitleBar title_bar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private LabeAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setDark(CommonApp.getApplication().isBarTvColor());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_labe);
        initViews();
    }
    private void initViews(){
        title_bar.setTitleBarBackgroundDrawable(CommonApp.getApplication().getStateBarDrawable());
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter=new LabeAdapter(null);
        recyclerView.setAdapter(adapter);

        BaseQuestStart.getVideoLabeList(this,this);
    }

    @OnClick({R.id.enter_bt})
    public void onClick(View view){
        if (view.getId()==R.id.enter_bt){
            ArrayList<LabeBean> arrayList = adapter.getCheckArrayList();
            if (arrayList.size()>0){
                Intent intent=new Intent();
                intent.putExtra("list",arrayList);
                setResult(1,intent);
                finish();
            }
        }
    }
    @Override
    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
        super.nofityUpdateUi(requestCode, response, view);
        if (response.code==1){
            List<LabeBean> beans= (List<LabeBean>) response.data;
            adapter.setNewData(beans);

        }
    }
}
