package cn.wu1588.dancer.my_video.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class LabeBean implements Parcelable {

    private ArrayList<LabeBean> labeBeans;
    private String title;
    private int id;
    private boolean isClicked;
    private int index;

    public LabeBean(){}
    protected LabeBean(Parcel in) {
        labeBeans = in.createTypedArrayList(LabeBean.CREATOR);
        title = in.readString();
        id = in.readInt();
        isClicked = in.readByte() != 0;
        index = in.readInt();
    }

    public static final Creator<LabeBean> CREATOR = new Creator<LabeBean>() {
        @Override
        public LabeBean createFromParcel(Parcel in) {
            return new LabeBean(in);
        }

        @Override
        public LabeBean[] newArray(int size) {
            return new LabeBean[size];
        }
    };

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean isClicked() {
        return isClicked;
    }

    public void setClicked(boolean clicked) {
        isClicked = clicked;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<LabeBean> getLabeBeans() {
        return labeBeans;
    }

    public void setLabeBeans(ArrayList<LabeBean> labeBeans) {
        this.labeBeans = labeBeans;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(labeBeans);
        parcel.writeString(title);
        parcel.writeInt(id);
        parcel.writeByte((byte) (isClicked ? 1 : 0));
        parcel.writeInt(index);
    }
}
