package cn.wu1588.dancer.my_video.adapter;

import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.MultipleItemRvAdapter;

import cn.wu1588.dancer.my_video.bean.MyVideoBean;

public class MyVideoAdapter extends MultipleItemRvAdapter<MyVideoBean, BaseViewHolder> {
    public static final int TYPE_CODE = 1;
    public static final int ITEM_CODE = 2;

    public MyVideoAdapter() {
        super(null);
        finishInitialize();

    }

    @Override
    protected int getViewType(MyVideoBean myVideoBean) {
        int type = myVideoBean.getType();
        if (type == 1) {
            return TYPE_CODE;
        }else {
            return ITEM_CODE;
        }
    }

    @Override
    public void registerItemProvider() {
        mProviderDelegate.registerProvider(new TypeProvider());
        mProviderDelegate.registerProvider(new ItemProvider0());

    }
}
