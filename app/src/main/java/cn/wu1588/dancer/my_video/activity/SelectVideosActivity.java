package cn.wu1588.dancer.my_video.activity;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.my.toolslib.http.utils.LzyResponse;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.enums.MyVideoTypeEnum;
import cn.wu1588.dancer.common.quest.BaseQuestConfig;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.PageLimitDelegate;
import cn.wu1588.dancer.common.widget.LoadDialogUtils;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;
import cn.wu1588.dancer.my_video.adapter.SelectVideoAdapter;
import cn.wu1588.dancer.my_video.bean.MyVideoBean;
import cn.wu1588.dancer.my_video.bean.MyVideoData;

public class SelectVideosActivity extends LBaseActivity {
    @BindView(R.id.title_bar)
    TitleBar title_bar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;

    private SelectVideoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setDark(CommonApp.getApplication().isBarTvColor());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_videos);
        initViews();
        initData();
    }
    PageLimitDelegate<MyVideoBean> pageLimitDelegate=new PageLimitDelegate<>(new PageLimitDelegate.DataProvider() {
        @Override
        public void loadData(int page) {
            if (page==1){
                LoadDialogUtils.showDialog(SelectVideosActivity.this);
            }
            BaseQuestStart.getMineVideos(SelectVideosActivity.this, MyVideoTypeEnum.ALL_TYPE.getCode(),page,40);
        }
    });
    private void initViews(){
        title_bar.setRightListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent();
                intent.putExtra("list",selectBeans);
                setResult(1,intent);
                finish();
            }
        });
        title_bar.setTitleBarBackgroundDrawable(CommonApp.getApplication().getStateBarDrawable());
        recyclerView.setLayoutManager(new GridLayoutManager(this,3));

    }

    private int clickIndex;
    private ArrayList<MyVideoBean> selectBeans=new ArrayList<>();
    private void initData(){
        adapter=new SelectVideoAdapter();
        recyclerView.setAdapter(adapter);
       pageLimitDelegate.attach(refreshLayout,recyclerView,adapter);
        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                MyVideoBean item = (MyVideoBean) adapter.getItem(position);
                TextView textView= (TextView) adapter.getViewByPosition(position,R.id.index_tv);
                if (item.getIndexNum()==-1){
                    clickIndex++;
                    item.setIndexNum(clickIndex);
                    selectBeans.add(item);
                    textView.setVisibility(View.VISIBLE);
                }else {
                    selectBeans.remove(item);
                    textView.setVisibility(View.GONE);
                    ArrayList<MyVideoBean> beans= (ArrayList<MyVideoBean>) adapter.getData();
                    for (MyVideoBean bean:beans) {
                        if (bean.getIndexNum()>item.getIndexNum()){
                            bean.setIndexNum(bean.getIndexNum()-1);
                        }
                    }
                    item.setIndexNum(-1);
                    clickIndex--;
                }

                adapter.notifyDataSetChanged();

            }
        });
    }

    @Override
    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
        super.nofityUpdateUi(requestCode, response, view);
        if (requestCode== BaseQuestConfig.GET_MINE_VIDEOS_CODE){
            if (response.code==1){
                MyVideoData videoBean= (MyVideoData) response.data;
                List<MyVideoBean> videoBeans= videoBean.movies;
                pageLimitDelegate.setData(videoBeans);
            }
        }
    }

    @Override
    public void httpLoadFail(String err) {
        super.httpLoadFail(err);
        ToastUtils.longToast(err);
    }
}
