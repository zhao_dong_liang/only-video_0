package cn.wu1588.dancer.my_video.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bilibili.boxing.Boxing;
import com.bilibili.boxing.model.config.BoxingConfig;
import com.bilibili.boxing.model.entity.BaseMedia;
import com.bilibili.boxing.model.entity.impl.ImageMedia;
import com.bilibili.boxing_impl.ui.BoxingActivity;
import com.my.toolslib.StringUtils;
import com.my.toolslib.http.utils.LzyResponse;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;
import com.sunrun.sunrunframwork.utils.EmptyDeal;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.quest.BaseQuestConfig;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.UpLoadUtils;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;
import cn.wu1588.dancer.my_video.adapter.SelectVideoAdapter;
import cn.wu1588.dancer.my_video.bean.MyVideoBean;

public class AddAlbumActivity extends LBaseActivity implements UpLoadUtils.UpLoadCallBack {
    @BindView(R.id.title_bar)
    TitleBar title_bar;
    @BindView(R.id.cover_img)
    ImageView cover_img;
    @BindView(R.id.add_cover_bt)
    Button add_cover_bt;
    @BindView(R.id.add_video_bt)
    Button add_video_bt;
    @BindView(R.id.video_recyclear)
    RecyclerView video_recyclear;
    @BindView(R.id.video_num)
    TextView video_num;
    @BindView(R.id.size_tv)
    TextView size_tv;
    @BindView(R.id.album_introduce_tv)
    EditText album_introduce_tv;
    @BindView(R.id.edit_title)
    EditText edit_title;
    @BindView(R.id.edit_price)
    EditText edit_price;

    private SelectVideoAdapter adapter;
    private int REQUEST_CODE=99;
    private String coverUrl;
    private UpLoadUtils upLoadUtils;
    private String movie_ids;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setDark(CommonApp.getApplication().isBarTvColor());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_album);
        initViews();
    }
    private void initViews(){
        title_bar.setTitleBarBackgroundDrawable(CommonApp.getApplication().getStateBarDrawable());
        adapter=new SelectVideoAdapter();
        adapter.setType(1);
        video_recyclear.setLayoutManager(new GridLayoutManager(that,3));
        video_recyclear.setAdapter(adapter);

    }

    @OnClick({R.id.add_cover_bt,R.id.add_video_bt,R.id.send_bt})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.add_cover_bt://添加专辑封面
            //这里设置的config的mode是single_img  就是单选图片的模式  支持相机
                BoxingConfig videoConfig = new BoxingConfig(BoxingConfig.Mode.SINGLE_IMG);//.withVideoDurationRes(R.mipmap.add_video_icon)
                Boxing.of(videoConfig).withIntent(this, BoxingActivity.class).start(this, REQUEST_CODE);
                break;
            case R.id.add_video_bt://添加专辑视频
                CommonIntent.startSelectVideosActivity(this,2);
                break;
            case R.id.send_bt://提交专辑
                String title = edit_title.getText().toString();
                if (StringUtils.isNull(title)){
                    ToastUtils.longToast("请填写专辑标题");
                    return;
                }
                String introduce = album_introduce_tv.getText().toString();
                if (StringUtils.isNull(introduce)){
                    ToastUtils.longToast("请填写专辑简介");
                    return;
                }
                String price = edit_price.getText().toString();
                if (StringUtils.isNull(coverUrl)){
                    ToastUtils.longToast("请先上传专辑封面");
                    return;
                }
                if (EmptyDeal.isEmpy(movie_ids)){
                    ToastUtils.longToast("请先选择专辑视频");
                    return;
                }
                BaseQuestStart.addUserChannel(this,title,introduce,price,coverUrl,movie_ids);
                break;

        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            final ArrayList<BaseMedia> medias = Boxing.getResult(data);
            BaseMedia baseMedia = medias.get(0);
            if (baseMedia instanceof ImageMedia){
                ImageMedia media= (ImageMedia) baseMedia;
                String path = media.getPath();
                if (!StringUtils.isNull(path)){
                    size_tv.setText("尺寸比例："+media.getWidth()+"*"+media.getHeight());
                    add_cover_bt.setText("替换");
                    cover_img.setVisibility(View.VISIBLE);
                    if (upLoadUtils==null){
                        upLoadUtils=new UpLoadUtils(that);
                        upLoadUtils.setUpLoadCallBack(this::upLoadResult);
                    }
                    upLoadUtils.upLoad(path);

                }
            }
        }
        if (requestCode==2&&data!=null){//选择视频
          ArrayList<MyVideoBean> videoBeans=  data.getParcelableArrayListExtra("list");
            video_num.setText("已挑选"+videoBeans.size()+"个");
            adapter.setNewData(videoBeans);
            movie_ids="";
                for (int i = 0; i < videoBeans.size(); i++) {
                    String s = videoBeans.get(i).getId();
                    if (i==0){
                        movie_ids=s;
                    }else {
                        movie_ids=movie_ids+","+s;
                    }
                }
        }
    }

    @Override
    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
        super.nofityUpdateUi(requestCode, response, view);
        if (requestCode== BaseQuestConfig.ADD_USER_CHANNEL_CODE && response.code == 1){//添加专辑成功
            ToastUtils.longToast("添加成功");
            finish();
        } else if (requestCode== BaseQuestConfig.ADD_USER_CHANNEL_CODE) {
            ToastUtils.longToast(response.msg);
        }
    }

    @Override
    public void upLoadResult(String url, File file) {
        GlideMediaLoader.load(that,cover_img,url,0);
        coverUrl=url;
    }
}
