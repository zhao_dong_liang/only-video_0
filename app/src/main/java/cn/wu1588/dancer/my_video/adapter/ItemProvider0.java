package cn.wu1588.dancer.my_video.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.provider.BaseItemProvider;
import com.my.toolslib.DateUtil;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.my_video.bean.MyVideoBean;

//单行类型view
public class ItemProvider0 extends BaseItemProvider<MyVideoBean, BaseViewHolder> {
    @Override
    public int viewType() {
        return MyVideoAdapter.ITEM_CODE;
    }

    @Override
    public int layout() {
        return R.layout.my_video_item0;
    }

    @Override
    public void convert(BaseViewHolder helper, MyVideoBean data, int position) {
        ImageView imageView = helper.getView(R.id.img);
        LinearLayout llGoneShoujia = helper.getView(R.id.ll_gone_shoujia);
        TextView text_shenhe = helper.getView(R.id.text_shenhe);
        GlideMediaLoader.load(mContext, imageView, data.getImage(), R.mipmap.img_def);
        long play_time = data.getPlay_time();
        if (data.getVFType() == 2) {
            llGoneShoujia.setVisibility(View.VISIBLE);
            text_shenhe.setVisibility(View.VISIBLE);
//           审核状态 1 待审；2过审；3审核失败
            switch (data.getSta()){
                case 1:
                    text_shenhe.setText("待审核");
                    break;
                case 2:
                    text_shenhe.setVisibility(View.GONE);
                    break;
                case 3:
                    text_shenhe.setText("3审核失败");
                    break;
                default:
                    text_shenhe.setVisibility(View.GONE);
            }
        } else {
            llGoneShoujia.setVisibility(View.INVISIBLE);
            text_shenhe.setVisibility(View.GONE);
        }
        String strBySecond = DateUtil.getTimeStrBySecond(play_time);
        helper.setText(R.id.text_title, data.getTitle())
                .setText(R.id.see_num, (data.getPlay_num() == null ? 0 : data.getPlay_num()) + "播放\t\t\t" + data.getLike_num() + "评论")
                .setText(R.id.text_title, data.getTitle())
                .setText(R.id.video_time, strBySecond)
                .setText(R.id.text_price_sum, data.getPrice() == null ? "0" : data.getPrice())
                .setText(R.id.text_volume_sum, data.getSize() == null ? "0" : data.getSize())
                .setText(R.id.text_sold_sum, "0");
    }
}
