package cn.wu1588.dancer.my_video.adapter;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.my_video.bean.TypeBean;

public class TypeAdapter extends BaseQuickAdapter<TypeBean.HomeCategory,BaseViewHolder> {

    public TypeAdapter( @Nullable List data) {
        super(R.layout.type_item, data);
    }


    @Override
    protected void convert(BaseViewHolder helper, TypeBean.HomeCategory item) {
         helper.setText(R.id.type_tv,item.title);
    }
}
