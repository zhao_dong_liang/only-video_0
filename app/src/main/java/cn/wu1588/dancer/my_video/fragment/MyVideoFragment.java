package cn.wu1588.dancer.my_video.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.kongzue.baseframework.interfaces.FragmentLayout;
import com.kongzue.baseokhttp.HttpRequest;
import com.kongzue.baseokhttp.listener.ResponseListener;
import com.kongzue.baseokhttp.util.JsonMap;
import com.kongzue.baseokhttp.util.Parameter;
import com.kongzue.dialog.v3.CustomDialog;
import com.kongzue.dialog.v3.WaitDialog;
import com.my.toolslib.http.utils.LzyResponse;
import com.sunrun.sunrunframwork.uibase.BaseActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.quest.BaseQuestConfig;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.common.util.LoginUtil;
import cn.wu1588.dancer.my_video.activity.MyVideo2Activity;
import cn.wu1588.dancer.my_video.adapter.MyVideoAdapter;
import cn.wu1588.dancer.my_video.bean.MyVideoBean;
import cn.wu1588.dancer.my_video.bean.MyVideoData;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.enums.MyVideoTypeEnum;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.PageLimitDelegate;
import cn.wu1588.dancer.utils.map.MD5Utils;

//我的视频fragment
public class MyVideoFragment extends LBaseFragment {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.num_tv)
    TextView num_tv;
    @BindView(R.id.text_original_application)
    TextView text_original_application;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    @BindView(R.id.flayout_gone_right)
    FrameLayout flayout_gone_right;
    @BindView(R.id.flayout_null)
    FrameLayout flayout_null;
    private boolean isFirstLoadData = true;
    private int type;//   0"全部",
    //                        1"小视频",
//                        2"4K",
//                        3"教学",
//                        4"专辑",
//                        5"分享视频"
    private MyVideoAdapter adapter;

    public MyVideoFragment newInstance(int type) {
        MyVideoFragment fragment = new MyVideoFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.myy_video_fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        initEvent();
    }

    private void initEvent() {
        text_original_application.setOnClickListener(v ->{
//            requestIsHasOriginal()
            //申请原创
            if (LoginUtil.startLogin(getActivity())) {
                CommonIntent.startLoginActivity(that);
            } else {
                CommonIntent.startMyVideoActivity((BaseActivity) getActivity());
            }
        });
    }

    private void initViews() {
        type = getArguments().getInt("type");
        if (type == 5) {
            flayout_gone_right.setVisibility(View.GONE);
        } else {
            flayout_gone_right.setVisibility(View.VISIBLE);
        }
        switch (type) {
            case 0:
            case 2:
            case 3:
            case 5:
                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                break;
            case 4:
                GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);
                layoutManager.setOrientation(RecyclerView.VERTICAL);
                recyclerView.setLayoutManager(layoutManager);
                break;
        }
        adapter = new MyVideoAdapter();
        recyclerView.setAdapter(adapter);
        adapter.setSpanSizeLookup((gridLayoutManager, i) -> {
            MyVideoBean item = adapter.getItem(i);
            int type = item.getType();
            if (type == 1) {
                return 3;
            } else {
                return 1;
            }
        });
        adapter.setOnItemClickListener((adapter, view, position) -> {
            MyVideoBean bean = (MyVideoBean) adapter.getItem(position);
            CommonIntent.startMoviePlayerActivity(that, bean.getId());
        });
        pageLimitDelegate.attach(refreshLayout, recyclerView, adapter);

    }

    private PageLimitDelegate<MyVideoBean> pageLimitDelegate = new PageLimitDelegate<>(new PageLimitDelegate.DataProvider() {
        @Override
        public void loadData(int page) {
            BaseQuestStart.getMineVideos(MyVideoFragment.this, type, page, 15);
        }
    });

    @Override
    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
        super.nofityUpdateUi(requestCode, response, view);
        if (response.code == 1) {
            isFirstLoadData = false;
            if (requestCode == BaseQuestStart.GET_MINE_VIDEOS_CODE) {
                MyVideoData data = (MyVideoData) response.data;
                num_tv.setText("作品\t" + data.count);
                ArrayList<MyVideoBean> myVideoBeans = (ArrayList<MyVideoBean>) data.movies;
                flayout_null.setVisibility(myVideoBeans == null ? View.VISIBLE : View.GONE);
                if (myVideoBeans != null) {
                    for (int i = 0; i < myVideoBeans.size(); i++) {
                        myVideoBeans.get(i).setVFType(type);
                    }
                    pageLimitDelegate.setData(groupMyVideoBeansByUpdateDate(myVideoBeans));
                }
            } else {
                num_tv.setText("作品\t0");
            }
        }
    }

    private List<MyVideoBean> groupMyVideoBeansByUpdateDate(ArrayList<MyVideoBean> myVideoBeans) {
        List<MyVideoBean> results = new ArrayList<>();

        if (myVideoBeans != null) {
            Map<String, List<MyVideoBean>> groupsByUpdateDate = new HashMap<>();
            for (MyVideoBean video : myVideoBeans) {
                String key = video.getUpdate_time().split(" ")[0].replace("-", ".");
                video.setType(2);
                List<MyVideoBean> videoBeanList = groupsByUpdateDate.get(key);
                if (videoBeanList == null) {
                    videoBeanList = new ArrayList<>();
                }
                videoBeanList.add(video);
                groupsByUpdateDate.put(key, videoBeanList);
            }

            for (Map.Entry<String, List<MyVideoBean>> entry : groupsByUpdateDate.entrySet()) {
                String key = entry.getKey();
                MyVideoBean videoBean = new MyVideoBean();
                videoBean.setDate(key);
                videoBean.setType(1);
                results.add(videoBean);
                results.addAll(entry.getValue());
            }

        }
        return results;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!isFirstLoadData) {
            pageLimitDelegate.refreshPage();
        }
    }

    /**
     * 查询原创申请条件是否满足
     */
    private void requestIsHasOriginal() {
        WaitDialog.show((AppCompatActivity) getActivity(), "数据加载中...");
        HttpRequest.POST(getActivity(),
                BaseQuestConfig.GET_TO_CAN_APPLY_ORIGINAL,
                new Parameter()
                        .add("uid", Config.getLoginInfo().id)
                        .add("md5", MD5Utils.md5()),
                new ResponseListener() {
                    @Override
                    public void onResponse(String value, Exception error) {
                        WaitDialog.dismiss();
                        JsonMap jsonMap = new JsonMap(value);
                        if (jsonMap.get("status").equals("1")) {
                            JsonMap data = jsonMap.getJsonMap("data");
                            int allowData = data.getInt("allow");
                            //0表示没有
                            if (allowData == 0) {
                                showNoAllowDialog();
                            } else {

                            }
                        } else {
                            LogUtils.e("GET_TO_CAN_APPLY_ORIGINAL接口请求失败 服务器返回不为 1" + error);
                            ToastUtils.showShort("服务器请求失败！");
                        }
                    }
                });
    }

    /**
     * 无资格弹框
     */
    public void showNoAllowDialog() {
        CustomDialog.build((AppCompatActivity) getActivity(), R.layout.dialog_has_allow, new CustomDialog.OnBindView() {
            @Override
            public void onBind(CustomDialog dialog, View v) {
                Button btnOk = v.findViewById(R.id.btnSure);
                btnOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.doDismiss();
                        getActivity().finish();
                    }
                });
            }
        }).show();
    }

}