package cn.wu1588.dancer.my_video.activity;

import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.blankj.utilcode.util.ToastUtils;
import com.flyco.tablayout.SlidingTabLayout;
import com.kongzue.baseframework.interfaces.BindView;
import com.kongzue.baseframework.interfaces.DarkStatusBarTheme;
import com.kongzue.baseframework.interfaces.Layout;
import com.kongzue.baseframework.util.JumpParameter;
import com.kongzue.dialog.util.DialogSettings;

import java.util.ArrayList;
import java.util.List;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.base.BaseAty;
import cn.wu1588.dancer.base.ViewPagerFragmentAdapter;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.common.enums.MyVideoType2Enum;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;
import cn.wu1588.dancer.my_video.fragment.MyVideo2Fragment;
import cn.wu1588.dancer.my_video.fragment.MyVideo3Fragment;
import cn.wu1588.dancer.my_video.fragment.MyVideoFragment;

//我的视频
@Layout(R.layout.activity_my_video2)
@DarkStatusBarTheme(true)
public class MyVideo2Activity extends BaseAty {
    @BindView(R.id.title_bar)
    private TitleBar titleBar;
    @BindView(R.id.tab_layout)
    private SlidingTabLayout tabLayout;
    @BindView(R.id.view_pager)
    private ViewPager viewPager;
    private boolean check_is_secret;//是否设置过密码
    private boolean validate_secret;//验证密码是否正确
    private List<Fragment> baseFragments;

    private boolean checkIsSecret = false;
    private ViewPagerFragmentAdapter mVPAdapter;
    String[] titles = new String[]{
            "全部",
            "小视频",
            "4K",
            "教学",
            "专辑",
            "分享视频"
    };

    @Override
    public void initViews() {
        baseFragments = new ArrayList<>();
        mVPAdapter = new ViewPagerFragmentAdapter(getSupportFragmentManager());
        baseFragments.add(new MyVideoFragment().newInstance(MyVideoType2Enum.ALL_TYPE.getCode()));
        baseFragments.add(new MyVideo2Fragment().newInstance(MyVideoType2Enum.PUBLIC_TYPE.getCode()));
        baseFragments.add(new MyVideoFragment().newInstance(MyVideoType2Enum.CHARGE_TYPE.getCode()));
        baseFragments.add(new MyVideoFragment().newInstance(MyVideoType2Enum.PRIVATE_TYPE.getCode()));
        baseFragments.add(new MyVideo3Fragment().newInstance(MyVideoType2Enum.ALBUM_TYPE.getCode()));
        baseFragments.add(new MyVideoFragment().newInstance(MyVideoType2Enum.SHARE_VIDEOS.getCode()));
        mVPAdapter.setFragments(baseFragments);
        viewPager.setAdapter(mVPAdapter);
        viewPager.setCurrentItem(0, false);
        tabLayout.setViewPager(viewPager, titles);
    }

    @Override
    public void initDatas(JumpParameter parameter) {
        DialogSettings.init();
        DialogSettings.cancelable = false;
    }

    @Override
    public void setEvents() {

        titleBar.setRightAction(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonIntent.startUpVideosActivity(me, true);
                ToastUtils.showShort("后浪 推 前浪 " +
                        "前辈被伤了心 " +
                        "黑眼睛 黄色皮 " +
                        "China 力 want_to_be_king");
            }
        });

    }

}
