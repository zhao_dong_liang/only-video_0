package cn.wu1588.dancer.fgt.subscribe;

import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.kongzue.baseokhttp.HttpRequest;
import com.kongzue.baseokhttp.listener.ResponseListener;
import com.kongzue.baseokhttp.util.JsonList;
import com.kongzue.baseokhttp.util.JsonMap;
import com.kongzue.baseokhttp.util.Parameter;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.adp.SubscribeAdp;
import cn.wu1588.dancer.adp.SubscribeTopAdp;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.home.mode.MovieBean;
import cn.wu1588.dancer.share.ShareBottomSheetDialog;
import cn.wu1588.dancer.utils.map.JSONUtils;
import cn.wu1588.dancer.utils.map.MD5Utils;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_ATTENTION_MOVIES;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.HOT_DY_NULL;


/**
 * 作者：ninetailedfox
 * 时间：2021/1/15 2:52 PM
 * 邮箱: 1037438704@qq.com
 * 功能：订阅页面
 **/
public class SubscribeFgt extends LBaseFragment {

    private RecyclerView rv_content, recycler_visiton;
    private SubscribeAdp subscribeAdp;
    private LinearLayout ll_no_subscription;
    SubscribeTopAdp subscribeTopAdp;

    @Override
    public int getLayoutRes() {
        return R.layout.fgt_subscribe;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        initData();
        initEvent();
    }

    private void initData() {
        getListHttp();
    }


    private void initEvent() {
//        subscribeAdp.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
//            @Override
//            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
//                ToastUtils.shortToast("点击了");
//            }
//        });
        subscribeAdp.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                List<Map<String, String>> datajson = adapter.getData();
                Map<String, String> stringJson = datajson.get(position);
                switch (view.getId()) {
                    case R.id.text_share:
                        MovieBean movieBean = new MovieBean();
                        movieBean.id = Integer.parseInt(stringJson.get("id"));
                        movieBean.title = stringJson.get("title");
                        movieBean.play_url = stringJson.get("play_url");
                        movieBean.image = stringJson.get("image");
                        ShareBottomSheetDialog shareBottomSheetDialog = ShareBottomSheetDialog.newInstance(movieBean);
                        shareBottomSheetDialog.show(getActivity().getSupportFragmentManager(), ShareBottomSheetDialog.TAG);
                        break;

                }
            }
        });
    }

    private void initViews(View view) {
        rv_content = view.findViewById(R.id.rv_content);
        rv_content.setLayoutManager(new LinearLayoutManager(getActivity()));
        subscribeAdp = new SubscribeAdp(R.layout.item_subscribe_adp);
        rv_content.setAdapter(subscribeAdp);

        ll_no_subscription = view.findViewById(R.id.ll_no_subscription);
        recycler_visiton = view.findViewById(R.id.recycler_visiton);
        recycler_visiton.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));
        subscribeTopAdp = new SubscribeTopAdp(R.layout.item_subscribe_head);


    }

    private void getListHttp() {
        HttpRequest.POST(getContext(), GET_ATTENTION_MOVIES, new Parameter()
                .add("md5", MD5Utils.md5())
                .add("device", Config.deviceId)
                .add("pageSize", 5)
                .add("task_driver", "1")
                .add("task_tion", "1"), new ResponseListener() {
            @Override
            public void onResponse(String value, Exception error) {
                if (error == null) {
                    Map<String, String> jsonMap = JSONUtils.parseKeyAndValueToMap(value);
                    Log.d("zdl","======"+jsonMap.get("status"));
                    if (jsonMap.get("status").equals("1")){
                        Map<String, String> data = JSONUtils.parseKeyAndValueToMap(jsonMap.get("data"));
                        Map<String, String> attention_info = JSONUtils.parseKeyAndValueToMap(data.get("attention_info"));
                        if (attention_info.get("total_attention").equals("0")){
                            getNull();
                            ll_no_subscription.setVisibility(View.VISIBLE);
                        }else{
                            ll_no_subscription.setVisibility(View.GONE);
                            ArrayList<Map<String, String>> attention_list = JSONUtils.parseKeyAndValueToMapList(data.get("attention_list"));
                            subscribeTopAdp.setNewData(attention_list);
                            ArrayList<Map<String, String>> movie_list1 = JSONUtils.parseKeyAndValueToMapList(data.get("movie_list"));
                            subscribeAdp.setNewData(movie_list1);
                        }
                    }
                }
            }
        });

    }
    public void getNull(){
        //        device	是	string	设备标识号
//        pageSize	否	int	每页数量，默认5
//        task_driver	是	int	投放设备 1移动端；2PC端
//        task_tion	是	int	投放位置 1数据流；2视频插播；3播放页小横条；4播放页右上角；5视频图片推送；6开屏
        //获取数据
        HttpRequest.POST(getContext(), HOT_DY_NULL, new Parameter()
                        .add("md5", MD5Utils.md5())
                        .add("device", Config.deviceId)
                        .add("pageSize", 5)
                        .add("task_driver", "1")
                        .add("task_tion", "1")
                , new ResponseListener() {
                    @Override
                    public void onResponse(String value, Exception error) {
                        if (error == null) {
                            Map<String, String> jsonMap = JSONUtils.parseKeyAndValueToMap(value);
                            if (jsonMap == null) {
                                return;
                            }

                            if (jsonMap.get("status").equals("1")) {
                                if (jsonMap.get("data") != null) {
                                    ArrayList<Map<String, String>> dataMap = JSONUtils.parseKeyAndValueToMapList(jsonMap.get("data"));
                                    subscribeAdp.setNewData(dataMap);
                                }
                            }
                        }
                    }
                });
    }

    public static SubscribeFgt newInstance() {
        return new SubscribeFgt();
    }
}