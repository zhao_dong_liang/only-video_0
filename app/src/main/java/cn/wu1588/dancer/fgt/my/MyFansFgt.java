package cn.wu1588.dancer.fgt.my;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.kongzue.baseframework.interfaces.Layout;
import com.my.toolslib.http.utils.NetRequestListenerProxy;
import com.sunrun.sunrunframwork.bean.BaseBean;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.base.BaseFgt;
import cn.wu1588.dancer.model.FansListBean;
import cn.wu1588.dancer.common.quest.BaseQuestStart;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MyFansFgt#newInstance} factory method to
 * create an instance of this fragment.
 */

@Layout(R.layout.fgt_my_t_ime)
public class MyFansFgt extends BaseFgt {
    private RecyclerView recyclerView;
    String mType = "";
    private BaseQuickAdapter<FansListBean, BaseViewHolder> mAdapter;
    int page = 1;
    public static MyFansFgt newInstance(String type) {
        MyFansFgt fragment = new MyFansFgt();
        Bundle args = new Bundle();
        args.putString("type", type);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void initViews() {
        recyclerView = (RecyclerView) findViewById(R.id.fgt_my_t_ime_rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(mAdapter = new BaseQuickAdapter<FansListBean, BaseViewHolder>(R.layout.item_fans_layout) {
            @Override
            protected void convert(BaseViewHolder helper, FansListBean item) {
                Glide.with(mContext).load(item.getIcon()).into((ImageView) helper.getView(R.id.item_fans_photo));
                helper.getView(R.id.item_fans_gz).setBackgroundResource(item.getIs_attention().equals("0") ? R.drawable.bg_fans_btn_unselect : R.drawable.bg_fans_btn_select);
                TextView des = helper.getView(R.id.item_fans_des);
                des.setTextColor(item.getIs_new().equals("1") ? Color.parseColor("#FF0000") : Color.parseColor("#B9B6B6"));
                helper.setText(R.id.item_fans_name,item.getNickname())
                        .setText(R.id.item_fans_des,item.getIs_new().equals("1") ? "新关注了你" : item.getAdd_time())
                        .setText(R.id.item_fans_gz,item.getIs_attention().equals("0") ? "关 注" : "取消关注");
            }
        });
    }
    @Override
    public void initDatas() {
        Bundle bundle = getArguments();
        mType = bundle.getString("type");
        BaseQuestStart.getFansList(mType,page,new NetRequestListenerProxy(getContext()){
            @Override
            public void nofityUpdate(int i, BaseBean baseBean) {
                mAdapter.setNewData(baseBean.Data());
            }
        });
    }

    @Override
    public void setEvents() {

    }
}