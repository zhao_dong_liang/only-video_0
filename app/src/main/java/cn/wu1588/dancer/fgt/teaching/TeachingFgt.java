package cn.wu1588.dancer.fgt.teaching;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.flyco.tablayout.SlidingTabLayout;
import com.google.android.material.tabs.TabLayout;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uibase.BaseActivity;
import com.sunrun.sunrunframwork.uiutils.UIUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.adp.FragmentAdapter;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.discover.FlowLayout;
import cn.wu1588.dancer.discover.fragment.DiscoverChildFragment;
import cn.wu1588.dancer.discover.fragment.DiscoverChildIndexFragment;
import cn.wu1588.dancer.model.DiscoverMenu;
import cn.wu1588.dancer.home.activity.SearchActivity;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_DISCOVER_MENU_LIST_CODE;

/**
 * 作者：ninetailedfox
 * 时间：2021/1/14 11:00 PM
 * 邮箱: 1037438704@qq.com
 * 功能：教学页面
 **/
public class TeachingFgt extends LBaseFragment {

    private List<DiscoverMenu> list;
    private TextView text_search;
    private ViewPager viewPager;
    private SlidingTabLayout sliding_tab_layout;
    private ImageView ib_upload;
    private FlowLayout flowLayout;
    private FragmentAdapter fragmentAdapter;
    private String[] title;
    private List<String> mTitles;
    private List<String> titleLists = new ArrayList<>();
    private List<TextView> textViews = new ArrayList<>();
    private List<Fragment> fragmentList;

    @Override
    public int getLayoutRes() {
        return R.layout.fgt_teaching;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        initEvent();
    }

    private void initViews(View v) {
        sliding_tab_layout = v.findViewById(R.id.sliding_tab_layout);
        flowLayout = v.findViewById(R.id.flow_layout);
        ib_upload = v.findViewById(R.id.ib_upload);
        viewPager = v.findViewById(R.id.viewPager);
        text_search = v.findViewById(R.id.text_search);
        fragmentList = new ArrayList<>();
        mTitles = new ArrayList<>();
    }

    private void initEvent() {
        text_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SearchActivity.class));
            }
        });
        ib_upload.setOnClickListener(target -> CommonIntent.startUpVideosActivity((BaseActivity) getActivity(), false));
        BaseQuestStart.getDiscoverMenuList(TeachingFgt.this);
    }

    private void initChildViews() {
        if (getActivity() == null) {
            return;
        }
        ViewGroup.MarginLayoutParams lp = new ViewGroup.MarginLayoutParams(
                UIUtils.dip2px(getActivity(), 70), ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.leftMargin = UIUtils.dip2px(getActivity(), 0);
        lp.rightMargin = UIUtils.dip2px(getActivity(), 10);
        lp.topMargin = UIUtils.dip2px(getActivity(), 0);
        lp.bottomMargin = UIUtils.dip2px(getActivity(), 10);
        int l = UIUtils.dip2px(getActivity(), 3);
        int t = UIUtils.dip2px(getActivity(), 3);
        int r = UIUtils.dip2px(getActivity(), 3);
        int b = UIUtils.dip2px(getActivity(), 3);
        for (int i = 0; i < title.length; i++) {
            TextView view = new TextView(TeachingFgt.this.getActivity());
            view.setMaxLines(1);
            view.setEllipsize(TextUtils.TruncateAt.END);
            view.setText(title[i]);
            titleLists.add(title[i]);
            view.setTextColor(getResources().getColor(R.color.black_color));
            view.setGravity(Gravity.CENTER);
            view.setPadding(l, t, r, b);
            view.setBackgroundDrawable(getResources().getDrawable(R.drawable.shape_round_gray));
            view.setOnClickListener(view1 -> {
                int index = textViews.indexOf(view1);
                flowLayout.setVisibility(View.GONE);
                viewPager.setCurrentItem(index);
            });
            textViews.add(view);
            flowLayout.addView(view, lp);
        }
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode) {
            case GET_DISCOVER_MENU_LIST_CODE:
                list = bean.Data();
                if (list != null && !list.isEmpty()) {
                    title = new String[list.size() + 1];
                    title[0] = "推荐";
                    DiscoverChildIndexFragment.KindCallback callback = str -> {
                        int index = titleLists.indexOf(str);
                        viewPager.setCurrentItem(index);
                    };
                    fragmentList.add(DiscoverChildIndexFragment.newInstance(1, callback));
                    for (int i = 0; i < list.size(); i++) {
                        title[i + 1] = list.get(i).title;
                        fragmentList.add(DiscoverChildFragment.newInstance(1, list.get(i).id, callback));
                    }
                    for (int i = 0; i < title.length; i++) {
                        mTitles.add(title[i]);
                    }
                    if (fragmentList == null) {
                        return;
                    }
                    if (mTitles == null) {
                        return;
                    }
                    if (getActivity() == null) {
                        return;
                    }
                    if (getActivity().getSupportFragmentManager() != null) {
                        fragmentAdapter = new FragmentAdapter(getActivity().getSupportFragmentManager(), fragmentList, mTitles);
                        viewPager.setAdapter(fragmentAdapter);
                        sliding_tab_layout.setViewPager(viewPager, title);
                        initChildViews();
                    }
                }
                break;
        }
        super.nofityUpdate(requestCode, bean);
    }

    //
    public static TeachingFgt newInstance() {
        TeachingFgt discoverFragment = new TeachingFgt();
        Bundle bundle = new Bundle();
        discoverFragment.setArguments(bundle);
        return discoverFragment;
    }
}