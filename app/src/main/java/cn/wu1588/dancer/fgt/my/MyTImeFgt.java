package cn.wu1588.dancer.fgt.my;

import android.os.Bundle;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.kongzue.baseframework.interfaces.Layout;
import com.my.toolslib.http.utils.NetRequestListenerProxy;
import com.sunrun.sunrunframwork.bean.BaseBean;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.base.BaseFgt;
import cn.wu1588.dancer.common.model.TimeListBean;
import cn.wu1588.dancer.common.quest.BaseQuestStart;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MyTImeFgt#newInstance} factory method to
 * create an instance of this fragment.
 */

@Layout(R.layout.fgt_my_t_ime)
public class MyTImeFgt extends BaseFgt {
    private RecyclerView recyclerView;
    String mType = "";
    private BaseQuickAdapter<TimeListBean, BaseViewHolder> mAdapter;
    int page = 1;
    public static MyTImeFgt newInstance(String type) {
        MyTImeFgt fragment = new MyTImeFgt();
        Bundle args = new Bundle();
        args.putString("type", type);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void initViews() {
        recyclerView = (RecyclerView) findViewById(R.id.fgt_my_t_ime_rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(mAdapter = new BaseQuickAdapter<TimeListBean, BaseViewHolder>(R.layout.item_my_time_layout) {
            @Override
            protected void convert(BaseViewHolder helper, TimeListBean item) {
                helper.setText(R.id.item_my_time_name,item.getNickname())
                        .setText(R.id.item_my_time_long,item.getTime_miao())
                        .setText(R.id.item_my_time_time,item.getTime_time())
                        .setText(R.id.item_my_time_shixiao,item.getTime_sta().equals("1") ? "有效" : "无效");
            }
        });
    }
    @Override
    public void initDatas() {
        Bundle bundle = getArguments();
        mType = bundle.getString("type");
        BaseQuestStart.getUserTimeList(mType,page,new NetRequestListenerProxy(getContext()){
            @Override
            public void nofityUpdate(int i, BaseBean baseBean) {
//                mAdapter.setNewData(baseBean.Data());
            }
        });
        for (int i = 0; i < 10; i++) {
            TimeListBean listBean = new TimeListBean();
            listBean.setNickname(i+"祝2021年事业有成,一帆风顺");
            listBean.setTime_time("2021-01-0"+i);
            listBean.setTime_miao("9"+i);
            listBean.setTime_sta(i%3 == 0 ? "1" : "2");
            mAdapter.addData(listBean);
        }
        mAdapter.notifyDataSetChanged();

    }

    @Override
    public void setEvents() {

    }
}