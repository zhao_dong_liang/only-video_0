package cn.wu1588.dancer.channel.mode;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import cn.wu1588.dancer.model.Ad;

public class Channels {

    public List<Ad> ad;
    public List<MustChannel> must_channel;
    public List<MustChannel> hot_channel;
    public List<MustChannel> new_channel;


    public static class MustChannel {

        public String id;
        public String type;
        public String title;
        public String content;
        public String logo;

        public String background_image;
        public String sort;

        public String add_time;

        public String update_time;

        public String is_hid;

        public String is_del;
        public String recommend;
        public String desc;

        public String is_play;

        public String user_id;
        public String price;

        public String download_num;
        public int movies_count;
        public List<Movie> movies;


    }

    public static class Movie implements Parcelable  {

        public String id;
        public String actor_id;
        public String title;
        public String image;
        public String comment_num;
        public String play_num;
        public String play_url;
        public String label;
        public String home_category_id;
        public String recommend_category_id;
        public String hot_category_id;
        public String top_num;
        public String down_num;
        public String collect_num;
        public String grade;
        public String sort;
        public String add_time;
        public String update_time;
        public String is_hid;
        public String is_del;
        public String recommend;
        public String play_image;

        public String play_time;
        public String content;

        public String is_free;
        public String gold;

        public String user_id;
        public String like;

        public String download_url;

        public String download_num;

        public String user_movie_type;

        public String share_num;
        public String videoid;

        public Movie() {
        }

        protected Movie(Parcel in) {
            id = in.readString();
            actor_id = in.readString();
            title = in.readString();
            image = in.readString();
            comment_num = in.readString();
            play_num = in.readString();
            play_url = in.readString();
            label = in.readString();
            home_category_id = in.readString();
            recommend_category_id = in.readString();
            hot_category_id = in.readString();
            top_num = in.readString();
            down_num = in.readString();
            collect_num = in.readString();
            grade = in.readString();
            sort = in.readString();
            add_time = in.readString();
            update_time = in.readString();
            is_hid = in.readString();
            is_del = in.readString();
            recommend = in.readString();
            play_image = in.readString();
            play_time = in.readString();
            content = in.readString();
            is_free = in.readString();
            gold = in.readString();
            user_id = in.readString();
            like = in.readString();
            download_url = in.readString();
            download_num = in.readString();
            user_movie_type = in.readString();
            share_num = in.readString();
            videoid = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(id);
            dest.writeString(actor_id);
            dest.writeString(title);
            dest.writeString(image);
            dest.writeString(comment_num);
            dest.writeString(play_num);
            dest.writeString(play_url);
            dest.writeString(label);
            dest.writeString(home_category_id);
            dest.writeString(recommend_category_id);
            dest.writeString(hot_category_id);
            dest.writeString(top_num);
            dest.writeString(down_num);
            dest.writeString(collect_num);
            dest.writeString(grade);
            dest.writeString(sort);
            dest.writeString(add_time);
            dest.writeString(update_time);
            dest.writeString(is_hid);
            dest.writeString(is_del);
            dest.writeString(recommend);
            dest.writeString(play_image);
            dest.writeString(play_time);
            dest.writeString(content);
            dest.writeString(is_free);
            dest.writeString(gold);
            dest.writeString(user_id);
            dest.writeString(like);
            dest.writeString(download_url);
            dest.writeString(download_num);
            dest.writeString(user_movie_type);
            dest.writeString(share_num);
            dest.writeString(videoid);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<Movie> CREATOR = new Creator<Movie>() {
            @Override
            public Movie createFromParcel(Parcel in) {
                return new Movie(in);
            }

            @Override
            public Movie[] newArray(int size) {
                return new Movie[size];
            }
        };
    }
}
