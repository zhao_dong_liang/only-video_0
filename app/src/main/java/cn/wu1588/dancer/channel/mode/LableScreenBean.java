package cn.wu1588.dancer.channel.mode;

import java.util.List;

public class LableScreenBean {

    /**
     * id : 6
     * second : []
     * title : 国产
     */

    public String id;
    public String title;
    public boolean isSelect;
    public List<SecondBean> second;


    public static class SecondBean {
        /**
         * "id": "5",
         * "title": "白虎"
         */
        public int id;
        public String title;
        public  boolean isSelect;

        @Override
        public String toString() {
            return String.valueOf(id);
        }

        @Override
        public int hashCode() {
            return id;
        }

        @Override
        public boolean equals(Object obj) {
            if(obj instanceof LableScreenBean.SecondBean){
                return  hashCode()==obj.hashCode();
            }
            return super.equals(obj);
        }
    }
}
