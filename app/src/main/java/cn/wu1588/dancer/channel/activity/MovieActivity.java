package cn.wu1588.dancer.channel.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.sunrun.sunrunframwork.bean.BaseBean;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.PageLimitDelegate;
import cn.wu1588.dancer.common.util.RecycleHelper;
import cn.wu1588.dancer.adp.MovieAdapter;
import cn.wu1588.dancer.home.mode.MovieBean;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.HOT_SHOW_CODE;

public class MovieActivity extends LBaseActivity {
    private ImageView back;
    private RecyclerView recyclerView;
    private TextView textView;
    private MovieAdapter movieAdapter;
    SwipeRefreshLayout refreshLayout;
    private int id;
    PageLimitDelegate pageLimitDelegate = new PageLimitDelegate(new PageLimitDelegate.DataProvider() {
        @Override
        public void loadData(int page) {
            BaseQuestStart.getHotShowList(that, page, id);
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);
        back = findViewById(R.id.hot_cate_kind_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        textView = findViewById(R.id.hot_cate_kind_name);
        recyclerView = findViewById(R.id.hot_cate_kind_rv);
        refreshLayout = findViewById(R.id.hot_cate_re);
        String title = getIntent().getStringExtra("TITLE");
        id = getIntent().getIntExtra("ID", 0);
        if (title == null) {
            title = "暂无";
        }
        textView.setText(title);
        RecycleHelper.setGridLayoutManager(recyclerView, 2);
        movieAdapter = new MovieAdapter();
        pageLimitDelegate.attach(refreshLayout, recyclerView, movieAdapter);
        recyclerView.setAdapter(movieAdapter);
        movieAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                MovieBean item = movieAdapter.getItem(position);
                CommonIntent.startMoviePlayerActivity(that, String.valueOf(item.id));
            }
        });
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode) {
            case HOT_SHOW_CODE:
                if (bean.status == 1) {
                    List<MovieBean> list = bean.Data();
                    pageLimitDelegate.setData(list);
                }
                break;
        }
        super.nofityUpdate(requestCode, bean);
    }
}
