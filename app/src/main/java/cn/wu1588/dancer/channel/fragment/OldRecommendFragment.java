package cn.wu1588.dancer.channel.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.sunrun.sunrunframwork.bean.BaseBean;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.wu1588.dancer.channel.activity.MovieActivity;
import cn.wu1588.dancer.adp.HotCateAdapter;
import cn.wu1588.dancer.adp.SpaceItemDecoration;
import cn.wu1588.dancer.adp.StarAdapter;
import cn.wu1588.dancer.channel.mode.Channels;
import cn.wu1588.dancer.channel.mode.Recommend;
import cn.wu1588.dancer.channel.mode.RecommendHeadMode;
import cn.wu1588.dancer.channel.mode.ThemeaticBean;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.quest.BaseQuestStart;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.CHANNEL_LIST_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_THEMATIC_LIST_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.HOT_LIST_CODE;

/**
 * 专栏推荐
 */
public class OldRecommendFragment extends LBaseFragment {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    private RecommendHeadMode recomendHeadMode;
    private StarAdapter starAdapter;
    private HotCateAdapter hotCateAdapter;
    private View headView;
    private LinearLayout linearLayout;
    private List<Recommend> list = new ArrayList<>();
    private RecyclerView hotCateRv;
    @Override
    public int getLayoutRes() {
        return R.layout.fragment_recomend;
    }

    public static OldRecommendFragment newInstance() {
        OldRecommendFragment recommendFragment = new OldRecommendFragment();
        Bundle bundle = new Bundle();
        recommendFragment.setArguments(bundle);
        return recommendFragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        initData();
    }

    private void initData() {
        BaseQuestStart.getThematicList(OldRecommendFragment.this);
        BaseQuestStart.getRecommendList(OldRecommendFragment.this);
        BaseQuestStart.getChannelList(this);
    }

    private void initViews() {
        recyclerView.setLayoutManager(new LinearLayoutManager(that));
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initData();
                refreshLayout.setRefreshing(false);
            }
        });
        starAdapter = new StarAdapter();
        headView = View.inflate(that, R.layout.view_reconmend_head, null);
        linearLayout = headView.findViewById(R.id.llRenqiStarMore);
        hotCateRv = headView.findViewById(R.id.rv_hot_cate);
        starAdapter.addHeaderView(headView);
        recomendHeadMode = new RecommendHeadMode(headView);
        recyclerView.setAdapter(starAdapter);

        hotCateAdapter = new HotCateAdapter(list,OldRecommendFragment.this.getContext());
        hotCateAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                startActivity(new Intent(OldRecommendFragment.this.getActivity(), MovieActivity.class).putExtra("TITLE",list.get(position).title).putExtra("ID",list.get(position).id));
            }
        });
        hotCateRv.setLayoutManager(new LinearLayoutManager(OldRecommendFragment.this.getContext()));
        hotCateRv.addItemDecoration(new SpaceItemDecoration(OldRecommendFragment.this.getContext(),2));
        hotCateRv.setAdapter(hotCateAdapter);
        starAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                ThemeaticBean.GirllistBean item = starAdapter.getItem(position);
                CommonIntent.startNvYouDetailActivity(that,item.id);
            }
        });
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        super.nofityUpdate(requestCode, bean);
        switch (requestCode) {
            case GET_THEMATIC_LIST_CODE:
                if (bean.status == 1) {
                    ThemeaticBean themeaticBean = bean.Data();
                    if (themeaticBean.girllist == null || themeaticBean.girllist.isEmpty()){
                        linearLayout.setVisibility(View.GONE);
                    }else {
                        linearLayout.setVisibility(View.VISIBLE);
                    }
                    updateUI(themeaticBean);
                }
                break;
            case HOT_LIST_CODE:
                if (list!=null){
                    list.addAll(bean.Data());
                    hotCateAdapter.notifyDataSetChanged();
                }
                break;
            case CHANNEL_LIST_CODE:
                Channels channels = bean.Data();
                Log.d("GetChannels", channels.toString());
                break;
        }
    }

    private void updateUI(ThemeaticBean themeaticBean) {
        getSession().put("hotMoreList", themeaticBean);
        recomendHeadMode.setData(themeaticBean);
        starAdapter.setNewData(themeaticBean.girllist);

    }
}
