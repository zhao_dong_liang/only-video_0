package cn.wu1588.dancer.channel.mode;

import java.util.List;

import cn.wu1588.dancer.home.mode.MovieBean;

/**
 * Created by caidong on 2019/8/26.
 * email:mircaidong@163.com
 * describe: 描述
 */
public class Recommend {
    public int id;
    public String title;
    public String content;
    public String logo;
    public String background_image;
    public String desc;
    public List<MovieBean> data;
}
