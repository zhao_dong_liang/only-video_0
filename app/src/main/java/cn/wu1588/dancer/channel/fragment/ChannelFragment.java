package cn.wu1588.dancer.channel.fragment;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;

import butterknife.OnClick;
import cn.wu1588.dancer.R;
import com.flyco.tablayout.SlidingTabLayout;
import com.my.toolslib.NumUtils;
import com.sunrun.sunrunframwork.uibase.BaseActivity;
import com.tencent.mmkv.MMKV;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.base.ViewPagerFragmentAdapter;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.model.SystemParams;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;

/**
 * 导航-频道
 */
public class ChannelFragment extends LBaseFragment {
    @BindView(R.id.tabLayout)
    SlidingTabLayout tabLayout;
    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.rl_titleBar)
    RelativeLayout rl_titleBar;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.ib_upload)
    View ib_upload;
    @BindView(R.id.ib_history)
    View ib_history;
    private String[] mTitles = {"专栏推荐", "标签筛选"};
    private List<Fragment> baseFragments;
    private ViewPagerFragmentAdapter mVPAdapter;

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_channel;
    }

    public static ChannelFragment newInstance() {
        ChannelFragment channelFragment = new ChannelFragment();
        Bundle bundle = new Bundle();
        channelFragment.setArguments(bundle);
        return channelFragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }

    private void initViews() {
        //设置状态栏颜色
        titleBar.setTitleBarBackgroundDrawable(CommonApp.getApplication().getStateBarDrawable());
        rl_titleBar.setBackground(CommonApp.getApplication().getStateBarDrawable());
        baseFragments = new ArrayList<>();
        baseFragments.add(RecommendFragment.newInstance());//专栏推荐
        baseFragments.add(LabelScreenFragment.newInstance());//标签筛选
        mVPAdapter = new ViewPagerFragmentAdapter(getFragmentManager());
        mVPAdapter.setFragments(baseFragments);
        viewPager.setAdapter(mVPAdapter);
        viewPager.setCurrentItem(0,false);
        tabLayout.setViewPager(viewPager,mTitles);

        rl_titleBar.setOnClickListener(view1 -> CommonIntent.startSearchActivity(that));
        ib_upload.setOnClickListener(target -> CommonIntent.startUpVideosActivity((BaseActivity) getActivity(), false));
        ib_history.setOnClickListener(target -> CommonIntent.startHistoryRecordActivity(getActivity()));

        initUIConfig();
    }

    @OnClick(R.id.ib_upload)
    void upload(View view) {

    }

    private void initUIConfig() {
        MMKV mmkv = MMKV.defaultMMKV();
        int[] colors = {Color.parseColor(mmkv.decodeString("up_b_color","#000000")),
                Color.parseColor(mmkv.decodeString("up_e_color","#000000"))
        };
        GradientDrawable drawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT,colors);
        drawable.setGradientType(GradientDrawable.LINEAR_GRADIENT);//设置线性渐变
        drawable.setOrientation(GradientDrawable.Orientation.LEFT_RIGHT);//设置渐变方向
        tabLayout.setBackground(drawable);

        int searchBarShowMode = NumUtils.parseInt(mmkv.decodeString(SystemParams.IS_SHOW_CATE, "2"), 2);
        String logoImgUrl = mmkv.decodeString("logo_img");
        GlideMediaLoader.load(getActivity(),getView().findViewById(R.id.ivLogo),logoImgUrl);
        titleBar.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                titleBar.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                ViewGroup.LayoutParams lp = titleBar.getLayoutParams();
                switch (searchBarShowMode) {
                    case 0:
                        rl_titleBar.setVisibility(View.VISIBLE);
                        tabLayout.setVisibility(View.GONE);
                        lp.height -= tabLayout.getHeight();
                        break;
                    case 1:
                        rl_titleBar.setVisibility(View.GONE);
                        tabLayout.setVisibility(View.VISIBLE);
                        lp.height -= rl_titleBar.getHeight();
                        break;
                    default:
                    case 2:
                        rl_titleBar.setVisibility(View.VISIBLE);
                        tabLayout.setVisibility(View.VISIBLE);
                        break;
                }
                titleBar.setLayoutParams(lp);
            }
        });
    }


}
