package cn.wu1588.dancer.channel.activity;

import android.os.Bundle;

import com.cnsunrun.commonui.widget.titlebar.TitleBar;
import com.sunrun.sunrunframwork.utils.EmptyDeal;

import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import cn.wu1588.dancer.adp.HotZhuanMoreAdapter;
import cn.wu1588.dancer.channel.mode.ThemeaticBean;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.util.RecycleHelper;

/**
 * 频道-热门专题更多11
 */
public class HotZhanTiMoreActivity extends LBaseActivity {

    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hot_zhuanti_more);
        ButterKnife.bind(this);
        initViews();
    }

    private void initViews() {
        ThemeaticBean themeaticBean = getSession().getObject("hotMoreList", ThemeaticBean.class);
        List<List<ThemeaticBean.HotmoreBean>> hotmore = themeaticBean.hotmore;
        RecycleHelper.setLinearLayoutManager(recyclerView, LinearLayoutManager.VERTICAL);
        HotZhuanMoreAdapter hotZhuanMoreAdapter=new HotZhuanMoreAdapter();
        if (!EmptyDeal.isEmpy(hotmore)){
            hotZhuanMoreAdapter.setNewData(hotmore);
        }
        recyclerView.setAdapter(hotZhuanMoreAdapter);
    }
}
