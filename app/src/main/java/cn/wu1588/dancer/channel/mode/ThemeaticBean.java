package cn.wu1588.dancer.channel.mode;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class ThemeaticBean  {

    public List<AddslideBean> addslide;
    public List<GirllistBean> girllist;
    public List<HotBean> hot;
    public List<List<HotmoreBean>> hotmore;
    public List<MustseeBean> mustsee;



    public static class AddslideBean {
        /**
         * image : http://www.baidu.com/11111111
         * url : 11111111111
         */

        public String image;
        public String url;


    }

    public static class GirllistBean {
        /**
         * content :
         * icon : http://www.baidu.com/uploads/一
         * id : 1
         * name : 波多野结衣
         * num : 50
         */

        public String content;
        public String icon;
        public String id;
        public String name;
        public String num;
        public List<InfoBean> info;


        public static class InfoBean {
            /**
             * grade : 8.1
             * id : 6
             * image : http://www.baidu.com/uploads/aaa/bbb
             * title : 影片6
             */

            public String grade;
            public String id;
            public String image;
            public String title;


        }
    }

    public static class HotBean {
        /**
         * add_time : 上新时间：01-01 08:00:00
         * content : 三
         * id : 3
         * logo : http://www.baidu.com/1
         * title : 推荐分类三
         * type : 1
         */

        public String add_time;
        public String content;
        public String id;
        public String logo;
        public String title;
        public String type;


    }

    public static class HotmoreBean  {
        /**
         * logo : http://www.baidu.com/uploads/三
         * title : 推荐分类三
         */

        public String logo;
        public String title;
        public String id;


    }

    public static class MustseeBean {
        /**
         * add_time : 上新时间：01-01 08:00:00
         * content : 一
         * id : 1
         * logo : http://www.baidu.com/0
         * title : 推荐分类一
         * type : 0
         */

        public String add_time;
        public String content;
        public String id;
        public String logo;
        public String title;
        public String type;

    }
}
