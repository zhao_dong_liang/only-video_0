package cn.wu1588.dancer.channel.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.cnsunrun.commonui.widget.titlebar.TitleBar;
import com.makeramen.roundedimageview.RoundedImageView;
import com.sunrun.sunrunframwork.bean.BaseBean;

import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import cn.wu1588.dancer.adp.StartDetailAdapter;
import cn.wu1588.dancer.channel.mode.NvYouDetaiBean;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.RecycleHelper;
import cn.wu1588.dancer.home.mode.MovieBean;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_NV_YOU_DETAIL_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_SCREEN_NV_YOU_LIST_CODE;

public class NvYouDetailActivity extends LBaseActivity {

    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.rIvStar)
    RoundedImageView rIvStar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvContent)
    TextView tvContent;
    @BindView(R.id.rbMore)
    RadioButton rbMore;
    @BindView(R.id.rbNews)
    RadioButton rbNews;
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tvPlayNum)
    TextView tvPlayNum;
    @BindView(R.id.tvJianjie)
    TextView tvJianjie;
    private String ac_id;
    private String select_id = "4";//筛选标签id  最多播放=3，最近更新=4
    private StartDetailAdapter startDetailAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nvyou_detail);
        ButterKnife.bind(this);
        initViews();
        initData();
        initListener();
    }

    private void initListener() {
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                //筛选标签id  最多播放=3，最近更新=4
                if (checkedId == R.id.rbMore) {
                    select_id = "3";
                    BaseQuestStart.getScreenNvyouList(that, ac_id, select_id);
                } else if (checkedId == R.id.rbNews) {
                    select_id = "4";
                    BaseQuestStart.getScreenNvyouList(that, ac_id, select_id);
                }
            }
        });
    }

    private void initData() {
        String id = getIntent().getStringExtra("id");
        BaseQuestStart.getNvYouDetail(that, id);
    }

    private void initViews() {
        RecycleHelper.setLinearLayoutManager(recyclerView, LinearLayoutManager.VERTICAL);
        startDetailAdapter = new StartDetailAdapter();
        recyclerView.setAdapter(startDetailAdapter);
        startDetailAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                MovieBean item = startDetailAdapter.getItem(position);
                CommonIntent.startMoviePlayerActivity(that,String.valueOf(item.id));
            }
        });
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode) {
            case GET_NV_YOU_DETAIL_CODE:
                if (bean.status == 1) {
                    NvYouDetaiBean nvYouDetaiBean = bean.Data();
                    updateUI(nvYouDetaiBean);
                }
                break;
            case GET_SCREEN_NV_YOU_LIST_CODE:
                if (bean.status == 1) {
                    List<MovieBean> list = bean.Data();
                    startDetailAdapter.setNewData(list);
                }
                break;
        }
        super.nofityUpdate(requestCode, bean);
    }

    private void updateUI(NvYouDetaiBean data) {
        ac_id = data.ac_id;
        GlideMediaLoader.loadHead(that, rIvStar, data.icon);
        tvTitle.setText(data.name);
        tvContent.setText("身高:" + data.stature + "   " + "三围:" + data.measur + "   " + data.title);
        tvPlayNum.setText(String.format(getString(R.string.video_num), data.num));
        tvJianjie.setText(data.content);
        BaseQuestStart.getScreenNvyouList(that, ac_id, select_id);

    }
}
