package cn.wu1588.dancer.channel.fragment;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.TextView;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemChildClickListener;
import com.cnsunrun.commonui.widget.roundwidget.QMUIRoundButton;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.sunrun.sunrunframwork.utils.EmptyDeal;
import com.sunrun.sunrunframwork.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.adp.LableScreenAdapter;
import cn.wu1588.dancer.adp.ScreenResultAdapter;
import cn.wu1588.dancer.adp.SecondAryLabelAdapter;
import cn.wu1588.dancer.channel.mode.LableScreenBean;
import cn.wu1588.dancer.channel.mode.MovieScreenBean;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.RecycleHelper;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_LABLE_SCREEN_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.SELECT_CONTENT_CODE;

/**
 * 标签筛选
 */
public class LabelScreenFragment extends LBaseFragment {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tvReset)
    TextView tvReset;
    @BindView(R.id.recyclerViewContent)
    RecyclerView recyclerViewContent;
    @BindView(R.id.btnSure)
    QMUIRoundButton btnSure;
    private LableScreenAdapter lableScreenAdapter;//一级标签
    private SecondAryLabelAdapter secondAryLabelAdapter;//二级标签
    private List<MovieScreenBean> movieBeans;
    private List<LableScreenBean> lableScreenBeans;

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_label_screen;
    }

    public static LabelScreenFragment newInstance() {
        LabelScreenFragment labelScreenFragment = new LabelScreenFragment();
        Bundle bundle = new Bundle();
        labelScreenFragment.setArguments(bundle);
        return labelScreenFragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        initData();
    }

    private void initData() {
        BaseQuestStart.getLableScreenList(LabelScreenFragment.this);
    }

    private void initViews() {
        RecycleHelper.setLinearLayoutManager(recyclerView, LinearLayoutManager.HORIZONTAL);
        RecycleHelper.setGridLayoutManager(recyclerViewContent, 2);
        lableScreenAdapter = new LableScreenAdapter();
        recyclerView.setAdapter(lableScreenAdapter);
        secondAryLabelAdapter = new SecondAryLabelAdapter();
        recyclerViewContent.setAdapter(secondAryLabelAdapter);

        lableScreenAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                List<LableScreenBean> data = lableScreenAdapter.getData();
//                secondAryLabelAdapter.setNewData(data.get(position).second);
                for (int i = 0; i < data.size(); i++) {
                    if (i == position) {
                        data.get(i).isSelect = true;
                    } else {
                        data.get(i).isSelect = false;
                    }
                }
                lableScreenAdapter.notifyDataSetChanged();
              showSencondLabel(data,position);
            }
        });
        recyclerViewContent.addOnItemTouchListener(new OnItemChildClickListener() {
            @Override
            public void onSimpleItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                LableScreenBean.SecondBean item = secondAryLabelAdapter.getItem(position);
                if (secondAryLabelAdapter.getSelectList().contains(item)){
                    secondAryLabelAdapter.getSelectList().remove(item);
                }else{
                    secondAryLabelAdapter.getSelectList().add(item);
                }
                btnSure.setVisibility(EmptyDeal.isEmpy(secondAryLabelAdapter.getSelectList())? View.GONE : View.VISIBLE);
                secondAryLabelAdapter.notifyDataSetChanged();
            }
        });
    }



    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        super.nofityUpdate(requestCode, bean);
        switch (requestCode) {
            case GET_LABLE_SCREEN_CODE:
                if (bean.status == 1) {
                    lableScreenBeans = bean.Data();
                    updateUI(lableScreenBeans);
                }
                break;
            case SELECT_CONTENT_CODE:
                if (bean.status == 1) {
                    movieBeans = bean.Data();
                    showScreenResult();
                }else {
                    UIUtils.shortM(bean.msg);
                }
                break;
        }
    }

    /**
     * 点击二级标签显示筛选结果
     */
    private void showScreenResult() {
        btnSure.setVisibility(View.GONE);
        ScreenResultAdapter screenResultAdapter = new ScreenResultAdapter();
        RecycleHelper.setLinearLayoutManager(recyclerViewContent, LinearLayoutManager.VERTICAL);
        recyclerViewContent.setAdapter(screenResultAdapter);
        screenResultAdapter.setNewData(movieBeans);
        screenResultAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                MovieScreenBean item = screenResultAdapter.getItem(position);
                CommonIntent.startMoviePlayerActivity(that,item.id);
            }
        });
    }
    /**
     *点击一级标签显示二级标签
     */
    private void showSencondLabel(List<LableScreenBean> data, int position) {
        RecycleHelper.setGridLayoutManager(recyclerViewContent, 2);
        recyclerViewContent.setAdapter(secondAryLabelAdapter);
        secondAryLabelAdapter.setNewData(data.get(position).second);
    }
    private void updateUI(List<LableScreenBean> lableScreenBeans) {
        if (!EmptyDeal.isEmpy(lableScreenBeans)) {
            lableScreenBeans.get(0).isSelect = true;
            lableScreenAdapter.setNewData(lableScreenBeans);
            secondAryLabelAdapter.setNewData(lableScreenBeans.get(0).second);
        }
    }

    @OnClick({R.id.tvReset, R.id.btnSure})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvReset:
                initData();
                showSencondLabel(lableScreenBeans,0);
                recyclerView.smoothScrollToPosition(0);
                if (!EmptyDeal.isEmpy(secondAryLabelAdapter.getSelectList())) {
                    secondAryLabelAdapter.getSelectList().clear();
                    btnSure.setVisibility(View.GONE);
                }
                break;
            case R.id.btnSure:
                String ids = Utils.listToString(secondAryLabelAdapter.getSelectList(), new Utils.DefaultToString(","));
                BaseQuestStart.getLabelSelectContent(LabelScreenFragment.this, ids);
                break;
        }
    }
}
