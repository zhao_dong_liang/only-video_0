package cn.wu1588.dancer.channel.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.UIUtils;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import cn.wu1588.dancer.adp.NvYouAdapter;
import cn.wu1588.dancer.channel.mode.NvyouBean;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.event.MessageEvent;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.PageLimitDelegate;
import cn.wu1588.dancer.common.util.RecycleHelper;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_NVYOU_LIST_CODE;

public class NvYouFragment extends LBaseFragment {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    private String order="1";
    private String id;
    private NvYouAdapter nvYouAdapter;
    PageLimitDelegate pageLimitDelegate = new PageLimitDelegate(new PageLimitDelegate.DataProvider() {
        @Override
        public void loadData(int page) {
            BaseQuestStart.getNvYouListData(NvYouFragment.this, page, order, id);
        }
    });

    public static NvYouFragment newInstance(String id) {
        NvYouFragment nvYouFragment = new NvYouFragment();
        Bundle bundle = new Bundle();
        bundle.putString("id",id);
        nvYouFragment.setArguments(bundle);
        return nvYouFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        id = getArguments().getString("id");
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        initEventBus();
    }

    private void initViews() {
        nvYouAdapter = new NvYouAdapter();
        recyclerView.setAdapter(nvYouAdapter);
        RecycleHelper.setGridLayoutManager(recyclerView, 3);
        pageLimitDelegate.attach(refreshLayout, recyclerView, nvYouAdapter);
        nvYouAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                NvyouBean item = nvYouAdapter.getItem(position);
                CommonIntent.startNvYouDetailActivity(that,item.id);
            }
        });
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_movie;
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode) {
            case GET_NVYOU_LIST_CODE:
                List<NvyouBean> nvyouBeans = bean.Data();
                if (bean.status == 1) {
                    pageLimitDelegate.setData(nvyouBeans);
//                    GetEmptyViewUtils.bindEmptyView(that, nvYouAdapter, R.drawable.ic_empty_default, "暂无数据", true);
                } else {
                    pageLimitDelegate.setData(nvyouBeans);
//                    GetEmptyViewUtils.bindEmptyView(that, nvYouAdapter, R.drawable.ic_empty_default, "暂无数据", true);
                    pageLimitDelegate.loadComplete();
                    nvYouAdapter.setEnableLoadMore(false);
                }
                break;
        }
        super.nofityUpdate(requestCode, bean);
    }
    @Subscribe
    public void eventBusMethod(MessageEvent event) {
        String type = event.getType();
        String content = event.getContent();
        if (TextUtils.equals("nvyou_type", type)) {
            order=content;
            UIUtils.showLoadDialog(that);
            pageLimitDelegate.refreshPage();
        }
    }
}
