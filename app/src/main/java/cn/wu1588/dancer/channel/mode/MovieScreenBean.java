package cn.wu1588.dancer.channel.mode;

import java.util.List;

import cn.wu1588.dancer.common.util.DataUtils;

public class MovieScreenBean {

    /**
     * id : 7
     * title : 影片7
     * image : https://test.cnsunrun.com/duboshiping_app/uploads/aaa/bbb
     * play_num : 7
     * label : ["国产","白虎"]
     * grade : 8.1
     * play_url : uploads/aaa/bbb
     */

    public String id;
    public String title;
    public String image;
    public int play_num;
    public String grade;
    public String play_url;
    public List<String> label;
    public String dealPlayNum(){
        if (play_num>1000){
            return DataUtils.div(play_num,10000,1) +"万";
        }else {
            return String.valueOf(play_num);
        }
    }

}
