package cn.wu1588.dancer.channel.mode;

import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.makeramen.roundedimageview.RoundedImageView;
import com.stx.xhb.xbanner.XBanner;
import com.sunrun.sunrunframwork.adapter.ViewHodler;
import com.sunrun.sunrunframwork.adapter.ViewHolderAdapter;
import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.sunrun.sunrunframwork.utils.EmptyDeal;

import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.wu1588.dancer.adp.GuanggaoAdapter;
import cn.wu1588.dancer.adp.ZhuantiAdapter;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.config.Const;

public class RecommendHeadMode {
    @BindView(R.id.llRenqiStarMore)
    LinearLayout llRenqiStarMore;
    @BindView(R.id.xbanner)
    XBanner xbanner;
    private Activity that;
    @BindView(R.id.recyclerViewZhuanti)
    RecyclerView recyclerViewZhuanti;
    //    @BindView(R.id.recyclerViewGuanggao)
//    RecyclerView recyclerViewGuanggao;
    @BindView(R.id.tvMore)
    TextView tvMore;
    @BindView(R.id.llMore)
    LinearLayout llMore;
    @BindView(R.id.gridViewChannel)
    GridView gridViewChannel;
    private ZhuantiAdapter zhuantiAdapter;
    private GuanggaoAdapter guanggaoAdapter;
    private List<List<ThemeaticBean.HotmoreBean>> hotmore;
    private List<ThemeaticBean.HotBean> hotBeanList;

    public RecommendHeadMode(View headView) {
        ButterKnife.bind(this, headView);
        that = (Activity) headView.getContext();
        int view = R.layout.view_reconmend_head;
        initViews();
    }

    private void initViews() {
        zhuantiAdapter = new ZhuantiAdapter();
        recyclerViewZhuanti.setLayoutManager(new LinearLayoutManager(that, LinearLayoutManager.HORIZONTAL, false));
        recyclerViewZhuanti.setAdapter(zhuantiAdapter);
        zhuantiAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                ThemeaticBean.MustseeBean item = zhuantiAdapter.getItem(position);
                CommonIntent.startHomeMoreClassifiListActivity(that,item.id, Const.FROM_CHANNEL,Const.ONE);
            }
        });

    }


    @OnClick({R.id.llMore, R.id.llRenqiStarMore})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llMore://热门专题
                if (!EmptyDeal.isEmpy(hotmore)) {
                    CommonIntent.startHotZhanTiMoreActivity(that);
                } else {
                    UIUtils.shortM("暂无更多");
                }
                break;
            case R.id.llRenqiStarMore://人气
                CommonIntent.startNvYouListActivity(that);
                break;
        }
    }

    public void setData(ThemeaticBean themeaticBean) {
        if (EmptyDeal.isEmpy(themeaticBean)) return;
        hotmore = themeaticBean.hotmore;
        zhuantiAdapter.setNewData(themeaticBean.mustsee);
//        guanggaoAdapter.setNewData(themeaticBean.addslide);
        //轮播图
       final List<ThemeaticBean.AddslideBean> bannerBeanList = themeaticBean.addslide;
        if (!EmptyDeal.isEmpy(bannerBeanList)) {
            //刷新数据之后，需要重新设置是否支持自动轮播
            xbanner.setAutoPlayAble(bannerBeanList.size() > 1);
            xbanner.setData(R.layout.home_banner_view, bannerBeanList, null);
            xbanner.loadImage(new XBanner.XBannerAdapter() {
                @Override
                public void loadBanner(XBanner banner, Object model, View view, int position) {
                    RoundedImageView roundedImageView = (RoundedImageView) view.findViewById(R.id.rIvBanner);
                    GlideMediaLoader.load(that, roundedImageView, bannerBeanList.get(position).image, R.drawable.home_banner_placle);
                }
            });
            xbanner.setOnItemClickListener(new XBanner.OnItemClickListener() {
                @Override
                public void onItemClick(XBanner banner, Object model, View view, int position) {
                    CommonIntent.startBrowser(that,bannerBeanList.get(position).url);
                }
            });
        }
        hotBeanList = themeaticBean.hot;
        if (!EmptyDeal.isEmpy(hotBeanList)){

            gridViewChannel.setAdapter(new ViewHolderAdapter<ThemeaticBean.HotBean>(that, hotBeanList, R.layout.item_grid_channel) {
                @Override
                public void fillView(ViewHodler viewHodler, ThemeaticBean.HotBean s, int i) {
                    viewHodler.setText(R.id.itemTitle, s.title);
                    GlideMediaLoader.load(that, viewHodler.getView(R.id.itemIcon), s.logo, R.drawable.ic_place_ovel);
                }
            });
            gridViewChannel.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    ThemeaticBean.HotBean hotBean = hotBeanList.get(position);
                    CommonIntent.startHomeMoreClassifiListActivity(that,hotBean.id, Const.FROM_CHANNEL,Const.TWO);
                }
            });
        }
    }
}
