package cn.wu1588.dancer.channel.fragment;

import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.makeramen.roundedimageview.RoundedImageView;
import com.stx.xhb.xbanner.XBanner;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.sunrun.sunrunframwork.utils.EmptyDeal;

import java.util.List;

import cn.wu1588.dancer.channel.mode.Channels;
import cn.wu1588.dancer.model.Ad;
import butterknife.BindView;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.config.Const;
import cn.wu1588.dancer.common.quest.BaseQuestStart;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.CHANNEL_LIST_CODE;

/**
 * 专栏推荐
 */
public class RecommendFragment extends LBaseFragment {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;

    private Adapter mAdapter;

    private View mMustChannelView;
    private View mBannerView;
    private View mHotChannelView;


    @Override
    public int getLayoutRes() {
        return R.layout.fragment_recomend;
    }

    public static RecommendFragment newInstance() {
        RecommendFragment recommendFragment = new RecommendFragment();
        Bundle bundle = new Bundle();
        recommendFragment.setArguments(bundle);
        return recommendFragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        refreshLayout.setOnRefreshListener(this::initData);
        initViews();
        initData();
    }

    private void initData() {
        refreshLayout.setRefreshing(true);
        BaseQuestStart.getChannelList(this);
    }

    private void initViews() {
        mMustChannelView = LayoutInflater.from(getContext()).inflate(R.layout.layout_channels_must_channel, recyclerView, false);
        mBannerView = LayoutInflater.from(getContext()).inflate(R.layout.layout_banner, recyclerView, false);
        mHotChannelView = LayoutInflater.from(getContext()).inflate(R.layout.layout_channels_hot, recyclerView, false);

        mAdapter = new Adapter();
        mAdapter.addHeaderView(mMustChannelView);
        mAdapter.addHeaderView(mBannerView);
        mAdapter.addHeaderView(mHotChannelView);
        recyclerView.setAdapter(mAdapter);

    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        super.nofityUpdate(requestCode, bean);
        switch (requestCode) {
            case CHANNEL_LIST_CODE:
//                Channels channels = bean.Data();
//                bindMustChannels(channels);
//                bindBanner(channels);
//                bindHotChannels(channels);
//                mAdapter.setNewData(channels.new_channel);
//                refreshLayout.setRefreshing(false);
                break;
            default:
                // ig
        }

    }

    private void bindMustChannels(Channels channels) {
        if (channels == null) {
            return;
        }
        List<Channels.MustChannel> must_channel = channels.must_channel;
        RecyclerView rvMustChannels = mMustChannelView.findViewById(R.id.rv_must_channel);
        rvMustChannels.setAdapter(new BaseQuickAdapter<Channels.MustChannel, BaseViewHolder>(R.layout.item_must_channel, must_channel) {
            {
                setOnItemClickListener((adapter, view, position) -> {
                    Channels.MustChannel item = getItem(position);
                    CommonIntent.startHomeMoreClassifiListActivity(that, item.id, Const.FROM_CHANNEL, Const.ONE);
                });
            }

            @Override
            protected void convert(BaseViewHolder helper, Channels.MustChannel item) {
                View view = helper.getView(R.id.iv_must_channel);
                GlideMediaLoader.load(getActivity(), view, item.background_image, R.drawable.ic_place_ad_img);
                helper.setText(R.id.tv_must_channel, item.title);
            }
        });
        int itemDecorationCount = rvMustChannels.getItemDecorationCount();
        for (int i = 0; i < itemDecorationCount; i++) {
            rvMustChannels.removeItemDecorationAt(i);
        }
        rvMustChannels.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                if (parent.getChildAdapterPosition(view) != parent.getAdapter().getItemCount() - 1) {
                    outRect.right = UIUtils.dip2px(getActivity(), 8);
                }
            }
        });
    }

    private void bindBanner(Channels channels) {
        if (channels == null) {
            return;
        }
        List<Ad> bannerBeanList = channels.ad;
        XBanner xbanner = mBannerView.findViewById(R.id.iv_banner);
        if (!EmptyDeal.isEmpy(bannerBeanList)) {
            //刷新数据之后，需要重新设置是否支持自动轮播
            xbanner.setAutoPlayAble(bannerBeanList.size() > 1);
            xbanner.setData(R.layout.home_banner_view, bannerBeanList, null);
            xbanner.loadImage((banner, model, view, position) -> {
                RoundedImageView roundedImageView = (RoundedImageView) view.findViewById(R.id.rIvBanner);
                GlideMediaLoader.load(that, roundedImageView, bannerBeanList.get(position).img_url, R.drawable.home_banner_placle);
            });
            xbanner.setOnItemClickListener((banner, model, view, position) -> CommonIntent.startLoadUrlWebActivity(that, bannerBeanList.get(position).outer_url, bannerBeanList.get(position)));
        }
    }

    private void bindHotChannels(Channels channels) {
        List<Channels.MustChannel> hot_channel = channels.hot_channel;
        RecyclerView recyclerView = mHotChannelView.findViewById(R.id.rv_hot_channel);
        mHotChannelView.findViewById(R.id.tv_more).setOnClickListener(v -> {
            CommonIntent.startHotZhanTiMoreActivity(that);
        });
        recyclerView.setAdapter(new BaseQuickAdapter<Channels.MustChannel, BaseViewHolder>(R.layout.item_channel_hot, hot_channel) {
            {
                setOnItemClickListener((adapter, view, position) -> {
                    Channels.MustChannel hotBean = getItem(position);
                    CommonIntent.startHomeMoreClassifiListActivity(that, hotBean.id, Const.FROM_CHANNEL, Const.TWO);
                });
            }

            @Override
            protected void convert(BaseViewHolder helper, Channels.MustChannel item) {
                GlideMediaLoader.load(getActivity(), helper.getView(R.id.iv_channel_hot_image), item.background_image, R.drawable.ic_place_arcimg);
                helper.setText(R.id.tv_channel_hot_title, item.title);
            }
        });
    }

    private class Adapter extends BaseQuickAdapter<Channels.MustChannel, BaseViewHolder> {

        Adapter() {
            super(R.layout.item_channels);
        }

        @Override
        protected void convert(BaseViewHolder helper, Channels.MustChannel item) {
            com.makeramen.roundedimageview.RoundedImageView firstImage = helper.getView(R.id.iv_channel_first_image);
            com.makeramen.roundedimageview.RoundedImageView secondImage = helper.getView(R.id.iv_channel_second_image);
            com.makeramen.roundedimageview.RoundedImageView thridImage = helper.getView(R.id.iv_channel_thrid_image);

            helper.getView(R.id.tv_channel_type_title).setOnClickListener(v -> CommonIntent.startHomeMoreClassifiListActivity(that, item.id, Const.FROM_CHANNEL, Const.ONE));

            List<Channels.Movie> movies = item.movies;
            if (movies == null || movies.isEmpty()) {
                firstImage.setVisibility(View.GONE);
                secondImage.setVisibility(View.GONE);
                thridImage.setVisibility(View.GONE);
                secondImage.setOnClickListener(null);
                thridImage.setOnClickListener(null);
                firstImage.setOnClickListener(null);
            } else {
                switch (movies.size()) {
                    case 1:
                        firstImage.setVisibility(View.VISIBLE);
                        secondImage.setVisibility(View.GONE);
                        thridImage.setVisibility(View.GONE);
                        GlideMediaLoader.load(getActivity(), firstImage, movies.get(0).image, R.drawable.ic_place_ad_img);
                        secondImage.setOnClickListener(null);
                        thridImage.setOnClickListener(null);
                        firstImage.setOnClickListener(v -> {
                            Channels.Movie movie = movies.get(0);
                            CommonIntent.startMoviePlayerActivity(getContext(), String.valueOf(movie.id));
                        });
                        break;
                    case 2:
                        firstImage.setVisibility(View.VISIBLE);
                        secondImage.setVisibility(View.VISIBLE);
                        thridImage.setVisibility(View.GONE);
                        GlideMediaLoader.load(getActivity(), firstImage, movies.get(0).image, R.drawable.ic_place_ad_img);
                        GlideMediaLoader.load(getActivity(), secondImage, movies.get(1).image, R.drawable.ic_place_ad_img);
                        firstImage.setOnClickListener(v -> {
                            Channels.Movie movie = movies.get(0);
                            CommonIntent.startMoviePlayerActivity(getContext(), String.valueOf(movie.id));
                        });
                        secondImage.setOnClickListener(v -> {
                            Channels.Movie movie = movies.get(1);
                            CommonIntent.startMoviePlayerActivity(getContext(), String.valueOf(movie.id));
                        });
                        thridImage.setOnClickListener(null);
                        break;
                    default:
                    case 3:
                        firstImage.setVisibility(View.VISIBLE);
                        secondImage.setVisibility(View.VISIBLE);
                        thridImage.setVisibility(View.VISIBLE);
                        GlideMediaLoader.load(getActivity(), firstImage, movies.get(0).image, R.drawable.ic_place_ad_img);
                        GlideMediaLoader.load(getActivity(), secondImage, movies.get(1).image, R.drawable.ic_place_ad_img);
                        GlideMediaLoader.load(getActivity(), thridImage, movies.get(2).image, R.drawable.ic_place_ad_img);
                        firstImage.setOnClickListener(v -> {
                            Channels.Movie movie = movies.get(0);
                            CommonIntent.startMoviePlayerActivity(getContext(), String.valueOf(movie.id));
                        });
                        secondImage.setOnClickListener(v -> {
                            Channels.Movie movie = movies.get(1);
                            CommonIntent.startMoviePlayerActivity(getContext(), String.valueOf(movie.id));
                        });
                        thridImage.setOnClickListener(v -> {
                            Channels.Movie movie = movies.get(2);
                            CommonIntent.startMoviePlayerActivity(getContext(), String.valueOf(movie.id));
                        });
                        break;
                }
            }
        }
    }
}
