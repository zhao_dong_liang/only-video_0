package cn.wu1588.dancer.channel.mode;

import java.util.List;

public class LableBean {

    public List<SelectFourBean> select_four;
    public List<SelectOneBean> select_one;
    public List<SelectThreeBean> select_three;
    public List<SelectTwoBean> select_two;



    public static class SelectFourBean {
        /**
         * id : 6
         * second : []
         * title : 国产
         */

        public String id;
        public String title;
        public List<?> second;

    }

    public static class SelectOneBean {
        /**
         * id : 1
         * name : 人气最高
         */

        public String id;
        public String name;
        public boolean isSelect;


    }

    public static class SelectThreeBean {
        /**
         * id : 3
         * name : 最多播放
         */

        public int id;
        public String name;


    }

    public static class SelectTwoBean {
        /**
         * id : 1
         * title : A杯
         */

        public String id;
        public String title;
        public boolean isSelect;


    }
}
