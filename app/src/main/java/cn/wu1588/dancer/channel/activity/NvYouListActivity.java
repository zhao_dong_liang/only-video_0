package cn.wu1588.dancer.channel.activity;

import android.os.Bundle;
import android.view.View;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemChildClickListener;
import com.cnsunrun.commonui.widget.titlebar.TitleBar;
import com.flyco.tablayout.SlidingTabLayout;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.utils.EmptyDeal;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import cn.wu1588.dancer.adp.LableHorizontalAdapter;
import cn.wu1588.dancer.channel.fragment.NvYouFragment;
import cn.wu1588.dancer.channel.mode.LableBean;
import cn.wu1588.dancer.channel.mode.XiongbeiBean;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.base.ViewPagerFragmentAdapter;
import cn.wu1588.dancer.common.event.MessageEvent;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.RecycleHelper;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_CHESTSELECT_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_LABLE_LIST_CODE;

/**
 * 列表
 */
public class NvYouListActivity extends LBaseActivity {

    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.recyclerViewSelectOne)
    RecyclerView recyclerViewSelectOne;
    @BindView(R.id.magicIndicator)
    SlidingTabLayout magicIndicator;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    private LableHorizontalAdapter lableHorizontalAdapter;
    private ViewPagerFragmentAdapter mVPAdapter;
    private ArrayList<Fragment> baseFragments;
    private List<String> mTitles;
    private List<XiongbeiBean> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nvyoulist);
        ButterKnife.bind(this);
        initViews();
        initData();
        initListener();
    }

    private void initViews() {
        baseFragments = new ArrayList<>();
        mTitles = new ArrayList<>();
        mVPAdapter = new ViewPagerFragmentAdapter(this.getSupportFragmentManager());

        RecycleHelper.setLinearLayoutManager(recyclerViewSelectOne, LinearLayoutManager.HORIZONTAL);
        lableHorizontalAdapter = new LableHorizontalAdapter();
        recyclerViewSelectOne.setAdapter(lableHorizontalAdapter);


    }

    private void initListener() {
        recyclerViewSelectOne.addOnItemTouchListener(new OnItemChildClickListener() {
            @Override
            public void onSimpleItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                List<LableBean.SelectOneBean> data = lableHorizontalAdapter.getData();
                for (int i = 0; i < data.size(); i++) {
                    if (i == position) {
                        data.get(i).isSelect = true;
                        EventBus.getDefault().post(new MessageEvent("nvyou_type",data.get(i).id));
                    } else {
                        data.get(i).isSelect = false;
                    }
                }
                lableHorizontalAdapter.notifyDataSetChanged();
            }
        });
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void initData() {
        BaseQuestStart.getLableList(that);//获取列表第一层标签
        BaseQuestStart.getChestselect(that);//获取列表标签

    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        super.nofityUpdate(requestCode, bean);
        switch (requestCode) {
            case GET_LABLE_LIST_CODE:
                if (bean.status == 1) {
                    LableBean lableBean = bean.Data();
                    updateUI(lableBean);
                }
                break;
            case GET_CHESTSELECT_CODE:
                if (bean.status == 1) {
                    list = bean.Data();
                    if (!EmptyDeal.isEmpy(list)) {
                        for (int i = 0; i < list.size(); i++) {
                            baseFragments.add(NvYouFragment.newInstance(list.get(i).id));
                            mTitles.add(list.get(i).title);
                        }
                    }
                    mVPAdapter.setFragments(baseFragments);
                    magicIndicator.setViewPager(viewPager, mTitles.toArray(new String[list.size()]), this, baseFragments);
                    viewPager.setCurrentItem(0);
                }
                break;
        }
    }

    private void updateUI(LableBean lableBean) {
        if (!EmptyDeal.isEmpy(lableBean.select_one)) {
            lableBean.select_one.get(0).isSelect = true;
        }
        lableHorizontalAdapter.setNewData(lableBean.select_one);
    }

}
