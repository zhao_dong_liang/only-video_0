package cn.wu1588.dancer.base;

import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerFragmentAdapter extends FragmentPagerAdapter {
    private List<? extends Fragment> fragments = new ArrayList<>();
    private Fragment mCurrentFragment;

    public ViewPagerFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int arg0) {
        // TODO Auto-generated method stub
        return fragments.get(arg0);
    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return fragments.size();
    }


    public List<? extends Fragment> getFragments() {
        return fragments;
    }

    public void setFragments(List<? extends Fragment> fragments) {
        this.fragments = fragments;
    }

    //用于区分具体属于哪个fragment
    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        mCurrentFragment = (Fragment) object;
        super.setPrimaryItem(container, position, object);
    }

    public Fragment getCurrentFragment() {
        return mCurrentFragment;
    }

}