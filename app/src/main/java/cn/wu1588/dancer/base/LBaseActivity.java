package cn.wu1588.dancer.base;

/**
 * @时间: 2017/5/19
 * @功能描述:
 */

import android.content.Context;
import android.os.Build;
import android.os.IBinder;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.lzy.okgo.OkGo;
import com.lzy.okgo.model.Progress;
import com.my.toolslib.http.utils.LzyResponse;
import com.my.toolslib.http.utils.OkHttpRequest;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.http.NAction;
import com.sunrun.sunrunframwork.http.cache.NetSession;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;
import com.sunrun.sunrunframwork.utils.log.Logger;

import org.greenrobot.eventbus.EventBus;

import java.io.File;

import cn.wu1588.dancer.common.logic.TokenRefreshLogic;

/**
 * 预留后面会增加的空间
 */

public class LBaseActivity extends TranslucentActivity implements OkHttpRequest {

    /**
     * 启用eventBus,不用手动关闭
     */
    public void initEventBus() {
        EventBus.getDefault().register(this);
    }

    @Override
    protected boolean isStatusContentDark() {
        return true;
    }

    @Override
    protected boolean isTranslucent() {
        return true;
    }

    @Override
    protected void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        OkGo.getInstance().cancelTag(this);
        super.onDestroy();
    }

    TokenRefreshLogic tokenRefreshLogic = new TokenRefreshLogic();

    @Override
    public void requestAsynGet(NAction action) {
//        tokenRefreshLogic.recordRequest(action);
//        if(!tokenRefreshLogic.isAuthCode(action.requestCode)&&!Config.getLoginInfo().isExpiresTime()){
//            BaseQuestStart.UserOauthToken(this);
//            return;
//        }
        super.requestAsynGet(action);
    }

    @Override
    public void requestAsynPost(NAction action) {
//        tokenRefreshLogic.recordRequest(action);
//        if(!tokenRefreshLogic.isAuthCode(action.requestCode)&&!Config.getLoginInfo().isExpiresTime()){
//            BaseQuestStart.UserOauthToken(this);
//            return;
//        }

        super.requestAsynPost(action);
    }

    @Override
    public void requestFinish() {
//        if (tokenRefreshLogic.isEmpty())
        super.requestFinish();
    }

    @Override
    public void loadFaild(int requestCode, BaseBean bean) {
//        tokenRefreshLogic.removeRequest(requestCode);
        super.loadFaild(requestCode, bean);
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
//        if (tokenRefreshLogic.isAuthCode(requestCode) && bean.status==home1) {
//            LoginInfo.TokenBean tokenBean=bean.Data();//刷新token
//            Config.getLoginInfo().setToken(tokenBean);
//            tokenRefreshLogic.retryRequest(this);//重新请求
//        } else if (bean.status == 2||!Config.getLoginInfo().isExpiresTime()) {
//
//        } else {
//            tokenRefreshLogic.removeRequest(requestCode);
//        }

        super.nofityUpdate(requestCode, bean);
    }

    @Override
    public NetSession getSession() {
        return super.getSession();
    }

    protected boolean isStrangePhone() {
        boolean strangePhone = "mx5".equalsIgnoreCase(Build.DEVICE)
                || "Redmi Note2".equalsIgnoreCase(Build.DEVICE)
                || "Z00A_1".equalsIgnoreCase(Build.DEVICE)
                || "hwH60-L02".equalsIgnoreCase(Build.DEVICE)
                || "hermes".equalsIgnoreCase(Build.DEVICE)
                || ("V4".equalsIgnoreCase(Build.DEVICE) && "Meitu".equalsIgnoreCase(Build.MANUFACTURER))
                || ("m1metal".equalsIgnoreCase(Build.DEVICE) && "Meizu".equalsIgnoreCase(Build.MANUFACTURER));

        Logger.E("lfj1115 ", " Build.Device = " + Build.DEVICE + " , isStrange = " + strangePhone);
        return strangePhone;
    }


    private long okGoRequestCode;

    @Override
    public void tokenInvalid() {

    }

    @Override
    public void httpLoadFail(String err) {
        ToastUtils.longToast(err);
    }

    @Override
    public void httpLoadFinal() {

    }

    @Override
    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {

    }

    @Override
    public void onProgress(Progress progress) {
    }
    @Override
    public void downLoadFinish(File file) {
    }

    /**
     * 点击页面空白处时，让键盘消失
     *
     * @param event 事件
     * @return boolean
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager mInputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (getCurrentFocus() != null && getCurrentFocus().getWindowToken() != null) {
                if (getCurrentFocus().getWindowToken() != null) {
                    mInputMethodManager.hideSoftInputFromWindow(
                            getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                }
            }
        }
        return super.onTouchEvent(event);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (isShouldHideKeyboard(v, ev)) {
                hideKeyboard(v.getWindowToken());
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时则不能隐藏
     *
     * @param v     View
     * @param event 事件
     * @return boolean
     */

    private boolean isShouldHideKeyboard(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)){
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0],
                    top = l[1],
                    bottom = top + v.getHeight(),
                    right = left + v.getWidth();
            return !(event.getX() > left) || !(event.getX() < right)
                    || !(event.getY() > top) || !(event.getY() < bottom);
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditText上，和用户用轨迹球选择其他的焦点
        return false;
    }

    /**
     * 获取InputMethodManager，隐藏软键盘
     *
     * @param token1 Ibinder
     */
    private void hideKeyboard(IBinder token1) {
        if (token1 != null) {
            InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(token1, InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

}
