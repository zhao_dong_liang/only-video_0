package cn.wu1588.dancer.other_user.adapter;

import android.util.Log;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.my.toolslib.DateUtil;

import java.util.List;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.my_video.bean.MyVideoBean;

public class AlbumAdp extends BaseQuickAdapter<MyVideoBean, BaseViewHolder> {

    public AlbumAdp(int layoutResId, @Nullable List<MyVideoBean> data) {
        super(layoutResId, data);
    }

    public AlbumAdp(int item_album_adp) {
        super(item_album_adp);
    }

    @Override
    protected void convert(BaseViewHolder helper, MyVideoBean data) {
        String strBySecond = DateUtil.getTimeStrBySecond(data.getPlay_time());
        helper
                .setText(R.id.time_tv, data.getDate())
                .setText(R.id.text_title, data.getTitle())
                .setText(R.id.time_tv, strBySecond + "\t\t0视频\t\t售价\t\t" + (data.getPrice() == null ? 0 : data.getPrice()))
                .addOnClickListener(R.id.image_delete);

    }
}
