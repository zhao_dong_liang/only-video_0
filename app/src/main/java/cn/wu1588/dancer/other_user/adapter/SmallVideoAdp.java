package cn.wu1588.dancer.other_user.adapter;

import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.my_video.bean.MyVideoBean;

public class SmallVideoAdp extends BaseQuickAdapter<MyVideoBean, BaseViewHolder> {

    public SmallVideoAdp(int my_video_item1) {
        super(my_video_item1);
    }

    @Override
    protected void convert(BaseViewHolder helper, MyVideoBean item) {
        ImageView imageView = helper.getView(R.id.img);
        GlideMediaLoader.load(mContext, imageView, item.getImage(), R.mipmap.img_def);
        helper.setText(R.id.text_num, item.getPlay_num());
    }
}
