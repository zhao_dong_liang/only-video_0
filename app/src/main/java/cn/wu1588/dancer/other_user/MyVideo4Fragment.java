package cn.wu1588.dancer.other_user;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.kongzue.baseokhttp.HttpRequest;
import com.kongzue.baseokhttp.listener.ResponseListener;
import com.kongzue.baseokhttp.util.Parameter;
import com.my.toolslib.http.utils.LzyResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.PageLimitDelegate;
import cn.wu1588.dancer.my_video.adapter.MyVideo3Adapter;
import cn.wu1588.dancer.my_video.adapter.MyVideo4Adapter;
import cn.wu1588.dancer.my_video.bean.MyVideoBean;
import cn.wu1588.dancer.my_video.bean.MyVideoData;
import cn.wu1588.dancer.utils.map.MD5Utils;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.DEL_MOVIE;

//我的视频fragment
public class MyVideo4Fragment extends LBaseFragment {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.num_tv)
    TextView num_tv;
    @BindView(R.id.text_original_application)
    TextView text_original_application;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    private boolean isFirstLoadData = true;
    @BindView(R.id.flayout_null)
    FrameLayout flayout_null;
    @BindView(R.id.ll_top_visiton)
    LinearLayout ll_top_visiton;
    private int uid;
    private int type;//   0"全部",
    //                        1"小视频",
//                        2"4K",
//                        3"教学",
//                        4"专辑",
//                        5"分享视频"
    private MyVideo4Adapter adapter;

    public static MyVideo4Fragment newInstance(int type, int uid) {
        MyVideo4Fragment fragment = new MyVideo4Fragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        bundle.putInt("uid", uid);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.myy_video_fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }

    @SuppressLint("NewApi")
    private void initViews() {
        num_tv.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.mipmap.icon_album_map, null),
                null, null, null);
        text_original_application.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.mipmap.icon_add_album, null), null, getResources().getDrawable(R.mipmap.icon_right_jump, null), null);
        type = getArguments().getInt("type");
        uid = getArguments().getInt("uid");


        if (uid != 0) {
            ll_top_visiton.setVisibility(View.GONE);
        }
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new MyVideo4Adapter();
        recyclerView.setAdapter(adapter);
        adapter.setSpanSizeLookup((gridLayoutManager, i) -> {
            MyVideoBean item = adapter.getItem(i);
            int type = item.getType();
            if (type == 1) {
                return 3;
            } else {
                return 1;
            }
        });
        adapter.setOnItemClickListener((adapter, view, position) -> {
            MyVideoBean bean = (MyVideoBean) adapter.getItem(position);
            CommonIntent.startMoviePlayerActivity(that, bean.getId());
        });
        adapter.setOnItemChildClickListener((adapter, view, position) -> {
            switch (view.getId()) {
                case R.id.image_delete:
                    MyVideoBean bean = (MyVideoBean) adapter.getItem(position);
                    HttpRequest.POST(getActivity(), DEL_MOVIE, new Parameter()
                                    .add("id", bean.getId())
                                    .add("md5", MD5Utils.md5())
                            , new ResponseListener() {
                                @Override
                                public void onResponse(String main, Exception error) {
                                    if (error == null) {
                                        List<MyVideoBean> data = adapter.getData();
                                        data.remove(bean);
                                        adapter.notifyDataSetChanged();
                                        BaseQuestStart.getMineVideos(MyVideo4Fragment.this, type, 0, 15);
                                    }
                                }
                            });
                    break;
            }
        });
        pageLimitDelegate.attach(refreshLayout, recyclerView, adapter);
    }

    private PageLimitDelegate<MyVideoBean> pageLimitDelegate = new PageLimitDelegate<>(new PageLimitDelegate.DataProvider() {
        @Override
        public void loadData(int page) {
            BaseQuestStart.getMineVideos(MyVideo4Fragment.this, type, page, 15);
        }
    });

    @Override
    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
        super.nofityUpdateUi(requestCode, response, view);
        if (response.code == 1) {
            isFirstLoadData = false;
            if (requestCode == BaseQuestStart.GET_MINE_VIDEOS_CODE) {
                MyVideoData data = (MyVideoData) response.data;
                if (data == null) {
                    return;
                }
                num_tv.setText("作品\t" + data.count);
                ArrayList<MyVideoBean> myVideoBeans = (ArrayList<MyVideoBean>) data.movies;
                if (myVideoBeans != null) {
                    pageLimitDelegate.setData(groupMyVideoBeansByUpdateDate(myVideoBeans));
                }
                flayout_null.setVisibility(myVideoBeans.size() == 0 ? View.VISIBLE : View.GONE);
            } else {
                num_tv.setText("作品\t0");
            }
        }
    }

    private List<MyVideoBean> groupMyVideoBeansByUpdateDate(ArrayList<MyVideoBean> myVideoBeans) {
        List<MyVideoBean> results = new ArrayList<>();
        if (myVideoBeans != null) {
            Map<String, List<MyVideoBean>> groupsByUpdateDate = new HashMap<>();
            for (MyVideoBean video : myVideoBeans) {
                String key = video.getUpdate_time().split(" ")[0].replace("-", ".");
                video.setType(2);
                List<MyVideoBean> videoBeanList = groupsByUpdateDate.get(key);
                if (videoBeanList == null) {
                    videoBeanList = new ArrayList<>();
                }
                videoBeanList.add(video);
                groupsByUpdateDate.put(key, videoBeanList);
            }
            for (Map.Entry<String, List<MyVideoBean>> entry : groupsByUpdateDate.entrySet()) {
                String key = entry.getKey();
                MyVideoBean videoBean = new MyVideoBean();
                videoBean.setDate(key);
                videoBean.setType(1);
                results.add(videoBean);
                results.addAll(entry.getValue());
            }
        }
        return results;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (!isFirstLoadData) {
            pageLimitDelegate.refreshPage();
        }
    }
}
