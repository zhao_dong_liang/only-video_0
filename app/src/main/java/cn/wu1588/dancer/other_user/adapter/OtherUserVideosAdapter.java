package cn.wu1588.dancer.other_user.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.my.toolslib.DateUtil;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.my_video.bean.MyVideoBean;

public class OtherUserVideosAdapter extends BaseQuickAdapter<MyVideoBean, BaseViewHolder> {
    public OtherUserVideosAdapter() {
        super(R.layout.other_user_videos_item);
    }

    @Override
    protected void convert(BaseViewHolder helper, MyVideoBean item) {

        String strBySecond = DateUtil.getTimeStrBySecond(item.getPlay_time());
        GlideMediaLoader.load(mContext, helper.getView(R.id.img), item.getImage(), R.mipmap.img_def);
        helper.setText(R.id.see_num, item.getPlay_num())
                .setText(R.id.video_name, item.getTitle())
                .setText(R.id.video_time, strBySecond)
                .setText(R.id.time_tv, item.getUpdate_time())
                .setText(R.id.fabulous_num, item.getLike_num() + "")
        ;


    }
}
