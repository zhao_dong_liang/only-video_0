package cn.wu1588.dancer.other_user;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.cnsunrun.commonui.widget.image.RoundImageView;
import com.flyco.tablayout.SlidingTabLayout;
import com.my.toolslib.http.utils.LzyResponse;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;
import com.tencent.mmkv.MMKV;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.base.ViewPagerFragmentAdapter;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.model.LoginInfo;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;
import cn.wu1588.dancer.my_video.fragment.MyVideo3Fragment;

public class OtherUserActiivty extends LBaseActivity {

    @BindView(R.id.user_info_layout)
    View user_info_layout;
    @BindView(R.id.follow_bt)
    Button follow_bt;
    @BindView(R.id.user_header_img)
    RoundImageView user_header_img;
    @BindView(R.id.user_name)
    TextView user_name;
    @BindView(R.id.user_auth_state)
    TextView user_auth_state;
    @BindView(R.id.user_labe_tv)
    TextView user_labe_tv;
    @BindView(R.id.user_info_pager)
    ViewPager user_info_pager;
    @BindView(R.id.tab_layout_1)
    SlidingTabLayout tab_layout_1;
    @BindView(R.id.video_num)
    TextView video_num;
    @BindView(R.id.album_num)
    TextView album_num;
    @BindView(R.id.fans_num)
    TextView fans_num;
    @BindView(R.id.fabulous_num)
    TextView fabulous_num;
    @BindView(R.id.image_finish)
    ImageView image_finish;

    private String attention_uid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setDark(CommonApp.getApplication().isBarTvColor());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actiivty_other_user);
        initViews();
        ininData();
    }

    String[] titles = new String[]{
            "全部", "小视频","4K","教学","专辑"
    };

    private void initViews() {
        MMKV mmkv = MMKV.defaultMMKV();
        int[] colors = {Color.parseColor(mmkv.decodeString("up_b_color", "#000000")),
                Color.parseColor(mmkv.decodeString("up_e_color", "#000000"))
        };
        GradientDrawable drawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, colors);
        drawable.setGradientType(GradientDrawable.LINEAR_GRADIENT);//设置线性渐变
        drawable.setOrientation(GradientDrawable.Orientation.LEFT_RIGHT);//设置渐变方向
//        user_info_layout.setBackground(drawable);
    }

    private void ininData() {
        attention_uid = getIntent().getStringExtra("attention_uid");
        BaseQuestStart.checkUserAttention(this, attention_uid);//获取用户是否关注
        BaseQuestStart.getUserInfoOk(this, attention_uid);//用户用户信息

        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(OtherUserVideosFragment.newInstance(0, attention_uid));
        fragments.add(OtherUserVideosFragment.newInstance(1, attention_uid));
        fragments.add(OtherUserVideosFragment.newInstance(2, attention_uid));
        fragments.add(OtherUserVideosFragment.newInstance(3, attention_uid));
        fragments.add(MyVideo4Fragment.newInstance(4, Integer.valueOf(attention_uid)));

        ViewPagerFragmentAdapter viewPagerFragmentAdapter = new ViewPagerFragmentAdapter(getSupportFragmentManager());
        viewPagerFragmentAdapter.setFragments(fragments);

        user_info_pager.setAdapter(viewPagerFragmentAdapter);
        tab_layout_1.setViewPager(user_info_pager, titles, this, fragments);
    }

    @OnClick({R.id.follow_bt, R.id.image_finish})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.follow_bt://关注
                BaseQuestStart.followUser(OtherUserActiivty.this, attention_uid);
                break;
            case R.id.image_finish:
                finish();
                break;
        }
    }

    @Override
    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
        super.nofityUpdateUi(requestCode, response, view);
        if (requestCode == BaseQuestStart.CHECK_USER_ATTENTION_CODE) {//是否关注
            if (response.code == 1) {//已关注
                follow_bt.setEnabled(false);
                follow_bt.setText("已关注");
                follow_bt.setBackgroundResource(R.drawable.gray_rect_4_bg);
            } else {
//                follow_bt.setEnabled(true);
            }
        } else if (requestCode == BaseQuestStart.ATTENTION_USER_CODE) {
            if (response.code == 1) {
                ToastUtils.longToast("关注成功");
                LzyResponse lzyResponse = new LzyResponse();
                lzyResponse.code = 1;
                nofityUpdateUi(BaseQuestStart.CHECK_USER_ATTENTION_CODE, lzyResponse, null);
            } else {
                ToastUtils.longToast("关注失败");
            }
        } else if (requestCode == BaseQuestStart.GET_USER_INFO_CODE) {//获取用户信息
            if (response.code == 1) {
                LoginInfo info = (LoginInfo) response.data;
                GlideMediaLoader.loadHead(this, user_header_img, info.icon);
                user_name.setText(info.nickname);
                video_num.setText(info.movie_count + "");
                album_num.setText(info.channel_count + "");
                fans_num.setText(info.attention_count + "");
                fabulous_num.setText(info.like_num + "");
            }
        }
    }
}
