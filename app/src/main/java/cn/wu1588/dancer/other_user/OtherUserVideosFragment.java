package cn.wu1588.dancer.other_user;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.my.toolslib.http.utils.LzyResponse;
import com.tencent.bugly.proguard.O;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.my_video.adapter.MyVideo3Adapter;
import cn.wu1588.dancer.my_video.fragment.MyVideoFragment;
import cn.wu1588.dancer.other_user.adapter.AlbumAdp;
import cn.wu1588.dancer.other_user.adapter.OtherUserVideosAdapter;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.PageLimitDelegate;
import cn.wu1588.dancer.mine.adapter.BuyAlbumAdapter;
import cn.wu1588.dancer.mine.mode.BuyAlbumBean;
import cn.wu1588.dancer.mine.mode.MyAlbumBean;
import cn.wu1588.dancer.my_video.bean.MyVideoBean;
import cn.wu1588.dancer.my_video.bean.MyVideoData;
import cn.wu1588.dancer.other_user.adapter.SmallVideoAdp;

//其他用户的视频列表
public class OtherUserVideosFragment extends LBaseFragment {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private int type;//0全部 ， 1小视频   2, 4K  3,教学  4，专辑
    private String uid;

    public static OtherUserVideosFragment newInstance(int type, String uid) {
        Bundle args = new Bundle();
        args.putInt("type", type);
        args.putString("uid", uid);
        OtherUserVideosFragment fragment = new OtherUserVideosFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.other_user_fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle == null) {
            return;
        }
        type = bundle.getInt("type", 0);
        uid = bundle.getString("uid", "0");

        //全部
        if (type == 0 || type == 2 || type == 3) {  // 2 == 4k  0 == 全部  3 == 教学
            recyclerView.setLayoutManager(new LinearLayoutManager(that));
            OtherUserVideosAdapter videosAdapter = new OtherUserVideosAdapter();
            recyclerView.setAdapter(videosAdapter);
            pageLimitDelegateVideos.attach(null, recyclerView, videosAdapter);
            videosAdapter.setOnItemClickListener((adapter, view1, position) -> {
                MyVideoBean bean = (MyVideoBean) adapter.getItem(position);
                CommonIntent.startMoviePlayerActivity(that, bean.getId());
            });
        } else if(type == 1){//1小视频
            GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);
            recyclerView.setLayoutManager(layoutManager);
            SmallVideoAdp smallVideoAdp = new SmallVideoAdp(R.layout.my_video_item1);
            recyclerView.setAdapter(smallVideoAdp);
            pageLimitDelegateVideos.attach(null,recyclerView,smallVideoAdp);
            smallVideoAdp.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                    MyVideoBean bean = (MyVideoBean) adapter.getItem(position);
                    CommonIntent.startMoviePlayerActivity(that, bean.getId());
                }
            });

        } else {//专辑
//            GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);
//            layoutManager.setOrientation(RecyclerView.VERTICAL);
//            MyVideo3Adapter albumAdp = new MyVideo3Adapter();
//            albumAdp.setSpanSizeLookup((gridLayoutManager, i) -> {
//                MyVideoBean item = albumAdp.getItem(i);
//                int type = item.getType();
//                if (type == 1) {
//                    return 3;
//                } else {
//                    return 1;
//                }
//            });
//            recyclerView.setAdapter(albumAdp);
//            pageLimitDelegateVideos.attach(null, recyclerView, albumAdp);
//            albumAdp.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
//                @Override
//                public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
//                    MyVideoBean bean = (MyVideoBean) adapter.getItem(position);
//                    CommonIntent.startMoviePlayerActivity(that, bean.getId());
//                }
//            });

//            recyclerView.setLayoutManager(new LinearLayoutManager(that));
//            //适配器
//            AlbumAdp albumAdp = new AlbumAdp(R.layout.item_album_adp);
//            recyclerView.setAdapter(albumAdp);
//            //放入pageLimitDelegateAlbums
//            pageLimitDelegateVideos.attach(null,recyclerView,albumAdp);
//            //点击事件
//            albumAdp.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
//                @Override
//                public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
//                    MyVideoBean bean = (MyVideoBean) adapter.getItem(position);
//                    CommonIntent.startMoviePlayerActivity(that, bean.getId());
//                }
//            });
//            GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);
//            layoutManager.setOrientation(RecyclerView.VERTICAL);
//            recyclerView.setLayoutManager(layoutManager);
//            final BuyAlbumAdapter albumAdapter = new BuyAlbumAdapter();
//            albumAdapter.setSpanSizeLookup((gridLayoutManager, i) -> {
//                Object item = albumAdapter.getItem(i);
//                return item instanceof BuyAlbumBean ? 3 : 1;
//            });
//            recyclerView.setAdapter(albumAdapter);
//            pageLimitDelegateAlbums.attach(null, recyclerView, albumAdapter);
        }
    }

    private PageLimitDelegate<Object> pageLimitDelegateAlbums = new PageLimitDelegate<>(new PageLimitDelegate.DataProvider() {
        @Override
        public void loadData(int page) {
//            BaseQuestStart.getUserChannel(OtherUserVideosFragment.this, uid);
            BaseQuestStart.getMineVideos(OtherUserVideosFragment.this, type, page, 15);

        }
    });
    private PageLimitDelegate<MyVideoBean> pageLimitDelegateVideos = new PageLimitDelegate<>(new PageLimitDelegate.DataProvider() {
        @Override
        public void loadData(int page) {
//            BaseQuestStart.getOtherUserVideos(OtherUserVideosFragment.this, type, page, 15, uid);
            //全部
            BaseQuestStart.getMineVideos(OtherUserVideosFragment.this, type, page, 15);
        }
    });

    @Override
    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
        super.nofityUpdateUi(requestCode, response, view);
        if (requestCode == BaseQuestStart.GET_MINE_VIDEOS_CODE) {//视频列表
            MyVideoData data = (MyVideoData) response.data;
            pageLimitDelegateVideos.setData(data.movies);
        } else {//专辑列表
            MyAlbumBean albumBean = (MyAlbumBean) response.data;
            List<Object> results = new ArrayList<Object>();
            if (albumBean == null) {
                pageLimitDelegateAlbums.setData(results);
                return;
            }
            if (albumBean.channels == null) {
                return;
            }
            for (BuyAlbumBean bean : albumBean.channels) {
                bean.viewType = 1;
                results.add(bean);
                results.addAll(bean.movies);
            }

            pageLimitDelegateAlbums.setData(results);
        }
    }
}
