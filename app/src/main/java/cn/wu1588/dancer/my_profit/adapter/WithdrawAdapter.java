package cn.wu1588.dancer.my_profit.adapter;

import android.annotation.SuppressLint;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.my_profit.bean.WithdrawBean;

/**
 * Created by zhou on 2021/1/18 14:42.
 */
public class WithdrawAdapter extends BaseQuickAdapter<WithdrawBean, BaseViewHolder> {

    public WithdrawAdapter(int layoutResId, @Nullable List<WithdrawBean> data) {
        super(layoutResId, data);
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void convert(BaseViewHolder helper, WithdrawBean item) {

        LinearLayout linearLayout = helper.getView(R.id.holder_with_draw_bg);
        TextView textView = helper.getView(R.id.holder_with_draw_txt);
        textView.setText(item.price+"元");
        if(item.isCheck()){
            textView.setTextColor(ContextCompat.getColor(mContext, R.color.color_FF9018));
            linearLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.gold_checked_bg));
        }else{
            textView.setTextColor(ContextCompat.getColor(mContext, R.color.color_262626));
            linearLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.gold_check_bg));
        }
    }
}
