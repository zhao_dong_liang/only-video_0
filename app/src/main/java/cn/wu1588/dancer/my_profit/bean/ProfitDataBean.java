package cn.wu1588.dancer.my_profit.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * 创建日期：2021/1/7 on 16:13
 * 作者:付珂
 * 描述:
 */
public class ProfitDataBean {

    /**
     * status : 1
     * msg : success
     * data : {"account":{"id":"17","member_id":"111","gold_account":"10","account":"0.00","ad_account":"1.00","wu_coin":"68","today_time":"27425","add_time":"153474","fans_time":"10942","totalzsy":"0","sytxlj":"0","4kzsy":"0","zjzsy":"0","xspzsy":"0","zzspzsy":"0","ycspzsy":"0","ycjxspzsy":"0","bfllj":"0","dzlj":"0"},"statistics":{"broadcast_income":0,"play_income":0,"4k_income":0,"zjzsy":0,"wu_coin_income":0,"task_income":0,"play_num":0},"broadcast_income":0,"records":[{"add_time":"","play":0,"4k":0,"wu_coin":0,"task":0,"play_num":0,"collection":0},{"add_time":"","play":0,"4k":0,"wu_coin":0,"task":0,"play_num":0,"collection":0}]}
     */

    private String status;
    private String msg;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * account : {"id":"17","member_id":"111","gold_account":"10","account":"0.00","ad_account":"1.00","wu_coin":"68","today_time":"27425","add_time":"153474","fans_time":"10942","totalzsy":"0","sytxlj":"0","4kzsy":"0","zjzsy":"0","xspzsy":"0","zzspzsy":"0","ycspzsy":"0","ycjxspzsy":"0","bfllj":"0","dzlj":"0"}
         * statistics : {"broadcast_income":0,"play_income":0,"4k_income":0,"zjzsy":0,"wu_coin_income":0,"task_income":0,"play_num":0}
         * broadcast_income : 0
         * records : [{"add_time":"","play":0,"4k":0,"wu_coin":0,"task":0,"play_num":0,"collection":0},{"add_time":"","play":0,"4k":0,"wu_coin":0,"task":0,"play_num":0,"collection":0}]
         */

        private AccountBean account;
        private StatisticsBean statistics;
        private int broadcast_income;
        private List<RecordsBean> records;

        public AccountBean getAccount() {
            return account;
        }

        public void setAccount(AccountBean account) {
            this.account = account;
        }

        public StatisticsBean getStatistics() {
            return statistics;
        }

        public void setStatistics(StatisticsBean statistics) {
            this.statistics = statistics;
        }

        public int getBroadcast_income() {
            return broadcast_income;
        }

        public void setBroadcast_income(int broadcast_income) {
            this.broadcast_income = broadcast_income;
        }

        public List<RecordsBean> getRecords() {
            return records;
        }

        public void setRecords(List<RecordsBean> records) {
            this.records = records;
        }

        public static class AccountBean {
            /**
             * id : 17
             * member_id : 111
             * gold_account : 10
             * account : 0.00
             * ad_account : 1.00
             * wu_coin : 68
             * today_time : 27425
             * add_time : 153474
             * fans_time : 10942
             * totalzsy : 0
             * sytxlj : 0
             * 4kzsy : 0
             * zjzsy : 0
             * xspzsy : 0
             * zzspzsy : 0
             * ycspzsy : 0
             * ycjxspzsy : 0
             * bfllj : 0
             * dzlj : 0
             */

            private String id;
            private String member_id;
            private String gold_account;
            private String account;
            private String ad_account;
            private String wu_coin;
            private String today_time;
            private String add_time;
            private String fans_time;
            private String totalzsy;
            private String sytxlj;
            @SerializedName("4kzsy")
            private String _$4kzsy;
            private String zjzsy;
            private String xspzsy;
            private String zzspzsy;
            private String ycspzsy;
            private String ycjxspzsy;
            private String bfllj;
            private String dzlj;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getMember_id() {
                return member_id;
            }

            public void setMember_id(String member_id) {
                this.member_id = member_id;
            }

            public String getGold_account() {
                return gold_account;
            }

            public void setGold_account(String gold_account) {
                this.gold_account = gold_account;
            }

            public String getAccount() {
                return account;
            }

            public void setAccount(String account) {
                this.account = account;
            }

            public String getAd_account() {
                return ad_account;
            }

            public void setAd_account(String ad_account) {
                this.ad_account = ad_account;
            }

            public String getWu_coin() {
                return wu_coin;
            }

            public void setWu_coin(String wu_coin) {
                this.wu_coin = wu_coin;
            }

            public String getToday_time() {
                return today_time;
            }

            public void setToday_time(String today_time) {
                this.today_time = today_time;
            }

            public String getAdd_time() {
                return add_time;
            }

            public void setAdd_time(String add_time) {
                this.add_time = add_time;
            }

            public String getFans_time() {
                return fans_time;
            }

            public void setFans_time(String fans_time) {
                this.fans_time = fans_time;
            }

            public String getTotalzsy() {
                return totalzsy;
            }

            public void setTotalzsy(String totalzsy) {
                this.totalzsy = totalzsy;
            }

            public String getSytxlj() {
                return sytxlj;
            }

            public void setSytxlj(String sytxlj) {
                this.sytxlj = sytxlj;
            }

            public String get_$4kzsy() {
                return _$4kzsy;
            }

            public void set_$4kzsy(String _$4kzsy) {
                this._$4kzsy = _$4kzsy;
            }

            public String getZjzsy() {
                return zjzsy;
            }

            public void setZjzsy(String zjzsy) {
                this.zjzsy = zjzsy;
            }

            public String getXspzsy() {
                return xspzsy;
            }

            public void setXspzsy(String xspzsy) {
                this.xspzsy = xspzsy;
            }

            public String getZzspzsy() {
                return zzspzsy;
            }

            public void setZzspzsy(String zzspzsy) {
                this.zzspzsy = zzspzsy;
            }

            public String getYcspzsy() {
                return ycspzsy;
            }

            public void setYcspzsy(String ycspzsy) {
                this.ycspzsy = ycspzsy;
            }

            public String getYcjxspzsy() {
                return ycjxspzsy;
            }

            public void setYcjxspzsy(String ycjxspzsy) {
                this.ycjxspzsy = ycjxspzsy;
            }

            public String getBfllj() {
                return bfllj;
            }

            public void setBfllj(String bfllj) {
                this.bfllj = bfllj;
            }

            public String getDzlj() {
                return dzlj;
            }

            public void setDzlj(String dzlj) {
                this.dzlj = dzlj;
            }
        }

        public static class StatisticsBean {
            /**
             * broadcast_income : 0
             * play_income : 0
             * 4k_income : 0
             * zjzsy : 0
             * wu_coin_income : 0
             * task_income : 0
             * play_num : 0
             */

            private int broadcast_income;
            private int play_income;
            @SerializedName("4k_income")
            private int _$4k_income;
            private int zjzsy;
            private int wu_coin_income;
            private int task_income;
            private int play_num;

            public int getBroadcast_income() {
                return broadcast_income;
            }

            public void setBroadcast_income(int broadcast_income) {
                this.broadcast_income = broadcast_income;
            }

            public int getPlay_income() {
                return play_income;
            }

            public void setPlay_income(int play_income) {
                this.play_income = play_income;
            }

            public int get_$4k_income() {
                return _$4k_income;
            }

            public void set_$4k_income(int _$4k_income) {
                this._$4k_income = _$4k_income;
            }

            public int getZjzsy() {
                return zjzsy;
            }

            public void setZjzsy(int zjzsy) {
                this.zjzsy = zjzsy;
            }

            public int getWu_coin_income() {
                return wu_coin_income;
            }

            public void setWu_coin_income(int wu_coin_income) {
                this.wu_coin_income = wu_coin_income;
            }

            public int getTask_income() {
                return task_income;
            }

            public void setTask_income(int task_income) {
                this.task_income = task_income;
            }

            public int getPlay_num() {
                return play_num;
            }

            public void setPlay_num(int play_num) {
                this.play_num = play_num;
            }
        }

        public static class RecordsBean {
            /**
             * add_time :
             * play : 0
             * 4k : 0
             * wu_coin : 0
             * task : 0
             * play_num : 0
             * collection : 0
             */

            private String add_time;
            private int play;
            @SerializedName("4k")
            private int _$4k;
            private int wu_coin;
            private int task;
            private int play_num;
            private int collection;

            public String getAdd_time() {
                return add_time;
            }

            public void setAdd_time(String add_time) {
                this.add_time = add_time;
            }

            public int getPlay() {
                return play;
            }

            public void setPlay(int play) {
                this.play = play;
            }

            public int get_$4k() {
                return _$4k;
            }

            public void set_$4k(int _$4k) {
                this._$4k = _$4k;
            }

            public int getWu_coin() {
                return wu_coin;
            }

            public void setWu_coin(int wu_coin) {
                this.wu_coin = wu_coin;
            }

            public int getTask() {
                return task;
            }

            public void setTask(int task) {
                this.task = task;
            }

            public int getPlay_num() {
                return play_num;
            }

            public void setPlay_num(int play_num) {
                this.play_num = play_num;
            }

            public int getCollection() {
                return collection;
            }

            public void setCollection(int collection) {
                this.collection = collection;
            }
        }
    }
}
