package cn.wu1588.dancer.my_profit.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.kongzue.baseframework.BaseActivity;
import com.kongzue.baseframework.interfaces.Layout;
import com.kongzue.baseframework.util.JumpParameter;

import java.util.ArrayList;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.my_profit.adapter.ViewPagerAdp;
import cn.wu1588.dancer.my_profit.fragment.ProfitDataFragment;
import cn.wu1588.dancer.my_profit.fragment.ProfitFragment;

@Layout(R.layout.activity_profit)
public class ProfitActivity extends BaseActivity {

    private RadioGroup profitRgp;
    private ViewPager viewPager;
    private ArrayList<Fragment> list = new ArrayList<>();
    private ViewPagerAdp viewPagerAdp;
    private ImageView image_finish;

    public void initViews(){

        image_finish = findViewById(R.id.image_finish);
        profitRgp = findViewById(R.id.profit_rgp);
        viewPager = findViewById(R.id.viewPager);
        list.add(new ProfitDataFragment());
        list.add(new ProfitFragment());

        RadioButton rb = (RadioButton) profitRgp.getChildAt(0);
        rb.setChecked(true);
        rb.setTextSize(23);

        viewPagerAdp = new ViewPagerAdp(getSupportFragmentManager(), list);
        viewPager.setOffscreenPageLimit(list.size());
        viewPager.setCurrentItem(0);
        viewPager.setAdapter(viewPagerAdp);
    }

    @Override
    public void initDatas(JumpParameter parameter) {

    }

    @Override
    public void setEvents() {
        //viewPager的滑动监听
        viewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                //获取当前位置的RadioButton
                RadioButton rb = (RadioButton) profitRgp.getChildAt(position);
                //设置为true
                rb.setChecked(true);
                rb.setTextSize(23);
            }
        });
        //RadioGroup的事件监听
        profitRgp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                for (int i = 0; i < profitRgp.getChildCount(); i++) {
                    RadioButton rb = (RadioButton) profitRgp.getChildAt(i);
                    if (rb.isChecked()) {
                        viewPager.setCurrentItem(i, true);
                        rb.setTextSize(23);
                    }else{
                        rb.setTextSize(15);
                    }
                }
            }
        });
        image_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
