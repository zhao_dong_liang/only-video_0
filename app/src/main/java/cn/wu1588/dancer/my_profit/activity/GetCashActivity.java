package cn.wu1588.dancer.my_profit.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.my.toolslib.StringUtils;
import com.my.toolslib.http.utils.LzyResponse;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;
import com.tencent.mmkv.MMKV;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.quest.BaseQuestConfig;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.common.util.PageLimitDelegate;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;
import cn.wu1588.dancer.mine.mode.AccountBean;
import cn.wu1588.dancer.my_profit.adapter.GetCashListAdapter;
import cn.wu1588.dancer.my_profit.bean.GetCashBean;

public class GetCashActivity extends LBaseActivity {
    @BindView(R.id.title_bar)
    TitleBar title_bar;
    @BindView(R.id.total_money_layout)
    View total_money_layout;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.get_cash_account)
    TextView get_cash_account;
    @BindView(R.id.total_money_tv)
    TextView total_money_tv;
    @BindView(R.id.can_get_cash_tv)
    TextView can_get_cash_tv;
    private GetCashListAdapter adapter;
    //提现页面
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setDark(CommonApp.getApplication().isBarTvColor());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_cash);
        initViews();
    }
    PageLimitDelegate<GetCashBean> pageLimitDelegate=new PageLimitDelegate<>(new PageLimitDelegate.DataProvider() {
        @Override
        public void loadData(int page) {
            BaseQuestStart.getCashList(GetCashActivity.this,GetCashActivity.this);
        }
    });
    private void initViews(){
        MMKV mmkv = MMKV.defaultMMKV();
        int[] colors = {Color.parseColor(mmkv.decodeString("up_b_color","#000000")),
                Color.parseColor(mmkv.decodeString("up_e_color","#000000"))
        };
        GradientDrawable drawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT,colors);
        drawable.setGradientType(GradientDrawable.LINEAR_GRADIENT);//设置线性渐变
        drawable.setOrientation(GradientDrawable.Orientation.LEFT_RIGHT);//设置渐变方向
        total_money_layout.setBackground(drawable);
        title_bar.setTitleBarBackgroundDrawable(CommonApp.getApplication().getStateBarDrawable());

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter=new GetCashListAdapter();
        recyclerView.setAdapter(adapter);
        pageLimitDelegate.attach(null,recyclerView,adapter);
        pageLimitDelegate.setPageEnable(false);


        BaseQuestStart.checkIsCashSecret(this);//判断用户是否设置了支付密码


        if (!StringUtils.isNull(Config.getLoginInfo().ali_name)){
            get_cash_account.setText(Config.getLoginInfo().ali_name);
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        BaseQuestStart.getUserAccount(this);//获取用户总金额
    }

    @OnClick({R.id.bind_zhifubao_layout,R.id.get_cash_bt})
    public void onClick(View v){
        switch (v.getId()){
            case R.id.bind_zhifubao_layout://绑定支付宝
                if (StringUtils.isNull(Config.getLoginInfo().ali_name)){
                    CommonIntent.startBindZhiFuBaoDialog(this,1,0,"0");
                }
                break;
            case R.id.get_cash_bt://提现
                if (StringUtils.isNull(Config.getLoginInfo().ali_name)){
                    CommonIntent.startBindZhiFuBaoDialog(this,1,0,"0");
                }else {//提现
                    if (bean!=null){
                        CommonIntent.startBindZhiFuBaoDialog(this,1,1,bean.account);
                    }else {
                        ToastUtils.longToast("总金额获取失败");
                    }

                }
                break;
        }
    }
private  AccountBean bean;
    @Override
    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
        super.nofityUpdateUi(requestCode, response, view);
        if (requestCode== BaseQuestConfig.GET_GET_CASH_LIST_CODE){
            if (response.code==1){
                List<GetCashBean> cashBeans= (List<GetCashBean>) response.data;
                pageLimitDelegate.setData(cashBeans);
            }
        }else if (requestCode==BaseQuestStart.USER_ACCOUNT_CODE){//获取用户余额
            if (response.code==1){
                 bean= (AccountBean) response.data;
                total_money_tv.setText(bean.account);
                can_get_cash_tv.setText("可提现余额(元)："+bean.account+"元");
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==1&&data!=null){
            Config.getLoginInfo().ali_name=data.getStringExtra("account");
            if (!StringUtils.isNull(Config.getLoginInfo().ali_name)){
                get_cash_account.setText(Config.getLoginInfo().ali_name);
            }
        }
    }
}
