package cn.wu1588.dancer.my_profit.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.my.toolslib.http.utils.LzyResponse;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.common.util.PageLimitDelegate;
import cn.wu1588.dancer.my_video.bean.MyVideoBean;
import cn.wu1588.dancer.my_video.bean.MyVideoData;

public class ProfitFragment extends LBaseFragment {

//    private ProfitVideoAdapter adapter;

    public static ProfitFragment newInstance() {
        Bundle args = new Bundle();
        ProfitFragment fragment = new ProfitFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public int getLayoutRes() {
        return R.layout.profit_fragment;
    }

    PageLimitDelegate<MyVideoBean> pageLimitDelegate=new PageLimitDelegate<>(new PageLimitDelegate.DataProvider() {
        @Override
        public void loadData(int page) {
            BaseQuestStart.getOtherUserVideos(ProfitFragment.this,1,page,15, Config.getLoginInfo().id);
        }
    });
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }

    private void initViews(){
//        recyclerView.setLayoutManager(new LinearLayoutManager(that));
//         adapter=new ProfitVideoAdapter();
//        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
//                MyVideoBean bean= (MyVideoBean) adapter.getItem(position);
//                CommonIntent.startMoviePlayerActivity(that,bean.getId());
//            }
//        });
//        recyclerView.setAdapter(adapter);
//        pageLimitDelegate.attach(null,recyclerView,adapter);
//

    }

    @Override
    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
        super.nofityUpdateUi(requestCode, response, view);
        if (requestCode==BaseQuestStart.GET_MINE_VIDEOS_CODE){//视频列表
            MyVideoData data= (MyVideoData) response.data;
            pageLimitDelegate.setData(data.movies);
        }
    }
}
