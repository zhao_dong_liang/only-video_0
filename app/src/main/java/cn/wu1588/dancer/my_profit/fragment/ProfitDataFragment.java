package cn.wu1588.dancer.my_profit.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.Gson;
import com.kongzue.baseokhttp.HttpRequest;
import com.kongzue.baseokhttp.listener.ResponseListener;
import com.kongzue.baseokhttp.util.Parameter;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.mine.mode.BuyAlbumBean;
import cn.wu1588.dancer.my_profit.adapter.ProfitDataAdapter;
import cn.wu1588.dancer.my_profit.bean.ProfitDataBean;

import static android.content.Context.MODE_PRIVATE;

public class ProfitDataFragment extends LBaseFragment implements BaseQuickAdapter.OnItemChildClickListener {

    @BindView(R.id.recycle)
    RecyclerView recycle;
    @BindView(R.id.broadcast_income)
    TextView broadcastIncome;
    @BindView(R.id.play_income)
    TextView playIncome;
    @BindView(R.id.k_income)
    TextView kIncome;
    @BindView(R.id.ji_income)
    TextView jiIncome;
    @BindView(R.id.withdrawal)
    TextView withdrawal;

    String withdrawMoney;

    private ProfitDataAdapter adapter;
    private String userid;

    @Override
    public int getLayoutRes() {
        return R.layout.profit_data_fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        getData();
    }

    private void initViews() {
        if (getActivity().getSharedPreferences("login", MODE_PRIVATE) != null) {
            SharedPreferences login = getActivity().getSharedPreferences("login", MODE_PRIVATE);
            if (login.getString("userid", "") != null) {
                userid = login.getString("userid", "");
            }
        }

        recycle.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new ProfitDataAdapter(R.layout.profit_data_list_item);
        recycle.setAdapter(adapter);


    }

    //获取数据
    private void getData() {
        HttpRequest.POST(getContext(), "http://app.myylook.com/Appapi/Channel/UserCenter/get_user_income",
                new Parameter().add("uid", Integer.valueOf(userid)),
                new ResponseListener() {
                    @Override
                    public void onResponse(String response, Exception error) {
                        ProfitDataBean bean = new Gson().fromJson(response, ProfitDataBean.class);
                        ProfitDataBean.DataBean data = bean.getData();
                        if (data == null) {
                            return;
                        }
                        ProfitDataBean.DataBean.AccountBean account = data.getAccount();
                        if (account == null) {
                            return;
                        }
                        String account1 = account.getAccount() == null ? "" : account.getAccount();
                        String xspzsy = account.getXspzsy() == null ? "" : account.getXspzsy();
                        String money4k = account.get_$4kzsy() == null ? "" : account.get_$4kzsy();
                        String zjzsy = account.getZjzsy() == null ? "" : account.getZjzsy();
                        withdrawMoney = account.getAccount() == null ? "" : account.getAccount();

                        broadcastIncome.setText(account1);
                        playIncome.setText(xspzsy);
                        kIncome.setText(money4k);
                        jiIncome.setText(zjzsy);

                    }
                });
    }

    @Override
    public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        BuyAlbumBean bean = (BuyAlbumBean) adapter.getItem(position);
        switch (view.getId()) {
           /* case R.id.img01:
                CommonIntent.startMoviePlayerActivity(that,bean.movies.get(0).getId());
                break;
            case R.id.img02:
                CommonIntent.startMoviePlayerActivity(that,bean.movies.get(1).getId());
                break;
            case R.id.img03:
                CommonIntent.startMoviePlayerActivity(that,bean.movies.get(2).getId());
                break;
            case R.id.linear:
                CommonIntent.startMyAlbumVidesActivity(that,bean.id);
                break;*/
        }

    }

    public static ProfitDataFragment newInstance() {
        Bundle args = new Bundle();
        ProfitDataFragment fragment = new ProfitDataFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @OnClick({R.id.withdrawal})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.withdrawal:
                CommonIntent.startWithdrawActivity(that, withdrawMoney);
                break;
            default:
                break;
        }
    }

}
