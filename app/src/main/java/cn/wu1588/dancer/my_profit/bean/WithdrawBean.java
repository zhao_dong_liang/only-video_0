package cn.wu1588.dancer.my_profit.bean;

/**
 * Created by zhou on 2021/1/18 14:43.
 */
public class WithdrawBean {

    public String price;
    public boolean check;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }
}
