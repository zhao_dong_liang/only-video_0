package cn.wu1588.dancer.my_profit.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.aty.AlumDetailsPlayerActivity;
import cn.wu1588.dancer.base.BaseAty;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.common.util.RecycleHelper;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;
import cn.wu1588.dancer.mine.mode.BuyAlbumBean;
import cn.wu1588.dancer.my_profit.adapter.WithdrawAdapter;
import cn.wu1588.dancer.my_profit.bean.WithdrawBean;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.cnsunrun.commonui.widget.roundwidget.QMUIRoundButton;
import com.kongzue.baseframework.interfaces.BindView;
import com.kongzue.baseframework.interfaces.DarkStatusBarTheme;
import com.kongzue.baseframework.interfaces.Layout;
import com.kongzue.baseframework.util.JumpParameter;
import com.tencent.bugly.proguard.T;

import java.util.ArrayList;
import java.util.List;

@Layout(R.layout.activity_withdraw)
@DarkStatusBarTheme(true)
public class WithdrawActivity extends BaseAty implements BaseQuickAdapter.OnItemClickListener {

    @BindView(R.id.title_bar)
    private TitleBar titleBar;
    @BindView(R.id.txtWithdrawMoney)
    private TextView txtWithdrawMoney;
    @BindView(R.id.ll_weixin)
    private LinearLayout ll_weixin;
    @BindView(R.id.ll_zhifubao)
    private LinearLayout ll_zhifubao;
    @BindView(R.id.txt_go_binding)
    private TextView txt_go_binding;
    @BindView(R.id.withdraw_recy)
    private RecyclerView withdraw_recy;
    @BindView(R.id.btnSure)
    private QMUIRoundButton btnSure;
    private WithdrawAdapter adapters;
    private List<WithdrawBean> list = new ArrayList<WithdrawBean>();
    private String withdrawMoney;//可提现金额
    private int choosePay = -1;

    public static void start(Context context,  String withdrawMoney) {
        Intent intent = new Intent(context, WithdrawActivity.class);
        intent.putExtra("broadcast_income", withdrawMoney);;
        context.startActivity(intent);
    }

    @Override
    public void initViews() {
        ll_weixin.setSelected(true);
        RecycleHelper.setGridLayoutManager(withdraw_recy, 2);
        choosePay = 0;
    }

    @Override
    public void initDatas(JumpParameter parameter) {

        withdrawMoney =  getIntent().getStringExtra("broadcast_income");

        txtWithdrawMoney.setText(withdrawMoney);

        for (String s : getResources().getStringArray(R.array.withdrawArray)){
            WithdrawBean bean = new WithdrawBean();
            bean.setPrice(s);
            list.add(bean);
        }
        adapters = new WithdrawAdapter(R.layout.item_with_draw,list);
        withdraw_recy.setAdapter(adapters);

        adapters.setOnItemClickListener(this);
    }

    @Override
    public void setEvents() {
        titleBar.setRightAction(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: 2021/1/18 跳转 提现记录 
            }
        });
        
        
        txt_go_binding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: 2021/1/18 跳转绑定 支付方式 页面
            }
        });


        ll_weixin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_weixin.setSelected(true);
                ll_zhifubao.setSelected(false);
            }
        });

        ll_zhifubao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_zhifubao.setSelected(true);
                ll_weixin.setSelected(false);
            }
        });

        btnSure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Integer.parseInt(withdrawMoney) < 50){
                    ToastUtils.showShort("您的金额不足以提现！");
                    return;
                }else{
                    if(ll_weixin.isSelected()){
                        ToastUtils.showShort("微信！");
                        // TODO: 2021/1/19 唤起微信
                    }else{
                        ToastUtils.showShort("支付宝！");
                        // TODO: 2021/1/19  唤起支付宝
                    }
                }
            }
        });
    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {

        ToastUtils.showShort("下标：" + position);

        for (WithdrawBean info : adapters.getData()) {
            info.setCheck(false);
        }
        adapters.getItem(position).setCheck(true);
        adapters.notifyDataSetChanged();
    }
}