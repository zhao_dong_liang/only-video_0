package cn.wu1588.dancer.my_profit.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.my_profit.bean.GetCashBean;

public class GetCashListAdapter extends BaseQuickAdapter<GetCashBean, BaseViewHolder> {
    public GetCashListAdapter() {
        super(R.layout.get_cash_list_item);
    }

    @Override
    protected void convert(BaseViewHolder helper, GetCashBean item) {

        helper.setText(R.id.time_tv,item.add_time)
                .setText(R.id.money_tv,"-"+item.money+"元");
    }
}
