package cn.wu1588.dancer.my_profit.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.my.toolslib.DateUtil;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.my_video.bean.MyVideoBean;

public class ProfitVideoAdapter extends BaseQuickAdapter<MyVideoBean, BaseViewHolder> {
    public ProfitVideoAdapter() {
        super(R.layout.profit_video_item);
    }

    @Override
    protected void convert(BaseViewHolder helper, MyVideoBean item) {
        String updateTime = item.getUpdate_time();
        GlideMediaLoader.load(mContext,helper.getView(R.id.img),item.getImage(),R.mipmap.img_def);
        String strBySecond = DateUtil.getTimeStrBySecond(item.getPlay_time());
        helper.setText(R.id.see_num,item.getPlay_num())
                .setText(R.id.video_time,strBySecond)
                .setText(R.id.time_tv,updateTime.split(" ")[0])
        ;
    }
}
