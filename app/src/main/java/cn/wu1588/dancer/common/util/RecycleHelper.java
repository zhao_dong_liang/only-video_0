package cn.wu1588.dancer.common.util;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by WQ on 2017/10/12.
 */

public class RecycleHelper {
    public static void setLinearLayoutManager(RecyclerView recyclerView, int orientation) {
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), orientation, false));
    }

    public static void setGridLayoutManager(RecyclerView recyclerView, int spanCount) {
        recyclerView.setLayoutManager(new GridLayoutManager(recyclerView.getContext(), spanCount));
    }

    public static void setHasHeaderGridLayoutManager(RecyclerView recyclerView, int spanCount, boolean isHasHeader) {
        if (isHasHeader) {
            final GridLayoutManager gridLayoutManager = new GridLayoutManager(recyclerView.getContext(), spanCount);
            gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return position == 0 ? gridLayoutManager.getSpanCount() : 1;
                }
            });
            recyclerView.setLayoutManager(gridLayoutManager);
        }
    }
}
