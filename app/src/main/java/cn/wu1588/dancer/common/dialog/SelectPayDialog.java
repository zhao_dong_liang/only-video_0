package cn.wu1588.dancer.common.dialog;

import android.app.Dialog;
import android.content.Context;

import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import cn.wu1588.dancer.mine.mode.PayChannelBean;


/**
 * 选择支付
 */

public class SelectPayDialog extends Dialog {

    private  List<PayChannelBean> vipBean;
    private PaySelectAdapter mAdapter;

    private OnViewClickListener onViewClickListener;

    public interface OnViewClickListener {
        void onItemClick(String content);
    }

    public SelectPayDialog(@NonNull Context context, List<PayChannelBean> vipBean, OnViewClickListener onViewClickListener) {
        super(context);
        Window window = this.getWindow();
        window.requestFeature(Window.FEATURE_NO_TITLE);
        View popupView = View.inflate(context, R.layout.layout_dialog_pay_select, null);
        window.setContentView(popupView);
        this.vipBean = vipBean;
        initViews(popupView, context, onViewClickListener);
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        lp.height = 720;
//        lp.windowAnimations = R.style.bottomInWindowAnim;
        lp.gravity = Gravity.BOTTOM;
        window.setAttributes(lp);
        window.setBackgroundDrawableResource(android.R.color.transparent);
        SelectPayDialog.this.show();
    }

    private void initViews(final View popupView, final Context context, final OnViewClickListener onViewClickListener) {

        //选择
        RecyclerView recyclerView = popupView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
        recyclerView.setAdapter(mAdapter = new PaySelectAdapter(context));
        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                mAdapter.setPosition(position);
            }
        });
        mAdapter.setNewData(vipBean);


        //取消
        popupView.findViewById(R.id.iv_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectPayDialog.this.dismiss();
            }
        });
        //确定
        popupView.findViewById(R.id.btnSure).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vipBean != null) {
                    onViewClickListener.onItemClick(String.valueOf(vipBean.get(mAdapter.selectPosition).id));
                }
                SelectPayDialog.this.dismiss();
            }
        });
    }

    public class PaySelectAdapter extends BaseQuickAdapter<PayChannelBean, BaseViewHolder> {
        private Context context;
        private int selectPosition;

        PaySelectAdapter(Context context) {
            super(R.layout.item_pay_select);
            this.context = context;
        }

        @Override
        protected void convert(BaseViewHolder helper, PayChannelBean item) {
            helper.setText(R.id.tv_text,item.title);
            //选项
            if (helper.getLayoutPosition() == selectPosition) {
                helper.setBackgroundRes(R.id.iv_check, R.drawable.vip_btn_xuanze_selected);
            } else {
                helper.setBackgroundRes(R.id.iv_check, R.drawable.vip_btn_xuanze_normal);
            }

        }

        public void setPosition(int position) {
            selectPosition = position;
            notifyDataSetChanged();
        }
    }

}
