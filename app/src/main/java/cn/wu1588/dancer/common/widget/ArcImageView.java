package cn.wu1588.dancer.common.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import android.util.AttributeSet;

import cn.wu1588.dancer.R;

/**
 * 圆弧的ImgView
 */
public class ArcImageView extends AppCompatImageView {
    /*
     *弧形高度
     */
    private int mArcHeight;
    private static final String TAG = "ArcImageView";
    Paint mPaint;
    public ArcImageView(Context context) {
        this(context, null);
    }

    public ArcImageView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ArcImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ArcImageView);
        mArcHeight = typedArray.getDimensionPixelSize(R.styleable.ArcImageView_arcHeight, 0);
        mPaint = new Paint();
        mPaint.setColor(Color.RED);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        //弧形部分
//        Path path = new Path();
//        path.moveTo(0, getHeight() - mArcHeight);
//        path.quadTo(getWidth() / 2, getHeight(), getWidth(), getHeight() - mArcHeight);
//        canvas.drawPath(path, mPaint);

        Path path = new Path();
        path.moveTo(0, 0);
        path.lineTo(0,getHeight()- 2*mArcHeight);
//        path.quadTo(getWidth()/2, getHeight() , getWidth() ,getHeight() - 2 * mArcHeight);
        path.quadTo(getWidth()/2, getHeight()+2*mArcHeight, getWidth() ,getHeight() - 2 * mArcHeight);
        path.lineTo(getWidth(), 0);
        path.close();
        canvas.clipPath(path);
        super.onDraw(canvas);
    }

}

