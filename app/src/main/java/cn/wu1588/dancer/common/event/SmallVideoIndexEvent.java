package cn.wu1588.dancer.common.event;

import cn.wu1588.dancer.home.mode.MovieBean;

/**
 * 小视频列表滚动下标同步
 */
public class SmallVideoIndexEvent {
    private   int index;
    private MovieBean bean;
    public SmallVideoIndexEvent(int index){
        this.index=index;
    }

    public int getIndex() {
        return index;
    }

    public MovieBean getBean() {
        return bean;
    }

    public void setBean(MovieBean bean) {
        this.bean = bean;
    }
}
