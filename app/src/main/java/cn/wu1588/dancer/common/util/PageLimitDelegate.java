package cn.wu1588.dancer.common.util;

import android.util.Log;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.sunrun.sunrunframwork.utils.EmptyDeal.empty;
import static com.sunrun.sunrunframwork.utils.EmptyDeal.size;

/**
 * 分页代理类
 * Created by WQ on 2017/5/26.
 */

public class PageLimitDelegate<T> {
    public int page = 1;//当前页
    DataProvider provider;//数据提供者,主要实现loadData 方法即可
    BaseQuickAdapter<T,?> quickAdapter;//适配器
    SwipeRefreshLayout refreshLayout;//下拉刷新控件
    AtomicBoolean isLoadMore=new AtomicBoolean(false);//是否处于加载状态
    int maxPageSize=10;//每页最大值

    public void setPage(int page){
        this.page=page;
    }
    public PageLimitDelegate(DataProvider provider) {
        this.provider
                = provider;
    }
    boolean isAttach=false;
    boolean pageEnable=true;
    /**
     * 附加列表信息,设置加载刷新信息
     * @param refreshLayout
     * @param recyclerView
     * @param quickAdapter
     */
private BaseQuickAdapter.RequestLoadMoreListener loadMoreListener;

    public BaseQuickAdapter.RequestLoadMoreListener getLoadMoreListener() {
        return loadMoreListener;
    }

    public SwipeRefreshLayout getRefreshLayout() {
        return refreshLayout;
    }

    public void attach(SwipeRefreshLayout refreshLayout, RecyclerView recyclerView, BaseQuickAdapter<T,?> quickAdapter) {
        this.quickAdapter=quickAdapter;
        if (refreshLayout!=null){
            this.refreshLayout=refreshLayout;
            refreshLayout.setEnabled(true);
            refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    refreshPage();
                }
            });
        }
        quickAdapter.setEnableLoadMore(true);
        quickAdapter.disableLoadMoreIfNotFullPage(recyclerView);
        quickAdapter.setOnLoadMoreListener(loadMoreListener=new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                Log.i("NetServer:","onLoadMoreRequested");
                isLoadMore.set(true);
                page++;
                provider.loadData(page);
            }
        }, recyclerView);
        provider.loadData(page);
        isAttach=true;
    }

    public void attachCommect(RecyclerView recyclerView, BaseQuickAdapter<T,?> quickAdapter) {
        this.quickAdapter=quickAdapter;
        quickAdapter.setEnableLoadMore(true);
        quickAdapter.disableLoadMoreIfNotFullPage(recyclerView);
        quickAdapter.setOnLoadMoreListener(loadMoreListener=new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                Log.i("NetServer:","onLoadMoreRequested");
                isLoadMore.set(true);
                page++;
                provider.loadData(page);
            }
        }, recyclerView);
        isAttach=true;
    }
    /**
     * 刷新
     */
    public void refreshPage() {
        page = 1;
        isLoadMore.set(false);
        provider.loadData(page);
    }

    /**
     * 设置数据,
     * @param data
     */
    public  void setData(List<T> data){
        maxPageSize=Math.max(maxPageSize,size(data));
        if((isLoadMore.get() ||page!=1)&&pageEnable) {
            if(empty(data)){
                page--;
                quickAdapter.setEnableLoadMore(false);
                quickAdapter.loadMoreEnd();
            }else {
                quickAdapter.addData(data);
            }
            loadComplete();
        }else {
            loadComplete();
            quickAdapter.setNewData(data);
            quickAdapter.setEnableLoadMore(pageEnable&&(size(data)>=maxPageSize));
        }
    }
    public void autoStopAnim(){
        refreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                refreshLayout.setRefreshing(false);
            }
        },20);
    }


    /**
     * 加载/刷新完成
     */
    public void loadComplete(){
        isLoadMore.set(false);
        if (refreshLayout!=null){
            refreshLayout.setRefreshing(false);
        }
        quickAdapter.loadMoreComplete();

    }

    /**
     * 数据提供者接口,用于进行加载动作
     */
    public interface DataProvider {
        public void loadData(int page);
    }


    public static void addData2List(List src, List data) {
        if(data != null) {
            Iterator var3 = data.iterator();

            while(true) {
                while(var3.hasNext()) {
                    Object object = var3.next();
                    int index = src.indexOf(object);
                    if(index != -1) {
                        src.set(index, object);
                    } else {
                        src.add(object);
                    }
                }

                return;
            }
        }
    }

    public boolean isAttach() {
        return isAttach;
    }

    public void setPageEnable(boolean pageEnable) {
        this.pageEnable = pageEnable;
    }
}
