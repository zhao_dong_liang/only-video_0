package cn.wu1588.dancer.common.boxing;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.DrawableRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.sunrun.sunrunframwork.utils.EmptyDeal;

import java.io.File;

import cn.wu1588.dancer.R;


/**
 * Created by WQ on 2017/5/5.
 */

public class GlideMediaLoader {
    public static void load(Object context, View imgview, String path, int placeholder) {
        if (!String.valueOf(path).startsWith("http")) {
             path = "http://" + path;
        }
        if(EmptyDeal.isEmpy(path))return;

        GlideUrl cookie = new GlideUrl(path, new LazyHeaders.Builder().addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 RedOne (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36")
.build());
        DrawableRequestBuilder<GlideUrl> stringDrawableRequestBuilder = with(context)
                .load(cookie)
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(false);
        // TODO 2019/12/28 目前需求所有图片暂不设置预加载图
        placeholder = 0;
        if (placeholder != 0) {
            stringDrawableRequestBuilder
                    .placeholder(placeholder);
        } ;
        stringDrawableRequestBuilder.into((ImageView) imgview);

//        with(context)
//                .load(path).centerCrop().dontAnimate()
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .placeholder(placeholder).into((ImageView) imgview);
    }


    public static void load(Object context, View imgview, String path) {
        load(context, imgview, path, 0);
    }
    public static void load(Context context, ImageView imgview, File file) {
        if (context==null){
            return;
        }
        if (context instanceof Activity){
            Activity activity= (Activity) context;
            if (activity.isDestroyed()){
                return;
            }
        }
        Glide.with(context).load(file).placeholder(0).error(0).into(imgview);
    }

    public static void loadHead(Object context, View imgview, String path) {
        load(context, imgview, path, R.drawable.me_user_def);
    }

    static RequestManager with(Object context) {
        if (context instanceof Activity) {
            return Glide.with((Activity) context);
        } else if (context instanceof Fragment) {
            return Glide.with((android.app.Fragment) context);
        } else if (context instanceof Context) {
            return Glide.with((Context) context);
        }
        return null;
    }

    public static void loadLargeImg(Object context, View imgview, String path, int placeholder){
        if (!String.valueOf(path).startsWith("http")) {
            path = "http://" + path;
        }
        if(EmptyDeal.isEmpy(path))return;

        GlideUrl cookie = new GlideUrl(path, new LazyHeaders.Builder().addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 RedOne (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36")
                .build());
        DrawableRequestBuilder<GlideUrl> stringDrawableRequestBuilder = with(context)
                .load(cookie)
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true);
        if (placeholder != 0) {
            stringDrawableRequestBuilder
                    .placeholder(placeholder);
        } ;
        stringDrawableRequestBuilder.into((ImageView) imgview);

    }

}
