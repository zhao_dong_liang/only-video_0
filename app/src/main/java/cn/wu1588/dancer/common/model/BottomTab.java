package cn.wu1588.dancer.common.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.flyco.tablayout.listener.CustomTabEntity;

import cn.wu1588.dancer.common.CommonApp;


/**
 * Created by WQ on 2017/4/17.
 */

public class BottomTab implements CustomTabEntity {
    public String title;
    public int selectedIcon;
    public int unSelectedIcon;

    public Bitmap selectedBtp;
    public Bitmap unSelectedBtp;
    public Context context;

    public BottomTab(String title, int selectedIcon, int unSelectedIcon) {
        this.title = title;
        this.selectedIcon = selectedIcon;
        this.unSelectedIcon = unSelectedIcon;
    }
    public BottomTab(String title, Bitmap selectedBtp, Bitmap unSelectedBtp,Context context) {
        this.title = title;
        this.selectedBtp = selectedBtp;
        this.unSelectedBtp = unSelectedBtp;
        this.context=context;
    }
    public static CustomTabEntity createTab(String title, int selectedIcon, int unSelectedIcon){
        return new BottomTab(title, selectedIcon, unSelectedIcon);
    }
    public static CustomTabEntity createTab(String title, Bitmap selectedBtp, Bitmap unSelectedBtp,Context context){
        return new BottomTab(title, selectedBtp, unSelectedBtp,context);
    }
    @Override
    public String getTabTitle() {
        return title;
    }

    @Override
    public int getTabSelectedIcon() {
        return selectedIcon;
    }
    @Override
    public int getTabUnselectedIcon() {
        return unSelectedIcon;
    }

    @Override
    public Bitmap getTabSelectedBtp() {
        if (selectedBtp==null){
//            selectedBtp= BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_place_ovel);
            selectedBtp= BitmapFactory.decodeResource(context.getResources(), selectedIcon);
        }
        return selectedBtp;
    }

    @Override
    public Bitmap getTabUnSelectedBtp() {
        if (context==null){
            context= CommonApp.getApplication().getBaseContext();
        }
        if (unSelectedBtp==null){
            unSelectedBtp= BitmapFactory.decodeResource(context.getResources(), selectedIcon);
//            unSelectedBtp= BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_place_ovel);
        }
        return unSelectedBtp;
    }


}
