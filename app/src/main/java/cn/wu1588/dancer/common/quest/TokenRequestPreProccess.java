package cn.wu1588.dancer.common.quest;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.SyncHttpClient;
import com.sunrun.sunrunframwork.http.BaseRequestPreproccess;
import com.sunrun.sunrunframwork.http.NAction;
import com.sunrun.sunrunframwork.http.NetUtils;

import java.lang.reflect.Field;

import cn.wu1588.dancer.common.model.LoginInfo;

/**
 * 添加token请求头
 * Created by WQ on 2017/11/21.
 */

public class TokenRequestPreProccess extends BaseRequestPreproccess {

    public TokenRequestPreProccess() {
        AsyncHttpClient synClient = NetUtils.getAsynHttpClient();
        synClient.setTimeout(30000);
        synClient.setConnectTimeout(30000);
        SSLSocketFactoryEx.setSSLSocketFactory(NetUtils.getAsynHttpClient(), addTokenInner());
    }

    @Override
    public NAction proccess(NAction action) {
//        addToken();
        return super.proccess(action);
    }

    public static void addToken() {
        LoginInfo loginInfo = Config.getLoginInfo();
        NetUtils.getAsynHttpClient().addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 RedOne (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36");

        if (loginInfo.isValid()) {
            NetUtils.getAsynHttpClient().addHeader("Authorization", loginInfo.id + " " + loginInfo.id);
            SyncHttpClient syncHttpClient = addTokenInner();
            if (syncHttpClient != null) {
                syncHttpClient.addHeader("Authorization", loginInfo.id + " " + loginInfo.id);
                syncHttpClient.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 RedOne (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36");

            }
        }
    }

    public static SyncHttpClient addTokenInner() {
        Field field = null;
        try {
            field = NetUtils.class.getDeclaredField("synClient");
            field.setAccessible(true);
            SyncHttpClient synClient = (SyncHttpClient) field.get(null);
            return synClient;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
