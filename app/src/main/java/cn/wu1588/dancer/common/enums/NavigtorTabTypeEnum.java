package cn.wu1588.dancer.common.enums;

//主界面的底部导航类型
public enum NavigtorTabTypeEnum {

    OLD_HOME_PAGE_TYPE(1,"原首页"),
    ALBUM_PAGE_TYPE(2,"专辑"),
    HOME_PAGE_TYPE(3,"首页"),
    SMALL_VIDEO_TYPE(4,"小视频"),
    MY_TYPE(5,"新我的"),
    OLD_MY_TYPE(6,"旧我的"),
    ALUM(7, "教学专辑");

    private int code;
    private String value;
    NavigtorTabTypeEnum(int code,String value){
        this.code=code;
        this.value=value;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
