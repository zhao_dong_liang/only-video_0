package cn.wu1588.dancer.common.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;



import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;
import cn.wu1588.dancer.R;

import static android.content.Context.TELEPHONY_SERVICE;

public class Utils {
    public static Uri ZOOM_PHOTO_URI = null;
    //状态栏-颜色配置工具。
    public static void statusBar(Activity activity, int color) {
        Window window = activity.getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //修改为深色，因为我们把状态栏的背景色修改为主题色白色，默认的文字及图标颜色为白色，导致看不到了。
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }

    //bitmap to byte[]
    static byte[] bmpToByteArray(Bitmap bitmap, boolean isRecycle) {
        ByteArrayOutputStream byteOutput = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteOutput);
        if (isRecycle) {
            bitmap.recycle();
        }
        byte[] result = byteOutput.toByteArray();
        try {
            byteOutput.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    //微信分享 buildTransaction
    static String buildTransaction(final String type) {
        return TextUtils.isEmpty(type) ? String.valueOf(System.currentTimeMillis()) :
                type + System.currentTimeMillis();
    }

    //手机号验证
    public static boolean isPhoneNum(String phone) {
        String telRegex = "^1[0-9]{10}";
        Pattern r = Pattern.compile(telRegex);
        Matcher m = r.matcher(phone);
        return m.matches();
    }

    //获取当前手机系统上的手机号码
    @SuppressLint("MissingPermission")
    public static String phoneNumber(Context context) {
            return ((TelephonyManager) context.getSystemService(TELEPHONY_SERVICE)).getLine1Number();
    }

    //调用系统相册
    @SuppressLint("IntentReset")
    public static void callAlbum(Activity activity){
        Intent album = new Intent(Intent.ACTION_PICK, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
        album.setType("image/*");
//        activity.startActivityForResult(album, Common.ALBUM_REQUEST_CODE);
    }

    //图片压缩剪裁
    public static void cutPhoto(Activity activity, Uri uri){
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", 300);
        intent.putExtra("outputY", 300);
        intent.putExtra("scale", true);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        File storageDir = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
//        try {
//            Uri cutImg = Uri.fromFile(File.createTempFile(Common.ZOOM,".png",storageDir));
//            intent.putExtra(MediaStore.EXTRA_OUTPUT, cutImg);
//            ZOOM_PHOTO_URI = cutImg;
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        intent.putExtra("return-data", false);
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        intent.putExtra("noFaceDetection", true);
//        activity.startActivityForResult(intent, Common.ALBUM_ZOOM_CODE);
    }
    /**
     * 时间转换格式
     * 具体到年月日 yyyy-MM-dd
     * 具体到年月日时分秒 yyyy-MM-dd HH:mm:ss
     * */
    public static String timeFormat(Date s){
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat a = new SimpleDateFormat("yyyy-MM-dd");
        return a.format(s);
    }

    //关闭recycler 刷新动画
    public static void closeDefaultAnimator(RecyclerView mRvCustomer) {
        if(null==mRvCustomer)return;
        mRvCustomer.getItemAnimator().setAddDuration(0);
        mRvCustomer.getItemAnimator().setChangeDuration(0);
        mRvCustomer.getItemAnimator().setMoveDuration(0);
        mRvCustomer.getItemAnimator().setRemoveDuration(0);
        ((SimpleItemAnimator) mRvCustomer.getItemAnimator()).setSupportsChangeAnimations(false);
    }
    public static int dp2px(Context context, float dpValue){
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    //图片   处理
    public static Bitmap changeBitmapSize(Context context, int size){
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.mipmap.find_release_img);
        int width = bitmap.getWidth();
        Log.d("----------->w" ,width+"");
        int height = bitmap.getHeight();
        Log.d("----------->h",height+"");
        float scaleWidth =((float)size/width);
        float scaleHeight = ((float)size/height);
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth,scaleHeight);
        bitmap = Bitmap.createBitmap(bitmap,0,0,width,height,matrix,true);
        return bitmap;
    }
}
