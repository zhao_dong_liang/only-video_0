package cn.wu1588.dancer.common.util.ppvod;

public class PPVodQuickUploadResult {
    private int ifExist;

    private String duration;

    private String rpath;

    private String path;

    private String pic;

    private String pics;

    private int size;

    private String gif;

    private String qr;

    private String orgfile;

    private String url;

    private String suffix;

    private String mp4;

    private String share;

    public void setIfExist(int ifExist) {
        this.ifExist = ifExist;
    }

    public int getIfExist() {
        return this.ifExist;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDuration() {
        return this.duration;
    }

    public void setRpath(String rpath) {
        this.rpath = rpath;
    }

    public String getRpath() {
        return this.rpath;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return this.path;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getPic() {
        return this.pic;
    }

    public void setPics(String pics) {
        this.pics = pics;
    }

    public String getPics() {
        return this.pics;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return this.size;
    }

    public void setGif(String gif) {
        this.gif = gif;
    }

    public String getGif() {
        return this.gif;
    }

    public void setQr(String qr) {
        this.qr = qr;
    }

    public String getQr() {
        return this.qr;
    }

    public void setOrgfile(String orgfile) {
        this.orgfile = orgfile;
    }

    public String getOrgfile() {
        return this.orgfile;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return this.url;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getSuffix() {
        return this.suffix;
    }

    public void setMp4(String mp4) {
        this.mp4 = mp4;
    }

    public String getMp4() {
        return this.mp4;
    }

    public void setShare(String share) {
        this.share = share;
    }

    public String getShare() {
        return this.share;
    }

}