package cn.wu1588.dancer.common.dialog;

import android.app.Dialog;
import android.content.Context;

/**
 * Created by wangchao on 2019-02-12.
 */
public class BaseDialog extends Dialog {
    private int res;

    public BaseDialog(Context context, int theme, int res) {
        super(context, theme);
        // TODO 自动生成的构造函数存根
        setContentView(res);
        this.res = res;
        setCanceledOnTouchOutside(false);
    }
}
