package cn.wu1588.dancer.common.util.ppvod;

public class Log {

    private static final String TAG = "PPVodTag";
    public static void info(String s) {
        android.util.Log.i(TAG, s);
    }

    public static void error(String s) {
        android.util.Log.e(TAG, s);
    }

    public static void debug(String s) {
        android.util.Log.d(TAG, s);
    }
}