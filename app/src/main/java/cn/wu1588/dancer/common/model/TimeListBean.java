package cn.wu1588.dancer.common.model;

public class TimeListBean {
    String time_sta;
    String time_time;
    String time_miao;
    String nickname;

    public String getTime_sta() {
        return time_sta;
    }

    public void setTime_sta(String time_sta) {
        this.time_sta = time_sta;
    }

    public String getTime_time() {
        return time_time;
    }

    public void setTime_time(String time_time) {
        this.time_time = time_time;
    }

    public String getTime_miao() {
        return time_miao;
    }

    public void setTime_miao(String time_miao) {
        this.time_miao = time_miao;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public String toString() {
        return "TimeListBean{" +
                "time_sta='" + time_sta + '\'' +
                ", time_time='" + time_time + '\'' +
                ", time_miao='" + time_miao + '\'' +
                ", nickname='" + nickname + '\'' +
                '}';
    }
}
