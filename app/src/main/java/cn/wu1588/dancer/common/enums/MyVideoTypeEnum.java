package cn.wu1588.dancer.common.enums;

//我的视频的种类
public enum MyVideoTypeEnum {
    ALL_TYPE(0,"所有视频"),
    PUBLIC_TYPE(1,"公开视频"),
    CHARGE_TYPE(2,"收费视频"),
    PRIVATE_TYPE(3,"私密视频"),
    ALBUM_TYPE(4,"我的专辑");
    private int code;
    private String value;

    MyVideoTypeEnum(int code,String value){
        this.code=code;
        this.value=value;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

