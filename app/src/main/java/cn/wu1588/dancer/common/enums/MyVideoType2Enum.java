package cn.wu1588.dancer.common.enums;

//我的视频的种类 改
public enum MyVideoType2Enum {
    ALL_TYPE(0,"全部"),
    PUBLIC_TYPE(1,"小视频"),
    CHARGE_TYPE(2,"4K"),
    PRIVATE_TYPE(3,"教学"),
    ALBUM_TYPE(4,"专辑"),
    SHARE_VIDEOS(5,"分享视频");
    private int code;
    private String value;

    MyVideoType2Enum(int code,String value){
        this.code=code;
        this.value=value;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

