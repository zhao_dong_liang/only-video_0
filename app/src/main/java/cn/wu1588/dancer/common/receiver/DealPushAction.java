package cn.wu1588.dancer.common.receiver;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.text.TextUtils;


import com.sunrun.sunrunframwork.http.utils.JsonDeal;
import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.sunrun.sunrunframwork.utils.EmptyDeal;
import com.sunrun.sunrunframwork.utils.Utils;
import com.sunrun.sunrunframwork.utils.log.Logger;

import androidx.core.app.NotificationManagerCompat;

import org.greenrobot.eventbus.EventBus;

import cn.jpush.android.api.JPushInterface;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.common.quest.Config;

/**
 * 处理推送动作
 *
 * @author WQ 下午1:17:37
 */
public class DealPushAction {
    public DealPushAction(Context context, Intent intent) {
        String action = intent.getAction();
        if (action == null)
            return;
        printMsg(intent);
        switch (action) {
            case "cn.jpush.android.intent.NOTIFICATION_OPENED":
                // //通知消息打开
                clickHandler(context, getCustomMsg(intent));
                Logger.E("消息打开");
                break;
            case "cn.jpush.android.intent.REGISTRATION":// 注册成功
                String device_token = intent.getExtras().getString(
                        JPushInterface.EXTRA_REGISTRATION_ID);
                Config.putConfigInfo(context, "device_token", device_token);// 保存设备号
                break;
            case "cn.jpush.android.intent.MESSAGE_RECEIVED":// 接收到了自定义消息
                try {
                    String message = getMessage(intent);
                    receiverMsg(context, getCustomMsg(intent), message, getNofityID(intent));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case "cn.jpush.android.intent.NOTIFICATION_RECEIVED":// 接收到了通知消息
                if (!NotificationManagerCompat.from(context).areNotificationsEnabled()) {
                    UIUtils.shortM("请检查是否允许App通知");
                }
                try {
                    String message = getMessage(intent);
                    receiverMsg(context, getCustomMsg(intent), message, getNofityID(intent));
//                    receiverMsg(context, getCustomMsg(intent),message, getNofityID(intent));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
    }

    /**
     * 接收消息时的事务处理
     *
     * @param context
     * @param msgJson
     */
    private void receiverMsg(Context context, String msgJson, String message, int notifiId) {
        boolean rst = false;
        PushInfo pushInfo = JsonDeal.json2Object(msgJson, PushInfo.class);
        if (pushInfo == null || Utils.isQuck(1500)) return;
        if (TextUtils.isEmpty(pushInfo.type) && !EmptyDeal.isEmpy(pushInfo.iris_id)) {
//            StartIntent.startGlobalInfoAcitivity(context,pushInfo.iris_id);
        }
        EventBus.getDefault().post(pushInfo);
        Logger.E("接收到推送消息:" + msgJson);
    }

    /**
     * 通知栏点击事件事务处理
     *
     * @param context
     * @param msgJson
     */
    private void clickHandler(Context context, String msgJson) {
        if (!TextUtils.isEmpty(msgJson)) {
            PushInfo pushInfo = JsonDeal.json2Object(msgJson, PushInfo.class);
            if (pushInfo == null) return;
//            CommonIntent.startTongzhiDetailsActivity(context,pushInfo.article_id);
//            if (TextUtils.equals(pushInfo.type,"bid")){
//                CommonIntent.startZhaobiaoTongzhiMessageActivity(context,pushInfo.article_id);
//            }else{
//                CommonIntent.startTongzhiDetailsActivity(context,pushInfo.article_id);
//            }

            CommonIntent.startDealPushNavigatorActivity(context);

        }
    }

    public String getCustomMsg(Intent intent) {
        Bundle bundle = intent.getExtras();
        if (bundle == null)
            return null;
        String extras = bundle.getString(JPushInterface.EXTRA_EXTRA);
        return extras;
    }

    public String getMessage(Intent intent) {
        Bundle bundle = intent.getExtras();
        if (bundle == null)
            return "";
        String message = bundle.getString(JPushInterface.EXTRA_MESSAGE);
        return message;

    }

    public int getNofityID(Intent intent) {
        Bundle bundle = intent.getExtras();
        if (bundle == null)
            return 0;
        int notificationId = bundle
                .getInt(JPushInterface.EXTRA_NOTIFICATION_ID);
        return notificationId;
    }

    private void printMsg(Intent intent) {
        Bundle bundle = intent.getExtras();
        if (bundle == null)
            return;
        String message = bundle.getString(JPushInterface.EXTRA_MESSAGE);
        String extras = bundle.getString(JPushInterface.EXTRA_EXTRA);
        String type = bundle.getString(JPushInterface.EXTRA_CONTENT_TYPE);
        Logger.E("消息内容:" + extras + "  " + message + "  " + "  " + type + "  "
                + intent.getAction());

    }
}
