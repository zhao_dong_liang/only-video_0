package cn.wu1588.dancer.common;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.sunrun.sunrunframwork.uibase.BaseActivity;
import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.sunrun.sunrunframwork.utils.BaseStartIntent;
import com.tencent.bugly.proguard.B;

import java.util.ArrayList;

import cn.wu1588.dancer.advert.activity.AdPayActivity;
import cn.wu1588.dancer.advert.activity.AdPayHistoriesActivity;
import cn.wu1588.dancer.advert.activity.AddAdvertActivity;
import cn.wu1588.dancer.advert.activity.AdverPutHistoriesActivity;
import cn.wu1588.dancer.advert.activity.AdvertAdminActivity;
import cn.wu1588.dancer.advert.activity.AdvertManagerActivity;
import cn.wu1588.dancer.advert.activity.RicheditorActivity;
import cn.wu1588.dancer.common.util.LoginUtil;
import cn.wu1588.dancer.home.activity.MoviePlayerV3Activity;
import cn.wu1588.dancer.mine.activity.OriginalActivity;
import cn.wu1588.dancer.mine.activity.UpdatePassActivity;
import cn.wu1588.dancer.model.Ad;
import cn.wu1588.dancer.channel.activity.HotZhanTiMoreActivity;
import cn.wu1588.dancer.channel.activity.NvYouDetailActivity;
import cn.wu1588.dancer.channel.activity.NvYouListActivity;
import cn.wu1588.dancer.aty.AlumDetailesActivity;
import cn.wu1588.dancer.aty.AlumDetailsPlayerActivity;
import cn.wu1588.dancer.common.dialog.BindZhiFuBaoDialog;
import cn.wu1588.dancer.common.dialog.PrivateVideosPsdDialog;
import cn.wu1588.dancer.home.activity.AllMovieActivity;
import cn.wu1588.dancer.home.activity.AlumDetailActivity;
import cn.wu1588.dancer.home.activity.AlumPayActivity;
import cn.wu1588.dancer.home.activity.AlumPayOrderActivity;
import cn.wu1588.dancer.home.activity.ForgetPasswordActivity;
import cn.wu1588.dancer.home.activity.HistoryRecordActivity;
import cn.wu1588.dancer.home.activity.HomeMoreClassifiListActivity;
import cn.wu1588.dancer.home.activity.LableScreenActivity;
import cn.wu1588.dancer.home.activity.LoginActivity;

import cn.wu1588.dancer.home.activity.NewMoviePlayerActivity;
import cn.wu1588.dancer.home.activity.RegisterActivity;
import cn.wu1588.dancer.home.activity.SearchActivity;
import cn.wu1588.dancer.home.activity.SearchResultActivity;
import cn.wu1588.dancer.home.activity.SmallVideoPlayerActivity;
import cn.wu1588.dancer.home.mode.MovieBean;
import cn.wu1588.dancer.MainActivity;
import cn.wu1588.dancer.mine.activity.AuthActivity;
import cn.wu1588.dancer.mine.activity.BindPhoneActivity;
import cn.wu1588.dancer.mine.activity.EditUserSignActivity;
import cn.wu1588.dancer.mine.activity.FeedBackActivity;
import cn.wu1588.dancer.mine.activity.FollowFansActivity;
import cn.wu1588.dancer.mine.activity.MyBuyActivity;
import cn.wu1588.dancer.mine.activity.MyCollectionActivity;
import cn.wu1588.dancer.mine.activity.MyDownloadActivity;
import cn.wu1588.dancer.mine.activity.MyFollowActivity;
import cn.wu1588.dancer.mine.activity.NoticeActivity;
import cn.wu1588.dancer.mine.activity.NoticeDetailActivity;
import cn.wu1588.dancer.mine.activity.PayImgActivity;
import cn.wu1588.dancer.mine.activity.QuestionDeatilActvity;
import cn.wu1588.dancer.mine.activity.RechargeMemberActivity;
import cn.wu1588.dancer.mine.activity.RechargeRecordActivity;
import cn.wu1588.dancer.mine.activity.RechargeV3Activity;
import cn.wu1588.dancer.mine.activity.SelectProvinceActivity;
import cn.wu1588.dancer.mine.activity.ServiceWebActivity;
import cn.wu1588.dancer.mine.activity.SettingActivity;
import cn.wu1588.dancer.mine.activity.TuiGuangActivity;
import cn.wu1588.dancer.mine.activity.UpdateInfoActivity;
import cn.wu1588.dancer.mine.activity.UserAuthActivity;
import cn.wu1588.dancer.mine.activity.UserInfoV3Activity;
import cn.wu1588.dancer.mine.activity.WebActivity;
import cn.wu1588.dancer.mine.mode.BuyAlbumBean;
import cn.wu1588.dancer.mine.mode.PayUrlBean;
import cn.wu1588.dancer.moviefeedback.MovieFeedbackActivity;
import cn.wu1588.dancer.my_profit.activity.GetCashActivity;
import cn.wu1588.dancer.my_profit.activity.ProfitActivity;
import cn.wu1588.dancer.my_profit.activity.WithdrawActivity;
import cn.wu1588.dancer.my_video.activity.AddAlbumActivity;
import cn.wu1588.dancer.my_video.activity.LabeActivity;
import cn.wu1588.dancer.my_video.activity.MyAlbumVidesActivity;
import cn.wu1588.dancer.my_video.activity.MyVideo2Activity;
import cn.wu1588.dancer.my_video.activity.MyVideoActivity;
import cn.wu1588.dancer.my_video.activity.ResetVideosPwdActivity;
import cn.wu1588.dancer.my_video.activity.SelectVideosActivity;
import cn.wu1588.dancer.my_video.activity.TypeActivity;
import cn.wu1588.dancer.my_video.activity.UpVideosActivity;
import cn.wu1588.dancer.my_video.activity.UpVideosV3Activity;
import cn.wu1588.dancer.other_user.OtherUserActiivty;


/**
 * 页面跳转帮助类
 * Created by WQ on 2017/5/24.
 */

public class CommonIntent extends BaseStartIntent {
    /**
     * 跳转浏览器
     */
    public static void startBrowser(Activity that,String url) {
//        Intent intent = new Intent();
//        intent.setAction("android.intent.action.VIEW");
//        Uri content_url = Uri.parse(url);
//        intent.setDataAndType(content_url, "text/html");
//        intent.addCategory(Intent.CATEGORY_BROWSABLE);
//        that.startActivity(intent);
        Intent intent;
        try {
            intent = Intent.parseUri(url,
                    Intent.URI_INTENT_SCHEME);
            intent.addCategory(Intent.CATEGORY_BROWSABLE);
            //保证新启动的APP有单独的堆栈，如果希望新启动的APP和原有APP使用同一个堆栈则去掉该项
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setComponent(null);
            that.startActivity(intent);
        } catch (Exception e) {
            UIUtils.shortM("没有匹配的浏览器，请下载安装");
            e.printStackTrace();
        }
    }

    public static void callPhone(Activity that, String phoneNum) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        Uri data = Uri.parse("tel:" + phoneNum);
        intent.setData(data);
        that.startActivity(intent);
    }

    /**
     * 导航界面
     *
     * @param that
     */

    public static void startNavigatorActivity(Activity that) {
        that.startActivity(new Intent(that, MainActivity.class));
//        that.startActivity(new Intent(that, MainActivity.class));
    }


    /**
     * 跳转到登录
     * @param that
     */
    public static void startLoginActivity(Context that) {
        that.startActivity(new Intent(that, LoginActivity.class));
    }
    /**
     * 跳转到注册
     * @param that
     */
    public static void startRegisterActivity(Context that) {
        that.startActivity(new Intent(that, RegisterActivity.class));
    }
    /**
     * 忘记密码
     * @param that
     */
    public static void forgetPasswordActivity(Context that) {
        that.startActivity(new Intent(that, ForgetPasswordActivity.class));
    }
    /**
     * 通知点击跳首页
     * @param context
     */
    public static void startDealPushNavigatorActivity(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    /**
     * 搜索
     *
     * @param that
     */
    public static void startSearchActivity(Activity that) {
        that.startActivity(new Intent(that, SearchActivity.class));
    }

    /**
     * 搜索结果
     *
     * @param that
     * @param keyWords
     */
    public static void startSearchResult(Activity that, String keyWords) {
        that.startActivity(new Intent(that, SearchResultActivity.class).putExtra("keyWords", keyWords));
    }

    /**
     * 历史记录
     *
     * @param that type
     */
    public static void startHistoryRecordActivity(Activity that) {
        that.startActivity(new Intent(that, HistoryRecordActivity.class));
    }

    /**
     * 设置
     *
     * @param that
     */
    public static void startSettingActivity(Activity that) {
        that.startActivity(new Intent(that, SettingActivity.class));
    }

    /**
     * 我要推广
     *
     * @param that
     */
    public static void startTuiGuangActivity(Activity that) {
        that.startActivity(new Intent(that, TuiGuangActivity.class));
    }

    /**
     * 会员充值
     *
     * @param that
     */
    public static void startRechargeActivity(Context that,int index) {
      Intent intent=  new Intent(that, RechargeV3Activity.class);
      intent.putExtra("index",index);
        that.startActivity(intent);
    }

    /**
     * 个人资料
     *
     * @param that
     */
    public static void startUserInfoActivity(Activity that) {
        that.startActivity(new Intent(that, UserInfoV3Activity.class));
    }

    /**
     * 更新昵称
     *
     * @param that
     * @param content
     */
    public static void startUpdateInfoActivity(Activity that, String content) {
        Intent intent = new Intent(that, UpdateInfoActivity.class);
        intent.putExtra("content", content);
        that.startActivityForResult(intent, 1);
    }

    /**
     * 编辑个性签名页面
     * @param that
     * @param content
     */
    public static void startEditUserSignActivity(Activity that,String content,int requestCode){
        Intent intent = new Intent(that, EditUserSignActivity.class);
        intent.putExtra("content", content);
        that.startActivityForResult(intent, requestCode);
    }
    /**
     * 我的收藏
     *
     * @param that
     */
    public static void startMyCollectionActivity(Activity that) {
        that.startActivity(new Intent(that, MyCollectionActivity.class));
    }

    /**
     * 意见反馈
     *
     * @param that
     */
    public static void startFeedBackActivity(Activity that) {
        that.startActivity(new Intent(that, FeedBackActivity.class));
    }

    /**
     * 通知
     *
     * @param that
     */
    public static void startNoticeActivity(Activity that) {
        that.startActivity(new Intent(that, NoticeActivity.class));
    }

    /**
     * 通知详情
     *
     * @param that
     * @param noticeid
     * @param comment_id
     */
    public static void startNoticeDetailActivity(Activity that, String noticeid, String comment_id) {
        that.startActivity(new Intent(that, NoticeDetailActivity.class).putExtra("noticeid", noticeid).putExtra("id", comment_id));
    }

    /**
     * 人气
     *
     * @param that
     */
    public static void startNvYouListActivity(Activity that) {
        that.startActivity(new Intent(that, NvYouListActivity.class));
    }

    /**
     * 首页推荐分类更多
     *
     * @param that
     * @param from_type
     */
    public static void startHomeMoreClassifiListActivity(Context that, String id, String from_type, int typeid) {
        that.startActivity(new Intent(that, HomeMoreClassifiListActivity.class).putExtra("id", id).putExtra("from_type", from_type).putExtra("typeid", typeid));
    }

    /**
     * 频道-热门专题更多
     *
     * @param that
     */
    public static void startHotZhanTiMoreActivity(Activity that) {
        that.startActivity(new Intent(that, HotZhanTiMoreActivity.class));
    }

    /**
     * 全部影片
     *
     * @param that
     * @param style
     */
    public static void startAllMovieActivity(Activity that, String style, String screen_id, String home_category_id) {
        Intent intent = new Intent(that, AllMovieActivity.class);
        intent.putExtra("style_type", style);
        intent.putExtra("screen_id", screen_id);
        intent.putExtra("home_category_id", home_category_id);
        that.startActivity(intent);
    }

    /**
     * 播放页面
     *影片详情页
     * @param that
     * @param id
     */
    public static void startMoviePlayerActivity(Context that, String id) {

        if (LoginUtil.startLogin(that)){
            CommonIntent.startLoginActivity(that);
        }else {
//            Intent intent = new Intent(that, MoviePlayerActivity.class);
            Intent intent = new Intent(that, MoviePlayerV3Activity.class);
            intent.putExtra("movieId", id);
            that.startActivity(intent);
        }
    }

    /**
     * 启动新的播放页面
     *影片详情页
     * @param that
     * @param id
     */
    public static void startNewMoviePlayerActivity(Context that, String id) {

        if (LoginUtil.startLogin(that)){
            CommonIntent.startLoginActivity(that);
        }else {
            Intent intent = new Intent(that, NewMoviePlayerActivity.class);
            intent.putExtra("movieId", id);
            that.startActivity(intent);
        }
    }

    /**
     * 播放页面
     *
     * @param that
     * @param index
     */
    public static void startSmallVideoPlayerActivity(Context that, int index, ArrayList<MovieBean> movieBeans) {
        if (LoginUtil.startLogin(that)){
            CommonIntent.startLoginActivity(that);
        }else {
            Gson gson = new Gson();
            String obj2 = gson.toJson(movieBeans);
            Intent intent = new Intent(that, SmallVideoPlayerActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("movieBeans",obj2);
            bundle.putInt("index",index);
            intent.putExtras(bundle);
            that.startActivity(intent);
        }
    }
//    /**
//     * 播放页面  从发现列表进入
//     * @param that
//     * @param id
//     */
//    public static void startPlayerActivity(Activity that, String id, View transitionView) {
//        Intent intent = new Intent(that, MoviePlayerActivity.class);
//        intent.putExtra("movieId",id);
//        // 这里指定了共享的视图元素
//        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(that, transitionView, "view");
//        ActivityCompat.startActivity(that, intent, options.toBundle());
//        that.startActivity(intent);
//    }

    /**
     * 问题详情
     *
     * @param that
     * @param id
     */
    public static void startQuestionDeatilActvity(Activity that, String id) {
        that.startActivity(new Intent(that, QuestionDeatilActvity.class).putExtra("id", id));
    }

    /**
     * 详情
     *
     * @param that
     * @param id
     */
    public static void startNvYouDetailActivity(Activity that, String id) {
        that.startActivity(new Intent(that, NvYouDetailActivity.class).putExtra("id", id));
    }

    /**
     * 会员充值
     *
     * @param that
     */
    public static void startRechargeMemberActivity(Context that) {
        that.startActivity(new Intent(that, RechargeMemberActivity.class));
    }

    /**
     * 用户协议
     *
     * @param that
     * @param type
     */
    public static void startWebActivity(Activity that, String type,String url) {
        that.startActivity(new Intent(that, WebActivity.class).putExtra("type", type).putExtra("url",url));
    }

    public static void startLoadHtmlWebActivity(Activity that, String html, Ad ad) {
        that.startActivity(
                new Intent(that, WebActivity.class)
                        .putExtra("html",html)
                        .putExtra("ignoreType", true)
                        .putExtra("ad", ad)
        );
    }

    public static void startLoadUrlWebActivity(Activity that, String url, Ad ad) {
        that.startActivity(
                new Intent(that, WebActivity.class)
                        .putExtra("url",url)
                        .putExtra("ignoreType", true)
                        .putExtra("ad", ad)
        );
    }

    /**
     * 标签筛选
     *
     * @param mContext
     * @param lableText
     */
    public static void startLableScreenActivity(Context mContext, String lableText) {
        mContext.startActivity(new Intent(mContext, LableScreenActivity.class).putExtra("lableText", lableText));
    }

    /**
     * 服务协议
     *
     * @param that
     * @param h5
     */
    public static void startServiceWebActivity(BaseActivity that, String h5) {
        that.startActivity(new Intent(that, ServiceWebActivity.class).putExtra("url", h5));
    }

    /**
     * 充值记录
     *
     * @param that
     */
    public static void startRechargeRecordActivity(BaseActivity that) {
        that.startActivity(new Intent(that, RechargeRecordActivity.class));
    }

    /**
     * 支付图片
     * @param that

     */
    public static void startPayImgActivity(BaseActivity that, PayUrlBean payUrlBean) {
        that.startActivity(new Intent(that, PayImgActivity.class).putExtra("PayUrlBean",payUrlBean));
    }

    /**
     * 进入选择省份页面
     * @param that
     */
    public static void startSelectProvinceActivity(BaseActivity that) {
        that.startActivityForResult(new Intent(that, SelectProvinceActivity.class),3);
    }

    /**
     * 进入绑定手机号页面
     * @param that
     */
    public static void startBindPhoneActivity(BaseActivity that,int requestCode) {
        that.startActivityForResult(new Intent(that, BindPhoneActivity.class),requestCode);
    }
    /**
     * 进入其他用户个人中心页面
     * @param that
     */
    public static void startOtherUserActiivty(BaseActivity that,String attention_uid) {
        Intent intent = new Intent(that, OtherUserActiivty.class);
        intent.putExtra("attention_uid",attention_uid);
        that.startActivity(intent);
    }

    /**
     * 进入我的关注页面
     * @param that
     */
    public static void startMyFollowActivity(BaseActivity that) {
        Intent intent = new Intent(that, MyFollowActivity.class);
        that.startActivity(intent);
    }

    /**
     * 进入我的关注和粉丝页面
     * @param that
     */
    public static void startFollowFansActivity(BaseActivity that) {
        Intent intent = new Intent(that, FollowFansActivity.class);
        that.startActivity(intent);
    }
    /**
     * 进入我的视频页面
     * @param that
     */
    public static void startMyVideoActivity(BaseActivity that) {
//        Intent intent = new Intent(that, MyVideoActivity.class);
        Intent intent = new Intent(that, MyVideo2Activity.class);
        that.startActivity(intent);
    }
    /**
     * 进入发布视频页面
     * @param that
     */
    public static void startUpVideosActivity(BaseActivity that, boolean isPrivate) {
        Intent intent = new Intent(that, UpVideosActivity.class);
        intent.putExtra("isPrivate", isPrivate);
        that.startActivity(intent);
    }
    /**
     * 进入发布视频页面
     * @param that
     */
    public static void startUpVideosActivity(Context context, boolean isPrivate) {
        Intent intent = new Intent(context, UpVideosV3Activity.class);
        intent.putExtra("isPrivate", isPrivate);
        context.startActivity(intent);
    }


    /**
     * 进入视频标签页面
     * @param that
     */
    public static void startLabeActivity(BaseActivity that,int request) {
        Intent intent = new Intent(that, LabeActivity.class);
        that.startActivityForResult(intent,request);
    }
    /**
     * 进入视频分类页面
     * @param that
     */
    public static void startTypeActivity(BaseActivity that,int request,boolean isVertical) {
        Intent intent = new Intent(that, TypeActivity.class);
        intent.putExtra("isVertical",isVertical);
        that.startActivityForResult(intent,request);
    }

    /**
     * 进入添加专辑页面
     * @param that
     */
    public static void startAddAlbumActivity(BaseActivity that,int request) {
        Intent intent = new Intent(that, AddAlbumActivity.class);
        that.startActivityForResult(intent,request);
    }
    /**
     * 进入选择视频页面
     * @param that
     */
    public static void startSelectVideosActivity(BaseActivity that,int request) {
        Intent intent = new Intent(that, SelectVideosActivity.class);
        that.startActivityForResult(intent,request);
    }
    /**
     * 进入视频收益页面
     * @param fragment
     */
    public static void startProfitActivity(Fragment fragment) {
        Intent intent = new Intent(fragment.getActivity(), ProfitActivity.class);
        fragment.startActivity(intent);
    }
    /**
     * 进入提现页面
     * @param context
     */
    public static void startGetCashActivity(Context context) {
        Intent intent = new Intent(context, GetCashActivity.class);
        context.startActivity(intent);
    }

    /**
     * 进入绑定支付宝账号页面
     * @param activity
     */
    public static void startBindZhiFuBaoDialog(Activity activity,int request,int type,String account) {
        Intent intent = new Intent(activity, BindZhiFuBaoDialog.class);
        intent.putExtra("account",account);
        intent.putExtra("type",type);
        activity.startActivityForResult(intent,request);
    }
    /**
     * 进入我的购买页面
     * @param context
     */
    public static void startMyBuyActivity(Context context) {
        Intent intent = new Intent(context, MyBuyActivity.class);
        context.startActivity(intent);
    }

    /**
     * 进入我的下载页面
     * @param context
     */
    public static void startMyDownloadActivity(Context context){
        Intent intent=new Intent(context, MyDownloadActivity.class);
        context.startActivity(intent);
    }

    /**
     * 进入达人认证页面
     * @param context
     */
    public static void startAuthActivity(Context context){
        Intent intent=new Intent(context, AuthActivity.class);
        context.startActivity(intent);
    }

    /**
     * 广告管理
     */
    public static void startAdvertAdminActivity(Context context){
        Intent intent=new Intent(context, AdvertAdminActivity.class);
        context.startActivity(intent);
    }
    /**
     * 我的专辑内的视频列表页
     */
    public static void startMyAlbumVidesActivity(Context context,String channel_id){
        Intent intent=new Intent(context, MyAlbumVidesActivity.class);
        intent.putExtra("channel_id",channel_id);
        context.startActivity(intent);
    }

    /**
     * 私密视频密码输入框
     * @param activity
     */
    public static void startPrivateVideosActivity(Activity activity,boolean check_is_secret){
        Intent intent=new Intent(activity, PrivateVideosPsdDialog.class);
        intent.putExtra("check_is_secret",check_is_secret);
        activity.startActivityForResult(intent,1);
    }
    /**
     * 进入设置密码
     * @param activity
     */
    public static void startResetVideosPwdActivity(Activity activity){
        Intent intent=new Intent(activity, ResetVideosPwdActivity.class);
        activity. startActivityForResult(intent,1);
    }

    /**
     * 添加广告
     */
    public static void  startAddAdvertActivity(Context context, @Nullable Ad ad){

        AddAdvertActivity.start(context, ad);
    }

    public static final int PICK_FILE_REQUEST_CODE = 34;
    // 打开系统的文件选择器
    public static void pickFile(Activity context) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        context.startActivityForResult(Intent.createChooser(intent, "请选择文件"), PICK_FILE_REQUEST_CODE);
    }

    //进入专辑详情页
    public static void startAlumDetailActivity(Context context,String id){
        Intent intent=new Intent(context, AlumDetailActivity.class);
        intent.putExtra("id",id);
        context.startActivity(intent);
    }
    //进入富文本编辑页
    public static void startRicheditorActivity(Activity activity,int requestCode,String html){
        Intent intent=new Intent(activity, RicheditorActivity.class);
        intent.putExtra("html",html);
        activity.startActivityForResult(intent,requestCode);
    }

    // 专辑详情
    public static void startAlumDetailsActivity(Context context, BuyAlbumBean bean) {
        AlumDetailesActivity.start(context, bean);
    }
    // 专辑详情
    public static void startAlumDetailsPlayerActivity(Context context, BuyAlbumBean bean, String movieId, int index) {
        AlumDetailsPlayerActivity.start(context, bean, movieId, index);
    }

    // 购买专辑确认订单
    public static void startAlumDetailsOrderPayActivity(Context context, BuyAlbumBean bean) {
        AlumPayOrderActivity.start(context, bean);
    }

    // 购买专辑订单支付
    public static void startAlumPayActivity(Context context, BuyAlbumBean bean) {
        AlumPayActivity.start(context, bean);
    }

    // 广告投放管理
    public static void startAdManagerActivity(Context context, Ad ad) {
        AdvertManagerActivity.start(context, ad);
    }

    // 广告投放记录
    public static void startAdHistoriesActivity(Context context) {
        AdverPutHistoriesActivity.start(context);
    }

    // 广告充值
    public static void startAdRecharge(Context context) {
        AdPayActivity.start(context);
    }

    // 视频投诉
    public static void startMovieFeedback(Context context, String movieId) {
        MovieFeedbackActivity.start(context, movieId);
    }

    // 广告充值记录
    public static void startAdPayHistories(Context context) {
        AdPayHistoriesActivity.start(context);
    }

    // 实名认证
    public static void startUserAuth(Context context) {
        UserAuthActivity.start(context);
    }

    /**
     * 申请原创
     *
     * @param that
     */
    public static void startOriginalActivity(Activity that) {
        that.startActivity(new Intent(that, OriginalActivity.class));
    }


    /**
     * 跳转提现
     *
     * @param that
     */
    public static void startWithdrawActivity(Activity that,String withdrawMoney) {
        WithdrawActivity.start(that,withdrawMoney);
    }


    /**
     * 修改密码
     *
     * @param that
     */
    public static void startUpdatePassActivity(Activity that) {
        UpdatePassActivity.start(that);
    }
}
