package cn.wu1588.dancer.common.event;

import android.content.Intent;

import androidx.annotation.IntDef;

public class SharedCompleteEvent {

    private Intent data;
    @SharedType
    private int type;

    public SharedCompleteEvent(@SharedType int type, Intent data) {
        this.data = data;
        this.type = type;
    }

    public Intent getData() {
        return data;
    }

    public void setData(Intent data) {
        this.data = data;
    }

    @SharedType
    public int getType() {
        return type;
    }

    public void setType(@SharedType int type) {
        this.type = type;
    }

    @IntDef({SharedType.QQ, SharedType.WX})
    public @interface SharedType {
        int QQ = 0;
        int WX = 1;
    }
}
