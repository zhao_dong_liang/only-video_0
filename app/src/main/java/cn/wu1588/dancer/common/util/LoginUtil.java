package cn.wu1588.dancer.common.util;

import android.content.Context;

import com.sunrun.sunrunframwork.uiutils.UIUtils;

import cn.wu1588.dancer.common.quest.Config;


/**
 * 登录工具类
 * Created by WQ on 2017/home1/10.
 */

public class LoginUtil {
    public static void exitLogin() {
        exitLogin( false);
    }

    public static void exitLogin(boolean isOrtherLogin) {
        Config.putLoginInfo(null);
        try {
//            CommonApp.getInstance().closeAllActivity(LoginActivity.class);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void closeOther(){
        try {
//            CommonApp.getInstance().closeAllActivity(LoginActivity.class);
            UIUtils.shortM("已退出登录");
        }catch (Exception e){
            e.printStackTrace();
        }

    }
    public static boolean startLogin(Context context){
        boolean isLogin = false;
        int need_login = (int) SPUtils.get(context,ConstantValue.NEED_LOGIN, 1);
        boolean is_login = (boolean) SPUtils.get(context,ConstantValue.IS_LOGIN, false);
        if (need_login== 1 ){
          if (!is_login){
              isLogin = true;
          }
        }
        return isLogin;
    }
}
