package cn.wu1588.dancer.common.util.ppvod;

public class PPVodResult {
    private String title;

    private String rpath;

    private String path;

    private String pic;

    private String gif;

    private String qr;

    private String url;

    private String orgfile;

    private String mp4;

    private int size;

    private String md5;

    private String sizeview;

    private String suffix;

    private String shareid;

    private String share;

    private String duration;

    private Metadata metadata;

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return this.title;
    }

    public void setRpath(String rpath) {
        this.rpath = rpath;
    }

    public String getRpath() {
        return this.rpath;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return this.path;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getPic() {
        return this.pic;
    }

    public void setGif(String gif) {
        this.gif = gif;
    }

    public String getGif() {
        return this.gif;
    }

    public void setQr(String qr) {
        this.qr = qr;
    }

    public String getQr() {
        return this.qr;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return this.url;
    }

    public void setOrgfile(String orgfile) {
        this.orgfile = orgfile;
    }

    public String getOrgfile() {
        return this.orgfile;
    }

    public void setMp4(String mp4) {
        this.mp4 = mp4;
    }

    public String getMp4() {
        return this.mp4;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return this.size;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public String getMd5() {
        return this.md5;
    }

    public void setSizeview(String sizeview) {
        this.sizeview = sizeview;
    }

    public String getSizeview() {
        return this.sizeview;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getSuffix() {
        return this.suffix;
    }

    public void setShareid(String shareid) {
        this.shareid = shareid;
    }

    public String getShareid() {
        return this.shareid;
    }

    public void setShare(String share) {
        this.share = share;
    }

    public String getShare() {
        return this.share;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDuration() {
        return this.duration;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public Metadata getMetadata() {
        return this.metadata;
    }

    public static class Metadata {
        private int bitrate;

        private String resolution;

        private int time;

        private int fps;

        private int length;

        public void setBitrate(int bitrate) {
            this.bitrate = bitrate;
        }

        public int getBitrate() {
            return this.bitrate;
        }

        public void setResolution(String resolution) {
            this.resolution = resolution;
        }

        public String getResolution() {
            return this.resolution;
        }

        public void setTime(int time) {
            this.time = time;
        }

        public int getTime() {
            return this.time;
        }

        public void setFps(int fps) {
            this.fps = fps;
        }

        public int getFps() {
            return this.fps;
        }

        public void setLength(int length) {
            this.length = length;
        }

        public int getLength() {
            return this.length;
        }

    }

}