package cn.wu1588.dancer.common.receiver;

/**
 * Created by cnsunrun on 2017/7/25.
 */

public class PushInfo {

    /**
     * path : http://localhost/zhaobao/Uploads/System/Image/59e96511e35b0.jpg
     * height : 1080
     * width : 1920
     */


    public ImageBean image;
    /**
     * type : 20
     */

    public String type;
    public String iris_id;
    /**
     * add_time : 2018-12-22 09:00:00
     * article_id : 346958
     * title : 合经区2018年农村道路畅通工程等项目监理
     */

    public String add_time;
    public String article_id;
    public String title;
    public int fansCount;
    public int likedCount;

    public static class ImageBean {
        public String path;
        public int height=1;
        public int width=1;
    }

    public ImageBean getImage() {
        return image==null?new ImageBean():image;
    }
}
