package cn.wu1588.dancer.common;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import androidx.multidex.MultiDex;

import com.bilibili.boxing.BoxingCrop;
import com.bilibili.boxing.BoxingMediaLoader;
import com.bilibili.boxing.loader.IBoxingMediaLoader;
import com.kongzue.baseokhttp.util.BaseOkHttp;
import com.lzy.okgo.OkGo;
import com.sunrun.sunrunframwork.app.BaseApplication;
import com.sunrun.sunrunframwork.common.DefaultMediaLoader;
import com.sunrun.sunrunframwork.http.NetServer;
import com.tencent.bugly.Bugly;
import com.tencent.mmkv.MMKV;

import java.util.HashMap;

import cn.wu1588.dancer.BuildConfig;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.util.DisplayUtil;
import cn.wu1588.dancer.common.util.OtherDataConvert;
import cn.wu1588.dancer.common.util.PushHelper;
import cn.wu1588.dancer.common.boxing.BoxingGlideLoader;
import cn.wu1588.dancer.common.boxing.BoxingUcrop;
import cn.wu1588.dancer.common.boxing.FixDefaultMediaLoader;
import cn.wu1588.dancer.common.config.RongIMHelper;
import cn.wu1588.dancer.common.quest.TokenRequestPreProccess;


/**
 * Created by WQ on 2017/5/24.
 */

public class CommonApp extends BaseApplication{

    private static CommonApp commonApp;
    public static int samllVideoId=16;//小视频分类的id
    @Override
    public void onCreate() {
        super.onCreate();
        commonApp=this;
        //初始化播放器
//        AliVcMediaPlayer.init(getApplicationContext());
        //Fresco初始化
//        Fresco.initialize(this);
        String processName = getCurProcessName(this);
        IBoxingMediaLoader loader = new BoxingGlideLoader();
        BoxingMediaLoader.getInstance().init(loader);
        BoxingCrop.getInstance().init(new BoxingUcrop());
        ((DefaultMediaLoader) DefaultMediaLoader.getInstance()).init(new FixDefaultMediaLoader());
        NetServer.Settings.getSetting().setDataConvert(new OtherDataConvert());
        NetServer.Settings.getSetting().setRequestPreproccess(new TokenRequestPreProccess());
        Bugly.init(getApplicationContext(), "973625c7fa", BuildConfig.DEBUG);
        MultiDex.install(this);
        if (getPackageName().equals(getCurProcessName(this))) {
            //初始化推送
            PushHelper.initPush(this);
        }
//初始化数据存储
        MMKV.initialize(this);
//初始化融云
        RongIMHelper.init(this);
//        RongCloudEvent.init(this);
        DisplayUtil.init(getApplicationContext());
        OkGo.getInstance().init(this);

        BaseOkHttp.serviceUrl = " http://app.myylook.com/";
        BaseOkHttp.DEBUGMODE = true;
    }



    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
    public static BaseApplication getInstance() {
        return BaseApplication.getInstance();
    }
    public static  CommonApp getApplication(){
        return commonApp;
    }
    private Drawable stateBarDrawable;
    private boolean barTvColor=true;

    public void setStateBarDrawable(Drawable stateBarDrawable) {
        this.stateBarDrawable = stateBarDrawable;
    }
    public Drawable getStateBarDrawable() {
        if (stateBarDrawable==null){
            stateBarDrawable=getResources().getDrawable(R.drawable.bar_gradient_bg);
        }
        return stateBarDrawable;
    }


    public Drawable getStateBarDrawable2() {
        if (stateBarDrawable==null){
            stateBarDrawable=getResources().getDrawable(R.drawable.title_bg_01);
        }
        return stateBarDrawable;
    }
    public Drawable getStateBarDrawableWhite() {
        if (stateBarDrawable==null){
            stateBarDrawable=getResources().getDrawable(R.color.white);
        }
        return stateBarDrawable;
    }


    public boolean isBarTvColor() {
        return barTvColor;
    }

    public void setBarTvColor(boolean barTvColor) {
        this.barTvColor = barTvColor;
    }


    private HashMap<String, Bitmap> tabBitmapMap=new HashMap<>();

    public HashMap<String, Bitmap> getTabBitmapMap() {
        return tabBitmapMap;
    }

    public void setTabBitmapMap(HashMap<String, Bitmap> tabBitmapMap) {
        this.tabBitmapMap = tabBitmapMap;
    }
}
