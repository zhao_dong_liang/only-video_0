package cn.wu1588.dancer.common.widget;

import android.content.Context;

import com.maning.mndialoglibrary.MProgressDialog;

public class LoadDialogUtils {

    public static void showDialog(Context context){
        MProgressDialog.showProgress(context,"请稍等");
    }
    public static void dissmiss(){
        MProgressDialog.dismissProgress();
    }
}
