package cn.wu1588.dancer.common.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.widget.TextView;

import cn.wu1588.dancer.R;

import androidx.appcompat.widget.AppCompatTextView;

public class FlickerTextView extends AppCompatTextView {
    private int mViewWidth = 0;
    private Paint mPaint;
    private LinearGradient mLinearGradient;
    private Matrix mGradientMatrix;
    private int mTranslate = 0;

    public FlickerTextView(Context context) {
        super(context);
    }

    public FlickerTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FlickerTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if(mViewWidth == 0){
            mViewWidth = getMeasuredWidth();
            if(mViewWidth > 0){
                mPaint = getPaint();
                //第1个参数      起始点坐标x位置
                //第2个参数      起始点坐标y位置
                //第3个参数      终点坐标x位置
                //第4个参数      终点坐标y位置
                //第5个参数      参与渐变的颜色组合
                //第6个参数      定义每个颜色处于渐变的相对位置，如果为null，则表示所有颜色按顺序均匀分布
                //第7个参数      使用Shader.TileMode.CLAMP,主要有3种，CALMP:如果渲染器超出原始边界范围，会复制范围内边缘染色；REPEAT:横向和纵向的重复渲染器图片，平铺；MIRROR：横向和纵向的重复渲染器图片，以镜像方式平铺。
                mLinearGradient = new LinearGradient(0,0,mViewWidth,0,new int[]{Color.RED,0xffffff, R.color.color_ffa400},null, Shader.TileMode.CLAMP);
                mPaint.setShader(mLinearGradient);
                mGradientMatrix = new Matrix();
            }
        }

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (mGradientMatrix != null){
            mTranslate += mViewWidth/5;
            if (mTranslate > 2 * mViewWidth){
                mTranslate = - mViewWidth;

            }

            mGradientMatrix.setTranslate(mTranslate, 0);
            mLinearGradient.setLocalMatrix(mGradientMatrix);
            postInvalidateDelayed(100);

        }
    }

}
