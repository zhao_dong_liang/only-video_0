package cn.wu1588.dancer.common.config;

/**
 * 字符串常量
 * Created by WQ on 2017/12/8.
 */

public interface Const {
    String COLLECT = "collect"; //我的收藏
    String HISTORY = "history";//历史记录
    String EVENT_CHAT_REFRESH_HOME_PAGE = "chat_refresh_home_page";

    int PERMISSION = 100;//权限申请

    String FROM_CHANNEL = "channel"; //频道
    String FROM_HOME_PAGE = "home_page"; //首页
    int ONE = 1; //必看专题
    int TWO = 2; //必看专题

    String GONGGAO_DETAIL = "gonggaoxiangqing"; //公告详情
    String USER_PROTOCOL = "user_protocol";//用户协议
}
