package cn.wu1588.dancer.common.model;

import android.text.TextUtils;

import com.sunrun.sunrunframwork.utils.EmptyDeal;

import cn.wu1588.dancer.common.quest.Config;

/**
 * Created by cnsunrun on 2017/5/26.
 * <p>
 * 登录模型类
 */

public class LoginInfo {
    /**
     * "id": "6",
     * "nickname": "131***360",
     * "icon": "www.code.cn/duboshiping_app/22222222222",
     * "gender": "男",
     * "identity_id": "1000000000057"
     */

    public String id;
    public String uid;
    public String nickname;
    public String member_end_time;
    public String title;
    public String icon;
    public String gender;
    public String identity_id;
    public String r_token;
    public String title_name;
    public String is_member; //是否会员   0=否  1=是
    public String gold_account;//金币数
    public String like_num;//获赞数
    public String attention_count;//粉丝数量
    public String channel_count;//专辑数量
    public String movie_count;//影片数量
    public String is_talent;//是否达人认证
    public String signature;//个性签名
    public String province;//省
    public String city;//市
    public String mobile;//手机号
    public String ali_name;//提现账户
    public String real_name;
    public String idcard;

    public boolean isMember() {
        return TextUtils.equals("1", is_member);
    }

    public LoginInfo setUid(String uid) {
        this.uid = uid;
        return saveSelf();
    }

    private LoginInfo saveSelf() {
        Config.putLoginInfo(this);
        return this;
    }


    public boolean isValid() {
        return !EmptyDeal.isEmpy(uid);
    }

    public boolean isTalentAuth() {
        return is_talent.equals("1");
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getMember_end_time() {
        return member_end_time;
    }

    public void setMember_end_time(String member_end_time) {
        this.member_end_time = member_end_time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIdentity_id() {
        return identity_id;
    }

    public void setIdentity_id(String identity_id) {
        this.identity_id = identity_id;
    }

    public String getR_token() {
        return r_token;
    }

    public void setR_token(String r_token) {
        this.r_token = r_token;
    }

    public String getTitle_name() {
        return title_name;
    }

    public void setTitle_name(String title_name) {
        this.title_name = title_name;
    }

    public String getIs_member() {
        return is_member;
    }

    public void setIs_member(String is_member) {
        this.is_member = is_member;
    }

    public String getGold_account() {
        return gold_account;
    }

    public void setGold_account(String gold_account) {
        this.gold_account = gold_account;
    }

    public String getLike_num() {
        return like_num;
    }

    public void setLike_num(String like_num) {
        this.like_num = like_num;
    }

    public String getAttention_count() {
        return attention_count;
    }

    public void setAttention_count(String attention_count) {
        this.attention_count = attention_count;
    }

    public String getChannel_count() {
        return channel_count;
    }

    public void setChannel_count(String channel_count) {
        this.channel_count = channel_count;
    }

    public String getMovie_count() {
        return movie_count;
    }

    public void setMovie_count(String movie_count) {
        this.movie_count = movie_count;
    }

    public String getIs_talent() {
        return is_talent;
    }

    public void setIs_talent(String is_talent) {
        this.is_talent = is_talent;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAli_name() {
        return ali_name;
    }

    public void setAli_name(String ali_name) {
        this.ali_name = ali_name;
    }

    public String getReal_name() {
        return real_name;
    }

    public void setReal_name(String real_name) {
        this.real_name = real_name;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    @Override
    public String toString() {
        return "LoginInfo{" +
                "id='" + id + '\'' +
                ", uid='" + uid + '\'' +
                ", nickname='" + nickname + '\'' +
                ", member_end_time='" + member_end_time + '\'' +
                ", title='" + title + '\'' +
                ", icon='" + icon + '\'' +
                ", gender='" + gender + '\'' +
                ", identity_id='" + identity_id + '\'' +
                ", r_token='" + r_token + '\'' +
                ", title_name='" + title_name + '\'' +
                ", is_member='" + is_member + '\'' +
                ", gold_account='" + gold_account + '\'' +
                ", like_num=" + like_num +
                ", attention_count=" + attention_count +
                ", channel_count=" + channel_count +
                ", movie_count=" + movie_count +
                ", is_talent=" + is_talent +
                ", signature='" + signature + '\'' +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", mobile='" + mobile + '\'' +
                ", ali_name='" + ali_name + '\'' +
                ", real_name='" + real_name + '\'' +
                ", idcard='" + idcard + '\'' +
                '}';
    }
}
