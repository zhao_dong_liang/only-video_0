package cn.wu1588.dancer.common.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.Handler;
import android.os.Message;

import com.maning.mndialoglibrary.MProgressDialog;
import com.my.toolslib.StringUtils;

import java.util.ArrayList;

import wseemann.media.FFmpegMediaMetadataRetriever;

//获取视频信息和视频指定帧图
public class VideoThumbnailUtils {

    private ArrayList<Bitmap> bitmaps=new ArrayList<>();
    private VideoThumbnailCall videoThumbnailCall;

    private Context context;
    private  String video_code;
    private int video_width;
    private int video_height;
    private String video_bitrate;//码率

    private String path;
    private MyThead myThead;

    public VideoThumbnailUtils(Context context, VideoThumbnailCall videoThumbnailCall){
        this.context=context;
        this.videoThumbnailCall=videoThumbnailCall;
    }
    Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (videoThumbnailCall!=null){
                videoThumbnailCall.getVideoThumbnailResult(bitmaps);
            }
            MProgressDialog.dismissProgress();
        }
    };
    public void getVideoThumbnail(String path) {
        this.path=path;
          myThead=new MyThead();
          myThead.start();
        MProgressDialog.showProgress(context,"视频处理中");
    }

    class MyThead extends Thread{
        @Override
        public void run() {
            super.run();
            int length=6;
            bitmaps.clear();
            FFmpegMediaMetadataRetriever retriever = new FFmpegMediaMetadataRetriever();
            try {
                retriever.setDataSource(path);
                String width=retriever.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);
                if (!StringUtils.isNull(width)){
                    video_width=Integer.parseInt(width);
                }
                String height=retriever.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
                if (!StringUtils.isNull(height)){
                    video_height=Integer.parseInt(height);
                }
                video_code= retriever.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_VIDEO_CODEC);
                video_bitrate=retriever.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_VARIANT_BITRATE);
                String time = retriever.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_DURATION);
                int timeS=Integer.valueOf(time)/length;
                for(int i=1;i<=length;i++){
                    Bitmap bitmap=retriever.getFrameAtTime(i*timeS*1000, MediaMetadataRetriever.OPTION_CLOSEST);
                    bitmaps.add(bitmap);
                }
                handler.sendEmptyMessage(1);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (RuntimeException e) {
                e.printStackTrace();
            } finally {
                try {
                    retriever.release();
                } catch (RuntimeException e) {
                    e.printStackTrace();
                }
            }


        }
    }
    //获取编码方式，如 h264等
    public String  getVideoVode(){
        return video_code;
    }
    public Bitmap getFristThumbnail(){
        if (bitmaps.size()>0){
            return bitmaps.get(0);
        }else {
            return null;
        }
    }

    public int getVideo_height() {
        return video_height;
    }

    public int getVideo_width() {
        return video_width;
    }

    public interface VideoThumbnailCall{
        void getVideoThumbnailResult(ArrayList<Bitmap> bitmaps);
    }
    public void close(){
        handler.removeMessages(1);
        myThead=null;
        if (bitmaps.size()>0){
            for (int i = 0; i < bitmaps.size(); i++) {
                Bitmap bitmap = bitmaps.get(i);
                if (bitmap!=null){
                    bitmap.recycle();
                    bitmap=null;
                }
            }
        }
    }

    public ArrayList<Bitmap> getBitmaps() {
        return bitmaps;
    }
}
