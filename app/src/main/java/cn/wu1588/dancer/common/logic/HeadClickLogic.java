//package common.logic;
//
//import android.content.Context;
//import android.view.View;
//
//import com.cnsunrun.common.CommonIntent;
//import com.cnsunrun.common.quest.Config;
//import com.sunrun.sunrunframwork.uibase.BaseActivity;
//
//import io.rong.imkit.RongIM;
//import io.rong.imlib.model.Conversation;
//import io.rong.imlib.model.Message;
//import io.rong.imlib.model.UserInfo;
//
///**
// * 会话页面,头像点击处理逻辑
// * Created by WQ on 2017/12/8.
// */
//
//public class HeadClickLogic implements RongIM.ConversationBehaviorListener {
//    BaseActivity that;
//
//    public HeadClickLogic(BaseActivity that, String targetId) {
//        this.that = that;
//    }
//
//
//    public static void dealMessageClick(Context that, int type, String id) {
//        //type
//        //   10 系统通知
//        //   home1   订单审核通知 data ：course_id 课程id
//        //
//        //   2    分配老师通知
//        if (Config.getLoginInfo().isValid()) {
//          switch (type){
//              case 10:
//                  CommonIntent.startMessageNoticeActivity(that);
//                  break;
//              case home1:
//                  CommonIntent.startPayCourseActivity(that);
//                  break;
//              case 2:
//                  CommonIntent.startCourseDetailActivity(that,id,"");
//                  break;
//          }
//        } else {
//            CommonIntent.startLoginActivity();
//        }
//
//    }
//
//    @Override
//    public boolean onUserPortraitClick(Context context, Conversation.ConversationType conversationType, UserInfo userInfo) {
//        return false;
//    }
//
//    @Override
//    public boolean onUserPortraitLongClick(Context context, Conversation.ConversationType conversationType, UserInfo userInfo) {
//        return false;
//    }
//
//    @Override
//    public boolean onMessageClick(Context context, View view, Message message) {
//        return false;
//    }
//
//    @Override
//    public boolean onMessageLinkClick(Context context, String s) {
//        return false;
//    }
//
//    @Override
//    public boolean onMessageLongClick(Context context, View view, Message message) {
//        return false;
//    }
//}
