package cn.wu1588.dancer.common.dialog;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.my.toolslib.DecimalEditFilter;
import com.my.toolslib.StringUtils;
import com.my.toolslib.http.utils.LzyResponse;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;

import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.widget.LoadDialogUtils;
import cn.wu1588.dancer.common.quest.BaseQuestStart;

//绑定支付的dialog
public class BindZhiFuBaoDialog extends LBaseActivity {
@BindView(R.id.edit_acount)
    EditText edit_acount;
@BindView(R.id.title_tv)
    TextView title_tv;
@BindView(R.id.acount_labe)
TextView acount_labe;

private int type;//0绑定账户 1提现
    private double account;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bind_zhi_fu_bao_dialog);
        type=getIntent().getIntExtra("type",0);
        String accountStr = getIntent().getStringExtra("account");
        if (!StringUtils.isNull(accountStr)){
            account=Double.parseDouble(accountStr);
        }
        if (type==1){
            edit_acount.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL|InputType.TYPE_NUMBER_FLAG_SIGNED);
            edit_acount.setFilters(new InputFilter[]{new DecimalEditFilter(2)});//保留小数点后两位
            title_tv.setText("请输入提现金额（元）");
            acount_labe.setText("金额");
            edit_acount.setHint("请输入金额");
        }
    }
    @OnClick({R.id.cancel_bt,R.id.enter_bt})
    public void onClick(View v){
        switch (v.getId()){
            case R.id.cancel_bt:
                finish();
                break;
            case R.id.enter_bt:
                String s = edit_acount.getText().toString();
                if (StringUtils.isNull(s)){
                    ToastUtils.longToast(type==0?"账号不能为空":"请输入提现金额");
                }else {
//                    绑定账号
                    if (type==0){
                        BaseQuestStart.setUserAliName(this,s);
                    }else {
                        if (Double.parseDouble(s)>account){
                            ToastUtils.longToast("余额不足");
                        }else {
                            BaseQuestStart.withdraw(this,s);
                            LoadDialogUtils.showDialog(that);
                        }
                    }

                }
                break;
        }
    }

    @Override
    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
        super.nofityUpdateUi(requestCode, response, view);
        if (requestCode==BaseQuestStart.SET_USER_ALI_NAME_CODE){//绑定支付宝
            if (response.code==1){
                ToastUtils.longToast("绑定成功");
                BaseQuestStart.getUserInfo(that);
                Intent intent=new Intent();
                intent.putExtra("account",edit_acount.getText().toString());
                setResult(1,intent);
                finish();
            }
        }else if (requestCode==BaseQuestStart.WITHDRAW_CODE){//提现成功
            if (response.code==1){
                ToastUtils.longToast("提现成功");
                finish();
            }else {
                ToastUtils.longToast("提现失败");
            }
        }
    }
}
