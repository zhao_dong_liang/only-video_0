package cn.wu1588.dancer.common.event;

import java.util.List;

import cn.wu1588.dancer.home.mode.MovieBean;
//小视频列表和播放页数据同步
public class SmallVideoDataEvent {
    private List<MovieBean> movieBeans;

    public SmallVideoDataEvent(List<MovieBean> movieBeans){
        this.movieBeans=movieBeans;
    }

    public List<MovieBean> getMovieBeans() {
        return movieBeans;
    }
}
