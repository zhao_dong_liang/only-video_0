package cn.wu1588.dancer.common.model;

import com.sunrun.sunrunframwork.view.sidebar.SortModel;

/**
 * Created by cnsunrun on 2018-03-14.
 */

public class ClickBean extends SortModel {
    public String id ;
    public String title;
    public String pid;
    public String logo;
    public boolean isChoose;
    public ClickBean(String id, String title) {
        super();
        this.id = id;
        this.title = title;
    }
    public ClickBean(String id, String title, boolean isChoose) {
        this.id = id;
        this.title = title;
        this.isChoose = isChoose;
    }

    public ClickBean() {
        super();
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }


    public boolean isChoose() {
        return isChoose;
    }

    public void setChoose(boolean choose) {
        this.isChoose = choose;
    }

    @Override
    public String toString() {
        return title;
    }
}
