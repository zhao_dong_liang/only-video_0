package cn.wu1588.dancer.common.config;


import android.net.Uri;


import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.http.NetUtils;
import com.sunrun.sunrunframwork.http.cache.NetSession;
import com.sunrun.sunrunframwork.http.utils.JsonDeal;
import com.sunrun.sunrunframwork.utils.EmptyDeal;
import com.sunrun.sunrunframwork.utils.log.Logger;

import org.apache.http.Header;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.common.model.LoginInfo;
import io.rong.imkit.RongIM;
import io.rong.imlib.model.UserInfo;

import static cn.wu1588.dancer.common.quest.NetQuestConfig.HTTP_API;


/**
 * 融云用户信息提供者
 * Created by WQ on 2017/5/17.
 */

public class IMUserInfoProvider implements RongIM.UserInfoProvider {

    public static HashMap<String, String> userListUri = new HashMap<>();
    public static HashMap<String, String> userListNic = new HashMap<>();
    private static Set<String> requestSet = Collections.synchronizedSet(new HashSet<String>());
    private static IMUserInfoProvider _INSTANCE=new IMUserInfoProvider();

    public static IMUserInfoProvider getInstance() {
        return _INSTANCE;
    }

    @Override
    public UserInfo getUserInfo(final String userId) {
        final NetSession session = NetSession.instance(CommonApp.getInstance());
        LoginInfo user_info = session.getObject("user_info" + userId, LoginInfo.class);
        if (requestSet.contains(userId)) {
            return user_info==null?null:( new UserInfo(userId, user_info.nickname, Uri.parse("" + user_info.icon)));
        }
        String url = HTTP_API + "Api/Neighborhood/IM/im_member_info";
        RequestParams params = new RequestParams();
        List<String> list=new ArrayList<>();
        list.add(userId);
        params.put("list",list);
//        params.put("password", Config.getLoginInfo().password);
        requestSet.add(userId);
//        addToken();
        NetUtils.doPost(url, params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int i, Header[] headers, String s, Throwable throwable) {
                //
                Logger.E("用户信息请求失败");
            }

            @Override
            public void onSuccess(int i, Header[] headers, String s) {
                BaseBean bean = JsonDeal.createBean(s, new TypeToken<List<LoginInfo>>(){});
                List<LoginInfo> memberInfo = bean.Data();
                if (!EmptyDeal.isEmpy(memberInfo)){
                    session.put("user_info" + userId, memberInfo.get(0));
                    refreshUserInfo(memberInfo.get(0), userId);
                }
            }

            @Override
            public void onFinish() {
                requestSet.remove(userId);
                super.onFinish();
            }
        });
        return refreshUserInfo(user_info, userId);
    }

    public static UserInfo refreshUserInfo(LoginInfo memberInfo, String userId) {
        if (memberInfo == null) return null;
        UserInfo userInfo = new UserInfo(userId, memberInfo.nickname, Uri.parse("" + memberInfo.icon));
        RongIM.getInstance().refreshUserInfoCache(userInfo);
//        EventBus.getDefault().post(DefaultEvent.createEvent(Const.EVENT_UPDATE_USERINFO, memberInfo));
        return userInfo;
    }

    public static void focusRefeshUserInfo(String userId){
        final NetSession session = NetSession.instance(CommonApp.getInstance());
        session.remove("user_info" + userId);
//        TestTool.invokeMethod(RongUserInfoManager.getInstance(),"clearUserInfoCache");
    }
}
