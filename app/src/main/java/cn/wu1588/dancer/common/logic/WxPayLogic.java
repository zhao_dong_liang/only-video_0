package cn.wu1588.dancer.common.logic;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;

import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.sunrun.sunrunframwork.utils.EmptyDeal;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import org.json.JSONObject;

/**
 * Created by WQ on 2017/12/6.
 */

public class WxPayLogic {
    private Activity context;
    public static final int SDK_PAY_FLAG = 1;
    private Thread payThread;
    private Handler mHandler;
    private IWXAPI api;
    public static final int STATE_DEF = -1, STATE_PAYING = 0, STATE_SUCCESS = 1, STATE_FAIL = 2;
    public static int pay_status = STATE_DEF;//未初始化, 0 起调 home1 成功 2 失败

    public WxPayLogic(Activity context, String appId, Handler mHandler) {
        this.context = context;
        this.mHandler = mHandler;
        api = WXAPIFactory.createWXAPI(context, null);
        api.registerApp(appId);
        pay_status = STATE_DEF;
    }

    /**
     * 启动支付 -
     *
     * @param json jsonObject对象
     */
    public void startWxPay(JSONObject json) {
        pay_status = STATE_PAYING;
        Runnable payRunnable = new Runnable() {
            @Override
            public void run() {
                while (pay_status == STATE_PAYING) {//等待支付结果
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if (pay_status != STATE_DEF) {
                    Message msg = new Message();
                    msg.what = SDK_PAY_FLAG;
                    msg.obj = pay_status == STATE_SUCCESS ? "9000" : "-3000";//这里跟支付宝保持一致
                    mHandler.sendMessage(msg);
                }
            }
        };
        if (mHandler != null) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    UIUtils.cancelLoadDialog();
                }
            }, 1000);
        }
        payThread = new Thread(payRunnable);
        payThread.start();
        PayReq req = new PayReq();
        req.appId = json.optString("appid");
        req.partnerId = json.optString("partnerid");
        req.prepayId = json.optString("prepayid");
        req.nonceStr = json.optString("noncestr");
        req.timeStamp = json.optString("timestamp");
        req.packageValue = json.optString("package");
        req.sign = json.optString("sign");
        req.extData = "app data"; // optional
//        // 在支付之前，如果应用没有注册到微信，应该先调用IWXMsg.registerApp将应用注册到微信
        if (EmptyDeal.isEmpy(req.appId) || EmptyDeal.isEmpy(req.partnerId) || EmptyDeal.isEmpy(req.prepayId)||req.prepayId.equals("null") || EmptyDeal.isEmpy(req.nonceStr)
                || EmptyDeal.isEmpy(req.timeStamp) || EmptyDeal.isEmpy(req.packageValue) || EmptyDeal.isEmpy(req.sign) || EmptyDeal.isEmpy(req.extData)) {
            UIUtils.shortM("支付参数有误，请检查参数");
        }else{
            UIUtils.showLoadDialog(context, "正在启动微信...");
            api.sendReq(req);
        }
    }

    public static void setStatus(int status) {
        pay_status = status;
    }

    /**
     * 销毁-资源释放
     */
    public void onDestory() {
        if (payThread != null) {
            payThread = null;
            UIUtils.cancelLoadDialog();
            mHandler.removeMessages(SDK_PAY_FLAG);
            mHandler = null;
            context = null;
        }
    }
}
