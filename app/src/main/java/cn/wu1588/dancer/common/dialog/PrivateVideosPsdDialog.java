package cn.wu1588.dancer.common.dialog;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.my.toolslib.StringUtils;
import com.my.toolslib.http.utils.LzyResponse;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;

import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.quest.BaseQuestStart;

//私密视频密码输入框
public class PrivateVideosPsdDialog extends LBaseActivity {
    @BindView(R.id.reset_labe)
    TextView reset_labe;
    @BindView(R.id.pwd_edit)
    EditText pwd_edit;

    private   boolean check_is_secret;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_private_videos);
        initViews();
    }
    private void initViews(){
         check_is_secret = getIntent().getBooleanExtra("check_is_secret",false);
        if (!check_is_secret){
            reset_labe.setText("设置密码");
        }

    }

    @OnClick({R.id.reset_labe,R.id.enter_tv,R.id.cencel_tv})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.reset_labe://设置密码或者重置密码
                CommonIntent.startResetVideosPwdActivity(that);
                break;
            case R.id.enter_tv://确认
                String s = pwd_edit.getText().toString();
                if (StringUtils.isNull(s)){
                    ToastUtils.longToast("请输入密码");
                    return;
                }
                BaseQuestStart.validateSecret(this,s);
                break;
            case R.id.cencel_tv://取消

                finish();
                break;

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data!=null){
            check_is_secret=true;
            reset_labe.setText("设置密码");
        }
    }

    @Override
    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
        super.nofityUpdateUi(requestCode, response, view);
        if (requestCode==BaseQuestStart.VALIDATE_SECRET_CODE){//验证私密视频密码
            if (response.code==1){
                Intent intent=new Intent();
                intent.putExtra("check_is_secret",check_is_secret);
                intent.putExtra("validate_secret",true);
                setResult(1,intent);
                finish();
            }else {
                ToastUtils.longToast("密码错误");
            }
        }
    }
}
