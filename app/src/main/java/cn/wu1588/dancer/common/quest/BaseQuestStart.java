package cn.wu1588.dancer.common.quest;


import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.google.gson.reflect.TypeToken;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.FileCallback;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.HttpParams;
import com.lzy.okgo.model.Progress;
import com.lzy.okgo.model.Response;
import com.maning.mndialoglibrary.MProgressBarDialog;
import com.my.toolslib.FileUtils;
import com.my.toolslib.StringUtils;
import com.my.toolslib.http.utils.JsonCallback;
import com.my.toolslib.http.utils.LzyResponse;
import com.my.toolslib.http.utils.OkHttpRequest;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.http.NAction;
import com.sunrun.sunrunframwork.http.NetRequestHandler;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;
import com.sunrun.sunrunframwork.utils.AppUtils;
import com.sunrun.sunrunframwork.utils.EmptyDeal;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.common.model.LoginInfo;
import cn.wu1588.dancer.common.util.PinyinUtil;
import cn.wu1588.dancer.common.util.VoucherBean;
import cn.wu1588.dancer.common.widget.LoadDialogUtils;
import cn.wu1588.dancer.home.mode.MovieByCateV3Bean;
import cn.wu1588.dancer.home.mode.MovieDetailV3Bean;
import cn.wu1588.dancer.model.Ad;
import cn.wu1588.dancer.model.AdHistory;
import cn.wu1588.dancer.model.AdPayHistory;
import cn.wu1588.dancer.channel.mode.Channels;
import cn.wu1588.dancer.channel.mode.LableBean;
import cn.wu1588.dancer.channel.mode.LableScreenBean;
import cn.wu1588.dancer.channel.mode.MovieScreenBean;
import cn.wu1588.dancer.channel.mode.NvYouDetaiBean;
import cn.wu1588.dancer.channel.mode.NvyouBean;
import cn.wu1588.dancer.channel.mode.Recommend;
import cn.wu1588.dancer.channel.mode.SettingInfo;
import cn.wu1588.dancer.channel.mode.ThemeDetailBean;
import cn.wu1588.dancer.channel.mode.ThemeaticBean;
import cn.wu1588.dancer.channel.mode.XiongbeiBean;
import cn.wu1588.dancer.model.FansListBean;
import cn.wu1588.dancer.model.FansNumBean;
import cn.wu1588.dancer.model.MessageNoticeBean;
import cn.wu1588.dancer.common.model.PictureInfo;
import cn.wu1588.dancer.common.model.SystemParams;
import cn.wu1588.dancer.common.model.TimeListBean;
import cn.wu1588.dancer.common.model.UserTimeBean;
import cn.wu1588.dancer.model.DiscoverMenu;
import cn.wu1588.dancer.model.TabBar;
import cn.wu1588.dancer.mine.mode.FollowsBean;
import cn.wu1588.dancer.mine.mode.GoldMealBean;
import cn.wu1588.dancer.mine.mode.MemberRecordBean;
import cn.wu1588.dancer.mine.mode.RechargeBeanV3;
import cn.wu1588.dancer.mine.mode.TodayDataBean;
import cn.wu1588.dancer.mine.mode.UserAuthInfo;
import cn.wu1588.dancer.moviefeedback.MovieFeedBack;
import cn.wu1588.dancer.home.mode.AlumBean;
import cn.wu1588.dancer.home.mode.CommentDetailBean;
import cn.wu1588.dancer.home.mode.HeaderScreenBean;
import cn.wu1588.dancer.home.mode.HomeBean;
import cn.wu1588.dancer.home.mode.HomeNoticeBean;
import cn.wu1588.dancer.home.mode.HomeThematicDetailBean;
import cn.wu1588.dancer.home.mode.HotSearchBean;
import cn.wu1588.dancer.home.mode.MovieActionStateBean;
import cn.wu1588.dancer.home.mode.MovieBean;
import cn.wu1588.dancer.home.mode.MovieCommentBean;
import cn.wu1588.dancer.home.mode.MovieCommentSectionEntity;
import cn.wu1588.dancer.home.mode.MovieDetailBean;
import cn.wu1588.dancer.home.mode.MovieHisBean;
import cn.wu1588.dancer.home.mode.NavbarColorBean;
import cn.wu1588.dancer.home.mode.ThematicBean;
import cn.wu1588.dancer.home.mode.ThirdBean;
import cn.wu1588.dancer.home.mode.VersionBean;
import cn.wu1588.dancer.mine.mode.AccountBean;
import cn.wu1588.dancer.mine.mode.AdverBean;
import cn.wu1588.dancer.mine.mode.BuyAlbumBean;
import cn.wu1588.dancer.mine.mode.DownloadBean;
import cn.wu1588.dancer.mine.mode.HistoryAndLoveBean;
import cn.wu1588.dancer.mine.mode.MemberSpendBean;
import cn.wu1588.dancer.mine.mode.MsgBean;
import cn.wu1588.dancer.mine.mode.MyAlbumBean;
import cn.wu1588.dancer.mine.mode.MyFeedBackBean;
import cn.wu1588.dancer.mine.mode.MyFollowVideoBean;
import cn.wu1588.dancer.mine.mode.PayChannelBean;
import cn.wu1588.dancer.mine.mode.PayMentBean;
import cn.wu1588.dancer.mine.mode.PayUrlBean;
import cn.wu1588.dancer.mine.mode.ProblemBean;
import cn.wu1588.dancer.mine.mode.RechargeBean;
import cn.wu1588.dancer.mine.mode.RegionBean;
import cn.wu1588.dancer.mine.mode.TuiguangBean;
import cn.wu1588.dancer.mine.mode.WebUrlBean;
import cn.wu1588.dancer.mine.mode.WxPayBean;
import cn.wu1588.dancer.moviefeedback.MovieFeedbackRequest;
import cn.wu1588.dancer.my_profit.bean.GetCashBean;
import cn.wu1588.dancer.my_profit.bean.ProfitBean;
import cn.wu1588.dancer.my_video.bean.LabeBean;
import cn.wu1588.dancer.my_video.bean.MyVideoBean;
import cn.wu1588.dancer.my_video.bean.MyVideoData;
import cn.wu1588.dancer.my_video.bean.TypeBean;
import cn.wu1588.dancer.utils.map.MD5Utils;


/**
 * 接口请求调用帮助类
 */
public class BaseQuestStart extends BaseQuestConfig {


    /**
     * 首页
     *
     * @param netRequestHandler
     */
    public static void getHomeData(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynGet(new NAction()
                .setUrl(GET_HOME_DATA)
                .setRequestCode(GET_HOME_DATA_CODE)
                .setTypeToken(HomeBean.class));
    }

    /**
     * 频道-专栏推荐
     *
     * @param netRequestHandler
     */
    public static void getThematicList(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynGet(new NAction()
                .setUrl(GET_THEMATIC_LIST)
                .setRequestCode(GET_THEMATIC_LIST_CODE)
                .setTypeToken(ThemeaticBean.class));
    }

    /**
     * 获取标签数据
     *
     * @param netRequestHandler
     */
    public static void getLableList(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynGet(new NAction()
                .setUrl(GET_LABLE_LIST)
                .setRequestCode(GET_LABLE_LIST_CODE)
                .setTypeToken(LableBean.class));
    }

    /**
     * 列表
     *
     * @param netRequestHandler
     * @param page
     * @param order
     * @param id
     */
    public static void getNvYouListData(NetRequestHandler netRequestHandler, int page, String order, String id) {
        netRequestHandler.requestAsynGet(new NAction()
                .setUrl(GET_NVYOU_LIST)
                .put("order", order)
                .put("id", id)
                .put("p", page)
                .put("md5",MD5Utils.md5())
                .setRequestCode(GET_NVYOU_LIST_CODE)
                .setTypeToken(new TypeToken<List<NvyouBean>>() {
                }));
    }

    /**
     * 全部影片头部筛选
     *
     * @param netRequestHandler
     */
    public static void getHederScreen(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynGet(new NAction()
                .setUrl(GET_HEDER_SCREEN)
                .setRequestCode(GET_HEDER_SCREEN_CODE)
                .setTypeToken(HeaderScreenBean.class));
    }


    public static void getDiscoverMenuList(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynPost(new NAction()
                .setUrl(GET_DISCOVER_MENU_LIST)
                .put("md5",MD5Utils.md5())
                .setRequestCode(GET_DISCOVER_MENU_LIST_CODE)
                .setTypeToken(new TypeToken<List<DiscoverMenu>>() {
                }));
    }

    /**
     * 全部影片
     *
     * @param netRequestHandler
     * @param screen_id
     * @param home_category_id
     * @param recommend_category_id
     * @param page
     */
    public static void getMovieAll(NetRequestHandler netRequestHandler, String screen_id, String home_category_id, String recommend_category_id, int page) {
        netRequestHandler.requestAsynGet(new NAction()
                .setUrl(GET_ALL_MOVIE)
                .put("screen_id", screen_id)
                .put("home_category_id", home_category_id)
                .put("home_category_id", home_category_id)
                .put("p", page)
                .put("md5",MD5Utils.md5())
                .setRequestCode(GET_ALL_MOVIE_CODE)
                .setTypeToken(new TypeToken<List<MovieBean>>() {
                }));
    }

    /**
     * 小视频列表页
     *
     * @param netRequestHandler
     * @param cate_id
     * @param page
     */
    public static void getSmallVideo(NetRequestHandler netRequestHandler, String cate_id, int page, String device) {
//        cate_id	是	int	分类 id
//        device	是	string	设备标识
//        pageSize	否	int	每页的数量，默认为 5
//        task_driver
        netRequestHandler.requestAsynPost(new NAction()
                .setUrl(GET_CATE_SHOW_LIST)
                .put("md5",MD5Utils.md5())
                .put("device", device)
                .put("cate_id", cate_id)
                .put("pageSize", 5)
                .put("task_driver", 1)
                .setRequestCode(GET_CATE_SHOW_LIST_CODE)
                .setTypeToken(new TypeToken<List<MovieBean>>() {
                }));
    }

    /**
     * 用户登录注册
     *
     * @param netRequestHandler
     * @param deviceId
     */
    public static void getUserLogin(NetRequestHandler netRequestHandler, String deviceId) {
        netRequestHandler.requestAsynPost(new NAction()
                .setUrl(GET_USER_LOGIN)
                .put("device_id", deviceId)
                .put("type", "0")
                .put("md5",MD5Utils.md5())
                .setRequestCode(GET_USER_LOGIN_CODE)
                .setTypeToken(LoginInfo.class));
    }

    /**
     * 用户QQ登录
     *
     * @param netRequestHandler
     * @param deviceId
     */
    public static void QQLogin(NetRequestHandler netRequestHandler, String token, String deviceId) {
        netRequestHandler.requestAsynPost(new NAction()
                .setUrl(QQ_LOGIN)
                .put("access_token", token)
                .put("device_id", deviceId)
                .put("type", "0")
                .put("md5",MD5Utils.md5())
                .setRequestCode(QQ_LOGIN_CODE)
                .setTypeToken(LoginInfo.class));
    }

    /**
     * 用户微信登录
     *
     * @param netRequestHandler
     * @param deviceId
     */
    public static void WXLogin(NetRequestHandler netRequestHandler, String code, String deviceId) {
        netRequestHandler.requestAsynPost(new NAction()
                .setUrl(WX_LOGIN)
                .put("code", code)
                .put("device_id", deviceId)
                .put("type", "0")
                .put("md5",MD5Utils.md5())
                .setRequestCode(WX_LOGIN_CODE)
                .setTypeToken(LoginInfo.class));
    }

    /**
     * 用户手机号注册
     *
     * @param netRequestHandler
     * @param deviceId
     */
    public static void registerUser(NetRequestHandler netRequestHandler, String phone, String password, String code, String deviceId) {
        netRequestHandler.requestAsynPost(new NAction()
                .setUrl(REGISTER_USER)
                .put("phone", phone)
                .put("password", password)
                .put("code", code)
                .put("type", "0")
                .put("device_id", deviceId)
                .put("md5",MD5Utils.md5())
                .setRequestCode(REGISTER_USER_CODE)
                .setTypeToken(LoginInfo.class));
    }

    /**
     * 用户密码修改
     *
     * @param netRequestHandler
     */
    public static void modifyPassword(NetRequestHandler netRequestHandler, String phone, String password, String code) {
        netRequestHandler.requestAsynPost(new NAction()
                .setUrl(CHANGE_PASSWORD)
                .put("phone", phone)
                .put("password", password)
                .put("code", code)
                .put("md5",MD5Utils.md5())
                .setRequestCode(CHANGE_PASSWORD_CODE)
                .setTypeToken(LoginInfo.class));
    }

    /**
     * 用户手机登录
     *
     * @param netRequestHandler
     * @param deviceId
     */
    public static void userLogin(NetRequestHandler netRequestHandler, String login_type, String phone, String password, String code, String deviceId) {
        netRequestHandler.requestAsynPost(new NAction()
                .setUrl(PHONE_LOGIN)
                .put("login_type", login_type)
                .put("phone", phone)
                .put("password", password)
                .put("code", code)
                .put("type", "0")
                .put("device_id", deviceId)
                .put("md5",MD5Utils.md5())
                .setRequestCode(PHONE_LOGIN_CODE)
                .setTypeToken(LoginInfo.class));
    }

    /**
     * 获取验证码
     *
     * @param netRequestHandler
     */
    public static void getCode(String phone, NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynPost(new NAction()
                .setUrl(SEND_SMS_CODE)
                .put("phone", phone)
                .put("md5",MD5Utils.md5())
                .setRequestCode(SEND_SMS_CODE_CODE)
                .setTypeToken(LoginInfo.class));
    }

    /**
     * 启动页图片
     *
     * @param netRequestHandler
     */
    public static void getStartUpPics(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynPost(new NAction()
                .setUrl(START_UP_PICS)
                .setRequestCode(START_UP_PICS_CODE)
                .setTypeToken(PictureInfo.class));
    }

    /**
     * 获取发现页顶部列表
     *
     * @param netRequestHandler
     */
    public static void getDiscoverPageList(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynPost(new NAction()
                .setUrl(DISCOVER_PAGE_LIST)
                .setRequestCode(DISCOVER_PAGE_LIST_CODE)
                .setTypeToken(PictureInfo.class));
    }


    /**
     * 用户信息修改----头像 昵称 性别
     *
     * @param netRequestHandler
     * @param headUrl
     * @param sex
     * @param nikeName
     */
    public static void postUserInfo(NetRequestHandler netRequestHandler, String headUrl, int sex,
                                    String nikeName, String signature, String province, String city) {

        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(POST_UPDATE_USER_INFO)
                .put("icon", headUrl)
                .put("nickname", nikeName)
                .put("sex", sex)
                .put("signature", signature)
                .put("province", province)
                .put("city", city)
                .put("md5",MD5Utils.md5())
                .setRequestCode(POST_UPDATE_USER_INFO_CODE));
//        netRequestHandler.requestAsynPost(Config.UAction()
//                .setUrl(BaseQuestConfig.POST_UPDATE_USER_INFO)
//                .put("img", headImageFilePath)
//                .put("nickname", nikeName)
//                .put("sex", sex)
//                .setRequestCode(BaseQuestConfig.POST_UPDATE_USER_INFO_CODE));

    }

    /**
     * 新 用户信息修改----头像 昵称 性别
     *
     * @param netRequestHandler
     * @param
     * @param sex
     * @param
     */
    public static void postUserInfoV3(String nickname,
                                      String sex,
                                      String signature,
                                      String province,
                                      String city,
                                      String icon,
                                      String birthday,
                                      String background, NetRequestHandler netRequestHandler) {

        if (birthday.equals("请选择")){
            birthday = "";
        }
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(POST_UPDATE_USER_INFO)
                .put("uid", Config.getLoginInfo().id)
                .put("nickname", nickname)
                .put("sex", sex)
                .put("signature", signature)
                .put("province", province)
                .put("city", city)
                .put("icon", icon)
                .put("birthday", birthday)
                .put("background", background)
                .put("md5",MD5Utils.md5())
                .setRequestCode(POST_UPDATE_USER_INFO_CODE_1));
//        netRequestHandler.requestAsynPost(Config.UAction()
//                .setUrl(BaseQuestConfig.POST_UPDATE_USER_INFO)
//                .put("img", headImageFilePath)
//                .put("nickname", nikeName)
//                .put("sex", sex)
//                .setRequestCode(BaseQuestConfig.POST_UPDATE_USER_INFO_CODE));

    }

    /**
     * 获取用户信息
     *
     * @param netRequestHandler
     */
    public static void getUserInfo(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(GET_USER_INFO)
                .setRequestCode(GET_USER_INFO_CODE)
                .setTypeToken(LoginInfo.class));
    }

    /**
     * 获取用户信息
     *
     * @param request
     */
    public static void getUserInfoOk(OkHttpRequest request, String uid) {
        OkGo.<LzyResponse<LoginInfo>>post(GET_USER_INFO)
                .tag(request)
                .params("uid", uid)
                .params("md5",MD5Utils.md5())
                .execute(new JsonCallback<LzyResponse<LoginInfo>>() {
                    @Override
                    public void onSuccess(Response<LzyResponse<LoginInfo>> response) {
                        request.nofityUpdateUi(GET_USER_INFO_CODE, response.body(), null);
                    }
                });
        ;
    }

    /**
     * 我的-历史记录和我的收藏列表
     *
     * @param netRequestHandler
     */
    public static void getHisRecordAndLove(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(GET_HIS_RECORD_LOVE)
                .setRequestCode(GET_HIS_RECORD_LOVE_CODE)
                .setTypeToken(HistoryAndLoveBean.class));
    }

    /**
     * 【安卓】专题分类分页数据
     *
     * @param netRequestHandler
     * @param id
     * @param typeid
     * @param page
     */
    public static void getThematicDetail(NetRequestHandler netRequestHandler, String id, int typeid, int page) {
        netRequestHandler.requestAsynGet(new NAction()
                .setUrl(GET_THEMATIC_DETAIL)
                .put("id", id)
                .put("typeid", typeid)
                .put("p", page)
                .put("md5",MD5Utils.md5())
                .setRequestCode(GET_THEMATIC_DETAIL_CODE)
                .setTypeToken(new TypeToken<List<ThemeDetailBean>>() {
                }));
    }

    /**
     * 首页更多
     *
     * @param netRequestHandler
     * @param id
     * @param page
     */
    public static void getHomeThematicDetail(NetRequestHandler netRequestHandler, String id, int page) {
        netRequestHandler.requestAsynGet(new NAction()
                .setUrl(GET_HOME_MORE)
                .put("recommend_category_id", id)
                .put("p", page)
                .put("md5",MD5Utils.md5())
                .setRequestCode(GET_HOME_MORE_CODE)
                .setTypeToken(HomeThematicDetailBean.class));
    }

    /**
     * 列表胸型标签数据
     *
     * @param netRequestHandler
     */
    public static void getChestselect(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynGet(new NAction()
                .setUrl(GET_CHESTSELECT)
                .setRequestCode(GET_CHESTSELECT_CODE)
                .setTypeToken(new TypeToken<List<XiongbeiBean>>() {
                }));
    }

    /**
     * 频道标签筛选栏目标签数据
     *
     * @param netRequestHandler
     */
    public static void getLableScreenList(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynGet(new NAction()
                .setUrl(GET_LABLE_SCREEN)
                .setRequestCode(GET_LABLE_SCREEN_CODE)
                .setTypeToken(new TypeToken<List<LableScreenBean>>() {
                }));
    }

    /**
     * 频道--标签筛选专栏
     * *
     *
     * @param netRequestHandler
     * @param ids
     */
    public static void getLabelSelectContent(NetRequestHandler netRequestHandler, String ids) {
        netRequestHandler.requestAsynPost(new NAction()
                .setUrl(SELECT_CONTENT)
                .put("ids", ids)
                .put("md5",MD5Utils.md5())
                .setRequestCode(SELECT_CONTENT_CODE)
                .setTypeToken(new TypeToken<List<MovieScreenBean>>() {
                }));
    }

    /**
     * 发现
     *
     * @param netRequestHandler
     * @param page
     * @param refresh
     */
    public static void getDiscoverData(NetRequestHandler netRequestHandler, int page, String refresh) {
        netRequestHandler.requestAsynGet(Config.UAction()
                .setUrl(GET_DISCOVER_LIST)
                .put("p", page)
                .put("refresh", refresh)
                .put("md5",MD5Utils.md5())
                .setRequestCode(GET_DISCOVER_LIST_CODE)
                .setTypeToken(new TypeToken<List<MovieBean>>() {
                }));
    }

    /**
     * 获取发现页推荐页数据
     * device	是	string	设备标识号
     * pageSize	否	int	每页数量，默认5
     * task_driver	是	int	投放设备 1移动端；2PC端
     * task_tion	是	int	投放位置 1数据流；2视频插播；3播放页小横条；4播放页右上角；5视频图片推送；6开屏
     *
     * @param netRequestHandler
     * @param page
     */
    public static void getDiscoverRecData(String device, int page, NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(GET_DISCOVER_REC)
//                .put("page", page)
                .put("md5", MD5Utils.md5())
                .put("pagesize", 10)
                .put("device", device)
                .put("task_tion", 1)
                .put("task_driver", 1)
                .setRequestCode(GET_DISCOVER_REC_CODE)
                .setTypeToken(new TypeToken<List<MovieBean>>() {
                }));
    }

    /**
     * 获取发现页分类视频具体数据
     *
     * @param netRequestHandler
     * @param page
     */
    public static void getCateShowList(NetRequestHandler netRequestHandler, int page, int id, String deviceId) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(GET_CATE_SHOW_LIST)
//                .put("cate_id", id)
//                .put("page", page)
//                .put("pageSize", "10")
                .put("md5", MD5Utils.md5())
                .put("device", deviceId)
                .put("cate_id", CommonApp.samllVideoId + "")
                .put("pageSize", 5)
                .put("task_driver", 1)
                .setRequestCode(GET_CATE_SHOW_LIST_CODE)
                .setTypeToken(new TypeToken<List<Map<String, Object>>>() {
                }));
    }

    /**
     * 个人中心（意见反馈常见问题列表+我要反馈反馈分类标签+设置里的用户协议+版本更新）
     *
     * @param netRequestHandler
     */
    public static void getCommProblemList(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynGet(new NAction()
                .setUrl(COMM_PROBLEM_LIST)
                .setRequestCode(COMM_PROBLEM_LIST_CODE)
                .setTypeToken(ProblemBean.class));
    }

    /**
     * 我的反馈列表
     *
     * @param netRequestHandler
     */
    public static void getMyFeedBackList(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(GET_MY_FEEDBACK)
                .setRequestCode(GET_MY_FEEDBACK_CODE)
                .setTypeToken(new TypeToken<List<MyFeedBackBean>>() {
                }));
    }

    /**
     * 视频收藏
     *
     * @param netRequestHandler
     * @param id
     */
    public static void getMovieCollect(NetRequestHandler netRequestHandler, String id) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(GET_COLLECT_MOVIE)
                .put("movie_id", id)
                .put("md5",MD5Utils.md5())
                .setRequestCode(GET_COLLECT_MOVIE_CODE));
    }

    /**
     * 用户反馈提交
     *
     * @param netRequestHandler
     * @param content
     * @param id
     */
    public static void postFeedBack(NetRequestHandler netRequestHandler, String content, String id) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(POST_FEED_BACK)
                .put("typeid", id)
                .put("content", content)
                .put("md5",MD5Utils.md5())
                .setRequestCode(POST_FEED_BACK_CODE));
    }

    /**
     * 常见问题详情
     *
     * @param netRequestHandler
     */
    public static void getQuestDetail(NetRequestHandler netRequestHandler, String id) {
        netRequestHandler.requestAsynPost(new NAction()
                .setUrl(GET_QUEST_DETAIL)
                .put("id", id)
                .put("md5",MD5Utils.md5())
                .setRequestCode(GET_QUEST_DETAIL_CODE)
                .setTypeToken(ProblemBean.QuestionlistBean.class));
    }

    /**
     * 我的----历史记录和我的收藏筛选
     *
     * @param netRequestHandler
     * @param page
     * @param type
     * @param type_id
     */
    public static void getMyCollectList(NetRequestHandler netRequestHandler, int page, String type, String type_id) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(GET_MY_COLLECTION_LIST)
                .put("type", type)
                .put("type_id", type_id)
                .put("p", page)
                .put("md5",MD5Utils.md5())
                .setRequestCode(GET_MY_COLLECTION_LIST_CODE)
                .setTypeToken(new TypeToken<List<MovieHisBean>>() {
                }));
    }

    /**
     * 历史记录和我的收藏删除+批量删除
     *
     * @param netRequestHandler
     * @param ids
     */
    public static void delMovie(NetRequestHandler netRequestHandler, String ids, String type) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(POST_DEL_MOVIE)
                .put("ids", ids)
                .put("type", type)
                .put("md5",MD5Utils.md5())
                .setRequestCode(POST_DEL_MOVIE_CODE));
    }

    /**
     * 视频播放详情页
     *
     * @param netRequestHandler
     * @param movieId
     * @param typeid
     */
    public static void getMoviePlayDetail(NetRequestHandler netRequestHandler, String movieId, String typeid) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(GET_MOVIE_PLAY_DETAIL)
                .put("movie_id", movieId)
                .put("typeid", typeid)
                .put("md5",MD5Utils.md5())
                .setRequestCode(GET_MOVIE_PLAY_DETAIL_CODE)
                .setTypeToken(MovieDetailBean.class));
    }

    /**
     * 详情
     *
     * @param netRequestHandler
     * @param id
     */
    public static void getNvYouDetail(NetRequestHandler netRequestHandler, String id) {
        netRequestHandler.requestAsynPost(new NAction()
                .setUrl(GET_NV_YOU_DETAIL)
                .put("id", id)
                .put("md5",MD5Utils.md5())
                .setRequestCode(GET_NV_YOU_DETAIL_CODE)
                .setTypeToken(NvYouDetaiBean.class));
    }

    /**
     * 详情页相关影片筛选
     *
     * @param netRequestHandler
     * @param ac_id
     * @param select_id
     */
    public static void getScreenNvyouList(NetRequestHandler netRequestHandler, String ac_id, String select_id) {
        netRequestHandler.requestAsynPost(new NAction()
                .setUrl(GET_SCREEN_NV_YOU_LIST)
                .put("actor_id", ac_id)
                .put("select_id", select_id)
                .put("md5",MD5Utils.md5())
                .setRequestCode(GET_SCREEN_NV_YOU_LIST_CODE)
                .setTypeToken(new TypeToken<List<MovieBean>>() {
                }));
    }

    /**
     * 用户协议
     *
     * @param netRequestHandler
     */
    public static void getWebUrl(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynGet(new NAction()
                .setUrl(GET_WEB_URL)
                .setRequestCode(GET_WEB_URL_CODE)
                .setTypeToken(WebUrlBean.class));
    }

    /**
     * 消息列表
     *
     * @param netRequestHandler
     * @param type
     * @param page
     */
    public static void getMsgList(NetRequestHandler netRequestHandler, String type, int page) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(GET_MSG_LIST)
                .put("type", type)
                .put("p", page)
                .put("md5",MD5Utils.md5())
                .setRequestCode(GET_MSG_LIST_CODE)
                .setTypeToken(new TypeToken<List<MsgBean>>() {
                })
        );
    }

    /**
     * 公告详情H5页面
     *
     * @param netRequestHandler
     * @param id
     */
    public static void getNoticeDetailUrl(NetRequestHandler netRequestHandler, String id) {
        netRequestHandler.requestAsynPost(new NAction()
                .setUrl(GET_NOTICE_DETAIL_URL)
                .put("id", id)
                .put("md5",MD5Utils.md5())
                .setRequestCode(GET_NOTICE_DETAIL_URL_CODE)
                .setTypeToken(WebUrlBean.class));
    }

    /**
     * 评论详情页
     *
     * @param netRequestHandler
     * @param mem_id
     * @param noticeid
     */
    public static void getCommentsDetail(NetRequestHandler netRequestHandler, String mem_id, String noticeid) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(GET_COMMENTS_DETAIL)
                .put("id", mem_id)
                .put("noticeid", noticeid)
                .put("md5",MD5Utils.md5())
                .setRequestCode(GET_COMMENTS_DETAIL_CODE)
                .setTypeToken(CommentDetailBean.class));
    }

    /**
     * 提交评论回复
     *
     * @param netRequestHandler
     * @param typeid
     * @param userid
     * @param movie_id
     * @param pid
     * @param content
     */
    public static void postComments(NetRequestHandler netRequestHandler, String typeid, String userid, String movie_id, String pid, String content) {
        netRequestHandler.requestAsynPost(new NAction()
                .setUrl(POST_COMMENTS)
                .put("md5",MD5Utils.md5())
                .put("typeid", typeid)
                .put("uid", Config.getLoginInfo().uid)
                .put("userid", userid)
                .put("movie_id", movie_id)
                .put("pid", pid)
                .put("content", content)
                .setRequestCode(POST_COMMENTS_CODE));
    }

    /**
     * 提交评论回复
     *
     * @param request
     * @param typeid
     * @param userid
     * @param movie_id
     * @param pid
     * @param content
     */
    public static void postComments(OkHttpRequest request, String typeid, String userid, String movie_id, String pid, String content) {
        OkGo.<String>post(POST_COMMENTS)
                .tag(request)
                .params("typeid", typeid)
                .params("uid", Config.getLoginInfo().uid)
                .params("userid", userid)
                .params("movie_id", movie_id)
                .params("pid", pid)
                .params("content", content)
                .params("md5",MD5Utils.md5())
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        LzyResponse lzyResponse = new LzyResponse();
                        try {
                            JSONObject jsonObject = new JSONObject(response.body());
                            lzyResponse.code = jsonObject.getInt("status");
                            if (lzyResponse.code == 1) {
                                JSONObject data = jsonObject.getJSONObject("data");
                                lzyResponse.data = data;
                            }
                            request.nofityUpdateUi(POST_COMMENTS_CODE, lzyResponse, null);
                        } catch (Exception e) {
                            ToastUtils.longToast("评论失败");
                        }

                    }

                    @Override
                    public void onError(Response<String> response) {
                        super.onError(response);
                        request.httpLoadFail(response.message());
                    }
                });
    }

    /**
     * 评论点赞
     *
     * @param netRequestHandler
     * @param mem_id
     */
    public static void CommentsDianzan(NetRequestHandler netRequestHandler, String mem_id) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(COMMENTS_DIANZAN)
                .put("id", mem_id)
                .put("md5",MD5Utils.md5())
                .setRequestCode(COMMENTS_DIANZAN_CODE));
    }

    /**
     * 评论点赞
     *
     * @param request
     * @param mem_id
     */
    public static void CommentsDianzanOk(OkHttpRequest request, String mem_id, final View view) {
        OkGo.<LzyResponse>post(COMMENTS_DIANZAN)
                .tag(request)
                .params("id", mem_id)
                .params("uid", Config.getLoginInfo().id)
                .params("md5",MD5Utils.md5())
                .execute(new JsonCallback<LzyResponse>() {
                    @Override
                    public void onSuccess(Response<LzyResponse> response) {
                        request.nofityUpdateUi(COMMENTS_DIANZAN_CODE, response.body(), view);
                    }

                    @Override
                    public void onError(Response<LzyResponse> response) {
                        super.onError(response);
                        request.httpLoadFail("网络异常");
                    }
                });
    }

    /**
     * 查看影片是否点赞或者收藏
     *
     * @param netRequestHandler
     * @param movieId
     */
    public static void getMoveieActionStates(NetRequestHandler netRequestHandler, String movieId) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(GET_MOVIE_ACTION_STATES)
                .put("movie_id", movieId)
                .put("md5",MD5Utils.md5())
                .setRequestCode(GET_MOVIE_ACTION_STATES_CODE)
                .setTypeToken(MovieActionStateBean.class));
    }

    /**
     * 查看影片是否点赞或者收藏
     *
     * @param request
     * @param movieId
     */
    public static void getMoveieActionStatesOk(OkHttpRequest request, String movieId) {
        OkGo.<LzyResponse<MovieActionStateBean>>post(GET_MOVIE_ACTION_STATES)
                .tag(request)
                .params("movie_id", movieId)
                .params("uid", Config.getLoginInfo().id)
                .params("md5",MD5Utils.md5())
                .execute(new JsonCallback<LzyResponse<MovieActionStateBean>>() {
                    @Override
                    public void onSuccess(Response<LzyResponse<MovieActionStateBean>> response) {
                        request.nofityUpdateUi(GET_MOVIE_ACTION_STATES_CODE, response.body(), null);
                    }
                });
    }

    /**
     * 视频点赞
     *
     * @param request
     * @param is_top
     * @param movieId
     */
    public static void movieZanOk(OkHttpRequest request, int is_top, String movieId) {
        OkGo.<LzyResponse>post(MOVIE_DIANZAN)
                .tag(request)
                .params("uid", Config.getLoginInfo().id)
                .params("is_top", is_top)
                .params("movie_id", movieId)
                .params("md5",MD5Utils.md5())
                .execute(new JsonCallback<LzyResponse>() {
                    @Override
                    public void onSuccess(Response<LzyResponse> response) {
                        request.nofityUpdateUi(MOVIE_DIANZAN_CODE, response.body(), null);
                    }

                    @Override
                    public void onError(Response<LzyResponse> response) {
                        super.onError(response);
                        request.httpLoadFail("点赞失败");
                    }
                });

    }

    /**
     * 视频点赞
     *
     * @param netRequestHandler
     * @param is_top
     * @param movieId
     */
    public static void movieZan(NetRequestHandler netRequestHandler, int is_top, String movieId) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(MOVIE_DIANZAN)
                .put("is_top", is_top)
                .put("movie_id", movieId)
                .put("md5",MD5Utils.md5())
                .setRequestCode(MOVIE_DIANZAN_CODE));
    }

    /**
     * 视频收藏
     *
     * @param netRequestHandler
     * @param movieId
     */
    public static void movieShoucang(NetRequestHandler netRequestHandler, String movieId) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(MOVIE_SHOUCANG)
                .put("movie_id", movieId)
                .put("md5",MD5Utils.md5())
                .setRequestCode(MOVIE_SHOUCANG_CODE));
    }

    /**
     * 视频收藏
     *
     * @param request
     * @param movieId
     */
    public static void movieShoucangByOkHttp(OkHttpRequest request, String movieId) {
        OkGo.<LzyResponse>post(MOVIE_SHOUCANG)
                .tag(request)
                .params("movie_id", movieId)
                .params("md5",MD5Utils.md5())
                .execute(new JsonCallback<LzyResponse>() {
                    @Override
                    public void onSuccess(Response<LzyResponse> response) {
                        request.nofityUpdateUi(MOVIE_SHOUCANG_CODE, response.body(), null);
                    }
                });

    }

    /**
     * 首页-热门搜索关键字
     *
     * @param netRequestHandler
     */
    public static void getHotSearchData(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynGet(new NAction()
                .setUrl(GET_HOT_SEARCH)
                .setRequestCode(GET_HOT_SEARCH_CODE)
                .setTypeToken(new TypeToken<List<HotSearchBean>>() {
                }));
    }

    /**
     * 搜索
     *
     * @param netRequestHandler
     * @param keyWords
     * @param page
     */
    public static void getSearchList(NetRequestHandler netRequestHandler, String keyWords, int page) {
        netRequestHandler.requestAsynGet(new NAction()
                .setUrl(GET_SEARCH)
                .put("keywords", keyWords)
                .put("p", page)
                .put("md5",MD5Utils.md5())
                .setRequestCode(GET_SEARCH_CODE)
                .setTypeToken(new TypeToken<List<MovieBean>>() {
                }));
    }

    /**
     * 个人中心广告
     *
     * @param netRequestHandler
     */
    public static void getAdvertisementData(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynGet(new NAction()
                .setUrl(GET_ADVER_DATA)
                .setRequestCode(GET_ADVER_CODE)
                .setTypeToken(new TypeToken<List<AdverBean>>() {
                }));
    }

    /**
     * 会员充值信息
     *
     * @param netRequestHandler
     */
    public static void getRechargeInfo(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynGet(Config.UAction()
                .setUrl(GET_RECHARGE_INFO)
                .setRequestCode(GET_RECHARGE_INFO_CODE)
                .setTypeToken(RechargeBean.class));
    }

    /**
     * 金币充值套餐信息
     *
     * @param netRequestHandler
     */
    public static void getGoldInfo(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynGet(new NAction()
                .setUrl(GET_GOLD_MEAL)
                .setRequestCode(GET_GOLD_MEAL_CODE)
                .setTypeToken(new TypeToken<List<RechargeBean.VipMealBean>>() {
                }));
    }

    /**
     * 扣除金币
     *
     * @param netRequestHandler
     */
    public static void deductGoldAccount(NetRequestHandler netRequestHandler, String goldCount) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(DEDUCT_GOLD_ACCOUNT)
                .put("gold", goldCount)
                .put("md5",MD5Utils.md5())
                .setRequestCode(DEDUCT_GOLD_ACCOUNT_CODE)
                .setTypeToken(BaseBean.class));
    }

    /**
     * 购买会员-购买记录
     *
     * @param netRequestHandler
     * @param page
     */
    public static void getMemberConsume(NetRequestHandler netRequestHandler, int page) {
        netRequestHandler.requestAsynGet(Config.UAction()
                .setUrl(GET_CONSUME)
                .put("p", page)
                .put("md5",MD5Utils.md5())
                .setRequestCode(GET_CONSUME_CODE)
                .setTypeToken(new TypeToken<List<MemberSpendBean>>() {
                }));
    }

    /**
     * 首页-换一批
     *
     * @param netRequestHandler
     */
    public static void getChangeData(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynGet(new NAction()
                .setUrl(GET_CHANGE_DATA)
                .setRequestCode(GET_CHANGE_DATA_CODE)
                .setTypeToken(new TypeToken<List<HomeBean.DataListBean>>() {
                }));

    }

    /**
     * 【支付】
     *
     * @param netRequestHandler
     * @param id
     * @param pay_id
     */

    public static void getPayUrl(NetRequestHandler netRequestHandler, String id, String pay_id) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(GET_PAY_URL)
                .put("combo_price_id", id)
                .put("pay_id", pay_id)
                .put("md5",MD5Utils.md5())
                .setRequestCode(GET_PAY_URL_CODE));
    }

    /**
     * 【支付】通道列表
     *
     * @param netRequestHandler
     * @param id
     */
    public static void getPayChanel(NetRequestHandler netRequestHandler, String id) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(GET_PAY_CHANEL)
                .put("type", id)
                .put("md5",MD5Utils.md5())
                .setRequestCode(GET_PAY_CHANEL_CODE)
                .setTypeToken(new TypeToken<List<PayChannelBean>>() {
                }));
    }

    /**
     * 首页公告弹窗
     *
     * @param netRequestHandler
     */
    public static void getHomeNotice(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynGet(new NAction()
                .setUrl(GET_HOME_NOTICE)
                .setRequestCode(GET_HOME_NOTICE_CODE)
                .setTypeToken(HomeNoticeBean.class));
    }

    /**
     * 视频播放接口
     *
     * @param netRequestHandler
     * @param movieId
     */
    public static void getMovieUrl(NetRequestHandler netRequestHandler, String movieId) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(GET_MOVIE_URL)
                .put("movie_id", movieId)
                .put("md5",MD5Utils.md5())
                .setRequestCode(GET_MOVIE_URL_CODE)
                .setTypeToken(MovieBean.class));
    }


    /**
     * 标签搜索
     *
     * @param netRequestHandler
     * @param label
     * @param page
     */

    public static void getLableSearchList(NetRequestHandler netRequestHandler, String label, int page) {
        netRequestHandler.requestAsynGet(new NAction()
                .setUrl(GET_lable_SEARCH)
                .put("label", label)
                .put("p", page)
                .put("md5",MD5Utils.md5())
                .setRequestCode(GET_lable_SEARCH_CODE)
                .setTypeToken(new TypeToken<List<MovieBean>>() {
                }));
    }

    /**
     * 推广
     *
     * @param netRequestHandler
     */
    public static void getTuiGuang(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynGet(new NAction()
                .setUrl(GET_TUIGUANG)
                .setRequestCode(GET_TUIGUANG_CODE)
                .setTypeToken(TuiguangBean.class));
    }

    /**
     * 下载添加用户记录
     *
     * @param netRequestHandler
     * @param title
     * @param uid
     * @param type
     */
    public static void getAddMemberRecord(NetRequestHandler netRequestHandler, String title, String uid, String type) {
        netRequestHandler.requestAsynPost(new NAction()
                .setUrl(POST_ADD_MEMBER_RECORD)
                .put("title", title)
                .put("uid", uid)
                .put("type", type)
                .put("md5",MD5Utils.md5())
                .setRequestCode(POST_ADD_MEMBER_RECORD_CODE));
    }

    /**
     * 版本更新
     *
     * @param netRequestHandler
     */
    public static void getVersionUpdate(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynPost(new NAction()
                .setUrl(GET_VERSION_UPDATE)
                .put("type", 1)
                .put("version", AppUtils.getVersionName(CommonApp.getInstance()))
                .put("md5",MD5Utils.md5())
                .setRequestCode(GET_VERSION_UPDATE_CODE)
                .setTypeToken(VersionBean.class));
    }

    /**
     * 【安卓】专题分类详情
     *
     * @param netRequestHandler
     * @param id
     */
    public static void getHeaderInfo(NetRequestHandler netRequestHandler, String id) {
        netRequestHandler.requestAsynPost(new NAction()
                .setUrl(GET_HEDER_INFO)
                .put("id", id)
                .put("md5",MD5Utils.md5())
                .setRequestCode(GET_HEDER_INFO_CODE)
                .setTypeToken(ThematicBean.class));
    }

    /**
     * 获取支持的支付方式
     *
     * @param that
     */
    public static void getPayMethod(NetRequestHandler that) {
        that.requestAsynGet(new NAction()
                .setUrl(GET_PAY_METHOD)
                .setRequestCode(GET_PAY_METHOD_CODE)
                .setTypeToken(new TypeToken<List<PayMentBean>>() {
                }));
    }

    /**
     * 获取支付使用原生或者第三方支付平台
     *
     * @param that
     */
    public static void getPayWay(NetRequestHandler that) {
        that.requestAsynGet(new NAction()
                .setUrl(GET_PAY_METHOD_NEW)
                .setRequestCode(GET_PAY_METHOD_NEW_CODE)
                .setTypeToken(String.class));
    }

    /**
     * 获取发现页面分类列表
     *
     * @param netRequestHandler
     */
    public static void getTabMenuList(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynGet(new NAction()
                .setUrl(GET_TAB_LIST)
                .setRequestCode(GET_TAB_LIST_CODE)
                .setTypeToken(new TypeToken<List<TabBar>>() {
                }));
    }

    /**
     * 配置信息
     *
     * @param netRequestHandler
     */
    public static void getSettingInfo(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynGet(new NAction()
                .setUrl(SETTING_INFO)
                .setRequestCode(SETTING_INFO_CODE)
                .setTypeToken(new TypeToken<List<SettingInfo>>() {
                }));
    }

    /**
     * 第三方登录的配置信息
     *
     * @param netRequestHandler
     */
    public static void getThirdLoginParam(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynGet(new NAction()
                .setUrl(GET_THIRD_LOGIN_PARAM)
                .setRequestCode(GET_THIRD_LOGIN_PARAM_CODE)
                .setTypeToken(new TypeToken<List<ThirdBean>>() {
                }));
    }

    /**
     * 获取主题颜色
     *
     * @param netRequestHandler
     */
    public static void getNavbarColor(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynGet(new NAction()
                .setUrl(GET_NAVBAR_COLOR)
                .setRequestCode(GET_NAVBAR_COLOR_CODE)
                .setTypeToken(new TypeToken<NavbarColorBean>() {
                }));
    }

    /**
     * 频道页
     *
     * @param netRequestHandler
     */
    public static void getRecommendList(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynGet(new NAction()
                .setUrl(HOT_LIST)
                .setRequestCode(HOT_LIST_CODE)
                .setTypeToken(new TypeToken<List<Recommend>>() {
                }));
    }

    public static void getHotShowList(NetRequestHandler netRequestHandler, int page, int cate_id) {
        netRequestHandler.requestAsynGet(new NAction()
                .setUrl(HOT_SHOW)
                .put("page", page)
                .put("pageSize", 10)
                .put("cate_id", cate_id)
                .put("md5",MD5Utils.md5())
                .setRequestCode(HOT_SHOW_CODE)
                .setTypeToken(new TypeToken<List<MovieBean>>() {
                }));
    }

    public static void createOrder(NetRequestHandler netRequestHandler, RechargeBean.VipMealBean item) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(CREATE_ORDER)
                .put("combo_price_id", item.id)
                .put("md5",MD5Utils.md5())
                .setRequestCode(CREATE_ORDER_CODE));
    }

    /**
     * 生成微信订单
     *
     * @param netRequestHandler
     * @param item
     */
    public static void createWeixinrder(NetRequestHandler netRequestHandler, RechargeBean.VipMealBean item, int rechargeType) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(CREATE_WEIXIN_ORDER)
                .put("combo_price_id", item.id)
                .put("meal_type", rechargeType + "")
                .put("price", item.price)
                .put("md5",MD5Utils.md5())
                .setRequestCode(CREATE_WEIXIN_ORDER_CODE)
                .setTypeToken(WxPayBean.class));
    }

    public static void createWeixinrderV3(NetRequestHandler netRequestHandler, RechargeBeanV3 item, int rechargeType) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(CREATE_WEIXIN_ORDER)
                .put("combo_price_id", item.id)
                .put("meal_type", rechargeType + "")
                .put("price", item.discount_price)
                .put("md5",MD5Utils.md5())
                .setRequestCode(CREATE_WEIXIN_ORDER_CODE)
                .setTypeToken(WxPayBean.class));
    }

    public static void createWeixinrderV3(NetRequestHandler netRequestHandler, GoldMealBean item, int rechargeType) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(CREATE_WEIXIN_ORDER)
                .put("combo_price_id", item.id)
                .put("meal_type", rechargeType + "")
                .put("price", item.discount_price)
                .put("md5",MD5Utils.md5())
                .setRequestCode(CREATE_WEIXIN_ORDER_CODE_1)
                .setTypeToken(WxPayBean.class));
    }


    /**
     * 支付
     *
     * @param netRequestHandler
     * @param item
     */
    public static void postCreateOrder(NetRequestHandler netRequestHandler, RechargeBean.VipMealBean item, int rechargeType) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(CREATE_PAYMENT_ORDER)
                .put("combo_price_id", item.id)
                .put("meal_type", rechargeType + "")
                .put("price", item.price)
                .put("md5",MD5Utils.md5())
                .setRequestCode(CREATE_PAYMENT_ORDER_CODE));
    }

    /**
     * 支付宝支付 新
     *
     * @param netRequestHandler
     * @param item
     * @param rechargeType
     */
    public static void postCreateOrderV3(NetRequestHandler netRequestHandler, RechargeBeanV3 item, int rechargeType) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(CREATE_PAYMENT_ORDER)
                .put("combo_price_id", item.id)
                .put("meal_type", rechargeType + "")
                .put("price", item.discount_price)
                .put("md5",MD5Utils.md5())
                .setRequestCode(CREATE_PAYMENT_ORDER_CODE));
    }

    public static void postCreateOrderV3(NetRequestHandler netRequestHandler, GoldMealBean item, int rechargeType) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(CREATE_PAYMENT_ORDER)
                .put("combo_price_id", item.id)
                .put("meal_type", rechargeType + "")
                .put("price", item.discount_price)
                .put("md5",MD5Utils.md5())
                .setRequestCode(CREATE_PAYMENT_ORDER_CODE_1));
    }


    /**
     * 支付地址
     *
     * @param netRequestHandler
     * @param id
     */
    public static void postPayUrl(NetRequestHandler netRequestHandler, String id) {
        netRequestHandler.requestAsynGet(Config.UAction()
                .setUrl(POST_PAY_URL)
                .put("id", id)
                .put("md5",MD5Utils.md5())
                .setRequestCode(POST_PAY_URL_CODE)
                .setTypeToken(PayUrlBean.class));
    }


    /**
     * 获取广告列表
     *
     * @param netRequestHandler
     */
    public static void getAdList(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(AD_LIST)
                .put("user_id", Config.getLoginInfo().id)
                .put("md5",MD5Utils.md5())
                .setRequestCode(AD_LIST_CODE)
                .setTypeToken(new TypeToken<List<Ad>>() {
                }));
    }

    /**
     * 获取广告列表
     *
     * @param netRequestHandler
     */
    public static void deleteAd(NetRequestHandler netRequestHandler, String adId) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(AD_DELETE)
                .put("user_id", Config.getLoginInfo().id)
                .put("id", adId)
                .put("md5",MD5Utils.md5())
                .setRequestCode(AD_DELETE_CODE)
                .setTypeToken(new TypeToken<List<Object>>() {
                }));
    }


    /**
     * 获取省市
     *
     * @param netRequestHandler
     */
    public static void getRegion(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynGet(Config.UAction()
                .setUrl(GET_REGION)
                .setRequestCode(GET_REGION_CODE)
                .setTypeToken(new TypeToken<List<RegionBean>>() {

                })

        );
    }

    /**
     * 绑定手机
     */
    public static void bindPhone(NetRequestHandler netRequestHandler, String phone, String code) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(BIND_PHONE)
                .put("phone", phone)
                .put("code", code)
                .put("md5",MD5Utils.md5())
                .setRequestCode(BIND_PHONE_CODE)
                .setTypeToken(new TypeToken<List<RegionBean>>() {

                })

        );
    }

    /**
     * 下载视频
     */
    public static void downLoadVideo(OkHttpRequest request, String url) {
        String dir = FileUtils.getMediaDownloadDir();
        String fileName = FileUtils.getUrlToFileName(url);
        OkGo.<File>get(url).tag(request).execute(new FileCallback(dir, fileName) {
            @Override
            public void onSuccess(Response<File> response) {
                request.downLoadFinish(response.body());
                FileUtils.videoInsertAlbum(CommonApp.getApplication(), response.body());
            }

            @Override
            public void downloadProgress(Progress progress) {
                super.downloadProgress(progress);
                request.onProgress(progress);
            }

            @Override
            public void onError(Response<File> response) {
                super.onError(response);
                request.httpLoadFail("下载失败");
            }

            @Override
            public void onFinish() {
                super.onFinish();
                request.httpLoadFinal();
            }
        });

    }

    /**
     * 记录视频下载次数
     */
    public static void updateMovieDownloadNum(OkHttpRequest request, String movie_id) {
        OkGo.<LzyResponse>post(UPDATE_MOVIE_DOWNLOAD_NUM)
                .tag(request)
                .params("movie_id", movie_id)
                .params("md5",MD5Utils.md5())
                .execute(new JsonCallback<LzyResponse>() {
                    @Override
                    public void onSuccess(Response<LzyResponse> response) {
                        request.nofityUpdateUi(UPDATE_MOVIE_DOWNLOAD_NUM_CODE, response.body(), null);
                    }
                });
    }

    //判断用户是否关注过
    public static void checkUserAttention(OkHttpRequest request, String attention_uid) {

        if ("0".equals(attention_uid)) {
            attention_uid = "5";
        }

        OkGo.<LzyResponse>post(CHECK_USER_ATTENTION)
                .tag(request)
//                .upJson(body)
                .params("attention_uid", attention_uid)
                .params("uid", Config.getLoginInfo().id)
                .params("md5",MD5Utils.md5())
                .execute(new JsonCallback<LzyResponse>() {
                    @Override
                    public void onSuccess(Response<LzyResponse> response) {
                        request.nofityUpdateUi(CHECK_USER_ATTENTION_CODE, response.body(), null);
                    }
                });
    }

    /**
     * 添加视频分享次数
     */
    public static void updateMovieShareNum(OkHttpRequest request, String movie_id) {
        OkGo.<LzyResponse>post(UPDATE_MOVIE_SHARE_NUM)
                .tag(request)
                .params("movie_id", movie_id)
                .params("md5",MD5Utils.md5())
                .execute(new JsonCallback<LzyResponse>() {
                    @Override
                    public void onSuccess(Response<LzyResponse> response) {
                        request.nofityUpdateUi(UPDATE_MOVIE_SHARE_NUM_CODE, response.body(), null);
                    }
                });
    }

    //关注用户
    public static void followUser(OkHttpRequest request, String attention_id) {
        OkGo.<LzyResponse<List<StateBean>>>post(ATTENTION_USER)
                .tag(request)
                .params("attention_uid", attention_id)
                .params("uid", Config.getLoginInfo().id)
                .params("md5",MD5Utils.md5())
                .execute(new JsonCallback<LzyResponse<List<StateBean>>>() {
                    @Override
                    public void onSuccess(Response<LzyResponse<List<StateBean>>> response) {
                        request.nofityUpdateUi(ATTENTION_USER_CODE, response.body(), null);
                    }

                    @Override
                    public void onError(Response<LzyResponse<List<StateBean>>> response) {
                        super.onError(response);
                        request.httpLoadFail(response.message());
                    }
                });
    }

    //取消关注
    public static void cancelAttention(OkHttpRequest request, String attention_id) {
        OkGo.<LzyResponse<List<StateBean>>>post(CANCEL_ATTENTION)
                .tag(request)
                .params("attention_uid", attention_id)
                .params("uid", Config.getLoginInfo().id)
                .params("md5",MD5Utils.md5())
                .execute(new JsonCallback<LzyResponse<List<StateBean>>>() {
                    @Override
                    public void onSuccess(Response<LzyResponse<List<StateBean>>> response) {
                        request.nofityUpdateUi(CANCEL_ATTENTION_CODE, response.body(), null);
                    }

                    @Override
                    public void onError(Response<LzyResponse<List<StateBean>>> response) {
                        super.onError(response);
                        request.httpLoadFail(response.message());
                    }
                });
    }

    //获取二级评论列表
    public static void getMovieChildComment(OkHttpRequest request, String parent_id, int page) {
        HttpParams params = new HttpParams();
        params.put("uid", Config.getLoginInfo().id);
        params.put("parent_id", parent_id);
        params.put("detail_page", page);
        params.put("detail_count", 6);
        params.put("md5",MD5Utils.md5());
        OkGo.<String>post(GET_MOVIE_CHILD_COMMENT)
                .tag(request)
                .params(params)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        try {
                            LzyResponse lzyResponse = new LzyResponse();
                            lzyResponse.code = 1;

                            ArrayList<MovieCommentSectionEntity> entities = new ArrayList<>();
                            JSONObject jsonObject = new JSONObject(response.body());
                            if (jsonObject.getInt("status") == 1) {
                                JSONArray data = jsonObject.getJSONArray("data");
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject detailsJSONObject = data.getJSONObject(i);
                                    MovieCommentBean bean = new MovieCommentBean();
                                    bean.setUid(detailsJSONObject.getString("uid"));
                                    bean.setParentId(parent_id);
                                    bean.setNickname(detailsJSONObject.getString("nickname"));
                                    bean.setContent(detailsJSONObject.getString("content"));
                                    bean.setLike_num(detailsJSONObject.getInt("like_num"));
                                    bean.setAdd_time(detailsJSONObject.getString("add_time"));
                                    bean.setId(detailsJSONObject.getString("id"));
                                    bean.setPid(detailsJSONObject.getString("pid"));
                                    bean.setIcon(detailsJSONObject.getString("icon"));
                                    bean.setIs_like(detailsJSONObject.getInt("is_like"));
                                    bean.setReply_user(detailsJSONObject.getString("reply_user"));
                                    bean.setPage(page);
                                    int details_next_page = detailsJSONObject.getInt("details_next_page");
                                    if (i == data.length() - 1) {
                                        bean.setDetails_next_page(details_next_page);
                                    } else {
                                        bean.setDetails_next_page(0);
                                    }
                                    MovieCommentSectionEntity entity = new MovieCommentSectionEntity(bean);
                                    entities.add(entity);
                                }
                                lzyResponse.data = entities;
                                request.nofityUpdateUi(GET_MOVIE_CHILD_COMMENT_CODE, lzyResponse, null);
                            }


                        } catch (Exception e) {
                            ToastUtils.longToast("加载失败");
                            Log.e("OkGo:", e.toString());
                        }
                    }

                    @Override
                    public void onError(Response<String> response) {
                        super.onError(response);
                        Log.e("OkGo:", response.getException().toString());
                        ToastUtils.longToast("获取失败");
                    }
                });
    }

    //获取评论列表
    public static void getMovieComment(OkHttpRequest request, String movie_id, int page) {
        HttpParams params = new HttpParams();
        params.put("md5",MD5Utils.md5());
        params.put("movie_id", movie_id);
        params.put("page", page);
        params.put("uid", Config.getLoginInfo().id);
        params.put("count", 10);
        params.put("detail_page", 1);
        params.put("detail_count", 3);
        OkGo.<String>post(GET_MOVIE_COMMENT)
                .tag(request)
                .params(params)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        try {
                            LzyResponse lzyResponse = new LzyResponse();
                            JSONObject jsonObject = new JSONObject(response.body());
                            lzyResponse.code = jsonObject.getInt("status");
                            JSONArray data = jsonObject.getJSONArray("data");
                            ArrayList<MovieCommentSectionEntity> movieCommenBeans = new ArrayList<>();
                            for (int i = 0; i < data.length(); i++) {
                                MovieCommentBean commenBean = new MovieCommentBean();
                                JSONObject object = data.getJSONObject(i);
                                String id = object.getString("id");
                                commenBean.setId(id);
                                commenBean.setUid(object.getString("uid"));
                                commenBean.setNickname(object.getString("nickname"));
                                commenBean.setContent(object.getString("content"));
                                commenBean.setLike_num(object.getInt("like_num"));
                                commenBean.setAdd_time(object.getString("add_time"));
                                commenBean.setIcon(object.getString("icon"));
                                commenBean.setIs_like(object.getInt("is_like"));

                                MovieCommentSectionEntity sectionEntity = new MovieCommentSectionEntity(commenBean);
                                sectionEntity.isHeader = true;
                                movieCommenBeans.add(sectionEntity);

                                int detail_count = object.getInt("detail_count");
                                commenBean.setDetail_count(detail_count);
                                Object o = object.get("details");
                                if (o != null && !"null".equals(o)) {
                                    JSONArray details = object.getJSONArray("details");
                                    for (int j = 0; j < details.length(); j++) {
                                        JSONObject detailsJSONObject = details.getJSONObject(j);
                                        MovieCommentBean bean = new MovieCommentBean();
                                        bean.setDetail_count(detail_count);
                                        bean.setUid(detailsJSONObject.getString("uid"));
                                        bean.setNickname(detailsJSONObject.getString("nickname"));
                                        bean.setContent(detailsJSONObject.getString("content"));
                                        bean.setLike_num(detailsJSONObject.getInt("like_num"));
                                        bean.setAdd_time(detailsJSONObject.getString("add_time"));
                                        bean.setId(detailsJSONObject.getString("id"));
                                        bean.setPid(detailsJSONObject.getString("pid"));
                                        bean.setIcon(detailsJSONObject.getString("icon"));
                                        bean.setIs_like(detailsJSONObject.getInt("is_like"));
                                        bean.setReply_user(detailsJSONObject.getString("reply_user"));
                                        int details_next_page = detailsJSONObject.getInt("details_next_page");
                                        if (j == details.length() - 1) {
                                            bean.setDetails_next_page(details_next_page);
                                        } else {
                                            bean.setDetails_next_page(0);
                                        }

                                        bean.setParentId(id);
                                        MovieCommentSectionEntity entity = new MovieCommentSectionEntity(bean);
                                        entity.isHeader = false;
                                        movieCommenBeans.add(entity);
                                    }
                                }

                            }
                            lzyResponse.data = movieCommenBeans;
                            request.nofityUpdateUi(GET_MOVIE_COMMENT_CODE, lzyResponse, null);
                        } catch (Exception e) {
                            Log.e("OkGo:", e.toString());
                        }
                    }
                });
    }


    //获取我的视频列表
//    所有0,公开视频1,收费视频2,私密视频3,4k视频4,教学视频5,分享专用视频6
    public static void getMineVideos(OkHttpRequest request, int type, int page, int count) {
        HttpParams params = new HttpParams();
//        params.put("uid", Config.getLoginInfo().id);
        params.put("uid", 198);
        params.put("type", type);
        params.put("page", page);
        params.put("count", count);
        params.put("md5", MD5Utils.md5());
        OkGo.<LzyResponse<MyVideoData>>post(GET_MINE_VIDEOS)
                .tag(request)
                .params(params)
                .execute(new JsonCallback<LzyResponse<MyVideoData>>() {
                    @Override
                    public void onSuccess(Response<LzyResponse<MyVideoData>> response) {
                        request.nofityUpdateUi(GET_MINE_VIDEOS_CODE, response.body(), null);
                    }

                    @Override
                    public void onError(Response<LzyResponse<MyVideoData>> response) {
                        super.onError(response);
                        request.httpLoadFail("网络错误");
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        LoadDialogUtils.dissmiss();
                    }
                });
    }

    //获取其他用户的视频列表
    public static void getOtherUserVideos(OkHttpRequest request, int type, int page, int count, String uid) {
        HttpParams params = new HttpParams();
        params.put("uid", uid);
        params.put("type", type);
        params.put("page", page);
        params.put("count", count);
        params.put("md5",MD5Utils.md5());
        OkGo.<LzyResponse<MyVideoData>>post(GET_MINE_VIDEOS)
                .tag(request)
                .params(params)
                .execute(new JsonCallback<LzyResponse<MyVideoData>>() {
                    @Override
                    public void onSuccess(Response<LzyResponse<MyVideoData>> response) {
                        request.nofityUpdateUi(GET_MINE_VIDEOS_CODE, response.body(), null);
                    }

                    @Override
                    public void onError(Response<LzyResponse<MyVideoData>> response) {
                        super.onError(response);
                        request.httpLoadFail("网络错误");
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        LoadDialogUtils.dissmiss();
                    }
                });
    }

    /**
     * 获取上传视频的分类
     *
     * @param request
     */
    public static void getVideoTypeList(OkHttpRequest request, Context context) {
        LoadDialogUtils.showDialog(context);
        OkGo.<LzyResponse<TypeBean>>post(GET_VIDEOS_TYPE)
                .tag(request)
                .execute(new JsonCallback<LzyResponse<TypeBean>>() {
                    @Override
                    public void onSuccess(Response<LzyResponse<TypeBean>> response) {
                        request.nofityUpdateUi(GET_VIDEOS_TYPE_CODE, response.body(), null);
                    }

                    @Override
                    public void onError(Response<LzyResponse<TypeBean>> response) {
                        super.onError(response);
                        ToastUtils.longToast("网络异常，请重试");
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        LoadDialogUtils.dissmiss();
                    }
                });


    }

    /**
     * 获取上传视频的标签
     *
     * @param request
     */
    public static void getVideoLabeList(OkHttpRequest request, Context context) {
        LoadDialogUtils.showDialog(context);
        OkGo.<LzyResponse<List<LabeBean>>>post(GET_VIDEOS_LABE)
                .tag(request)
                .execute(new JsonCallback<LzyResponse<List<LabeBean>>>() {
                    @Override
                    public void onSuccess(Response<LzyResponse<List<LabeBean>>> response) {
                        List<LabeBean> data = response.body().data;
                        Collections.sort(data, new Comparator<LabeBean>() {
                            @Override
                            public int compare(LabeBean labeBean, LabeBean t1) {
                                String s1 = PinyinUtil.converterToFirstSpell(labeBean.getTitle());
                                String s2 = PinyinUtil.converterToFirstSpell(labeBean.getTitle());
                                return Collator.getInstance(Locale.ENGLISH).compare(s1, s2);
                            }
                        });
                        HashMap<String, ArrayList<LabeBean>> map = new HashMap<>();
                        ArrayList<String> zm = new ArrayList<>();
                        for (int i = 0; i < data.size(); i++) {
                            LabeBean labeBean = data.get(i);
                            String firstSpell = PinyinUtil.converterToFirstSpell(labeBean.getTitle());
                            if (!zm.contains(firstSpell)) {
                                zm.add(firstSpell);
                            }
                            List<LabeBean> labeBeans = map.get(firstSpell);
                            if (EmptyDeal.isEmpy(labeBeans)) {
                                ArrayList<LabeBean> beans = new ArrayList<>();
                                beans.add(labeBean);
                                map.put(firstSpell, beans);
                            } else {
                                labeBeans.add(labeBean);
                            }
                        }
                        ArrayList<LabeBean> labeBeanArrayList = new ArrayList<>();
                        for (int i = 0; i < zm.size(); i++) {
                            LabeBean bean = new LabeBean();
                            bean.setTitle(zm.get(i));
                            bean.setLabeBeans(map.get(bean.getTitle()));
                            labeBeanArrayList.add(bean);
                        }
                        LzyResponse lzyResponse = new LzyResponse();
                        lzyResponse.code = 1;
                        lzyResponse.data = labeBeanArrayList;
                        request.nofityUpdateUi(GET_VIDEOS_LABE_CODE, lzyResponse, null);

                    }

                    @Override
                    public void onError(Response<LzyResponse<List<LabeBean>>> response) {
                        super.onError(response);
                        ToastUtils.longToast("网络异常，请重试");
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        LoadDialogUtils.dissmiss();
                    }
                });


    }


    /**
     * 获取视频收益中页面中的视频列表
     */
    public static void getProfitVideoList(OkHttpRequest request, int page) {
        HttpParams params = new HttpParams();
        params.put("uid", Config.getLoginInfo().id);
        params.put("cate_id", 16);
        params.put("pageSize", 10);
        params.put("md5",MD5Utils.md5());
        params.put("page", page);
        OkGo.<LzyResponse<List<MyVideoBean>>>post(GET_PROFIT_VIDEO_LIST)
                .tag(request)
                .params(params)
                .execute(new JsonCallback<LzyResponse<List<MyVideoBean>>>() {
                    @Override
                    public void onSuccess(Response<LzyResponse<List<MyVideoBean>>> response) {
                        request.nofityUpdateUi(GET_PROFIT_VIDEO_LIST_CODE, response.body(), null);
                    }

                    @Override
                    public void onError(Response<LzyResponse<List<MyVideoBean>>> response) {
                        super.onError(response);
                        ToastUtils.longToast("网络异常");
                    }
                });
    }

    /**
     * 获取视频收益中页面中的专辑列表
     *
     * @param request
     */
    public static void getProfitAlbumList(OkHttpRequest request) {
        OkGo.<String>post(GET_PROFIT_ALBUM_LIST)
                .tag(request)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        try {
                            LzyResponse lzyResponse = new LzyResponse();
                            JSONObject jsonObject = new JSONObject(response.body());
                            int status = jsonObject.getInt("status");
                            lzyResponse.code = status;
                            if (lzyResponse.code == 1) {
                                JSONArray data = jsonObject.getJSONArray("data");
                                ArrayList<MyVideoBean> videoBeans = new ArrayList<>();
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject dataJSONObject = data.getJSONObject(i);
                                    MyVideoBean bean = new MyVideoBean();
                                    bean.setTitle(dataJSONObject.getString("title"));
                                    bean.setType(1);
                                    videoBeans.add(bean);

                                    String ob = dataJSONObject.getString("data");
                                    if (!StringUtils.isNull(ob)) {
                                        JSONArray jsonArray = dataJSONObject.getJSONArray("data");
                                        for (int j = 0; j < jsonArray.length(); j++) {
                                            JSONObject object = jsonArray.getJSONObject(j);
                                            MyVideoBean myVideoBean = new MyVideoBean();
                                            myVideoBean.setType(2);
                                            myVideoBean.setTitle(object.getString("title"));
                                            videoBeans.add(myVideoBean);
                                        }
                                    }
                                }
                                lzyResponse.data = videoBeans;
                            } else {
                                lzyResponse.code = 0;
                            }
                            request.nofityUpdateUi(GET_PROFIT_ALBUM_LIST_CODE, lzyResponse, null);
                        } catch (Exception e) {
                            Log.e("OkGo:", e.toString());
                            request.httpLoadFail("数据格式异常");
                        }
                    }

                    @Override
                    public void onError(Response<String> response) {
                        super.onError(response);
                        request.httpLoadFail("网络错误");
                    }

                });
    }

    /**
     * 获取用户提现记录的接口
     *
     * @param request
     */
    public static void getCashList(OkHttpRequest request, Context context) {
        LoadDialogUtils.showDialog(context);
        OkGo.<LzyResponse<List<GetCashBean>>>post(GET_GET_CASH_LIST)
                .tag(request)
                .params("uid", Config.getLoginInfo().id)
                .execute(new JsonCallback<LzyResponse<List<GetCashBean>>>() {
                    @Override
                    public void onSuccess(Response<LzyResponse<List<GetCashBean>>> response) {
                        request.nofityUpdateUi(GET_GET_CASH_LIST_CODE, response.body(), null);
                    }

                    @Override
                    public void onError(Response<LzyResponse<List<GetCashBean>>> response) {
                        super.onError(response);
                        ToastUtils.longToast("网络异常");
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        LoadDialogUtils.dissmiss();
                    }
                });
    }

    //获取上传视频的凭证
    public static void get_createUploadVideo(OkHttpRequest request, String fileName, String ma) {
        OkGo.<String>post(GET_CREATEUPLOADVIDEO)
                .tag(request)
                .params("fileName", fileName)
                .params("ma", ma)
                .params("md5",MD5Utils.md5())
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        VoucherBean bean = new VoucherBean();
                        try {
                            JSONObject jsonObject = new JSONObject(response.body());
                            int code = jsonObject.getInt("status");
                            if (code == 1) {
                                JSONObject data = jsonObject.getJSONObject("data");
                                bean.setUploadAddress(data.getString("UploadAddress"));
                                bean.setRequestId(data.getString("RequestId"));
                                bean.setVideoId(data.getString("VideoId"));
                                bean.setUploadAuth(data.getString("UploadAuth"));
                                LzyResponse lzyResponse = new LzyResponse();
                                lzyResponse.code = code;
                                lzyResponse.data = bean;
                                request.nofityUpdateUi(GET_CREATEUPLOADVIDEO_CODE, lzyResponse, null);
                            } else {
                                ToastUtils.longToast("凭证获取失败");
                            }

                        } catch (Exception e) {

                        }
                    }

                    @Override
                    public void onError(Response<String> response) {
                        super.onError(response);
                        ToastUtils.longToast("凭证获取失败");
                    }
                });
    }


    //我的购买的视频列表
    public static void getMyBuyVideos(OkHttpRequest request) {
        OkGo.<LzyResponse<List<MyVideoBean>>>post(GET_MY_BUY_VIDEOS)
                .tag(request)
                .params("uid", Config.getLoginInfo().id)
                .params("md5",MD5Utils.md5())
                .execute(new JsonCallback<LzyResponse<List<MyVideoBean>>>() {
                    @Override
                    public void onSuccess(Response<LzyResponse<List<MyVideoBean>>> response) {
                        request.nofityUpdateUi(GET_MY_BUY_VIDEOS_CODE, response.body(), null);
                    }

                    @Override
                    public void onError(Response<LzyResponse<List<MyVideoBean>>> response) {
                        super.onError(response);
                        ToastUtils.longToast("网络异常");
                    }
                });

    }

    //我的购买的专辑列表
    public static void getMyBuyAlbum(int page, OkHttpRequest request) {
        OkGo.<LzyResponse<List<BuyAlbumBean>>>post(GET_MY_BUY_ALBUM)
                .tag(request)
                .params("uid", Config.getLoginInfo().id)
                .params("page", page)
                .params("count", "10")
                .params("md5",MD5Utils.md5())
                .execute(new JsonCallback<LzyResponse<List<BuyAlbumBean>>>() {
                    @Override
                    public void onSuccess(Response<LzyResponse<List<BuyAlbumBean>>> response) {
                        request.nofityUpdateUi(GET_MY_BUY_ALBUM_CODE, response.body(), null);
                    }

                    @Override
                    public void onError(Response<LzyResponse<List<BuyAlbumBean>>> response) {
                        super.onError(response);
                        ToastUtils.longToast("网络异常");

                    }
                });

    }


    //发布视频
    public static void addVideo(OkHttpRequest httpRequest, MProgressBarDialog dialog, String title, String image, String videoid, String label,
                                String gold, int user_movie_type, String content, String home_category_id) {
        int is_free = 0;//是否免费 0否1是
        HttpParams params = new HttpParams();
        params.put("title", title);
        params.put("label", label);
        params.put("image", image);
        params.put("is_free", is_free);
        params.put("videoid", videoid);
        params.put("home_category_id", home_category_id);
        params.put("md5",MD5Utils.md5());
        if (StringUtils.isNull(gold) || TextUtils.equals("0", gold)) {
            gold = "0"; // 免费
            params.put("is_free", 1);
        } else {
            // 不免费
            params.put("is_free", 0);
        }
        params.put("gold", gold);
        params.put("user_movie_type", user_movie_type);
        params.put("user_id", Config.getLoginInfo().id);
        params.put("content", content);
        OkGo.<LzyResponse>post(ADD_VIDEO)
                .params(params)
                .execute(new JsonCallback<LzyResponse>() {
                    @Override
                    public void onSuccess(Response<LzyResponse> response) {
                        httpRequest.nofityUpdateUi(ADD_VIDEO_CODE, response.body(), null);
                    }

                    @Override
                    public void onError(Response<LzyResponse> response) {
                        super.onError(response);
                        ToastUtils.longToast("上传失败请重试");
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        dialog.dismiss();
                    }
                });
    }


    //我的下载记录
    public static void getMyDownload(OkHttpRequest httpRequest, int page) {
        HttpParams params = new HttpParams();
        params.put("page", page);
        params.put("uid", Config.getLoginInfo().id);
        params.put("type", 1);//1:视频；2：专辑
        params.put("count", 10);
        params.put("md5",MD5Utils.md5());
        OkGo.<LzyResponse<List<DownloadBean>>>post(GET_USER_DOWNLOAD_RECORDS)
                .tag(httpRequest)
                .params(params)
                .execute(new JsonCallback<LzyResponse<List<DownloadBean>>>() {
                    @Override
                    public void onSuccess(Response<LzyResponse<List<DownloadBean>>> response) {
                        httpRequest.nofityUpdateUi(GET_USER_DOWNLOAD_RECORDS_CODE, response.body(), null);
                    }

                    @Override
                    public void onError(Response<LzyResponse<List<DownloadBean>>> response) {
                        super.onError(response);
                        ToastUtils.longToast("网络异常");
                    }
                });
    }

    //添加用户专辑
    public static void addUserChannel(OkHttpRequest httpRequest, String title, String content, String price, String background_image, String movie_ids) {
        HttpParams params = new HttpParams();
        params.put("title", title);
        params.put("content", content);
        if (StringUtils.isNull(price)) {
            price = "0";
        }
        params.put("price", price);
        params.put("background_image", background_image);
        params.put("movie_ids", movie_ids);
        params.put("uid", Config.getLoginInfo().id);
        params.put("md5",MD5Utils.md5());
        OkGo.<LzyResponse>post(ADD_USER_CHANNEL)
                .tag(httpRequest)
                .params(params)
                .execute(new JsonCallback<LzyResponse>() {
                    @Override
                    public void onSuccess(Response<LzyResponse> response) {
                        httpRequest.nofityUpdateUi(ADD_USER_CHANNEL_CODE, response.body(), null);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        LoadDialogUtils.dissmiss();
                    }

                    @Override
                    public void onError(Response<LzyResponse> response) {
                        super.onError(response);
                        ToastUtils.longToast("添加失败");
                    }
                });
    }

    //我的专辑
    public static void getUserChannel(OkHttpRequest httpRequest, String uid) {
        HttpParams params = new HttpParams();
        params.put("uid", uid);
        params.put("md5",MD5Utils.md5());
        OkGo.<LzyResponse<MyAlbumBean>>post(GET_USER_CHANNEL)
                .tag(httpRequest)
                .params(params)
                .execute(new JsonCallback<LzyResponse<MyAlbumBean>>() {
                    @Override
                    public void onSuccess(Response<LzyResponse<MyAlbumBean>> response) {
                        httpRequest.nofityUpdateUi(GET_USER_CHANNEL_CODE, response.body(), null);
                    }

                    @Override
                    public void onError(Response<LzyResponse<MyAlbumBean>> response) {
                        super.onError(response);
                        ToastUtils.longToast("网络异常");
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        LoadDialogUtils.dissmiss();
                    }
                });
    }

    //获取专辑内的视频
    public static void getChannelMovies(OkHttpRequest httpRequest, String channel_id) {
        OkGo.<LzyResponse<List<MyVideoBean>>>post(GET_CHANNEL_MOVIES)
                .tag(httpRequest)
                .params("channel_id", channel_id)
                .params("md5",MD5Utils.md5())
                .execute(new JsonCallback<LzyResponse<List<MyVideoBean>>>() {
                    @Override
                    public void onSuccess(Response<LzyResponse<List<MyVideoBean>>> response) {
                        httpRequest.nofityUpdateUi(GET_CHANNEL_MOVIES_CODE, response.body(), null);
                    }

                    @Override
                    public void onError(Response<LzyResponse<List<MyVideoBean>>> response) {
                        super.onError(response);
                        ToastUtils.longToast("网络异常");
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        LoadDialogUtils.dissmiss();
                    }
                });
    }

    //查询用户是否设置了私密视频的密码
    public static void checkIsSecret(OkHttpRequest httpRequest) {
        OkGo.<LzyResponse>post(CHECK_IS_SECRET)
                .tag(httpRequest)
                .params("uid", Config.getLoginInfo().id)
                .params("md5",MD5Utils.md5())
                .execute(new JsonCallback<LzyResponse>() {
                    @Override
                    public void onSuccess(Response<LzyResponse> response) {
                        httpRequest.nofityUpdateUi(CHECK_IS_SECRET_CODE, response.body(), null);
                    }
                });
    }

    //重置私密视频的密码
    public static void setSecretPwd(OkHttpRequest httpRequest, String phone, String code, String secret) {
        HttpParams params = new HttpParams();
        params.put("uid", Config.getLoginInfo().id);
        params.put("phone", phone);
        params.put("code", code);
        params.put("secret", secret);
        params.put("md5",MD5Utils.md5());
        OkGo.<LzyResponse<List<StateBean>>>post(SET_SECRET)
                .tag(httpRequest)
                .params(params)
                .execute(new JsonCallback<LzyResponse<List<StateBean>>>() {
                    @Override
                    public void onSuccess(Response<LzyResponse<List<StateBean>>> response) {
                        httpRequest.nofityUpdateUi(SET_SECRET_CODE, response.body(), null);
                    }

                    @Override
                    public void onError(Response<LzyResponse<List<StateBean>>> response) {
                        super.onError(response);
                        Log.e("OkGo:", response.getException().getMessage());
                        ToastUtils.longToast("设置失败");
                    }
                });
    }

    //验证私密视频密码
    public static void validateSecret(OkHttpRequest httpRequest, String secret) {
        HttpParams params = new HttpParams();
        params.put("uid", Config.getLoginInfo().id);
        params.put("secret", secret);
        params.put("md5",MD5Utils.md5());
        OkGo.<LzyResponse<List<StateBean>>>post(VALIDATE_SECRET)
                .tag(httpRequest)
                .params(params)
                .execute(new JsonCallback<LzyResponse<List<StateBean>>>() {
                    @Override
                    public void onSuccess(Response<LzyResponse<List<StateBean>>> response) {
                        httpRequest.nofityUpdateUi(VALIDATE_SECRET_CODE, response.body(), null);
                    }

                    @Override
                    public void onError(Response<LzyResponse<List<StateBean>>> response) {
                        super.onError(response);
                        ToastUtils.longToast("密码错误");
                    }
                });
    }

    //获取用户的关注列表
    public static void getAttentionList(OkHttpRequest httpRequest, String uid) {
        OkGo.<LzyResponse<List<MyFollowVideoBean>>>post(GET_MY_ATTENTION)
                .tag(httpRequest)
                .params("uid", uid)
                .params("md5",MD5Utils.md5())
                .execute(new JsonCallback<LzyResponse<List<MyFollowVideoBean>>>() {
                    @Override
                    public void onSuccess(Response<LzyResponse<List<MyFollowVideoBean>>> response) {
                        httpRequest.nofityUpdateUi(GET_MY_ATTENTION_CODE, response.body(), null);
                    }

                    @Override
                    public void onError(Response<LzyResponse<List<MyFollowVideoBean>>> response) {
                        super.onError(response);
                        ToastUtils.longToast("网络异常");
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        LoadDialogUtils.dissmiss();
                    }
                });
    }

    //获取用户的粉丝列表
    public static void getMyFans(OkHttpRequest httpRequest, String uid) {
        OkGo.<LzyResponse<List<MyFollowVideoBean>>>post(GET_MY_FANS)
                .tag(httpRequest)
                .params("uid", uid)
                .params("md5",MD5Utils.md5())
                .execute(new JsonCallback<LzyResponse<List<MyFollowVideoBean>>>() {
                    @Override
                    public void onSuccess(Response<LzyResponse<List<MyFollowVideoBean>>> response) {
                        httpRequest.nofityUpdateUi(GET_MY_FANS_CODE, response.body(), null);
                    }

                    @Override
                    public void onError(Response<LzyResponse<List<MyFollowVideoBean>>> response) {
                        super.onError(response);
                        ToastUtils.longToast("网络异常");
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        LoadDialogUtils.dissmiss();
                    }
                });
    }

    //获取用户收益
    public static void getUserAllIncome(OkHttpRequest httpRequest) {
        OkGo.<LzyResponse<ProfitBean>>post(GET_USER_ALL_INCOME)
                .tag(httpRequest)
                .params("uid", Config.getLoginInfo().id)
                .params("md5",MD5Utils.md5())
                .execute(new JsonCallback<LzyResponse<ProfitBean>>() {
                    @Override
                    public void onSuccess(Response<LzyResponse<ProfitBean>> response) {
                        httpRequest.nofityUpdateUi(GET_USER_ALL_INCOME_CODE, response.body(), null);
                    }

                    @Override
                    public void onError(Response<LzyResponse<ProfitBean>> response) {
                        super.onError(response);
                        ToastUtils.longToast("网络异常");
                    }
                });
    }

    //验证用户是否设置了提现密码
    public static void checkIsCashSecret(OkHttpRequest httpRequest) {
        OkGo.<LzyResponse<List<StateBean>>>post(CHECK_IS_CASH_SECRET)
                .tag(httpRequest)
                .params("uid", Config.getLoginInfo().id)
                .params("md5",MD5Utils.md5())
                .execute(new JsonCallback<LzyResponse<List<StateBean>>>() {
                    @Override
                    public void onSuccess(Response<LzyResponse<List<StateBean>>> response) {
                        httpRequest.nofityUpdateUi(CHECK_IS_CASH_SECRET_CODE, response.body(), null);
                    }
                });
    }

    //用户是否绑定了提现账户
    public static void checkAliName(OkHttpRequest httpRequest) {
        OkGo.<LzyResponse<List<StateBean>>>post(CHECK_ALI_NAME)
                .tag(httpRequest)
                .params("uid", Config.getLoginInfo().id)
                .params("md5",MD5Utils.md5())
                .execute(new JsonCallback<LzyResponse<List<StateBean>>>() {
                    @Override
                    public void onSuccess(Response<LzyResponse<List<StateBean>>> response) {
                        httpRequest.nofityUpdateUi(CHECK_ALI_NAME_CODE, response.body(), null);
                    }
                });
    }

    //设置用户提现账户
    public static void setUserAliName(OkHttpRequest httpRequest, String ali_name) {
        OkGo.<LzyResponse<List<StateBean>>>post(SET_USER_ALI_NAME)
                .tag(httpRequest)
                .params("uid", Config.getLoginInfo().id)
                .params("ali_name", ali_name)
                .params("md5",MD5Utils.md5())
                .execute(new JsonCallback<LzyResponse<List<StateBean>>>() {
                    @Override
                    public void onSuccess(Response<LzyResponse<List<StateBean>>> response) {
                        httpRequest.nofityUpdateUi(SET_USER_ALI_NAME_CODE, response.body(), null);
                    }

                    @Override
                    public void onError(Response<LzyResponse<List<StateBean>>> response) {
                        super.onError(response);
                        ToastUtils.longToast("绑定失败");
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        LoadDialogUtils.dissmiss();
                    }
                });
    }

    //提现
    public static void withdraw(OkHttpRequest httpRequest, String money) {
        OkGo.<LzyResponse<List<StateBean>>>post(WITHDRAW)
                .tag(httpRequest)
                .params("uid", Config.getLoginInfo().id)
                .params("money", money)
                .params("md5",MD5Utils.md5())
                .execute(new JsonCallback<LzyResponse<List<StateBean>>>() {
                    @Override
                    public void onSuccess(Response<LzyResponse<List<StateBean>>> response) {
                        httpRequest.nofityUpdateUi(WITHDRAW_CODE, response.body(), null);
                    }

                    @Override
                    public void onError(Response<LzyResponse<List<StateBean>>> response) {
                        super.onError(response);
                        ToastUtils.longToast("提现失败");
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        LoadDialogUtils.dissmiss();
                    }
                });
    }

    //用户余额
    public static void getUserAccount(OkHttpRequest httpRequest) {
        OkGo.<LzyResponse<AccountBean>>post(USER_ACCOUNT)
                .tag(httpRequest)
                .params("uid", Config.getLoginInfo().id)
                .params("md5",MD5Utils.md5())
                .execute(new JsonCallback<LzyResponse<AccountBean>>() {
                    @Override
                    public void onSuccess(Response<LzyResponse<AccountBean>> response) {
                        httpRequest.nofityUpdateUi(USER_ACCOUNT_CODE, response.body(), null);
                    }

                    @Override
                    public void onError(Response<LzyResponse<AccountBean>> response) {
                        super.onError(response);
//                        ToastUtils.longToast("提现失败");
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
//                        LoadDialogUtils.dissmiss();
                    }
                });
    }


    /**
     * 保存或者修改广告
     *
     * @param id                  新增不传，修改传
     * @param upload_way          上传方式1：http;2:ppvod,3:oss
     * @param title               标题
     * @param short_title         短语
     * @param package_name        包名
     * @param android_package_url 安卓下载外链地址
     * @param ios_package_url     ios下载外链地址
     * @param qq                  联系qq
     * @param phone               联系电话
     * @param video_url           上传的视频地址，oss或ppvod
     * @param img_url             图片地址，oss或ppvod
     * @param html                图文广告html
     * @param outer_url           广告外链地址
     * @param introduction_url    介绍地址
     * @param qualification_url   资质介绍地址
     */
    public static void saveAd(OkHttpRequest httpRequest, String id, int upload_way, String title, String short_title, String package_name, String android_package_url, String ios_package_url
            , String qq, String phone, String video_url, String img_url, String html, String outer_url, String introduction_url, String qualification_url) {
        HttpParams params = new HttpParams();
        if (!TextUtils.isEmpty(id)) {
            params.put("id", id);
        }
        params.put("md5",MD5Utils.md5());
        params.put("upload_way", upload_way);
        params.put("title", title);
        params.put("short_title", short_title);
        params.put("package_name", package_name);
        params.put("android_package_url", android_package_url);
        params.put("ios_package_url", ios_package_url);
        params.put("qq", qq);
        params.put("phone", phone);
        params.put("video_url", video_url);
        params.put("img_url", img_url);
        params.put("html", html);
        params.put("outer_url", outer_url);
        params.put("introduction_url", introduction_url);
        params.put("qualification_url", qualification_url);
        params.put("md5",MD5Utils.md5());
        OkGo.<LzyResponse<StateBean>>post(SAVE_AD)
                .tag(httpRequest)
                .params(params)
                .params("uid", Config.getLoginInfo().id)
                .execute(new JsonCallback<LzyResponse<StateBean>>() {
                    @Override
                    public void onSuccess(Response<LzyResponse<StateBean>> response) {
                        httpRequest.nofityUpdateUi(SAVE_AD_CODE, response.body(), null);
                    }

                    @Override
                    public void onError(Response<LzyResponse<StateBean>> response) {
                        super.onError(response);
                        ToastUtils.longToast("添加失败");
                    }
                });

    }

    //获取数字专辑
    public static void getTeachingChannel(OkHttpRequest httpRequest) {
        OkGo.<LzyResponse<AlumBean>>post(TEACHING_CHANNEL)
                .tag(httpRequest)
                .params("md5",MD5Utils.md5())
                .execute(new JsonCallback<LzyResponse<AlumBean>>() {
                    @Override
                    public void onSuccess(Response<LzyResponse<AlumBean>> response) {
                        httpRequest.nofityUpdateUi(TEACHING_CHANNEL_CODE, response.body(), null);
                    }

                    @Override
                    public void onError(Response<LzyResponse<AlumBean>> response) {
                        super.onError(response);
                        ToastUtils.longToast("网络异常");
                    }
                });
    }

    // 获取专辑列表
    public static void getChannelList(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynGet(Config.UAction()
                .setUrl(CHANNEL_LIST)
                .setRequestCode(CHANNEL_LIST_CODE)
                .setTypeToken(Channels.class));
    }

    // 设置广告投放方式 1
    public static void setAdPutType1(NetRequestHandler netRequestHandler, String begin_time, String end_time, String money, String id) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(AD_PUT)
                .put("mode_type", 1)
                .put("begin_time", begin_time)
                .put("end_time", end_time)
                .put("money", money)
                .put("id", id)
                .put("md5",MD5Utils.md5())
                .put("user_id", Config.getLoginInfo().id)
                .setRequestCode(AD_PUT_CODE)
                .setTypeToken(new TypeToken<List<Object>>() {
                }));
    }

    // 设置广告投放方式 2
    public static void setAdPutType2(NetRequestHandler netRequestHandler, String begin_date, String end_date, String begin_time, String end_time, String money, String id) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(AD_PUT)
                .put("mode_type", 2)
                .put("begin_date", begin_date)
                .put("end_date", end_date)
                .put("begin_time", begin_time)
                .put("end_time", end_time)
                .put("money", money)
                .put("id", id)
                .put("md5",MD5Utils.md5())
                .put("user_id", Config.getLoginInfo().id)
                .setRequestCode(AD_PUT_CODE)
                .setTypeToken(new TypeToken<List<Object>>() {
                }));
    }

    // 设置广告投放方式 3
    public static void setAdPutType3(NetRequestHandler netRequestHandler, String month, String begin_time, String id) {
        int moneyCount = Integer.parseInt(month) * 1500;
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(AD_PUT)
                .put("mode_type", 3)
                .put("begin_time", begin_time)
                .put("month", month)
                .put("money", moneyCount)
                .put("id", id)
                .put("user_id", Config.getLoginInfo().id)
                .put("md5",MD5Utils.md5())
                .setRequestCode(AD_PUT_CODE)
                .setTypeToken(new TypeToken<List<Object>>() {
                }));
    }

    // 获取广告投放记录
    public static void getPutAdHistories(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(AD_HISTORIES)
                .put("user_id", Config.getLoginInfo().id)
                .put("md5",MD5Utils.md5())
                .setRequestCode(AD_HISTORIES_CODE)
                .setTypeToken(new TypeToken<List<AdHistory>>() {
                }));
    }

    // 获取系统参数
    public static void getSystemParam(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(SYSTEM_PARAM)
                .put("user_id", Config.getLoginInfo().id)
                .put("md5",MD5Utils.md5())
                .setRequestCode(SYSTEM_PARAM_CODE)
                .setTypeToken(new TypeToken<List<SystemParams>>() {
                }));
    }

    // 获取投诉类型
    public static void getFeedBacks(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(FEED_BACK)
                .put("user_id", Config.getLoginInfo().id)
                .setRequestCode(FEED_BACK_CODE)
                .put("md5",MD5Utils.md5())
                .setTypeToken(new TypeToken<List<MovieFeedBack>>() {
                }));
    }

    // 提交投诉类型
    public static void submitFeedBacks(NetRequestHandler netRequestHandler, MovieFeedbackRequest request) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(SUBMIT_FEED_BACK)
                .put("user_id", Config.getLoginInfo().id)
                .put("cate_id", request.cate_id)
                .put("content", request.content)
                .put("movie_id", request.movie_id)
                .put("md5",MD5Utils.md5())
                .setRequestCode(SUBMIT_FEED_BACK_CODE)
                .setTypeToken(new TypeToken<List<Object>>() {
                }));
    }

    // 提交投诉类型
    public static void getPlayAds(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(PLAY_AD)
                .put("user_id", Config.getLoginInfo().id)
                .put("md5",MD5Utils.md5())
                .setRequestCode(PLAY_AD_CODE)
                .setTypeToken(Ad.class));
    }

    // 提交投诉类型
    public static void getUserTalentInfo(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(USER_TALENT_INFO)
                .put("uid", Config.getLoginInfo().id)
                .put("md5",MD5Utils.md5())
                .setRequestCode(USER_TALENT_INFO_CODE)
                .setTypeToken(UserAuthInfo.class));
    }

    // 提交用户认证
    public static void submitUserAuth(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(APPLY_TALENT)
                .put("uid", Config.getLoginInfo().id)
                .put("md5",MD5Utils.md5())
                .setRequestCode(APPLY_TALENT_CODE)
                .setTypeToken(Object.class));
    }

    // 提交投诉类型
    public static void updateStatistics(NetRequestHandler netRequestHandler, String adId, String type) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(UPDATE_STATISTICS)
                .put("uid", Config.getLoginInfo().id)
                .put("ad_id", adId)
                .put("type", type)
                .put("md5",MD5Utils.md5())
                .setRequestCode(UPDATE_STATISTICS_CODE)
                .setTypeToken(Object.class));
    }

    // 获取广告充值信息
    public static void getAdPayHistories(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(AD_RECHARGE_RECORDS)
                .put("uid", Config.getLoginInfo().id)
                .put("md5",MD5Utils.md5())
                .setRequestCode(AD_RECHARGE_RECORDS_CODE)
                .setTypeToken(new TypeToken<List<AdPayHistory>>() {
                }));
    }

    // 实名认证
    public static void userAuth(String name, String idcard, String phone, String code, NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(USER_AUTH)
                .put("uid", Config.getLoginInfo().id)
                .put("idcard", idcard)
                .put("name", name)
                .put("phone", phone)
                .put("code", code)
                .put("md5",MD5Utils.md5())
                .setRequestCode(USER_AUTH_CODE)
                .setTypeToken(new TypeToken<List<Object>>() {
                }));
    }

    //获取粉丝和关注用户数
    public static void getMineFans(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynPost(new NAction()
                .setUrl(MINE_FANS)
                .put("md5",MD5Utils.md5())
                .put("user_id", Config.getLoginInfo().id)
                .setRequestCode(MINE_FANS_CODE)
                .setTypeToken(FollowsBean.class));
    }

    //获取用户今日数据
    public static void getMineTodayData(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynPost(new NAction()
                .setUrl(MINE_TODAY_DATA)
                .put("md5",MD5Utils.md5())
                .put("user_id", Config.getLoginInfo().id)
                .setRequestCode(MINE_TODAY_DATA_CODE)
                .setTypeToken(TodayDataBean.class)
        );
    }

    //查询会员购买记录
    public static void getMineMemberRecord(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynGet(new NAction()
                .setUrl(MINE_MEMBER_RECORD)
                .put("uid", Config.getLoginInfo().uid)
                .put("md5",MD5Utils.md5())
                .setRequestCode(MINE_MEMBER_RECORD_CODE)
                .setTypeToken(new TypeToken<List<MemberRecordBean>>() {
                }));
    }

    //VIP套餐获取
    public static void getVipSetMeal(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynPost(new NAction()
                .setUrl(VIP_SET_MEAL)
                .setRequestCode(VIP_SET_MEAL_CODE)
                .setTypeToken(new TypeToken<List<RechargeBeanV3>>() {
                }));
    }

    //舞币获取
    public static void getWbSetMeal(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynPost(new NAction()
                .setUrl(WB_SET_MEAL)
                .setRequestCode(WB_SET_MEAL_CODE)
                .setTypeToken(new TypeToken<List<GoldMealBean>>() {
                }));
    }

    /**
     * 获取用户时间余额信息
     *
     * @param netRequestHandler
     */
    public static void getUserTime(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(GET_USER_TIME)
                .put("uid", Config.getLoginInfo().uid)
                .put("md5",MD5Utils.md5())
                .setRequestCode(GET_USER_TIME_CODE)
                .setTypeToken(UserTimeBean.class)
        );
    }

    /**
     * 获取用户时间列表
     *
     * @param type              1本人列表 2金粉列表
     * @param limit             页数
     * @param netRequestHandler
     */
    public static void getUserTimeList(String type, int limit, NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(GET_USER_TIME_LIST)
                .put("uid", Config.getLoginInfo().uid)
                .put("type", type)
                .put("limit", String.valueOf(limit))
                .put("md5",MD5Utils.md5())
                .setRequestCode(GET_USER_TIME_LIST_CODE)
                .setTypeToken(new TypeToken<List<TimeListBean>>() {
                })
        );
    }

    /**
     * 获取关注用户列表
     *
     * @param type              0铁粉列表 1金粉列表
     * @param page              页数
     * @param netRequestHandler
     */
    public static void getFansList(String type, int page, NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(GET_FANS_LIST)
                .put("user_id", Config.getLoginInfo().uid)
                .put("type", type)
                .put("page", String.valueOf(page))
                .put("md5",MD5Utils.md5())
                .setRequestCode(GET_FANS_LIST_CODE)
                .setTypeToken(new TypeToken<List<FansListBean>>() {
                })
        );
    }

    /**
     * 获取用户粉丝数
     *
     * @param netRequestHandler
     */
    public static void getFansNum(NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(GET_FANS_NUM)
                .put("user_id", Config.getLoginInfo().uid)
                .put("md5",MD5Utils.md5())
                .setRequestCode(GET_FANS_NUM_CODE)
                .setTypeToken(FansNumBean.class)
        );
    }

    /**
     * 获取关注用户列表
     *
     * @param page              页数
     * @param netRequestHandler
     */
    public static void getMsgNoticeList(int page, NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynPost(Config.UAction()
                        .setUrl(GET_MSG_NOTICE_LIST)
                        .put("uid", Config.getLoginInfo().uid)
                        .put("md5",MD5Utils.md5())
//                .put("page", String.valueOf(page))
                        .setRequestCode(GET_MSG_NOTICE_LIST_CODE)
                        .setTypeToken(new TypeToken<List<MessageNoticeBean>>() {
                        })
        );
    }


    /**
     * 播放接口
     */
    public static void getMoviePlayDetailV3(String movieId, NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynPost(Config.UAction()
                .setUrl(GET_MOVIE_PLAY_DETAIL_V3)
                .put("movie_id", movieId)
                .put("md5",MD5Utils.md5())
                .setRequestCode(GET_MOVIE_PLAY_DETAIL_CODE_V3)
                .setTypeToken(MovieDetailV3Bean.class));
    }



    /**
     * 当期分类按照用户依次获取视频
     *
     */
    public static void getMovieByCateV3(int cate_id,int num, NetRequestHandler netRequestHandler) {
        netRequestHandler.requestAsynGet(Config.UAction()
                .setUrl(GET_NEW_MOVIE_BY_CATE)
                .put("md5", MD5Utils.md5())
                .put("cate_id", cate_id)
                .put("num", num)
                .setRequestCode(GET_NEW_MOVIE_BY_CATE_CODE)
                .setTypeToken(new TypeToken<List<MovieByCateV3Bean>>(){

                })
        );
    }


}
