package cn.wu1588.dancer.common.model;

/**
 * Created by：Z_B on 2018/3/30 16:21
 * Effect： 版本更新
 */
public class ApkVersion {

    /**
     * version : home1.0.home1
     * description : 有新版本等着你更新哦！
     * path : http://ashduhancbhasghufd.apk
     */
    public String version;
    public String description;
    public String url;
    public String content;
}
