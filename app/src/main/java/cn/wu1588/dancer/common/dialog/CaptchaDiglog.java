//package common.dialog;
//
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.view.View;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import cn.wu1588.dancer.R;
//import com.bumptech.glide.Glide;
//import com.bumptech.glide.load.engine.DiskCacheStrategy;
//import com.sunrun.sunrunframwork.bean.BaseBean;
//import com.sunrun.sunrunframwork.uiutils.UIUtils;
//import com.sunrun.sunrunframwork.utils.EmptyDeal;
//
//import butterknife.BindView;
//import butterknife.OnClick;
//import common.base.LBaseDialogFragment;
//
//
///**
// * Created by wangchao on 2018-11-22.
// * 验证码图片
// */
//public class CaptchaDiglog extends LBaseDialogFragment {
//    @BindView(R.id.tv_title)
//    TextView tvTitle;
//    @BindView(R.id.iv_num)
//    ImageView ivNum;
//    @BindView(R.id.ed_content)
//    EditText edContent;
//    @BindView(R.id.submit)
//    TextView submit;
//    @BindView(R.id.cancel)
//    TextView cancel;
//    @BindView(R.id.out)
//    LinearLayout out;
//    OnConfirmClickListenr onConfirmClickListenr;
//    private String mobileNum;
//    private String imgData;
//    private String num;
//
//    public CaptchaDiglog setOnConfirmClickListenr(OnConfirmClickListenr onConfirmClickListenr) {
//        this.onConfirmClickListenr = onConfirmClickListenr;
//        return this;
//    }
//
//    @Override
//    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//        setImgView();
//    }
//
//    private void setImgView() {
//        Glide.with(that)
//                .load(imgData)
//                .centerCrop()
//                .diskCacheStrategy(DiskCacheStrategy.NONE)
//                .skipMemoryCache( true )
////                .diskCacheStrategy(DiskCacheStrategy.ALL)
////                .error(R.drawable.mine_icon_avatar_nor)
//                .dontAnimate()
//                .into(ivNum);
//
//    }
//
//    @Override
//    protected int getLayoutRes() {
//        return R.layout.layout_dialog_pic_confirm;
//    }
//
//    public static CaptchaDiglog newInstance() {
//        Bundle args = new Bundle();
//        CaptchaDiglog fragment = new CaptchaDiglog();
//        fragment.setArguments(args);
//        return fragment;
//    }
//
//    public CaptchaDiglog setMobile(String mobile) {
//        this.mobileNum = mobile;
//        return this;
//    }
//
//    public CaptchaDiglog setImgData(String imgData) {
//        this.imgData = imgData;
//        return this;
//    }
//
//    @OnClick({R.id.iv_num, R.id.submit, R.id.cancel})
//    public void onViewClicked(View view) {
//        switch (view.getId()) {
//            case R.id.iv_num:
//                UIUtils.showLoadDialog(that);
//                BaseQuestStart.getRegisteredPicVerificationCode(CaptchaDiglog.this, mobileNum, "120", "30");
//                break;
//            case R.id.submit:
//                num = edContent.getText().toString().trim();
//                if (!EmptyDeal.isEmpy(num)) {
//                    BaseQuestStart.checkRegVerify(CaptchaDiglog.this, mobileNum, num);
//                } else {
//                    UIUtils.shortM("请输入图片验证码");
//                }
//
//                break;
//            case R.id.cancel:
//                dismissAllowingStateLoss();
//                break;
//        }
//    }
//
//    @Override
//    public void nofityUpdate(int requestCode, BaseBean bean) {
//        super.nofityUpdate(requestCode, bean);
//        switch (requestCode){
//            case QUEST_GET_REGISTERED_PIC_VERIFICATION_CODE://图形验证码
//                if (bean.status==home1){
//                    if (!EmptyDeal.isEmpy(bean.Data().toString())){
//                        imgData=bean.Data().toString();
//                        setImgView();
//                    }
//                }else{
//                    UIUtils.shortM(bean.msg);
//                }
//                break;
//            case CHECK_REG_VERIFY_CODE://检查图像验证码
//                if (bean.status == home1) {
//                    onConfirmClickListenr.onConfirm(num);
//                    dismissAllowingStateLoss();
//                } else {
//                    UIUtils.shortM(bean.msg);
//                }
//                break;
//        }
//    }
//
//
//
//
//    public interface OnConfirmClickListenr {
//        void onConfirm(String num);
//    }
//}
