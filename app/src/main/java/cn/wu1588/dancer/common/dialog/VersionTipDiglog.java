package cn.wu1588.dancer.common.dialog;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import cn.wu1588.dancer.R;

import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.sunrun.sunrunframwork.utils.EmptyDeal;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.base.LBaseDialogFragment;
import cn.wu1588.dancer.common.service.DownLoadService;
import cn.wu1588.dancer.home.mode.VersionBean;

/**
 * Created by wangchao on 2018-10-09.
 * 版本提示框
 */
public class VersionTipDiglog extends LBaseDialogFragment {
    View.OnClickListener onSubmitAction;
    DialogInterface.OnDismissListener onDismissListener;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_msg)
    TextView tvMsg;
    @BindView(R.id.verson)
    TextView verson;
    @BindView(R.id.btnSure)
    Button btnSure;
    @BindView(R.id.btnCancel)
    Button btnCancel;
    private VersionBean versionBean;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!EmptyDeal.isEmpy(versionBean)) {
            tvMsg.setText(versionBean.description);
            verson.setText("V"+versionBean.version);
            if (versionBean.isForcedToUpdate()) {
                btnCancel.setVisibility(View.GONE);
            }
        }

        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
                    return true;
                } else {
                    return false;
                }
            }
        });
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.dialog_version_tip;
    }


    public static VersionTipDiglog newInstance() {
        Bundle args = new Bundle();
        VersionTipDiglog fragment = new VersionTipDiglog();
        fragment.setArguments(args);
        return fragment;
    }

    public VersionTipDiglog setOnSubmitAction(View.OnClickListener onSubmitAction) {
        this.onSubmitAction = onSubmitAction;
        return this;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (onDismissListener != null) {
            onDismissListener.onDismiss(dialog);
        }

    }


    @OnClick({R.id.btnSure, R.id.btnCancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnSure:
                // 下载APK并替换安装
                if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                    Intent service = new Intent(that, DownLoadService.class);
                    service.putExtra(DownLoadService.DOWNLOAD_URL, versionBean.url);
                    that.startService(service);
                    UIUtils.shortM("开始下载更新");
                } else {
                    Toast.makeText(that, "未检测到SD卡，请插入SD卡再运行",
                            Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btnCancel:
                if (onSubmitAction != null) {
                    onSubmitAction.onClick(view);
                }
                dismissAllowingStateLoss();
                break;
        }
    }

    public VersionTipDiglog setVersionData(VersionBean versionBean) {
        this.versionBean = versionBean;
        return this;
    }


}
