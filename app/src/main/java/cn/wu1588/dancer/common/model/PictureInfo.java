package cn.wu1588.dancer.common.model;

public class PictureInfo {
    private String url;
    private int time;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }
}
