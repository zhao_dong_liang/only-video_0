//package common.dialog;
//
//import android.graphics.Bitmap;
//import android.os.Bundle;
//import android.view.View;
//
//import com.cnsunrun.R;
//import com.cnsunrun.common.base.LBaseDialogFragment;
//import com.cnsunrun.commonui.widget.roundwidget.QMUIRoundButton;
//import com.sunrun.sunrunframwork.uiutils.UIUtils;
//import com.williamww.silkysignature.views.SignaturePad;
//
//import butterknife.BindView;
//import butterknife.OnClick;
//
///**
// * Created by wangchao on 2019-03-04.
// */
//public class SignDialogFragment extends LBaseDialogFragment {
//    @BindView(R.id.signature_pad)
//    SignaturePad mSignaturePad;
//    @BindView(R.id.btnSave)
//    QMUIRoundButton btnSave;
//    @BindView(R.id.btnClear)
//    QMUIRoundButton btnClear;
//    private OnSaveClickListener onSaveClickListener;
//
//    @Override
//    protected int getLayoutRes() {
//        return R.layout.fragment_sign_dialog;
//    }
//
//    public static SignDialogFragment newInstance() {
//        Bundle args = new Bundle();
//        SignDialogFragment fragment = new SignDialogFragment();
//        fragment.setArguments(args);
//        return fragment;
//    }
//
//    @OnClick({R.id.btnSave, R.id.btnClear})
//    public void onViewClicked(View view) {
//        switch (view.getId()) {
//            case R.id.btnSave:
//                Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
//                if (signatureBitmap!=null){
//                    onSaveClickListener.save(view,signatureBitmap);
//                    dismissAllowingStateLoss();
//                }else{
//                    UIUtils.shortM("请签名");
//                }
//                break;
//            case R.id.btnClear:
//                mSignaturePad.clear();
//                break;
//        }
//    }
//
//    //    public boolean addJpgSignatureToGallery(Bitmap signature) {
////        boolean result = false;
////        try {
////            File photo = new File(getAlbumStorageDir("SignaturePad"), String.format("Signature_%d.jpg", System.currentTimeMillis()));
////            saveBitmapToJPG(signature, photo);
////            result = true;
////        } catch (IOException e) {
////            e.printStackTrace();
////        }
////        return result;
////    }
////    public File getAlbumStorageDir(String albumName) {
////        // Get the directory for the user's public pictures directory.
////        File file = new File(Environment.getExternalStoragePublicDirectory(
////                Environment.DIRECTORY_PICTURES), albumName);
////        if (!file.mkdirs()) {
////            Log.e("SignaturePad", "Directory not created");
////        }
////        return file;
////    }
////    public void saveBitmapToJPG(Bitmap bitmap, File photo) throws IOException {
////        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
////        Canvas canvas = new Canvas(newBitmap);
////        canvas.drawColor(Color.WHITE);
////        canvas.drawBitmap(bitmap, 0, 0, null);
////        OutputStream stream = new FileOutputStream(photo);
////        newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
////        stream.close();
////    }
//    public interface OnSaveClickListener {
//
//        void save(View view, Bitmap bitmap);
//
//    }
//    public SignDialogFragment setOnSaveClickListener(OnSaveClickListener onSaveClickListener) {
//        this.onSaveClickListener = onSaveClickListener;
//        return this;
//    }
//}
