package cn.wu1588.dancer.common.util.ppvod;


import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;
import okio.BufferedSink;
import okio.ForwardingSink;
import okio.Okio;
import okio.Sink;
import okio.Source;

import static com.aliyun.vod.common.utils.IOUtils.toByteArray;

public class HttpUtil {
    private static final String TAG = "httputil";
    private OkHttpClient client;

    public HttpUtil() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.writeTimeout(120, TimeUnit.SECONDS);
        builder.readTimeout(10, TimeUnit.SECONDS);
        builder.connectTimeout(10, TimeUnit.SECONDS);
        this.client = builder.build();
    }

    public interface ProgressListener {
        // 已发送
        void writed(long bytes);
    }

    protected class CountingSink extends ForwardingSink {

        private long bytesWritten = 0;
        private ProgressListener listener;

        public CountingSink(Sink delegate, ProgressListener progressListener) {
            super(delegate);
            this.listener = progressListener;
        }

        @Override
        public void write(Buffer source, long byteCount) throws IOException {
            super.write(source, byteCount);

            bytesWritten += byteCount;
            listener.writed(byteCount);
        }
    }

    public String request(String surl) throws IOException {
        URL url = new URL(surl);
        String result = "";
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            byte[] data = new byte[1024];
            int r = in.read(data);
            result = new String(data, 0, r);
        } finally {
            urlConnection.disconnect();
        }

        return result;
    }

    public JSONObject requestForJSON(String url) throws IOException {
        String r = request(url);
        if (r == null)
            return null;
        JSONTokener tokener = new JSONTokener(r);
        JSONObject result = null;
        try {
            result = new JSONObject(tokener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;

    }

    public String postInputStream(final InputStream in, final long length, String url, JSONObject fields,
                                  String filename, final ProgressListener progressListener) throws IOException {
        System.out.println("上传数据" + length + ":" + "" + url + ":" + filename);
        RequestBody mbody = new RequestBody() {
            @Override
            public MediaType contentType() {
                return null;
            }

            @Override
            public long contentLength() {
                return length;
            }

            @Override
            public void writeTo(BufferedSink sink) throws IOException {
                Source source = null;
                try {
                    source = Okio.source(in);
                    if (progressListener == null) {

                        sink.write(source, length);
                    } else {

                        CountingSink countingSink = new CountingSink(sink, progressListener);
                        BufferedSink bufferedSink = Okio.buffer(countingSink);

                        bufferedSink.write(source, length);
                        bufferedSink.flush();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (source != null) {
                        try {
                            source.close();
                        } catch (RuntimeException rethrown) {
                            throw rethrown;
                        } catch (Exception ignored) {
                        }
                    }

                }
            }
        };


        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MediaType.parse("multipart/form-data"));

        Iterator<String> itera = fields.keys();
        while (itera.hasNext()) {
            String key = itera.next();
            try {
                System.out.println(key +  fields.getString(key));
                builder.addFormDataPart(key, fields.getString(key));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        builder.addFormDataPart("filename", filename, mbody);

        RequestBody body = builder.build();
        Request request = new Request.Builder().url(url).post(body).build();
        Response response = this.client.newCall(request).execute();
        String result = response.body().string();
        Log.debug("postInputStream返回结果:" + result);

        return result;
    }

}