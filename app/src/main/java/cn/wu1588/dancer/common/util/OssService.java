package cn.wu1588.dancer.common.util;


import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;

import com.alibaba.sdk.android.vod.upload.VODUploadCallback;
import com.alibaba.sdk.android.vod.upload.VODUploadClient;
import com.alibaba.sdk.android.vod.upload.VODUploadClientImpl;
import com.alibaba.sdk.android.vod.upload.model.UploadFileInfo;
import com.alibaba.sdk.android.vod.upload.model.VodInfo;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.model.Progress;
import com.lzy.okgo.request.base.ProgressRequestBody;
import com.my.toolslib.http.utils.LzyResponse;
import com.my.toolslib.http.utils.OkHttpRequest;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.wu1588.dancer.common.util.ppvod.JobFuture;
import cn.wu1588.dancer.common.util.ppvod.PPVodQuickUploadResult;
import cn.wu1588.dancer.common.util.ppvod.PPVodResult;
import cn.wu1588.dancer.common.util.ppvod.Uploader;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.common.quest.BaseQuestConfig;
import cn.wu1588.dancer.common.quest.NetQuestConfig;
import okhttp3.ResponseBody;

public class OssService implements OkHttpRequest {

    public static final String Tag = "OssService";
    private String imgUrl;
    private Handler handler;

    public OssService() {
//        PutObjectRequest request=null;
//        https://help.aliyun.com/knowledge_detail/39607.html 获取url
//        https://blog.csdn.net/qq_34855745/article/details/82715666
    }

    private String fileName;
    private String title;
    private String dec;
    private String ma;

    public void startUpload(String fileName, String ma, String imgUrl, String title, String dec, Handler handler) {
        this.handler = handler;
        this.fileName = fileName;
        this.imgUrl = imgUrl;
        this.title = title;
        this.dec = dec;
        this.ma = ma;
        start();
    }

    public void upload(final String uploadAddress, final String uploadAuth, String videoId, String imageUri, final String fileName) {
        Log.i(Tag, "成功获取凭证 ------------------" + "uploadAuth==" + uploadAuth + "\n" + uploadAddress);
        Log.i(Tag, "地址fileName ------------------" + fileName);
        // 上传视频
        final VODUploadClient uploader = new VODUploadClientImpl(CommonApp.getApplication());
//        uploader.setRegion(VOD_REGION);
//        uploader.setRecordUploadProgressEnabled(VOD_RECORD_UPLOAD_PROGRESS_ENABLED);
        VODUploadCallback callback = new VODUploadCallback() {

            public void onUploadSucceed(UploadFileInfo info) {
                Log.i(Tag, "onsucceed ------------------" + info.getFilePath());
                Message message = Message.obtain();
                message.what = 2;
                message.obj = new HashMap<String, String>() {
                    {
                        put("videoId", videoId);
                        put("playUrl", NetQuestConfig.HTTP_VIDEO_HOST + info.getObject());
                    }
                };
                handler.sendMessage(message);
//                Toast.makeText(getCurrentActivity(), "视频上传成功", Toast.LENGTH_SHORT).show();
                Log.d("VODUpload", String.format("bucket:%s\nendpoint:%s\nobject:%s", info.getBucket(), info.getEndpoint(), info.getObject()));
            }

            public void onUploadFailed(UploadFileInfo info, String code, String message) {
                Message mess = Message.obtain();
                mess.what = 3;
                handler.sendMessage(mess);
                Log.i(Tag, "onfailed ------------------ " + info.getFilePath() + "code=" + code + "message=" + message);
            }

            public void onUploadProgress(UploadFileInfo info, long uploadedSize, long totalSize) {
                Log.i(Tag, "onProgress ------------------ " + info.getFilePath() + " " + uploadedSize + " " + totalSize);
                Log.i(Tag, "onProgress ------------------ " + uploadedSize * 100 / totalSize);
                int pro = (int) (uploadedSize * 100 / totalSize);
                Message message = Message.obtain();
                message.obj = pro;
                message.what = 1;
                handler.sendMessage(message);
            }

            public void onUploadTokenExpired() {
                Log.i(Tag, "onExpired ------------- ");
                uploader.resumeWithAuth(uploadAuth);
            }

            public void onUploadRetry(String code, String message) {
                Log.i(Tag, "onUploadRetry ------------- ");
            }

            public void onUploadRetryResume() {
                Log.i(Tag, "onUploadRetryResume ------------- ");
            }

            public void onUploadStarted(UploadFileInfo uploadFileInfo) {
                Log.i(Tag, "onUploadStarted ------------- ");
                if (isVodMode(uploadAuth)) {

                    uploader.setUploadAuthAndAddress(uploadFileInfo, uploadAuth, uploadAddress);
                }
                Log.i(Tag, "file path:" + uploadFileInfo.getFilePath() +
                        ", endpoint: " + uploadFileInfo.getEndpoint() +
                        ", bucket:" + uploadFileInfo.getBucket() +
                        ", object:" + uploadFileInfo.getObject() +
                        ", status:" + uploadFileInfo.getStatus());
            }
        };

        //初始化
        uploader.init(callback);
        uploader.setPartSize(1024 * 1024);
        uploader.setTemplateGroupId("xxx");
        uploader.setStorageLocation("xxx");
        //添加文件
        uploader.addFile(fileName, getVodInfo());
        //开始上传
        uploader.start();
    }

    @Override
    public void tokenInvalid() {
    }

    @Override
    public void httpLoadFail(String err) {

    }

    @Override
    public void httpLoadFinal() {

    }

    private boolean isVodMode(String uploadAuth) {
        return (null != uploadAuth && uploadAuth.length() > 0);
    }

    @Override
    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
        if (requestCode == BaseQuestConfig.GET_CREATEUPLOADVIDEO_CODE) {//获取上传凭证
            VoucherBean voucherBean = (VoucherBean) response.data;
            videoId = voucherBean.getVideoId();
            upload(voucherBean.getUploadAddress(), voucherBean.getUploadAuth(), voucherBean.getVideoId(), imgUrl, fileName);
        }
    }

    private String videoId;

    @Override
    public void onProgress(Progress progress) {

    }

    @Override
    public void downLoadFinish(File file) {

    }

    private VodInfo getVodInfo() {
        VodInfo vodInfo = new VodInfo();
        vodInfo.setTitle(title);
        vodInfo.setDesc(dec);
        vodInfo.setCateId(1);
        vodInfo.setCoverUrl(imgUrl);
        vodInfo.setIsProcess(true);
//        vodInfo.setCoverUrl("http://www.taobao.com/" + index + ".jpg");
        List<String> tags = new ArrayList<>();
        tags.add("标签");
        vodInfo.setTags(tags);
//        if (isVodMode()) {
        vodInfo.setIsShowWaterMark(false);
        vodInfo.setPriority(7);
//        } else {
//            vodInfo.setUserData("自定义数据" + index);
//        }
        return vodInfo;
    }

    private void start() {
//        OkGo.<LzyResponse<UploadChannel>>post(BaseQuestConfig.UPLOAD_CHANNEL).execute(new JsonCallback<LzyResponse<UploadChannel>>() {
//            @Override
//            public void onSuccess(Response<LzyResponse<UploadChannel>> response) {
//                LzyResponse<UploadChannel> re = response.body();
//                if (re.code == 1) {
//                    UploadChannel uploadChannel = re.data;
//                    if (uploadChannel.ali != null) {
//                        BaseQuestStart.get_createUploadVideo(OssService.this, fileName, ma);
//                    } else if (uploadChannel.ppvod != null) {
//                        Log.d("OssUpload", "ppvod upload channel");
//                        ppvodUpload(uploadChannel.ppvod.url, uploadChannel.ppvod.yao);
//                    }
//                } else {
//                    Log.d("OssUpload", "获取上传渠道失败，默认使用阿里云");
//                    BaseQuestStart.get_createUploadVideo(OssService.this, fileName, ma);
//                }
//            }
//        });
        ppvodUpload("http://106.13.35.14:2000", "hypfHcZO");
    }

    private void ppvodUpload(String ppvodHost, String key) {
        new Thread(() -> {
// 标准上传
            Uploader up = new Uploader(ppvodHost + "/uploads/", key, 10 * 1024 * 1024);
            JobFuture future = up.upload(fileName);
            boolean wait = true;
            while (wait) {
                int statue = future.getStatue();
                if (statue == JobFuture.STATUS_FINISH_GENERAL
                        || statue == JobFuture.STATUS_FINISH_BIU
                        || statue == JobFuture.STATUS_ERROR) {
                    wait = false;
                } else {
                    //logger.debug("进度:" + future.progress());
                    Message message = Message.obtain();
                    message.obj = future.progress();
                    message.what = 1;
                    handler.sendMessage(message);
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            Message message = Message.obtain();
            switch (future.getStatue()) {
                case JobFuture.STATUS_FINISH_GENERAL:
                    PPVodResult result = future.getResult();
                    message.what = 2;
                    message.obj = new HashMap<String, String>() {
                        {
                            put("videoId", videoId);
                            put("playUrl", result.getUrl());
                        }
                    };
                    handler.sendMessage(message);
                    break;
                case JobFuture.STATUS_FINISH_BIU:
                    PPVodQuickUploadResult quickUploadResult = future.getQuickUploadResult();
                    Log.d("PPVodTag", "OssService: quickUploadResult == null " + (quickUploadResult == null));
                    message.what = 2;
                    message.obj = new HashMap<String, String>() {
                        {
                            put("videoId", videoId);
                            put("playUrl", quickUploadResult.getUrl());
                        }
                    };
                    handler.sendMessage(message);

                    break;
                case JobFuture.STATUS_ERROR:
                    Message mess = Message.obtain();
                    mess.what = 3;
                    handler.sendMessage(mess);
                    break;
            }
        }).start();

    }

    @WorkerThread
    public @Nullable String uploadFile(String filePath, ProgressRequestBody.UploadInterceptor interceptor) {
        okhttp3.Response response = null;
        ResponseBody body = null;
        String result = null;
        try {
            response = OkGo.<LzyResponse<Map<String, String>>>post(BaseQuestConfig.UPLOAD_OSS_FILE)
                    .params("file", new File(filePath)).uploadInterceptor(interceptor).execute();

            if (response.isSuccessful()) {
                body = response.body();
                LzyResponse<Map<String, String>> respMsg = new Gson().fromJson(body.string(), new TypeToken<LzyResponse<Map<String, String>>>() {
                }.getType());
                body.close();
                response.close();
                if (respMsg.code == 1) {
                    result = respMsg.data.get("url");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (response != null) {
                response.close();
            }
            if (body != null) {
                body.close();
            }
        }
        return result;
    }
}
