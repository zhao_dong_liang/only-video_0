package cn.wu1588.dancer.common.quest;


import cn.wu1588.dancer.BuildConfig;

/**
 * 接口地址
 */
public interface NetQuestConfig {

    /**
     * 默认的服务端口
     */
    String HTTP_API = BuildConfig.API_ADDRESS;

//    String HTTP_VIDEO_HOST = "http://video.myylook.com/";
    String HTTP_VIDEO_HOST = "http://app.myylook.com/";

}
