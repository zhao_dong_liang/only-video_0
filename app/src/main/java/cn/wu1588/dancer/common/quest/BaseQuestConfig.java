package cn.wu1588.dancer.common.quest;


/**
 * 接口参数帮助类
 */
public class BaseQuestConfig implements NetQuestConfig {


    public static final String LUCKDRAW_URL = HTTP_API + "App/User/LuckDraw/index?member_id=";
    public static final String GAME_RULE_URL = HTTP_API + "App/User/Game/index";
    //首页
    public static final String GET_HOME_DATA = HTTP_API + "Appapi/Home/home/data_arr";
    public static final int GET_HOME_DATA_CODE = 0X001;
    //频道-专栏推荐
    public static final String GET_THEMATIC_LIST = HTTP_API + "Appapi/Channel/Index/thematic_list";
    public static final int GET_THEMATIC_LIST_CODE = 0X002;
    //频道-人气、相关影片、筛选标签数据返回
    public static final String GET_LABLE_LIST = HTTP_API + "Appapi/Channel/Index/actor_list";
    public static final int GET_LABLE_LIST_CODE = 0X003;
    //列表
    public static final String GET_NVYOU_LIST = HTTP_API + "Appapi/Channel/Index/actor_select";
    public static final int GET_NVYOU_LIST_CODE = 0X004;
    //更多头部筛选
    public static final String GET_HEDER_SCREEN = HTTP_API + "Appapi/Home/home/screen";
    public static final int GET_HEDER_SCREEN_CODE = 0X005;
    //全部影片
    public static final String GET_ALL_MOVIE = HTTP_API + "Appapi/Home/home/newest_moive_more";
    public static final int GET_ALL_MOVIE_CODE = 0X006;
    //用户登录注册
    public static final String GET_USER_LOGIN = HTTP_API + "Appapi/Channel/UserCenter/login";
    public static final int GET_USER_LOGIN_CODE = 0X007;
    //用户信息修改----头像 昵称 性别
    public static final String POST_UPDATE_USER_INFO = HTTP_API + "Appapi/Channel/UserCenter/updateuserinfo";
    public static final int POST_UPDATE_USER_INFO_CODE = 0X008;
    public static final int POST_UPDATE_USER_INFO_CODE_1 = 0X0088;
    //获取用户信息
    public static final String GET_USER_INFO = HTTP_API + "Appapi/Channel/UserCenter/userinfo";
    public static final int GET_USER_INFO_CODE = 0X009;
    //我的-历史记录和我的收藏列表
    public static final String GET_HIS_RECORD_LOVE = HTTP_API + "Appapi/Channel/UserCenter/historylist";
    public static final int GET_HIS_RECORD_LOVE_CODE = 0X0010;
    //【安卓】专题分类分页数据
    public static final String GET_THEMATIC_DETAIL = HTTP_API + "Appapi/Channel/Index/thematic_info_list";
    public static final int GET_THEMATIC_DETAIL_CODE = 0X0011;
    //首页-更多
    public static final String GET_HOME_MORE = HTTP_API + "Appapi/Home/home/newest_moive_more";
    public static final int GET_HOME_MORE_CODE = 0X0012;
    //列表胸型标签数据
    public static final String GET_CHESTSELECT = HTTP_API + "Appapi/Channel/Index/chestselect";
    public static final int GET_CHESTSELECT_CODE = 0X0013;
    //频道标签筛选栏目标签数据
    public static final String GET_LABLE_SCREEN = HTTP_API + "Appapi/Channel/Index/channel_select";
    public static final int GET_LABLE_SCREEN_CODE = 0X0014;
    //频道--标签筛选专栏
    public static final String SELECT_CONTENT = HTTP_API + "Appapi/Channel/Index/label_select";
    public static final int SELECT_CONTENT_CODE = 0X0015;
    //发现
    public static final String GET_DISCOVER_LIST = HTTP_API + "Appapi/Find/Find/get_list";
    public static final int GET_DISCOVER_LIST_CODE = 0X0016;
    //个人中心（意见反馈常见问题列表+我要反馈反馈分类标签+设置里的用户协议+版本更新）
    public static final String COMM_PROBLEM_LIST = HTTP_API + "Appapi/Channel/UserCenter/data_arr";
    public static final int COMM_PROBLEM_LIST_CODE = 0X0017;
    //我的反馈列表
    public static final String GET_MY_FEEDBACK = HTTP_API + "Appapi/Channel/UserCenter/myfeed";
    public static final int GET_MY_FEEDBACK_CODE = 0X0018;
    //视频收藏
    public static final String GET_COLLECT_MOVIE = HTTP_API + "Appapi/Channel/Index/love";
    public static final int GET_COLLECT_MOVIE_CODE = 0X0019;
    //用户反馈提交
    public static final String POST_FEED_BACK = HTTP_API + "Appapi/Channel/UserCenter/feedback_submit";
    public static final int POST_FEED_BACK_CODE = 0X0020;
    //常见问题详情
    public static final String GET_QUEST_DETAIL = HTTP_API + "Appapi/Channel/UserCenter/questiondetail";
    public static final int GET_QUEST_DETAIL_CODE = 0X0021;
    //我的----历史记录和我的收藏筛选
    public static final String GET_MY_COLLECTION_LIST = HTTP_API + "Appapi/Channel/UserCenter/historyselect";
    public static final int GET_MY_COLLECTION_LIST_CODE = 0X0022;
    //历史记录和我的收藏删除+批量删除
    public static final String POST_DEL_MOVIE = HTTP_API + "Appapi/Channel/UserCenter/history_del";
    public static final int POST_DEL_MOVIE_CODE = 0X0023;
    //视频播放详情页
    public static final String GET_MOVIE_PLAY_DETAIL = HTTP_API + "Appapi/Channel/Index/movieplay";
    public static final int GET_MOVIE_PLAY_DETAIL_CODE = 0X0024;
    //视频播放详情页 - 新
    public static final String GET_MOVIE_PLAY_DETAIL_V3 = HTTP_API + "Appapi/Channel/Index/seemovie";
    public static final int GET_MOVIE_PLAY_DETAIL_CODE_V3 = 0X002444;
    //详情
    public static final String GET_NV_YOU_DETAIL = HTTP_API + "Appapi/Channel/Index/actor_info";
    public static final int GET_NV_YOU_DETAIL_CODE = 0X0025;
    //详情页相关影片筛选
    public static final String GET_SCREEN_NV_YOU_LIST = HTTP_API + "Appapi/Channel/Index/actor_movie";
    public static final int GET_SCREEN_NV_YOU_LIST_CODE = 0X0026;
    //用户协议
    public static final String GET_WEB_URL = HTTP_API + "Appapi/Agreement/Index/user_agreement";
    public static final int GET_WEB_URL_CODE = 0X0027;
    //消息列表
    public static final String GET_MSG_LIST = HTTP_API + "Appapi/Channel/UserCenter/notice";
    public static final int GET_MSG_LIST_CODE = 0X0028;
    //公告详情H5页面
    public static final String GET_NOTICE_DETAIL_URL = HTTP_API + "Appapi/Agreement/Index/noticedetail";
    public static final int GET_NOTICE_DETAIL_URL_CODE = 0X0029;
    //评论详情页
    public static final String GET_COMMENTS_DETAIL = HTTP_API + "Appapi/Channel/Index/commentdetail";
    public static final int GET_COMMENTS_DETAIL_CODE = 0X0030;
    //提交评论回复
    public static final String POST_COMMENTS = HTTP_API + "Appapi/Channel/Index/go_comment";
    public static final int POST_COMMENTS_CODE = 0X0031;
    //评论点赞
    public static final String COMMENTS_DIANZAN = HTTP_API + "Appapi/Channel/Index/comment_top";
    public static final int COMMENTS_DIANZAN_CODE = 0X0032;
    // 查看影片是否点赞或者收藏
    public static final String GET_MOVIE_ACTION_STATES = HTTP_API + "Appapi/Channel/Index/action_status";
    public static final int GET_MOVIE_ACTION_STATES_CODE = 0X0033;
    //视频点赞
    public static final String MOVIE_DIANZAN = HTTP_API + "Appapi/Channel/Index/give";
    public static final int MOVIE_DIANZAN_CODE = 0X0034;
    //视频收藏
    public static final String MOVIE_SHOUCANG = HTTP_API + "Appapi/Channel/Index/love";
    public static final int MOVIE_SHOUCANG_CODE = 0X0035;
    //首页-热门搜索关键字
    public static final String GET_HOT_SEARCH = HTTP_API + "Appapi/Home/home/hot_search";
    public static final int GET_HOT_SEARCH_CODE = 0X0036;
    //首页搜索
    public static final String GET_SEARCH = HTTP_API + "Appapi/Home/home/search_list";
    public static final int GET_SEARCH_CODE = 0X0037;
    //个人中心广告
    public static final String GET_ADVER_DATA = HTTP_API + "Appapi/Home/Home/user_banner";
    public static final int GET_ADVER_CODE = 0X0038;
    //购买会员-用户信息
    public static final String GET_RECHARGE_INFO = HTTP_API + "Appapi/Channel/UserVip/user_info";
    public static final int GET_RECHARGE_INFO_CODE = 0X0039;

    //购买会员-购买记录
    public static final String GET_CONSUME = HTTP_API + "Appapi/Channel/UserVip/buy_record";
    public static final int GET_CONSUME_CODE = 0X0040;
    //首页-换一批
    public static final String GET_CHANGE_DATA = HTTP_API + "Appapi/Home/home/change_another";
    public static final int GET_CHANGE_DATA_CODE = 0X0041;
    //【支付
    public static final String GET_PAY_URL = HTTP_API + "Appapi/Pay/Order/create_order";
    public static final int GET_PAY_URL_CODE = 0X0042;
    //【支付】通道列表
    public static final String GET_PAY_CHANEL = HTTP_API + "Appapi/Pay/Aisle/aisle_list";
    public static final int GET_PAY_CHANEL_CODE = 0X0043;
    //【首页】公告
    public static final String GET_HOME_NOTICE = HTTP_API + "Appapi/Pay/Aisle/notice";
    public static final int GET_HOME_NOTICE_CODE = 0X0044;
    //获取视频播放地址
    public static final String GET_MOVIE_URL = HTTP_API + "Appapi/Channel/Index/seemovie";
    public static final int GET_MOVIE_URL_CODE = 0X0045;
    //标签搜索
    public static final String GET_lable_SEARCH = HTTP_API + "Appapi/Home/Home/label_search";
    public static final int GET_lable_SEARCH_CODE = 0X0046;
    //推广
    public static final String GET_TUIGUANG = HTTP_API + "Appapi/Pay/Aisle/generalize";
    public static final int GET_TUIGUANG_CODE = 0X0047;
    //下载添加用户记录
    public static final String POST_ADD_MEMBER_RECORD = HTTP_API + "Appapi/Pay/Aisle/add_member_record";
    public static final int POST_ADD_MEMBER_RECORD_CODE = 0X0048;
    //版本更新
    public static final String GET_VERSION_UPDATE = HTTP_API + "Appapi/Channel/UserCenter/version";
    public static final int GET_VERSION_UPDATE_CODE = 0X0049;
    //【安卓】专题分类详情
    public static final String GET_HEDER_INFO = HTTP_API + "Appapi/Channel/Index/thematic_info";
    public static final int GET_HEDER_INFO_CODE = 0X0050;
    //获取支付方式
    public static final String GET_PAY_METHOD = HTTP_API + "Appapi/Pay/Aisle/get_pay_way";
    public static final int GET_PAY_METHOD_CODE = 0X0051;
    //生成订单
    public static final String CREATE_ORDER = HTTP_API + "Appapi/Pay/Wx/create_order";
    public static final int CREATE_ORDER_CODE = 0X0052;
    //生成订单微信
    public static final String CREATE_WEIXIN_ORDER = HTTP_API + "Appapi/Pay/Wxpay/create_order";
    public static final int CREATE_WEIXIN_ORDER_CODE = 0X0053;
    public static final int CREATE_WEIXIN_ORDER_CODE_1 = 0X00533;
    //支付宝支付
    public static final String CREATE_PAYMENT_ORDER = HTTP_API + "Appapi/Pay/Ali/create_order";
    public static final int CREATE_PAYMENT_ORDER_CODE = 0X0054;
    public static final int CREATE_PAYMENT_ORDER_CODE_1 = 0X00544;
    //生成订单微信
    public static final String POST_PAY_URL = HTTP_API + "Appapi/Qrcode/Index/pay_url";
    public static final int POST_PAY_URL_CODE = 0X0055;
    //获取启动图列表
    public static final String START_UP_PICS = HTTP_API + "Appapi/Agreement/StartUp";
    public static final int START_UP_PICS_CODE = 0X0056;
    //发现页顶部列表
    public static final String DISCOVER_PAGE_LIST = HTTP_API + "Appapi/Agreement/Tabbar";
    public static final int DISCOVER_PAGE_LIST_CODE = 0X0057;
    //获取支付方式
    public static final String GET_PAY_METHOD_NEW = HTTP_API + "Appapi/Pay/Index/get_pay_way";
    public static final int GET_PAY_METHOD_NEW_CODE = 0X0058;
    //获取频道或发现是否显示
    public static final String GET_TAB_LIST = HTTP_API + "Appapi/Agreement/Tabbar";
    public static final int GET_TAB_LIST_CODE = 0X0059;
    //获取发现页内容
    public static final String GET_DISCOVER_MENU_LIST = HTTP_API + "Appapi/Channel/Index/home_category";
    public static final int GET_DISCOVER_MENU_LIST_CODE = 0X0060;
    //获取发现页内容
    public static final String GET_DISCOVER_REC = HTTP_API + "Appapi/Channel/Index/home_show";
    public static final int GET_DISCOVER_REC_CODE = 0X0061;
    //获取发现页内容
    public static final String GET_CATE_SHOW_LIST = HTTP_API + "Appapi/Channel/Index/cate_show";
    public static final int GET_CATE_SHOW_LIST_CODE = 0X0062;
    //获取频道页新增模块
    public static final String HOT_LIST = HTTP_API + "Appapi/Channel/Index/hot_cate_data";
    public static final int HOT_LIST_CODE = 0X0063;
    //获取频道页新增模块内容
    public static final String HOT_SHOW = HTTP_API + "Appapi/Channel/Index/hot_show";
    public static final int HOT_SHOW_CODE = 0X0064;
    //设置信息
    public static final String SETTING_INFO = HTTP_API + "Appapi/Channel/Index/setting";
    public static final int SETTING_INFO_CODE = 0X0065;
    //金币套餐
    public static final String GET_GOLD_MEAL = HTTP_API + "Appapi/Pay/index/get_gold_meal";
    public static final int GET_GOLD_MEAL_CODE = 0X0066;
    //扣除金币
    public static final String DEDUCT_GOLD_ACCOUNT = HTTP_API + "Appapi/pay/index/deduct_gold_account";
    public static final int DEDUCT_GOLD_ACCOUNT_CODE = 0X0067;
    //手机号注册
    public static final String REGISTER_USER = HTTP_API + "Appapi/Channel/UserCenter/register";
    public static final int REGISTER_USER_CODE = 0X0068;
    //获取验证码
    public static final String SEND_SMS_CODE = HTTP_API + "Appapi/Channel/UserCenter/send_sms_code";
    public static final int SEND_SMS_CODE_CODE = 0X0069;
    //手机登录
    public static final String PHONE_LOGIN = HTTP_API + "Appapi/Channel/UserCenter/phone_login";
    public static final int PHONE_LOGIN_CODE = 0X0070;
    //密码修改
    public static final String CHANGE_PASSWORD = HTTP_API + "Appapi/Channel/UserCenter/change_password";
    public static final int CHANGE_PASSWORD_CODE = 0X0071;
    //第三方登录配置
    public static final String GET_THIRD_LOGIN_PARAM = HTTP_API + "Appapi/Param/index/get_third_login_param";
    public static final int GET_THIRD_LOGIN_PARAM_CODE = 0X0072;
    //第三方登录配置
    public static final String QQ_LOGIN = HTTP_API + "Appapi/Channel/UserCenter/qq_login";
    public static final int QQ_LOGIN_CODE = 0X0073;
    //第三方登录配置
    public static final String WX_LOGIN = HTTP_API + "Appapi/Channel/UserCenter/wx_login";
    public static final int WX_LOGIN_CODE = 0X0074;
    //主题颜色
    public static final String GET_NAVBAR_COLOR = HTTP_API + "/Appapi/param/Index/get_navbar_color";
    public static final int GET_NAVBAR_COLOR_CODE = 0X0075;

    //获取省市
    public static final String GET_REGION = HTTP_API + "Appapi/param/Index/get_region";
    public static final int GET_REGION_CODE = 0X0076;

    //绑定手机
    public static final String BIND_PHONE = HTTP_API + "Appapi/Channel/UserCenter/bind_phone";
    public static final int BIND_PHONE_CODE = 0X0077;

    //记录视频下载次数
    public static final String UPDATE_MOVIE_DOWNLOAD_NUM = HTTP_API + "Appapi/Channel/index/update_movie_download_num";
    public static final int UPDATE_MOVIE_DOWNLOAD_NUM_CODE = 0X0078;

    //查看用户是否关注该用户
    public static final String CHECK_USER_ATTENTION = HTTP_API + "Appapi/Channel/userCenter/check_user_attention";
    public static final int CHECK_USER_ATTENTION_CODE = 0X0079;

    //关注用户
    public static final String ATTENTION_USER = HTTP_API + "Appapi/Channel/userCenter/attention_user";
    public static final int ATTENTION_USER_CODE = 0X0080;

    //更新分享小视频的次数
    public static final String UPDATE_MOVIE_SHARE_NUM = HTTP_API + "Appapi/Channel/index/update_movie_share_num";
    public static final int UPDATE_MOVIE_SHARE_NUM_CODE = 0X0081;

    //获取视频评论列表
    public static final String GET_MOVIE_COMMENT = HTTP_API + "Appapi/Channel/index/get_movie_comment";
    public static final int GET_MOVIE_COMMENT_CODE = 0X0082;


    //获取我的视频
    public static final String GET_MINE_VIDEOS = HTTP_API + "Appapi/Channel/userCenter/get_user_movie";
    public static final int GET_MINE_VIDEOS_CODE = 0X0083;

    //获取视频分类列表
    public static final String GET_VIDEOS_TYPE = HTTP_API + "Appapi/Home/home/screen";
    public static final int GET_VIDEOS_TYPE_CODE = 0X0084;

    //获取视频分类标签
    public static final String GET_VIDEOS_LABE = HTTP_API + "Appapi/Channel/Index/get_label";
    public static final int GET_VIDEOS_LABE_CODE = 0X0085;

    //获取选择视频页列表
    public static final String GET_ALL_VIDEO = HTTP_API + "Appapi/Channel/Index/cate_show?cate_id=16&page=1&pageSize=10&uid=70";
    public static final int GET_ALL_VIDEO_CODE = 0X0086;

    //获取收益视频列表页
    public static final String GET_PROFIT_VIDEO_LIST = HTTP_API + "Appapi/Channel/Index/cate_show";
    public static final int GET_PROFIT_VIDEO_LIST_CODE = 0X0087;

    //获取收益专辑列表页
    public static final String GET_PROFIT_ALBUM_LIST = HTTP_API + "Appapi/Channel/Index/hot_cate_data";
    public static final int GET_PROFIT_ALBUM_LIST_CODE = 0X0088;

    //获取我的提现记录列表
    public static final String GET_GET_CASH_LIST = HTTP_API + "Appapi/Channel/userCenter/get_withdraw_records";
    public static final int GET_GET_CASH_LIST_CODE = 0X0089;

    //获取上传凭证
    public static final String GET_CREATEUPLOADVIDEO = HTTP_API + "Appapi/Home/Index/get_createUploadVideo";
    public static final int GET_CREATEUPLOADVIDEO_CODE = 0X0090;

    //获取我的关注列表
    public static final String GET_MY_FOLLOW_LIST = HTTP_API + "Appapi/Channel/Index/cate_show";
    public static final int GET_MY_FOLLOW_LIST_CODE = 0X0091;

    //获取我的购买的视频列表
    public static final String GET_MY_BUY_VIDEOS = HTTP_API + "Appapi/Channel/Index/get_user_buy_movie";
    public static final int GET_MY_BUY_VIDEOS_CODE = 0X0092;


    //获取我的购买的专辑列表
    public static final String GET_MY_BUY_ALBUM = HTTP_API + "Appapi/Channel/Index/get_user_buy_channel";
    public static final int GET_MY_BUY_ALBUM_CODE = 0X0093;

    //获取二级评论
    public static final String GET_MOVIE_CHILD_COMMENT = HTTP_API + "Appapi/Channel/Index/get_movie_child_comment";
    public static final int GET_MOVIE_CHILD_COMMENT_CODE = 0X0094;

    //发布视频
    public static final String ADD_VIDEO = HTTP_API + "Appapi/Home/Index/add";
    public static final int ADD_VIDEO_CODE = 0X0095;

    //文件上传
//    public static final String UPLOAD_FILE="https://test.yzyy666.com"+"/api/upload/upload";
    public static final String UPLOAD_FILE = HTTP_API + "Appapi/channel/Index/upload_picture";
    public static final int UPLOAD_FILE_CODE = 0X0096;

    //我的下载记录
    public static final String GET_USER_DOWNLOAD_RECORDS = HTTP_API + "Appapi/Channel/index/get_user_download_records";
    public static final int GET_USER_DOWNLOAD_RECORDS_CODE = 0X0097;

    //添加用户专辑
    public static final String ADD_USER_CHANNEL = HTTP_API + "Appapi/Channel/userCenter/add_user_channel";
    public static final int ADD_USER_CHANNEL_CODE = 0X0098;

    //获取用户专辑
    public static final String GET_USER_CHANNEL = HTTP_API + "Appapi/Channel/userCenter/get_user_channel";
    public static final int GET_USER_CHANNEL_CODE = 0X0099;

    //获取专辑内的视频
    public static final String GET_CHANNEL_MOVIES = HTTP_API + "Appapi/Channel/Index/get_channel_movies";
    public static final int GET_CHANNEL_MOVIES_CODE = 0X0100;

    //查询用户是否设置了私密视频的密码
    public static final String CHECK_IS_SECRET = HTTP_API + "Appapi/channel/userCenter/check_is_secret";
    public static final int CHECK_IS_SECRET_CODE = 0X0101;

    //重置私密视频的密码
    public static final String SET_SECRET = HTTP_API + "Appapi/Channel/UserCenter/set_secret";
    public static final int SET_SECRET_CODE = 0X0102;

    //验证私密视频密码
    public static final String VALIDATE_SECRET = HTTP_API + "Appapi/Channel/UserCenter/validate_secret";
    public static final int VALIDATE_SECRET_CODE = 0X0103;

    //获取我的关注
    public static final String GET_MY_ATTENTION = HTTP_API + "Appapi/Channel/userCenter/get_my_attention";
    public static final int GET_MY_ATTENTION_CODE = 0X0104;

    //取消关注
    public static final String CANCEL_ATTENTION = HTTP_API + "Appapi/Channel/userCenter/cancel_attention";
    public static final int CANCEL_ATTENTION_CODE = 0X0105;

    //获取粉丝列表
    public static final String GET_MY_FANS = HTTP_API + "Appapi/Channel/userCenter/get_my_fans";
    public static final int GET_MY_FANS_CODE = 0X0106;

    //获取我的收益
    public static final String GET_USER_ALL_INCOME = HTTP_API + "Appapi/Channel/userCenter/get_user_all_income";
    public static final int GET_USER_ALL_INCOME_CODE = 0X0107;

    //用户是否设置了提现密码
    public static final String CHECK_IS_CASH_SECRET = HTTP_API + "Appapi/channel/userCenter/check_is_cash_secret";
    public static final int CHECK_IS_CASH_SECRET_CODE = 0X0108;

    //用户是否设置了提现账户
    public static final String CHECK_ALI_NAME = HTTP_API + "Appapi/channel/userCenter/check_ali_name";
    public static final int CHECK_ALI_NAME_CODE = 0X0109;

    //设置用户提现账户
    public static final String SET_USER_ALI_NAME = HTTP_API + "Appapi/Channel/UserCenter/set_user_ali_name";
    public static final int SET_USER_ALI_NAME_CODE = 0X0110;

    //用户提现
    public static final String WITHDRAW = HTTP_API + "Appapi/Channel/userCenter/withdraw";
    public static final int WITHDRAW_CODE = 0X0111;

    //获取用户余额
    public static final String USER_ACCOUNT = HTTP_API + "Appapi/Channel/userCenter/get_user_account";
    public static final int USER_ACCOUNT_CODE = 0X0112;

    //添加或修改广告
    public static final String SAVE_AD = HTTP_API + "Appapi/ad/index/save_ad";
    public static final int SAVE_AD_CODE = 0X0113;

    //获取数字专辑页面数据
    public static final String TEACHING_CHANNEL = HTTP_API + "Appapi/Channel/Index/teaching_channel";
    public static final int TEACHING_CHANNEL_CODE = 0X0114;

    // 新版专辑首页
    public static final String CHANNEL_LIST = HTTP_API + "Appapi/Channel/Index/channel_list";
    public static final int CHANNEL_LIST_CODE = 0X0115;

    // 获取广告
    public static final String AD_LIST = HTTP_API + "Appapi/Ad/index/get_user_ad";
    public static final int AD_LIST_CODE = 0X0116;

    // 删除广告
    public static final String AD_DELETE = HTTP_API + "Appapi/Ad/index/del_user_ad";
    public static final int AD_DELETE_CODE = 0X0117;

    // 设置广告投放方式
    public static final String AD_PUT = HTTP_API + "Appapi/ad/index/edit_ad_mode";
    public static final int AD_PUT_CODE = 0X0118;

    // 设置广告投放记录
    public static final String AD_HISTORIES = HTTP_API + "Appapi/ad/index/get_ad_records";
    public static final int AD_HISTORIES_CODE = 0X0119;

    // 获取系统参数
    public static final String SYSTEM_PARAM = HTTP_API + "Appapi/param/Index/get_param";
    public static final int SYSTEM_PARAM_CODE = 0X0120;

    // 获取投诉类型
    public static final String FEED_BACK = HTTP_API + "Appapi/Feedback/index/get_feedback_cate";
    public static final int FEED_BACK_CODE = 0X0121;

    // 提交投诉
    public static final String SUBMIT_FEED_BACK = HTTP_API + "Appapi/Feedback/index/add_feedback";
    public static final int SUBMIT_FEED_BACK_CODE = 0X0122;

    // 获取可播放的广告
    public static final String PLAY_AD = HTTP_API + "Appapi/Channel/Index/get_play_ad";
    public static final int PLAY_AD_CODE = 0X0123;

    // 获取申请达人认证信息
    public static final String USER_TALENT_INFO = HTTP_API + "Appapi/Channel/userCenter/get_user_talent_info";
    public static final int USER_TALENT_INFO_CODE = 0X0124;
    // 提交申请达人认证信息
    public static final String APPLY_TALENT = HTTP_API + "Appapi/Channel/userCenter/apply_talent";
    public static final int APPLY_TALENT_CODE = 0X0125;
    // 更新广告点击信息
    public static final String UPDATE_STATISTICS = HTTP_API + "Appapi/ad/index/update_statistics";
    public static final int UPDATE_STATISTICS_CODE = 0X0126;

    // 获取广告充值记录
    public static final String AD_RECHARGE_RECORDS = HTTP_API + "Appapi/Channel/userCenter/get_ad_recharge_records";
    public static final int AD_RECHARGE_RECORDS_CODE = 0X0127;

    // 实名认证
    public static final String USER_AUTH = HTTP_API + "Appapi/Channel/userCenter/validate_idcard";
    public static final int USER_AUTH_CODE = 0X0128;
    // 上传oss文件
    public static final String UPLOAD_OSS_FILE = HTTP_API + "Appapi/Home/Index/addossfile";
    public static final int UPLOAD_OSS_FILE_CODE = 0X0129;
    // 获取上传通道
    public static final String UPLOAD_CHANNEL = HTTP_API + "Appapi/Home/Index/ret_passageway";
    public static final int UPLOAD_CHANNEL_CODE = 0X0130;

    //获取粉丝和关注用户数
    public static final String MINE_FANS = HTTP_API + "Appapi/Channel/UserCenter/get_fans_num";
    public static final int MINE_FANS_CODE = 0X0190;

    //获取用户今日数据
    public static final String MINE_TODAY_DATA = HTTP_API + "Appapi/Channel/UserCenter/get_today_data";
    public static final int MINE_TODAY_DATA_CODE = 0X0191;

    //查询会员购买记录
    public static final String MINE_MEMBER_RECORD = HTTP_API + "Appapi/Channel/UserCenter/get_member_record";
    public static final int MINE_MEMBER_RECORD_CODE = 0X0192;

    //VIP套餐获取
    public static final String VIP_SET_MEAL = HTTP_API + "Appapi/Channel/UserVip/vip_meal";
    public static final int VIP_SET_MEAL_CODE = 0X0193;

    //舞币套餐获取
    public static final String WB_SET_MEAL = HTTP_API + "Appapi/Channel/UserVip/gold_meal";
    public static final int WB_SET_MEAL_CODE = 0X0194;

    //获取用户信息
    public static final String GET_USER_TIME = HTTP_API + "Appapi/ad/index/get_user_time";
    public static final int GET_USER_TIME_CODE = 0X0600;

    //获取用户时间列表
    public static final String GET_USER_TIME_LIST = HTTP_API + "Appapi/ad/index/get_time";
    public static final int GET_USER_TIME_LIST_CODE = 0X0601;

    //获取用户时间列表
    public static final String GET_FANS_LIST = HTTP_API + "Appapi/Channel/UserCenter/get_fans_list";
    public static final int GET_FANS_LIST_CODE = 0X0602;

    //获取用户时间列表
    public static final String GET_FANS_NUM = HTTP_API + "Appapi/Channel/UserCenter/get_fans_num";
    public static final int GET_FANS_NUM_CODE = 0X0603;

    //获取用户消息列表
    public static final String GET_MSG_NOTICE_LIST = HTTP_API + "Appapi/Channel/userCenter/get_message";
    public static final int GET_MSG_NOTICE_LIST_CODE = 0X0604;


    //查询原创申请条件是否满足
    public static final String GET_TO_CAN_APPLY_ORIGINAL = HTTP_API + "Appapi/Channel/userCenter/can_apply_original";
    public static final int GET_TO_CAN_APPLY_ORIGINAL_CODE = 0X0196;


    //申请原创
    public static final String GET_TO_APPLY_ORIGINAL = HTTP_API + "Appapi/Channel/UserCenter/to_apply_original";
    public static final int GET_TO_APPLY_ORIGINAL_CODE = 0X0197;


    //订阅视频页面没有订阅时展示的接口
    public static final String HOT_DY_NULL = HTTP_API + "Appapi/Channel/Index/hot_dy";

    //获取订阅用户视频列表 关注数+视频列表
    public static final String GET_ATTENTION_MOVIES = HTTP_API + "Appapi/Channel/Index/get_attention_movies";

    //设置当前广告
    public static final String SET_AD_CURRENT = HTTP_API + "Appapi/ad/index/set_ad_current";
    //分享记录
    public static final String GET_FORWARD_RECORD = HTTP_API + "Appapi/Channel/UserCenter/get_forward_record";

    //    添加登陆记录（正）
    public static final String LOGIN_LOG = HTTP_API + "Appapi/Channel/UserCenter/login_log";

    //4K购买记录查询
    public static final String GET_PURCHASE_RECORDS = HTTP_API + "Appapi/Channel/UserCenter/get_purchase_records";


    //新 视频点赞
    public static final String GET_NEW_MOVIE_DIANZAN = HTTP_API + "Appapi/Channel/Index/give";
    //新 视频收藏 -- 接口有问题 无法取消收藏
    public static final String GET_NEW_MOVIE_COLLECT = HTTP_API + "/Appapi/Channel/Index/love";
    //新 关注新用户（正）
    public static final String GET_NEW_ATTENTION_USER = HTTP_API + "/Appapi/Channel/userCenter/attention_user";
    //新 取消关注用户
    public static final String GET_NEW_CANCEL_ATTENTION = HTTP_API + "/Appapi/Channel/userCenter/cancel_attention";
    //新 查看影片是否点赞或者收藏
    public static final String GET_NEW_MOVIE_ACTION_STATES = HTTP_API + "/Appapi/Channel/Index/action_status";
    //新 当期分类按照用户依次获取视频
    public static final String GET_NEW_MOVIE_BY_CATE = HTTP_API + "/Appapi/Channel/UserCenter/get_movie_by_cate";
    public static final int GET_NEW_MOVIE_BY_CATE_CODE = 0X0024444;
    //新 查询会员 VIP 套餐状态
    public static final String GET_NEW_IS_VIP = HTTP_API + "/Appapi/Channel/UserVip/is_vip";
    //新 保存用户的分享记录
    public static final String GET_NEW_FORWARD_RECORD = HTTP_API + "/Appapi/Channel/UserCenter/set_forward_record";
    //新 添加下载记录
    public static final String GET_NEW_ADD_DOWNLOAD_RECORD = HTTP_API + "/Appapi/Channel/index/add_download_records";
    //删除
    public static final String DEL_MOVIE = HTTP_API + "/Appapi/Home/Index/del_movie";
    //新 获取专辑详情
    public static final String GET_NEW_CHANNEL_MOVIES = HTTP_API + "/Appapi/Channel/Index/get_channel_movies";

}



