package cn.wu1588.dancer.common.util;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;

import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.sunrun.sunrunframwork.utils.Utils;

import java.net.URISyntaxException;
import java.util.List;

/**
 * Created by WQ on 2017/6/15.
 */

public class Tool {

    public static void opWxin(Context context){
        if(isWeixinAvilible(context)){
            Intent intent = new Intent();
            ComponentName cmp=new ComponentName("com.tencent.mm","com.tencent.mm.ui.LauncherUI");
            intent.setAction(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setComponent(cmp);
            context.startActivity(intent);
        }else {
            UIUtils.shortM("为检测到微信程序或者微信版本过低");
        }
    }

    public static boolean isWeixinAvilible(Context context) {
        final PackageManager packageManager = context.getPackageManager();// 获取packagemanager
        List<PackageInfo> pinfo = packageManager.getInstalledPackages(0);// 获取所有已安装程序的包信息
        if (pinfo != null) {
            for (int i = 0; i < pinfo.size(); i++) {
                String pn = pinfo.get(i).packageName;
                if (pn.equals("com.tencent.mm")) {
                    return true;
                }
            }
        }
        return false;
    }

    public static  void goToMarket(Context context){
        try {
            Uri uri = Uri.parse("market://details?id="+context.getPackageName());
            Intent intent = new Intent(Intent.ACTION_VIEW,uri);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }catch (Exception e){
            e.printStackTrace();
            UIUtils.shortM("未检测到应用市场!");
        }

    }
    public static boolean checkAliPayInstalled(Context context) {

        Uri uri = Uri.parse("alipays://platformapi/startApp");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        ComponentName componentName = intent.resolveActivity(context.getPackageManager());
        return componentName != null;
    }

    /**
     * return the intent of start other apps by url intent
     *
     * @param url
     *     url
     * @param tag
     *     Used to pick the specified application among the candidates,
     *     it should be the package name or the key word of the package name
     *
     * @return the intent of the specified app
     */
    public static Intent getCallOtherAppsByUrlIntent(String url, String tag) {
        if (url.startsWith("https") || url.startsWith("http") || url.startsWith("ftp")) {
            return handleHttpIntent(url, tag);
        } else {
            try {
                return Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private static Intent handleHttpIntent(String url, String tag) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        ResolveInfo resolveInfo = getResolveInfo(intent, tag);
        if (null != resolveInfo) {
            try {
                intent.setClassName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name);
                return intent;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private static ResolveInfo getResolveInfo(Intent intent, String tag) {

//        List<ResolveInfo> resolveInfos = .getApp().getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
//        for (ResolveInfo resolveInfo : resolveInfos) {
//            //use the title or Remotely configured package name to find the target app
//            if (resolveInfo.activityInfo.packageName.contains(tag)) {
//                return resolveInfo;
//            }
//        }
        return null;
    }
}
