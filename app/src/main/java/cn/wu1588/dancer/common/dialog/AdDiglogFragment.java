package cn.wu1588.dancer.common.dialog;

import android.os.Bundle;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cn.wu1588.dancer.R;
import com.cnsunrun.commonui.widget.roundwidget.QMUIRoundButton;
import com.sunrun.sunrunframwork.utils.EmptyDeal;

import androidx.annotation.Nullable;
import androidx.core.text.HtmlCompat;
import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.base.LBaseDialogFragment;
import cn.wu1588.dancer.home.mode.HomeNoticeBean;

/**
 * 首页公告弹窗
 */
public class AdDiglogFragment extends LBaseDialogFragment {

    @BindView(R.id.ivHead)
    ImageView ivHead;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvContent)
    TextView tvContent;
    @BindView(R.id.btnSure)
    QMUIRoundButton btnSure;
    public HomeNoticeBean homeNoticeBean;

    public static AdDiglogFragment newInstance() {
        Bundle args = new Bundle();
        AdDiglogFragment fragment = new AdDiglogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!EmptyDeal.isEmpy(homeNoticeBean)) {
            tvTitle.setText(homeNoticeBean.title);
            Spanned spanned = HtmlCompat.fromHtml(homeNoticeBean.content, HtmlCompat.FROM_HTML_MODE_LEGACY);
            tvContent.setText(spanned);
            tvContent.setMovementMethod(LinkMovementMethod.getInstance());
        }
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_dialog_ad;
    }

    @OnClick(R.id.btnSure)
    public void onViewClicked() {
        dismissAllowingStateLoss();
    }

    public AdDiglogFragment setData(HomeNoticeBean homeNoticeBean) {
        this.homeNoticeBean = homeNoticeBean;
        return this;
    }
}
