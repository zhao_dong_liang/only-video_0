package cn.wu1588.dancer.common.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;

import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.my.toolslib.FileUtils;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;

import org.json.JSONObject;

import java.io.File;

import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.widget.LoadDialogUtils;

/**
 * Created by 86149 on 2018/12/3.
 */

public class UpLoadUtils {
    private UpLoadCallBack upLoadCallBack;

    private Context context;

    public UpLoadUtils(Context context) {
        this.context = context;
    }

    public void setUpLoadCallBack(UpLoadCallBack upLoadCallBack) {
        this.upLoadCallBack = upLoadCallBack;
    }

    public void upLoad(Uri picUri) {
        LoadDialogUtils.showDialog(context);
        Thread thread = new MyThead(picUri);
        thread.start();
    }

    private int index;

    public void setIndex(int index) {
        this.index = index;
    }

    public void upLoad(String path) {
        LoadDialogUtils.showDialog(context);
        Thread thread = new MyThead(null);
        ((MyThead) thread).setPath(path);
        thread.start();

    }

    private void httpUp(File file) {
        OkGo.<String>post(BaseQuestStart.UPLOAD_FILE)
                .headers("Connection", "close")
                .params("img", file)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        String picUrl = null;
                        try {
                            JSONObject jsonObject = new JSONObject(response.body());
                            int code = jsonObject.getInt("status");
                            if (code == 1) {
                                picUrl = jsonObject.getString("data");
                            } else {
                                ToastUtils.longToast("上传封面图失败");
                                LoadDialogUtils.dissmiss();
                            }


                        } catch (Exception e) {
                            Log.e("上传图片异常", e.toString());
                        }
                        if (picUrl == null) {
                            ToastUtils.longToast("上传失败，请重试");
                        } else {
                            if (upLoadCallBack != null) {
                                upLoadCallBack.upLoadResult(picUrl, file);
                            }
                        }

                    }

                    @Override
                    public void onError(Response<String> response) {
                        super.onError(response);
                        ToastUtils.longToast("上传失败，请重试");
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
//                        index--;
//                        if (index<=0){
                        LoadDialogUtils.dissmiss();
//                        }

                    }
                });
    }

    class MyThead extends Thread {
        private Uri picUri;
        private String path;

        public void setPath(String path) {
            this.path = path;
        }

        public MyThead(Uri picUri) {
            this.picUri = picUri;
        }

        @Override
        public void run() {
            super.run();
            File file = null;
            if (picUri == null) {
                file = new File(path);
            } else {
                file = FileUtils.getFileByUri(picUri, context);
            }

            Bitmap bitmap = BitmapFactory.decodeFile(file.toString());
            byte[] bytes = null;
            if (bitmap != null) {
                int orientation = BitmapUtils.getCameraPhotoOrientation(file.toString());
//            Log.i("OkGo","旋转角度=="+orientation);

                if (orientation != 0) {
                    bytes = BitmapUtils.compressImage(BitmapUtils.rotateBitmap(bitmap, orientation));
                } else {
                    bytes = BitmapUtils.compressImage(bitmap);
                }
            }


            if (bytes != null) {
                File fileWithByte = FileUtils.createFileWithByte(bytes, file);
                Log.i("OkGo:", fileWithByte.toString());

                if (fileWithByte.exists()) {
//                Log.i("OkGo",fileWithByte.toString());
                    UpLoadUtils.this.httpUp(fileWithByte);
                }
            } else {
                UpLoadUtils.this.httpUp(file);
            }

        }
    }

    public interface UpLoadCallBack {
        void upLoadResult(String url, File file);
    }

    public void clear() {
        OkGo.getInstance().cancelTag(this);
    }
}
