package cn.wu1588.dancer.common.widget.gridpasswordview;

/**
 * @author Jungly
 * jungly.ik@gmail.com
 * 5/3/21 16:47
 */
public enum PasswordType {

    NUMBER, TEXT, TEXTVISIBLE, TEXTWEB;

}
