package cn.wu1588.dancer.common.util.ppvod;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import com.lzy.okgo.model.Progress;
import com.lzy.okserver.ProgressListener;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.Buffer;
import okio.BufferedSink;
import okio.ForwardingSink;
import okio.Okio;
import okio.Sink;

public class ProgressRequestBody extends RequestBody {
    public static final int UPDATE = 0x01;
    private RequestBody requestBody;
    private com.lzy.okgo.request.base.ProgressRequestBody.UploadInterceptor uploadInterceptor;
    private BufferedSink bufferedSink;
    private MyHandler myHandler;
    private Progress progress = new Progress();

    public ProgressRequestBody(RequestBody body, com.lzy.okgo.request.base.ProgressRequestBody.UploadInterceptor listener) throws IOException {
        requestBody = body;
        progress.totalSize = requestBody.contentLength();
        uploadInterceptor = listener;
        if (myHandler == null) {
            myHandler = new MyHandler();
        }
    }

    class MyHandler extends Handler {
        //放在主线程中显示
        public MyHandler() {
            super(Looper.getMainLooper());
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case UPDATE:
                    ProgressModel progressModel = (ProgressModel) msg.obj;
                    if (uploadInterceptor != null) {
                        Progress.changeProgress(progress, progressModel.bytesWritten, new Progress.Action() {
                            @Override
                            public void call(Progress progress) {
                                uploadInterceptor.uploadProgress(progress);
                            }
                        });

                    }

                    break;

            }
        }


    }

    @Override
    public MediaType contentType() {
        return requestBody.contentType();
    }

    @Override
    public long contentLength() throws IOException {
        return requestBody.contentLength();
    }

    @Override
    public void writeTo(BufferedSink sink) throws IOException {

        if (bufferedSink == null) {
            bufferedSink = Okio.buffer(sink(sink));
        }
        //写入
        requestBody.writeTo(bufferedSink);
        //刷新
        bufferedSink.flush();
    }

    private Sink sink(BufferedSink sink) {

        return new ForwardingSink(sink) {
            long bytesWritten = 0L;
            long contentLength = 0L;

            @Override
            public void write(Buffer source, long byteCount) throws IOException {
                super.write(source, byteCount);
                if (contentLength == 0) {
                    contentLength = contentLength();
                }
                bytesWritten += byteCount;
                //回调
                Message msg = Message.obtain();
                msg.what = UPDATE;
                msg.obj = new ProgressModel(bytesWritten, contentLength, bytesWritten == contentLength);
                myHandler.sendMessage(msg);
            }
        };
    }


    private class ProgressModel {
        private long bytesWritten;
        private long contentLength;
        boolean complete;

        public ProgressModel(long bytesWritten, long contentLength, boolean complete) {
            this.bytesWritten = bytesWritten;
            this.contentLength = contentLength;
            this.complete = complete;
        }
    }
}