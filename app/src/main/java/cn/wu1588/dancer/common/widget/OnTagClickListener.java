package cn.wu1588.dancer.common.widget;

/**
 * Created by wangchao on 2018-12-06.
 */
public interface OnTagClickListener {
    void onTagClick(int position);
}
