package cn.wu1588.dancer.common.util;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cn.wu1588.dancer.common.widget.MomentPicView;

/**
 * Created by WQ on 2017/11/2.
 */

public class UploadImageHelper {
    String addImgResId = "-home1";
    int maxImg=9;
    MomentPicView momentPicView;
    boolean isAddImgInStart=false;
    List<String>currentData=new ArrayList<>();
    public UploadImageHelper(int addImgResId) {
        this.addImgResId = String.valueOf(addImgResId);
    }
    public void attachMomentPicView(MomentPicView momentPicView){
        this.momentPicView=momentPicView;
        momentPicView.setImageUrls(Arrays.asList(addImgResId));
    }

    public void addImgUrl(List<String> imageUrls){
        currentData.addAll(imageUrls);
        if(imageUrls.size()<maxImg){
            if(isAddImgInStart){
                imageUrls.add(0,addImgResId);
            }else {
                imageUrls.add(addImgResId);
            }
            momentPicView.setImageUrls(imageUrls);
        }
    }
}
