package cn.wu1588.dancer.common.dialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import cn.wu1588.dancer.R;
import com.cnsunrun.commonui.widget.roundwidget.QMUIRoundButton;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.base.LBaseDialogFragment;

public class RechageDialogFragment extends LBaseDialogFragment {
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tv_content)
    TextView tvContent;
    @BindView(R.id.cancel)
    QMUIRoundButton cancel;
    @BindView(R.id.submit)
    QMUIRoundButton submit;
    View.OnClickListener onSubmitAction, onCancelAction;
    DialogInterface.OnDismissListener onDismissListener;
    CharSequence titleTxt="温馨提示", contentTxt, leftBtnTxt="取消", rightBtnTxt="确认";

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvTitle.setText(titleTxt);
        tvContent.setText(contentTxt);
        cancel.setText(leftBtnTxt);
        submit.setText(rightBtnTxt);
        if(this.leftBtnTxt==null){
            cancel.setVisibility(View.GONE);
        }
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.dialog_recharge_message;
    }


    public static RechageDialogFragment newInstance() {
        Bundle args = new Bundle();
        RechageDialogFragment fragment = new RechageDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public RechageDialogFragment setOnSubmitAction(View.OnClickListener onSubmitAction) {
        this.onSubmitAction = onSubmitAction;
        return this;
    }

    public RechageDialogFragment setOnCancelAction(View.OnClickListener onCancelAction) {
        this.onCancelAction = onCancelAction;
        return this;
    }

    public RechageDialogFragment setTitleTxt(CharSequence titleTxt) {
        this.titleTxt = titleTxt;
        return this;
    }

    public RechageDialogFragment setContentTxt(CharSequence contentTxt) {
        this.contentTxt = contentTxt;
        return this;
    }

    public RechageDialogFragment setLeftBtnTxt(CharSequence leftBtnTxt) {
        this.leftBtnTxt = leftBtnTxt;
        return this;
    }

    public RechageDialogFragment setRightBtnTxt(CharSequence rightBtnTxt) {
        this.rightBtnTxt = rightBtnTxt;
        return this;
    }

    public RechageDialogFragment
    setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
        this.onDismissListener = onDismissListener;
        return this;
    }


    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if(onDismissListener!=null){
            onDismissListener.onDismiss(dialog);
        }

    }

    @OnClick({R.id.cancel, R.id.submit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancel:
                if (onCancelAction != null) {
                    onCancelAction.onClick(view);
                }
                dismissAllowingStateLoss();
                break;
            case R.id.submit:
                if (onSubmitAction != null) {
                    onSubmitAction.onClick(view);
                }
                dismissAllowingStateLoss();
                break;
        }
    }

}
