package cn.wu1588.dancer.common.model;

import android.content.Context;

import com.my.toolslib.http.utils.NetRequestListenerProxy;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.tencent.mmkv.MMKV;

import java.util.List;

import cn.wu1588.dancer.common.quest.BaseQuestStart;

public class SystemParams {
    public String id;

    public String title;

    public String param;

    public String value;

    public String comment;

    public String is_del;

    public String is_hid;

    public static final String AUTO_PLAY = "autoplay";
    public static final String FREE_PLAY_TIME = "free_play_time";
    public static final String LIST_AUTO_PLAY = "list_auto_play";
    public static final String IS_SHOW_DOWBTN = "is_show_dowBtn";
    public static final String NEED_LOGIN = "need_login";
    public static final String PPVOD_URL = "PPVOD_URL";
    public static final String IS_SHOW_USERINFO = "is_show_userinfo";
    public static final String IS_SHOW_CATE = "is_show_cate";
    public static final String IOS_SHELF = "ios_shelf";
    public static final String ACCESS_KEYID = "accessKeyId";
    public static final String ACCESS_KEY_SECRET = "accessKeySecret";
    public static final String VIP_DISCOUNT = "vip_discount";
    public static final String SELF_AD_OPEN = "self_ad_open";
    public static final String AD_MINIMUM_RECHARGE_AMOUNT = "ad_minimum_recharge_amount";
    public static final String AD_SHOW_COST_EVERY_THOUSAND = "ad_show_cost_every_thousand";
    public static final String AD_TIME_INTERVAL = "ad_time_interval";
    public static final String INSERT_AD_IS_OPEN = "insert_ad_is_open";

    public static void initSystemParams(Context context) {
        BaseQuestStart.getSystemParam(new NetRequestListenerProxy(context) {
            @Override
            public void nofityUpdate(int i, BaseBean baseBean) {
                if (baseBean.status == 1) {
                    MMKV mmkv = MMKV.defaultMMKV();
                    List<SystemParams> paramsList = baseBean.Data();
                    for (SystemParams params : paramsList) {
                        mmkv.encode(params.param, params.value);
                    }
                }
            }
        });

    }


}