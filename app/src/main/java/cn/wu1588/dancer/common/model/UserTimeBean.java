package cn.wu1588.dancer.common.model;

public class UserTimeBean {
    private String sum_time;
    private String credit_time;
    private String user_time;
    private String gold_time;

    public String getSum_time() {
        return sum_time;
    }

    public void setSum_time(String sum_time) {
        this.sum_time = sum_time;
    }

    public String getCredit_time() {
        return credit_time;
    }

    public void setCredit_time(String credit_time) {
        this.credit_time = credit_time;
    }

    public String getUser_time() {
        return user_time;
    }

    public void setUser_time(String user_time) {
        this.user_time = user_time;
    }

    public String getGold_time() {
        return gold_time;
    }

    public void setGold_time(String gold_time) {
        this.gold_time = gold_time;
    }

    @Override
    public String toString() {
        return "UserTimeBean{" +
                "sum_time='" + sum_time + '\'' +
                ", credit_time='" + credit_time + '\'' +
                ", user_time='" + user_time + '\'' +
                ", gold_time='" + gold_time + '\'' +
                '}';
    }
}