//package common.widget;
//
//import android.content.Context;
//import android.graphics.drawable.ColorDrawable;
//import android.os.Build;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.text.TextUtils;
//import android.view.Gravity;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.WindowManager;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.chad.library.adapter.base.BaseQuickAdapter;
//import com.chad.library.adapter.base.common.listener.OnItemClickListener;
//import com.cnsunrun.commonui.widget.FixedPopupWindow;
//import com.cnsunrun.commonui.widget.recyclerview.DivideLineItemDecoration;
//import com.sunrun.sunrunframwork.utils.EmptyDeal;
//import com.zhy.view.flowlayout.FlowLayout;
//import com.zhy.view.flowlayout.TagFlowLayout;
//
//import java.util.List;
//
///**
// * Created by cnsunrun on 2018-03-14.
// */
//
//public class SelectPopupWindow extends FixedPopupWindow {
//    private int currentPosition = 0;
//    private PopupWindowAdapter popupWinAdapter;
//    private RecyclerView recyclerView;
//    private TextView txtTip;
//    public  View rightPopupWindow;
//    private List data;
//    private LeftAdapter leftAdapter;
//    private TagFlowLayout tagFlowLayout;
//    private LayoutInflater inflater;
//    private List<CategoryBean.ChildBean> child;
//    private Context context;
//    private OnTagClickListener onTagClickListener;
//
//    public SelectPopupWindow(Context context, List mDatas, String type, OnItemClickListener onItemClickListener) {
//        super(context);
//        inflater = LayoutInflater.from(context);
//        this.context=context;
//        if (TextUtils.equals(type,Const.THE_LIST_SHOWS)){
//            rightPopupWindow = View.inflate(context, R.layout.new_include_recyclerview, null);
//        }else if (TextUtils.equals(type,Const.THE_FLOWL_SHOWS)){
//            rightPopupWindow = View.inflate(context, R.layout.new_categroy_recyclerview, null);
//        }
//        initViews(context, rightPopupWindow, mDatas, type, onItemClickListener);
//        this.setContentView(rightPopupWindow);
//        this.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
//        this.setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
//        this.setFocusable(true);
//        this.setOutsideTouchable(true);
//        this.update();
//        ColorDrawable dw = new ColorDrawable(0x55000000);
//        this.setBackgroundDrawable(dw);
//        this.setAnimationStyle(R.style.dialogWindowAnim);
//    }
//
//
//    /**
//     * 初始化
//     *
//     * @param context
//     * @param rightPopupWindow
//     * @param mDates
//     * @param onItemClickListener
//     */
//    private void initViews(final Context context, View rightPopupWindow, final List mDates, String type, final OnItemClickListener onItemClickListener) {
//        data =mDates;
//        recyclerView = (RecyclerView) rightPopupWindow.findViewById(R.id.public_recyclerView);
//        txtTip = (TextView) rightPopupWindow.findViewById(R.id.txtTip);
//        tagFlowLayout = (TagFlowLayout) rightPopupWindow.findViewById(R.id.tagFlowLayout);
//        switch (type) {
//            case Const.THE_LIST_SHOWS:
//                popupWinAdapter = new PopupWindowAdapter(mDates, currentPosition);
//                recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayout.VERTICAL, false));
//                recyclerView.addItemDecoration(new DivideLineItemDecoration(context, context.getResources().getColor(R.color.grey_color_e7e7e7), home1));
//                recyclerView.setAdapter(popupWinAdapter);
//                popupWinAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
//                        if (onItemClickListener != null) {
//                            currentPosition = position;
//                            List<ClickBean> clickBean = popupWinAdapter.getData();
//                            if (clickBean.get(position).isChoose()) return;
//                            for (ClickBean clickBeanData : clickBean) {
//                                clickBeanData.setChoose(false);
//                            }
//                            clickBean.get(position).setChoose(true);
//                            popupWinAdapter.notifyDataSetChanged();
//                            onItemClickListener.onSimpleItemClick(adapter, view, position);
//                        }
//                        SelectPopupWindow.this.dismiss();
//                    }
//                });
//                break;
//            case Const.THE_FLOWL_SHOWS:
//                leftAdapter = new LeftAdapter(mDates);
//                recyclerView.setLayoutManager(new LinearLayoutManager(context));
//                recyclerView.setAdapter(leftAdapter);
//                setRightData(0);
//                leftAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
//                        if (onItemClickListener!=null){
//
//                            onItemClickListener.onSimpleItemClick(adapter, view, position);
//                        }
//                        setRightData(position);
//                    }
//                });
//
//                break;
//        }
//
//
//    }
//
//    private void setRightData(int position) {
//        leftAdapter.setPosition(position);
//        if (EmptyDeal.isEmpy(data)) return;
//        CategoryBean categoryBean = (CategoryBean) data.get(position);
//        child = categoryBean.child;
//        tagFlowLayout.setAdapter(new TagAdapter<CategoryBean.ChildBean>(child){
//
//            @Override
//            public View getView(FlowLayout parent, int position, CategoryBean.ChildBean childBean) {
//                TextView tv = (TextView) inflater.inflate(R.layout.item_simple_type_text, tagFlowLayout, false);
//                tv.setText(childBean.title);
//                if (childBean.title.length()<3){
//                    tv.setPadding(45,30,45,30);
//                }
//                return tv;
//            }
//        });
//        tagFlowLayout.setOnTagClickListener(new TagFlowLayout.OnTagClickListener() {
//            @Override
//            public boolean onTagClick(View view, int position, FlowLayout parent) {
//                if (onTagClickListener!=null){
//                    onTagClickListener.onTagClick(position);
//                }
//                return false;
//            }
//        });
//
//    }
//
//    /**
//     * 显示popupWindow
//     *
//     */
//    public void showPopupWindow(View view) {
////        if (!this.isShowing()) {
////            // 以下拉方式显示popupwindow
////            this.showAsDropDown(parent, 0, 0);
////        } else {
////            this.dismiss();
////        }
//        if (this != null && this.isShowing()) {
//            this.dismiss();
//        } else {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                int[] location = new int[2];
//                view.getLocationOnScreen(location);
//                int tempHeight = this.getHeight();
//                if (tempHeight == WindowManager.LayoutParams.MATCH_PARENT || ScreenUtils.getHeight(context) <=
//                        tempHeight) {
//                    this.setHeight(ScreenUtils.getHeight(context) - location[home1] - view.getHeight());
//                }
//                this.showAtLocation(view, Gravity.NO_GRAVITY, location[0], location[home1] + view.getHeight());
//                this.update();
//            } else {
//                if (this != null) {
//                    this.showAsDropDown(view, 0, 0);
//                    this.update();
//                }
//            }
//
//    }
//    }
//     public SelectPopupWindow setOnTagClickListener(OnTagClickListener onTagClickListener){
//        this.onTagClickListener=onTagClickListener;
//        return this;
//
//     }
//    public SelectPopupWindow setLeftAdapterPosition(int position){
//        setRightData(position);
//        return this;
//    }
//}
