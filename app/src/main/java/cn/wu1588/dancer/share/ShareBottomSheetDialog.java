package cn.wu1588.dancer.share;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.my.toolslib.http.utils.LzyResponse;
import com.my.toolslib.http.utils.NetRequestListenerProxy;
import com.my.toolslib.http.utils.OkHttpRequestListener;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.tencent.connect.share.QQShare;
import com.tencent.mm.opensdk.modelmsg.SendMessageToWX;
import com.tencent.mm.opensdk.modelmsg.WXMediaMessage;
import com.tencent.mm.opensdk.modelmsg.WXVideoObject;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.common.event.SharedCompleteEvent;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.BitmapUtils;
import cn.wu1588.dancer.common.util.ConstantValue;
import cn.wu1588.dancer.common.util.SPUtils;
import cn.wu1588.dancer.home.mode.MovieBean;

public class ShareBottomSheetDialog extends BottomSheetDialogFragment {

    public static final String TAG = "ShareBottomSheetDialog";

    private MovieBean mMovieBean;
    private int userId, movieId, imageUrl, movieTitle;
    private Tencent mTencent;
    private IUiListener mQQShareListener = new IUiListener() {
        @Override
        public void onComplete(Object o) {
            Log.d("QQShare", o.toString());
            countSharedComplete();
        }

        @Override
        public void onError(UiError uiError) {
            Log.d("QQShare", uiError.toString());
        }

        @Override
        public void onCancel() {
            Log.d("QQShare", "QQ share canceled!!");
        }
    };

    private IWXAPI mWXApi;

    public static ShareBottomSheetDialog newInstance(MovieBean movieBean) {
        ShareBottomSheetDialog shareBottomSheetDialog = new ShareBottomSheetDialog();
        Bundle bundle = new Bundle();
        bundle.putParcelable("share_content", movieBean);
        shareBottomSheetDialog.setArguments(bundle);
        return shareBottomSheetDialog;
    }

    public static ShareBottomSheetDialog newInstance(int userId, int movieId, int imageUrl, int movieTitle) {
        ShareBottomSheetDialog shareBottomSheetDialog = new ShareBottomSheetDialog();
        Bundle bundle = new Bundle();
//        bundle.putParcelable("share_content", movieBean);
        bundle.putInt("userId", userId);
        bundle.putInt("movieId", movieId);
        bundle.putInt("imageUrl", imageUrl);
        bundle.putInt("movieTitle", movieTitle);
        shareBottomSheetDialog.setArguments(bundle);
        return shareBottomSheetDialog;
    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        mMovieBean = getArguments().getParcelable("share_content");

        userId = getArguments().getInt("userId");
        movieId = getArguments().getInt("movieId");
        imageUrl = getArguments().getInt("imageUrl");
        movieTitle = getArguments().getInt("movieTitle");

        String qqid = (String) SPUtils.get(getContext(), ConstantValue.QQ_ID, "0");
        mTencent = Tencent.createInstance(qqid, this.getContext());
        String wechatid = (String) SPUtils.get(getContext(), ConstantValue.WECHAT_ID, "0");
        mWXApi = WXAPIFactory.createWXAPI(getActivity(), wechatid);
        mWXApi.registerApp(wechatid);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.bottom_sheet_dialog_share, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        ImageButton ibClose = view.findViewById(R.id.ib_close);
        ibClose.setOnClickListener(v -> dismiss());

        TextView tvShareFriend = view.findViewById(R.id.tv_share_friend);
        tvShareFriend.setOnClickListener(v -> weChatShare(0));

        TextView tvShareFriends = view.findViewById(R.id.tv_share_friends);
        tvShareFriends.setOnClickListener(v -> weChatShare(1));

        TextView tvShareQQFriend = view.findViewById(R.id.tv_share_qq_friend);
        tvShareQQFriend.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putString(QQShare.SHARE_TO_QQ_TARGET_URL, mMovieBean.play_url);
            bundle.putString(QQShare.SHARE_TO_QQ_TITLE, mMovieBean.title);
            mTencent.shareToQQ(getActivity(), bundle, mQQShareListener);
        });

        TextView tvShareCollect = view.findViewById(R.id.tv_share_collec);
        tvShareCollect.setOnClickListener(v -> BaseQuestStart.movieShoucang(new NetRequestListenerProxy(getContext()) {
            @Override
            public void nofityUpdate(int i, BaseBean baseBean) {
                UIUtils.shortM(baseBean.msg);
            }
        }, mMovieBean.id + ""));

        TextView tvShareReport = view.findViewById(R.id.tv_share_report);
        tvShareReport.setOnClickListener(v -> CommonIntent.startMovieFeedback(getActivity(), mMovieBean.id + ""));
    }

    private void weChatShare(int scene) {
        WXVideoObject video = new WXVideoObject();
        video.videoUrl = mMovieBean.play_url;
        video.videoLowBandUrl = mMovieBean.image;


        //用 WXWebpageObject 对象初始化一个 WXMediaMessage 对象
        WXMediaMessage msg = new WXMediaMessage(video);
        msg.title = mMovieBean.title;
        msg.description = mMovieBean.title;
        Bitmap thumbBmp = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher_round);
        msg.thumbData = BitmapUtils.compressImage(thumbBmp);

        //构造一个Req
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = System.currentTimeMillis() + "-webpage";
        req.message = msg;
        req.scene = scene;

        //调用api接口，发送数据到微信
        mWXApi.sendReq(req);
    }

    @Subscribe
    public void onSharedComplete(SharedCompleteEvent event) {
        switch (event.getType()) {
            case SharedCompleteEvent.SharedType.QQ:
                Tencent.handleResultData(event.getData(), mQQShareListener);
                break;
            case SharedCompleteEvent.SharedType.WX:
                countSharedComplete();
                break;
            default:
                // ignore
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void countSharedComplete() {
        BaseQuestStart.updateMovieShareNum(new OkHttpRequestListener() {
            @Override
            public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
                if (response.code == 1) {
                    UIUtils.shortM("分享成功");
                } else {
                    UIUtils.shortM(response.msg);
                }
            }
        }, mMovieBean.id + "");
    }
}


