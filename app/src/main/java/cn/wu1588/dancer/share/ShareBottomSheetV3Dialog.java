package cn.wu1588.dancer.share;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.blankj.utilcode.util.ToastUtils;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.kongzue.baseokhttp.HttpRequest;
import com.kongzue.baseokhttp.listener.ResponseListener;
import com.kongzue.baseokhttp.util.JsonMap;
import com.kongzue.baseokhttp.util.Parameter;
import com.my.toolslib.http.utils.LzyResponse;
import com.my.toolslib.http.utils.NetRequestListenerProxy;
import com.my.toolslib.http.utils.OkHttpRequestListener;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.tencent.connect.share.QQShare;
import com.tencent.mm.opensdk.modelmsg.SendMessageToWX;
import com.tencent.mm.opensdk.modelmsg.WXMediaMessage;
import com.tencent.mm.opensdk.modelmsg.WXVideoObject;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.common.event.SharedCompleteEvent;
import cn.wu1588.dancer.common.quest.BaseQuestConfig;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.common.util.BitmapUtils;
import cn.wu1588.dancer.common.util.ConstantValue;
import cn.wu1588.dancer.common.util.SPUtils;
import cn.wu1588.dancer.home.mode.MovieBean;

public class ShareBottomSheetV3Dialog extends BottomSheetDialogFragment {

    public static final String TAG = "ShareBottomSheetDialog";

    private MovieBean mMovieBean;
    private String userId, movieId, imageUrl, movieTitle,play_url;
    private Tencent mTencent;
    private IUiListener mQQShareListener = new IUiListener() {
        @Override
        public void onComplete(Object o) {
            Log.d("QQShare", o.toString());
            countSharedComplete();
        }

        @Override
        public void onError(UiError uiError) {
            Log.d("QQShare", uiError.toString());
        }

        @Override
        public void onCancel() {
            Log.d("QQShare", "QQ share canceled!!");
        }
    };

    private IWXAPI mWXApi;

    public static ShareBottomSheetV3Dialog newInstance(MovieBean movieBean) {
        ShareBottomSheetV3Dialog shareBottomSheetDialog = new ShareBottomSheetV3Dialog();
        Bundle bundle = new Bundle();
        bundle.putParcelable("share_content", movieBean);
        shareBottomSheetDialog.setArguments(bundle);
        return shareBottomSheetDialog;
    }

    public static ShareBottomSheetV3Dialog newInstance(String userId, String movieId, String imageUrl, String movieTitle,String play_url) {
        ShareBottomSheetV3Dialog shareBottomSheetDialog = new ShareBottomSheetV3Dialog();
        Bundle bundle = new Bundle();
//        bundle.putParcelable("share_content", movieBean);
        bundle.putString("userId", userId);
        bundle.putString("movieId", movieId);
        bundle.putString("imageUrl", imageUrl);
        bundle.putString("movieTitle", movieTitle);
        bundle.putString("play_url", play_url);
        shareBottomSheetDialog.setArguments(bundle);
        return shareBottomSheetDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        mMovieBean = getArguments().getParcelable("share_content");

        userId = getArguments().getString("userId");
        movieId = getArguments().getString("movieId");
        imageUrl = getArguments().getString("imageUrl");
        movieTitle = getArguments().getString("movieTitle");
        play_url = getArguments().getString("play_url");

        String qqid = (String) SPUtils.get(getContext(), ConstantValue.QQ_ID, "0");
        mTencent = Tencent.createInstance(qqid, this.getContext());
        String wechatid = (String) SPUtils.get(getContext(), ConstantValue.WECHAT_ID, "0");
        mWXApi = WXAPIFactory.createWXAPI(getActivity(), wechatid);
        mWXApi.registerApp(wechatid);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.bottom_sheet_dialog_share_v3, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        ImageButton ibClose = view.findViewById(R.id.ib_close);
        ibClose.setOnClickListener(v -> dismiss());

        TextView tvShareFriend = view.findViewById(R.id.tv_share_friend);
        tvShareFriend.setOnClickListener(v -> weChatShare(0));

        TextView tvShareFriends = view.findViewById(R.id.tv_share_friends);
        tvShareFriends.setOnClickListener(v -> weChatShare(1));

        TextView tvShareQQFriend = view.findViewById(R.id.tv_share_qq_friend);
        tvShareQQFriend.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putString(QQShare.SHARE_TO_QQ_TARGET_URL, play_url);
            bundle.putString(QQShare.SHARE_TO_QQ_TITLE, movieTitle);
            mTencent.shareToQQ(getActivity(), bundle, mQQShareListener);
        });

//        TextView tvShareCollect = view.findViewById(R.id.tv_share_collec);
//        tvShareCollect.setOnClickListener(v -> BaseQuestStart.movieShoucang(new NetRequestListenerProxy(getContext()) {
//            @Override
//            public void nofityUpdate(int i, BaseBean baseBean) {
//                UIUtils.shortM(baseBean.msg);
//            }
//        }, mMovieBean.id + ""));

//        TextView tvShareReport = view.findViewById(R.id.tv_share_report);
//        tvShareReport.setOnClickListener(v -> CommonIntent.startMovieFeedback(getActivity(), mMovieBean.id + ""));
    }

    private void weChatShare(int scene) {
        WXVideoObject video = new WXVideoObject();
        video.videoUrl = play_url;
        video.videoLowBandUrl = imageUrl;


        //用 WXWebpageObject 对象初始化一个 WXMediaMessage 对象
        WXMediaMessage msg = new WXMediaMessage(video);
        msg.title = movieTitle;
        msg.description = movieTitle;
        Bitmap thumbBmp = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher_round);
        msg.thumbData = BitmapUtils.compressImage(thumbBmp);

        //构造一个Req
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = System.currentTimeMillis() + "-webpage";
        req.message = msg;
        req.scene = scene;

        //调用api接口，发送数据到微信
        mWXApi.sendReq(req);
    }

    @Subscribe
    public void onSharedComplete(SharedCompleteEvent event) {
        switch (event.getType()) {
            case SharedCompleteEvent.SharedType.QQ:
                Tencent.handleResultData(event.getData(), mQQShareListener);
                break;
            case SharedCompleteEvent.SharedType.WX:
                countSharedComplete();
                break;
            default:
                // ignore
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void countSharedComplete() {
        BaseQuestStart.updateMovieShareNum(new OkHttpRequestListener() {
            @Override
            public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
                if (response.code == 1) {
                    UIUtils.shortM("分享成功");

                } else {
                    UIUtils.shortM(response.msg);
                }
            }
        }, movieId + "");

        getForwordRecord();

    }



    /**
     * 用户的分享记录
     */
    private void getForwordRecord() {
//        user_id	是	int	用户id
//        movie_id	是	int	视频id
//        image_url	是	int	封面图片地址
//        title	是	int	视频标题
            HttpRequest.POST(getActivity(),
                    BaseQuestConfig.GET_NEW_FORWARD_RECORD,
                    new Parameter()
                            .add("user_id", Integer.parseInt(Config.getLoginInfo().uid))
                            .add("movie_id", Integer.parseInt(movieId))
                            .add("image_url", Integer.parseInt(imageUrl))
                            .add("title", Integer.parseInt(movieTitle)),
                    new ResponseListener() {
                        @Override
                        public void onResponse(String main, Exception error) {
                            if(error == null){
                                JsonMap jsonMap = JsonMap.parse(main);
                                if(jsonMap.get("status").equals("1")){
                                    if(jsonMap.getString("msg").equals("success")){
                                        String data = jsonMap.getString("data");
                                        ToastUtils.showShort("分享已经记录");
                                    }
                                }
                            }
                        }
                    });

    }

}


