package cn.wu1588.dancer;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.kongzue.baseokhttp.HttpRequest;
import com.kongzue.baseokhttp.listener.ResponseListener;
import com.kongzue.baseokhttp.util.Parameter;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.sunrun.sunrunframwork.utils.Utils;
import com.unistrong.yang.zb_permission.ZbPermission;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import cn.wu1588.dancer.channel.mode.SettingInfo;
import cn.wu1588.dancer.adp.MyPagerAdapter;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.config.RongIMHelper;
import cn.wu1588.dancer.common.event.RefreshIMTokenEvent;
import cn.wu1588.dancer.common.event.SharedCompleteEvent;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.common.util.DeviceIdUtil;
import cn.wu1588.dancer.common.util.PushHelper;
import cn.wu1588.dancer.discover.fragment.DiscoverFragment;
import cn.wu1588.dancer.fgt.subscribe.SubscribeFgt;
import cn.wu1588.dancer.fgt.teaching.TeachingFgt;
import cn.wu1588.dancer.home.fragment.SmallVideoFragment;
import io.rong.imkit.RongIM;
import io.rong.imlib.RongIMClient;
import cn.wu1588.dancer.mine.fragment.MineFragmentV3;

import static cn.wu1588.dancer.common.config.Const.PERMISSION;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.LOGIN_LOG;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.SETTING_INFO_CODE;
import static io.rong.imlib.RongIMClient.ConnectionStatusListener.ConnectionStatus.CONNECTED;
import static io.rong.imlib.RongIMClient.ConnectionStatusListener.ConnectionStatus.CONNECTING;


/**
 * Created by wangchao on 2018-07-25.
 * 主导航页
 */
public class MainActivity extends LBaseActivity implements RadioGroup.OnCheckedChangeListener, ViewPager.OnPageChangeListener {
    private List<Fragment> baseFragments;
    private ViewPager mViewPager;
    private RadioGroup mainRgp;
    private RadioButton rb_home, rb_video, rb_teaching, rb_subscribe, rb_my;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setDark(CommonApp.getApplication().isBarTvColor());//状态栏文字颜色
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigator);
        Config.deviceId = DeviceIdUtil.getDeviceId(this);

        List<String> list = new ArrayList<>();
        list.add("1");
        mViewPager = findViewById(R.id.vp_main_center);
        mainRgp = findViewById(R.id.main_rgp);
        rb_home = findViewById(R.id.rb_home);
        rb_video = findViewById(R.id.rb_video);
        rb_teaching = findViewById(R.id.rb_teaching);
        rb_subscribe = findViewById(R.id.rb_subscribe);
        rb_my = findViewById(R.id.rb_my);
        baseFragments = new ArrayList<>();
        baseFragments.add(DiscoverFragment.newInstance());
        baseFragments.add(SmallVideoFragment.newInstance());
        baseFragments.add(TeachingFgt.newInstance());
//        baseFragments.add(AlumFragment.newInstance());
        baseFragments.add(SubscribeFgt.newInstance());
        baseFragments.add(MineFragmentV3.newInstance());
//        另一个主页面
//        HomeNewFragment
        //数字专辑
//        AlumFragment
        //定义底部标签图片大小
        Drawable drawableFirst = getResources().getDrawable(R.drawable.menu_home_selector);
        drawableFirst.setBounds(0, 0, 69, 69);//第一0是距左右边距离，第二0是距上下边距离，第三69长度,第四宽度
        rb_home.setCompoundDrawables(null, drawableFirst, null, null);//只放上面
        Drawable drawableFirst2 = getResources().getDrawable(R.drawable.menu_video_selector);
        drawableFirst2.setBounds(0, 0, 69, 69);//第一0是距左右边距离，第二0是距上下边距离，第三69长度,第四宽度
        rb_video.setCompoundDrawables(null, drawableFirst2, null, null);//只放上面
        Drawable drawableFirst3 = getResources().getDrawable(R.drawable.menu_teaching_selector);
        drawableFirst3.setBounds(0, 0, 69, 69);//第一0是距左右边距离，第二0是距上下边距离，第三69长度,第四宽度
        rb_teaching.setCompoundDrawables(null, drawableFirst3, null, null);//只放上面
        Drawable drawableFirst4 = getResources().getDrawable(R.drawable.menu_subscribe_selector);
        drawableFirst4.setBounds(0, 0, 69, 69);//第一0是距左右边距离，第二0是距上下边距离，第三69长度,第四宽度
        rb_subscribe.setCompoundDrawables(null, drawableFirst4, null, null);//只放上面
        Drawable drawableFirst5 = getResources().getDrawable(R.drawable.menu_my_selector);
        drawableFirst5.setBounds(0, 0, 69, 69);//第一0是距左右边距离，第二0是距上下边距离，第三69长度,第四宽度
        rb_my.setCompoundDrawables(null, drawableFirst5, null, null);//只放上面

        MyPagerAdapter adapter = new MyPagerAdapter(getSupportFragmentManager(), baseFragments, this);
        mViewPager.setAdapter(adapter);
        mainRgp.check(R.id.rb_home);
        mViewPager.setCurrentItem(0);
        mViewPager.setOffscreenPageLimit(baseFragments.size());
        mainRgp.setOnCheckedChangeListener(this);
        mViewPager.addOnPageChangeListener(this);
        // 处理因为低内存产生的一些问题
        @SuppressLint("RestrictedApi")
        List<Fragment> fs = getSupportFragmentManager().getFragments();
        if (fs != null) {
            for (Fragment fragment2 : fs) {
                getSupportFragmentManager().beginTransaction().remove(fragment2)
                        .commitAllowingStateLoss();
            }
        }
        if (Config.getLoginInfo().isValid()) {
            RongIMHelper.connect(Config.getLoginInfo().r_token);
            PushHelper.initPush(this.that);
        }
        RongIMClient.ConnectionStatusListener.ConnectionStatus currentConnectionStatus = RongIM.getInstance().getCurrentConnectionStatus();
        if (currentConnectionStatus != CONNECTED || currentConnectionStatus != CONNECTING) {
            RongIMHelper.connectFocus();
        }
        if (!RongIMHelper.isConnect()) {
            EventBus.getDefault().post(RefreshIMTokenEvent.newInstance());//刷新Token
        }
        ZbPermission.with(this.that)
                .addRequestCode(PERMISSION)
                .permissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.INTERNET)
                .request();
//        IntentFilter intentFilter = new IntentFilter();
//        intentFilter.addAction("android.intent.action.to.mine");
//        receiver = new RefreshDataReceiver();
//        registerReceiver(receiver, intentFilter);
        getHttp();
    }

    private void getHttp() {
        if (Config.getLoginInfo() != null) {
            if (Config.getLoginInfo().uid != null) {
                HttpRequest.POST(this, LOGIN_LOG, new Parameter()
                                .add("usdrid", Config.getLoginInfo().uid)
                                .add("log_type", "1")
                                .add("device", Config.deviceId)
                        , new ResponseListener() {
                            @Override
                            public void onResponse(String main, Exception error) {

                            }
                        });
            }
        }
    }


//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void refreshIMToken(RefreshIMTokenEvent refreshIMTokenEvent) {
//        if (!isConnect()) {
////            UIUtils.showLoadDialog(that,"加载中...");
////            BaseQuestStart.NeighborhoodImGetToken(this);
//        }
//    }

    @Override
    protected void onResume() {
        super.onResume();
        PushHelper.updateAlias(that);
        BaseQuestStart.getSettingInfo(that);
    }


    /**
     * 设置标签视图内容,
     * bitmap
     */
    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode) {
            case SETTING_INFO_CODE:
                List<SettingInfo> list = bean.Data();
                if (list != null && !list.isEmpty()) {
                    for (SettingInfo info : list) {
                        Config.putData(info.param, info.value);
                    }
                }
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.rb_home:
                mViewPager.setCurrentItem(0);
                break;
            case R.id.rb_video:
                mViewPager.setCurrentItem(1);
                break;
            case R.id.rb_teaching:
                mViewPager.setCurrentItem(2);
                break;
            case R.id.rb_subscribe:
                mViewPager.setCurrentItem(3);
                break;
            case R.id.rb_my:
                mViewPager.setCurrentItem(4);
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        switch (position) {
            case 0:
                mainRgp.check(R.id.rb_home);
                break;
            case 1:
                mainRgp.check(R.id.rb_video);
                break;
            case 2:
                mainRgp.check(R.id.rb_teaching);
                break;
            case 3:
                mainRgp.check(R.id.rb_subscribe);
                break;
            case 4:
                mainRgp.check(R.id.rb_my);
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // qq 分享 requestcode 10103
        if (requestCode == 10103) {
            EventBus.getDefault().post(new SharedCompleteEvent(SharedCompleteEvent.SharedType.QQ, data));
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = baseFragments.get(mViewPager.getCurrentItem());
        if (fragment instanceof DiscoverFragment && ((DiscoverFragment) fragment).onBackPressed()) {
            return;
        }
        if (!Utils.isQuck(2000)) {
            UIUtils.shortM("再按一次退出程序");
        } else {
            finish();
        }
    }

}