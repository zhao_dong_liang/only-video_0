package cn.wu1588.dancer.mine.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import cn.wu1588.dancer.R;

import com.bilibili.boxing.Boxing;
import com.bilibili.boxing.model.config.BoxingConfig;
import com.bilibili.boxing.model.entity.BaseMedia;
import com.bilibili.boxing.model.entity.impl.ImageMedia;
import com.bilibili.boxing.utils.ImageCompressor;
import com.bilibili.boxing_impl.ui.BoxingActivity;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.cnsunrun.commonui.widget.roundwidget.QMUIRoundButton;
import com.sunrun.sunrunframwork.adapter.SelectableAdapter;
import com.sunrun.sunrunframwork.adapter.ViewHodler;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.PictureShow;
import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.sunrun.sunrunframwork.utils.EmptyDeal;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.mine.adapter.FeedBackAdp;
import cn.wu1588.dancer.mine.mode.ProblemBean;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.widget.MomentPicView;
import cn.wu1588.dancer.common.widget.UploadPicView;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.COMM_PROBLEM_LIST_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.POST_FEED_BACK_CODE;

/**
 * 意见反馈
 */
public class FeedBackFragment extends LBaseFragment {


    @BindView(R.id.editText)
    EditText editText;
    @BindView(R.id.imgPics)
    UploadPicView imgPics;
    @BindView(R.id.btnSubmit)
    QMUIRoundButton btnSubmit;
    @BindView(R.id.opinion_rv)
    RecyclerView opinion_rv;
    private SelectableAdapter<ProblemBean.FeedbackCategoryBean> selectableAdapter;
    private PictureShow pictureShow;
    public static final int IMG_REQUET_CODE = 0x010;
    FeedBackAdp feedBackAdp;

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_feed_back;
    }

    public static FeedBackFragment newInstance() {
        FeedBackFragment feedBackFragment = new FeedBackFragment();
        Bundle bundle = new Bundle();
        feedBackFragment.setArguments(bundle);
        return feedBackFragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        initListener();
        initData();
    }

    private void initData() {
        BaseQuestStart.getCommProblemList(FeedBackFragment.this);

    }

    private void initListener() {
        feedBackAdp.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                List<ProblemBean.FeedbackCategoryBean> feedbackCategory = adapter.getData();
                for (int i = 0; i < feedbackCategory.size(); i++) {
                    feedbackCategory.get(i).switechBean = i == position;
                }
                feedBackAdp.notifyDataSetChanged();
            }
        });


        imgPics.setOnAddBtnListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BoxingConfig mulitImgConfig = new BoxingConfig(BoxingConfig.Mode.MULTI_IMG)
                        .needCamera()
                        .needGif()
                        .withMaxCount(9);
                Boxing.of(mulitImgConfig).
                        withIntent(FeedBackFragment.this.getContext(), BoxingActivity.class).
                        start(FeedBackFragment.this, 0);
            }
        });

        imgPics.setOnClickItemListener(new MomentPicView.OnClickItemListener() {
            @Override
            public void onClick(int i, ArrayList<String> url) {
                pictureShow.setArgment(imgPics.getImageFilePaths(), i);
                pictureShow.show();
            }
        });
    }

    private void initViews() {
        pictureShow = new PictureShow(getActivity());
        opinion_rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        feedBackAdp = new FeedBackAdp(R.layout.item_feed_back);
        opinion_rv.setAdapter(feedBackAdp);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        ArrayList<BaseMedia> images = Boxing.getResult(data);
        ImageCompressor compressor = new ImageCompressor(getActivity());
        if (images != null) {
            ArrayList<String> imgPath = new ArrayList<>();
            for (BaseMedia image : images) {
                ImageMedia imageMedia = (ImageMedia) image;
                imageMedia.compress(compressor);
                imgPath.add(imageMedia.getThumbnailPath());
            }
            imgPics.setImageUrls(imgPath);
        }
    }

    @OnClick(R.id.btnSubmit)
    public void onViewClicked() {
        String content = editText.getText().toString();
        if (EmptyDeal.isEmpy(content)) {
            UIUtils.shortM("请填写问题描述");
            return;
        }
        if (content.length() < 10) {
            UIUtils.shortM("请填写内容大于10个字符");
            return;
        }
        String id = null;
        List<ProblemBean.FeedbackCategoryBean> feedbackCategory = feedBackAdp.getData();
        for (int i = 0; i < feedbackCategory.size(); i++) {
            if (feedbackCategory.get(i).switechBean) {
                id = feedbackCategory.get(i).id;
            }
        }
        if (id == null) {
            UIUtils.shortM("请选择反馈类型");
            return;
        }
        BaseQuestStart.postFeedBack(FeedBackFragment.this, content, id);
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode) {
            case COMM_PROBLEM_LIST_CODE:
                if (bean.status == 1) {
                    ProblemBean problemBean = bean.Data();
                    List<ProblemBean.FeedbackCategoryBean> feedback_category = problemBean.feedback_category;
//                    updateUI(feedback_category);
                    for (int i = 0; i < feedback_category.size(); i++) {
                        feedback_category.get(i).switechBean = false;
                    }
                    feedBackAdp.setNewData(feedback_category);
                }
                break;
            case POST_FEED_BACK_CODE:
                if (bean.status == 1) {
                    UIUtils.shortM(bean.msg);
                    getActivity().finish();
                } else {
                    UIUtils.shortM(bean.Data().toString());
                }
                break;


        }
        super.nofityUpdate(requestCode, bean);
    }

    private void updateUI(List<ProblemBean.FeedbackCategoryBean> feedback_category) {
        selectableAdapter = new SelectableAdapter<ProblemBean.FeedbackCategoryBean>(that, feedback_category, R.layout.item_feed_back) {

            @Override
            public void fillView(ViewHodler viewHodler, ProblemBean.FeedbackCategoryBean s, int position) {
                View layBg = viewHodler.getView(R.id.layBg);
                CheckBox checkBox = viewHodler.getView(R.id.checkbox);
                checkBox.setChecked(isSelected(position));
                checkBox.setText(s.title);
                layBg.setBackgroundResource(isSelected(position) ? R.drawable.shape_proble_bg2 : R.drawable.shape_proble_bg);
            }
        };
        selectableAdapter.select(0);
//        gridMoneyType.setAdapter(selectableAdapter);
//        gridMoneyType.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                selectableAdapter.select(position);
//            }
//        });
    }

}
