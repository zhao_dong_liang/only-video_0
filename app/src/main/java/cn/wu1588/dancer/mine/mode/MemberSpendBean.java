package cn.wu1588.dancer.mine.mode;

public class MemberSpendBean {

    /**
     * vip_id : 1
     * money : 20.00
     * add_time : 2019-04-19 09:14:31
     * vip : {"id":"1","title":"月卡","day":"30","discount_price":"5.00","price":"10.00"}
     */

    public String vip_id;
    public String money;
    public String add_time;
    public VipBean vip;


    public static class VipBean {
        /**
         * id : 1
         * title : 月卡
         * day : 30
         * discount_price : 5.00
         * price : 10.00
         */

        public String id;
        public String title;
        public String day;
        public String discount_price;
        public String price;
    }
}
