package cn.wu1588.dancer.mine.fragment;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.utils.EmptyDeal;

import java.util.List;

import butterknife.BindView;
import cn.wu1588.dancer.mine.adapter.MsgAdapter;
import cn.wu1588.dancer.mine.adapter.NoticeAdapter;
import cn.wu1588.dancer.mine.mode.MsgBean;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.GetEmptyViewUtils;
import cn.wu1588.dancer.common.util.PageLimitDelegate;
import cn.wu1588.dancer.common.widget.MyItemDecoration;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_MSG_LIST_CODE;

public class NoticeFragment extends LBaseFragment {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    private NoticeAdapter noticeAdapter;
    private String type;
    PageLimitDelegate pageLimitDelegate = new PageLimitDelegate(new PageLimitDelegate.DataProvider() {
        @Override
        public void loadData(int page) {
            BaseQuestStart.getMsgList(NoticeFragment.this, type, page);
        }
    });
    private MsgAdapter msgAdapter;

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_common_problem;
    }

    public static NoticeFragment newInstance(String type) {
        NoticeFragment noticeFragment = new NoticeFragment();
        Bundle bundle = new Bundle();
        bundle.putString("type", type);
        noticeFragment.setArguments(bundle);
        return noticeFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getString("type");
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }

    private void initViews() {
        refreshLayout.setColorSchemeColors(getResources().getColor(R.color.main_button_color));
        recyclerView.setLayoutManager(new LinearLayoutManager(that));
        noticeAdapter = new NoticeAdapter();
        msgAdapter = new MsgAdapter();
        if ("1".equals(type)) {
            recyclerView.setAdapter(noticeAdapter);
            pageLimitDelegate.attach(refreshLayout, recyclerView, noticeAdapter);
            noticeAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                    MsgBean item = noticeAdapter.getItem(position);
                    CommonIntent.startWebActivity(that, item.id,"");
                }
            });

        } else {
            recyclerView.setAdapter(msgAdapter);
            pageLimitDelegate.attach(refreshLayout, recyclerView, msgAdapter);

            msgAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                    MsgBean item = msgAdapter.getItem(position);
                    CommonIntent.startNoticeDetailActivity(that,item.id,item.comment_id);
                }
            });

        }
        recyclerView.addItemDecoration(new MyItemDecoration(that, R.color.gray_color_f5f5, R.dimen.dp_1));

    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode) {
            case GET_MSG_LIST_CODE:
                if (bean.status == 1) {
                    List<MsgBean> msgBeans = bean.Data();
                    pageLimitDelegate.setData(msgBeans);
                    if (EmptyDeal.isEmpy(msgBeans)) {
                        GetEmptyViewUtils.bindEmptyView(that, noticeAdapter, R.drawable.ic_empty_message, "暂无数据", true);
                        GetEmptyViewUtils.bindEmptyView(that, msgAdapter, R.drawable.ic_empty_message, "暂无数据", true);
                    }
                } else {
                    GetEmptyViewUtils.bindEmptyView(that, noticeAdapter, R.drawable.ic_empty_message, "暂无数据", true);
                    GetEmptyViewUtils.bindEmptyView(that, msgAdapter, R.drawable.ic_empty_message, "暂无数据", true);
                }
                break;
        }
        super.nofityUpdate(requestCode, bean);
    }
}
