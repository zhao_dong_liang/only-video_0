package cn.wu1588.dancer.mine.fragment;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.utils.EmptyDeal;

import java.util.List;

import butterknife.BindView;
import cn.wu1588.dancer.mine.adapter.MyFeedBackAdapter;
import cn.wu1588.dancer.mine.adapter.ProblemAdapter;
import cn.wu1588.dancer.mine.mode.MyFeedBackBean;
import cn.wu1588.dancer.mine.mode.ProblemBean;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.GetEmptyViewUtils;
import cn.wu1588.dancer.common.widget.MyItemDecoration;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.COMM_PROBLEM_LIST_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_MY_FEEDBACK_CODE;

/**
 * 常见问题 type=1;
 * 我的反馈 type=2
 */
public class CommonProblemFragment extends LBaseFragment {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    private int type;
    private ProblemAdapter problemAdapter;
    private MyFeedBackAdapter myFeedBackAdapter;

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_common_problem;
    }

    public static CommonProblemFragment newInstance(int type) {
        CommonProblemFragment commonProblemFragment = new CommonProblemFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        commonProblemFragment.setArguments(bundle);
        return commonProblemFragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getInt("type");
        }
        initViews();
        initData();
    }

    private void initData() {
        //常见问题 type=1;
        // 我的反馈 type=2
        if (type == 1) {
            BaseQuestStart.getCommProblemList(CommonProblemFragment.this);
        } else if (type == 2) {
            BaseQuestStart.getMyFeedBackList(CommonProblemFragment.this);
        }
    }

    private void initViews() {
        refreshLayout.setColorSchemeColors(getResources().getColor(R.color.main_button_color));
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initData();
                refreshLayout.setRefreshing(false);
            }
        });
        problemAdapter = new ProblemAdapter();
        myFeedBackAdapter = new MyFeedBackAdapter();

        recyclerView.setLayoutManager(new LinearLayoutManager(that));
        if (type == 1) {
            recyclerView.setAdapter(problemAdapter);
            recyclerView.addItemDecoration(new MyItemDecoration(that, R.color.gray_color_f5f5, R.dimen.dp_1));
        } else if (type == 2) {
            recyclerView.setAdapter(myFeedBackAdapter);
        }
        problemAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                CommonIntent.startQuestionDeatilActvity(that,problemAdapter.getItem(position).id);
            }
        });
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode) {
            case COMM_PROBLEM_LIST_CODE:
                if (bean.status==1){
                    ProblemBean problemBean=bean.Data();
                    List<ProblemBean.QuestionlistBean> questionlist = problemBean.questionlist;
                    problemAdapter.setNewData(questionlist);
                }
                break;
            case GET_MY_FEEDBACK_CODE:
                if (bean.status==1){
                    List<MyFeedBackBean> feedBackBeans=bean.Data();
                    myFeedBackAdapter.setNewData(feedBackBeans);
                    if (EmptyDeal.isEmpy(feedBackBeans)){
                        GetEmptyViewUtils.bindEmptyView(that, myFeedBackAdapter, R.drawable.ic_empty_message, "暂无反馈", true);
                    }
                }
                break;

        }
        super.nofityUpdate(requestCode, bean);
    }
}
