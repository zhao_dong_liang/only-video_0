package cn.wu1588.dancer.mine.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.my.toolslib.http.utils.NetRequestListenerProxy;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;
import com.sunrun.sunrunframwork.uiutils.UIUtils;

import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.utils.VerificationTimer;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.quest.BaseQuestStart;

//实名认证
public class UserAuthActivity extends LBaseActivity {
    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_id)
    EditText etId;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_verificationv_code)
    EditText etVerificationvCode;
    @BindView(R.id.text_get_verification_code)
    TextView textGetVerificationCode;


    private VerificationTimer verificationTimer;


    public static void start(Context context) {
        Intent intent = new Intent(context, UserAuthActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_auth);
        verificationTimer = new VerificationTimer(60000, 1000, textGetVerificationCode);
    }

    /**
     * 获取状态栏高度
     *
     * @param context
     * @return
     */
    public int getStatusBarHeight(Context context) {
        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        int height = resources.getDimensionPixelSize(resourceId);
        return height;
    }

    @OnClick({R.id.btn_submit, R.id.text_get_verification_code})
    void submit(View view) {
        switch (view.getId()) {
            case R.id.btn_submit:
                if (TextUtils.isEmpty(etName.getEditableText())) {
                    UIUtils.shortM("请输入真实姓名");
                    return;
                }
                if (TextUtils.isEmpty(etId.getEditableText())) {
                    UIUtils.shortM("请输入身份证号");
                    return;
                }
                if (TextUtils.isEmpty(etPhone.getEditableText())) {
                    UIUtils.shortM("请输入手机号");
                    return;
                }
                if (!etPhone.getEditableText().toString().matches("^((13[0-9])|(15[^4,\\D])|(18[0,1-9])|(17[0-9])|(14[0-9])|(19[0-9])|(16[0-9]))\\d{8}$")) {
                    ToastUtils.shortToast("请输入正确的手机格式");
                    return;
                }
                if (TextUtils.isEmpty(etVerificationvCode.getEditableText())) {
                    UIUtils.shortM("请输入验证码");
                    return;
                }
                BaseQuestStart.userAuth(
                        etName.getEditableText().toString().trim(),
                        etId.getEditableText().toString().trim(),
                        etPhone.getEditableText().toString().trim(),
                        etVerificationvCode.getEditableText().toString().trim(),
                        new NetRequestListenerProxy(this) {
                            @Override
                            public void nofityUpdate(int i, BaseBean baseBean) {
                                if (baseBean.status == 1) {
                                    UIUtils.shortM("提交成功");
                                    finish();
                                } else {
                                    UIUtils.shortM(baseBean.msg);
                                }
                            }
                        });
                break;
            case R.id.text_get_verification_code:
                //获取验证码
                BaseQuestStart.getCode(etPhone.getEditableText().toString().trim(),
                        new NetRequestListenerProxy(this) {
                    @Override
                    public void nofityUpdate(int i, BaseBean baseBean) {
                        super.nofityUpdate(i, baseBean);
                        if (baseBean.status == 1) {
                            verificationTimer.start();
                        }
                        UIUtils.shortM(baseBean.msg);
                    }
                });
                break;
        }

    }
}
