package cn.wu1588.dancer.mine.activity;

import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.my.toolslib.http.utils.LzyResponse;

import java.util.List;

import butterknife.BindView;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.PageLimitDelegate;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;
import cn.wu1588.dancer.mine.adapter.MyDownloadAdapter;
import cn.wu1588.dancer.mine.mode.DownloadBean;

public class MyDownloadActivity extends LBaseActivity {

    @BindView(R.id.title_bar)
    TitleBar title_bar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private MyDownloadAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        setDark(CommonApp.getApplication().isBarTvColor());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_download);
        initViews();
    }
    PageLimitDelegate<DownloadBean> pageLimitDelegate=new PageLimitDelegate<>(page -> BaseQuestStart.getMyDownload(MyDownloadActivity.this,page));
    private void initViews(){
//        title_bar.setTitleBarBackgroundDrawable(CommonApp.getApplication().getStateBarDrawable());
        recyclerView.setLayoutManager(new LinearLayoutManager(that));
        adapter=new MyDownloadAdapter();
        recyclerView.setAdapter(adapter);
        pageLimitDelegate.attach(null,recyclerView,adapter);
        adapter.setOnItemClickListener((adapter, view, position) -> {
            DownloadBean bean= (DownloadBean) adapter.getItem(position);
            CommonIntent.startMoviePlayerActivity(that,bean.getId());
        });
    }

    @Override
    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
        super.nofityUpdateUi(requestCode, response, view);
        if (response.code==1){
            pageLimitDelegate.setData((List<DownloadBean>) response.data);
        }
    }
}
