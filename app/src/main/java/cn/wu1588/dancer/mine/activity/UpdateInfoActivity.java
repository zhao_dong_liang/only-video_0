package cn.wu1588.dancer.mine.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.cnsunrun.commonui.widget.roundwidget.QMUIRoundButton;
import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.sunrun.sunrunframwork.utils.EmptyDeal;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;

/**
 * 更新昵称
 */
public class UpdateInfoActivity extends LBaseActivity {

    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.editTitle)
    EditText editTitle;
    @BindView(R.id.btnSure)
    QMUIRoundButton btnSure;
    private String title;
    private String content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setDark(CommonApp.getApplication().isBarTvColor());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_info);
        ButterKnife.bind(this);
        initViews();
    }

    private void initViews() {
        titleBar.setBackground(CommonApp.getApplication().getStateBarDrawable());
        if (getIntent() != null) {
            content = getIntent().getStringExtra("content");
            editTitle.setText(content);
        }
        //输入类型为文本
        editTitle.setInputType(InputType.TYPE_CLASS_TEXT);
        //限制长度为10
        editTitle.setFilters(new InputFilter[]{new InputFilter.LengthFilter(11)});
        editTitle.setSelection(content.length());

    }


    @OnClick(R.id.btnSure)
    public void onViewClicked() {
        //修改昵称/姓名
        setInput();
        String name = editTitle.getText().toString();
        if (!EmptyDeal.isEmpy(name)){
            Intent intent = new Intent();
            intent.putExtra("content", editTitle.getText().toString());
            setResult(Activity.RESULT_OK, intent);
            finish();
        }else {
            UIUtils.shortM("请输入昵称");
        }

    }
    private void setInput() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
    }
}
