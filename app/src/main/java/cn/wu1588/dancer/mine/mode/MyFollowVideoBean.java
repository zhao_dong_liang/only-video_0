package cn.wu1588.dancer.mine.mode;

import java.util.List;

import cn.wu1588.dancer.my_video.bean.MyVideoBean;

public class MyFollowVideoBean {
    public String attention_uid;
    public String name;
    public String icon;
    public int attention_num;//粉丝数
    public int video_count;
    public String signature;//个性签名
    public int is_talent;//是否达人认证
    public int mutual_attention;//是否互相关注了
    public boolean is_attention=true;//是否关注
    public List<MyVideoBean> video;
}
