package cn.wu1588.dancer.mine.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.mine.mode.DownloadBean;

public class MyDownloadAdapter extends BaseQuickAdapter<DownloadBean, BaseViewHolder> {



    public MyDownloadAdapter() {
        super(R.layout.my_download_item);
    }
    @Override
    protected void convert(BaseViewHolder helper, DownloadBean item) {
        String download_time = item.getDownload_time();
        String[] s = download_time.split(" ");
        GlideMediaLoader.load(mContext,helper.getView(R.id.img),item.getImage(),R.drawable.ic_history_place);
        helper.setText(R.id.video_name,item.getTitle())
                .setText(R.id.time_tv,"下载日期:"+s[0])
                .setText(R.id.download_num,"已下载:"+item.getDown_num());

    }
}
