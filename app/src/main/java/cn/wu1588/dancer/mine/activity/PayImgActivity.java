package cn.wu1588.dancer.mine.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cn.wu1588.dancer.R;
import com.cnsunrun.commonui.widget.titlebar.TitleBar;
import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.sunrun.sunrunframwork.utils.AHandler;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.mine.mode.PayUrlBean;

import static android.os.Environment.DIRECTORY_DCIM;

/**
 * 支付图片
 */
public class PayImgActivity extends LBaseActivity {


    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.ivQrCode)
    ImageView ivQrCode;
    @BindView(R.id.tvtTitle1)
    TextView tvtTitle1;
    @BindView(R.id.tvtTitle2)
    TextView tvtTitle2;
    @BindView(R.id.ivImg)
    ImageView ivImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payimg);
        ButterKnife.bind(this);
        initViews();

    }

    private void initViews() {
        titleBar.setLeftAction(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        PayUrlBean payUrlBean = getIntent().getParcelableExtra("PayUrlBean");
        tvtTitle1.setText(payUrlBean.title_1);
        tvtTitle2.setText(payUrlBean.title_2);
        GlideMediaLoader.load(that, ivQrCode, payUrlBean.url);

        ivQrCode.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AHandler.runTask(new AHandler.Task() {
                    @Override
                    public void update() {
                        getScreenShot(ivQrCode);
                    }
                }, 10);
                return false;
            }
        });
    }

    public void getScreenShot(View view) {
        Context context = view.getContext();
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();  //启用DrawingCache并创建位图
        Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache()); //创建一个DrawingCache的拷贝，因为DrawingCache得到的位图在禁用后会被回收
        view.setDrawingCacheEnabled(false);  //禁用DrawingCahce否则会影响性能
        File dcimDirectory = Environment.getExternalStoragePublicDirectory(DIRECTORY_DCIM);
        File screenshot = new File(dcimDirectory, System.currentTimeMillis() + ".jpg");
        if (UIUtils.saveBitmapToFile(bitmap, screenshot.getAbsolutePath())) {
            UIUtils.shortM("截图成功,保存在" + screenshot.getAbsolutePath());
            // 其次把文件插入到系统图库
            try {
                MediaStore.Images.Media.insertImage(context.getContentResolver(),
                        screenshot.getAbsolutePath(), screenshot.getName(), null);
                // 最后通知图库更新

                context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + screenshot.getAbsolutePath())));
            } catch (Exception e) {
                e.printStackTrace();
                UIUtils.shortM("截图失败,请检查存储权限");
            }

        } else {
            UIUtils.shortM("截图失败,请检查存储权限");
        }
    }
}
