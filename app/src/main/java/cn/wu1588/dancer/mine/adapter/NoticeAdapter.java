package cn.wu1588.dancer.mine.adapter;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import cn.wu1588.dancer.mine.mode.MsgBean;

public class NoticeAdapter extends BaseQuickAdapter<MsgBean, BaseViewHolder> {
    public NoticeAdapter() {
        super(R.layout.item_notice_layout);
    }

    @Override
    protected void convert(BaseViewHolder helper, MsgBean item) {
        helper.setText(R.id.tvTitle,item.title)
                .setText(R.id.tvTime,item.add_time);
    }
}
