package cn.wu1588.dancer.mine.adapter;
import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.mine.mode.HistoryAndLoveBean;

public class HistoryHorizAdapter extends BaseQuickAdapter<HistoryAndLoveBean.HistoryBean.InfoBean,BaseViewHolder> {

    public HistoryHorizAdapter() {
        super(R.layout.item_his_horiz_layout);
    }

    @Override
    protected void convert(BaseViewHolder helper, HistoryAndLoveBean.HistoryBean.InfoBean item) {
        GlideMediaLoader.load(mContext,helper.getView(R.id.rIvCourse),item.image,R.drawable.ic_history_place);
        helper.setText(R.id.tvTitle,item.title);
    }
}
