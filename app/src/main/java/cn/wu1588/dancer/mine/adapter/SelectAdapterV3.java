package cn.wu1588.dancer.mine.adapter;

import android.graphics.Paint;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.mine.mode.RechargeBeanV3;

/**
 * Created by zhou on 2021/1/6 17:08.
 */
public class SelectAdapterV3 extends BaseQuickAdapter<RechargeBeanV3, BaseViewHolder> {

    private int pos;
    public SelectAdapterV3() {
        super(R.layout.item_taocan_layout_v3);
    }
    @Override
    protected void convert(BaseViewHolder helper, RechargeBeanV3 item) {
//        helper.addOnClickListener(R.id.flContainer);
        TextView tvOldPrice = helper.getView(R.id.tvOldPrice);
//        TextView tvNewPrice = helper.getView(R.id.tvNewPrice);
//        TextView tvTitle = helper.getView(R.id.tvTitle);
//        TextView tvContent = helper.getView(R.id.tvContent);

        tvOldPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);// 设置中划线并加清晰
        tvOldPrice.getPaint().setAntiAlias(true);

        helper.setBackgroundRes(R.id.flContainer,
                helper.getAdapterPosition()==pos?R.drawable.shape_taocan_bg_sel:R.drawable.shape_taocan_bg_nor);


        helper.setText(R.id.tvTitle,item.title)
                .setText(R.id.tvContent,item.title)
                .setText(R.id.tvNewPrice,mContext.getString(R.string.price,item.discount_price))
                .setText(R.id.tvOldPrice,mContext.getString(R.string.price,item.price));

    }

    public void setSelectedPos(int position) {
        this.pos=position;
        notifyDataSetChanged();
    }
}
