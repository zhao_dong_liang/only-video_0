package cn.wu1588.dancer.mine.mode;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import cn.wu1588.dancer.my_video.bean.MyVideoBean;

public class BuyAlbumBean implements Parcelable {
    public int viewType;
    public String id;
    public String type;
    public String title;
    public String content;
    public String logo;

    public String background_image;
    public String sort;

    public String add_time;

    public String update_time;

    public String is_hid;

    public String is_del;
    public String recommend;
    public String desc;

    public String is_play;

    public String username;
    public String user_id;
    public String icon;
    public String price;

    public String download_num;
    public int movies_count;
    public int movie_count;
    public List<MyVideoBean> movies;
    public String income;
    public UserInfo user_info;
    public String followMessage;
    public int is_buy;

    public BuyAlbumBean() {
    }

    protected BuyAlbumBean(Parcel in) {
        id = in.readString();
        type = in.readString();
        title = in.readString();
        content = in.readString();
        logo = in.readString();
        background_image = in.readString();
        sort = in.readString();
        add_time = in.readString();
        update_time = in.readString();
        is_hid = in.readString();
        is_del = in.readString();
        recommend = in.readString();
        desc = in.readString();
        is_play = in.readString();
        username = in.readString();
        user_id = in.readString();
        icon = in.readString();
        price = in.readString();
        download_num = in.readString();
        movies_count = in.readInt();
        movie_count = in.readInt();
        movies = in.createTypedArrayList(MyVideoBean.CREATOR);
        income = in.readString();
        user_info = in.readParcelable(UserInfo.class.getClassLoader());
        followMessage = in.readString();
        is_buy = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(type);
        dest.writeString(title);
        dest.writeString(content);
        dest.writeString(logo);
        dest.writeString(background_image);
        dest.writeString(sort);
        dest.writeString(add_time);
        dest.writeString(update_time);
        dest.writeString(is_hid);
        dest.writeString(is_del);
        dest.writeString(recommend);
        dest.writeString(desc);
        dest.writeString(is_play);
        dest.writeString(username);
        dest.writeString(user_id);
        dest.writeString(icon);
        dest.writeString(price);
        dest.writeString(download_num);
        dest.writeInt(movies_count);
        dest.writeInt(movie_count);
        dest.writeTypedList(movies);
        dest.writeString(income);
        dest.writeParcelable(user_info, flags);
        dest.writeString(followMessage);
        dest.writeInt(is_buy);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BuyAlbumBean> CREATOR = new Creator<BuyAlbumBean>() {
        @Override
        public BuyAlbumBean createFromParcel(Parcel in) {
            return new BuyAlbumBean(in);
        }

        @Override
        public BuyAlbumBean[] newArray(int size) {
            return new BuyAlbumBean[size];
        }
    };

    public static class UserInfo implements Parcelable {


        public String id;
        public String identity_id;
        public String generalize_id;
        public String pid;
        public String device_number;
        public String nickname;
        public String code;
        public String mobile;
        public String salt;
        public String password;
        public String icon;
        public String gender;
        public String is_member;
        public String is_hid;
        public String is_del;
        public String login_ip;
        public String login_time;
        public String register_time;
        public String register_ip;
        public String member_start_time;
        public String member_end_time;
        public String agency_id;
        public String total_num;
        public String pay_num;
        public String recommend_num;
        public String type;
        public String r_token;
        public String wx_openid;
        public String wx_access_token;
        public String wx_refresh_token;
        public String qq_openid;
        public String qq_access_token;
        public String signature;
        public String province;
        public String city;
        public String is_talent;
        public String secret;
        public String cash_secret;
        public String ali_name;

        public UserInfo() {
        }

        protected UserInfo(Parcel in) {
            id = in.readString();
            identity_id = in.readString();
            generalize_id = in.readString();
            pid = in.readString();
            device_number = in.readString();
            nickname = in.readString();
            code = in.readString();
            mobile = in.readString();
            salt = in.readString();
            password = in.readString();
            icon = in.readString();
            gender = in.readString();
            is_member = in.readString();
            is_hid = in.readString();
            is_del = in.readString();
            login_ip = in.readString();
            login_time = in.readString();
            register_time = in.readString();
            register_ip = in.readString();
            member_start_time = in.readString();
            member_end_time = in.readString();
            agency_id = in.readString();
            total_num = in.readString();
            pay_num = in.readString();
            recommend_num = in.readString();
            type = in.readString();
            r_token = in.readString();
            wx_openid = in.readString();
            wx_access_token = in.readString();
            wx_refresh_token = in.readString();
            qq_openid = in.readString();
            qq_access_token = in.readString();
            signature = in.readString();
            province = in.readString();
            city = in.readString();
            is_talent = in.readString();
            secret = in.readString();
            cash_secret = in.readString();
            ali_name = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(id);
            dest.writeString(identity_id);
            dest.writeString(generalize_id);
            dest.writeString(pid);
            dest.writeString(device_number);
            dest.writeString(nickname);
            dest.writeString(code);
            dest.writeString(mobile);
            dest.writeString(salt);
            dest.writeString(password);
            dest.writeString(icon);
            dest.writeString(gender);
            dest.writeString(is_member);
            dest.writeString(is_hid);
            dest.writeString(is_del);
            dest.writeString(login_ip);
            dest.writeString(login_time);
            dest.writeString(register_time);
            dest.writeString(register_ip);
            dest.writeString(member_start_time);
            dest.writeString(member_end_time);
            dest.writeString(agency_id);
            dest.writeString(total_num);
            dest.writeString(pay_num);
            dest.writeString(recommend_num);
            dest.writeString(type);
            dest.writeString(r_token);
            dest.writeString(wx_openid);
            dest.writeString(wx_access_token);
            dest.writeString(wx_refresh_token);
            dest.writeString(qq_openid);
            dest.writeString(qq_access_token);
            dest.writeString(signature);
            dest.writeString(province);
            dest.writeString(city);
            dest.writeString(is_talent);
            dest.writeString(secret);
            dest.writeString(cash_secret);
            dest.writeString(ali_name);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<UserInfo> CREATOR = new Creator<UserInfo>() {
            @Override
            public UserInfo createFromParcel(Parcel in) {
                return new UserInfo(in);
            }

            @Override
            public UserInfo[] newArray(int size) {
                return new UserInfo[size];
            }
        };
    }
}
