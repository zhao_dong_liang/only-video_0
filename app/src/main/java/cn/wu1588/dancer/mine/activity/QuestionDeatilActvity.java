package cn.wu1588.dancer.mine.activity;

import android.os.Bundle;
import android.widget.TextView;

import cn.wu1588.dancer.R;
import com.cnsunrun.commonui.widget.titlebar.TitleBar;
import com.sunrun.sunrunframwork.bean.BaseBean;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.mine.mode.ProblemBean;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_QUEST_DETAIL_CODE;

/**
 * 问题详情
 */
public class QuestionDeatilActvity extends LBaseActivity {

    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvContent)
    TextView tvContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_detail);
        ButterKnife.bind(this);
        initData();
    }

    private void initData() {
        String id = getIntent().getStringExtra("id");
        BaseQuestStart.getQuestDetail(that, id);
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode) {
            case GET_QUEST_DETAIL_CODE:
                if (bean.status == 1) {
                    ProblemBean.QuestionlistBean questionlistBean = bean.Data();
                    updateUI(questionlistBean);

                }
                break;
        }
        super.nofityUpdate(requestCode, bean);
    }

    private void updateUI(ProblemBean.QuestionlistBean questionlistBean) {
        tvTitle.setText(questionlistBean.title);
        tvContent.setText(questionlistBean.content);
    }
}
