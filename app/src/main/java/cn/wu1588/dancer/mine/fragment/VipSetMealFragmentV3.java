package cn.wu1588.dancer.mine.fragment;

import android.os.Bundle;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.sunrun.sunrunframwork.bean.BaseBean;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.RecycleHelper;
import cn.wu1588.dancer.mine.adapter.SelectAdapterV3;
import cn.wu1588.dancer.mine.mode.RechargeBeanV3;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.VIP_SET_MEAL_CODE;

/**
 * Created by zhou on 2021/1/6 18:48.
 *
 * @link VipSetMealFragment
 */
@Deprecated
public class VipSetMealFragmentV3 extends LBaseFragment implements BaseQuickAdapter.OnItemClickListener {

    @BindView(R.id.vipRv)
    RecyclerView vipRv;

    private SelectAdapterV3 adapterV3;
    private List<RechargeBeanV3> rechargeBeanV3List = new ArrayList<>();
    public RechargeBeanV3 rechargeBeanV3;

    public int selecteditem = 0;

    public static VipSetMealFragmentV3 newInstance() {
        VipSetMealFragmentV3 fragment = new VipSetMealFragmentV3();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_vip_set_meal;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        initData();


    }

    private void initData() {
        BaseQuestStart.getVipSetMeal(VipSetMealFragmentV3.this);
    }

    private void initView() {
        RecycleHelper.setLinearLayoutManager(vipRv, LinearLayoutManager.VERTICAL);
        adapterV3 = new SelectAdapterV3();
        vipRv.setAdapter(adapterV3);
        adapterV3.setOnItemClickListener(this);

//        List<RechargeBeanV3> rechargeBeanV3s = adapterV3.getData();
//        for (RechargeBeanV3 r : rechargeBeanV3s) {
//            if (r.isCheck()) {
//                rechargeBeanV3 = r;
//                break;
//            }
//        }
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode){
            case VIP_SET_MEAL_CODE:
                if(bean.status ==1){
                    rechargeBeanV3List.clear();
                    rechargeBeanV3List = bean.Data();
                    //设置值之前 先给第一行设置为选中状态
//                    adapterV3.setSelectedPos(selecteditem);
                    rechargeBeanV3List.get(0).setCheck(true);
                    adapterV3.setNewData(rechargeBeanV3List);
//                    adapterV3.notifyDataSetChanged();
//                    rechargeBeanV3 = adapterV3.getItem(selecteditem);

                    rechargeBeanV3 = rechargeBeanV3List.get(0);

                }
                break;
            default:
                break;

        }
    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
//        rechargeBeanV3 = adapterV3.getItem(position);
//        selecteditem = position;
//        adapterV3.setSelectedPos(selecteditem);
        for(RechargeBeanV3 re : adapterV3.getData()){
            re.setCheck(false);
        }
        adapterV3.getItem(position).setCheck(true);
        rechargeBeanV3 = adapterV3.getItem(position);
        adapterV3.notifyDataSetChanged();
    }
}