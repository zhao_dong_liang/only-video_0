package cn.wu1588.dancer.mine.mode;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by zhou on 2021/1/5 16:30.
 */
public class MemberRecordBean implements Parcelable {


    public MemberRecordBean() {
    }


    /**
     * id : 84
     * member_id : 198
     * title : 购买免费投广告会员
     * price : 1.00
     * start_time : 2021-11-22
     * end_time : 2022-11-22
     * add_time : 2020-11-26 22:49:29
     * update_time : 2020-11-26 22:49:29
     * is_del : 0
     * is_past_due : 0
     * combo_price_id : 1
     */

    public String id;
    public String member_id;
    public String title;
    public String price;
    public String start_time;
    public String end_time;
    public String add_time;
    public String update_time;
    public String is_del;
    public String is_past_due;
    public String combo_price_id;


    protected MemberRecordBean(Parcel in) {
        id = in.readString();
        member_id = in.readString();
        title = in.readString();
        price = in.readString();
        start_time = in.readString();
        end_time = in.readString();
        add_time = in.readString();
        update_time = in.readString();
        is_del = in.readString();
        is_past_due = in.readString();
        combo_price_id = in.readString();
    }

    public static final Creator<MemberRecordBean> CREATOR = new Creator<MemberRecordBean>() {
        @Override
        public MemberRecordBean createFromParcel(Parcel in) {
            return new MemberRecordBean(in);
        }

        @Override
        public MemberRecordBean[] newArray(int size) {
            return new MemberRecordBean[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(member_id);
        dest.writeString(title);
        dest.writeString(price);
        dest.writeString(start_time);
        dest.writeString(end_time);
        dest.writeString(add_time);
        dest.writeString(update_time);
        dest.writeString(is_del);
        dest.writeString(is_past_due);
        dest.writeString(combo_price_id);
    }
}
