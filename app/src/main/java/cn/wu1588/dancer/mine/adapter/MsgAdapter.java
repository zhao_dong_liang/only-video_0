package cn.wu1588.dancer.mine.adapter;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import cn.wu1588.dancer.mine.mode.MsgBean;

public class MsgAdapter extends BaseQuickAdapter<MsgBean, BaseViewHolder> {
    public MsgAdapter() {
        super(R.layout.item_msg_layout);
    }

    @Override
    protected void convert(BaseViewHolder helper, MsgBean item) {
        helper.setText(R.id.tvNikeName,item.nickname)
                .setText(R.id.tvTime,item.add_time)
                .setText(R.id.tvCon,item.reply_content)
                .setText(R.id.tvContent,item.content);
    }
}
