package cn.wu1588.dancer.mine.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.my.toolslib.OneClickUtils;
import com.my.toolslib.http.utils.LzyResponse;
import com.sunrun.sunrunframwork.uibase.BaseActivity;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;

import java.util.List;

import butterknife.BindView;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.mine.adapter.FollowFansAdapter;
import cn.wu1588.dancer.mine.mode.MyFollowVideoBean;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.common.util.PageLimitDelegate;

public class MyFollowFragment extends LBaseFragment implements BaseQuickAdapter.OnItemChildClickListener {
@BindView(R.id.recyclerView)
    RecyclerView recyclerView;
@BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;

private int type;
private FollowFansAdapter adapter;
//    type 1关注列表 2粉丝列表
public static MyFollowFragment newInstance(int type) {

    Bundle args = new Bundle();
    args.putInt("type",type);
    MyFollowFragment fragment = new MyFollowFragment();
    fragment.setArguments(args);
    return fragment;
}
    PageLimitDelegate<MyFollowVideoBean> pageLimitDelegate=new PageLimitDelegate<>(new PageLimitDelegate.DataProvider() {
        @Override
        public void loadData(int page) {
            if (type==0){//关注列表
                BaseQuestStart.getAttentionList(MyFollowFragment.this, Config.getLoginInfo().id);
            }else {//粉丝列表
                BaseQuestStart.getMyFans(MyFollowFragment.this, Config.getLoginInfo().id);
            }

        }
    });
    @Override
    public int getLayoutRes() {
        return R.layout.my_follow_fragmeng_fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle!=null){
            type=bundle.getInt("type",0);
        }
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
         adapter=new FollowFansAdapter();
        adapter.setOnItemChildClickListener(this::onItemChildClick);
        recyclerView.setAdapter(adapter);
        pageLimitDelegate.attach(refreshLayout,recyclerView,adapter);
    }

    @Override
    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
        super.nofityUpdateUi(requestCode, response, view);
        if (requestCode==BaseQuestStart.GET_MY_ATTENTION_CODE){//获取关注的列表
            pageLimitDelegate.setData((List<MyFollowVideoBean>) response.data);
        }else if (requestCode==BaseQuestStart.ATTENTION_USER_CODE){//关注用户成功
            if (response.code==1){
                if (clickPosition>=0){
                    adapter.getItem(clickPosition).is_attention=true;
                    adapter.notifyDataSetChanged();
                    clickPosition=-1;
                }
                ToastUtils.longToast("关注成功");
            }else {
                ToastUtils.longToast("关注失败");
            }

        }else if (requestCode==BaseQuestStart.CANCEL_ATTENTION_CODE){//取消关注
            if (response.code==1){
                if (clickPosition>=0){
                    adapter.getItem(clickPosition).is_attention=false;
                    adapter.notifyDataSetChanged();
                    clickPosition=-1;
                }
                ToastUtils.longToast("取消成功");
            }else {
                ToastUtils.longToast("取消失败");
            }
        }else if (requestCode==BaseQuestStart.GET_MY_FANS_CODE){//粉丝列表
            List<MyFollowVideoBean> data = (List<MyFollowVideoBean>) response.data;
            for (MyFollowVideoBean bean: data) {
                if (bean.mutual_attention==1){
                    bean.is_attention=true;
                }else {
                    bean.is_attention=false;
                }

            }
            pageLimitDelegate.setData(data);
        }
    }


    private int clickPosition=-1;
    @Override
    public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        if (OneClickUtils.isFastDoubleClick(view.getId())){
            return;
        }
        MyFollowVideoBean bean= (MyFollowVideoBean) adapter.getItem(position);
        switch (view.getId()){
            case R.id.follow_box://取消关注或者关注
                clickPosition=position;
                CheckBox checkBox= (CheckBox) view;
                if (checkBox.isChecked()){//已关注
                    BaseQuestStart.cancelAttention(this,bean.attention_uid);
                }else {//未关注
                    BaseQuestStart.followUser(this,bean.attention_uid);
                }
                break;
            case R.id.header_img://进入用户个人中心
                CommonIntent.startOtherUserActiivty((BaseActivity) getActivity(),bean.attention_uid);
                break;

        }
    }
}
