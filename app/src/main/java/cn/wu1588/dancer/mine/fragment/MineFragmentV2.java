package cn.wu1588.dancer.mine.fragment;

import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.cnsunrun.commonui.widget.button.RoundButton;
import com.makeramen.roundedimageview.RoundedImageView;
import com.my.toolslib.http.utils.NetRequestListenerProxy;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uibase.BaseActivity;
import com.sunrun.sunrunframwork.utils.EmptyDeal;
import com.tencent.mmkv.MMKV;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.BuildConfig;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.mine.mode.AdverBean;
import cn.wu1588.dancer.mine.mode.HistoryAndLoveBean;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.config.RongIMHelper;
import cn.wu1588.dancer.common.model.LoginInfo;
import cn.wu1588.dancer.common.model.SystemParams;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.common.receiver.PushInfo;
import cn.wu1588.dancer.common.util.LoginUtil;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;
import io.rong.imkit.RongIM;
import io.rong.imlib.model.CSCustomServiceInfo;
import io.rong.imlib.model.UserInfo;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_ADVER_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_HIS_RECORD_LOVE_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_USER_INFO_CODE;

//我的页面，新版本
public class MineFragmentV2 extends LBaseFragment implements View.OnClickListener {

    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.user_info_layout)
    ConstraintLayout user_info_layout;
    @BindView(R.id.my_set_linear)
    View my_set_linear;
    @BindView(R.id.opinion_linear)
    View opinion_linear;
    @BindView(R.id.service_person_linear)
    View service_person_linear;
    @BindView(R.id.my_spread_linear)
    View my_spread_linear;
    @BindView(R.id.my_advert_linear)
    View my_advert_linear;
    @BindView(R.id.my_see_history_linear)
    View my_see_history_linear;
    @BindView(R.id.my_video_profit_linear)
    View my_video_profit_linear;
    @BindView(R.id.my_video_linear)
    View my_video_linear;
    @BindView(R.id.my_follow_linear)
    View my_follow_linear;
    @BindView(R.id.my_buy_linear)
    View my_buy_linear;
    @BindView(R.id.my_msg_linear)
    View my_msg_linear;
    @BindView(R.id.my_download_linear)
    View my_download_linear;
    @BindView(R.id.vip_layout)
    View vip_layout;
    @BindView(R.id.bug_gold_layout)
    View bug_gold_layout;
    @BindView(R.id.user_header_img)
    RoundedImageView user_header_img;
    @BindView(R.id.user_name)
    TextView user_name;
    @BindView(R.id.user_id)
    TextView user_id;
    @BindView(R.id.go_user_view)
    View go_user_view;
    @BindView(R.id.video_num)
    TextView video_num;
    @BindView(R.id.album_num)
    TextView album_num;
    @BindView(R.id.fans_num_content)
    TextView fans_num;
    @BindView(R.id.fabulous_num_content)
    TextView fabulous_num;
    @BindView(R.id.btn_fans_added)
    RoundButton btn_fans_added;
    @BindView(R.id.btn_fabulous_added)
    RoundButton btn_fabulous_added;
    @BindView(R.id.gold_num)
    TextView gold_num;
    @BindView(R.id.member_time)
    TextView tvEndTime;
    @BindView(R.id.user_labe_tv)
    TextView tvMember;
    @BindView(R.id.user_auth_state)
    TextView user_auth_state;
    @BindView(R.id.user_auth_tv)
    TextView user_auth_tv;
    @BindView(R.id.member_time_labe)
    TextView member_time_labe;

    public static MineFragmentV2 newInstance() {
        MineFragmentV2 fragmentV2 = new MineFragmentV2();
        Bundle bundle = new Bundle();
        fragmentV2.setArguments(bundle);
        return fragmentV2;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_mine_v2;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();

    }

    @OnClick(R.id.user_auth_tv)
    void auth(View view) {
        if (LoginUtil.startLogin(getActivity())) {
            CommonIntent.startLoginActivity(getActivity());
        } else {
            CommonIntent.startAuthActivity(getActivity());
        }
    }

    private void initView() {
        MMKV mmkv = MMKV.defaultMMKV();
        int[] colors = {Color.parseColor(mmkv.decodeString("up_b_color", "#000000")),
                Color.parseColor(mmkv.decodeString("up_e_color", "#000000"))
        };
        GradientDrawable drawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, colors);
        drawable.setGradientType(GradientDrawable.LINEAR_GRADIENT);//设置线性渐变
        drawable.setOrientation(GradientDrawable.Orientation.LEFT_RIGHT);//设置渐变方向
        user_info_layout.setBackground(drawable);


        GradientDrawable drawable_titbar = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, colors);
        drawable_titbar.setGradientType(GradientDrawable.LINEAR_GRADIENT);//设置线性渐变
        drawable_titbar.setOrientation(GradientDrawable.Orientation.LEFT_RIGHT);//设置渐变方向
        titleBar.setTitleBarBackgroundDrawable(drawable_titbar);

        String adopen = mmkv.decodeString(SystemParams.SELF_AD_OPEN, "1");
        my_advert_linear.setVisibility(adopen.equals("1") ? View.VISIBLE : View.GONE);

        my_set_linear.setOnClickListener(this);
        opinion_linear.setOnClickListener(this);
        service_person_linear.setOnClickListener(this::onClick);
        my_spread_linear.setOnClickListener(this);
        my_advert_linear.setOnClickListener(this);
        my_see_history_linear.setOnClickListener(this);
        my_video_profit_linear.setOnClickListener(this);
        my_video_linear.setOnClickListener(this);
        my_follow_linear.setOnClickListener(this);
        my_buy_linear.setOnClickListener(this);
        my_msg_linear.setOnClickListener(this);
        my_download_linear.setOnClickListener(this);
        vip_layout.setOnClickListener(this);
        bug_gold_layout.setOnClickListener(this);
        go_user_view.setOnClickListener(this);
        user_name.setOnClickListener(this);
        btn_fabulous_added.setVisibility(View.GONE);
        btn_fans_added.setVisibility(View.GONE);
    }

    @Subscribe
    public void receive(PushInfo pushInfo) {
        if (pushInfo.fansCount > 0 || pushInfo.likedCount > 0) {
            BaseQuestStart.getUserInfo(new NetRequestListenerProxy(getActivity()) {
                @Override
                public void nofityUpdate(int i, BaseBean baseBean) {
                    LoginInfo loginInfo = baseBean.Data();
                    if (pushInfo.fansCount > 0) {
                        int fansCountAdded = Integer.valueOf(loginInfo.attention_count) - Integer.valueOf(MineFragmentV2.this.loginInfo.attention_count);
                        String msgCount = fansCountAdded + "";
                        if (fansCountAdded >= 100) {
                            msgCount = "99+";
                        }
                        btn_fans_added.setVisibility(View.VISIBLE);
                        btn_fans_added.setText(msgCount);
                    }

                    if (pushInfo.likedCount > 0) {
                        int likedCount = Integer.valueOf(loginInfo.like_num) - Integer.valueOf(MineFragmentV2.this.loginInfo.like_num);
                        String msgCount = likedCount + "";
                        if (likedCount >= 100) {
                            msgCount = "99+";
                        }//18
                        btn_fabulous_added.setVisibility(View.VISIBLE);
                        btn_fabulous_added.setText(msgCount);
                    }
                }
            });
        }

    }

    @Override
    public void onResume() {
        initData();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void initData() {
        if (!LoginUtil.startLogin(getActivity())) {
            BaseQuestStart.getUserInfo(MineFragmentV2.this);
        } else {
            user_name.setText("登录/注册");
            user_id.setText("--");
            user_header_img.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.ic_def_head));
            user_auth_state.setText("登录让我更了解你");
            user_auth_tv.setVisibility(View.VISIBLE);
            tvMember.setText("更多精彩功能即将呈现");
            tvEndTime.setText("等录查看");
            video_num.setText("0");
            album_num.setText("0");
            fans_num.setText("0");
            fabulous_num.setText("0");
            member_time_labe.setText("会员到期时间");
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.user_name:
                if (LoginUtil.startLogin(getActivity())) {
                    CommonIntent.startLoginActivity(getActivity());
                }
                break;
            case R.id.go_user_view:
                if (LoginUtil.startLogin(getActivity())) {
                    CommonIntent.startLoginActivity(that);
                } else {
                    CommonIntent.startUserInfoActivity(that);
                }
                break;
            case R.id.vip_layout://购买vip
                if (LoginUtil.startLogin(getActivity())) {
                    CommonIntent.startLoginActivity(that);
                } else {
                    CommonIntent.startRechargeActivity(that, 0);
                }
                break;
            case R.id.bug_gold_layout://购买金币
                if (LoginUtil.startLogin(getActivity())) {
                    CommonIntent.startLoginActivity(that);
                } else {
                    CommonIntent.startRechargeActivity(that, 1);
                }
                break;
            case R.id.my_msg_linear://我的消息
                CommonIntent.startNoticeActivity(that);
                break;

            case R.id.my_download_linear://我的下载
                CommonIntent.startMyDownloadActivity(that);
                break;
            case R.id.my_buy_linear://我的购买
                CommonIntent.startMyBuyActivity(that);
                break;
            case R.id.my_follow_linear://我的关注
                CommonIntent.startMyFollowActivity((BaseActivity) getActivity());
                break;
            case R.id.my_video_linear://我的视频
                CommonIntent.startMyVideoActivity((BaseActivity) getActivity());
                break;
            case R.id.my_video_profit_linear://我的收益
                CommonIntent.startProfitActivity(this);
                break;
            case R.id.my_see_history_linear://观看历史
                CommonIntent.startHistoryRecordActivity(that);
                break;
            case R.id.my_advert_linear://自助广告
//                if (!EmptyDeal.isEmpy(adUrl)) {
//                    CommonIntent.startBrowser(that, adUrl);
//                }
                CommonIntent.startAdvertAdminActivity(that);
                break;
            case R.id.my_spread_linear://推广
                CommonIntent.startTuiGuangActivity(that);
                break;
            case R.id.service_person_linear://客服
                if (LoginUtil.startLogin(getActivity())) {
                    CommonIntent.startLoginActivity(that);
                } else {
                    tocus();
                }
                break;
            case R.id.opinion_linear://意见反馈
                if (LoginUtil.startLogin(getActivity())) {
                    CommonIntent.startLoginActivity(that);
                } else {
                    CommonIntent.startFeedBackActivity(that);
                }
                break;
            case R.id.my_set_linear://设置
                CommonIntent.startSettingActivity(that);
                break;
        }
    }

    private LoginInfo loginInfo;
    private UserInfo chatUserInfo;
    private String adUrl;

    private void tocus() {
        //联系客服
        if (loginInfo != null) {
            CSCustomServiceInfo.Builder builder = new CSCustomServiceInfo.Builder();
            builder.nickName(loginInfo.nickname);
            builder.userId(loginInfo.r_token);
            builder.portraitUrl(loginInfo.icon);
            RongIM.getInstance().startCustomerServiceChat(getActivity(), BuildConfig.CUSTOMER_SERVICEID, "在线客服", builder.build());
            //图像信息
            RongIM.setUserInfoProvider(new RongIM.UserInfoProvider() {
                @Override
                public UserInfo getUserInfo(String userId) {
                    Log.d("联系客服", "s====" + userId);
                    return findByUserInfo(userId);
                }
            }, true);
        }
    }

    private UserInfo findByUserInfo(String userId) {
        if (!TextUtils.isEmpty(userId)) {
            chatUserInfo = new UserInfo(loginInfo.r_token, loginInfo.nickname, Uri.parse(loginInfo.icon));
        }
        return chatUserInfo;
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode) {
            case GET_USER_INFO_CODE:
                if (bean.status == 1) {
                    loginInfo = bean.Data();
                    if (loginInfo != null) {
                        go_user_view.setVisibility(View.VISIBLE);
                        GlideMediaLoader.loadHead(that, user_header_img, loginInfo.icon);
                        user_name.setText(loginInfo.nickname);
                        user_id.setText(loginInfo.identity_id);
                        gold_num.setText(loginInfo.gold_account);
                        RongIMHelper.connect(loginInfo.r_token);
                        loginInfo.setUid(loginInfo.id);
//                        GlideMediaLoader.loadHead(that, rIvStar, loginInfo.icon);
//                        tvNikeName.setText(loginInfo.nickname);
//                        tvIDCard.setText(getString(R.string.identity_code, loginInfo.identity_id));
//                        tvTitleName.setText(loginInfo.title_name);
                        if (loginInfo.isMember()) {
                            tvEndTime.setText(loginInfo.member_end_time);
                            tvMember.setText(TextUtils.isEmpty(loginInfo.title) ? "更多精彩即将呈现" : loginInfo.title);
                            member_time_labe.setText("会员到期时间");
                        } else {
                            tvEndTime.setText("购买");
                            member_time_labe.setText("VIP会员");
                        }
                        video_num.setText(loginInfo.movie_count + "");
                        album_num.setText(loginInfo.channel_count + "");
                        fans_num.setText(loginInfo.attention_count + "");
                        fabulous_num.setText(loginInfo.like_num + "");
                        user_auth_state.setText(String.format("达人认证：%s", loginInfo.isTalentAuth() ? "已认证" : "未认证"));
                        user_auth_tv.setVisibility(loginInfo.isTalentAuth() ? View.GONE : View.VISIBLE);
                        Config.putLoginInfo(loginInfo);
                    }
                } else {
                    go_user_view.setVisibility(View.GONE);
                    tvEndTime.setText("暂无");
                    gold_num.setText("0");
                }
                break;
            case GET_HIS_RECORD_LOVE_CODE:
                if (bean.status == 1) {
                    HistoryAndLoveBean historyAndLoveBean = bean.Data();
                }
                break;
            case GET_ADVER_CODE:
                if (bean.status == 1) {
                    List<AdverBean> adverBeans = bean.Data();
                    if (!EmptyDeal.isEmpy(adverBeans)) {
                        adUrl = adverBeans.get(0).url;
//                        rIvGuanggao.setVisibility(View.VISIBLE);
//                        GlideMediaLoader.load(that, rIvGuanggao, adverBeans.get(0).image, R.drawable.ic_ad_detail_place);
                    } else {
//                        rIvGuanggao.setVisibility(View.GONE);
                    }
                }
                break;
        }
        super.nofityUpdate(requestCode, bean);
    }


}
