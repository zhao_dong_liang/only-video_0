package cn.wu1588.dancer.mine.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.cnsunrun.commonui.widget.roundwidget.QMUIRoundButton;
import com.cnsunrun.commonui.widget.titlebar.TitleBar;
import com.makeramen.roundedimageview.RoundedImageView;
import cn.wu1588.dancer.logic.AliPayLogic;
import cn.wu1588.dancer.logic.WxPayLogic;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;
import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.sunrun.sunrunframwork.utils.EmptyDeal;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.dialog.RechageDialogFragment;
import cn.wu1588.dancer.common.dialog.SelectPayDialog;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.RecycleHelper;
import cn.wu1588.dancer.common.util.Tool;
import cn.wu1588.dancer.mine.adapter.PayMentAdapter;
import cn.wu1588.dancer.mine.adapter.SelectAdapter;
import cn.wu1588.dancer.mine.mode.PayChannelBean;
import cn.wu1588.dancer.mine.mode.PayInfo;
import cn.wu1588.dancer.mine.mode.PayMentBean;
import cn.wu1588.dancer.mine.mode.RechargeBean;

import static android.provider.UserDictionary.Words.APP_ID;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.CREATE_ORDER_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.CREATE_WEIXIN_ORDER_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_PAY_CHANEL_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_PAY_METHOD_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_PAY_URL_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_RECHARGE_INFO_CODE;

/**
 * 会员充值
 */
public class RechargeMemberActivity extends LBaseActivity implements Handler.Callback {

    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.tvAgreements)
    TextView tvAgreements;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.recyclerViewPayment)
    RecyclerView recyclerViewPayment;
    @BindView(R.id.btnSure)
    QMUIRoundButton btnSure;
    @BindView(R.id.rIvStar)
    RoundedImageView rIvStar;
    @BindView(R.id.tvIDCard)
    TextView tvIDCard;
    @BindView(R.id.tvStates)
    TextView tvStates;
    @BindView(R.id.ivExpired)
    ImageView ivExpired;
    private PayMentAdapter payMentAdapter;
    private int checkPosition;//记录支付方式的位置
    private SelectAdapter selectAdapter;
    private RechargeBean rechargeBean;
    private int selectTaocanPosition;//记录套餐位置
    private WebView webView;
    private AliPayLogic aliPayLogic;
    private WxPayLogic wxPayLogic;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recharge_member);
        ButterKnife.bind(this);
        initViews();
        initData();
        initListener();
        Handler handler = new Handler(this);
        aliPayLogic = new AliPayLogic(that, handler);
        wxPayLogic = new WxPayLogic(that, APP_ID, handler);
    }

    private void initListener() {
        payMentAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                checkPosition = position;
                List<PayMentBean> data = payMentAdapter.getData();
                for (int i = 0; i < data.size(); i++) {
                    data.get(i).isCheckd = false;
                }
                data.get(position).isCheckd = true;
                payMentAdapter.notifyDataSetChanged();
            }
        });

        selectAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                selectTaocanPosition = position;
                selectAdapter.setSelectedPos(position);
            }
        });
    }

    private void initData() {
        BaseQuestStart.getRechargeInfo(that);
        BaseQuestStart.getPayMethod(that);
    }

    private void initViews() {
        webView = new WebView(that);
        RecycleHelper.setLinearLayoutManager(recyclerViewPayment, LinearLayoutManager.VERTICAL);
        RecycleHelper.setLinearLayoutManager(recyclerView, LinearLayoutManager.HORIZONTAL);
        payMentAdapter = new PayMentAdapter();
        recyclerViewPayment.setAdapter(payMentAdapter);

        selectAdapter = new SelectAdapter();
        recyclerView.setAdapter(selectAdapter);

        titleBar.setRightAction(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonIntent.startRechargeRecordActivity(that);
            }
        });
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode) {
            case GET_PAY_METHOD_CODE:
                if (bean.status == 1) {
                    List<PayMentBean> payMentBeans = bean.Data();
                    if (!EmptyDeal.isEmpy(payMentBeans)) {
                        payMentBeans.get(0).isCheckd = true;
                    }
                    payMentAdapter.setNewData(payMentBeans);
                }
                break;
            case GET_RECHARGE_INFO_CODE:
                if (bean.status == 1) {
                    rechargeBean = bean.Data();
                    updaeUI(rechargeBean);
                }
                break;
            case GET_PAY_CHANEL_CODE:
                if (bean.status == 1) {
                    List<PayChannelBean> payChannelBeans = bean.Data();
                    new SelectPayDialog(that, payChannelBeans, new SelectPayDialog.OnViewClickListener() {
                        @Override
                        public void onItemClick(String content) {
                            RechargeBean.VipMealBean VipMealBean = selectAdapter.getItem(selectTaocanPosition);
                            if (VipMealBean != null) {
                                BaseQuestStart.getPayUrl(that, VipMealBean.id, content);
                            }
                        }
                    });
                } else {
                    UIUtils.shortM(bean.msg);
                }
                break;
            case GET_PAY_URL_CODE:
                if (bean.status == 1) {
                    WebRechargeActivity.startThis(that, bean.Data().toString());
                } else {
                    UIUtils.shortM(bean.msg);
                }
                break;
            case CREATE_ORDER_CODE:
                if (bean.status == 1) {
//                    WebRechargeActivity.startThis(that, bean.Data().toString());
//                    CommonIntent.startWebActivity(that,"recharge",bean.Data().toString());
                    CommonIntent.startBrowser(that, bean.Data().toString());
                } else {
                    UIUtils.shortM(bean.msg);
                }
                break;
            case CREATE_WEIXIN_ORDER_CODE:
                if (bean.status == 1) {
                    PayInfo payInfo = bean.Data();
                    toWeixinPay(payInfo);
                } else {
                    UIUtils.shortM(bean.msg);
                }
                break;
        }
        super.nofityUpdate(requestCode, bean);
    }

    private void toWeixinPay(PayInfo payInfo) {
        JSONObject jsonObject = new JSONObject();
        try {
//            jsonObject.put("appid", payInfo.getAppid());
//            jsonObject.put("partnerid", payInfo.getPartnerid());
//            jsonObject.put("prepayid", payInfo.getPrepayid());
//            jsonObject.put("noncestr", payInfo.getNoncestr());
            jsonObject.put("timestamp", payInfo.getTimestamp());
            jsonObject.put("package", payInfo.getPackagex());
            jsonObject.put("sign", payInfo.getSign());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        wxPayLogic.startWxPay(jsonObject);
    }


    private void updaeUI(RechargeBean rechargeBean) {
        GlideMediaLoader.loadHead(that, rIvStar, rechargeBean.user_info.icon);
        tvIDCard.setText(rechargeBean.user_info.identity_id);
        tvStates.setText(rechargeBean.user_info.status);
        if (TextUtils.equals("已过期", rechargeBean.user_info.status)) {
            ivExpired.setVisibility(View.VISIBLE);
        }

        selectAdapter.setNewData(rechargeBean.vip_meal);
        List<RechargeBean.VipMealBean> data = selectAdapter.getData();
        if (!EmptyDeal.isEmpy(data)) {
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).isCommend()) {
                    selectTaocanPosition = i;
                    selectAdapter.setSelectedPos(i);
                }
            }
        }

    }

    @OnClick({R.id.tvAgreements, R.id.btnSure})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvAgreements:
                if (!EmptyDeal.isEmpy(rechargeBean)) {
                    CommonIntent.startServiceWebActivity(that, rechargeBean.h5);
                }
                break;
            case R.id.btnSure:
                if (!EmptyDeal.isEmpy(rechargeBean)) {
                    if (rechargeBean.isShow()) {
                        showWindow();
                    } else {
                        createOrder();
                    }
                }
                break;
        }
    }

    /**
     * 生成订单
     */
    private void createOrder() {
        PayMentBean item = payMentAdapter.getItem(checkPosition);
        RechargeBean.VipMealBean vipMealBean = selectAdapter.getItem(selectTaocanPosition);
        switch (item.id) {
            case "1":
                if (!EmptyDeal.isEmpy(vipMealBean)) {
                    BaseQuestStart.createOrder(that, vipMealBean);
                } else {
                    UIUtils.shortM("请选择套餐");
                }
                break;
            case "2":
                if (Tool.checkAliPayInstalled(that)) {
                    BaseQuestStart.getPayChanel(that, "1");
                } else {
                    UIUtils.shortM("请安装支付宝客户端");
                }
                break;
            case "3":
                if (Tool.isWeixinAvilible(that)) {

                    BaseQuestStart.getPayChanel(that,"2");
                } else {
                    UIUtils.shortM("未检测到微信程序或者微信版本过低");
                }
                break;
            case "4":
                if (!EmptyDeal.isEmpy(vipMealBean)) {
//                    BaseQuestStart.createWeixinrder(that, vipMealBean);
                } else {
                    UIUtils.shortM("请选择套餐");
                }
                break;
        }


    }

    /**
     * 弹出提示框
     */
    private void showWindow() {
        if (rechargeBean.popout_content == null) return;
        RechageDialogFragment.newInstance().setContentTxt(rechargeBean.popout_content).setOnSubmitAction(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createOrder();
            }
        }).setOnCancelAction(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createOrder();
            }
        }).show(getSupportFragmentManager(), "MessageTipDialog");
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case 1901:  //支付宝
                if (msg.obj.equals("9000")) {
                    ToastUtils.shortToast("支付1成功");
                    finish();
                } else {
                    ToastUtils.shortToast("支付失败");
                }
                break;
            case 1801:  //微信
                if (msg.obj.equals("9000")) {
                    ToastUtils.shortToast("支付成1功");
                    finish();
                } else {
                    ToastUtils.shortToast("支付失败");
                }
                break;
        }
        return false;
    }
}
