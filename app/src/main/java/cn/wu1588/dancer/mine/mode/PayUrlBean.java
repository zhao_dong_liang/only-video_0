package cn.wu1588.dancer.mine.mode;

import android.os.Parcel;
import android.os.Parcelable;

public class PayUrlBean implements Parcelable {


    /**
     * title_1 : 扫码充值备注一栏添加您的会员ID，否则充值无效！
     * title_2 : 无需跳转，请截图保存到手机相册，打开支付宝或微信【扫一扫】付款
     * url : http://telnet.juzishipinpic.com/Uploads/Product/5d1dcc4d8daf0.png
     */

    public String title_1;
    public String title_2;
    public String url;


    protected PayUrlBean(Parcel in) {
        title_1 = in.readString();
        title_2 = in.readString();
        url = in.readString();
    }

    public static final Creator<PayUrlBean> CREATOR = new Creator<PayUrlBean>() {
        @Override
        public PayUrlBean createFromParcel(Parcel in) {
            return new PayUrlBean(in);
        }

        @Override
        public PayUrlBean[] newArray(int size) {
            return new PayUrlBean[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title_1);
        dest.writeString(title_2);
        dest.writeString(url);
    }
}
