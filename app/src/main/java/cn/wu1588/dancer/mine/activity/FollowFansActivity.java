package cn.wu1588.dancer.mine.activity;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.flyco.tablayout.SlidingTabLayout;

import java.util.ArrayList;

import butterknife.BindView;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;
import cn.wu1588.dancer.mine.fragment.MyFollowFragment;

public class FollowFansActivity extends LBaseActivity {

    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.tab_layout)
    SlidingTabLayout tab_layout;
    @BindView(R.id.view_pager)
    ViewPager view_pager;

    String [] titles=new String[]{
            "关注",
            "粉丝"
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setDark(CommonApp.getApplication().isBarTvColor());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follow_fans);
        initViews();
    }

    private void initViews(){
        titleBar.setTitleBarBackgroundDrawable(CommonApp.getApplication().getStateBarDrawable());
        ArrayList<Fragment> fragments=new ArrayList<>();
        fragments.add(MyFollowFragment.newInstance(0));
        fragments.add(MyFollowFragment.newInstance(1));
        tab_layout.setViewPager(view_pager,titles,this,fragments);

    }
}
