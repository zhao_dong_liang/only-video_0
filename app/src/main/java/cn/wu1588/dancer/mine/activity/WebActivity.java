package cn.wu1588.dancer.mine.activity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.wu1588.dancer.model.Ad;
import cn.wu1588.dancer.R;
import com.cnsunrun.commonui.widget.titlebar.TitleBar;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.sunrun.sunrunframwork.utils.EmptyDeal;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.config.Const;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.DownloadUtils;
import cn.wu1588.dancer.common.util.WebViewTool;
import cn.wu1588.dancer.mine.mode.WebUrlBean;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_NOTICE_DETAIL_URL_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_WEB_URL_CODE;

/**
 * 用户协议
 */
public class WebActivity extends LBaseActivity {

    private static final String HTML_FRAME = "<!DOCTYPE html>\n" +
            "<html lang=\"en\" dir=\"ltr\"><head><meta charset=\"utf-8\"><meta name=\"viewport\" content=\"width=device-width,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no\"/><title></title></head><body><div align=\"center\">%s</div></body></html>";

    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.webView)
    WebView webView;
    @BindView(R.id.ll_ad_form)
    LinearLayout llAdForm;
    @BindView(R.id.my_mobile_num)
    EditText my_mobile_num;
    @BindView(R.id.my_name)
    EditText my_name;
    @BindView(R.id.tv_introduction_download)
    TextView tv_introduction_download;
    @BindView(R.id.tv_call_mobile)
    TextView tv_call_mobile;
    @BindView(R.id.tv_call_qq)
    TextView tv_call_qq;
    @BindView(R.id.submit)
    TextView submit;

    private Ad ad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        ButterKnife.bind(this);
        initData();
    }
    private void initData() {
        UIUtils.showLoadDialog(that);
        String type = getIntent().getStringExtra("type");
        String url = getIntent().getStringExtra("url");
        String html = getIntent().getStringExtra("html");
        boolean ignoreType = getIntent().getBooleanExtra("ignoreType", false);
        llAdForm.setVisibility(View.GONE);
        if (!ignoreType) {
            if (Const.USER_PROTOCOL.equals(type)){
                BaseQuestStart.getWebUrl(that);
            }else if ("recharge".equals(type)){
                titleBar.setTitle("在线充值");
            } else {
                titleBar.setTitle("公告详情");
                BaseQuestStart.getNoticeDetailUrl(that,type);
            }
        } else {
            titleBar.setTitle("");
        }

        if (!EmptyDeal.isEmpy(url)) {
            WebViewTool.webviewDefaultConfig(webView);
            webView.loadUrl(url);
        } else if (!EmptyDeal.isEmpy(html)) {
            ad = getIntent().getParcelableExtra("ad");
            llAdForm.setVisibility(View.VISIBLE);
            submit.setOnClickListener(v -> {
                String mobile = my_mobile_num.getText().toString();
                String name = my_name.getText().toString();
                UIUtils.shortM(String.format("mobile: %s, name: %s", mobile, name));
            });
            tv_introduction_download.setVisibility(TextUtils.isEmpty(ad.introduction_url) ? View.GONE : View.VISIBLE);
            tv_introduction_download.setOnClickListener(v -> {
                if (!TextUtils.isEmpty(ad.introduction_url)) {
                    DownloadUtils downloadUtils = new DownloadUtils(WebActivity.this);
                    downloadUtils.download(ad.introduction_url, null);
                } else {
                    UIUtils.shortM("暂时没有下载资料哦，敬请期待");
                }
            });
            tv_call_mobile.setVisibility(TextUtils.isEmpty(ad.phone) ? View.GONE : View.VISIBLE);
            tv_call_mobile.setOnClickListener(v -> {
                CommonIntent.callPhone(WebActivity.this, ad.phone);
            });
            tv_call_qq.setText(ad.qq);
            tv_call_qq.setVisibility(TextUtils.isEmpty(ad.qq) ? View.GONE : View.VISIBLE);
            tv_call_qq.setOnClickListener(v -> {

                ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);

                ClipData mClipData = ClipData.newPlainText("Label", "这里是要复制的文字");
                cm.setPrimaryClip(mClipData);

                UIUtils.shortM("qq复制成功，快去qq里面增加好友咨询吧～");
            });
            webView.loadDataWithBaseURL(null, String.format(HTML_FRAME, Html.fromHtml(html)), "text/html", "UTF-8", null);
        }
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        super.nofityUpdate(requestCode, bean);
        switch (requestCode) {
            case GET_WEB_URL_CODE:
            case GET_NOTICE_DETAIL_URL_CODE:
                if (bean.status == 1) {
                    WebUrlBean webUrlBean=bean.Data();
                    String url = webUrlBean.h5;
                    if (!EmptyDeal.isEmpy(url)) {
                        WebViewTool.webviewDefaultConfig(webView);
                        webView.loadUrl(url);
                    }
                }
                break;
        }
    }
}
