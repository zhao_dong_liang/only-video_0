package cn.wu1588.dancer.mine.adapter;

import android.view.View;
import android.widget.TextView;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import java.util.Arrays;

import cn.wu1588.dancer.common.widget.MomentPicView;
import cn.wu1588.dancer.mine.mode.MyFeedBackBean;

public class MyFeedBackAdapter extends BaseQuickAdapter<MyFeedBackBean, BaseViewHolder> {

    public MyFeedBackAdapter() {
        super(R.layout.item_my_feed_back_layout);
    }

    @Override
    protected void convert(BaseViewHolder helper, MyFeedBackBean item) {
        MomentPicView momentPicView = helper.getView(R.id.nineGrid);
        helper.setText(R.id.tvTitle, item.content);
        final TagFlowLayout tagFlowLayout = (TagFlowLayout) helper.getView(R.id.tagFlwLayout);
        tagFlowLayout.setAdapter(new TagAdapter<String>(Arrays.asList(item.title)) {
            @Override
            public View getView(FlowLayout parent, int position, String o) {
                TextView tv = (TextView) mLayoutInflater.inflate(R.layout.item_simple_type_text, tagFlowLayout, false);
                tv.setText(o);
                return tv;
            }
        });
//        momentPicView.setImageUrls(TestTool.list2StringList(image));
    }
}
