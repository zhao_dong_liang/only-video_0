package cn.wu1588.dancer.mine.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.kongzue.baseframework.interfaces.BindView;
import com.kongzue.baseframework.interfaces.DarkStatusBarTheme;
import com.kongzue.baseframework.interfaces.Layout;
import com.kongzue.baseframework.util.JumpParameter;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.base.BaseAty;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;

@Layout(R.layout.activity_update_pass)
@DarkStatusBarTheme(true)
public class UpdatePassActivity extends BaseAty {

    @BindView(R.id.titleBar)
    private TitleBar titleBar;
    @BindView(R.id.password_edit)
    private EditText password_edit;
    @BindView(R.id.get_code_bt)
    private Button get_code_bt;
    @BindView(R.id.new_password_edit)
    private EditText new_password_edit;

    public static void start(Context context) {
        Intent intent = new Intent(context, UpdatePassActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void initViews() {

    }

    @Override
    public void initDatas(JumpParameter parameter) {

    }

    @Override
    public void setEvents() {
        get_code_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
}