package cn.wu1588.dancer.mine.adapter;

import android.graphics.Paint;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.mine.mode.GoldMealBean;

/**
 * Created by zhou on 2021/1/6 19:49.
 */
public class WbAdapter extends BaseQuickAdapter<GoldMealBean, BaseViewHolder> {

    private int pos;

    public WbAdapter() {
        super(R.layout.item_wubi_layout_v3);
    }

    @Override
    protected void convert(BaseViewHolder helper, GoldMealBean item) {

//        helper.addOnClickListener(R.id.flContainer);
        TextView tvYuanPrice = helper.getView(R.id.tvYuanPrice);
        tvYuanPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);// 设置中划线并加清晰
        tvYuanPrice.getPaint().setAntiAlias(true);
        helper.setBackgroundRes(R.id.flContainer,helper.getAdapterPosition()==pos?R.drawable.shape_taocan_bg_sel:R.drawable.shape_taocan_bg_nor);
        helper.setText(R.id.wubi_number,item.title)
                .setText(R.id.tvPrice,mContext.getString(R.string.price,item.discount_price))
                .setText(R.id.tvYuanPrice,mContext.getString(R.string.price,item.price));

    }

    public void setSelectedPos(int position) {
        this.pos=position;
        notifyDataSetChanged();
    }

}
