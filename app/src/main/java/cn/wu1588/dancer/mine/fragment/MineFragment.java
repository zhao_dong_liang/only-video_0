package cn.wu1588.dancer.mine.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.cnsunrun.commonui.widget.titlebar.TitleBar;
import com.makeramen.roundedimageview.RoundedImageView;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.sunrun.sunrunframwork.utils.EmptyDeal;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.BuildConfig;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.mine.adapter.HistoryHorizAdapter;
import cn.wu1588.dancer.mine.adapter.MyCollectionHorizAdapter;
import cn.wu1588.dancer.mine.mode.AdverBean;
import cn.wu1588.dancer.mine.mode.HistoryAndLoveBean;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.config.RongIMHelper;
import cn.wu1588.dancer.common.event.MessageEvent;
import cn.wu1588.dancer.common.model.LoginInfo;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.common.util.LoginUtil;
import cn.wu1588.dancer.common.util.RecycleHelper;
import io.rong.imkit.RongIM;
import io.rong.imlib.model.CSCustomServiceInfo;
import io.rong.imlib.model.UserInfo;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_ADVER_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_HIS_RECORD_LOVE_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_USER_INFO_CODE;

/**
 * 导航-我的
 */
public class MineFragment extends LBaseFragment {

    @BindView(R.id.tvNikeName)
    TextView tvNikeName;
    @BindView(R.id.tvIDCard)
    TextView tvIDCard;
    @BindView(R.id.tvEndTime)
    TextView tvEndTime;
    @BindView(R.id.tvMember)
    TextView tvMember;
    @BindView(R.id.rIvStar)
    RoundedImageView rIvStar;
    @BindView(R.id.llTuiguang)
    LinearLayout llTuiguang;
    @BindView(R.id.llFankui)
    LinearLayout llFankui;
    @BindView(R.id.llTongzhi)
    LinearLayout llTongzhi;
    @BindView(R.id.llJiaoliuqun)
    LinearLayout llJiaoliuqun;
    @BindView(R.id.rIvGuanggao)
    RoundedImageView rIvGuanggao;
    @BindView(R.id.ivLishi)
    ImageView ivLishi;
    @BindView(R.id.rlHistory)
    RelativeLayout rlHistory;
    @BindView(R.id.recyclerViewLishi)
    RecyclerView recyclerViewLishi;
    @BindView(R.id.ivLike)
    ImageView ivLike;
    @BindView(R.id.rlShoucang)
    RelativeLayout rlShoucang;
    @BindView(R.id.ivShezhi)
    ImageView ivShezhi;
    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.llMemberLayot)
    LinearLayout llMemberLayot;
    @BindView(R.id.tvHistoryRecordTitle)
    TextView tvHistoryRecordTitle;
    @BindView(R.id.tvCollectionTitle)
    TextView tvCollectionTitle;
    @BindView(R.id.recyclerViewLove)
    RecyclerView recyclerViewLove;
    @BindView(R.id.view_line)
    View viewLine;
    @BindView(R.id.tvTitleName)
    TextView tvTitleName;
    @BindView(R.id.tv_register)
    TextView tvRegister;
    @BindView(R.id.tv_login)
    TextView tvLogin;

    @BindView(R.id.tv_gold_count)
    TextView tvGoldCount;
    @BindView(R.id.ll_buy_gold)
    LinearLayout llBuyGold;
    @BindView(R.id.ll_id_card)
    LinearLayout llIdCard;
    @BindView(R.id.ll_register_login)
    LinearLayout llRegisterLogin;
    private HistoryHorizAdapter historyHorizAdapter;
    private MyCollectionHorizAdapter myCollectionHorizAdapter;
    private String adUrl;
    private LoginInfo loginInfo;
    private UserInfo chatUserInfo;


    @Override
    public int getLayoutRes() {
        return R.layout.fragment_mine;
    }

    public static MineFragment newInstance() {
        MineFragment mineFragment = new MineFragment();
        Bundle bundle = new Bundle();
        mineFragment.setArguments(bundle);
        return mineFragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        initEventBus();
        if (LoginUtil.startLogin(getActivity())){
            llRegisterLogin.setVisibility(View.VISIBLE);
        }else {
            llIdCard.setVisibility(View.VISIBLE);
        }
    }

    private void initViews() {
        RecycleHelper.setLinearLayoutManager(recyclerViewLishi, LinearLayoutManager.HORIZONTAL);
        RecycleHelper.setLinearLayoutManager(recyclerViewLove, LinearLayoutManager.HORIZONTAL);
        historyHorizAdapter = new HistoryHorizAdapter();
        myCollectionHorizAdapter = new MyCollectionHorizAdapter();
        recyclerViewLishi.setAdapter(historyHorizAdapter);
        recyclerViewLove.setAdapter(myCollectionHorizAdapter);

        historyHorizAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                HistoryAndLoveBean.HistoryBean.InfoBean item = historyHorizAdapter.getItem(position);
                CommonIntent.startMoviePlayerActivity(that, item.mo_id);
            }
        });
        myCollectionHorizAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                HistoryAndLoveBean.LoveBean.InfoBeanX item = myCollectionHorizAdapter.getItem(position);
                CommonIntent.startMoviePlayerActivity(that, item.mo_id);
            }
        });

    }

    @Override
    public void onResume() {
        initData();
        super.onResume();
    }

    private void initData() {
        BaseQuestStart.getUserInfo(MineFragment.this);
        BaseQuestStart.getHisRecordAndLove(MineFragment.this);
        BaseQuestStart.getAdvertisementData(MineFragment.this);
    }


    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode) {
            case GET_USER_INFO_CODE:
                if (bean.status == 1) {
                    loginInfo = bean.Data();
                    if (loginInfo != null) {
                        tvGoldCount.setText(loginInfo.gold_account);
                        RongIMHelper.connect(loginInfo.r_token);
                        loginInfo.setUid(loginInfo.id);
                        GlideMediaLoader.loadHead(that, rIvStar, loginInfo.icon);
                        tvNikeName.setText(loginInfo.nickname);
                        tvIDCard.setText(getString(R.string.identity_code, loginInfo.identity_id));
                        tvTitleName.setText(loginInfo.title_name);
                        if (loginInfo.isMember()) {
                            tvEndTime.setText(loginInfo.member_end_time);
                            tvMember.setText(loginInfo.title);
                        }
                        Config.putLoginInfo(loginInfo);
                    }
                }else {
                    llRegisterLogin.setVisibility(View.VISIBLE);
                    tvEndTime.setText("暂无");
                    tvGoldCount.setText("0");
                    llIdCard.setVisibility(View.GONE);
                }
                break;
            case GET_HIS_RECORD_LOVE_CODE:
                if (bean.status == 1) {
                    HistoryAndLoveBean historyAndLoveBean = bean.Data();
                    updateUi(historyAndLoveBean);
                }
                break;
            case GET_ADVER_CODE:
                if (bean.status == 1) {
                    List<AdverBean> adverBeans = bean.Data();
                    if (!EmptyDeal.isEmpy(adverBeans)) {
                        adUrl = adverBeans.get(0).url;
                        rIvGuanggao.setVisibility(View.VISIBLE);
                        GlideMediaLoader.load(that, rIvGuanggao, adverBeans.get(0).image, R.drawable.ic_ad_detail_place);
                    } else {
                        rIvGuanggao.setVisibility(View.GONE);
                    }
                }
                break;
        }
        super.nofityUpdate(requestCode, bean);
    }

    private void updateUi(HistoryAndLoveBean historyAndLoveBean) {
        if (!EmptyDeal.isEmpy(historyAndLoveBean)) {
            tvHistoryRecordTitle.setText(historyAndLoveBean.history.title);
            tvCollectionTitle.setText(historyAndLoveBean.love.title);
            historyHorizAdapter.setNewData(historyAndLoveBean.history.info);
            myCollectionHorizAdapter.setNewData(historyAndLoveBean.love.info);
        }

    }

    @OnClick({R.id.llMemberLayot,R.id.tv_login,R.id.tv_register, R.id.ll_buy_gold, R.id.tvEndTime, R.id.tvMember, R.id.rIvStar, R.id.llTuiguang, R.id.llFankui, R.id.llTongzhi, R.id.llJiaoliuqun, R.id.rIvGuanggao, R.id.rlHistory, R.id.rlShoucang, R.id.ivShezhi})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llMemberLayot://会员
            case R.id.tvMember:
            case R.id.ll_buy_gold:
                if (LoginUtil.startLogin(getActivity())) {
                    CommonIntent.startLoginActivity(that);
                } else {
                    CommonIntent.startRechargeActivity(that,0);
                }
//                CommonIntent.startRechargeMemberActivity(that);
                break;
            case R.id.tvEndTime:
                break;
            case R.id.rIvStar:
                if (LoginUtil.startLogin(getActivity())) {
                    CommonIntent.startLoginActivity(that);
                } else {
                    CommonIntent.startUserInfoActivity(that);
                }
                break;
            case R.id.llTuiguang://我要推广
                CommonIntent.startTuiGuangActivity(that);
                break;
            case R.id.llFankui://意见反馈
                if (LoginUtil.startLogin(getActivity())) {
                    CommonIntent.startLoginActivity(that);
                } else {
                    CommonIntent.startFeedBackActivity(that);
                }
                break;
            case R.id.llTongzhi://通知
                CommonIntent.startNoticeActivity(that);
                break;
            case R.id.llJiaoliuqun://火爆交流群
//                String url="mqqwpa://im/chat?chat_type=wpa&uin=&version=1";
//                String url="https://potato.im/joinchat/aMVjvsqvhTT4vvHCNFc11Q";
//                String url="https://pt.im/juzivip";
//                openApp(url);
                if (LoginUtil.startLogin(getActivity())) {
                    CommonIntent.startLoginActivity(that);
                } else {
                    tocus();
                }
                break;
            case R.id.rIvGuanggao://广告
                if (!EmptyDeal.isEmpy(adUrl)) {
                    CommonIntent.startBrowser(that, adUrl);
                }
                break;
            case R.id.rlHistory://历史记录
                CommonIntent.startHistoryRecordActivity(that);
                break;
            case R.id.rlShoucang://我的收藏
                if (LoginUtil.startLogin(getActivity())) {
                    CommonIntent.startLoginActivity(that);
                } else {
                    CommonIntent.startMyCollectionActivity(that);
                }
                break;
            case R.id.ivShezhi://设置
                CommonIntent.startSettingActivity(that);
                break;
            case R.id.tv_login:
                CommonIntent.startLoginActivity(that);
                break;
            case R.id.tv_register:
                CommonIntent.startRegisterActivity(that);
                break;
        }
    }

    private void tocus() {
        //联系客服
        if (loginInfo != null) {
            CSCustomServiceInfo.Builder builder = new CSCustomServiceInfo.Builder();
            builder.nickName(loginInfo.nickname);
            builder.userId(loginInfo.r_token);
            builder.portraitUrl(loginInfo.icon);
            RongIM.getInstance().startCustomerServiceChat(getActivity(), BuildConfig.CUSTOMER_SERVICEID, "在线客服", builder.build());
            //图像信息
            RongIM.setUserInfoProvider(new RongIM.UserInfoProvider() {
                @Override
                public UserInfo getUserInfo(String userId) {
                    Log.d("联系客服", "s====" + userId);
                    return findByUserInfo(userId);
                }
            }, true);
        }
    }

    private UserInfo findByUserInfo(String userId) {
        if (!TextUtils.isEmpty(userId)) {
            chatUserInfo = new UserInfo(loginInfo.r_token, loginInfo.nickname, Uri.parse(loginInfo.icon));
        }
        return chatUserInfo;
    }

    private void openApp(String url) {
//        Uri data = Uri.parse(url);
//        Intent intent = new Intent(Intent.ACTION_VIEW,data);
//        //保证新启动的APP有单独的堆栈，如果希望新启动的APP和原有APP使用同一个堆栈则去掉该项
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        try {
//           startActivity(intent);
//        } catch (Exception e) {
//            e.printStackTrace();
//            UIUtils.shortM("没有匹配的APP，请下载安装");
//        }
        Intent intent;
        try {
            intent = Intent.parseUri(url,
                    Intent.URI_INTENT_SCHEME);
            intent.addCategory(Intent.CATEGORY_BROWSABLE);
            //保证新启动的APP有单独的堆栈，如果希望新启动的APP和原有APP使用同一个堆栈则去掉该项
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setComponent(null);
            startActivity(intent);
            finish();
        } catch (Exception e) {
            UIUtils.shortM("没有匹配的APP，请下载安装");
            e.printStackTrace();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void eventBusMethod(MessageEvent event) {
        String type = event.getType();
        if (type.equals("login_success")){
            if (LoginUtil.startLogin(getActivity())){
                llRegisterLogin.setVisibility(View.VISIBLE);
                llIdCard.setVisibility(View.GONE);
            }else {
                llIdCard.setVisibility(View.VISIBLE);
                llRegisterLogin.setVisibility(View.GONE);
            }
            initData();
        }
    }
}
