package cn.wu1588.dancer.mine.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.cnsunrun.commonui.widget.image.RoundImageView;
import com.my.toolslib.http.utils.LzyResponse;
import com.sunrun.sunrunframwork.utils.EmptyDeal;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.common.util.PageLimitDelegate;
import cn.wu1588.dancer.common.widget.LoadDialogUtils;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;
import cn.wu1588.dancer.mine.adapter.MyFollowVideoAdapter;
import cn.wu1588.dancer.mine.mode.MyFollowVideoBean;

public class MyFollowActivity extends LBaseActivity implements BaseQuickAdapter.OnItemChildClickListener {
    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.user_layout)
    ConstraintLayout user_layout;

    private MyFollowVideoAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setDark(CommonApp.getApplication().isBarTvColor());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_follow);
        titleBar.setTitleBarBackgroundDrawable(CommonApp.getApplication().getStateBarDrawable());
        initData();
    }

    PageLimitDelegate<MyFollowVideoBean> pageLimitDelegate=new PageLimitDelegate<>(new PageLimitDelegate.DataProvider() {
        @Override
        public void loadData(int page) {
            if (page==1){
                LoadDialogUtils.showDialog(that);
            }
            BaseQuestStart.getAttentionList(MyFollowActivity.this, Config.getLoginInfo().id);
        }
    });
    private void initData(){
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter=new MyFollowVideoAdapter();
        adapter.setOnItemChildClickListener(this::onItemChildClick);
        recyclerView.setAdapter(adapter);
        pageLimitDelegate.attach(null,recyclerView,adapter);
    }

    @Override
    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
        super.nofityUpdateUi(requestCode, response, view);
         //获取关注的列表
            List<MyFollowVideoBean> myFollowVideoBeans= (List<MyFollowVideoBean>) response.data;
            pageLimitDelegate.setData(myFollowVideoBeans);
            if (!EmptyDeal.isEmpy(myFollowVideoBeans)){
                user_layout.setVisibility(View.VISIBLE);
            }
        if (pageLimitDelegate.page==1){
            followVideoBeans=myFollowVideoBeans;
            setUserLayout(myFollowVideoBeans);
        }
    }

    @OnClick({R.id.all_tv,R.id.linear01,R.id.linear02,R.id.linear03})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.all_tv://进入我的关注和粉丝页面
                CommonIntent.startFollowFansActivity(that);
                break;
            case R.id.linear01:
                CommonIntent.startOtherUserActiivty(that,followVideoBeans.get(0).attention_uid);
                break;
            case R.id.linear02:
                CommonIntent.startOtherUserActiivty(that,followVideoBeans.get(1).attention_uid);
                break;
            case R.id.linear03:
                CommonIntent.startOtherUserActiivty(that,followVideoBeans.get(02).attention_uid);
                break;
        }

    }
    private List<MyFollowVideoBean> followVideoBeans;
private void setUserLayout(List<MyFollowVideoBean> beans){

    for (int i = 0; i < user_layout.getChildCount()-1; i++) {
            LinearLayout view= (LinearLayout) user_layout.getChildAt(i);
            if (i<beans.size()){
                RoundImageView imageView= (RoundImageView) view.getChildAt(0);
                GlideMediaLoader.loadHead(that,imageView,beans.get(i).icon);
                TextView textView= (TextView) view.getChildAt(1);
                textView.setText(beans.get(i).name);
            }else {
                view.setVisibility(View.GONE);
            }
    }
}
    @Override
    public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
     MyFollowVideoBean followVideoBean= (MyFollowVideoBean) adapter.getItem(position);
        switch (view.getId()){
            case R.id.img01:
            CommonIntent.startMoviePlayerActivity(that,followVideoBean.video.get(0).getId());
            break;
            case R.id.img02:
                CommonIntent.startMoviePlayerActivity(that,followVideoBean.video.get(1).getId());
            break;
            case R.id.img03:
                CommonIntent.startMoviePlayerActivity(that,followVideoBean.video.get(2).getId());
                break;
            case R.id.header_img:
            case R.id.go_view:
                CommonIntent.startOtherUserActiivty(that,followVideoBean.attention_uid);
                break;
        }
    }
}
