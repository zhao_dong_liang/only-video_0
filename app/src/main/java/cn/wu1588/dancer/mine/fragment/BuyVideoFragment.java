package cn.wu1588.dancer.mine.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kongzue.baseokhttp.HttpRequest;
import com.kongzue.baseokhttp.listener.ResponseListener;
import com.kongzue.baseokhttp.util.Parameter;
import com.my.toolslib.http.utils.LzyResponse;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.mine.adapter.BuyVideoAdapter;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.PageLimitDelegate;
import cn.wu1588.dancer.my_video.bean.MyVideoBean;
import cn.wu1588.dancer.utils.map.JSONUtils;
import cn.wu1588.dancer.utils.map.MD5Utils;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_PURCHASE_RECORDS;

public class BuyVideoFragment extends LBaseFragment {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private BuyVideoAdapter adapter;

    @Override
    public int getLayoutRes() {
        return R.layout.buy_video_fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        initData();
    }

    private void initData() {
        HttpRequest.GET(getContext(), GET_PURCHASE_RECORDS,
                new Parameter()
                        .add("user_id", Config.getLoginInfo().uid)
                        .add("md5", MD5Utils.md5())
                ,
                new ResponseListener() {
                    @Override
                    public void onResponse(String main, Exception error) {
                        if (error == null) {
                            Map<String, String> jsonMain = JSONUtils.parseKeyAndValueToMap(main);
                            if (jsonMain.get("status").equals("1")) {
                                adapter.setNewData(JSONUtils.parseKeyAndValueToMapList(jsonMain.get("data")));
                            }
                        }
                    }
                });
    }


    private void initViews() {
        adapter = new BuyVideoAdapter();
        recyclerView.setLayoutManager(new LinearLayoutManager(that));
        recyclerView.setAdapter(adapter);
//        pageLimitDelegate.attach(null,recyclerView,adapter);
    }


    //    PageLimitDelegate<MyVideoBean> pageLimitDelegate=new PageLimitDelegate<>(new PageLimitDelegate.DataProvider() {
//        @Override
//        public void loadData(int page) {
//            BaseQuestStart.getMyBuyVideos(BuyVideoFragment.this);
//        }
//    });
//    @Override
//    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
//        super.nofityUpdateUi(requestCode, response, view);
//        if (requestCode==BaseQuestStart.GET_MY_BUY_VIDEOS_CODE){
//           List<MyVideoBean> beans= (List<MyVideoBean>) response.data;
//            pageLimitDelegate.setData(beans);
//        }
//    }
}
