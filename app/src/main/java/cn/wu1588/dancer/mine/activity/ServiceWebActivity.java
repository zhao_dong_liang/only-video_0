package cn.wu1588.dancer.mine.activity;

import android.os.Bundle;
import android.webkit.WebView;

import cn.wu1588.dancer.R;
import com.cnsunrun.commonui.widget.titlebar.TitleBar;
import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.sunrun.sunrunframwork.utils.EmptyDeal;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.wu1588.dancer.base.LBaseActivity;

import cn.wu1588.dancer.common.util.WebViewTool;

public class ServiceWebActivity extends LBaseActivity {

    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.webView)
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        ButterKnife.bind(this);
        initData();
    }
    private void initData() {
        UIUtils.showLoadDialog(that);
        String url = getIntent().getStringExtra("url");
        titleBar.setTitle("服务协议");
        if (!EmptyDeal.isEmpy(url)) {
            WebViewTool.webviewDefaultConfig(webView);
            webView.loadUrl(url);
        }

    }
}
