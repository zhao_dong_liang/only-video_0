package cn.wu1588.dancer.mine.activity;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import cn.wu1588.dancer.R;

import com.flyco.tablayout.SlidingTabLayout;
import com.kongzue.baseframework.interfaces.Layout;
import com.kongzue.baseframework.util.JumpParameter;

import java.util.ArrayList;
import java.util.List;

import cn.wu1588.dancer.base.BaseAty;
import cn.wu1588.dancer.base.ViewPagerFragmentAdapter;
import cn.wu1588.dancer.mine.fragment.CommonProblemFragment;
import cn.wu1588.dancer.mine.fragment.FeedBackFragment;

///反馈建议

@Layout(R.layout.activity_feedback_new)
public class FeedBackActivity extends BaseAty {


    SlidingTabLayout tabLayout;
    ViewPager viewPager;
    ImageView image_finish;

    TextView textTitleBar;

    private String[] mTitles = {"反馈建议", "我的反馈"};
    private List<Fragment> baseFragments;
    private ViewPagerFragmentAdapter mVPAdapter;

    public void initViews() {
        image_finish = findViewById(R.id.image_finish);
        textTitleBar = findViewById(R.id.title_bar);
        viewPager = findViewById(R.id.viewPager);
        tabLayout = findViewById(R.id.tabLayout);
        baseFragments = new ArrayList<>();
        mVPAdapter = new ViewPagerFragmentAdapter(getSupportFragmentManager());

    }

    @Override
    public void initDatas(JumpParameter parameter) {
        textTitleBar.setText("反馈建议");
//        baseFragments.add(CommonProblemFragment.newInstance(1));//常见问题
        baseFragments.add(FeedBackFragment.newInstance());//意见反馈
        baseFragments.add(CommonProblemFragment.newInstance(2));//我的反馈
        mVPAdapter.setFragments(baseFragments);
        viewPager.setAdapter(mVPAdapter);
        viewPager.setCurrentItem(0, false);
        tabLayout.setViewPager(viewPager, mTitles);
    }

    @Override
    public void setEvents() {
        image_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
