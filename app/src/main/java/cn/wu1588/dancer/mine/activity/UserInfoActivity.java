package cn.wu1588.dancer.mine.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.bilibili.boxing.Boxing;
import com.bilibili.boxing.model.config.BoxingConfig;
import com.bilibili.boxing.model.config.BoxingCropOption;
import com.bilibili.boxing.model.entity.BaseMedia;
import com.bilibili.boxing.model.entity.impl.ImageMedia;
import com.bilibili.boxing.utils.BoxingFileHelper;
import com.bilibili.boxing.utils.ImageCompressor;
import com.bilibili.boxing_impl.ui.BoxingActivity;
import com.my.toolslib.StringUtils;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.sunrun.sunrunframwork.utils.EmptyDeal;
import com.unistrong.yang.zb_permission.ZbPermission;
import com.unistrong.yang.zb_permission.ZbPermissionFail;
import com.unistrong.yang.zb_permission.ZbPermissionSuccess;

import java.io.File;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.dialog.SelectPhotoDialog;
import cn.wu1588.dancer.common.model.LoginInfo;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.common.util.AlertDialogUtil;
import cn.wu1588.dancer.common.util.UpLoadUtils;
import de.hdodenhof.circleimageview.CircleImageView;

import static cn.wu1588.dancer.common.config.Const.PERMISSION;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.POST_UPDATE_USER_INFO_CODE;

/**
 * 个人资料
 */
public class UserInfoActivity extends LBaseActivity implements UpLoadUtils.UpLoadCallBack {
    @BindView(R.id.image_finish)
    ImageView image_finish;
    @BindView(R.id.title_bar)
    TextView titleBar;
    @BindView(R.id.ivUserPhoto)
    CircleImageView ivUserPhoto;
    @BindView(R.id.layoutChangeUserPhoto)
    RelativeLayout layoutChangeUserPhoto;
    @BindView(R.id.tvNickname)
    TextView tvNickname;
    @BindView(R.id.layoutChangeNickname)
    RelativeLayout layoutChangeNickname;
    @BindView(R.id.tvSex)
    TextView tvSex;
    @BindView(R.id.layoutChangeSex)
    RelativeLayout layoutChangeSex;
    @BindView(R.id.btnSure)
    Button btnSure;
    @BindView(R.id.signLayout)
    View signLayout;
    @BindView(R.id.sign_tv)
    TextView sign_tv;
    @BindView(R.id.layoutRegion)
    View layoutRegion;
    @BindView(R.id.tvPhone)
    TextView tvPhone;
    @BindView(R.id.tvRegion)
    TextView tvRegion;
    @BindView(R.id.tvAuth)
    TextView tvAuth;

    private String headUrl;
    private int sex;
    private String headImageUrl;
    private String nikeName;
    private File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setDark(CommonApp.getApplication().isBarTvColor());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
        ButterKnife.bind(this);
        initViews();
        image_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initViews() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //修改为深色
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        titleBar.setText("账号管理");
//        titleBar.setTitleBarBackgroundDrawable(CommonApp.getApplication().getStateBarDrawable());
        LoginInfo loginInfo = Config.getLoginInfo();
        if (loginInfo != null) {
            if (Config.getLoginInfo().icon != null) {
                GlideMediaLoader.loadHead(that, ivUserPhoto, Config.getLoginInfo().icon);
            }
            tvPhone.setText(Config.getLoginInfo().mobile);
            tvNickname.setText(Config.getLoginInfo().nickname);
            tvSex.setText(Config.getLoginInfo().gender);
            if (!TextUtils.isEmpty(Config.getLoginInfo().gender)) {
                sex = Config.getLoginInfo().gender.equals("男") ? 1 : 2;
            }
            nikeName = Config.getLoginInfo().nickname;
            headImageUrl = Config.getLoginInfo().icon;
            String signature = Config.getLoginInfo().signature;
            if (!StringUtils.isNull(signature)) {
                sign_tv.setText(signature);
            }
            proName = Config.getLoginInfo().province;
            cityName = Config.getLoginInfo().city;
            if (!StringUtils.isNull(proName)) {
                tvRegion.setText(proName + " " + cityName);
            }
            int talent = Integer.valueOf(Config.getLoginInfo().is_talent);
            if (talent != 0) {
                tvAuth.setText("已认证");
            }


        }
    }

    @OnClick({R.id.image_finish, R.id.layoutChangeUserPhoto, R.id.layoutChangeNickname, R.id.layoutChangeSex, R.id.signLayout
            , R.id.layoutRegion, R.id.layoutBindPhone, R.id.layoutAuth})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layoutChangeUserPhoto:
                choosePhoto();
                break;
            case R.id.layoutChangeNickname://昵称
                CommonIntent.startUpdateInfoActivity(that, tvNickname.getText().toString());
                break;
            case R.id.layoutChangeSex:
                AlertDialogUtil.selectPhotoDialog(that, "女", "男", new SelectPhotoDialog.OnSelectItemClickListener() {
                    @Override
                    public void selectItemOne(View view) {
                        sex = 2;
                        tvSex.setText("女");
                    }

                    @Override
                    public void selectItemTwo(View view) {
                        sex = 1;
                        tvSex.setText("男");
                    }
                });
                break;
            case R.id.signLayout://个性签名
                CommonIntent.startEditUserSignActivity(that, sign_tv.getText().toString(), 2);
                break;
            case R.id.btnSure://提交
                if (checkInput()) {
                    BaseQuestStart.postUserInfo(that, headUrl, sex, nikeName, sign_tv.getText().toString(), proName, cityName);
                }
                break;
            case R.id.layoutRegion://选择地址
                CommonIntent.startSelectProvinceActivity(that);
                break;
            case R.id.layoutBindPhone://绑定手机号
                CommonIntent.startBindPhoneActivity(this, 1);
                break;
            case R.id.layoutAuth://达人认证
                if (Config.getLoginInfo().is_talent.equals("0")) {
                    CommonIntent.startAuthActivity(this);
                }
                break;
            case R.id.image_finish:
                finish();
                break;
        }
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        if (requestCode == POST_UPDATE_USER_INFO_CODE && bean.status == 1) {
            finish();
        } else {
            UIUtils.shortM(bean.msg);
        }
        super.nofityUpdate(requestCode, bean);
    }

    private boolean checkInput() {
        nikeName = tvNickname.getText().toString().trim();
//        if (EmptyDeal.isEmpy(headImageFilePath)) {
//            UIUtils.shortM("请选择头像");
//            return false;
//        } else
        if (EmptyDeal.isEmpy(nikeName)) {
            UIUtils.shortM("请输入昵称");
            return false;
        } else if (sex == 0) {
            UIUtils.shortM("请选择性别");
            return false;
        }
        return true;
    }

    private void choosePhoto() {
        ZbPermission.with(that)
                .addRequestCode(PERMISSION)
                .permissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                .request();
    }

    @ZbPermissionSuccess(requestCode = PERMISSION)
    public void permissionSuccess() {
        String cachePath = BoxingFileHelper.getCacheDir(that);
        if (TextUtils.isEmpty(cachePath)) {
            Toast.makeText(that, R.string.storage_deny, Toast.LENGTH_SHORT).show();
            return;
        }
        Uri destUri = new Uri.Builder()
                .scheme("file")
                .appendPath(cachePath)
                .appendPath(String.format(Locale.US, "%s.jpg", System.currentTimeMillis()))
                .build();
        //这里设置的config的mode是single_img  就是单选图片的模式  支持相机
        BoxingConfig singleCropImgConfig = new BoxingConfig(BoxingConfig.Mode.SINGLE_IMG)
                .needCamera()
                .needGif()
                .withCropOption(new BoxingCropOption(destUri).withMaxResultSize(200, 200).aspectRatio(1, 1));
        Boxing.of(singleCropImgConfig).withIntent(that, BoxingActivity.class).start(that, 2);
    }

    private String proName = "";
    private String cityName = "";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 1) {
                if (data != null) {
                    String title = data.getStringExtra("content");
                    tvNickname.setText(title);
                }
            }
            if (requestCode == 2) {
                //相册获取
                List<BaseMedia> medias = Boxing.getResult(data);
                //这里一定要做非空的判断
                if (medias != null && medias.size() > 0) {
                    BaseMedia baseMedia = medias.get(0);
                    String path;
                    if (baseMedia instanceof ImageMedia) {
                        ((ImageMedia) baseMedia).compress(new ImageCompressor(that));
                        path = ((ImageMedia) baseMedia).getThumbnailPath();
                    } else {
                        path = baseMedia.getPath();
                    }
                    file = new File(path);
                    UpLoadUtils upLoadUtils = new UpLoadUtils(that);
                    upLoadUtils.setUpLoadCallBack(this::upLoadResult);
                    upLoadUtils.upLoad(file.toString());
                    //上传
                }
            }
        }
        if (requestCode == 1) {//绑定手机号
            if (data != null) {
                String phone = data.getStringExtra("phone");
                if (!StringUtils.isNull(phone)) {
                    tvPhone.setText(phone);
                }
            }
        } else if (requestCode == 2) {//个性签名
            if (data != null) {
                String text = data.getStringExtra("text");
                if (!StringUtils.isNull(text)) {
                    sign_tv.setText(text);
                }
            }
        } else if (requestCode == 3) {//选择省市
            if (data != null) {
                proName = data.getStringExtra("proName");
                cityName = data.getStringExtra("cityName");
                if (!StringUtils.isNull(proName) && !StringUtils.isNull(cityName)) {
                    tvRegion.setText(proName + " " + cityName);
                }
            }
        }
    }

    @ZbPermissionFail(requestCode = PERMISSION)
    public void permissionFail() {
        UIUtils.shortM("请在设置里面打开应用使用相机的权限");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        ZbPermission.onRequestPermissionsResult(UserInfoActivity.this, requestCode, permissions, grantResults);
    }

    @Override
    public void upLoadResult(String url, File file) {
        GlideMediaLoader.loadHead(that, ivUserPhoto, url);
        headUrl = url;
    }
}
