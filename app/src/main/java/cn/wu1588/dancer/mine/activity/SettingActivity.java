package cn.wu1588.dancer.mine.activity;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;
import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.sunrun.sunrunframwork.utils.AppUtils;
import com.sunrun.sunrunframwork.utils.DataCleanManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.config.Const;
import cn.wu1588.dancer.common.dialog.MessageTipDialog;
import cn.wu1588.dancer.common.dialog.VersionTipDiglog;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.common.util.ConstantValue;
import cn.wu1588.dancer.common.util.SPUtils;
import cn.wu1588.dancer.home.mode.VersionBean;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_VERSION_UPDATE_CODE;

/**
 * 设置1
 */
public class SettingActivity extends LBaseActivity {

//    @BindView(R.id.title_bar_rl)
//    RelativeLayout titleBar;
    @BindView(R.id.userProtocol)
    TextView userProtocol;
    @BindView(R.id.itemClearCache)
    LinearLayout itemClearCache;
    @BindView(R.id.itemVersion)
    LinearLayout itemVersion;
    @BindView(R.id.logout)
    TextView logout;
    @BindView(R.id.tvVersion)
    TextView tvVersion;
    @BindView(R.id.tvCache)
    TextView tvCache;
    @BindView(R.id.image_finish)
    ImageView image_finish;
    @BindView(R.id.title_bar)
    TextView title_bar;
    private String totalCacheSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setDark(CommonApp.getApplication().isBarTvColor());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settiing);
        ButterKnife.bind(this);
        int need_login = (int) SPUtils.get(this,ConstantValue.NEED_LOGIN, 1);
        if (need_login==1) {
            logout.setVisibility(View.VISIBLE);
        } else {
            logout.setVisibility(View.GONE);
        }
        totalCacheSize = DataCleanManager.getTotalCacheSize(that);
        initViews();

    }

    private void initViews() {
        tvCache.setText(totalCacheSize);
        tvVersion.setText("V" + AppUtils.getVersionName(that));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //修改为深色
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }

    @OnClick({R.id.image_finish,R.id.userProtocol, R.id.itemClearCache, R.id.itemVersion, R.id.logout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_finish:
                finish();
                break;
            case R.id.userProtocol:
                CommonIntent.startWebActivity(that, Const.USER_PROTOCOL, "");
                break;
            case R.id.itemClearCache:
                MessageTipDialog.newInstance().setTitleTxt("清除缓存").setContentTxt("是否要清除缓存?")
                        .setOnSubmitAction(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                DataCleanManager.clearAllCache(that);
                                totalCacheSize = DataCleanManager.getTotalCacheSize(that);
                                tvCache.setText(totalCacheSize);
                                ToastUtils.shortToast("缓存清理成功");
                            }
                        })
                        .show(getSupportFragmentManager(), "MessageTipDialog");
                break;
            case R.id.itemVersion:
                BaseQuestStart.getVersionUpdate(this);
                break;
            case R.id.logout:
                boolean isLogin = (boolean) SPUtils.get(that, ConstantValue.IS_LOGIN, false);
                if (isLogin) {
                    Config.putLoginInfo(null);
                    SPUtils.put(that, ConstantValue.IS_LOGIN, false);
                    finish();
                } else {
                    ToastUtils.shortToast("您未登录");
                }

                break;
        }
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode) {
            case GET_VERSION_UPDATE_CODE:
                if (bean.status == 1) {
                    VersionBean versionBean = bean.Data();
                    if (versionBean.getIs_box() == 1) {
                        VersionTipDiglog.newInstance().setVersionData(versionBean).show(getSupportFragmentManager(), "VersionTipDiglog");
                    }
                } else {
                    UIUtils.shortM(bean.msg);
                }
                break;
        }
        super.nofityUpdate(requestCode, bean);
    }
}
