package cn.wu1588.dancer.mine.adapter;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

public class CommentsAdapter extends BaseQuickAdapter<String, BaseViewHolder> {
    public CommentsAdapter() {
        super(R.layout.item_comments_layout);
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {

    }
}
