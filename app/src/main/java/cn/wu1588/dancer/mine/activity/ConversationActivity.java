package cn.wu1588.dancer.mine.activity;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import cn.wu1588.dancer.R;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.utils.log.Logger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.wu1588.dancer.base.ConversationHeightFix;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.config.IMUserInfoProvider;
import cn.wu1588.dancer.common.config.RongIMHelper;
import cn.wu1588.dancer.common.event.DefaultEvent;
import cn.wu1588.dancer.common.event.RefreshIMTokenEvent;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;
import io.rong.imkit.RongIM;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.CSCustomServiceInfo;
import io.rong.imlib.model.Message;
import io.rong.imlib.model.MessageContent;
import io.rong.imlib.model.UserInfo;
import io.rong.message.ImageMessage;
import io.rong.message.RichContentMessage;
import io.rong.message.TextMessage;
import io.rong.message.VoiceMessage;


/**
 * 咵天-聊天页面
 */
public class ConversationActivity extends LBaseActivity {


    boolean isGroup;
    String targetId = "";
    @BindView(R.id.imgChatBg)
    ImageView imgChatBg;
    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.layContent)
    LinearLayout layContent;
    @BindView(R.id.layRootView)
    RelativeLayout layRootView;
    private String courseId;
    private CSCustomServiceInfo customServiceInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logger.E("初始化");
        initEventBus();
        setContentView(R.layout.activity_chat_conversation);
        ButterKnife.bind(this);
        Uri uri = getIntent().getData();
        courseId = getIntent().getExtras().getString("CourseId");
        customServiceInfo = getIntent().getParcelableExtra("customServiceInfo");
        if (uri != null) {
            String title = uri.getQueryParameter("title");
            titleBar.setTitle(title);
            targetId = uri.getQueryParameter("targetId");
            UserInfo userInfo = IMUserInfoProvider.getInstance().getUserInfo(targetId);
            if (userInfo != null) {
                titleBar.setTitle(userInfo.getName());
            }
        }
        titleBar.setRightAction(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchToHumanMode(targetId);
            }
        });
        new ConversationHeightFix(this, layContent);

//        RongIM.setConversationBehaviorListener(new HeadClickLogic(that, targetId));
        if (!RongIMHelper.isConnect()) {
            EventBus.getDefault().post(RefreshIMTokenEvent.newInstance());//刷新Token
            return;
        }
        RongIM.getInstance().setSendMessageListener(onSendMessageListener);
    }

    /**
     * 切换人工客服
     * @param targetId
     */
    private void switchToHumanMode(String targetId) {
        RongIMClient.getInstance().switchToHumanMode(targetId);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadPageData();
    }

    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode) {
//            case NEIGHBORHOOD_IM_CHAT_INFO_CODE:
//                if (bean.status == 1) {
//                    LinkManInfo linkmana = bean.Data();
//                    titleBar.setTitle(linkmana.getNickname());
//                    getSession().put("chatinfo" + targetId, linkmana);
//                } else {
//                    titleBar.setRightVisible(View.GONE);
////                    CommonIntent.startChatAddUserActivity(this, targetId);
////                    UIUtils.shortM("您还不是对方好友");
////                    finish();
//                }
//                break;
//            case NEIGHBORHOOD_IM_COLLECT_CONTENT_CODE:
//            case NEIGHBORHOOD_IM_COLLECT_ATTACHMENT_CODE:
//                UIUtils.shortM(bean);
//                break;
        }
        super.nofityUpdate(requestCode, bean);
    }


    public void refreshChatBg() {
//        ChatBgLogic.loadChatBg(that, imgChatBg, targetId, isGroup);
    }

    @Subscribe
    public void onEvent(DefaultEvent defaultEvent) {
//        if (defaultEvent.match(Const.EVENT_COLLECT)) {
//            UIMessage uiMessage = defaultEvent.getExtra();
//            Message message = uiMessage.getMessage();
//            MessageContent content = message.getContent();
//            String userId = message.getSenderUserId();
//            if (content instanceof TextMessage) {
//                TextMessage textMessage = (TextMessage) content;
//                UIUtils.showLoadDialog(that, "收藏中..");
//            } else if (content instanceof VoiceMessage) {
//                VoiceMessage voiceMessage = (VoiceMessage) content;
//                UIUtils.showLoadDialog(that, "收藏中..");
//                String path = voiceMessage.getUri().getPath();
//            } else if (content instanceof ImageMessage) {
//                ImageMessage imageMessage = (ImageMessage) content;
//                UIUtils.showLoadDialog(that, "收藏中..");
//                String path = imageMessage.getLocalUri() == null ? imageMessage.getThumUri().getPath() : imageMessage.getLocalUri().getPath();
//            }
//        }
    }

    private void loadPageData() {
//        UIUtils.showLoadDialog(this);
        if (!isGroup) {
//            BaseQuestStart.NeighborhoodImChatInfo(this, targetId);
        }
        refreshChatBg();
    }

    private RongIM.OnSendMessageListener onSendMessageListener = new RongIM.OnSendMessageListener() {
        /**
         * 消息发送前监听器处理接口（是否发送成功可以从 SentStatus 属性获取）。
         *
         * @param message 发送的消息实例。
         * @return 处理后的消息实例。
         */
        @Override
        public Message onSend(Message message) {
            if (message.getContent() instanceof TextMessage) {
                TextMessage msg = (TextMessage) message.getContent();
                msg.setExtra(courseId);
            } else if (message.getContent() instanceof ImageMessage) {
                ImageMessage msg = (ImageMessage) message.getContent();
                msg.setExtra(courseId);
            } else if (message.getContent() instanceof VoiceMessage) {//语音消息
                VoiceMessage msg = (VoiceMessage) message.getContent();
                msg.setExtra(courseId);
            } else if (message.getContent() instanceof RichContentMessage) {//图文消息
                RichContentMessage msg = (RichContentMessage) message.getContent();
                msg.setExtra(courseId);
            }

            return message;
        }

        /**
         * 消息在 UI 展示后执行/自己的消息发出后执行,无论成功或失败。
         *
         * @param message              消息实例。
         * @param sentMessageErrorCode 发送消息失败的状态码，消息发送成功 SentMessageErrorCode 为 null。
         * @return true 表示走自己的处理方式，false 走融云默认处理方式。
         */
        @Override
        public boolean onSent(Message message, RongIM.SentMessageErrorCode sentMessageErrorCode) {
            MessageContent messageContent = message.getContent();

//            if (messageContent instanceof TextMessage) {//文本消息
//                TextMessage textMessage = (TextMessage) messageContent;
//                BaseQuestStart.postSendMsg(that, targetId, MSG_TEXT_TYPE, textMessage.getContent(),courseId);
//            } else if (messageContent instanceof ImageMessage) {//图片消息
//                ImageMessage imageMessage = (ImageMessage) messageContent;
//                String path = imageMessage.getLocalUri() == null ? imageMessage.getThumUri().getPath() : imageMessage.getLocalUri().getPath();
//                BaseQuestStart.postSendMsg(that, targetId, MSG_IMG_TYPE, path,courseId);
//            } else if (messageContent instanceof VoiceMessage) {//语音消息
//                VoiceMessage voiceMessage = (VoiceMessage) messageContent;
//                BaseQuestStart.postSendMsg(that, targetId, MSG_VOICE_TYPE, voiceMessage.getUri().getPath(),courseId);
//            } else if (messageContent instanceof RichContentMessage) {//图文消息
//                RichContentMessage richContentMessage = (RichContentMessage) messageContent;
//            }

            return false;
        }
    };
}
