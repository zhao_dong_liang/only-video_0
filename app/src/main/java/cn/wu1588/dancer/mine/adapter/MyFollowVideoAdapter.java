package cn.wu1588.dancer.mine.adapter;

import android.view.View;

import androidx.constraintlayout.widget.Group;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.sunrun.sunrunframwork.utils.EmptyDeal;

import java.util.List;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.mine.mode.MyFollowVideoBean;
import cn.wu1588.dancer.my_video.bean.MyVideoBean;

public class MyFollowVideoAdapter extends BaseQuickAdapter<MyFollowVideoBean, BaseViewHolder> {
    public MyFollowVideoAdapter() {
        super(R.layout.my_follow_video_item);
    }

    @Override
    protected void convert(BaseViewHolder helper, MyFollowVideoBean item) {

        GlideMediaLoader.load(mContext,helper.getView(R.id.header_img),item.icon);

        helper.setText(R.id.name_tv,item.name)
        .setText(R.id.fans_tv,"人气:"+item.attention_num)
        .setText(R.id.video_num_tv,"视频:"+item.video_count);

        Group group01= helper.getView(R.id.group01);
        Group group02= helper.getView(R.id.group02);
        Group group03= helper.getView(R.id.group03);
        List<MyVideoBean> videos = item.video;
        if (!EmptyDeal.isEmpy(videos)){
            switch (videos.size()){
                case 1:
                    group01.setVisibility(View.VISIBLE);
                    group02.setVisibility(View.GONE);
                    group03.setVisibility(View.GONE);
                    GlideMediaLoader.load(mContext,helper.getView(R.id.img01),videos.get(0).getImage(),R.mipmap.img_def);
                    helper.setText(R.id.title_tv01,videos.get(0).getTitle());
                    break;
                case 2:
                    group01.setVisibility(View.VISIBLE);
                    group02.setVisibility(View.VISIBLE);
                    group03.setVisibility(View.GONE);
                    GlideMediaLoader.load(mContext,helper.getView(R.id.img01),videos.get(0).getImage(),R.mipmap.img_def);
                    helper.setText(R.id.title_tv01,videos.get(0).getTitle());
                    GlideMediaLoader.load(mContext,helper.getView(R.id.img02),videos.get(1).getImage(),R.mipmap.img_def);
                    helper.setText(R.id.title_tv02,videos.get(1).getTitle());
                    break;
                case 3:
                    group01.setVisibility(View.VISIBLE);
                    group02.setVisibility(View.VISIBLE);
                    group03.setVisibility(View.VISIBLE);
                    GlideMediaLoader.load(mContext,helper.getView(R.id.img01),videos.get(0).getImage(),R.mipmap.img_def);
                    helper.setText(R.id.title_tv01,videos.get(0).getTitle());
                    GlideMediaLoader.load(mContext,helper.getView(R.id.img02),videos.get(1).getImage(),R.mipmap.img_def);
                    helper.setText(R.id.title_tv02,videos.get(1).getTitle());
                    GlideMediaLoader.load(mContext,helper.getView(R.id.img03),videos.get(2).getImage(),R.mipmap.img_def);
                    helper.setText(R.id.title_tv01,videos.get(2).getTitle());
                    break;
            }

        }else {
            group01.setVisibility(View.GONE);
            group02.setVisibility(View.GONE);
            group03.setVisibility(View.GONE);
        }
        helper.addOnClickListener(R.id.img01,R.id.img02,R.id.img03,R.id.header_img,R.id.go_view);
    }


}
