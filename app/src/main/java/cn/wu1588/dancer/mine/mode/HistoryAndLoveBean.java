package cn.wu1588.dancer.mine.mode;

import java.util.List;

public class HistoryAndLoveBean {

    /**
     * history : {"info":[{"image":"http://www.baidu.com/uploads/aaa/bbb","me_id":"1","mo_id":"4","play_url":"uploads/aaa/bbb","title":"影片4"},{"image":"http://www.baidu.com/uploads/aaa/bbb","me_id":"5","mo_id":"2","play_url":"uploads/aaa/bbb","title":"影片2"},{"image":"http://www.baidu.com/uploads/aaa/bbb","me_id":"7","mo_id":"6","play_url":"uploads/aaa/bbb","title":"影片6"},{"image":"http://www.baidu.com/uploads/aaa/bbb","me_id":"4","mo_id":"1","play_url":"uploads/aaa/bbb","title":"影片1"},{"image":"http://www.baidu.com/Uploads/Movie/5c9db9c9bfaac.jpg","me_id":"6","mo_id":"3","play_url":"uploads/aaa/bbb","title":"影片3"}],"title":"目前历史观看过5部"}
     * love : {"info":[{"image":"http://www.baidu.com/Uploads/Movie/5c9db9c9bfaac.jpg","mem_id":"4","mo_id":"3","play_url":"uploads/aaa/bbb","title":"影片3"},{"image":"http://www.baidu.com/uploads/aaa/bbb","mem_id":"1","mo_id":"1","play_url":"uploads/aaa/bbb","title":"影片1"}],"title":"目前已收藏2部"}
     */

    public HistoryBean history;
    public LoveBean love;


    public static class HistoryBean {
        /**
         * info : [{"image":"http://www.baidu.com/uploads/aaa/bbb","me_id":"1","mo_id":"4","play_url":"uploads/aaa/bbb","title":"影片4"},{"image":"http://www.baidu.com/uploads/aaa/bbb","me_id":"5","mo_id":"2","play_url":"uploads/aaa/bbb","title":"影片2"},{"image":"http://www.baidu.com/uploads/aaa/bbb","me_id":"7","mo_id":"6","play_url":"uploads/aaa/bbb","title":"影片6"},{"image":"http://www.baidu.com/uploads/aaa/bbb","me_id":"4","mo_id":"1","play_url":"uploads/aaa/bbb","title":"影片1"},{"image":"http://www.baidu.com/Uploads/Movie/5c9db9c9bfaac.jpg","me_id":"6","mo_id":"3","play_url":"uploads/aaa/bbb","title":"影片3"}]
         * title : 目前历史观看过5部
         */

        public String title;
        public List<InfoBean> info;


        public static class InfoBean {
            /**
             * image : http://www.baidu.com/uploads/aaa/bbb
             * me_id : 1
             * mo_id : 4
             * play_url : uploads/aaa/bbb
             * title : 影片4
             */

            public String image;
            public String me_id;
            public String mo_id;
            public String play_url;
            public String title;


        }
    }

    public static class LoveBean {
        /**
         * info : [{"image":"http://www.baidu.com/Uploads/Movie/5c9db9c9bfaac.jpg","mem_id":"4","mo_id":"3","play_url":"uploads/aaa/bbb","title":"影片3"},{"image":"http://www.baidu.com/uploads/aaa/bbb","mem_id":"1","mo_id":"1","play_url":"uploads/aaa/bbb","title":"影片1"}]
         * title : 目前已收藏2部
         */

        public String title;
        public List<InfoBeanX> info;


        public static class InfoBeanX {
            /**
             * image : http://www.baidu.com/Uploads/Movie/5c9db9c9bfaac.jpg
             * mem_id : 4
             * mo_id : 3
             * play_url : uploads/aaa/bbb
             * title : 影片3
             */

            public String image;
            public String mem_id;
            public String mo_id;
            public String play_url;
            public String title;

        }
    }
}
