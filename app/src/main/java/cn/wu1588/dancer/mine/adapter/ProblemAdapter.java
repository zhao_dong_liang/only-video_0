package cn.wu1588.dancer.mine.adapter;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import cn.wu1588.dancer.mine.mode.ProblemBean;

public class ProblemAdapter extends BaseQuickAdapter<ProblemBean.QuestionlistBean, BaseViewHolder> {
    public ProblemAdapter() {
        super(R.layout.item_problem_layout);
    }

    @Override
    protected void convert(BaseViewHolder helper, ProblemBean.QuestionlistBean item) {
            helper.setText(R.id.tvTitle,item.title)
                    .setText(R.id.tvContent,item.content);
    }
}
