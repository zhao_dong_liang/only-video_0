package cn.wu1588.dancer.mine.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.aty.my.MyTimeAty;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.mine.adapter.FreeAdvertAdapter;
import cn.wu1588.dancer.mine.mode.FreeAdvertBean;

/**
 * 免费投广告
 */
public class FreeAdvertActivity extends LBaseActivity implements View.OnClickListener {
    @BindView(R.id.activity_free_advert_rv)
    RecyclerView recyclerView;
    @BindView(R.id.activity_free_advert_ll_time)
    LinearLayout llTime;
    FreeAdvertAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_free_advert);
        ButterKnife.bind(this);
        initViews();
        initDatas();
        initListeners();
    }

    private void initViews() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new FreeAdvertAdapter();
        recyclerView.setAdapter(adapter);
    }

    /**
     * 创造并添加假数据
     */
    private void initDatas() {
        List<FreeAdvertBean> beanList = new ArrayList<>();
        FreeAdvertBean advertBean1 = new FreeAdvertBean("1、什么是免费投广告", "免费投广告就是你无需花钱，只需把你在舞者APP上的使用时间用来等额投放广告。比如你昨天在舞者APP上总共使用了2小时，今天平台就可以为你免费投放2小时 的广告。");
        FreeAdvertBean advertBean2 = new FreeAdvertBean("2、如何获取更多的免费投放时间", "除了增加你自己使用时间外，你可以通过发展金粉的方式获得更多的免费投放时间。金粉的使用时间按50%的比例奖励给你。比如一个金粉一天使用了2小时，你就可以获得1个小时的免费投放时间。如果你有10个金粉,每个金粉每天使用2小时，你每天就可以获得共10个小时的免费投放，时间总额无限制，上不封顶。");
        FreeAdvertBean advertBean3 = new FreeAdvertBean("3、什么是金粉", "金粉就是通过你在舞者APP里的帐号二维码、邀请码发 展的新用户。");
        FreeAdvertBean advertBean4 = new FreeAdvertBean("4、使用时长如何判断？", "为了防止作弊，息屏即停止计时；除播放视频外停止操作5秒，即停止计时；智能检测到重复机械的有规律的操作，即判定为在采用脚本机器人自动刷时间，一经查验属实，立即永久封禁该帐号。如果发现有设计自动刷时间的脚本机器人产品兜售，必将向法院以破坏计算机软件程序、妨碍公平竞争罪起诉。");
        FreeAdvertBean advertBean5 = new FreeAdvertBean("5、有了使用时长如何投放广告？", "有了使用时长后，在电脑端打开以下网址进行广告添加 和投放：http://www.wuzhesp.com");
        beanList.add(advertBean1);
        beanList.add(advertBean2);
        beanList.add(advertBean3);
        beanList.add(advertBean4);
        beanList.add(advertBean5);
        adapter.setNewData(beanList);
    }

    private void initListeners() {
        llTime.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.activity_free_advert_ll_time) {
            //我的时间
            startActivity(new Intent(this, MyTimeAty.class));
        }
    }
}