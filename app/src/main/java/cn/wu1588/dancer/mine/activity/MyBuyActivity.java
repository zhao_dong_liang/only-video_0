package cn.wu1588.dancer.mine.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import net.lucode.hackware.magicindicator.MagicIndicator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.adp.MyPagerAdapter;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.mine.fragment.BuyAlbumFragment;
import cn.wu1588.dancer.mine.fragment.BuyVideoFragment;

public class MyBuyActivity extends LBaseActivity {
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.image_finish)
    ImageView image_finish;
    @BindView(R.id.title_bar)
    TextView title_bar;
    @BindView(R.id.tab_two)
    TabLayout tab_two;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        setDark(CommonApp.getApplication().isBarTvColor());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_buy);

        title_bar.setText("我的购买");
        Window window = getWindow();
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(new BuyVideoFragment());
        fragments.add(new BuyAlbumFragment());
        List<String> list = new ArrayList<>();
        list.add("4K视频");
        list.add("专辑");
        MyPagerAdapter myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager(), fragments, list);
        viewPager.setAdapter(myPagerAdapter);
        tab_two.setupWithViewPager(viewPager);
        image_finish.setOnClickListener(view -> finish());


        for (int i = 0; i < myPagerAdapter.getCount(); i++) {
            TabLayout.Tab tab = tab_two.getTabAt(i);//获得每一个tab
            tab.setCustomView(R.layout.tab_item);//给每一个tab设置view
            TextView textView = (TextView) tab.getCustomView().findViewById(R.id.tab_text);
            textView.setText(list.get(i));//设置tab上的文字
            if (i == 0) {
                // 设置第一个tab的TextView是被选择的样式
                tab.getCustomView().findViewById(R.id.tab_text).setSelected(true);//第一个tab被选中
                View view = tab.getCustomView().findViewById(R.id.view_gone);
                view.setVisibility(View.VISIBLE);
                textView.setTextSize(19);
            } else {
                tab.getCustomView().findViewById(R.id.tab_text).setSelected(false);
                textView.setTextSize(16);
                textView.setTextColor(getResources().getColor(R.color.black));
                View view = tab.getCustomView().findViewById(R.id.view_gone);
                view.setVisibility(View.GONE);


            }
        }

        tab_two.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tab.getCustomView().findViewById(R.id.tab_text).setSelected(true);
                TextView tv = (TextView) tab.getCustomView().findViewById(R.id.tab_text);
                View view = tab.getCustomView().findViewById(R.id.view_gone);
                view.setVisibility(View.VISIBLE);
                tv.setTextSize(19);
                tv.setTextColor(getResources().getColor(R.color.color_FF6C00));
                tv.invalidate();
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                View view = tab.getCustomView().findViewById(R.id.view_gone);
                view.setVisibility(View.GONE);
                tab.getCustomView().findViewById(R.id.tab_text).setSelected(false);
                TextView tv = (TextView) tab.getCustomView().findViewById(R.id.tab_text);
                tv.setTextColor(getResources().getColor(R.color.black));
                tv.setTextSize(16);
                tv.invalidate();
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }
}