package cn.wu1588.dancer.mine.adapter;

import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.mine.mode.ProblemBean;

public class FeedBackAdp extends BaseQuickAdapter<ProblemBean.FeedbackCategoryBean, BaseViewHolder> {
    public FeedBackAdp(int layoutResId, @Nullable List<ProblemBean.FeedbackCategoryBean> data) {
        super(layoutResId, data);
    }

    public FeedBackAdp(int item_feed_back) {
        super(item_feed_back);
    }

    @Override
    protected void convert(BaseViewHolder helper, ProblemBean.FeedbackCategoryBean item) {
        ImageView imageUnchecked = helper.itemView.findViewById(R.id.image_unchecked);
        imageUnchecked.setImageResource(item.switechBean ? R.mipmap.icon_select : R.mipmap.icon_unchecked);
        helper.setText(R.id.text_title, item.title);
    }
}
