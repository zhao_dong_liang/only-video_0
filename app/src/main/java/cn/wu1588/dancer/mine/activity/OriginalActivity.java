package cn.wu1588.dancer.mine.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.base.BaseAty;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.quest.BaseQuestConfig;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.common.util.VideoThumbnailUtils;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bilibili.boxing.Boxing;
import com.bilibili.boxing.model.config.BoxingConfig;
import com.bilibili.boxing.model.entity.BaseMedia;
import com.bilibili.boxing_impl.ui.BoxingActivity;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.kongzue.baseframework.interfaces.BindView;
import com.kongzue.baseframework.interfaces.DarkStatusBarTheme;
import com.kongzue.baseframework.interfaces.Layout;
import com.kongzue.baseframework.util.JumpParameter;
import com.kongzue.baseokhttp.HttpRequest;
import com.kongzue.baseokhttp.listener.ResponseListener;
import com.kongzue.baseokhttp.util.JsonMap;
import com.kongzue.baseokhttp.util.Parameter;
import com.kongzue.dialog.util.DialogSettings;
import com.kongzue.dialog.v3.CustomDialog;
import com.kongzue.dialog.v3.TipDialog;
import com.kongzue.dialog.v3.WaitDialog;
import com.my.toolslib.StringUtils;
import com.shuyu.gsyvideoplayer.GSYVideoManager;
import com.shuyu.gsyvideoplayer.video.StandardGSYVideoPlayer;


import java.io.File;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 申请原创
 */
@Layout(R.layout.activity_original)
@DarkStatusBarTheme(true)
public class OriginalActivity extends BaseAty {

    @BindView(R.id.title_bar)
    private TitleBar titleBar;
    @BindView(R.id.video_player)
    private StandardGSYVideoPlayer video_player;
    @BindView(R.id.origin_ll)
    private LinearLayout origin_ll;
    @BindView(R.id.original_open_video)
    private ImageView original_open_video;
    @BindView(R.id.btnUpdate)
    private Button btnUpdate;
    @BindView(R.id.video_title_labe)
    private TextView video_title_labe;

    private VideoThumbnailUtils videoThumbnailUtils;

    private boolean isVertical;//是否是竖直视频
    private boolean isDebug = true;//是否测试
    private boolean isPrivate;
    private String videoPath; //视频的路径
    private final int REQUEST_CODE = 1;
    private File file;

    @Override
    public void initViews() {
        video_player.getBackButton().setVisibility(View.GONE);
        video_player.getStartButton().setVisibility(View.GONE);
        video_player.getFullscreenButton().setVisibility(View.GONE);

    }

    @Override
    public void initDatas(JumpParameter parameter) {

        DialogSettings.init();
        DialogSettings.cancelable = false;
        //资格请求
        requestIsHasOriginal();

        videoThumbnailUtils = new VideoThumbnailUtils(me, new VideoThumbnailUtils.VideoThumbnailCall() {
            @Override
            public void getVideoThumbnailResult(ArrayList<Bitmap> bitmaps) {
                if (bitmaps != null && bitmaps.size() > 0) {
                    if (isStandard()) {
                        origin_ll.setVisibility(View.GONE);
                        setThumbImageView(videoThumbnailUtils.getFristThumbnail());
                        video_player.getStartButton().setVisibility(View.VISIBLE);
                        btnUpdate.setEnabled(true);
                    } else {
                        origin_ll.setVisibility(View.VISIBLE);
                        video_player.getStartButton().setVisibility(View.GONE);
                        ToastUtils.showShort("该视频不符合规范");
                    }
                }
            }
        });
    }

    @Override
    public void setEvents() {
        original_open_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BoxingConfig videoConfig = new BoxingConfig(BoxingConfig.Mode.VIDEO);//.withVideoDurationRes(R.mipmap.add_video_icon)
                Boxing.of(videoConfig).withIntent(me, BoxingActivity.class).start(me, REQUEST_CODE);
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (StringUtils.isNull(videoPath)) {
                    ToastUtils.showShort("请先选择视频");
                    return;
                } else {
                    WaitDialog.show(me, "请稍候...");
                    reloadVideoPath();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            final ArrayList<BaseMedia> medias = Boxing.getResult(data);
            BaseMedia baseMedia = medias.get(0);
            videoPath = baseMedia.getPath();
            video_player.setUp(videoPath, false, "");
            Log.i("OkGo:path==", baseMedia.getPath() + ", " + baseMedia.getId());
            videoThumbnailUtils.getVideoThumbnail(baseMedia.getPath());
            video_player.getStartButton().setVisibility(View.GONE);
        }
    }

    /**
     * 查询原创申请条件是否满足
     */
    private void requestIsHasOriginal() {
        WaitDialog.show(me, "数据加载中...");
        HttpRequest.POST(me,
                BaseQuestConfig.GET_TO_CAN_APPLY_ORIGINAL,
                new Parameter().add("uid", Config.getLoginInfo().id),
                new ResponseListener() {
                    @Override
                    public void onResponse(String value, Exception error) {
                        WaitDialog.dismiss();
                        JsonMap jsonMap = new JsonMap(value);
                        if (jsonMap.get("status").equals("1")) {
                            JsonMap data = jsonMap.getJsonMap("data");
                            int allowData = data.getInt("allow");
                            //0表示没有
                            if (allowData == 0) {
                                showNoAllowDialog();
                            } else {

                            }
                        } else {
                            LogUtils.e("GET_TO_CAN_APPLY_ORIGINAL接口请求失败 服务器返回不为 1" + error);
                            ToastUtils.showShort("服务器请求失败！");
                        }
                    }
                });
    }


    private void reloadVideoPath() {
        File file = new File(videoPath);
        LogUtils.e("当前获取的 videopath 为：" + file);
        HttpRequest.POST(me, BaseQuestStart.GET_TO_APPLY_ORIGINAL, new Parameter().add("movie", file), new ResponseListener() {
            @Override
            public void onResponse(String value, Exception error) {
                if (error == null) {
                    JsonMap jsonMap = JsonMap.parse(value);
                    String status = jsonMap.getString("status");
                    if (status.equals("1")) {

                        TipDialog.show(me, "上传成功！", TipDialog.TYPE.SUCCESS);
                        ToastUtils.showShort("上传成功！请等待审核");
                        //
                        Timer timer = new Timer();
                        TimerTask tast = new TimerTask() {
                            @Override
                            public void run() {
                                finish();
                            }
                        };
                        timer.schedule(tast, 1500);

//                        TipDialog.show(me, "成功！", TipDialog.TYPE.SUCCESS);
//                        String data = jsonMap.getString("data");
//                        video_title_labe.setText(data);
//                        video_title_labe.setTextColor(Color.RED);
//
//                        video_player.getBackButton().setVisibility(View.GONE);
//                        video_player.getStartButton().setVisibility(View.GONE);
//                        video_player.getFullscreenButton().setVisibility(View.GONE);
//                        origin_ll.setVisibility(View.VISIBLE);
//                        btnUpdate.setEnabled(false);
                    }

                }
            }
        });
    }

    /**
     * 无资格弹框
     */
    public void showNoAllowDialog() {
        CustomDialog.build(me, R.layout.dialog_has_allow, new CustomDialog.OnBindView() {
            @Override
            public void onBind(CustomDialog dialog, View v) {
                Button btnOk = v.findViewById(R.id.btnSure);
                btnOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.doDismiss();
                        OriginalActivity.this.finish();
                    }
                });
            }
        }).show();
    }

    //判断视频是否规范
    private boolean isStandard() {
        String videoVode = videoThumbnailUtils.getVideoVode();
        int video_height = videoThumbnailUtils.getVideo_height();
        int video_width = videoThumbnailUtils.getVideo_width();
        if (!StringUtils.isNull(videoVode)) {
            if ("h265".equals(videoVode) || "H265".equals(videoVode) || "hevc".equals(videoVode)) {
                if ((video_height == 854 && video_width == 480) || (video_height == 480 && video_width == 854)) {
                    if (video_height > video_width) {
                        isVertical = true;
                    } else {
                        isVertical = false;
                    }
                    return true;
                }

            }
        }
        if (isDebug) {
            if (video_height > video_width) {
                isVertical = true;
            } else {
                isVertical = false;
            }
            return true;
        }
        return false;
    }

    //设置视频封面
    private void setThumbImageView(Bitmap bitmap) {
//        select_img.setImageBitmap(bitmap);
        ImageView imageView = new ImageView(me);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setImageBitmap(bitmap);
        video_player.setThumbImageView(imageView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        video_player.onVideoResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        video_player.onVideoPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        GSYVideoManager.releaseAllVideos();
    }

    @Override
    public boolean onBack() {
        //释放所有
        video_player.setVideoAllCallBack(null);
        return super.onBack();
    }
}