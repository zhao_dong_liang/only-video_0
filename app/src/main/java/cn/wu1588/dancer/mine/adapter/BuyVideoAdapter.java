package cn.wu1588.dancer.mine.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.my.toolslib.DateUtil;

import java.util.Map;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.util.DataUtils;
import cn.wu1588.dancer.my_video.bean.MyVideoBean;
import cn.wu1588.dancer.utils.SystemFunctionUtils;

public class BuyVideoAdapter extends BaseQuickAdapter<Map<String, String>, BaseViewHolder> {
    public BuyVideoAdapter() {
        super(R.layout.buy_video_list_item);
    }

    @Override
    protected void convert(BaseViewHolder helper, Map<String, String> item) {
        GlideMediaLoader.load(mContext, helper.getView(R.id.img), item.get("image"), R.mipmap.img_def);
        helper.setText(R.id.time_length_tv, item.get("play_time"))
                .setText(R.id.video_name, item.get("title"))
                .setText(R.id.text_shoujia_money, item.get("price"))
                .setText(R.id.text_tiji_money, item.get("size"))
                .setText(R.id.time_tv, DataUtils.getStringDate(item.get("purchase_time")))
                .setText(R.id.text_download_url, item.get("download_url") == null ? "无" : item.get("download_url"))
                .setText(R.id.text_psd, item.get("extract_password") == null ? "无" : item.get("extract_password"))
                .setText(R.id.text_extract, item.get("retrieve_password") == null ? "无" : item.get("retrieve_password"));
        helper.getView(R.id.text_download_url_cope).setOnClickListener(view -> {
            SystemFunctionUtils.copyContentToClipboard(item.get("download_url"), mContext);
        });
        helper.getView(R.id.text_psd_cope).setOnClickListener(view -> {
            SystemFunctionUtils.copyContentToClipboard(item.get("extract_password"), mContext);
        });
        helper.getView(R.id.text_extract_cope).setOnClickListener(view -> {
            SystemFunctionUtils.copyContentToClipboard(item.get("retrieve_password"), mContext);
        });
    }
}
