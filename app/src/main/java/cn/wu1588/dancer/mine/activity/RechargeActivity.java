package cn.wu1588.dancer.mine.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.cnsunrun.commonui.widget.roundwidget.QMUIRoundButton;
import com.cnsunrun.commonui.widget.titlebar.TitleBar;
import com.google.gson.Gson;
import com.makeramen.roundedimageview.RoundedImageView;
import cn.wu1588.dancer.logic.AliPayLogic;
import cn.wu1588.dancer.ui.WePaymentActivity;
import com.sunrun.sunrunframwork.adapter.SelectableAdapter;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;
import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.sunrun.sunrunframwork.utils.EmptyDeal;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.dialog.RechageDialogFragment;
import cn.wu1588.dancer.common.dialog.SelectPayDialog;
import cn.wu1588.dancer.common.model.LoginInfo;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.common.util.RecycleHelper;
import cn.wu1588.dancer.common.util.Tool;
import cn.wu1588.dancer.mine.adapter.PayMentAdapter;
import cn.wu1588.dancer.mine.adapter.SelectAdapter;
import cn.wu1588.dancer.mine.mode.PayChannelBean;
import cn.wu1588.dancer.mine.mode.PayInfo;
import cn.wu1588.dancer.mine.mode.PayMentBean;
import cn.wu1588.dancer.mine.mode.PayUrlBean;
import cn.wu1588.dancer.mine.mode.RechargeBean;
import cn.wu1588.dancer.mine.mode.VipRechargeBean;
import cn.wu1588.dancer.mine.mode.WxPayBean;

import static cn.wu1588.dancer.ui.WePaymentActivity.PAY_ORDER_CONTENT;
import static cn.wu1588.dancer.ui.WePaymentActivity.PAY_SUCCESS;
import static cn.wu1588.dancer.ui.WePaymentActivity.PAY_TYPE;
import static cn.wu1588.dancer.ui.WePaymentActivity.PAY_TYPE_WXPAY;
import static cn.wu1588.dancer.ui.WePaymentActivity.RESULT_MSG;
import static cn.wu1588.dancer.ui.WePaymentActivity.RESULT_STATUS;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.CREATE_PAYMENT_ORDER_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.CREATE_WEIXIN_ORDER_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_GOLD_MEAL_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_PAY_CHANEL_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_PAY_METHOD_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_PAY_METHOD_NEW_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_PAY_URL_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_RECHARGE_INFO_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_USER_INFO_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.POST_PAY_URL_CODE;

/**
 * 会员充值 VIP升级  (最新修改)
 */
public class RechargeActivity extends LBaseActivity implements Handler.Callback {


    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.rIvStar)
    RoundedImageView rIvStar;
    @BindView(R.id.tvIDCard)
    TextView tvIDCard;
    @BindView(R.id.tvStates)
    TextView tvStates;
    @BindView(R.id.ivExpired)
    ImageView ivExpired;
    @BindView(R.id.tvAgreements)
    TextView tvAgreements;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.recyclerViewPayment)
    RecyclerView recyclerViewPayment;
    @BindView(R.id.btnSure)
    QMUIRoundButton btnSure;
    @BindView(R.id.tv_meal)
    TextView tvMeal;
    @BindView(R.id.tv_gold)
    TextView tvGold;
    @BindView(R.id.view_meal)
    View viewMeal;
    @BindView(R.id.view_gold)
    View viewGold;
    private SelectableAdapter<VipRechargeBean> selectableAdapter;
    private PayMentAdapter payMentAdapter;
    private int checkPosition;
    private SelectAdapter selectAdapter;
    private int selectTaocanPosition = -1;
    private RechargeBean rechargeBean;
    private PayMentBean item;
    private AliPayLogic aliPayLogic;
    //    private WxPayLogic wxPayLogic;
    private boolean thirdPay = false;
    //充值类型
    private int rechargeType = 0;

    private int index;
    List<RechargeBean.VipMealBean> vip_meal = new ArrayList<>();
    private List<RechargeBean.VipMealBean> goldListBeanList= new ArrayList<>();;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recharge_member);
        ButterKnife.bind(this);
        initViews();
        initData();
        initListener();
        Handler handler = new Handler(this);
        aliPayLogic = new AliPayLogic(that, handler);
    }

    private void initListener() {
        payMentAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                checkPosition = position;
                List<PayMentBean> data = payMentAdapter.getData();
                for (int i = 0; i < data.size(); i++) {
                    data.get(i).isCheckd = false;
                }
                data.get(position).isCheckd = true;
                payMentAdapter.notifyDataSetChanged();
            }
        });
        selectAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                selectTaocanPosition = position;
                selectAdapter.setSelectedPos(position);
            }
        });
    }

    private void initData() {

        index=getIntent().getIntExtra("index",0);
        BaseQuestStart.getRechargeInfo(that);
        BaseQuestStart.getPayWay(that);
        BaseQuestStart.getGoldInfo(that);
        List<PayMentBean> payMentBeans = new ArrayList<>();
        payMentBeans.add(new PayMentBean("2", "支付宝", true));
        payMentBeans.add(new PayMentBean("1", "微信", false));
//        payMentBeans.add(new PayMentBean("5", "支付宝或微信支付", false));
        payMentAdapter.setNewData(payMentBeans);

    }

    private void initViews() {
        RecycleHelper.setLinearLayoutManager(recyclerViewPayment, LinearLayoutManager.VERTICAL);
        RecycleHelper.setLinearLayoutManager(recyclerView, LinearLayoutManager.HORIZONTAL);
        payMentAdapter = new PayMentAdapter();
        recyclerViewPayment.setAdapter(payMentAdapter);

        selectAdapter = new SelectAdapter();
        recyclerView.setAdapter(selectAdapter);

        titleBar.setRightAction(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonIntent.startRechargeRecordActivity(that);
            }
        });
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode) {
            case GET_PAY_METHOD_CODE:
                if (bean.status == 1) {
                    List<PayMentBean> payMentBeans = bean.Data();
                    if (!EmptyDeal.isEmpy(payMentBeans)) {
                        payMentBeans.get(0).isCheckd = true;
                    }
                    payMentAdapter.setNewData(payMentBeans);
                }
                break;
            case GET_RECHARGE_INFO_CODE://选择VIP套餐
                if (bean.status == 1) {
                    rechargeBean = bean.Data();
                    vip_meal.clear();
                    vip_meal.addAll(rechargeBean.vip_meal);
                    GlideMediaLoader.loadHead(that, rIvStar, rechargeBean.user_info.icon);
                    tvIDCard.setText(rechargeBean.user_info.identity_id);
                    tvStates.setText(rechargeBean.user_info.status);
                    if (TextUtils.equals("已过期", rechargeBean.user_info.status)) {
                        ivExpired.setVisibility(View.VISIBLE);
                    }
//                    if (index==0){
//                        setTabIndex(0);
//                    }
                    updaeUI();
                }
                break;
            case GET_GOLD_MEAL_CODE://选择金币
                if (bean.status == 1) {
                    goldListBeanList = bean.Data();
//                    if (index==1){
//                        setTabIndex(1);
//                    }

                }
                break;
            case CREATE_PAYMENT_ORDER_CODE:
                if (bean.status == 1) {
                    aliPayLogic.startAliPay(bean.data.toString());
                } else {
                    UIUtils.shortM(bean.msg);
                }
                break;
            case CREATE_WEIXIN_ORDER_CODE:
                if (bean.status == 1) {
                    Intent intent = new Intent(this, WePaymentActivity.class);
                    intent.putExtra(PAY_TYPE, PAY_TYPE_WXPAY);
                    WxPayBean wxPayBean = bean.Data();
                    PayInfo payInfo = wxPayBean.app_create_data;
                    intent.putExtra(PAY_ORDER_CONTENT, new Gson().toJson(payInfo, PayInfo.class));
                    startActivityForResult(intent, 0x09);
//                    PayInfo payInfo = bean.Data();
//                    wxPayLogic = new WxPayLogic(that, payInfo.getAppid(), new Handler());
//                    Map<String, String> map = new HashMap<>();
//                    map.put("sign", payInfo.getSign());
//                    map.put("appid", payInfo.getAppid());
//                    map.put("partnerid", payInfo.getPartnerid());
//                    map.put("prepayid", payInfo.getPrepayid());
//                    map.put("noncestr", payInfo.getNoncestr());
//                    map.put("timestamp", payInfo.getTimestamp());
//                    map.put("package", payInfo.getPackagex());
//                    JSONObject object = new JSONObject(map);
//                    wxPayLogic.startWxPay(object);
//                    PayInfo payInfo = bean.Data();
//                    WxPayBean wxPayBean = bean.Data();
//                    PayInfo payInfo = wxPayBean.app_create_data;
//                    WXPayHelper wxPayHelper = new WXPayHelper(that,
//                            payInfo.getAppid(),
//                            payInfo.getPartnerid(),
//                            payInfo.getPrepayid(),
//                            payInfo.getNoncestr(),
//                            payInfo.getSign());
//                    wxPayHelper.pay();
                } else {
                    UIUtils.shortM(bean.msg);
                }

                break;
            case POST_PAY_URL_CODE:
                if (bean.status == 1) {
                    PayUrlBean payUrlBean = bean.Data();
                    CommonIntent.startPayImgActivity(that, payUrlBean);
                } else {
                    UIUtils.shortM(bean.msg);
                }
                break;
            case GET_PAY_CHANEL_CODE:
                if (bean.status == 1) {
                    List<PayChannelBean> payChannelBeans = bean.Data();
                    new SelectPayDialog(that, payChannelBeans, new SelectPayDialog.OnViewClickListener() {
                        @Override
                        public void onItemClick(String content) {
                            RechargeBean.VipMealBean VipMealBean = selectAdapter.getItem(selectTaocanPosition);
                            if (VipMealBean != null) {
                                BaseQuestStart.getPayUrl(that, VipMealBean.id, content);
                            }
                        }
                    });
                } else {
                    UIUtils.shortM(bean.msg);
                }
                break;
            case GET_PAY_URL_CODE:
                if (bean.status == 1) {
                    WebRechargeActivity.startThis(that, bean.Data().toString());
                } else {
                    UIUtils.shortM(bean.msg);
                }
                break;
            case GET_PAY_METHOD_NEW_CODE:
                if (bean.status == 1) {
                    if ("第三方支付".equals(bean.Data())) {
                        thirdPay = true;
                    } else {
                        thirdPay = false;
                    }
                } else {
                    UIUtils.shortM(bean.msg);
                }
                break;
            case GET_USER_INFO_CODE:
                if (bean.status == 1) {
                    LoginInfo loginInfo = bean.Data();
                    if (loginInfo != null) {
                        loginInfo.setUid(loginInfo.id);
                        Config.putLoginInfo(loginInfo);
                        finish();
                    }
                }
                break;
        }
        super.nofityUpdate(requestCode, bean);
    }


    private void updaeUI() {
        selectAdapter.setNewData(vip_meal);
        List<RechargeBean.VipMealBean> data = selectAdapter.getData();
        if (!EmptyDeal.isEmpy(data)) {
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).isCommend()) {
                    selectTaocanPosition = i;
                    selectAdapter.setSelectedPos(i);
                }
            }
        }

    }

    @OnClick({R.id.tvAgreements, R.id.btnSure,R.id.tv_meal,R.id.tv_gold})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvAgreements:
                if (!EmptyDeal.isEmpy(rechargeBean)) {
                    CommonIntent.startServiceWebActivity(that, rechargeBean.h5);
                }
                break;
            case R.id.btnSure:
                if (!EmptyDeal.isEmpy(rechargeBean)) {
                    if (rechargeBean.isShow()) {
                        showWindow();
                    } else {
                        createOrder();
                    }
                }
                break;
            case R.id.tv_meal:
                setTabIndex(0);
                break;
            case R.id.tv_gold:
                setTabIndex(1);
                break;
        }
    }

    private void setTabIndex(int index){
        if (index==0){
            selectTaocanPosition = 0;
            selectAdapter.setSelectedPos(0);
            rechargeType = 0;
            viewMeal.setVisibility(View.VISIBLE);
            viewGold.setVisibility(View.INVISIBLE);
            vip_meal.clear();
            if (rechargeBean.vip_meal!=null) {
                vip_meal.addAll(rechargeBean.vip_meal);
                updaeUI();
            }
        }else {
            selectTaocanPosition = 0;
            selectAdapter.setSelectedPos(0);
            rechargeType = 1;
            viewMeal.setVisibility(View.INVISIBLE);
            viewGold.setVisibility(View.VISIBLE);
            vip_meal.clear();
            vip_meal.addAll(goldListBeanList);
            updaeUI();
        }
    }
    /**
     * 生成订单
     */
    private void createOrder() {
        item = payMentAdapter.getItem(checkPosition);
        RechargeBean.VipMealBean vipMealBean = selectAdapter.getItem(selectTaocanPosition);
        switch (item.id) {
            case "2":
                if (thirdPay) {
                    if (Tool.checkAliPayInstalled(that)) {
                        BaseQuestStart.getPayChanel(that, "1");
                    } else {
                        UIUtils.shortM("请安装支付宝客户端");
                    }
                } else {
                    BaseQuestStart.postCreateOrder(that, vipMealBean,rechargeType);
                }
                break;
            case "1":
                if (Tool.isWeixinAvilible(that)) {
                    if (thirdPay) {
                        BaseQuestStart.getPayChanel(that, "2");
                    } else {
                        BaseQuestStart.createWeixinrder(that, vipMealBean, rechargeType);
                    }
                } else {
                    UIUtils.shortM("请安装微信客户端");
                }
                break;
//            case "5":
//                BaseQuestStart.postPayUrl(that,vipMealBean.id);
//                break;
        }

    }

    /**
     * 弹出提示框
     */
    private void showWindow() {
        if (rechargeBean.popout_content == null || !thirdPay) {
            createOrder();
            return;
        }
        RechageDialogFragment.newInstance().setContentTxt(rechargeBean.popout_content).setOnSubmitAction(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createOrder();
            }
        }).setOnCancelAction(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createOrder();
            }
        }).show(getSupportFragmentManager(), "MessageTipDialog");
    }


    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case 1901:  //支付宝
                if (msg.obj.equals("9000")) {
                    BaseQuestStart.getUserInfo(this);
                } else {
                    ToastUtils.shortToast("支付失败");
                }
                break;
        }
        return false;
    }

    //接收支付结果回调和结果信息
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == 0x09) {
            int intExtra = data.getIntExtra(RESULT_STATUS, 0);
            Toast.makeText(this, data.getStringExtra(RESULT_MSG), Toast.LENGTH_SHORT).show();
            if (intExtra == PAY_SUCCESS) {
                Intent broadcast = new Intent("android.intent.action.to.mine");
                sendBroadcast(broadcast);
                Log.d("WePayDemoActivity", "成功");
                BaseQuestStart.getUserInfo(this);
            } else {
                Log.d("WePayDemoActivity", "失败");
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
