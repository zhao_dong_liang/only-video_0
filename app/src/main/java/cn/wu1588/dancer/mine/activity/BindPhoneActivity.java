package cn.wu1588.dancer.mine.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.my.toolslib.OneClickUtils;
import com.my.toolslib.StringUtils;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;

import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.quest.BaseQuestConfig;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.quest.Config;

//绑定手机号
public class BindPhoneActivity extends LBaseActivity {

    @BindView(R.id.title_right_text)
    TextView title_right_text;
    @BindView(R.id.title_bar)
    TextView title_bar;
    @BindView(R.id.btn_submit)
    Button btn_submit;
    @BindView(R.id.image_finish)
    ImageView image_finish;
    @BindView(R.id.get_code_bt)
    Button get_code_bt;

    @BindView(R.id.code_edit)
    EditText code_edit;

    //手机
    @BindView(R.id.phone_edit)
    EditText phone_edit;
    @BindView(R.id.phone_view)
    View phone_view;


    //密码
    @BindView(R.id.psd_edit)
    EditText psd_edit;
    @BindView(R.id.code_view_1)
    View code_view_1;

    String tag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        setDark(CommonApp.getApplication().isBarTvColor());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bind_phone);
        initViews();
    }

    private void initViews() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //修改为深色
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        tag = getIntent().getStringExtra("tag");
        title_right_text.setVisibility(View.VISIBLE);
        title_bar.setText(tag.equals("1") ? "更换手机号" : "修改密码");
        btn_submit.setVisibility(tag.equals("1") ? View.GONE : View.VISIBLE);
        //密码
        psd_edit.setVisibility(tag.equals("1") ? View.GONE : View.VISIBLE);
        code_view_1.setVisibility(tag.equals("1") ? View.GONE : View.VISIBLE);
        //手机
        phone_edit.setVisibility(tag.equals("2") ? View.GONE : View.VISIBLE);
        phone_view.setVisibility(tag.equals("2") ? View.GONE : View.VISIBLE);

    }

    @OnClick({R.id.image_finish, R.id.get_code_bt, R.id.title_right_text, R.id.btn_submit})
    public void onViewClicked(View view) {
        if (OneClickUtils.isFastDoubleClick(view.getId())) {
            return;
        }
        switch (view.getId()) {
            case R.id.image_finish:
                finish();
                break;
            case R.id.get_code_bt:
                //获取验证码
                if (tag.equals("1")){
                    String phone = phone_edit.getText().toString();
                    if (StringUtils.isNull(phone)) {
                        ToastUtils.longToast("请输入手机号");
                    } else {
                        BaseQuestStart.getCode(phone, that);
                    }
                }else{
                    BaseQuestStart.getCode(Config.getLoginInfo().mobile, that);
                }

                break;
            case R.id.title_right_text://绑定手机号
                String phone1 = phone_edit.getText().toString();
                if (StringUtils.isNull(phone1)) {
                    ToastUtils.longToast("请输入手机号");
                } else if (StringUtils.isNull(code_edit.getText().toString())) {
                    ToastUtils.longToast("请输入验证码");
                } else {
                    BaseQuestStart.bindPhone(this, phone1, code_edit.getText().toString());
                }
                break;
            case R.id.btn_submit:
                if (Config.getLoginInfo().mobile == null){
                    ToastUtils.shortToast("请先绑定手机");
                    return;
                }
                String psd = psd_edit.getText().toString();
                if (StringUtils.isNull(psd)) {
                    ToastUtils.longToast("请输入密码");
                } else if (StringUtils.isNull(code_edit.getText().toString())) {
                    ToastUtils.longToast("请输入验证码");
                } else {
                    BaseQuestStart.modifyPassword(this,Config.getLoginInfo().mobile, psd, code_edit.getText().toString());
                }
                //修改密码

                break;
        }
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        super.nofityUpdate(requestCode, bean);
        if (BaseQuestConfig.SEND_SMS_CODE_CODE == requestCode) {
            if (bean.status == 1) {
                timer = new TimeCount(60000, 1000);
                timer.start();
            } else {
                ToastUtils.shortToast(bean.msg);
            }
        } else if (BaseQuestConfig.BIND_PHONE_CODE == requestCode) {
            if (bean.status == 1) {
                ToastUtils.longToast("绑定成功！");
                Intent intent = new Intent();
                intent.putExtra("phone", phone_edit.getText().toString());
                setResult(1, intent);
                finish();
            } else {
                ToastUtils.longToast(bean.msg);
            }
        }else if (BaseQuestConfig.CHANGE_PASSWORD_CODE == requestCode){
            if (bean.status == 1) {
                ToastUtils.longToast("修改成功！");
                Intent intent = new Intent();
                intent.putExtra("psd", phone_edit.getText().toString());
                setResult(2, intent);
                finish();
            } else {
                ToastUtils.longToast(bean.msg);
            }
        }
    }

    /**
     * 计时器
     */
    private TimeCount timer;

    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            /*
             * millisInFuture 总时长,countDownInterval 计时的时间间隔
             */
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            /**
             * 计时完毕时触发
             */
            get_code_bt.setText("获取验证码");
            get_code_bt.setClickable(true);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            /**
             * 计时过程显示
             */
            get_code_bt.setClickable(false);
            get_code_bt.setText("获取验证码(" + millisUntilFinished / 1000 + "s)");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null) timer.cancel();
    }
}
