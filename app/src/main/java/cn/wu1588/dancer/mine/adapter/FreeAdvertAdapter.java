package cn.wu1588.dancer.mine.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.mine.mode.FreeAdvertBean;

public class FreeAdvertAdapter extends BaseQuickAdapter<FreeAdvertBean, BaseViewHolder> {
    public FreeAdvertAdapter() {
        super(R.layout.item_free_advert);
    }

    @Override
    protected void convert(BaseViewHolder helper, FreeAdvertBean item) {
        helper.setText(R.id.item_free_advert_title,item.getTitle())
                .setText(R.id.item_free_advert_content,item.getContent());
    }
}
