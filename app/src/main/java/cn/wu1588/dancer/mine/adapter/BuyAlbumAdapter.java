package cn.wu1588.dancer.mine.adapter;

import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.MultipleItemRvAdapter;
import com.chad.library.adapter.base.provider.BaseItemProvider;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.mine.mode.BuyAlbumBean;
import cn.wu1588.dancer.my_video.adapter.ItemProvider;
import cn.wu1588.dancer.my_video.bean.MyVideoBean;

public class BuyAlbumAdapter extends MultipleItemRvAdapter<Object, BaseViewHolder> {
    private int type;

    public void setType(int type) {
        this.type = type;
    }

    public static final int TYPE_CODE=1;
    public static final int ITEM_CODE=2;

    public BuyAlbumAdapter() {
        super(null);
        finishInitialize();
    }

    @Override
    protected int getViewType(Object myVideoBean) {
        return myVideoBean instanceof BuyAlbumBean ? TYPE_CODE : ITEM_CODE;
    }

    @Override
    public void registerItemProvider() {
        mProviderDelegate.registerProvider(new BaseItemProvider<BuyAlbumBean, BaseViewHolder>() {
            @Override
            public int viewType() {
                return TYPE_CODE;
            }

            @Override
            public int layout() {
                return R.layout.buy_album_list_item;
            }

            @Override
            public void convert(BaseViewHolder helper, BuyAlbumBean data, int position) {
                if (type == 1) {
                    helper.setGone(R.id.view, true);
                }
                helper.setText(R.id.album_name, data.title)
                        .setText(R.id.money_tv, String.format("共%s视频", data.movie_count))
                        .setText(R.id.video_num_tv, String.format("售价:%s元", data.price));
            }

            @Override
            public void onClick(BaseViewHolder helper, BuyAlbumBean data, int position) {
                CommonIntent.startMyAlbumVidesActivity(helper.itemView.getContext(), data.id);
            }
        });
        mProviderDelegate.registerProvider(new ItemProvider() {
            @Override
            public void onClick(BaseViewHolder helper, MyVideoBean data, int position) {
                CommonIntent.startMoviePlayerActivity(helper.itemView.getContext(), data.getId());
            }
        });
    }
}
