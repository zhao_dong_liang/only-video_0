package cn.wu1588.dancer.mine.mode;

import com.google.gson.annotations.SerializedName;

/**
 * 支付信息
 */
public class PayInfo {

    /**
     * appid : wx6977e90d430bf341
     * partnerid : 1502772531
     * package : Sign=WXPay
     * noncestr : d0duccloaajdwdl4r5vad6cg53cd79yb
     * timestamp : 1551062982
     * prepayid : wx251049414308565a1f4a0d872086635323
     * sign : 60CF929F12990AE2D19368FB8E5F644D
     */

    private String appid;
    private String noncestr;
    @SerializedName("package")
    private String packagex;
    private String partnerid;
    private String prepayid;
    private String timestamp;
    private String sign;

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getNoncestr() {
        return noncestr;
    }

    public void setNoncestr(String noncestr) {
        this.noncestr = noncestr;
    }

    public String getPackagex() {
        return packagex;
    }

    public void setPackagex(String packagex) {
        this.packagex = packagex;
    }

    public String getPartnerid() {
        return partnerid;
    }

    public void setPartnerid(String partnerid) {
        this.partnerid = partnerid;
    }

    public String getPrepayid() {
        return prepayid;
    }

    public void setPrepayid(String prepayid) {
        this.prepayid = prepayid;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
