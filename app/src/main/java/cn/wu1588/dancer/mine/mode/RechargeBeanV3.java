package cn.wu1588.dancer.mine.mode;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by zhou on 2021/1/6 16:56.
 */
public class RechargeBeanV3 implements Parcelable {


    /**
     * id : 3
     * title : 普通视频下载会员
     * day : 365
     * price : 199.00
     * discount_price : 1.00
     * sort : 2
     * is_commend : 0
     */

    public String id;
    public String title;
    public String day;
    public String price;
    public String discount_price;
    public String sort;
    public String is_commend;
    private boolean check;


    protected RechargeBeanV3(Parcel in) {
        id = in.readString();
        title = in.readString();
        day = in.readString();
        price = in.readString();
        discount_price = in.readString();
        sort = in.readString();
        is_commend = in.readString();
        check = in.readByte() != 0;
    }

    public static final Creator<RechargeBeanV3> CREATOR = new Creator<RechargeBeanV3>() {
        @Override
        public RechargeBeanV3 createFromParcel(Parcel in) {
            return new RechargeBeanV3(in);
        }

        @Override
        public RechargeBeanV3[] newArray(int size) {
            return new RechargeBeanV3[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(day);
        dest.writeString(price);
        dest.writeString(discount_price);
        dest.writeString(sort);
        dest.writeString(is_commend);
        dest.writeByte((byte) (check ? 1 : 0));
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }
}
