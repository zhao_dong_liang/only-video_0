package cn.wu1588.dancer.mine.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.cnsunrun.commonui.widget.roundwidget.QMUIRoundButton;
import com.cnsunrun.commonui.widget.titlebar.TitleBar;
import com.flyco.tablayout.SlidingTabLayout;
import com.google.gson.Gson;
import cn.wu1588.dancer.logic.AliPayLogic;
import cn.wu1588.dancer.ui.WePaymentActivity;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;
import com.sunrun.sunrunframwork.uiutils.UIUtils;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.base.ViewPagerFragmentAdapter;
import cn.wu1588.dancer.common.model.LoginInfo;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.common.util.RecycleHelper;
import cn.wu1588.dancer.common.util.Tool;
import cn.wu1588.dancer.mine.adapter.PayMentAdapter;
import cn.wu1588.dancer.mine.fragment.VipSetMealFragment;

import cn.wu1588.dancer.mine.fragment.WbSetMealFragment;
import cn.wu1588.dancer.mine.mode.GoldMealBean;
import cn.wu1588.dancer.mine.mode.PayInfo;
import cn.wu1588.dancer.mine.mode.PayMentBean;
import cn.wu1588.dancer.mine.mode.RechargeBeanV3;
import cn.wu1588.dancer.mine.mode.WxPayBean;

import static cn.wu1588.dancer.ui.WePaymentActivity.PAY_ORDER_CONTENT;
import static cn.wu1588.dancer.ui.WePaymentActivity.PAY_SUCCESS;
import static cn.wu1588.dancer.ui.WePaymentActivity.PAY_TYPE;
import static cn.wu1588.dancer.ui.WePaymentActivity.PAY_TYPE_WXPAY;
import static cn.wu1588.dancer.ui.WePaymentActivity.RESULT_MSG;
import static cn.wu1588.dancer.ui.WePaymentActivity.RESULT_STATUS;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.CREATE_PAYMENT_ORDER_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.CREATE_PAYMENT_ORDER_CODE_1;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.CREATE_WEIXIN_ORDER_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.CREATE_WEIXIN_ORDER_CODE_1;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_PAY_METHOD_NEW_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_USER_INFO_CODE;

/**
 * 购买会员
 */
public class RechargeV3Activity extends LBaseActivity implements BaseQuickAdapter.OnItemClickListener, Handler.Callback {


    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.tabLayout)
    SlidingTabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tvAgreements)
    TextView tvAgreements;
    @BindView(R.id.btnSure)
    QMUIRoundButton btnSure;
    @BindView(R.id.recyclerViewPayment)
    RecyclerView recyclerViewPayment;
    private boolean thirdPay = false;

    //获取panMentAdapter选中的item
    private PayMentBean item;
    private PayMentAdapter payMentAdapter;

    private List<Fragment> fragments;
    private ViewPagerFragmentAdapter mVPAdapter;
    private String[] mTitles = {"VIP套餐", "购买舞币"};
    private VipSetMealFragment vipSetMealFragment = VipSetMealFragment.newInstance();
    private WbSetMealFragment wbSetMealFragment = WbSetMealFragment.newInstance();
    private int currentFragment;
    private AliPayLogic aliPayLogic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recharge_v3);
        ButterKnife.bind(this);
        initViews();
        initData();
        Handler handler = new Handler(this);
        aliPayLogic = new AliPayLogic(that, handler);
    }

    private void initViews() {


        titleBar.setRightAction(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonIntent.startRechargeRecordActivity(that);
            }
        });
        //vp
        fragments = new ArrayList<>();
        //vip
        fragments.add(vipSetMealFragment);
        //wubi
        fragments.add(wbSetMealFragment);

        mVPAdapter = new ViewPagerFragmentAdapter(getSupportFragmentManager());
        mVPAdapter.setFragments(fragments);
        viewPager.setAdapter(mVPAdapter);
        viewPager.setCurrentItem(0, false);
        tabLayout.setViewPager(viewPager, mTitles);
        //支付方式rv
        RecycleHelper.setLinearLayoutManager(recyclerViewPayment, LinearLayoutManager.VERTICAL);
        payMentAdapter = new PayMentAdapter();
        recyclerViewPayment.setAdapter(payMentAdapter);
        List<PayMentBean> payMentBeans = new ArrayList<>();
        payMentBeans.add(new PayMentBean("1", "微信", true));
        payMentBeans.add(new PayMentBean("2", "支付宝", false));
        payMentAdapter.setNewData(payMentBeans);
        payMentAdapter.setOnItemClickListener(this);


    }

    @OnClick({R.id.btnSure, R.id.tvAgreements})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSure:
                createOrder();
                ToastUtils.shortToast("zzzzz");
                break;

            case R.id.tvAgreements:

                break;


            default:
                break;
        }
    }

    private void initData() {
        BaseQuestStart.getPayWay(that);
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode) {
            case GET_PAY_METHOD_NEW_CODE:
                if (bean.status == 1) {
                    if ("第三方支付".equals(bean.Data())) {
                        thirdPay = true;
                    } else {
                        thirdPay = false;
                    }
                } else {
                    UIUtils.shortM(bean.msg);
                }
                break;


            case CREATE_PAYMENT_ORDER_CODE:
                //这个是 当前fragment为 vipSetMealFragment 并且支付方式为 支付宝
                if (bean.status == 1) {
                    aliPayLogic.startAliPay(bean.data.toString());
                } else {
                    UIUtils.shortM(bean.msg);
                }
                break;

            case CREATE_PAYMENT_ORDER_CODE_1:
                //这个是 当前fragment为 wbSetMealFragment 并且支付方式为 支付宝
                if (bean.status == 1) {
                    aliPayLogic.startAliPay(bean.data.toString());
                } else {
                    UIUtils.shortM(bean.msg);
                }
                break;

            case CREATE_WEIXIN_ORDER_CODE:
                //这个是 当前fragment为 vipSetMealFragment 并且支付方式为 微信
                if (bean.status == 1) {
                    Intent intent = new Intent(this, WePaymentActivity.class);
                    intent.putExtra(PAY_TYPE, PAY_TYPE_WXPAY);
                    WxPayBean wxPayBean = bean.Data();
                    PayInfo payInfo = wxPayBean.app_create_data;
                    intent.putExtra(PAY_ORDER_CONTENT, new Gson().toJson(payInfo, PayInfo.class));
                    startActivityForResult(intent, 0x09);
                } else {
                    UIUtils.shortM(bean.msg);
                }
                break;

            case CREATE_WEIXIN_ORDER_CODE_1:
                //这个是 当前fragment为 wbSetMealFragment 并且支付方式为 微信
                if (bean.status == 1) {
                    Intent intent = new Intent(this, WePaymentActivity.class);
                    intent.putExtra(PAY_TYPE, PAY_TYPE_WXPAY);
                    WxPayBean wxPayBean = bean.Data();
                    PayInfo payInfo = wxPayBean.app_create_data;
                    intent.putExtra(PAY_ORDER_CONTENT, new Gson().toJson(payInfo, PayInfo.class));
                    startActivityForResult(intent, 0x08);
                } else {
                    UIUtils.shortM(bean.msg);
                }
                break;

            case GET_USER_INFO_CODE:
                if (bean.status == 1) {
                    LoginInfo loginInfo = bean.Data();
                    if (loginInfo != null) {
                        loginInfo.setUid(loginInfo.id);
                        Config.putLoginInfo(loginInfo);
                        finish();
                    }
                }
                break;

            default:
                break;
        }
        super.nofityUpdate(requestCode, bean);
    }

    /**
     * 生成订单
     */
    private void createOrder() {
        //选中的支付方式
//        item = payMentAdapter.getItem(checkPosition);
//        Log.e("zhoujianzzzzzzzzzzzz222",""+checkPosition);
//        Log.e("zhoujianzzzzzzzzzzzz333",""+item.id);


        List<PayMentBean> payMentBeans = payMentAdapter.getData();
        for (PayMentBean payMentBean : payMentBeans) {
            if (payMentBean.isCheckd) {
                item = payMentBean;
                break;
            }
        }

        //vip套餐获取选中的item
        RechargeBeanV3 rechargeBeanV3 = vipSetMealFragment.rechargeBeanV3;
        //舞币套餐获取选中的item
        GoldMealBean goldMealBean = wbSetMealFragment.goldMealBean;
        switch (item.id) {

            //支付宝
            case "2":
                //说明当前选中的fragment为 vipSetMealFragment
                if (viewPager.getCurrentItem() == 0) {
                    if (Tool.checkAliPayInstalled(that)) {
                        //这个第三方支付通道不太清楚
//                            BaseQuestStart.getPayChanel(that, "1");
                        //直接调用支付宝支付
                        BaseQuestStart.postCreateOrderV3(that, rechargeBeanV3, 0);
                    } else {
                        UIUtils.shortM("请安装支付宝客户端");
                    }

                }
                //说明当前选中的fragment为 wbSetMealFragment
                else {
                    if (Tool.checkAliPayInstalled(that)) {
                        //直接调用支付宝支付
                        BaseQuestStart.postCreateOrderV3(that, goldMealBean, 1);
                    } else {
                        UIUtils.shortM("请安装支付宝客户端");
                    }
                }


                break;
            //微信
            case "1":
                //说明当前选中的fragment为 vipSetMealFragment
                if (viewPager.getCurrentItem() == 0) {
                    if (Tool.isWeixinAvilible(that)) {
                        //这个第三方支付通道不太清楚
//                            BaseQuestStart.getPayChanel(that, "1");
                        //直接调用微信支付
                        BaseQuestStart.createWeixinrderV3(that, rechargeBeanV3, 0);
                    } else {
                        UIUtils.shortM("请安装微信客户端");
                    }
                }
                //说明当前选中的fragment为 wbSetMealFragment
                else {
                    if (Tool.isWeixinAvilible(that)) {
                        //直接调用微信支付
                        BaseQuestStart.createWeixinrderV3(that, goldMealBean, 1);
                    } else {
                        UIUtils.shortM("请安装微信客户端");
                    }
                }
                break;

            default:
                break;
        }
    }


    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {

        for (PayMentBean bean : payMentAdapter.getData()) {
            bean.isCheckd = false;
        }

        payMentAdapter.getData().get(position).isCheckd = true;
        payMentAdapter.notifyDataSetChanged();
        ToastUtils.shortToast("您选中的是：" + payMentAdapter.getData().get(position).id);
    }


    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case 1901:  //支付宝
                if (msg.obj.equals("9000")) {
                    BaseQuestStart.getUserInfo(this);
                } else {
                    ToastUtils.shortToast("支付失败");
                }
                break;

            default:
                break;
        }
        return false;
    }

    //接收支付结果回调和结果信息
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (resultCode == RESULT_OK && requestCode == 0x09) {
//            int intExtra = data.getIntExtra(RESULT_STATUS, 0);
//            Toast.makeText(this, data.getStringExtra(RESULT_MSG), Toast.LENGTH_SHORT).show();
//            if (intExtra == PAY_SUCCESS) {
//                Intent broadcast = new Intent("android.intent.action.to.mine");
//                sendBroadcast(broadcast);
//                Log.d("WePayDemoActivity", "成功");
//                BaseQuestStart.getUserInfo(this);
//            } else {
//                Log.d("WePayDemoActivity", "失败");
//            }
//        }
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case 0x09:
                    int intExtra = data.getIntExtra(RESULT_STATUS, 0);
                    Toast.makeText(this, data.getStringExtra(RESULT_MSG), Toast.LENGTH_SHORT).show();
                    if (intExtra == PAY_SUCCESS) {
                        Intent broadcast = new Intent("android.intent.action.to.mine");
                        sendBroadcast(broadcast);
                        Log.d("WePayDemoActivity", "成功");
                        BaseQuestStart.getUserInfo(this);
                    } else {
                        Log.d("WePayDemoActivity", "失败");
                    }
                    break;
                case 0x08:
                    int intExtra1 = data.getIntExtra(RESULT_STATUS, 0);
                    Toast.makeText(this, data.getStringExtra(RESULT_MSG), Toast.LENGTH_SHORT).show();
                    if (intExtra1 == PAY_SUCCESS) {
                        Intent broadcast = new Intent("android.intent.action.to.mine");
                        sendBroadcast(broadcast);
                        Log.d("WePayDemoActivity", "成功");
                        BaseQuestStart.getUserInfo(this);
                    } else {
                        Log.d("WePayDemoActivity", "失败");
                    }
                    break;
                default:
                    break;
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


}