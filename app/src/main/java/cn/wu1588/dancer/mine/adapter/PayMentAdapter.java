package cn.wu1588.dancer.mine.adapter;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import cn.wu1588.dancer.mine.mode.PayMentBean;


/**
 * Created by wangchao on 2018-12-06.
 */
public class PayMentAdapter extends BaseQuickAdapter<PayMentBean, BaseViewHolder> {
    public PayMentAdapter() {
        super(R.layout.item_payment_layout);
    }

    @Override
    protected void convert(BaseViewHolder helper, PayMentBean item) {
        helper.setText(R.id.tvTitle, item.name);
        helper.setChecked(R.id.checkbox, item.isCheckd);
        switch (item.id) {
            case "2":
                helper.setBackgroundRes(R.id.ivIcon, R.drawable.vip_icon_zhifubao_normal);
                break;
            case "1":
                helper.setBackgroundRes(R.id.ivIcon, R.drawable.vip_icon_weixin_normal);
                break;
            case "3":
                helper.setBackgroundRes(R.id.ivIcon, R.drawable.vip_icon_weixin_normal);
                break;
            case "4":
                helper.setBackgroundRes(R.id.ivIcon, R.drawable.vip_icon_weixin_normal);
                break;
            case "5":
                helper.setBackgroundRes(R.id.ivIcon, R.drawable.chongzhi_icon_yue_normal);
                break;
        }
    }
}
