package cn.wu1588.dancer.mine.activity;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import cn.wu1588.dancer.R;
import com.cnsunrun.commonui.widget.titlebar.TitleBar;
import com.flyco.tablayout.SlidingTabLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.base.ViewPagerFragmentAdapter;
import cn.wu1588.dancer.mine.fragment.NoticeFragment;

public class NoticeActivity extends LBaseActivity {

    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.tabLayout)
    SlidingTabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    private String[] mTitles = {"公告", "消息"};
    private List<Fragment> baseFragments;
    private ViewPagerFragmentAdapter mVPAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice);
        ButterKnife.bind(this);
        initViews();
    }
    private void initViews() {
        baseFragments = new ArrayList<>();
        baseFragments.add(NoticeFragment.newInstance("1"));//公告
        baseFragments.add(NoticeFragment.newInstance("2"));//消息
        mVPAdapter = new ViewPagerFragmentAdapter(getSupportFragmentManager());
        mVPAdapter.setFragments(baseFragments);
        viewPager.setAdapter(mVPAdapter);
        viewPager.setCurrentItem(0, false);
        tabLayout.setViewPager(viewPager, mTitles);
    }
}
