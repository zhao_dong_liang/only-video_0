package cn.wu1588.dancer.mine.adapter;

import android.graphics.Paint;
import android.widget.TextView;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import cn.wu1588.dancer.mine.mode.RechargeBean;

public class SelectAdapter extends BaseQuickAdapter<RechargeBean.VipMealBean, BaseViewHolder> {
    private int pos = -1;

    public SelectAdapter() {
        super(R.layout.item_taocan_layout);
    }

    @Override
    protected void convert(BaseViewHolder helper, RechargeBean.VipMealBean item) {
        helper.addOnClickListener(R.id.flContainer);
        TextView tvOldPrice = helper.getView(R.id.tvOldPrice);
        tvOldPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);// 设置中划线并加清晰
        tvOldPrice.getPaint().setAntiAlias(true);
        helper.setBackgroundRes(R.id.flContainer,helper.getAdapterPosition()==pos?R.drawable.shape_taocan_bg_sel:R.drawable.shape_taocan_bg_nor);
//        helper.setGone(R.id.tvTuijian,helper.getAdapterPosition()==pos);
        helper.setGone(R.id.tvTuijian,item.isCommend());


        helper.setText(R.id.tvTime,item.title)
                .setText(R.id.tvPrice,mContext.getString(R.string.price,item.discount_price))
                .setText(R.id.tvOldPrice,mContext.getString(R.string.price,item.price));

    }

    public void setSelectedPos(int position) {
        this.pos=position;
        notifyDataSetChanged();
    }
}
