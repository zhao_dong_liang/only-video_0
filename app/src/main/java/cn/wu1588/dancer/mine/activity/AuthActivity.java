package cn.wu1588.dancer.mine.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.my.toolslib.http.utils.NetRequestListenerProxy;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.UIUtils;

import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.model.LoginInfo;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.mine.mode.UserAuthInfo;

public class AuthActivity extends LBaseActivity {

    @BindView(R.id.v_user_auth_name_icon)
    View v_user_auth_name_icon;
    @BindView(R.id.name_auth_layout)
    LinearLayout name_auth_layout;
    @BindView(R.id.tv_user_auth_name)
    TextView tv_user_auth_name;
    @BindView(R.id.tv_play_num)
    TextView tv_play_num;
    @BindView(R.id.tv_attention_num)
    TextView tv_attention_num;
    @BindView(R.id.tv_movie_num)
    TextView tv_movie_num;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        BaseQuestStart.getUserTalentInfo(new NetRequestListenerProxy(this) {
            @Override
            public void nofityUpdate(int i, BaseBean baseBean) {
                if (baseBean.status == 0) {
                    UIUtils.shortM(baseBean.msg);
                }
                UserAuthInfo info = baseBean.Data();
                tv_play_num.setText(String.format("当前:%s次", info.play_nums));
                tv_attention_num.setText(String.format("当前:%s次", info.attention_nums));
                tv_movie_num.setText(String.format("原创舞蹈视频30个(已上传%s个)", info.movie_nums));
            }
        });

        LoginInfo loginInfo = Config.getLoginInfo();
        if (TextUtils.isEmpty(loginInfo.idcard)) {
            name_auth_layout.setClickable(true);
            name_auth_layout.setEnabled(true);
            tv_user_auth_name.setText("实名认证");
            v_user_auth_name_icon.setVisibility(View.VISIBLE);
        } else {
            name_auth_layout.setClickable(false);
            name_auth_layout.setEnabled(false);
            tv_user_auth_name.setText("实名认证(己认证)");
            v_user_auth_name_icon.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.name_auth_layout)
    void userAuth(View view) {
        CommonIntent.startUserAuth(this);
    }

    @OnClick(R.id.tv_upload_video)
    void uploadVideo(View view) {
        CommonIntent.startUpVideosActivity(this, false);
    }

    @OnClick(R.id.up_bt)
    void submit(View view) {
        BaseQuestStart.submitUserAuth(new NetRequestListenerProxy(this) {
            @Override
            public void nofityUpdate(int i, BaseBean baseBean) {
                if (baseBean.status == 0) {
                    UIUtils.shortM("认证失败");
                } else {
                    UIUtils.shortM("认证成功");
                    finish();
                }
            }
        });
    }
}
