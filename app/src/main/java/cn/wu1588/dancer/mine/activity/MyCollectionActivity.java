package cn.wu1588.dancer.mine.activity;

import android.os.Bundle;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import cn.wu1588.dancer.R;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemChildClickListener;
import com.cnsunrun.commonui.widget.titlebar.TitleBar;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;
import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.sunrun.sunrunframwork.utils.EmptyDeal;
import com.sunrun.sunrunframwork.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.dialog.MessageTipDialog;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.PageLimitDelegate;
import cn.wu1588.dancer.home.mode.MovieHisBean;
import cn.wu1588.dancer.mine.adapter.MyCollectionAdapter;
import cn.wu1588.dancer.share.ShareBottomSheetDialog;
import cn.wu1588.dancer.share.ShareWatchHisDialog;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_MY_COLLECTION_LIST_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.POST_DEL_MOVIE_CODE;

/**
 * 我的收藏
 */
public class MyCollectionActivity extends LBaseActivity {

    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    @BindView(R.id.tvAll)
    CheckBox tvAll;
    @BindView(R.id.tvDel)
    CheckBox tvDel;
    @BindView(R.id.llBottom)
    LinearLayout llBottom;
    private MyCollectionAdapter myCollectionAdapter;
    private boolean isEdit = true;
    PageLimitDelegate pageLimitDelegate = new PageLimitDelegate(new PageLimitDelegate.DataProvider() {
        @Override
        public void loadData(int page) {
            BaseQuestStart.getMyCollectList(that, page, "2", null);
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_collection);
        ButterKnife.bind(this);
        initViews();
        initListener();
    }

    private void initListener() {
        titleBar.setRightListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEdit) {
                    myCollectionAdapter.setEditor(isEdit);
                    isEdit = false;
                    titleBar.setRightText("取消");
                } else {
                    myCollectionAdapter.setEditor(isEdit);
                    isEdit = true;
                    titleBar.setRightText("编辑");

                }
                myCollectionAdapter.notifyDataSetChanged();
                llBottom.setVisibility(!isEdit ? View.VISIBLE : View.GONE);
            }
        });

        myCollectionAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                MovieHisBean item = myCollectionAdapter.getItem(position);
                CommonIntent.startMoviePlayerActivity(that, String.valueOf(item.mo_id));
            }
        });

        myCollectionAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                MovieHisBean item = myCollectionAdapter.getItem(position);
                switch (view.getId()) {
//                    case R.id.checkbox:
//                        if (myCollectionAdapter.getSelectList().contains(item)) {
//                            myCollectionAdapter.getSelectList().remove(item);
//                        } else {
//                            myCollectionAdapter.getSelectList().add(item);
//                        }
//                        myCollectionAdapter.notifyDataSetChanged();
//                        if (myCollectionAdapter.getData().size() == myCollectionAdapter.getSelectList().size()) {
//                            tvAll.setText("取消全选");
//                            tvAll.setChecked(true);
//                        } else {
//                            tvAll.setText("全选");
//                            tvAll.setChecked(false);
//                        }
//                        break;
                    case R.id.image_delete:
                        //删除收藏
                        ToastUtils.shortToast("删除收藏");
                        delete(item);
                        break;
                    case R.id.item_watch_history_share:
                        ShareWatchHisDialog.newInstance(item).show(getSupportFragmentManager(), ShareBottomSheetDialog.TAG);
                        break;
                }
            }
        });
    }

    private void initViews() {
        refreshLayout.setColorSchemeColors(getResources().getColor(R.color.main_button_color));
        myCollectionAdapter = new MyCollectionAdapter();
        recyclerView.setAdapter(myCollectionAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(that));
        pageLimitDelegate.attach(refreshLayout, recyclerView, myCollectionAdapter);
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode) {
            case GET_MY_COLLECTION_LIST_CODE:
                List<MovieHisBean> movieBeans = bean.Data();
                if (bean.status == 1) {
                    pageLimitDelegate.setData(movieBeans);
                } else {
                    pageLimitDelegate.setData(movieBeans);
                    pageLimitDelegate.loadComplete();
                    myCollectionAdapter.setEnableLoadMore(false);
                }
                break;
            case POST_DEL_MOVIE_CODE:
                if (bean.status == 1) {
                    UIUtils.shortM(bean.msg);
//                    myCollectionAdapter.getSelectList().clear();
//                    tvAll.setChecked(false);
//                    tvAll.setText("全选");
//                    myCollectionAdapter.getSelectList().remove(item);
//                    myCollectionAdapter.notifyDataSetChanged();
                    pageLimitDelegate.refreshPage();
                } else {
                    UIUtils.shortM(bean.msg);
                }
                break;
        }
        super.nofityUpdate(requestCode, bean);
    }

    @OnClick({R.id.tvAll, R.id.tvDel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvAll://全选或取消全选
//                selectAll();
                break;
            case R.id.tvDel:
                //删除
//                delete();
                break;
        }
    }

    private void delete(MovieHisBean item) {
        List<MovieHisBean> selectList = myCollectionAdapter.getSelectList();
        selectList.add(item);
        if (EmptyDeal.isEmpy(selectList)) {
            UIUtils.shortM("您还未选择要删除的影片");
        } else {
            MessageTipDialog.newInstance().setContentTxt("是否要删除这" + selectList.get(0).title + "条影片?")
                    .setOnSubmitAction(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            BaseQuestStart.delMovie(that, Utils.listToString(selectList, new Utils.DefaultToString(",")), "2");
                        }
                    }).show(getSupportFragmentManager(), "MessageTipDialog");
        }

    }

    /**
     * 全选或反选
     */
//    private void selectAll() {
//        boolean checked = tvAll.isChecked();
//        selectList = myCollectionAdapter.getSelectList();
//        if (checked) {
//            tvAll.setText("取消全选");
//            selectList.clear();
//            selectList.addAll(myCollectionAdapter.getData());
//        } else {
//            tvAll.setText("全选");
//            selectList.clear();
//        }
//        myCollectionAdapter.notifyDataSetChanged();
//    }
}
