package cn.wu1588.dancer.mine.adapter;

import android.view.View;

import cn.wu1588.dancer.R;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.util.DataUtils;
import cn.wu1588.dancer.home.mode.MovieHisBean;

public class MyCollectionAdapter extends BaseQuickAdapter<MovieHisBean, BaseViewHolder> {
    private boolean isEditor;
    public List<MovieHisBean> selectList = new ArrayList<>();

    public List<MovieHisBean> getSelectList() {
        return selectList;
    }

    public void setSelectList(List<MovieHisBean> selectList) {
        this.selectList = selectList;
    }

    public MyCollectionAdapter() {

        super(R.layout.item_history_record_layout);
        setOnItemChildClickListener(new OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                if (view.getId() == R.id.checkbox) {
                    MovieHisBean item = getItem(position);
                    if (selectList.contains(item)) {
                        selectList.remove(item);
                    } else {
                        selectList.add(item);
                    }
                }
            }
        });
    }

    @Override
    protected void convert(BaseViewHolder helper, MovieHisBean item) {
        GlideMediaLoader.load(mContext, helper.getView(R.id.ivImg), item.image, R.drawable.ic_history_place);
        helper.addOnClickListener(R.id.image_delete,R.id.item_watch_history_share)
                .setGone(R.id.checkbox, isEditor)
                .setText(R.id.tvTitle, item.title)
                .setText(R.id.tvTime, DataUtils.getStringDate(item.add_time))
                .setChecked(R.id.checkbox, selectList.contains(item));

        helper.getView(R.id.image_delete).setVisibility(View.VISIBLE);

    }

    public void setEditor(boolean isEditor) {
        this.isEditor = isEditor;
    }
}
