package cn.wu1588.dancer.mine.mode;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by zhou on 2021/1/6 18:11.
 */
public class GoldMealBean implements Parcelable {


    /**
     * id : 5
     * title : 500人舞币
     * quantity : 500
     * price : 300.00
     * discount_price : 500.00
     * sort : 5
     * is_commend : 0
     */

    public String id;
    public String title;
    public String quantity;
    public String price;
    public String discount_price;
    public String sort;
    public String is_commend;


    protected GoldMealBean(Parcel in) {
        id = in.readString();
        title = in.readString();
        quantity = in.readString();
        price = in.readString();
        discount_price = in.readString();
        sort = in.readString();
        is_commend = in.readString();
    }

    public static final Creator<GoldMealBean> CREATOR = new Creator<GoldMealBean>() {
        @Override
        public GoldMealBean createFromParcel(Parcel in) {
            return new GoldMealBean(in);
        }

        @Override
        public GoldMealBean[] newArray(int size) {
            return new GoldMealBean[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(quantity);
        dest.writeString(price);
        dest.writeString(discount_price);
        dest.writeString(sort);
        dest.writeString(is_commend);
    }
}
