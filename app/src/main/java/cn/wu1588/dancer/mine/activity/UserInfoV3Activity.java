package cn.wu1588.dancer.mine.activity;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.blankj.utilcode.util.ToastUtils;
import com.google.gson.Gson;
import com.kongzue.baseframework.interfaces.BindView;
import com.kongzue.baseframework.interfaces.Layout;
import com.kongzue.baseframework.util.JumpParameter;
import com.kongzue.baseokhttp.HttpRequest;
import com.kongzue.baseokhttp.listener.ResponseListener;
import com.kongzue.baseokhttp.util.JsonMap;
import com.kongzue.baseokhttp.util.Parameter;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.language.LanguageConfig;
import com.luck.picture.lib.listener.OnResultCallbackListener;
import com.my.toolslib.StringUtils;
import com.my.toolslib.http.utils.NetRequestListenerProxy;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.unistrong.yang.zb_permission.ZbPermission;
import com.unistrong.yang.zb_permission.ZbPermissionFail;
import com.unistrong.yang.zb_permission.ZbPermissionSuccess;

import java.io.File;
import java.util.Date;
import java.util.List;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.base.BaseAty;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.dialog.SelectPhotoDialog;
import cn.wu1588.dancer.common.model.LoginInfo;
import cn.wu1588.dancer.common.quest.BaseQuestConfig;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.common.util.AlertDialogUtil;
import cn.wu1588.dancer.common.util.GlideEngine;
import cn.wu1588.dancer.common.util.Utils;
import de.hdodenhof.circleimageview.CircleImageView;

import static cn.wu1588.dancer.common.config.Const.PERMISSION;

/**
 * 个人资料
 */
@Layout(R.layout.activity_user_info)
public class UserInfoV3Activity extends BaseAty {
    @BindView(R.id.title_bar)
    private TextView titleBar;
    @BindView(R.id.layoutAuth)
    private RelativeLayout layoutAuth;
    @BindView(R.id.ivUserPhoto)
    private CircleImageView ivUserPhoto;
    @BindView(R.id.layoutChangeUserPhoto)
    private RelativeLayout layoutChangeUserPhoto;
    @BindView(R.id.tvNickname)
    private TextView tvNickname;
    @BindView(R.id.layoutChangeNickname)
    private RelativeLayout layoutChangeNickname;
    @BindView(R.id.tvSex)
    private TextView tvSex;
    @BindView(R.id.layoutChangeSex)
    private RelativeLayout layoutChangeSex;
    @BindView(R.id.btnSure)
    private Button btnSure;
    @BindView(R.id.signLayout)
    private LinearLayout signLayout;
    @BindView(R.id.sign_tv)
    private TextView sign_tv;
    @BindView(R.id.layoutRegion)
    private RelativeLayout layoutRegion;
    @BindView(R.id.tvPhone)
    private TextView tvPhone;
    @BindView(R.id.tvRegion)
    private TextView tvRegion;
    @BindView(R.id.tvAuth)
    private TextView tvAuth;
    @BindView(R.id.has_been_certified)
    private ImageView hasBeenCertified;
    @BindView(R.id.no_been_certified)
    private ImageView noBeenCertified;
    @BindView(R.id.layoutBindPhone)
    private RelativeLayout layoutBindPhone;
    @BindView(R.id.layoutChangeBirthday)
    private RelativeLayout layoutChangeBirthday;
    @BindView(R.id.tvBirthday)
    private TextView tvBirthday;
    @BindView(R.id.image_finish)
    private ImageView image_finish;
    @BindView(R.id.layoutWithdrawal)
    private RelativeLayout layoutWithdrawal;
    @BindView(R.id.textWithdrawalNumber)
    private TextView textWithdrawalNumber;
    @BindView(R.id.layoutOriginal)
    private RelativeLayout layoutOriginal;
    @BindView(R.id.title_right_text)
    private TextView title_right_text;
    @BindView(R.id.layoutUpdatePass)
    private RelativeLayout layoutUpdatePass;

    private String headUrl;
    private int sex;
    private String nikeName;
    private File file;
    private String desc;
    String path;

    @Override
    public void initViews() {
        title_right_text.setText("保存");
        title_right_text.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //修改为深色
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

    }

    @Override
    public void initDatas(JumpParameter parameter) {
        titleBar.setText("账号管理");
        getUserInfor();
    }

    @Override
    public void setEvents() {

        title_right_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveAndFinish();
            }
        });

        //选择头像
        layoutChangeUserPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choosePhoto();
            }
        });
        //昵称
        layoutChangeNickname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonIntent.startUpdateInfoActivity(me, tvNickname.getText().toString());
            }
        });
        //性别
        layoutChangeSex.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialogUtil.selectPhotoDialog(me, "女", "男", new SelectPhotoDialog.OnSelectItemClickListener() {
                    @Override
                    public void selectItemOne(View view) {
                        sex = 2;
                        tvSex.setText("女");
                    }

                    @Override
                    public void selectItemTwo(View view) {
                        sex = 1;
                        tvSex.setText("男");
                    }
                });
            }
        });

        //个性签名
        signLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonIntent.startEditUserSignActivity(me, sign_tv.getText().toString(), 2);
            }
        });

        //地区
        layoutRegion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(me
                        , SelectProvinceActivity.class), 3);
            }
        });

        //绑定手机号
        layoutBindPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(me, BindPhoneActivity.class);
                intent.putExtra("tag", "1");
                startActivityForResult(intent, 1);
            }
        });

        //达人认证
        layoutAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Config.getLoginInfo().is_talent.equals("0")) {
                    CommonIntent.startAuthActivity(me);
                }
            }
        });

        //生日
        layoutChangeBirthday.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });

        //提现账户
        layoutWithdrawal.setOnClickListener(v -> ToastUtils.showShort("提现账户"));
        //申请原创
        layoutOriginal.setOnClickListener(v -> {
            startActivity(new Intent(this, OriginalActivity.class));
        });
        //返回键
        image_finish.setOnClickListener(v -> finish());
        //修改密码
        layoutUpdatePass.setOnClickListener(v -> {
            if (Config.getLoginInfo().mobile == null) {
                com.sunrun.sunrunframwork.uiutils.ToastUtils.shortToast("请先绑定手机！");
                return;
            }
            Intent intent = new Intent(me, BindPhoneActivity.class);
            intent.putExtra("tag", "2");
            startActivityForResult(intent, 1);
        });

    }

    private void saveAndFinish() {
        UIUtils.showLoadDialog(me);
        BaseQuestStart.postUserInfoV3(tvNickname.getText().toString(), String.valueOf(sex), sign_tv.getText().toString(), proName, cityName, headUrl, tvBirthday.getText().toString(), headUrl, new NetRequestListenerProxy(me) {
            @Override
            public void nofityUpdate(int i, BaseBean baseBean) {
                doData(baseBean);
            }
        });
    }

    private void doData(BaseBean bean) {
        if (bean.status == 1) {
            ToastUtils.showShort("账户设置修改完成");
            UserInfoV3Activity.this.finish();
        } else {
            ToastUtils.showShort(bean.msg);
        }
    }

    //时间选择器
    private DatePickerDialog picker = null;

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void showDatePicker() {
        if (picker == null) {
            picker = new DatePickerDialog(this, DatePickerDialog.THEME_HOLO_LIGHT);
            picker.getDatePicker().setMaxDate(new Date().getTime());
            picker.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    //获取日期对话框设定的年月份
                    desc = Utils.timeFormat(new Date(year - 1900, month, dayOfMonth));
                    tvBirthday.setText(desc);
                }
            });
        }
        picker.show();
    }

    private void choosePhoto() {
        ZbPermission.with(me)
                .addRequestCode(PERMISSION)
                .permissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                .request();
    }

    @ZbPermissionSuccess(requestCode = PERMISSION)
    public void permissionSuccess() {
        //选取照片
        PictureSelector.create(me)
                .openGallery(PictureMimeType.ofImage())//相册 媒体类型 PictureMimeType.ofAll()、ofImage()、ofVideo()、ofAudio()
                .isWeChatStyle(true)
                .maxSelectNum(1)//最大选择数量,默认9张
                .isGif(false)//是否显示gif
                .rotateEnabled(false)//裁剪是否可旋转图片
                .isDragFrame(false)//是否可拖动裁剪框(固定)
                .isEnableCrop(true)//是否开启裁剪
                .cropImageWideHigh(1000, 1000)// 裁剪宽高比，设置如果大于图片本身宽高则无效
                .withAspectRatio(1000, 1000)//裁剪比例
                .freeStyleCropEnabled(true)//裁剪框是否可拖拽
                .isCompress(false)//是否压缩
                .setLanguage(LanguageConfig.CHINESE)//国际化语言 LanguageConfig.CHINESE
                .imageEngine(GlideEngine.createGlideEngine())
                .forResult(new OnResultCallbackListener<LocalMedia>() {
                    @Override
                    public void onResult(List<LocalMedia> result) {
                        //获得剪裁路径
                        path = result.get(0).getCutPath();
                        //上传
                        file = new File(path);
                        HttpRequest.POST(me, BaseQuestStart.UPLOAD_FILE, new Parameter().add("img", file), new ResponseListener() {
                            @Override
                            public void onResponse(String value, Exception error) {
                                if (error == null) {
                                    JsonMap jsonMap = JsonMap.parse(value);
                                    String status = jsonMap.getString("status");
                                    if (status.equals("1")) {
                                        String data = jsonMap.getString("data");
                                        GlideMediaLoader.loadHead(me, ivUserPhoto, data);
                                        headUrl = data;
                                        Log.d(">>>", "==============" + headUrl);
                                    }
                                }
                            }
                        });
                    }

                    @Override
                    public void onCancel() {

                    }
                });
    }


    private String proName = "";
    private String cityName = "";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 1) {
                if (data != null) {
                    String title = data.getStringExtra("content");
                    tvNickname.setText(title);
                }
            }
        }
        if (requestCode == 1) {//绑定手机号
            if (data != null) {
                String phone = data.getStringExtra("phone");
                if (!StringUtils.isNull(phone)) {
                    tvPhone.setText(phone);
                    LoginInfo loginInfo = Config.getLoginInfo();
                    loginInfo.mobile = phone;
                    Config.putLoginInfo(loginInfo);
                }
            }
        } else if (requestCode == 2) {//个性签名
            if (data != null) {
                String text = data.getStringExtra("text");
                if (!StringUtils.isNull(text)) {
                    sign_tv.setText(text);
                }
            }
        } else if (requestCode == 3) {//选择省市
            if (data != null) {
                proName = data.getStringExtra("proName");
                cityName = data.getStringExtra("cityName");
                if (!StringUtils.isNull(proName) && !StringUtils.isNull(cityName)) {
                    tvRegion.setText(proName + " " + cityName);
                }
            }
        }
    }

    @ZbPermissionFail(requestCode = PERMISSION)
    public void permissionFail() {
        UIUtils.shortM("请在设置里面打开应用使用相机的权限");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        ZbPermission.onRequestPermissionsResult(UserInfoV3Activity.this, requestCode, permissions, grantResults);
    }

    /**
     * 用户信息的接口 里面需要的参数是 uid
     */
    public void getUserInfor() {
        HttpRequest.POST(me,
                BaseQuestConfig.GET_USER_INFO,
                new Parameter().add("uid", Config.getLoginInfo().id),
                new ResponseListener() {
                    //
                    @Override
                    public void onResponse(String value, Exception error) {
                        JsonMap jsonMap = JsonMap.parse(value);
                        if (jsonMap.get("status").equals("1")) {
                            JsonMap data = jsonMap.getJsonMap("data");
                            //实名认证
                            int talent = data.getInt("is_talent");
                            if (talent != 0) {
                                tvAuth.setText("已认证");
                                hasBeenCertified.setVisibility(View.VISIBLE);
                                noBeenCertified.setVisibility(View.GONE);
                            } else {
                                tvAuth.setText("立即认证");
                                noBeenCertified.setVisibility(View.VISIBLE);
                                hasBeenCertified.setVisibility(View.GONE);
                            }
                            //头像
                            if (data.getString("icon") != null) {
                                GlideMediaLoader.loadHead(me, ivUserPhoto, data.getString("icon"));
                                headUrl = data.getString("icon");
                            }
                            //昵称
                            tvNickname.setText(data.getString("nickname") == null ? "" : data.getString("nickname"));
                            //签名
                            if (data.getString("signature") != null) {
                                sign_tv.setText(data.getString("signature"));
                            } else {
                                sign_tv.setText("");
                                sign_tv.setHint("乖乖的呆在家里做一个安静...");
                            }
                            //性别
                            if (data.getString("gender") != null) {
                                sex = Integer.valueOf(data.getString("gender"));
                                tvSex.setText(sex == 1 ? "男" : "女");
                            } else {
                                tvSex.setText("");
                                tvSex.setHint("未选择");
                            }
                            if (data.getString("mobile") != null){
                                tvPhone.setText(data.getString("mobile"));
                            }
                            //区域
                            proName = data.getString("province");
                            cityName = data.getString("city");
                            if (!StringUtils.isNull(proName)) {
                                tvRegion.setText(proName + " " + cityName);
                            }

                            //生日
                            String birthday = data.getString("birthday");
                            if (birthday != null){
                                tvBirthday.setText(birthday);
                            }
                            //将信息存到本地
                            Gson gson = new Gson();
                            LoginInfo loginInfo = gson.fromJson(data.toString(), LoginInfo.class);
                            Config.putLoginInfo(loginInfo);

                        }
                    }
                });
    }
}