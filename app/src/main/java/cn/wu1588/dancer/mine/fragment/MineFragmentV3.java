package cn.wu1588.dancer.mine.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kongzue.baseokhttp.HttpRequest;
import com.kongzue.baseokhttp.listener.ResponseListener;
import com.kongzue.baseokhttp.util.JsonList;
import com.kongzue.baseokhttp.util.JsonMap;
import com.kongzue.baseokhttp.util.Parameter;
import com.makeramen.roundedimageview.RoundedImageView;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uibase.BaseActivity;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;

import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.aty.MessageNoticeAty;
import cn.wu1588.dancer.aty.my.MyFansAty;
import cn.wu1588.dancer.aty.my.QuestionAty;
import cn.wu1588.dancer.aty.my.ShareTweetsAty;
import cn.wu1588.dancer.mine.activity.FreeAdvertActivity;
import cn.wu1588.dancer.mine.activity.TuiGuangActivity;
import cn.wu1588.dancer.mine.mode.FollowsBean;
import cn.wu1588.dancer.mine.mode.MemberRecordBean;
import cn.wu1588.dancer.mine.mode.TodayDataBean;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.model.LoginInfo;
import cn.wu1588.dancer.common.quest.BaseQuestConfig;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.common.util.LoginUtil;
import cn.wu1588.dancer.my_profit.activity.ProfitActivity;
import cn.wu1588.dancer.utils.map.MD5Utils;

import static android.content.Context.MODE_PRIVATE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_USER_INFO_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.MINE_FANS_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.MINE_TODAY_DATA_CODE;


public class MineFragmentV3 extends LBaseFragment {

    @BindView(R.id.user_header_img)
    RoundedImageView userHeaderImg;
    @BindView(R.id.go_user_view)
    ImageView goUserView;
    @BindView(R.id.user_name)
    TextView userName;
    @BindView(R.id.user_id_signature)
    TextView userIdSignature;
    @BindView(R.id.guideline2)
    Guideline guideline2;
    @BindView(R.id.guideline3)
    Guideline guideline3;
    @BindView(R.id.mine_hardcore_fans)
    TextView mineHardcoreFans;
    @BindView(R.id.mine_golden_fans)
    TextView mineGoldenFans;
    @BindView(R.id.mine_follow)
    TextView mineFollow;
    @BindView(R.id.fans_num)
    FrameLayout fansNum;
    @BindView(R.id.fabulous_num_content)
    ImageView fabulousNumContent;
    @BindView(R.id.fabulous_num)
    FrameLayout fabulousNum;
    @BindView(R.id.fabulous_labe)
    TextView fabulousLabe;
    @BindView(R.id.user_info_layout)
    ConstraintLayout userInfoLayout;
    @BindView(R.id.termOfValidity)
    TextView termOfValidity;
    @BindView(R.id.termOfValidity2)
    TextView termOfValidity2;
    @BindView(R.id.ordinaryMembers)
    TextView ordinaryMembers;
    @BindView(R.id.dvertisingMembership)
    TextView dvertisingMembership;
    @BindView(R.id.member_time)
    TextView memberTime;
    @BindView(R.id.vip_layout)
    ConstraintLayout vipLayout;
    @BindView(R.id.gold_labe)
    ImageView goldLabe;
    @BindView(R.id.gold_num)
    ImageView goldNum;
    @BindView(R.id.buy_gold)
    TextView buyGold;
    @BindView(R.id.bug_gold_layout)
    ConstraintLayout bugGoldLayout;
    @BindView(R.id.mine_is_vip)
    LinearLayout mineIsVip;
    @BindView(R.id.myVip)
    TextView myVip;
    @BindView(R.id.noVip)
    TextView noVip;
    @BindView(R.id.novip_layout)
    ConstraintLayout novipLayout;
    @BindView(R.id.gold_labe2)
    ImageView goldLabe2;
    @BindView(R.id.gold_num2)
    ImageView goldNum2;
    @BindView(R.id.buy_gold2)
    TextView buyGold2;
    @BindView(R.id.bug_gold_layout2)
    ConstraintLayout bugGoldLayout2;
    @BindView(R.id.mine_no_vip)
    LinearLayout mineNoVip;
    @BindView(R.id.my_follow_linear)
    LinearLayout myFollowLinear;
    @BindView(R.id.my_buy_linear)
    LinearLayout myBuyLinear;
    @BindView(R.id.my_download_linear)
    LinearLayout myDownloadLinear;
    @BindView(R.id.my_msg_linear)
    LinearLayout myMsgLinear;
    @BindView(R.id.my_advert_linear)
    LinearLayout myAdvertLinear;
    @BindView(R.id.my_free_advert)
    LinearLayout myFreeAdvert;
    @BindView(R.id.my_share_advert)
    LinearLayout myShareAdvert;
    @BindView(R.id.my_video_linear)
    LinearLayout myVideoLinear;
    @BindView(R.id.my_earnings)
    LinearLayout myEarnings;
    @BindView(R.id.my_question)
    LinearLayout myQuestion;
    @BindView(R.id.my_feedback)
    LinearLayout myFeedback;
    @BindView(R.id.my_share_rose_powder)
    LinearLayout myShareRosePowder;
    @BindView(R.id.my_setting)
    LinearLayout mySetting;
    @BindView(R.id.mine_total_money)
    TextView mineTotalMoney;
    @BindView(R.id.view1)
    View view1;
    @BindView(R.id.view2)
    View view2;
    @BindView(R.id.guideline)
    Guideline guideline;
    @BindView(R.id.guideline4)
    Guideline guideline4;
    @BindView(R.id.guideline6)
    Guideline guideline6;
    @BindView(R.id.guideline7)
    Guideline guideline7;
    @BindView(R.id.textView3)
    TextView textView3;
    @BindView(R.id.textView2)
    TextView textView2;
    @BindView(R.id.textView4)
    TextView textView4;
    @BindView(R.id.textView5)
    TextView textView5;
    @BindView(R.id.mine_today_usetime)
    TextView mineTodayUsetime;
    @BindView(R.id.mine_4k_get_money)
    TextView mine4kGetMoney;
    @BindView(R.id.mine_album_gains)
    TextView mineAlbumGains;
    @BindView(R.id.mine_revenue)
    TextView mineRevenue;
    @BindView(R.id.textView10)
    TextView textView10;
    @BindView(R.id.textView11)
    TextView textView11;
    @BindView(R.id.textView12)
    TextView textView12;
    @BindView(R.id.textView13)
    TextView textView13;
    @BindView(R.id.textView16)
    TextView textView16;
    @BindView(R.id.mine_works_number)
    TextView mineWorksNumber;
    @BindView(R.id.textView20)
    TextView textView20;
    @BindView(R.id.mine_4k_number)
    TextView mine4kNumber;
    @BindView(R.id.textView22)
    TextView textView22;
    @BindView(R.id.mine_start)
    TextView mineStart;
    @BindView(R.id.textView24)
    TextView textView24;
    @BindView(R.id.mine_play_number)
    TextView minePlayNumber;
    @BindView(R.id.mine_today_data)
    RelativeLayout mineTodayData;
    @BindView(R.id.mine_vip_layout)
    LinearLayout mineVipLayout;

    private LoginInfo loginInfo;
    private FollowsBean followsBean;
    private TodayDataBean todayDataBean;
    private MemberRecordBean memberRecordBean;


    public static MineFragmentV3 newInstance() {
        MineFragmentV3 fragmentV3 = new MineFragmentV3();
        Bundle bundle = new Bundle();
        fragmentV3.setArguments(bundle);
        return fragmentV3;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        EventBus.getDefault().register(this);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_mine_v3;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    String userid = "";

    private void initView() {
        if (getActivity().getSharedPreferences("login", MODE_PRIVATE) != null) {
            sharedPreferences = getActivity().getSharedPreferences("login", MODE_PRIVATE);
            if (sharedPreferences.getString("userid", "") != null) {
                userid = sharedPreferences.getString("userid", "");
            }
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        initData();
    }

    SharedPreferences sharedPreferences;

    private void initData() {
        if (!LoginUtil.startLogin(getActivity())) {
            //获取用户信息
            getUserInfor();
            //查询会员购买记录
            getMineMemberRecord();
            //获取粉丝和关注用户数
            BaseQuestStart.getMineFans(MineFragmentV3.this);
            //获取用户今日数据
            BaseQuestStart.getMineTodayData(MineFragmentV3.this);
        } else {
            userHeaderImg.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.ic_def_head));
            userName.setText("请登录/注册");
            userIdSignature.setText("登录后查看签名");
            mineHardcoreFans.setText("0");
            mineGoldenFans.setText("0");
            mineFollow.setText("0");
            mineTotalMoney.setText("0");
            mineTodayUsetime.setText("0");
            mine4kGetMoney.setText("0");
            mineAlbumGains.setText("0");
            mineRevenue.setText("0");
            mineWorksNumber.setText("0");
            mine4kNumber.setText("0");
            mineStart.setText("0");
            minePlayNumber.setText("0");
            mineIsVip.setVisibility(View.GONE);
            mineNoVip.setVisibility(View.VISIBLE);
        }
    }


    @OnClick({R.id.user_name, R.id.go_user_view, R.id.mine_today_data, R.id.mine_vip_layout, R.id.my_follow_linear,
            R.id.my_buy_linear, R.id.my_download_linear, R.id.my_msg_linear, R.id.my_advert_linear,
            R.id.my_free_advert, R.id.my_share_advert, R.id.my_video_linear, R.id.my_earnings, R.id.my_question, R.id.my_feedback,
            R.id.my_share_rose_powder, R.id.my_setting, R.id.mine_hardcore_fans, R.id.mine_golden_fans, R.id.fabulous_num_content})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.user_name:
                if (LoginUtil.startLogin(getActivity())) {
                    CommonIntent.startLoginActivity(getActivity());
                }
                break;

            case R.id.go_user_view:
                if (LoginUtil.startLogin(getActivity())) {
                    CommonIntent.startLoginActivity(that);
                } else {
                    CommonIntent.startUserInfoActivity(that);
                }
                break;
            case R.id.mine_today_data:
                if (LoginUtil.startLogin(getActivity())) {
                    CommonIntent.startLoginActivity(that);
                } else {
                    ToastUtils.shortToast("今日数据");
                }

                break;
            case R.id.mine_vip_layout:
                if (LoginUtil.startLogin(getActivity())) {
                    CommonIntent.startLoginActivity(that);
                } else {
                    CommonIntent.startRechargeActivity(that, 0);
                }

                break;
            case R.id.my_follow_linear:
                if (LoginUtil.startLogin(getActivity())) {
                    CommonIntent.startLoginActivity(that);
                } else {
                    CommonIntent.startHistoryRecordActivity(that);
                }
                break;

            case R.id.my_buy_linear:
                if (LoginUtil.startLogin(getActivity())) {
                    CommonIntent.startLoginActivity(that);
                    return;
                }
                CommonIntent.startMyCollectionActivity(that);
                break;
            case R.id.my_download_linear:
                if (LoginUtil.startLogin(getActivity())) {
                    CommonIntent.startLoginActivity(that);
                } else {
                    CommonIntent.startMyBuyActivity(that);
                }
                break;

            case R.id.my_msg_linear:
                if (LoginUtil.startLogin(getActivity())) {
                    CommonIntent.startLoginActivity(that);
                } else {
                    CommonIntent.startMyDownloadActivity(that);
                }
                break;

            case R.id.my_advert_linear:
                if (LoginUtil.startLogin(getActivity())) {
                    CommonIntent.startLoginActivity(that);
                    return;
                }
                CommonIntent.startAdvertAdminActivity(that);
                break;
            case R.id.my_free_advert:
                if (LoginUtil.startLogin(getActivity())) {
                    CommonIntent.startLoginActivity(that);
                    return;
                }
                startActivity(new Intent(getActivity(), FreeAdvertActivity.class));
                break;
            case R.id.my_share_advert:
                //分享推广告
                if (LoginUtil.startLogin(getActivity())) {
                    CommonIntent.startLoginActivity(that);
                } else {
                    startActivity(new Intent(getActivity(), ShareTweetsAty.class));
                }
                break;
            case R.id.my_video_linear:
                if (LoginUtil.startLogin(getActivity())) {
                    CommonIntent.startLoginActivity(that);
                } else {
                    CommonIntent.startMyVideoActivity((BaseActivity) getActivity());
                }
                break;

            case R.id.my_earnings:
                if (LoginUtil.startLogin(getActivity())) {
                    CommonIntent.startLoginActivity(that);
                    return;
                }
                startActivity(new Intent(getActivity(), ProfitActivity.class));
                break;

            case R.id.my_question:
                if (LoginUtil.startLogin(getActivity())) {
                    CommonIntent.startLoginActivity(that);
                    return;
                }
                startActivity(new Intent(getActivity(), QuestionAty.class));
                break;

            case R.id.my_feedback:
                if (LoginUtil.startLogin(getActivity())) {
                    CommonIntent.startLoginActivity(that);
                } else {
                    CommonIntent.startFeedBackActivity(that);
                }
                break;

            case R.id.my_share_rose_powder:
                if (LoginUtil.startLogin(getActivity())) {
                    CommonIntent.startLoginActivity(that);
                } else {
                    if (name == null) {
                        return;
                    }
//                    CommonIntent.startTuiGuangActivity(that);
                    Intent intent = new Intent(getContext(), TuiGuangActivity.class);
                    intent.putExtra("userName", name);
                    startActivity(intent);
                }
                break;
            case R.id.mine_hardcore_fans:
                if (LoginUtil.startLogin(getActivity())) {
                    CommonIntent.startLoginActivity(that);
                    return;
                }
                Intent intent = new Intent(getActivity(), MyFansAty.class);
                intent.putExtra("isGoldFans", 0);
                startActivity(intent);
                break;
            case R.id.mine_golden_fans:
                if (LoginUtil.startLogin(getActivity())) {
                    CommonIntent.startLoginActivity(that);
                    return;
                }
                Intent intent2 = new Intent(getActivity(), MyFansAty.class);
                intent2.putExtra("isGoldFans", 1);
                startActivity(intent2);
                break;
            case R.id.fabulous_num_content:
                if (LoginUtil.startLogin(getActivity())) {
                    CommonIntent.startLoginActivity(that);
                    return;
                }
                startActivity(new Intent(getActivity(), MessageNoticeAty.class));
                break;
            case R.id.my_setting:
                CommonIntent.startSettingActivity(that);
                break;
            default:
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode) {
            case GET_USER_INFO_CODE:
                if (bean.status == 1) {
                    loginInfo = bean.Data();
                    Log.d("zdl", "============" + loginInfo.ali_name);
                    if (loginInfo != null) {
                        GlideMediaLoader.loadHead(that, userHeaderImg, loginInfo.icon);
                        userName.setText(loginInfo.nickname);
                        userIdSignature.setText(loginInfo.signature);
                        Config.putLoginInfo(loginInfo);
                    }
                }
                break;
            case MINE_FANS_CODE:
                if (bean.status == 1) {
                    followsBean = bean.Data();
                    if (followsBean != null) {
//                        Log.e("zjl","followsBean 有数据:" + followsBean.fans + "\n" + followsBean.focus + "\n" +followsBean.gold_fans);
                        mineHardcoreFans.setText(followsBean.fans);
                        mineGoldenFans.setText(followsBean.gold_fans);
                        mineFollow.setText(followsBean.focus);
                    }
                } else {
                    ToastUtils.shortToast("服务器返回异常1");
                }
                break;
            case MINE_TODAY_DATA_CODE:
                if (bean.status == 1) {
                    todayDataBean = bean.Data();
                    Log.e("zjl", "todayDataBean 有数据:" + todayDataBean.toString());
                    if (todayDataBean != null) {
                        Log.e("zjl", "todayDataBean 有数据:" + todayDataBean.works);
                        int minutes = (todayDataBean.today_time % (60 * 60)) / 60;
                        mineTodayUsetime.setText(minutes + "");
                        mine4kGetMoney.setText(todayDataBean._$4k_income + "");

                        mineAlbumGains.setText(todayDataBean.collection + "");

                        mineRevenue.setText(todayDataBean.play_income + "");
                        mineTotalMoney.setText(todayDataBean.total_play + "");
                        mineWorksNumber.setText(todayDataBean.works + "");
                        mine4kNumber.setText(todayDataBean._$4k + "");
                        mineStart.setText(todayDataBean.like + "");
                        minePlayNumber.setText(todayDataBean.play + "W");
                    }
                } else {
                    ToastUtils.shortToast("服务器返回异常2");
                }
                break;

            default:
                break;
        }
        super.nofityUpdate(requestCode, bean);
    }

    String name;

    /**
     * 用户信息的接口 里面需要的参数是 uid
     */
    public void getUserInfor() {
        HttpRequest.POST(getActivity(),
                BaseQuestConfig.GET_USER_INFO,
                new Parameter().add("uid", Config.getLoginInfo().id),
                new ResponseListener() {
                    //
                    @Override
                    public void onResponse(String value, Exception error) {
                        JsonMap jsonMap = JsonMap.parse(value);
                        if (jsonMap == null) {
                            return;
                        }
                        if (jsonMap.get("status") == null) {
                            return;
                        }
                        if (jsonMap.get("status").equals("1")) {
                            JsonMap data = jsonMap.getJsonMap("data");
                            GlideMediaLoader.loadHead(that, userHeaderImg, data.getString("icon"));
                            name = data.getString("nickname");
                            userName.setText(data.getString("nickname"));
                            if (data.getString("signature") != null) {
                                userIdSignature.setText(data.getString("signature"));
                            }
//                                    Config.putLoginInfo(loginInfo);
                        }
                    }
                });
    }


    /**
     * 查询会员购买记录的接口 里面需要的参数是 uid
     */
    public void getMineMemberRecord() {
        HttpRequest.POST(getActivity(),
                BaseQuestConfig.MINE_MEMBER_RECORD,
                new Parameter()
                        .add("uid", Config.getLoginInfo().id)
                        .add("md5", MD5Utils.md5()),
                new ResponseListener() {
                    @Override
                    public void onResponse(String value, Exception error) {
                        if (error == null) {
                            JsonMap jsonMap = JsonMap.parse(value);
                            if (jsonMap.get("status") == null) {
                                ToastUtils.longToast("数据出错");
                                return;
                            }
                            if (jsonMap.get("status").equals("1")) {
//                                JsonMap data = jsonMap.getJsonMap("data");
                                JsonList data1 = jsonMap.getList("data");

                                if (data1.isEmpty()) {
                                    Log.e("zjl", "data 为 []");
                                    mineIsVip.setVisibility(View.GONE);
                                    mineNoVip.setVisibility(View.VISIBLE);
                                } else {
                                    Log.e("zjl", "data 有值");
                                    mineIsVip.setVisibility(View.VISIBLE);
                                    mineNoVip.setVisibility(View.GONE);
                                    termOfValidity.setText(data1.getJsonMap(0).getString("end_time"));
                                    termOfValidity2.setText(data1.getJsonMap(0).getString("end_time"));
                                }
//                                List<MemberRecordBean> memberRecordBeans = bean.Data();
//                                Log.e("zjl2", "memberRecordBeans 有数据:" + bean.Data());
//                                if (!EmptyDeal.isEmpy(memberRecordBeans)) {
//                                    Log.e("zjl2", "memberRecordBeans 有数据:" + memberRecordBeans.size());
//                                    int isPastDue = Integer.parseInt(memberRecordBeans.get(0).is_past_due);
//                                    if (isPastDue == 0) {
//                                        //是会员
//                                        Log.e("zjl2", "memberRecordBeans 是会员:" + memberRecordBeans.size());
//                                        mineIsVip.setVisibility(View.VISIBLE);
//                                        mineNoVip.setVisibility(View.GONE);
//                                        termOfValidity.setText(memberRecordBeans.get(0).end_time);
//                                        termOfValidity2.setText(memberRecordBeans.get(0).end_time);
//                                    } else {
//                                        Log.e("zjl2", "memberRecordBeans 不是会员:" + memberRecordBeans.size());
//                                        //到期或者不是会员
//                                        mineIsVip.setVisibility(View.GONE);
//                                        mineNoVip.setVisibility(View.VISIBLE);
//                                    }
//                                } else {
//                                    mineIsVip.setVisibility(View.GONE);
//                                    mineNoVip.setVisibility(View.VISIBLE);
//                                    Log.e("zjl2", "memberRecordBeans 你返回为[] ?? 卧槽:" + memberRecordBeans.size());
//                                }
                            }
                        } else {
                            ToastUtils.longToast("后台数据出错");
                        }
                    }
                });
    }

}