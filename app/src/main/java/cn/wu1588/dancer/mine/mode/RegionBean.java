package cn.wu1588.dancer.mine.mode;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class RegionBean implements Parcelable {

    private String id;
    private String code;
    private String name;
    private String level;
    private String citycode;
    private String center;
    private String  parentid;
    private List<RegionBean> city;


    protected RegionBean(Parcel in) {
        id = in.readString();
        code = in.readString();
        name = in.readString();
        level = in.readString();
        citycode = in.readString();
        center = in.readString();
        parentid = in.readString();
        city = in.createTypedArrayList(RegionBean.CREATOR);
    }

    public static final Creator<RegionBean> CREATOR = new Creator<RegionBean>() {
        @Override
        public RegionBean createFromParcel(Parcel in) {
            return new RegionBean(in);
        }

        @Override
        public RegionBean[] newArray(int size) {
            return new RegionBean[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getCitycode() {
        return citycode;
    }

    public void setCitycode(String citycode) {
        this.citycode = citycode;
    }

    public String getCenter() {
        return center;
    }

    public void setCenter(String center) {
        this.center = center;
    }

    public String getParentid() {
        return parentid;
    }

    public void setParentid(String parentid) {
        this.parentid = parentid;
    }

    public List<RegionBean> getCity() {
        return city;
    }

    public void setCity(List<RegionBean> city) {
        this.city = city;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(code);
        parcel.writeString(name);
        parcel.writeString(level);
        parcel.writeString(citycode);
        parcel.writeString(center);
        parcel.writeString(parentid);
        parcel.writeTypedList(city);
    }
}
