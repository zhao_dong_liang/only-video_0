package cn.wu1588.dancer.mine.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.my.toolslib.StringUtils;

import butterknife.BindView;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;

//编辑个性签名页面
public class EditUserSignActivity extends LBaseActivity implements TextWatcher {
    @BindView(R.id.title_bar)
    TitleBar title_bar;
    @BindView(R.id.sign_edit)
    EditText sign_edit;
    @BindView(R.id.text_num)
    TextView text_num;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setDark(CommonApp.getApplication().isBarTvColor());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user_sign);
        initViews();
    }

    protected void initViews() {
        title_bar.setTitleBarBackgroundDrawable(CommonApp.getApplication().getStateBarDrawable());
        sign_edit.addTextChangedListener(this);
        title_bar.setRightListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s = sign_edit.getText().toString();
                Intent intent=null;
                if (!StringUtils.isNull(s)){
                     intent=new Intent();
                    intent.putExtra("text",s);
                }
                setResult(1,intent);
                finish();
            }
        });
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        String s = sign_edit.getText().toString();
        int length = s.length();
        text_num.setText(length+"/"+60);
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (sign_edit!=null){
            sign_edit.removeTextChangedListener(this);
        }
    }
}
