package cn.wu1588.dancer.mine.mode;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zhou on 2021/1/5 16:20.
 */
public class TodayDataBean {


    /**
     * works : 4
     * 4k : 0
     * like : 0
     * play : 0
     * 4k_income : 0
     * play_income : 0
     * can_carry : 0.00
     * today_time : 0
     * total_play : 0
     */

    public int works;
    @SerializedName("4k")
    public int _$4k;
    public int like;
    public int play;
    @SerializedName("4k_income")
    public int _$4k_income;
    public int play_income;
    public String can_carry;
    public int today_time;
    public int total_play;
    public int collection;

    @Override
    public String toString() {
        return "TodayDataBean{" +
                "works=" + works +
                ", _$4k=" + _$4k +
                ", like=" + like +
                ", play=" + play +
                ", _$4k_income=" + _$4k_income +
                ", play_income=" + play_income +
                ", can_carry='" + can_carry + '\'' +
                ", today_time=" + today_time +
                ", total_play=" + total_play +
                ", collection=" + collection +
                '}';
    }
}
