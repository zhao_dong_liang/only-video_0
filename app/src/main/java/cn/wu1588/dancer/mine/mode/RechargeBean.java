package cn.wu1588.dancer.mine.mode;

import java.util.List;

public class RechargeBean {


    /**
     * "is_popout": 1,
     *         "popout_content": "支付请注意金额",
     * h5 : https://test.cnsunrun.com/duboshiping_app/Appapi/Channel/UserVip/user_vip_h5/md5/275c7a456dfc0796b323191b4fc580e4.html
     * user_info : {"icon":"Uploads/Avatar/2019-04-18/5cb8407377fee.jpg","id":"74","identity_id":"0","is_member":"1","member_end_time":"2019-04-30 00:00:00","member_start_time":"2019-01-01 00:00:00","nickname":"王二小","status":"2019-04-30"}
     * vip_meal : [{"day":"30","discount_price":"5.00","id":"1","price":"11.00","title":"月卡"},{"day":"365","discount_price":"99.00","id":"2","price":"115.00","title":"年卡"},{"day":"90","discount_price":"25.00","id":"3","price":"30.00","title":"季卡"}]
     */

    public String h5;
    public String is_popout;
    public String popout_content;
    public UserInfoBean user_info;
    public List<VipMealBean> vip_meal;

    public boolean isShow(){
        return "1".equals(is_popout);
    }
    public static class UserInfoBean {
        /**
         * icon : Uploads/Avatar/2019-04-18/5cb8407377fee.jpg
         * id : 74
         * identity_id : 0
         * is_member : 1
         * member_end_time : 2019-04-30 00:00:00
         * member_start_time : 2019-01-01 00:00:00
         * nickname : 王二小
         * status : 2019-04-30
         */

        public String icon;
        public String id;
        public String identity_id;
        public String is_member;
        public String member_end_time;
        public String member_start_time;
        public String nickname;
        public String status;

    }

    public static class VipMealBean {
        /**
         * day : 30
         * discount_price : 5.00
         * id : 1
         * price : 11.00
         * title : 月卡
         */

        public String day;
        public String discount_price;
        public String id;
        public String price;
        public String title;
        public String is_commend;

        public boolean isCommend() {
            return "1".equals(is_commend);
        }

    }
}
