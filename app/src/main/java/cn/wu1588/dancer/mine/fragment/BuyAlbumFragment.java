package cn.wu1588.dancer.mine.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.my.toolslib.http.utils.LzyResponse;
import com.sunrun.sunrunframwork.uibase.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.mine.adapter.BuyAlbumAdapter;
import cn.wu1588.dancer.mine.mode.BuyAlbumBean;
import cn.wu1588.dancer.mine.mode.MyAlbumBean;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.enums.MyVideoTypeEnum;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.common.util.PageLimitDelegate;

public class BuyAlbumFragment extends LBaseFragment {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.num_tv)
    TextView num_tv;
    @BindView(R.id.add_layout)
    LinearLayout add_layout;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;

    private BuyAlbumAdapter adapter;
    private int page = 0;
    private int type;

    public static BuyAlbumFragment newInstance(int type) {
        Bundle args = new Bundle();
        args.putInt("type", type);
        BuyAlbumFragment fragment = new BuyAlbumFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.buy_album_fragment;
    }

    PageLimitDelegate<Object> pageLimitDelegate = new PageLimitDelegate<>(new PageLimitDelegate.DataProvider() {
        @Override
        public void loadData(int page) {
            if (type == MyVideoTypeEnum.ALBUM_TYPE.getCode()) {
                BaseQuestStart.getUserChannel(BuyAlbumFragment.this, Config.getLoginInfo().id);
            } else {
                BaseQuestStart.getMyBuyAlbum(page,BuyAlbumFragment.this);
            }
        }
    });

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            type = getArguments().getInt("type", 0);
            if (type == MyVideoTypeEnum.ALBUM_TYPE.getCode()) {
                add_layout.setVisibility(View.VISIBLE);
                num_tv.setVisibility(View.VISIBLE);
            }
        }
        adapter = new BuyAlbumAdapter();
        adapter.setSpanSizeLookup((gridLayoutManager, i) -> {
            Object item = adapter.getItem(i);
            return item instanceof BuyAlbumBean ? 3 : 1;
        });
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        pageLimitDelegate.attach(refreshLayout, recyclerView, adapter);
    }

    @Override
    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
        super.nofityUpdateUi(requestCode, response, view);
        if (requestCode == BaseQuestStart.GET_MY_BUY_ALBUM_CODE) {//我购买的专辑
            List<BuyAlbumBean> myAlbumBeans = (List<BuyAlbumBean>) response.data;
            List<Object> results = new ArrayList<Object>();
            adapter.setType(1);
            if (myAlbumBeans == null) {
                pageLimitDelegate.setData(results);
                return;
            }


            for (BuyAlbumBean bean : myAlbumBeans) {
                results.add(bean);
                if (bean.movies != null) {
                    results.addAll(bean.movies);
                }

            }
            pageLimitDelegate.setData(results);
        } else if (requestCode == BaseQuestStart.GET_USER_CHANNEL_CODE) {//我的专辑
            MyAlbumBean myAlbumBean = (MyAlbumBean) response.data;
            List<Object> results = new ArrayList<Object>();

            if (myAlbumBean == null) {
                pageLimitDelegate.setData(results);
                return;
            }
            List<BuyAlbumBean> beans = myAlbumBean.channels;
            if (pageLimitDelegate.page == 1) {
                num_tv.setText("共" + myAlbumBean.channels_count + "个专辑");
            }
            if (beans == null) {
                pageLimitDelegate.setData(results);
                return;
            }


            for (BuyAlbumBean bean : beans) {
                bean.viewType = 1;
                results.add(bean);
                if (bean.movies != null) {
                    results.addAll(bean.movies);
                }

            }
            pageLimitDelegate.setData(results);
        }
    }

    @OnClick({R.id.add_layout})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add_layout://添加专辑
                CommonIntent.startAddAlbumActivity((BaseActivity) getActivity(), 1);
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        pageLimitDelegate.refreshPage();
    }
}
