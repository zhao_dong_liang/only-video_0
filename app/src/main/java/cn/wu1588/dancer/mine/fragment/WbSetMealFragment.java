package cn.wu1588.dancer.mine.fragment;

import android.os.Bundle;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.sunrun.sunrunframwork.bean.BaseBean;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.mine.adapter.WbAdapter;
import cn.wu1588.dancer.mine.mode.GoldMealBean;
import cn.wu1588.dancer.base.LBaseFragment;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.RecycleHelper;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.WB_SET_MEAL_CODE;

/**
 * Created by zhou on 2021/1/6 18:48.
 */
public class WbSetMealFragment extends LBaseFragment implements BaseQuickAdapter.OnItemClickListener {

    @BindView(R.id.wbRv)
    RecyclerView wbRv;

    private WbAdapter wbAdapter;
    private List<GoldMealBean> goldMealBeans = new ArrayList<>();
    public GoldMealBean goldMealBean;
    public int selecteditem = 0;

    public static WbSetMealFragment newInstance() {
        WbSetMealFragment fragment = new WbSetMealFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_wb_set_meal;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        initData();
    }

    private void initData() {
        BaseQuestStart.getWbSetMeal(WbSetMealFragment.this);
    }

    private void initView() {
        RecycleHelper.setLinearLayoutManager(wbRv, LinearLayoutManager.VERTICAL);
        wbAdapter = new WbAdapter();
        wbRv.setAdapter(wbAdapter);
        wbAdapter.setOnItemClickListener(this);
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode){
            case WB_SET_MEAL_CODE:
                if(bean.status ==1){
                    goldMealBeans.clear();
                    goldMealBeans = bean.Data();
                    wbAdapter.setSelectedPos(selecteditem);
                    wbAdapter.setNewData(goldMealBeans);
//                    adapterV3.notifyDataSetChanged();
                    goldMealBean = wbAdapter.getItem(selecteditem);
                }
                break;
            default:
                break;

        }
    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        goldMealBean = wbAdapter.getItem(position);
        selecteditem = position;
        wbAdapter.setSelectedPos(selecteditem);
    }
}
