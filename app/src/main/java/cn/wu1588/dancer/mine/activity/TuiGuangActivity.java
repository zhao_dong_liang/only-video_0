package cn.wu1588.dancer.mine.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.kongzue.baseframework.interfaces.DarkStatusBarTheme;
import com.kongzue.baseframework.interfaces.Layout;
import com.kongzue.baseframework.util.JumpParameter;
import com.sunrun.sunrunframwork.uiutils.UIUtils;

import java.io.File;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.base.BaseAty;
import cn.wu1588.dancer.common.quest.Config;

import static android.os.Environment.DIRECTORY_DCIM;

/**
 * 我要推广
 */
@Layout(R.layout.activity_tuiguang)
@DarkStatusBarTheme(true)
public class TuiGuangActivity extends BaseAty {

    private WebView webView;
    private ImageView image_finish;

    @Override
    public void initViews() {
        image_finish = findViewById(R.id.image_finish);
        webView = findViewById(R.id.webView);
    }
    @Override
    public void initDatas(JumpParameter parameter) {
        String userName = getIntent().getStringExtra("userName");
        webView.loadUrl("http://app.myylook.com/xy/index.html?uid=" + Config.getLoginInfo().id + "&nickname=" + userName);
        WebSettings webSettings = webView.getSettings();
        //设置自适应屏幕，两者合用
        webSettings.setUseWideViewPort(true); //将图片调整到适合webview的大小
        webSettings.setLoadWithOverviewMode(true); // 缩放至屏幕的大小
        //缩放操作
        webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK); //关闭webview中缓存
        webSettings.setLoadsImagesAutomatically(true); //支持自动加载图片
        webSettings.setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                view.loadUrl(url);
                return true;
            }
        });
    }

    @Override
    public void setEvents() {
        image_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                finish();
            }
        });
    }

    private void getScreenShot(View view) {
        Context context = view.getContext();
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();  //启用DrawingCache并创建位图
        Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache()); //创建一个DrawingCache的拷贝，因为DrawingCache得到的位图在禁用后会被回收
        view.setDrawingCacheEnabled(false);  //禁用DrawingCahce否则会影响性能
        File dcimDirectory = Environment.getExternalStoragePublicDirectory(DIRECTORY_DCIM);
        File screenshot = new File(dcimDirectory, System.currentTimeMillis() + ".jpg");
        if (UIUtils.saveBitmapToFile(bitmap, screenshot.getAbsolutePath())) {
            UIUtils.shortM("保存成功");
            // 其次把文件插入到系统图库
            try {
                MediaStore.Images.Media.insertImage(context.getContentResolver(),
                        screenshot.getAbsolutePath(), screenshot.getName(), null);
                // 最后通知图库更新
                context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + screenshot.getAbsolutePath())));
            } catch (Exception e) {
                e.printStackTrace();
                UIUtils.shortM("截图失败,请检查存储权限");
            }

        } else {
            UIUtils.shortM("截图失败,请检查存储权限");
        }
    }
}
