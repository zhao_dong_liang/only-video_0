package cn.wu1588.dancer.mine.mode;

import java.util.List;

public class ProblemBean {

    /**
     * questionlist : [{"id":"3","title":"333","content":"444"}]
     * feedback_category : [{"id":"3","title":"标签错误"},{"id":"2","title":"播放卡顿"},{"id":"1","title":"无法播放"}]
     * user_agreement : &lt;p&gt;224&lt;/p&gt;
     * version : {"version":"1.1.18","description":"本次更新内容为123456","url":"https://test.cnsunrun.com/duboshiping_app/2454556787"}
     */

    public String user_agreement;
    public VersionBean version;
    public List<QuestionlistBean> questionlist;
    public List<FeedbackCategoryBean> feedback_category;

    public static class VersionBean {
        /**
         * version : 1.1.18
         * description : 本次更新内容为123456
         * url : https://test.cnsunrun.com/duboshiping_app/2454556787
         */

        public String version;
        public String description;
        public String url;


    }

    public static class QuestionlistBean {
        /**
         * id : 3
         * title : 333
         * content : 444
         */

        public String id;
        public String title;
        public String content;


    }

    public static class FeedbackCategoryBean {
        /**
         * id : 3
         * title : 标签错误
         */

        public String id;
        public String title;
        public boolean switechBean;

    }
}
