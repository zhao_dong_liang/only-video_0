package cn.wu1588.dancer.mine.mode;

/**
 * Created by zhou on 2021/1/5 15:27.
 */
public class FollowsBean {


    /**
     * focus : 0
     * fans : 0
     * gold_fans : 0
     */

    public String focus;
    public String fans;
    public String gold_fans;

    public String getFocus() {
        return focus;
    }

    public void setFocus(String focus) {
        this.focus = focus;
    }

    public String getFans() {
        return fans;
    }

    public void setFans(String fans) {
        this.fans = fans;
    }

    public String getGold_fans() {
        return gold_fans;
    }

    public void setGold_fans(String gold_fans) {
        this.gold_fans = gold_fans;
    }

}
