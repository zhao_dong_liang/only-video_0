package cn.wu1588.dancer.mine.activity;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.widget.TextView;

import cn.wu1588.dancer.R;
import com.cnsunrun.commonui.widget.titlebar.TitleBar;
import com.makeramen.roundedimageview.RoundedImageView;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.TextColorUtils;
import com.sunrun.sunrunframwork.uiutils.UIUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.adp.CommmentsDetailAdapter;
import cn.wu1588.dancer.home.mode.CommentDetailBean;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_COMMENTS_DETAIL_CODE;

/**
 * 通知详情
 */
public class NoticeDetailActivity extends LBaseActivity {


    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.rIvStar)
    RoundedImageView rIvStar;
    @BindView(R.id.tvNikeName)
    TextView tvNikeName;
    @BindView(R.id.tvTime)
    TextView tvTime;
    @BindView(R.id.tvZanNum)
    TextView tvZanNum;
    @BindView(R.id.tvContent)
    TextView tvContent;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private String noticeid;
    private String id;
    private String pid;
    private String nickname;
    private int status;
    private CommmentsDetailAdapter commentsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice_detail);
        ButterKnife.bind(this);
        initViews();
        initData();
    }

    private void initData() {
        UIUtils.showLoadDialog(that);
        BaseQuestStart.getCommentsDetail(this, id, noticeid);
    }

    private void initViews() {
        id = getIntent().getStringExtra("id");
        noticeid = getIntent().getStringExtra("noticeid");
        commentsAdapter = new CommmentsDetailAdapter();
        recyclerView.setAdapter(commentsAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(that));
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode) {
            case GET_COMMENTS_DETAIL_CODE:
                if (bean.status == 1) {
                    CommentDetailBean commentDetailBean = bean.Data();
                    updateUI(commentDetailBean);

                }
                break;

        }
        super.nofityUpdate(requestCode, bean);
    }

    private void updateUI(CommentDetailBean commentDetailBean) {
        pid = commentDetailBean.mem_id;
        nickname = commentDetailBean.nickname;
        status = commentDetailBean.status;
        GlideMediaLoader.loadHead(that, rIvStar, commentDetailBean.icon);
        tvNikeName.setText(commentDetailBean.nickname);
        tvZanNum.setText(commentDetailBean.like_num);
        tvContent.setText(commentDetailBean.content);
        commentsAdapter.setNewData(commentDetailBean.child);
        if (commentDetailBean.status == 1) {
            TextColorUtils.setCompoundDrawables(tvZanNum, null, null, that.getResources().getDrawable(R.drawable.pinglun_btn_zan_selected), null);
        } else {
            TextColorUtils.setCompoundDrawables(tvZanNum, null, null, that.getResources().getDrawable(R.drawable.pinglun_btn_zan_normal), null);
        }
    }
}
