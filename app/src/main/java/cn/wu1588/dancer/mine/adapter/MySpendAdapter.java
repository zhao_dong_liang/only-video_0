package cn.wu1588.dancer.mine.adapter;

import cn.wu1588.dancer.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import cn.wu1588.dancer.mine.mode.MemberSpendBean;


public class MySpendAdapter extends BaseQuickAdapter<MemberSpendBean, BaseViewHolder> {

    public MySpendAdapter() {
        super(R.layout.item_my_spend);
    }

    @Override
    protected void convert(BaseViewHolder helper, MemberSpendBean item) {
        if (item == null) {
            return;
        }
        if (item.vip == null) {
            return;
        }
        if (item.money == null) {
            return;
        }
        if (item.add_time == null || item.add_time.length() < 9) {
            return;
        }
        helper.setText(R.id.tv_type, item.vip.title)
                .setText(R.id.tv_price_title, item.vip.day)
                .setText(R.id.tv_money, mContext.getString(R.string.price, item.money))
                .setText(R.id.tv_time, item.add_time.substring(0,10));
    }
}
