package cn.wu1588.dancer.mine.adapter;

import android.view.View;
import android.widget.CheckBox;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.my.toolslib.StringUtils;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.util.DataUtils;
import cn.wu1588.dancer.mine.mode.MyFollowVideoBean;

public class FollowFansAdapter extends BaseQuickAdapter<MyFollowVideoBean, BaseViewHolder> {
    private int type;

    public void setType(int type) {
        this.type = type;
    }

    public FollowFansAdapter() {
        super(R.layout.follow_fans_item);
    }

    @Override
    protected void convert(BaseViewHolder helper, MyFollowVideoBean item) {

        if (StringUtils.isNull(item.signature)){
            item.signature="";
        }
        String fansNum="";
        if (item.attention_num>1000){
            fansNum= DataUtils.div(item.attention_num, 10000, 1)+"万";
        }else {
            fansNum=  String.valueOf(item.attention_num);
        }
        GlideMediaLoader.loadHead(mContext,helper.getView(R.id.header_img),item.icon);
        helper.setText(R.id.name_tv,item.name)
        .setText(R.id.tv_autograph,item.signature)
        .setText(R.id.fans_num,"粉丝："+fansNum);

        if (item.is_talent==0){//是否达人认证
            helper.getView(R.id.is_talent_view).setVisibility(View.GONE);
        }else {
            helper.getView(R.id.is_talent_view).setVisibility(View.VISIBLE);
        }

        CheckBox follow_box= helper.getView(R.id.follow_box);
        if(type==0){
            if (item.mutual_attention==1&&item.is_attention){//是否互相关注
                follow_box.setChecked(false);
                follow_box.setText("互相关注");
            }else {
                if (item.is_attention){
                    follow_box.setChecked(false);
                    follow_box.setText("已关注");
                }else {
                    follow_box.setChecked(true);
                    follow_box.setText("关注");
                }
            }
        }else {
            if (item.mutual_attention==1&&item.is_attention){//是否互相关注
                follow_box.setChecked(false);
                follow_box.setText("互相关注");
            }else {
                if (item.is_attention){
                    follow_box.setChecked(false);
                    follow_box.setText("已关注");
                }else {
                    follow_box.setChecked(true);
                    follow_box.setText("关注");
                }
            }
        }

        helper.addOnClickListener(R.id.follow_box,R.id.header_img);
    }
}
