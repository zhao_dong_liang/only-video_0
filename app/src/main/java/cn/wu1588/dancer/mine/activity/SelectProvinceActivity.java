package cn.wu1588.dancer.mine.activity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.my.toolslib.StringUtils;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;
import com.unistrong.yang.zb_permission.ZbPermission;
import com.unistrong.yang.zb_permission.ZbPermissionFail;
import com.unistrong.yang.zb_permission.ZbPermissionSuccess;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.quest.BaseQuestConfig;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;
import cn.wu1588.dancer.mine.adapter.SelectProCityAdapter;
import cn.wu1588.dancer.mine.mode.RegionBean;

public class SelectProvinceActivity extends LBaseActivity implements AMapLocationListener , BaseQuickAdapter.OnItemClickListener {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.location_tv)
    TextView location_tv;
    @BindView(R.id.location_view)
    View location_view;
    @BindView(R.id.title_bar)
    TitleBar title_bar;


    private int type;//0是省份 1是城市
    private SelectProCityAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_province);
        initViews();
        initData();
        checkPermission();

    }

    private void initViews() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        location_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermission();
            }
        });
    }

    private void initData(){
        ArrayList<RegionBean> list = getIntent().getParcelableArrayListExtra("list");
        if (list==null){
            BaseQuestStart.getRegion(that);
        }else {
            String name=getIntent().getStringExtra("name");
            title_bar.setTitle(name);
            type=1;
            BaseBean baseBean=new BaseBean();
            baseBean.status=1;
            baseBean.data=list;
            nofityUpdate(BaseQuestConfig.GET_REGION_CODE,baseBean);
        }

    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        super.nofityUpdate(requestCode, bean);
        if (requestCode== BaseQuestConfig.GET_REGION_CODE){
            if (bean.status==1){
                setViewData(bean.Data());
            }
        }

    }
    private void setViewData(List<RegionBean> regionBeans){
        if (adapter==null){
            adapter=new SelectProCityAdapter(regionBeans);
            adapter.setOnItemClickListener(this::onItemClick);
            recyclerView.setAdapter(adapter);
            adapter.setEnableLoadMore(false);
        }else {
            adapter.setDatas(regionBeans);
        }
        adapter.notifyDataSetChanged();

    }

    //声明mlocationClient对象
    public AMapLocationClient mlocationClient;
    //声明mLocationOption对象
    public AMapLocationClientOption mLocationOption = null;
    //获取定位
    private void initLocation(){
        mlocationClient = new AMapLocationClient(this);
//初始化定位参数
        mLocationOption = new AMapLocationClientOption();
//设置定位监听
        mlocationClient.setLocationListener(this);
        //设置定位模式为高精度模式，Battery_Saving为低功耗模式，Device_Sensors是仅设备模式
        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
//设置定位间隔,单位毫秒,默认为2000ms
//        mLocationOption.setInterval(10000);
        //单次定位
        mLocationOption.setOnceLocation(true);
        //设置定位参数
        mlocationClient.setLocationOption(mLocationOption);
        //启动定位
        mlocationClient.startLocation();
    }


    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
//        Gson gson=new Gson();
//        String s = gson.toJson(aMapLocation);
        String province = aMapLocation.getProvince();
        String city = aMapLocation.getCity();
        if (!StringUtils.isNull(province)&&!StringUtils.isNull(city)){
            location_tv.setText(province+" "+city);
            Log.i("OkGo:",province+" "+city);
//            Log.i("OkGo:",aMapLocation.getCityCode()+" "+aMapLocation.getAdCode());
//            Log.i("OkGo:",s);
        }else {
            ToastUtils.longToast("定位失败，请打开GPS后重试");
        }
    }

    private final int PERMISSION=1;
    private void checkPermission(){
        ZbPermission.with(that)
                .addRequestCode(PERMISSION)
                .permissions(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,  Manifest.permission.READ_PHONE_STATE
                        )
                .request();
    }
    @ZbPermissionSuccess(requestCode = PERMISSION)
    public void permissionSuccess() {
        initLocation();
    }

    @ZbPermissionFail(requestCode = PERMISSION)
    public void permissionFail() {
    ToastUtils.longToast("请允许定位权限");
    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        if (type==0){
            Intent intent=new Intent(this,SelectProvinceActivity.class);
            List<RegionBean> regionBeans= adapter.getData();
            ArrayList<RegionBean> city = (ArrayList<RegionBean>) regionBeans.get(position).getCity();
            intent.putParcelableArrayListExtra("list", city);
            intent.putExtra("name",regionBeans.get(position).getName());
            startActivityForResult(intent,1);
        }else {
            Intent intent=new Intent();
            intent.putExtra("proName",getIntent().getStringExtra("name"));
           ArrayList<RegionBean> regionBeans= (ArrayList<RegionBean>) adapter.getData();
            intent.putExtra("cityName",regionBeans.get(position).getName());
            setResult(1,intent);
            finish();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data!=null){
            setResult(1,data);
            finish();
        }
    }
}
