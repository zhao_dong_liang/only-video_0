package cn.wu1588.dancer.mine.activity;

import android.os.Bundle;

import cn.wu1588.dancer.R;
import com.cnsunrun.commonui.widget.titlebar.TitleBar;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.UIUtils;

import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.util.GetEmptyViewUtils;
import cn.wu1588.dancer.common.util.PageLimitDelegate;
import cn.wu1588.dancer.common.util.RecycleHelper;
import cn.wu1588.dancer.mine.adapter.MySpendAdapter;
import cn.wu1588.dancer.mine.mode.MemberSpendBean;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_CONSUME_CODE;

/**
 * 充值记录
 */
public class RechargeRecordActivity extends LBaseActivity {

    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    PageLimitDelegate pageLimitDelegate = new PageLimitDelegate(new PageLimitDelegate.DataProvider() {
        @Override
        public void loadData(int page) {
            BaseQuestStart.getMemberConsume(that, page);
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recharge_record);
        ButterKnife.bind(this);
        initViews();
    }

    private void initViews() {
        UIUtils.showLoadDialog(that);
        MySpendAdapter mySpendAdapter = new MySpendAdapter();
        RecycleHelper.setLinearLayoutManager(recyclerView, LinearLayoutManager.VERTICAL);
        recyclerView.setAdapter(mySpendAdapter);
        pageLimitDelegate.attach(refreshLayout, recyclerView, mySpendAdapter);
        GetEmptyViewUtils.bindEmptyView(that, mySpendAdapter, 0, "暂无数据", true);
    }

    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode){
            case GET_CONSUME_CODE:
                if (bean.status==1){
                    List<MemberSpendBean> list=bean.Data();
                    pageLimitDelegate.setData(list);
                }
                break;
        }
        super.nofityUpdate(requestCode, bean);
    }
}
