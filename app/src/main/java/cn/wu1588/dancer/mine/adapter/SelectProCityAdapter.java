package cn.wu1588.dancer.mine.adapter;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.mine.mode.RegionBean;

public class SelectProCityAdapter extends BaseQuickAdapter<RegionBean, BaseViewHolder> {

    public SelectProCityAdapter( @Nullable List<RegionBean> data) {
        super(R.layout.select_pro_city_item, data);
    }
    public void setDatas(List<RegionBean> data){
        this.mData=data;
    }

    @Override
    protected void convert(BaseViewHolder helper, RegionBean item) {
        helper.setText(R.id.name_tv,item.getName());
    }
}
