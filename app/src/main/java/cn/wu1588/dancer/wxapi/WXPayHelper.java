package cn.wu1588.dancer.wxapi;

import android.content.Context;

import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;

/**
 * Created by caidong on 2019/9/3.
 * email:mircaidong@163.com
 * describe: 描述
 */
public class WXPayHelper {
    public IWXAPI api;
    private PayReq req;

    private String appId;
    private String prePayId;
    private String partNerId;
    private String partNerSecret;//商户密钥
    private String sign;

    public WXPayHelper(Context context, String appId, String prePayId, String partNerId, String partNerSecret,String sign) {
        this.appId = appId;
        this.sign = sign;
        this.prePayId = prePayId;
        this.partNerId = partNerId;
        this.partNerSecret = partNerSecret;
        api = WXAPIFactory.createWXAPI(context, this.appId,false);
    }

    /**
     * 向微信服务器发起的支付请求
     */
    public void pay() {
        req = new PayReq();
        req.appId = appId;//APP-ID
        req.partnerId = partNerId;//    商户号
        req.prepayId = prePayId;//  预付款ID
        req.nonceStr = partNerSecret;                   //随机数
        req.timeStamp = String.valueOf(System.currentTimeMillis()*0.001);   //时间戳
        req.packageValue = "Sign=WXPay";//固定值Sign=WXPay
//        req.sign = getSign();//签名
        req.sign = sign;
        api.registerApp(appId);
        api.sendReq(req);
    }

    @NonNull
    private String getSign() {
        Map<String, String> map = new HashMap<>();
        map.put("appid", req.appId);
        map.put("partnerid", req.partnerId);
        map.put("prepayid", req.prepayId);
        map.put("package", req.packageValue);
        map.put("noncestr", req.nonceStr);
        map.put("timestamp", req.timeStamp);

        ArrayList<String> sortList = new ArrayList<>();
        sortList.add("appid");
        sortList.add("partnerid");
        sortList.add("prepayid");
        sortList.add("package");
        sortList.add("noncestr");
        sortList.add("timestamp");
        Collections.sort(sortList);

        StringBuilder md5 = new StringBuilder();
        int size = sortList.size();
        for (int k = 0; k < size; k++) {
            if (k == 0) {
                md5.append(sortList.get(k)).append("=").append(map.get(sortList.get(k)));
            } else {
                md5.append("&").append(sortList.get(k)).append("=").append(map.get(sortList.get(k)));
            }
        }
        String stringSignTemp = md5+"&key="+partNerSecret;

        return Md5(stringSignTemp).toUpperCase();
    }

    private String Md5(String s) {
        char[] hexDigits = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            byte[] e = s.getBytes(StandardCharsets.UTF_8);
            MessageDigest mdTemp = MessageDigest.getInstance("MD5");
            mdTemp.update(e);
            byte[] md = mdTemp.digest();
            int j = md.length;
            char[] str = new char[j * 2];
            int k = 0;
            for (byte byte0 : md) {
                str[k++] = hexDigits[byte0 >>> 4 & 15];
                str[k++] = hexDigits[byte0 & 15];
            }
            return new String(str);
        } catch (Exception var10) {
            return null;
        }
    }
}
