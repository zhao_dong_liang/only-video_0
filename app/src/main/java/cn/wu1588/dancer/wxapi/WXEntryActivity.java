package cn.wu1588.dancer.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.sunrun.sunrunframwork.uiutils.ToastUtils;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.modelmsg.SendMessageToWX;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import org.greenrobot.eventbus.EventBus;

import androidx.annotation.Nullable;
import cn.wu1588.dancer.common.event.MessageEvent;
import cn.wu1588.dancer.common.event.SharedCompleteEvent;
import cn.wu1588.dancer.common.util.ConstantValue;
import cn.wu1588.dancer.common.util.SPUtils;

public class WXEntryActivity extends Activity implements IWXAPIEventHandler {
    private static final int RETURN_MSG_TYPE_LOGIN = 1;
    private static final int RETURN_MSG_TYPE_SHARE = 2;

    private IWXAPI api;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("onlyVideo","onCreate:WXEntryActivity");
//通过WXAPIFactory工厂获取IWXApI的示例1
        String wechatid = (String) SPUtils.get(this, ConstantValue.WECHAT_ID, "0");
        api = WXAPIFactory.createWXAPI(this, "wx21fb0e3b0433eded");
        api.handleIntent(getIntent(), this);

        //如果没回调onResp，八成是这句没有写
    }
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        api.handleIntent(intent, this);
    }
    // 微信发送请求到第三方应用时，会回调到该方法
    @Override
    public void onReq(BaseReq req) {
        Log.i("WXPayEntryActivity","onReq"+req+"");
    }

    @Override
    public void onResp(BaseResp baseResp) {
        Log.i("WXPayEntryActivity", "onResp" + baseResp.errStr + "");
        String result = "";
        switch (baseResp.errCode) {
            case BaseResp.ErrCode.ERR_OK:
                result = "发送成功";
//                ToastUtils.shortToast(result);
                if (baseResp instanceof SendAuth.Resp) {
                    String code = ((SendAuth.Resp) baseResp).code;
                    EventBus.getDefault().post(new MessageEvent("wx_code",code));
                } else if (baseResp instanceof SendMessageToWX.Resp) {
                    EventBus.getDefault().post(new SharedCompleteEvent(SharedCompleteEvent.SharedType.WX, getIntent()));
                }
                finish();
                break;
            case BaseResp.ErrCode.ERR_USER_CANCEL:
                result = "发送取消";
                ToastUtils.shortToast(result);
                finish();
                break;
            case BaseResp.ErrCode.ERR_AUTH_DENIED:
                result = "发送被拒绝";
                ToastUtils.shortToast(result);
                finish();
                break;
            default:
                result = "发送返回";
                ToastUtils.shortToast(result);
                finish();
                break;
        }}
}