package cn.wu1588.dancer.main;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sunrun.sunrunframwork.utils.AHandler;

import cn.wu1588.dancer.MainActivity;
import cn.wu1588.dancer.common.CommonIntent;

/**
 * 验证码按钮状态管理
 * Created by cnsunrun on 2018-03-15.
 */

public class CaptchaStateMode {
    private  ImageView ivClose;
    private Activity activity;
    private TextView codeTextView;

    private String normalTxt = "获取验证码", countDownTxt = "%d";
    private int maxTime = 5;
    private AHandler.Task task;

    public CaptchaStateMode(SplashActivity activity, TextView codeTextView, ImageView ivClose) {
        this.ivClose=ivClose;
        this.activity = activity;
        this.codeTextView = codeTextView;

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.getSplash().startNavigatorActivity(null);
            }
        });
        this.codeTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopCountDown();
                CommonIntent.startNavigatorActivity(activity);
                activity.finish();
            }
        });
    }

    /**
     * 设置默认显示的文字 eg:  获取验证码
     *
     * @param normalTxt
     * @return
     */
    public CaptchaStateMode setNormalTxt(String normalTxt) {
        this.normalTxt = normalTxt;
        return this;
    }

    /**
     * 设置倒计时中显示的文字 eg: 重新发送%s秒
     *
     * @param countDownTxt 倒计时文字,需要包含一个%d占位符
     * @return
     */
    public CaptchaStateMode setCountDownTxt(String countDownTxt) {
        this.countDownTxt = countDownTxt;
        return this;
    }

    /**
     * 设置最大时间,默认60s
     *
     * @param time 最大时间
     * @return
     */
    public CaptchaStateMode setMaxTime(int time) {
        this.maxTime = time;
        return this;
    }

    /**
     * 开始倒计时
     *
     * @return 操作结果, 成功/失败
     */
    public boolean beginCountDown() {
        if (task == null) {
            runCountDown();
            return true;
        }
        return false;
    }

    /**
     * 停止倒计时
     */
    public void stopCountDown() {
        codeTextView.setText(normalTxt);
        AHandler.cancel(task);
    }


    /**
     * 开启倒计时,内部实现
     */
    private void runCountDown() {
        task = new AHandler.Task() {
            int time = maxTime;

            @Override
            public void update() {
                if (isDestory()) {
                    cancel();
                    return;
                }
                if (time <= 0) {
                    task.cancel();
                    codeTextView.setVisibility(View.INVISIBLE);
//                    ivClose.setVisibility(View.VISIBLE);
                    activity.startActivity(new Intent(activity, MainActivity.class));
//                    activity.startActivity(new Intent(activity,MainActivity.class));
                    activity.finish();
                } else {
                    codeTextView.setText("跳过  " + String.valueOf(time--));
                }
            }

            @Override
            public boolean cancel() {
                boolean flag = super.cancel();
                boolean isDestory = codeTextView == null || activity == null || activity.isFinishing() || activity.isDestroyed();
                if (isDestory) return true;
                codeTextView.setText(normalTxt);
                time = maxTime;
                task = null;
                return flag;
            }

        };
        AHandler.runTask(task, 0, 1000);
    }

    /**
     * 是否已经消亡
     *
     * @return
     */
    private boolean isDestory() {
        boolean isDestory = codeTextView == null || activity == null || activity.isFinishing() || activity.isDestroyed();
        if (isDestory) {
            codeTextView = null;
            activity = null;
            AHandler.cancel(task);
        }
        return isDestory;
    }
}

