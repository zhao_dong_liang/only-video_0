package cn.wu1588.dancer.main;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.sunrun.sunrunframwork.adapter.ImagePagerAdapter;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;
import com.sunrun.sunrunframwork.utils.AHandler;
import com.sunrun.sunrunframwork.weight.AutoScrollViewPager;
import com.tencent.mmkv.MMKV;
import com.unistrong.yang.zb_permission.ZbPermission;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.wu1588.dancer.channel.mode.SettingInfo;
import cn.wu1588.dancer.BuildConfig;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.model.LoginInfo;
import cn.wu1588.dancer.common.model.PictureInfo;
import cn.wu1588.dancer.common.model.SystemParams;
import cn.wu1588.dancer.common.quest.BaseQuestConfig;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.common.util.ConstantValue;
import cn.wu1588.dancer.common.util.DeviceIdUtil;
import cn.wu1588.dancer.common.util.DisplayUtil;
import cn.wu1588.dancer.common.util.SPUtils;
import cn.wu1588.dancer.home.mode.NavbarColorBean;
import cn.wu1588.dancer.home.mode.ThirdBean;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_TAB_LIST_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_THIRD_LOGIN_PARAM_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_USER_LOGIN_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.SETTING_INFO_CODE;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.START_UP_PICS_CODE;

/**
 * 启动页(引导页)
 */
public class SplashActivity extends LBaseActivity {

    private static final String TAG = "SplashActivity";
    int startNum = 0;
    //开始的时候是2000    改成1s了
    long time = 3000, time2 = 1500;
    static SplashActivity act;
    AHandler.Task task, task2;
    @BindView(R.id.view_pager)
    AutoScrollViewPager viewPager;
    @BindView(R.id.start)
    View start;
    @BindView(R.id.iv_splash)
    ImageView ivSplash;
    @BindView(R.id.tvSkip)
    TextView tvSkip;
    @BindView(R.id.start_skip)
    FrameLayout startSkip;
    @BindView(R.id.lay_bottom)
    LinearLayout layBottom;
    @BindView(R.id.ivClose)
    ImageView ivClose;
    private CaptchaStateMode captchaStateMode;
    private LoginInfo loginInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        act = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Config.deviceId = DeviceIdUtil.getDeviceId(this);

        ButterKnife.bind(this);
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
            finish();
            return;
        }
        startUpData();
        initData();
        startNum = Integer.parseInt(Config.getConfigInfo(this, Config.START_NUM, "0"));// 启动次数
        Config.putConfigInfo(this, Config.START_NUM, String.valueOf(startNum + 1));// 启动次数加1
//        startNum = 0;
        if (startNum <= 0 && hasStartImg()) {
            tvSkip.setVisibility(View.GONE);
            try {
                viewPager.setVisibility(View.VISIBLE);
                viewPager.setSlideBorderMode(AutoScrollViewPager.SLIDE_BORDER_MODE_TO_PARENT);
                String[] bgs = getAssets().list(Config.START_IMG);
                for (int i = 0; i < bgs.length; i++) {
                    bgs[i] = Config.START_IMG + "/" + bgs[i];
                }
                viewPager.setAdapter(new ImagePagerAdapter<String>(this, Arrays.asList(bgs)) {
                            @Override
                            protected void loadImage(@NonNull ImageView imageView, @NonNull String s) {
                            }

                            @Override
                            public View getView(int index, View view, ViewGroup container) {
                                View view1 = super.getView(index, view, container);
                                ImageView imageView = (ImageView) view1.findViewById(R.id.img);
                                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                                return view1;
                            }
                        }.setInfiniteLoop(false)
                                .setOnBannerClickListener(new ImagePagerAdapter.OnBannerClickListener() {
                                    @Override
                                    public void onBannerClick(int position, Object t) {
                                        if (viewPager.getAdapter() != null && viewPager.getCurrentItem() != viewPager.getAdapter().getCount() - 1) {
                                            return;
                                        }
                                        startNavigatorActivity(null);
                                    }
                                })
                );
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
//            PictureInfo pic = bean.Data();
            Glide.with(this).load("").placeholder(R.drawable.defalt_bg).into(ivSplash);
            captchaStateMode = new CaptchaStateMode(this, tvSkip, ivClose).setMaxTime(3);
            captchaStateMode.setNormalTxt("");
            captchaStateMode.beginCountDown();
        }
    }


    private void initData() {
        String deviceId = DeviceIdUtil.getDeviceId(that);
        BaseQuestStart.getTabMenuList(this);
        if (!TextUtils.isEmpty(deviceId)) {
            BaseQuestStart.getUserLogin(that, deviceId);

        }
        BaseQuestStart.getNavbarColor(this);//获取主题颜色
        BaseQuestStart.getThirdLoginParam(this);
        SystemParams.initSystemParams(this);
    }

    private void startUpData() {
        BaseQuestStart.getStartUpPics(that);
    }

    @SuppressLint("WrongConstant")
    @Override
    public void nofityUpdate(int requestCode, BaseBean bean) {
        switch (requestCode) {
            case GET_USER_LOGIN_CODE:
                if (bean.status == 1) {
                    loginInfo = bean.Data();
                    if (loginInfo != null) {
                        BaseQuestStart.getSettingInfo(that);
                        BaseQuestStart.getAddMemberRecord(that, BuildConfig.TITLE, loginInfo.uid, "0");
                    }
                }
                break;
            case START_UP_PICS_CODE:
                if (bean.status == 1) {

                }
                break;
            case GET_TAB_LIST_CODE:
                if (bean.status == 1) {
                    Config.putData("TABBAR",bean.data);
                }
                break;
            case SETTING_INFO_CODE:
                List<SettingInfo> list = bean.Data();
                if (list != null && !list.isEmpty()){
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).param.equals(ConstantValue.NEED_LOGIN)){
                            SPUtils.put(SplashActivity.this,ConstantValue.NEED_LOGIN,list.get(i).value);
                            if ("0".equals(list.get(i).value)){
                                Config.putLoginInfo(loginInfo);
                            }
                        }
                    }
                }
                break;
            case GET_THIRD_LOGIN_PARAM_CODE:
                if (bean.status == 1) {
                    List<ThirdBean> thirdBeanList = bean.Data();
                    if (thirdBeanList != null && !thirdBeanList.isEmpty()){
                        for (int i = 0; i < thirdBeanList.size(); i++) {
                            if (thirdBeanList.get(i).getType().equals("1")){
                                SPUtils.put(SplashActivity.this,ConstantValue.WECHAT_KEY,thirdBeanList.get(i).getApp_key());
                                SPUtils.put(SplashActivity.this,ConstantValue.WECHAT_ID,thirdBeanList.get(i).getApp_id());
                            }else if (thirdBeanList.get(i).getType().equals("2")){
                                SPUtils.put(SplashActivity.this,ConstantValue.QQ_KEY,thirdBeanList.get(i).getApp_key());
                                SPUtils.put(SplashActivity.this,ConstantValue.QQ_ID,thirdBeanList.get(i).getApp_id());
                            }
                        }
                    }
                }
                break;
            case BaseQuestConfig.GET_NAVBAR_COLOR_CODE://获取主题颜色
                    if (bean.status==1){
                        NavbarColorBean colorBean= bean.Data();
                        int[] colors = {Color.parseColor(colorBean.getUp_b_color()),
                                Color.parseColor(colorBean.getUp_e_color()) };
                        GradientDrawable drawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT,colors);
                        int i = DisplayUtil.getScreenHeight() / 2;
//                        drawable.setSize(DisplayUtil.getScreenWidth(),DisplayUtil.getScreenHeight()/2);
//                        drawable.setGradientType(GradientDrawable.RECTANGLE);
                        drawable.setGradientType(GradientDrawable.LINEAR_GRADIENT);//设置线性渐变
                        drawable.setOrientation(GradientDrawable.Orientation.LEFT_RIGHT);//设置渐变方向
                        CommonApp.getApplication().setStateBarDrawable(drawable);
                        CommonApp.getApplication().setBarTvColor(colorBean.getStatus_bar_color()==1?true:false);
                        MMKV mmkv = MMKV.defaultMMKV();
                        mmkv.encode("up_b_color",colorBean.getUp_b_color());
                        mmkv.encode("up_e_color",colorBean.getUp_e_color());
                        mmkv.encode("down_b_color",colorBean.getDown_b_color());
                        mmkv.encode("down_e_color",colorBean.getDown_e_color());
                        mmkv.encode("status_bar_color",colorBean.getStatus_bar_color());
                        mmkv.encode("search_bar_show", colorBean.getSearch_bar_show());
                        mmkv.encode("logo_img", colorBean.getLogo_img());
                    }
                    break;
        }
        super.nofityUpdate(requestCode, bean);
    }

    public static SplashActivity getSplash() {
        return act;
    }

    public static void close() {
        if (act != null) {
            if (act.task == null) {
                act.finish();
            }
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        AHandler.cancel(task);
        act = null;
    }

    @Override
    public void loadFaild(int requestCode, BaseBean bean) {
        super.loadFaild(requestCode, bean);
    }

    public void startNavigatorActivity(View view) {
        CommonIntent.startNavigatorActivity(that);
        finish();
    }

    @OnClick({R.id.start})
    public void onClick(View view) {
        if (viewPager.getAdapter() != null &&
                viewPager.getCurrentItem() != viewPager.getAdapter().getCount() - 1) {
            return;
        }
        if (viewPager.getAdapter() == null) {
            return;
        }
        switch (view.getId()) {
            case R.id.start:
                startNavigatorActivity(null);
                break;
        }
    }

    boolean hasStartImg() {
        try {
            String[] bgs = getAssets().list(Config.START_IMG);
            return bgs != null && bgs.length > 0;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

//    @ZbPermissionSuccess(requestCode = PERMISSION)
//    public void permissionSuccess() {
//        CommonIntent.startNavigatorActivity(that);
//        finish();
//    }
//
//    @ZbPermissionFail(requestCode = PERMISSION)
//    public void permissionFail() {
//        CommonIntent.startNavigatorActivity(that);
//        finish();
//    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        ZbPermission.onRequestPermissionsResult(SplashActivity.this, requestCode, permissions, grantResults);
    }

}
