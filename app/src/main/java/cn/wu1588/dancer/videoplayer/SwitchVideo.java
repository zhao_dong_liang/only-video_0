package cn.wu1588.dancer.videoplayer;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import com.shuyu.gsyvideoplayer.GSYVideoManager;
import com.shuyu.gsyvideoplayer.player.PlayerFactory;
import com.shuyu.gsyvideoplayer.video.StandardGSYVideoPlayer;

import cn.wu1588.dancer.R;
import tv.danmaku.ijk.media.exo2.Exo2PlayerManager;

public class SwitchVideo extends StandardGSYVideoPlayer {

    private RelativeLayout detailBtn;
    private int id;
    private boolean isClick;
    private GSYVideoManager mTmpManager;
    public SwitchVideo(Context context, Boolean fullFlag) {
        super(context, fullFlag);
    }

    public SwitchVideo(Context context) {
        super(context);
    }

    public SwitchVideo(Context context, AttributeSet attrs) {
        super(context, attrs);
        setPlayVideoManager();
    }

    private void setPlayVideoManager() {
        PlayerFactory.setPlayManager(Exo2PlayerManager.class);
    }

    @Override
    protected void init(Context context) {
        super.init(context);
        detailBtn = (RelativeLayout) findViewById(R.id.surface_container);
        detailBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInPlayingState()) {
//                    SwitchUtil.savePlayState(SwitchVideo.this);
//                    getGSYVideoManager().setLastListener(SwitchVideo.this);
                    //fixme 页面跳转是，元素共享，效果会有一个中间中间控件的存在
                    //fixme 这时候中间控件 CURRENT_STATE_PLAYING，会触发 startProgressTimer
                    //FIXME 但是没有cancel
//                    SwitchDetailActivity.startTActivity((Activity) getContext(), SwitchVideo.this);
//                    CommonIntent.startPlayerActivity((Activity)getContext(),String.valueOf(id),SwitchVideo.this);

                    if (isClick){
//                        GSYVideoManager.onPause();
//                        MoviePlayerActivity.startTActivity((Activity)getContext(),String.valueOf(id),SwitchVideo.this,false);
                    }
                }
            }
        });
        if (mIfCurrentIsFullscreen) {
//            detailBtn.setVisibility(GONE);
        }

    }

    @Override
    public int getLayoutId() {
        return R.layout.switch_video;
    }

    public void setSwitchUrl(String url) {
        mUrl = url;
        mOriginUrl = url;
    }
    public void setSwitchCache(boolean cache)  {
        mCache = cache;
    }

    public void setSwitchTitle(String title) {
        mTitle = title;
    }



    public void setSurfaceToPlay() {
        addTextureView();
        getGSYVideoManager().setListener(this);
        checkoutState();
    }

    public SwitchVideo saveState() {
        SwitchVideo switchVideo = new SwitchVideo(getContext());
        cloneParams(this, switchVideo);
        return switchVideo;
    }
    public void setIsClikListner(boolean isClick){
        this.isClick=isClick;

    }
    public void cloneState(SwitchVideo switchVideo)  {
        cloneParams(switchVideo, this);
    }

    public void setMovieID(int id) {
        this.id=id;
    }

    @Override
    protected void changeUiToCompleteClear() {
        super.changeUiToCompleteClear();
    }
}

