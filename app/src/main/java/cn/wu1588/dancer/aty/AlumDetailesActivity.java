package cn.wu1588.dancer.aty;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.cnsunrun.commonui.widget.button.RoundButton;
import com.makeramen.roundedimageview.RoundedImageView;

import java.text.DecimalFormat;

import butterknife.BindView;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.widget.ArcImageView;
import cn.wu1588.dancer.mine.mode.BuyAlbumBean;
import cn.wu1588.dancer.my_video.bean.MyVideoBean;

public class AlumDetailesActivity extends LBaseActivity {

    @BindView(R.id.srf_refresh_layout)
    SwipeRefreshLayout srf_refresh_layout;
    @BindView(R.id.rv_alum_list)
    RecyclerView rv_alum_list;
    private View mHeadView;
    private ArcImageView mAlumCoverView;
    private RelativeLayout mRlAuthor;
    private RoundedImageView mAuthorAvatarView;
    private TextView mAuthorName;
    private RoundButton mFollowView;
    private TextView mAlumTitleView;
    private TextView mMovieCountView;
    private TextView mMoviePriceView;
    private TextView mMoviePriceDiscountView;
    private TextView mMovieDiscountPrioceView;
    private RoundButton mBuyView;
    private TextView mMovieDescView;
    private ImageButton mBackView;

    private BaseQuickAdapter<MyVideoBean, BaseViewHolder> mAdapter;

    private BuyAlbumBean mBuyAlbumBean;

    private DecimalFormat decimalFormat = new DecimalFormat("#.00");

    public static void start(Context context, BuyAlbumBean bean) {
        /*Intent intent = new Intent(context, AlumDetailesActivity.class);
        intent.putExtra("alumBean", bean);
        context.startActivity(intent);*/
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alum_details);
        mBuyAlbumBean = getIntent().getParcelableExtra("alumBean");

        mHeadView = LayoutInflater.from(this).inflate(R.layout.item_head_alum_details, rv_alum_list, false);
        mRlAuthor = mHeadView.findViewById(R.id.rl_author_layout);
        mAlumTitleView = mHeadView.findViewById(R.id.tv_alum_title);
        mAlumCoverView = mHeadView.findViewById(R.id.iv_alum_cover);
        mAuthorAvatarView = mHeadView.findViewById(R.id.iv_video_author_avatar);
        mAuthorName = mHeadView.findViewById(R.id.tv_video_author_name);
        mFollowView = mHeadView.findViewById(R.id.btn_movie_subscription);
        mMovieCountView = mHeadView.findViewById(R.id.tv_movies_count);
        mMoviePriceView = mHeadView.findViewById(R.id.tv_movies_price);
        mMoviePriceDiscountView = mHeadView.findViewById(R.id.tv_movies_vip_discount);
        mMovieDiscountPrioceView = mHeadView.findViewById(R.id.tv_movies_discount_price);
        mMovieDescView = mHeadView.findViewById(R.id.tv_movies_desc);
        mBuyView = mHeadView.findViewById(R.id.btn_movies_buy);
        mBackView = mHeadView.findViewById(R.id.ib_back);

       /* mAlumTitleView.setText(mBuyAlbumBean.title);
        GlideMediaLoader.load(this, mAlumCoverView, mBuyAlbumBean.background_image, R.drawable.ic_place_ad_img);
        GlideMediaLoader.loadHead(this, mAuthorAvatarView, mBuyAlbumBean.user_info.icon);
        mAuthorName.setText(mBuyAlbumBean.user_info.nickname);
        mFollowView.setText(mBuyAlbumBean.followMessage);
        mMovieCountView.setText(String.format("共%s个视频", mBuyAlbumBean.movies_count));
        mMoviePriceView.setText(String.format("¥ %s元", mBuyAlbumBean.price));

        MMKV mmkv = MMKV.defaultMMKV();
        String vipDiscount = mmkv.decodeString(SystemParams.VIP_DISCOUNT);
        try {
            float vipDiscountFloat = Float.parseFloat(vipDiscount);
            float priceFloat = Float.parseFloat(mBuyAlbumBean.price);
            mMoviePriceDiscountView.setText(String.format("会员(%s折):", vipDiscount.replace("0.", "")));
            mMovieDiscountPrioceView.setText(String.format("¥ %s元", decimalFormat.format(priceFloat * vipDiscountFloat)));
        } catch (Exception r) {
            r.printStackTrace();
            mMovieDiscountPrioceView.setVisibility(View.GONE);
            mMovieDiscountPrioceView.setVisibility(View.GONE);
        }

        mMovieDescView.setText(String.format("专辑介绍:%s", mBuyAlbumBean.content));

        mFollowView.setOnClickListener(v -> {
            if ("已关注".equals(mBuyAlbumBean.followMessage)) {
                BaseQuestStart.cancelAttention(new OkHttpRequestListener() {
                    @Override
                    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
                        UIUtils.shortM("成功取消关注");
                        mFollowView.setText("关注");
                        mBuyAlbumBean.followMessage = "关注";
                    }
                }, mBuyAlbumBean.user_info.id);

            } else {
                BaseQuestStart.followUser(new OkHttpRequestListener() {
                    @Override
                    public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
                        UIUtils.shortM("成功增加关注");
                        mFollowView.setText("已关注");
                        mBuyAlbumBean.followMessage = "已关注";
                    }
                }, mBuyAlbumBean.user_info.id);
            }
        });
        mRlAuthor.setOnClickListener(v -> CommonIntent.startOtherUserActiivty(that, mBuyAlbumBean.user_info.id));
        mBuyView.setOnClickListener(v -> CommonIntent.startAlumDetailsOrderPayActivity(this, mBuyAlbumBean));
        mBackView.setOnClickListener(v -> onBackPressed());

        mAdapter = new BaseQuickAdapter<MyVideoBean, BaseViewHolder>(R.layout.item_alum_details) {
            @Override
            protected void convert(BaseViewHolder helper, MyVideoBean item) {
                GlideMediaLoader.load(AlumDetailesActivity.this, helper.getView(R.id.iv_cover), item.getImage(), R.drawable.ic_place_ad_img);
                helper.setText(R.id.tv_movie_duration, DateUtil.getDateToString(item.getPlay_time() * 1000, "mm:ss"));
                helper.setText(R.id.tv_movie_title, item.getTitle());
                helper.itemView.setOnClickListener(v -> CommonIntent.startAlumDetailsPlayerActivity(v.getContext(), mBuyAlbumBean, item.getId() + "", helper.getAdapterPosition()));
            }
        };

        mAdapter.addHeaderView(mHeadView);
        rv_alum_list.setAdapter(mAdapter);

        if (mBuyAlbumBean.movies != null) {
            mAdapter.setNewData(mBuyAlbumBean.movies);
        }

        BaseQuestStart.getChannelMovies(new OkHttpRequestListener() {
            @Override
            public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
                List<MyVideoBean> videoBeanList = (List<MyVideoBean>) response.data;
                mAdapter.setNewData(videoBeanList);
            }
        }, mBuyAlbumBean.id);*/
    }
}
