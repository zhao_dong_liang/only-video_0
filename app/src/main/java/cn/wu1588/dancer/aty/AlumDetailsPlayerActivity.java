package cn.wu1588.dancer.aty;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.cnsunrun.commonui.widget.button.RoundButton;
import com.lzy.okgo.model.Progress;
import com.makeramen.roundedimageview.RoundedImageView;
import com.maning.mndialoglibrary.MProgressBarDialog;
import com.my.toolslib.DateUtil;
import com.my.toolslib.http.utils.LzyResponse;
import com.my.toolslib.http.utils.NetRequestListenerProxy;
import com.my.toolslib.http.utils.OkHttpRequestListener;
import com.shuyu.gsyvideoplayer.GSYVideoManager;
import com.shuyu.gsyvideoplayer.builder.GSYVideoOptionBuilder;
import com.shuyu.gsyvideoplayer.listener.GSYSampleCallBack;
import com.shuyu.gsyvideoplayer.utils.OrientationUtils;
import com.sunrun.sunrunframwork.bean.BaseBean;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;
import com.sunrun.sunrunframwork.uiutils.UIUtils;
import com.tencent.mmkv.MMKV;

import java.io.File;
import java.text.DecimalFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.common.CommonApp;
import cn.wu1588.dancer.common.CommonIntent;
import cn.wu1588.dancer.base.LBaseActivity;
import cn.wu1588.dancer.common.boxing.GlideMediaLoader;
import cn.wu1588.dancer.common.model.SystemParams;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.common.widget.titlebar.TitleBar;
import cn.wu1588.dancer.home.fragment.CommentFragment;
import cn.wu1588.dancer.home.mode.MovieActionStateBean;
import cn.wu1588.dancer.home.mode.MovieBean;
import cn.wu1588.dancer.mine.mode.BuyAlbumBean;
import cn.wu1588.dancer.my_video.bean.MyVideoBean;
import cn.wu1588.dancer.share.ShareBottomSheetDialog;
import cn.wu1588.dancer.videoplayer.SwitchUtil;
import cn.wu1588.dancer.videoplayer.SwitchVideo;

public class AlumDetailsPlayerActivity extends LBaseActivity {

    @BindView(R.id.title_bar)
    TitleBar rlTitleLayout;
    @BindView(R.id.moviePlayer)
    SwitchVideo moviePlayer;
    @BindView(R.id.fl_cover)
    FrameLayout flCover;
    @BindView(R.id.tv_movie_title)
    TextView tvMovieTitle;
    @BindView(R.id.tv_price)
    TextView tvPrice;
    @BindView(R.id.tv_origin_price)
    TextView tvOriginPrice;
    @BindView(R.id.btn_movie_buy)
    RoundButton btnMovieBuy;
    @BindView(R.id.rl_price)
    RelativeLayout rlPrice;
    @BindView(R.id.tv_infos)
    TextView tvInfos;
    @BindView(R.id.iv_download)
    ImageView ivDownload;
    @BindView(R.id.tv_vip_info)
    TextView tvVipInfo;
    @BindView(R.id.tv_open_vip)
    TextView tvOpenVip;
    @BindView(R.id.rl_vip_tips)
    RelativeLayout rlVipTips;
    @BindView(R.id.tv_alum_videos_title)
    TextView tvAlumVideosTitle;
    @BindView(R.id.rv_videos)
    RecyclerView rvVideos;
    @BindView(R.id.rb_rating_bar)
    RatingBar rbRatingBar;
    @BindView(R.id.tv_score)
    TextView tvScore;
    @BindView(R.id.tv_see_all)
    TextView tvSeeAll;
    @BindView(R.id.iv_video_author_avatar)
    RoundedImageView ivVideoAuthorAvatar;
    @BindView(R.id.tv_video_author_name)
    TextView tvVideoAuthorName;
    @BindView(R.id.tv_video_author_desc)
    TextView tvVideoAuthorDesc;
    @BindView(R.id.btn_movie_subscription)
    RoundButton btnMovieSubscription;
    @BindView(R.id.fl_comment_layout)
    FrameLayout flCommentLayout;
    @BindView(R.id.ibVideoCommentActionLike)
    ImageButton ibVideoCommentActionLike;
    @BindView(R.id.ibVideoCommentActionCollect)
    ImageButton ibVideoCommentActionCollect;

    private MProgressBarDialog mProgressBarDialog;

    private BuyAlbumBean mBuyAlbumBean;
    private MyVideoBean mMyVideoBean;
    private MovieBean mMovieBean;
    private String movieId;
    // 正在播放专辑的集数
    private int episode;
    private OrientationUtils orientationUtils;
    private GSYVideoOptionBuilder gsyVideoOption;
    private boolean isPlay;
    private boolean isPause;

    private DecimalFormat decimalFormat = new DecimalFormat("#.00");


    public static void start(Context context, BuyAlbumBean bean, String movieId, int index) {
        Intent intent = new Intent(context, AlumDetailsPlayerActivity.class);
        intent.putExtra("alumBean", bean);
        intent.putExtra("movieId", movieId);
        intent.putExtra("index", index);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setDark(CommonApp.getApplication().isBarTvColor());//状态栏文字颜色
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alum_details_player);
        ButterKnife.bind(this);
        mBuyAlbumBean = getIntent().getParcelableExtra("alumBean");
        movieId = getIntent().getStringExtra("movieId");
        episode = getIntent().getIntExtra("index", -1);
        rlTitleLayout.setTitleBarBackgroundDrawable(CommonApp.getApplication().getStateBarDrawable());
        findMyVideo();
        initPlayer();
        initMovies();

        if (mMyVideoBean != null) {
            CommentFragment commentFragment = new CommentFragment();
            MovieBean movieBean = new MovieBean();
            movieBean.id = Integer.parseInt(mMyVideoBean.getId());
            movieBean.title = mMyVideoBean.getTitle();
            movieBean.image = mMyVideoBean.getImage();
            commentFragment.setMovieBeanHideCommentInput(movieBean);

            getSupportFragmentManager().beginTransaction().add(R.id.fl_comment_layout, commentFragment, CommentFragment.class.getSimpleName()).commit();
        }

        getMovieActionStates();
    }

    @OnClick(R.id.btn_movie_subscription)
    void follow(View view) {
        BaseQuestStart.followUser(new OkHttpRequestListener() {
            @Override
            public void nofityUpdateUi(int requestCode, LzyResponse response, View view) {
                UIUtils.shortM(response.msg);
            }
        }, mBuyAlbumBean.user_info.id);
    }

    @OnClick(R.id.btn_movie_buy)
    void pay(View view) {
        CommonIntent.startAlumDetailsOrderPayActivity(this, mBuyAlbumBean);
    }

    @OnClick(R.id.rl_vip_tips)
    void vip(View view) {
        CommonIntent.startRechargeActivity(this, 0);
    }

    @OnClick(R.id.iv_download)
    void download(View view) {
        if (!TextUtils.isEmpty(mMovieBean.download_url) && mBuyAlbumBean.is_buy == 1) {
            initDownloadDialog();
            mProgressBarDialog.showProgress(0, "已下载0%");
            BaseQuestStart.updateMovieDownloadNum(new OkHttpRequestListener(), movieId);
            BaseQuestStart.downLoadVideo(new OkHttpRequestListener() {
                @Override
                public void downLoadFinish(File file) {
                    super.downLoadFinish(file);
                    ToastUtils.longToast("下载完成");
                }

                @Override
                public void httpLoadFail(String err) {
                    super.httpLoadFail(err);
                    ToastUtils.longToast(err);
                }

                @Override
                public void httpLoadFinal() {
                    super.httpLoadFinal();
                    if (mProgressBarDialog != null) {
                        mProgressBarDialog.dismiss();
                    }
                }

                @Override
                public void onProgress(Progress progress) {
                    super.onProgress(progress);
                    if (mProgressBarDialog != null) {
                        int fraction = (int) (progress.fraction * 100);
                        mProgressBarDialog.showProgress(fraction, "已下载" + fraction + "%", true);
                    }
                }
            }, mMovieBean.download_url);
        } else if (mBuyAlbumBean.is_buy != 1) {
            CommonIntent.startRechargeActivity(this, 1);
        }
    }

    @OnClick(R.id.commect_tv)
    void sendComment(View view) {
        Fragment f = getSupportFragmentManager().findFragmentByTag(CommentFragment.class.getSimpleName());
        if (f instanceof CommentFragment) {
            ((CommentFragment) f).sendComment();
        }
    }

    @OnClick(R.id.ibVideoCommentActionCollect)
    void collectMovie(View view) {
        BaseQuestStart.movieShoucang(new NetRequestListenerProxy(this) {
            @Override
            public void nofityUpdate(int i, BaseBean baseBean) {
                if (baseBean.status == 1) {
                    UIUtils.shortM("收藏成功");
                    getMovieActionStates();
                } else {
                    UIUtils.shortM(baseBean.msg);
                }
            }
        }, movieId);
    }

    @OnClick(R.id.ibVideoCommentActionLike)
    void likedMovie(View view) {
        BaseQuestStart.movieZan(new NetRequestListenerProxy(this) {
            @Override
            public void nofityUpdate(int i, BaseBean baseBean) {
                if (baseBean.status == 1) {
                    UIUtils.shortM("点赞成功");
                    getMovieActionStates();
                } else {
                    UIUtils.shortM(baseBean.msg);
                }
            }
        }, 1, movieId);
    }

    @OnClick(R.id.ibVideoCommentActionSend)
    void shared(View view) {
        MovieBean movieBean = new MovieBean();
        movieBean.id = Integer.parseInt(movieId);
        movieBean.title = this.mMyVideoBean.getTitle();
        movieBean.play_url = "";
        movieBean.image = mMyVideoBean.getImage();
        ShareBottomSheetDialog shareBottomSheetDialog = ShareBottomSheetDialog.newInstance(movieBean);
        shareBottomSheetDialog.show(getSupportFragmentManager(), ShareBottomSheetDialog.TAG);
    }

    private void playMovie() {
        BaseQuestStart.getMovieUrl(new NetRequestListenerProxy(this) {
            @Override
            public void nofityUpdate(int i, BaseBean baseBean) {
                mMovieBean = baseBean.Data();
                setPlayVideo(baseBean.Data());
            }
        }, movieId);
    }

    private void initMovies() {
        if (mBuyAlbumBean == null) {
            return;
        }


        tvMovieTitle.setText(mMyVideoBean.getTitle());

        MMKV mmkv = MMKV.defaultMMKV();
        String vipDiscount = mmkv.decodeString(SystemParams.VIP_DISCOUNT);
        String showDownload = mmkv.decodeString(SystemParams.IS_SHOW_DOWBTN);
        try {
            float vipDiscountInt = Float.parseFloat(vipDiscount);
            Float priceInt = Float.parseFloat(mBuyAlbumBean.price);
            Float discounted = vipDiscountInt * priceInt;
            Float discountPrice = priceInt - discounted;
            tvVipInfo.setText(String.format("开通会员，此商品立减¥%s元", decimalFormat.format(discountPrice)));
            String originPrice = mBuyAlbumBean.price;
            if (Config.getLoginInfo().isMember()) {
                mBuyAlbumBean.price = (priceInt - discountPrice) + "";
                tvOriginPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                tvOriginPrice.setVisibility(View.VISIBLE);
                tvOriginPrice.setText(String.format("原价 ¥%s", originPrice));
                tvPrice.setText(String.format("¥%s", mBuyAlbumBean.price));
                rlVipTips.setVisibility(View.GONE);
            } else {
                tvOriginPrice.setVisibility(View.GONE);
                tvOriginPrice.setText(null);
                tvPrice.setText(String.format("¥%s", originPrice));
                rlVipTips.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            tvVipInfo.setVisibility(View.GONE);
        }


        tvPrice.setText(String.format("¥%s", mBuyAlbumBean.price));
        // todo tvInfos.setText(String.format("%s人已购买/%s人已经浏览", 00));
        tvInfos.setVisibility(View.GONE);

        ivDownload.setVisibility(showDownload.equals("1") ? View.VISIBLE : View.GONE);
        //todo rbRatingBar.setRating(mBuyAlbumBean.);
        ((View) rbRatingBar.getParent().getParent()).setVisibility(View.GONE);
        GlideMediaLoader.loadHead(this, ivVideoAuthorAvatar, mBuyAlbumBean.user_info.icon);
        tvVideoAuthorName.setText(mBuyAlbumBean.user_info.nickname);
        //todo tvVideoAuthorDesc.setText(String.format("作者评分：%s", mBuyAlbumBean.user_info.total_num));
        tvVideoAuthorDesc.setVisibility(View.GONE);
        final List<MyVideoBean> movies = mBuyAlbumBean.movies;
        long duration = 0;
        for (MyVideoBean video : movies) {
            duration += video.getPlay_time();
        }
        tvAlumVideosTitle.setText(String.format("本专辑共%s集/总时长%s", mBuyAlbumBean.movies_count, DateUtil.getDateToString(duration * 1000, "mm:ss")));
        rvVideos.setAdapter(new BaseQuickAdapter<MyVideoBean, BaseViewHolder>(R.layout.layout_alum_author_works, movies) {

            @Override
            protected void convert(BaseViewHolder helper, MyVideoBean item) {
                GlideMediaLoader.load(AlumDetailsPlayerActivity.this, helper.getView(R.id.iv_movie_img), item.getImage(), R.drawable.ic_placeholder_image);
                helper.setText(R.id.tv_movie_duration, DateUtil.getDateToString(item.getPlay_time() * 1000, "mm:ss"));
                helper.setText(R.id.tv_movie_title, item.getTitle());

                helper.itemView.setOnClickListener(v -> CommonIntent.startAlumDetailsPlayerActivity(v.getContext(), mBuyAlbumBean, item.getId(), helper.getAdapterPosition()));

            }
        });
    }

    private void findMyVideo() {
        List<MyVideoBean> movies = mBuyAlbumBean.movies;
        if (movies == null || movies.isEmpty()) {
            return;
        }

        for (MyVideoBean video : movies) {
            if (video.getId().equals(movieId)) {
                mMyVideoBean = video;
                return;
            }
        }
    }

    private void initPlayer() {
        //设置返回键
        moviePlayer.getBackButton().setVisibility(View.VISIBLE);
        moviePlayer.getBackButton().setOnClickListener(v -> finish());
        //外部辅助的旋转，帮助全屏
        orientationUtils = new OrientationUtils(this, moviePlayer);
        //初始化不打开外部的旋转
        orientationUtils.setEnable(false);


        moviePlayer.getFullscreenButton().setOnClickListener(v -> {
            //直接横屏
            orientationUtils.resolveByClick();
            //第一个true是否需要隐藏actionbar，第二个true是否需要隐藏statusbar
            moviePlayer.startWindowFullscreen(AlumDetailsPlayerActivity.this, true, true);
        });
        moviePlayer.setIsClikListner(false);
        ImageView imageView = new ImageView(this);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, UIUtils.dip2px(this, 210));
        imageView.setLayoutParams(layoutParams);
        moviePlayer.setThumbImageView(imageView);
        moviePlayer.getThumbImageViewLayout().setVisibility(View.VISIBLE);
        GlideMediaLoader.load(this, imageView, mMyVideoBean.getImage(), R.drawable.ic_place_arcimg);

        setBuyState();
    }

    private void setBuyState() {
        if (mBuyAlbumBean.is_buy == 1) {
            flCover.setVisibility(View.GONE);
            rlPrice.setVisibility(View.GONE);
            playMovie();
        } else {
            if (episode == 0) {
                flCover.setVisibility(View.GONE);
                rlPrice.setVisibility(View.VISIBLE);
                playMovie();
            } else {
                flCover.setVisibility(View.VISIBLE);
                rlPrice.setVisibility(View.VISIBLE);
                flCover.setOnClickListener(v -> CommonIntent.startRechargeActivity(that, 1));
            }
        }
    }

    /**
     * 设置播放的Url
     *
     * @param movieBean
     */
    private void setPlayVideo(MovieBean movieBean) {


        gsyVideoOption = new GSYVideoOptionBuilder();
        gsyVideoOption.setIsTouchWiget(true)
                .setRotateViewAuto(false)
                .setLockLand(false)
                .setAutoFullWithSize(true)
                .setShowFullAnimation(false)
                .setNeedLockFull(true)
                .setNeedShowWifiTip(true)
                .setCacheWithPlay(false)
                .setVideoAllCallBack(new GSYSampleCallBack() {

                    @Override
                    public void onPrepared(String url, Object... objects) {
                        super.onPrepared(url, objects);
                        //开始播放了才能旋转和全屏
                        orientationUtils.setEnable(true);
                        isPlay = true;
                    }

                    @Override
                    public void onQuitFullscreen(String url, Object... objects) {
                        super.onQuitFullscreen(url, objects);
                        if (orientationUtils != null) {
                            orientationUtils.backToProtVideo();
                        }
                    }
                }).setLockClickListener((view, lock) -> {
                    if (orientationUtils != null) {
                        //配合下方的onConfigurationChanged
                        orientationUtils.setEnable(!lock);
                    }
                }).build(moviePlayer);
        gsyVideoOption.setVideoTitle(movieBean.title);
        moviePlayer.setUp(movieBean.play_url, false, movieBean.title);
        moviePlayer.startPlayLogic();
    }

    private void getMovieActionStates() {
        BaseQuestStart.getMoveieActionStates(new NetRequestListenerProxy(this) {
            @Override
            public void nofityUpdate(int i, BaseBean baseBean) {
                if (baseBean.status == 1) {
                    MovieActionStateBean movieActionStateBean = baseBean.Data();
                    if (!movieActionStateBean.isAlowZan()) {
                    }
                    if (movieActionStateBean.top == 1) {
                        ibVideoCommentActionLike.setImageResource(R.drawable.icon_liked_oro);
                    } else if (movieActionStateBean.top == 0) {
                        ibVideoCommentActionLike.setImageResource(R.drawable.icon_comment_like);
                    }
                    if (movieActionStateBean.isLike()) {
                        ibVideoCommentActionCollect.setImageResource(R.drawable.icon_comment_collect);
                    } else {
                        ibVideoCommentActionCollect.setImageResource(R.drawable.icon_oro_collection);
                    }
                }
            }
        }, movieId);//查看影片是否点赞或者收藏
    }

    private void initDownloadDialog() {
        //新建一个Dialog
        mProgressBarDialog = new MProgressBarDialog.Builder(this)
                //全屏模式
                .isWindowFullscreen(true)
                .setStyle(MProgressBarDialog.MProgressBarDialogStyle_Circle)
                //全屏背景窗体的颜色
                .setBackgroundWindowColor(Color.TRANSPARENT)
                //View背景的颜色
                .setBackgroundViewColor(Color.BLACK)
                //字体的颜色
                .setTextColor(Color.WHITE)
                //View边框的颜色
                .setStrokeColor(Color.TRANSPARENT)
                //View边框的宽度
                .setStrokeWidth(2)
                //View圆角大小
                .setCornerRadius(10)
                //ProgressBar背景色
                .setProgressbarBackgroundColor(Color.BLACK)
                //ProgressBar 颜色
                .setProgressColor(Color.WHITE)
                //圆形内圈的宽度
                .setCircleProgressBarWidth(4)
                //圆形外圈的宽度
                .setCircleProgressBarBackgroundWidth(4)
                //水平进度条Progress圆角
                .setProgressCornerRadius(0)
                //水平进度条的高度
                .setHorizontalProgressBarHeight(10)
                //dialog动画
//                .setAnimationID(R.style.animate_dialog_custom)
                .build();
    }

    @Override
    public void onBackPressed() {
        if (orientationUtils != null) {
            orientationUtils.backToProtVideo();
        }
        if (GSYVideoManager.backFromWindowFull(this)) {
            return;
        }
        super.onBackPressed();
    }


    @Override
    protected void onPause() {
        moviePlayer.getCurrentPlayer().onVideoPause();
        super.onPause();
        isPause = true;
    }

    @Override
    protected void onResume() {
        moviePlayer.getCurrentPlayer().onVideoResume(false);
        super.onResume();
        isPause = false;
    }

    @Override
    protected void onDestroy() {
        moviePlayer.getGSYVideoManager().setListener(moviePlayer.getGSYVideoManager().lastListener());
        moviePlayer.getGSYVideoManager().setLastListener(null);
        GSYVideoManager.releaseAllVideos();
        if (orientationUtils != null) {
            orientationUtils.releaseListener();
        }
        SwitchUtil.release();
        super.onDestroy();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        //如果旋转了就全屏
        if (isPlay && !isPause) {
            moviePlayer.onConfigurationChanged(this, newConfig, orientationUtils, true, true);
        }
    }
}
