package cn.wu1588.dancer.aty.my;

import android.widget.ImageView;
import android.widget.TextView;

import com.kongzue.baseframework.interfaces.DarkStatusBarTheme;
import com.kongzue.baseframework.interfaces.Layout;
import com.kongzue.baseframework.util.JumpParameter;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.base.BaseAty;

/**
 * 作者：ninetailedfox
 * 时间：2021/1/17 6:54 PM
 * 邮箱: 1037438704@qq.com
 * 功能：广告
 **/
@Layout(R.layout.aty_share_tweets)
@DarkStatusBarTheme(true)           //开启顶部状态栏图标、文字暗色模式
public class ShareTweetsAty extends BaseAty {
    //    己经设计好了舞者小程序，APP分享出去的视频以小程序的页面打开
//    获取分享记录：https://www.showdoc.com.cn/1089640610093075?page_id=5682560610472112
//    分享设置：https://www.showdoc.com.cn/1089640610093075?page_id=5680627020354621
//    更新视频分享次数：https://www.showdoc.com.cn/1089640610093075?page_id=5577641345102767
    ImageView imageFinish;
    TextView title_bar;
    TextView text_share_settings, text_share_records;

    @Override
    public void initViews() {
        text_share_records = findViewById(R.id.text_share_records);
        text_share_settings = findViewById(R.id.text_share_settings);
        imageFinish = findViewById(R.id.image_finish);
        title_bar = findViewById(R.id.title_bar);

    }

    @Override
    public void initDatas(JumpParameter parameter) {
        title_bar.setText("分享推广告");

    }

    @Override
    public void setEvents() {
        imageFinish.setOnClickListener(v -> finish());
        //分享设置
        text_share_settings.setOnClickListener(v -> {
            jump(ShareSettingsAty.class);
        });
        //分享记录
        text_share_records.setOnClickListener(v -> {
            jump(ShareRecordsAty.class);
        });

    }
}