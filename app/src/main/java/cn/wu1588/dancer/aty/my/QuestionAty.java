package cn.wu1588.dancer.aty.my;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.kongzue.baseframework.interfaces.DarkStatusBarTheme;
import com.kongzue.baseframework.interfaces.Layout;
import com.kongzue.baseframework.util.JumpParameter;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.base.BaseAty;

/**
 *  作者：ninetailedfox
 *  时间：2021/1/16 2:37 PM
 *  邮箱: 1037438704@qq.com
 *  功能：常见问题
 **/
@Layout(R.layout.aty_question)
@DarkStatusBarTheme(true)           //开启顶部状态栏图标、文字暗色模式
public class QuestionAty extends BaseAty {
    private ImageView imageFinish;
    private TextView titleText;

    @Override
    public void initViews() {
        imageFinish = findViewById(R.id.image_finish);
        titleText = findViewById(R.id.title_bar);
        titleText.setText("常见问题");
    }

    @Override
    public void initDatas(JumpParameter parameter) {

    }

    @Override
    public void setEvents() {
        imageFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}