package cn.wu1588.dancer.aty.my;

import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kongzue.baseframework.interfaces.DarkStatusBarTheme;
import com.kongzue.baseframework.interfaces.Layout;
import com.kongzue.baseframework.util.JumpParameter;
import com.kongzue.baseokhttp.HttpRequest;
import com.kongzue.baseokhttp.listener.ResponseListener;
import com.kongzue.baseokhttp.util.Parameter;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;

import java.util.ArrayList;
import java.util.Map;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.adp.ShareSettingAdp;
import cn.wu1588.dancer.base.BaseAty;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.utils.map.JSONUtils;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.AD_LIST;
import static cn.wu1588.dancer.common.quest.BaseQuestConfig.SET_AD_CURRENT;

/**
 * 作者：ninetailedfox
 * 时间：2021/1/17 8:35 PM
 * 邮箱: 1037438704@qq.com
 * 功能：分享设置：https://www.showdoc.com.cn/1089640610093075?page_id=5680627020354621
 * <p>
 * https://www.showdoc.com.cn/1089640610093075?page_id=5577652797137275  列表
 **/
@Layout(R.layout.aty_share_settings)
@DarkStatusBarTheme(true)
public class ShareSettingsAty extends BaseAty {
    private RecyclerView recyclerView;
    private ImageView image_finish;
    private TextView title_bar, text_sure;
    private ShareSettingAdp shareSettingAdp;

    @Override
    public void initViews() {
        text_sure = findViewById(R.id.text_sure);
        title_bar = findViewById(R.id.title_bar);
        image_finish = findViewById(R.id.image_finish);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        shareSettingAdp = new ShareSettingAdp(R.layout.item_share_setting);
        recyclerView.setAdapter(shareSettingAdp);
    }

    @Override
    public void initDatas(JumpParameter parameter) {
        title_bar.setText("分享设置");
        getHttp();
    }

    private void getHttp() {
        HttpRequest.POST(me, AD_LIST, new Parameter()
                        .add("uid", Config.getLoginInfo().uid)
                , new ResponseListener() {
                    @Override
                    public void onResponse(String value, Exception error) {
                        if (error == null) {
                            Map<String, String> jsonMap = JSONUtils.parseKeyAndValueToMap(value);
                            if (jsonMap.get("status").equals("1")) {
                                ArrayList<Map<String, String>> dataJson = JSONUtils.parseKeyAndValueToMapList(jsonMap.get("data"));
                                shareSettingAdp.setNewData(dataJson);
                            }
                        } else {
                            ToastUtils.shortToast("网络错误");
                        }
                    }
                });
    }

    @Override
    public void setEvents() {
        image_finish.setOnClickListener(v -> finish());
        shareSettingAdp.setOnItemClickListener((adapter, view, position) -> {
            ArrayList<Map<String, String>> dataJson = (ArrayList<Map<String, String>>) adapter.getData();
            for (int i = 0; i < dataJson.size(); i++) {
                dataJson.get(i).put("current_ad", "0");
            }
            dataJson.get(position).put("current_ad", "1");
            shareSettingAdp.notifyDataSetChanged();
        });
        text_sure.setOnClickListener(v -> {
            ArrayList<Map<String, String>> dataJson = (ArrayList<Map<String, String>>) shareSettingAdp.getData();
            for (int i = 0; i < dataJson.size(); i++) {
                if (dataJson.get(i).get("current_ad").equals("1")) {
                    setSure(dataJson.get(i).get("id"));
                }
            }

        });
    }

    public void setSure(String id) {
        HttpRequest.POST(me, SET_AD_CURRENT,
                new Parameter()
                        .add("id", id)
                        .add("user_id", Config.getLoginInfo().uid),
                new ResponseListener() {
                    @Override
                    public void onResponse(String main, Exception error) {
                        if (error == null) {
                            Map<String, String> jsonMap = JSONUtils.parseKeyAndValueToMap(main);
                            if (jsonMap.get("status").equals("1")) {
                                finish();
                            }
                        }
                    }
                });
    }

}