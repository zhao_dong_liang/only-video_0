package cn.wu1588.dancer.aty.my;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.kongzue.baseframework.interfaces.DarkStatusBarTheme;
import com.kongzue.baseframework.interfaces.Layout;
import com.kongzue.baseframework.util.JumpParameter;
import com.kongzue.baseokhttp.HttpRequest;
import com.kongzue.baseokhttp.listener.ResponseListener;
import com.kongzue.baseokhttp.util.Parameter;
import com.sunrun.sunrunframwork.uiutils.ToastUtils;

import java.util.ArrayList;
import java.util.Map;

import cn.wu1588.dancer.R;
import cn.wu1588.dancer.adp.ShareRecordsAdp;
import cn.wu1588.dancer.base.BaseAty;
import cn.wu1588.dancer.common.quest.Config;
import cn.wu1588.dancer.home.mode.MovieBean;
import cn.wu1588.dancer.share.ShareBottomSheetDialog;
import cn.wu1588.dancer.utils.map.JSONUtils;

import static cn.wu1588.dancer.common.quest.BaseQuestConfig.GET_FORWARD_RECORD;

/**
 * 作者：ninetailedfox
 * 时间：2021/1/17 11:14 PM
 * 邮箱: 1037438704@qq.com
 * 功能：分享记录
 **/
@Layout(R.layout.aty_share_records)
@DarkStatusBarTheme(true)
public class ShareRecordsAty extends BaseAty {
    private ImageView image_finish;
    private TextView title_bar;
    private RecyclerView recyclerView;
    private ShareRecordsAdp shareRecordsAdp;

    @Override
    public void initViews() {
        title_bar = findViewById(R.id.title_bar);
        image_finish = findViewById(R.id.image_finish);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(me));
        shareRecordsAdp = new ShareRecordsAdp(R.layout.item_share_records);
        recyclerView.setAdapter(shareRecordsAdp);

    }

    @Override
    public void initDatas(JumpParameter parameter) {
        title_bar.setText("分享记录");
        getHttp();
    }

    private void getHttp() {
        HttpRequest.GET(me, GET_FORWARD_RECORD, new Parameter()
                        .add("user_id", Config.getLoginInfo().uid)
//                        .add("user_id", 198)
                , new ResponseListener() {
                    @Override
                    public void onResponse(String main, Exception error) {
                        if (error == null) {
                            Map<String, String> jsonMap = JSONUtils.parseKeyAndValueToMap(main);
                            if (jsonMap.get("status").equals("1")) {
                                ArrayList<Map<String, String>> data =
                                        JSONUtils.parseKeyAndValueToMapList(jsonMap.get("data"));
                                shareRecordsAdp.setNewData(data);
                            }
                        } else {
                            ToastUtils.shortToast("数据出错");
                        }
                    }
                });
    }

    @Override
    public void setEvents() {
        image_finish.setOnClickListener(view -> finish());
        shareRecordsAdp.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                ArrayList<Map<String, String>> data =
                        (ArrayList<Map<String, String>>) adapter.getData();
                if (view.getId() == R.id.buttom_share){
                    Map<String, String> jsonMap = data.get(position);
                    MovieBean movieBean = new MovieBean();
                    movieBean.id = Integer.parseInt(jsonMap.get("id"));
                    movieBean.title = jsonMap.get("title");
                    movieBean.play_url = "";
                    movieBean.image = jsonMap.get("image_url");
                    ShareBottomSheetDialog shareBottomSheetDialog = ShareBottomSheetDialog.newInstance(movieBean);
                    shareBottomSheetDialog.show(getSupportFragmentManager(), ShareBottomSheetDialog.TAG);
                }
            }
        });
    }
}