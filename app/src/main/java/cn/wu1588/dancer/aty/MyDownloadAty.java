package cn.wu1588.dancer.aty;

import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.kongzue.baseframework.interfaces.DarkStatusBarTheme;
import com.kongzue.baseframework.interfaces.Layout;
import com.kongzue.baseframework.util.JumpParameter;
import com.my.toolslib.http.utils.NetRequestListenerProxy;
import com.sunrun.sunrunframwork.bean.BaseBean;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.base.BaseAty;
import cn.wu1588.dancer.common.quest.BaseQuestStart;
import cn.wu1588.dancer.model.MessageNoticeBean;

@Layout(R.layout.aty_my_download)
@DarkStatusBarTheme(true)           //开启顶部状态栏图标、文字暗色模式
public class MyDownloadAty extends BaseAty {
    private ImageView imageFinish;
    private TextView titleText;
    private RecyclerView recyclerView;
    private BaseQuickAdapter<MessageNoticeBean, BaseViewHolder> mAdapter;
    @Override
    public void initViews() {
        imageFinish = findViewById(R.id.image_finish);
        titleText = findViewById(R.id.title_bar);
        titleText.setText("我的下载");
        recyclerView = findViewById(R.id.aty_my_download_rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mAdapter = new BaseQuickAdapter<MessageNoticeBean, BaseViewHolder>(R.layout.item_my_download_layout) {
            @Override
            protected void convert(BaseViewHolder helper, MessageNoticeBean item) {
            }
        });
    }

    @Override
    public void initDatas(JumpParameter parameter) {
        BaseQuestStart.getMsgNoticeList(1,new NetRequestListenerProxy(this){
            @Override
            public void nofityUpdate(int i, BaseBean baseBean) {
                mAdapter.setNewData(baseBean.Data());
            }
        });
    }

    @Override
    public void setEvents() {
        imageFinish.setOnClickListener(v -> finish());
    }
}
