package cn.wu1588.dancer.aty.my;

import android.widget.ImageView;
import android.widget.TextView;

import com.flyco.tablayout.SlidingTabLayout;
import com.kongzue.baseframework.interfaces.DarkStatusBarTheme;
import com.kongzue.baseframework.interfaces.Layout;
import com.kongzue.baseframework.util.JumpParameter;
import com.my.toolslib.http.utils.NetRequestListenerProxy;
import com.sunrun.sunrunframwork.bean.BaseBean;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.base.BaseAty;
import cn.wu1588.dancer.fgt.my.MyFansFgt;
import cn.wu1588.dancer.model.FansNumBean;
import cn.wu1588.dancer.base.ViewPagerFragmentAdapter;
import cn.wu1588.dancer.common.quest.BaseQuestStart;

@Layout(R.layout.aty_my_fans)
@DarkStatusBarTheme(true)
public class MyFansAty extends BaseAty {
    private String[] mTitles = {"铁粉", "金粉"};
    private ImageView imageFinish;
    private TextView titleText,tvFans,tvGoldFans;
    private List<Fragment> baseFragments;
    private ViewPagerFragmentAdapter mVPAdapter;
    SlidingTabLayout tabLayout;
    ViewPager viewPager;
    @Override
    public void initViews() {
        int goldFans = getIntent().getIntExtra("isGoldFans", 0);
        imageFinish = findViewById(R.id.image_finish);
        titleText = findViewById(R.id.title_bar);
        tvFans = findViewById(R.id.aty_my_fans_num);
        tvGoldFans = findViewById(R.id.aty_my_fans_gold);
        titleText.setText("我的粉丝");
        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewPager);
        baseFragments = new ArrayList<>();
        baseFragments.add(MyFansFgt.newInstance("0"));//铁粉
        baseFragments.add(MyFansFgt.newInstance("1"));//金粉
        mVPAdapter = new ViewPagerFragmentAdapter(getSupportFragmentManager());
        mVPAdapter.setFragments(baseFragments);
        viewPager.setAdapter(mVPAdapter);
        viewPager.setCurrentItem(goldFans, false);
        tabLayout.setViewPager(viewPager, mTitles);
    }

    @Override
    public void initDatas(JumpParameter parameter) {
        BaseQuestStart.getFansNum(new NetRequestListenerProxy(this){
            @Override
            public void nofityUpdate(int i, BaseBean baseBean) {
                doData(baseBean);
            }
        });
    }
    private void doData(BaseBean bean){
        if (bean.status == 1) {
            FansNumBean fansNumBean = bean.Data();
            if (fansNumBean != null) {
                tvFans.setText(fansNumBean.getFans() == null ? "0" : fansNumBean.getFans());
                tvGoldFans.setText(fansNumBean.getGold_fans() == null ? "0" : fansNumBean.getGold_fans());
            }
        }
    }
    @Override
    public void setEvents() {
        imageFinish.setOnClickListener(v -> finish());
    }
}