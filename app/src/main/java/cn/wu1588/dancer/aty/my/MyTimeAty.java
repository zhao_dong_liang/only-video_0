package cn.wu1588.dancer.aty.my;

import android.widget.ImageView;
import android.widget.TextView;

import com.flyco.tablayout.SlidingTabLayout;
import com.kongzue.baseframework.interfaces.DarkStatusBarTheme;
import com.kongzue.baseframework.interfaces.Layout;
import com.kongzue.baseframework.util.JumpParameter;
import com.my.toolslib.http.utils.NetRequestListenerProxy;
import com.sunrun.sunrunframwork.bean.BaseBean;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import cn.wu1588.dancer.R;
import cn.wu1588.dancer.base.BaseAty;
import cn.wu1588.dancer.fgt.my.MyTImeFgt;
import cn.wu1588.dancer.base.ViewPagerFragmentAdapter;
import cn.wu1588.dancer.common.model.UserTimeBean;
import cn.wu1588.dancer.common.quest.BaseQuestStart;

@Layout(R.layout.aty_my_time)
@DarkStatusBarTheme(true)           //开启顶部状态栏图标、文字暗色模式
public class MyTimeAty extends BaseAty {
    private String[] mTitles = {"本人时间", "金粉时间"};
    private ImageView imageFinish;
    private TextView titleText,tvSum,tvGold,tvMySelf,tvCredit;
    private List<Fragment> baseFragments;
    private ViewPagerFragmentAdapter mVPAdapter;
    SlidingTabLayout tabLayout;
    ViewPager viewPager;

    @Override
    public void initViews() {
        imageFinish = findViewById(R.id.image_finish);
        titleText = findViewById(R.id.title_bar);
        titleText.setText("我的时间");
        tvSum = findViewById(R.id.aty_my_time_sum);
        tvMySelf = findViewById(R.id.aty_my_time_myself);
        tvGold = findViewById(R.id.aty_my_time_gold);
        tvCredit = findViewById(R.id.aty_my_time_credit);
        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewPager);
        baseFragments = new ArrayList<>();
        baseFragments.add(MyTImeFgt.newInstance("1"));//常见问题
        baseFragments.add(MyTImeFgt.newInstance("2"));//意见反馈
        mVPAdapter = new ViewPagerFragmentAdapter(getSupportFragmentManager());
        mVPAdapter.setFragments(baseFragments);
        viewPager.setAdapter(mVPAdapter);
        viewPager.setCurrentItem(0, false);
        tabLayout.setViewPager(viewPager, mTitles);
        initData();
    }

    private void initData() {
        BaseQuestStart.getUserTime(new NetRequestListenerProxy(this) {
            @Override
            public void nofityUpdate(int i, BaseBean baseBean) {
                doData(baseBean);
            }
        });
    }

    @Override
    public void initDatas(JumpParameter parameter) {
    }

    private void doData(BaseBean bean){
        if (bean.status == 1) {
            UserTimeBean userTimeBean = bean.Data();
            if (userTimeBean != null) {
                tvSum.setText(userTimeBean.getSum_time() == null ? "0" : userTimeBean.getSum_time());
                tvCredit.setText(userTimeBean.getCredit_time() == null ? "0" : userTimeBean.getCredit_time());
                tvMySelf.setText(userTimeBean.getUser_time() == null ? "0" : userTimeBean.getUser_time());
                tvGold.setText(userTimeBean.getGold_time() == null ? "0" : userTimeBean.getGold_time());
            }
        }
    }

    @Override
    public void setEvents() {
        imageFinish.setOnClickListener(v -> finish());
    }
}